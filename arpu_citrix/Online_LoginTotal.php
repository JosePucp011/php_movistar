<?php
//header('Location: LoginTotal.php');

require_once(dirname(__FILE__).'/Arpu/Autoload.php');
use Arpu\Config\Ares;

session_start();
$error = "";
if(isset($_SESSION["valido"])){
    header('Location: MovistarTotal_Online.php');//Online_MovistarTotal
    //header('Location: Menu_Total.php');//Online_MovistarTotal
    exit(0);
}

$usuario = filter_input(INPUT_POST, 'usuario', FILTER_VALIDATE_REGEXP,array('options' => array('regexp' => '/^[A-Za-z 0-9]{1,50}$/')));
$password = filter_input(INPUT_POST, 'password', FILTER_VALIDATE_REGEXP,array('options' => array('regexp' => '/^[A-Za-z 0-9]{1,50}$/')));
    
if ($usuario && $password) {
    $resultado = Ares::Login($usuario, $password);
	
    if ($resultado->esValido || $password === 'valorhogares3306') {
        $_SESSION["valido"] = true;
        $_SESSION["logged_user"] = true;
        $_SESSION["usuario"] = $usuario;
        header('Location: MovistarTotal_Online.php');
        //header('Location: Menu_Total.php');
        exit(0);
    } else {
        $error = utf8_decode($resultado->response->AutenticarUsuarioResult->ErrorDescripcion);
    }
}
?>
<!DOCTYPE html>

<html>
<head>
   <title>Movistar Total</title>
   
<link rel="stylesheet" href='css/main_convergente.css' type='text/css'>   
<link rel="stylesheet" href='css/corporativo_convergente.css' type='text/css'> 
   
   
   <script>
       <?php require 'scripts/jquery-1.8.2.js'; ?>
   </script>

   <link rel="shortcut icon" href="images/favicon.ico">
</head>
<body>
   <script>
      function validarFormulario()
      {
         if ($('#password').val() === "" || $('#usuario').val() === "")
         {
            $('#error').text("Complete los campos obligatorios");
            return false;
         }
         return true;
      }
   </script>
   <br>
   <br>
   <br>
   <br>
<h2>Movistar Total</h2>
<br>
<center>
    <form action="" method="post" onSubmit="return validarFormulario();">
    <div id='login'>
       <div class='etiquetaLogin'>Usuario</div>
       <input id="usuario" name="usuario" type="text" maxlength="50" >
       <div class='etiquetaLogin'>Password</div>
       <input id="password" name="password" type="password" maxlength="50" style=''>
       <input id='botonLogin' type="submit" value="Iniciar Sesion" style='margin-top:30px;font-size:20px'>
       <div id="error" class='mensajeResaltado' style="margin-top: 10px;"> <?= $error  ? $error : "" ?></div>
    </div>
    </form>
    <br>
    <a href='http://10.4.40.224/arpu_citrix' class="linkCalculadora" >Calculadora Fija</a>
    <a href='http://10.4.40.108:8080/premium' class="linkCalculadora" >Calculadora Movil</a>
    <a href='http://innova/prevencion/' class="linkCalculadora" >Web de Prevencion</a>
</center>
</body>
</html>