$(function() {
    jQuery.fx.off = false;
   
    InicializarInterfaz();
    InicializarPopup();
    Formulario.Inicializar();
});

function InicializarInterfaz(){
    
}

function InicializarPopup(){
    //CrearPopup("#modal-NoRetener",500,500,150);
}

function CrearPopup(Dialogo,width,maxHeight,minHeight){
    $(Dialogo).dialog({
       autoOpen: false, resizable: false, draggable: false, modal: true,
       width: width,
       maxWidth: width,
       maxHeight: maxHeight,
       minHeight: minHeight
    });
}
  