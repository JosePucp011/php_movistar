<?php
session_start();
if (!isset($_SESSION["logged_user"]) || isset($_SESSION["logged_user"]) && !$_SESSION["logged_user"]) 
{
    header('Location: login.php');
}


?>

<!DOCTYPE html>

<html>
<head>
    <title>Ofertas en Playas</title>
    
    <link rel="shortcut icon" href="./../images/favicon.ico">
    
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">

    <link rel="stylesheet" href='css/main.css' type='text/css'>
    <link rel='stylesheet' href='css/custom-theme/jquery-ui-1.9.0.custom.min.css' type='text/css'>

    <script src='scripts/jquery-1.8.2.js' type='text/javascript'></script>
    <script src='scripts/Datos.js' type='text/javascript'></script>
    <script src='scripts/Formulario.js' type='text/javascript'></script>

    <script src='index.js' type='text/javascript'></script>
    <script src='scripts/jquery-ui-1.9.0.custom.min.js' type='text/javascript'></script>
    <script src='scripts/jquery-ui-1.9.0.custom.js' type='text/javascript'></script>
</head>

<body>

<div id="session-header"  align="right"><a href="login.php?action=logout">Cerrar Sesi&oacute;n</a></div>

<form action='' method='post' onsubmit="return false;">
<table>
    <tr>
        <td id='datos_buscar' valign='top'>
            <div id="Identificadores" >
                <h1>Buscar Playa: </h1><br>
                <input type="hidden" id="playa-id">
                <input  maxlength='25'
                        autocomplete='off' 
                        type='text' 
                        id='playa' 
                        name='playa' 
                        value=''><br>                
            </div>    
            <div id='MenuArgumentario'>
                <h1>Speech Argumentario:</h1><br>
                <a href='argumentario/Argumentario Alta Playas v1.htm' target='_blank'>
                        <div class ='MenuArgumentarioUl'>Argumentario Alta Playas</div></a>
                        
                <a href='argumentario/Argumentario Renovación Playas v1.htm' target='_blank'>
                        <div class ='MenuArgumentarioUl'>Argumentario Renovación Playas</div></a>
                        
                <a href='argumentario/Contrato Punto adicional v1.htm' target='_blank'>
                        <div class ='MenuArgumentarioUl'>Contrato Punto adicional</div></a>
                        
                <a href='argumentario/Contrato Servicio Playas v1.htm' target='_blank'>
                        <div class ='MenuArgumentarioUl'>Contrato Servicio Playas</div></a>
                        
              <!--   <a href='argumentario/Contrato de Dúo HFC.htm' target='_blank'>
                        <div class ='MenuArgumentarioUl'>Contrato de Dúo HFC</div></a>
                        
		        <a href='argumentario/Contrato de Voz Tv.htm' target='_blank'>
                        <div class ='MenuArgumentarioUl'>Contrato de Voz Tv</div></a> -->
                <!-- <a href='argumentario/ContratoPuntoadicional.htm' target='_blank'>
                        <div class ='MenuArgumentarioUl'>Contrato Punto Adicional</div></a> -->
            </div>
        </td>
        <td id='datos_mostrar'  valign="top">
            <div id='datos_playa' class='medio'></div>
            <div id="datos_contenido" class='medio'> </div>
        </td>
    </tr>
</table>
</form>
</body>
</html>