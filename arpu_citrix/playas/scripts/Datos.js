var Datos = {};

Datos.Playas = new Array();
Datos.Productos = new Array();
Datos.CantPlayas = 0;

Datos.MostrarDatos = function(PlayaId)
{
    Formulario.LimpiarPantalla();
    var Playa = Datos.Playas[PlayaId];
    
    var contenido = Datos.ImprimeDatos(Playa);
    $('#datos_mostrar').html(contenido);
    
    var ofertas = Datos.ImprimeOfertas(Playa.Oferta);
    $('#Parrilla_Ofertas').html(ofertas);
    
    Datos.InicializaAcordion();
};

Datos.ImprimeDatos = function(Playa) 
{
    var Observacion = Playa.Observaciones !== null ? Playa.Observaciones:'';
    var DTH = Playa.Cobertura_dth!== null ?  'Cobertura DTH: '+Playa.Cobertura_dth:'';
    var CATV = Playa.Cobertura_catv!== null ?  'Cobertura CATV: '+Playa.Cobertura_catv:'';
    var HFC = Playa.Cobertura_hfc==1 ?  'Cobertura HFC':'';
    var html = '';
    html += '<table>'
         +    '<tr>'
         +       '<td>'
         +          '<span class="Titulo1">' + Playa.Nombre + '</span>&nbsp;&nbsp;'
         +          '<span class="Titulo2">(' + Playa.Km + ')</span>'
         +       '</td>'
         +       '<td>'
         +          '<span class="error">' + DTH + '<br>' + CATV + '<br>' + HFC + '</span>&nbsp;&nbsp;'
         +       '</td>'
         +    '<tr>'
         +    '<tr>'
         +       '<td class="Ubicacion" colspan="100%">' + Playa.Sector + ' / ' + Playa.Zona + ' / ' + Playa.Provincia + '</td>'
         +    '<tr>'
         +    '<tr>'
         +       '<td class="Observacion error" colspan="100%">' + Observacion + '</td>'
         +    '<tr>'
         +    '<tr>'
         +       '<td id="Parrilla_Ofertas" colspan="100%"></td>'
         +    '<tr>'
         +    '<tr>'
         +       '<td class="Titulo1" colspan="100%">Playa: ' + Playa.Descripcion + '</td>'
         +    '<tr>'
         +  '</table>';
   return html;
};

Datos.ImprimeOfertas = function(Ofertas)
{
    var html = '<div id="Acordion">';
    for(var i=0; i<Ofertas.length; i++)
    {
        var Producto = Datos.Productos[ parseInt(Ofertas[i]) ];
        html += '<h3>'
             +      '<span class="Left">' + Producto.Nombre + '</span>'
             +      '<span class="Right">&nbsp;&nbsp;&nbsp;S/.' + Producto.Precio + '</span>'
             +  '</h3>'
             +  '<div><p>' + Producto.Descripcion + '</p><p class="Detalle">' + Producto.Detalle + '</p></div>';
    }
    html += '</div>';
    return html;
};

Datos.InicializaAcordion = function(){
    var icons = {
      header: "ui-icon-circle-arrow-e",
      activeHeader: "ui-icon-circle-arrow-s"
    };
    $( "#Acordion" ).accordion({
        heightStyle: "content",
        icons: icons
    });
};
