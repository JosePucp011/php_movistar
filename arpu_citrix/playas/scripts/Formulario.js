var Formulario = {};

Formulario.ultimoPlaya = null;

Formulario.Inicializar = function(){
    Formulario.ObtenerDatos();  
};

Formulario.Actualizar = function(){
    Formulario.LimpiarPantalla();
};

Formulario.ObtenerDatos = function(){
    $.ajax({
        url: 'http/Consultar.php',
        context: document.body,
        type: 'GET',
        data:  {},
        datatype: 'json',
        success: Formulario.CompletaDatos
    });
};

Formulario.CompletaDatos = function(response){
    Datos.Playas = response.Playas;
    Datos.CantPlayas = response.CantPlayas;
    Datos.Productos = response.Productos;
    
    var ListadoPlayas = Formulario.MostrarBusqueda();
    $('#playa').autocomplete({
        minLength: 0,        
        source: ListadoPlayas,
        focus: function( event, ui ) {
            $( "#playa" ).val( ui.item.label );
            return false;
        },
        select: function( event, ui ) {
            $( "#playa" ).val( ui.item.label );
            $( "#playa-id" ).val( ui.item.value );
            Datos.MostrarDatos(ui.item.value);
            return false;
        }
    });  
};

Formulario.MostrarBusqueda = function(){
    var Playas = new Array();
    Playas[0] = {
            value: 0,
            label: ''
        };
    for(var i=1; i<=Datos.CantPlayas; i++)
    {
        Playas[Datos.Playas[i].Id]= {
            value: Datos.Playas[i].Id,
            label: Datos.Playas[i].Descripcion
        };
    }
    return Playas;
};

Formulario.LimpiarPantalla = function(){
    $('#datos_playa').html('');
    $('#datos_contenido').html('');
    $('#datos_observacion').html('');

    Formulario.ultimoPlaya = null;
};

