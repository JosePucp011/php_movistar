<?php

use Arpu\Data\RegistroAccesoCalculadoraDL;

require_once(dirname(__FILE__).'/../Arpu/Autoload.php');

use Arpu\Config\Ares;

session_start();
$result = VerificaSession();
RedirectToIndex($result['RedirectToIndex']);
	
function VerificaSession()
{  
    
    $result = Array();
    $result['ErrorMessage'] = "";
    $result['Username'] = "";
    $result['Password'] = "";
    $result['RedirectToIndex'] = false;
    
    // Checking the actions on the login page
    if (isset($_POST["action"]) && $_POST["action"] == "login")
    {
       // Checking the fields from login form
       if (isset($_POST["username"]))
       {
          $result['Username'] = trim($_POST["username"]);
       }
       if ($result['Username'] == "")
       {
          $result['ErrorMessage'] = "Usuario es un campo obligatorio, usted debe ingresarlo. ";
       }
       if (isset($_POST["password"]))
       {
          $result['Password'] = trim($_POST["password"]);
       }
       if ($result['Password'] == "")
       {
          $result['ErrorMessage'] = $result['ErrorMessage'] . "Contraseña es un campo obligatorio, usted debe ingresarlo.";
       }
       if ($result['ErrorMessage'] == "")
       {
          // Invoking web service to authenticate user
          $response = Ares::Login($result['Username'], $result['Password']);
		  
		  
          if ($response->esValido)
          {
             // Changing state of session variable
              $_SESSION["logged_user"] = true;
              $_SESSION["logged_username"] = $result['Username'];
//              $_SESSION['start'] = time(); // Taking now logged in time.
//             // Ending a session in 10 minutes from the starting time.
//              $_SESSION['expire'] = $_SESSION['start'] + (10 * 60);
//             // Redirect to main page because user is logged
             $result['RedirectToIndex'] = true;
          }
          else
          { 
             $result['ErrorMessage'] = utf8_decode($response->response->AutenticarUsuarioResult->ErrorDescripcion);
          }
       }
    }
    elseif (isset($_GET["action"]) && $_GET["action"] == "logout")
    {
       RegistroAccesoCalculadoraDL::insertarRegistroDeLogeo($_SESSION["logged_username"],'Logout'); 
       // Changing state of session variable
       $_SESSION["logged_user"] = false;
    }
    elseif (isset($_SESSION["logged_user"]) && $_SESSION["logged_user"])
    {
       // Redirect to main page because user is logged
       $result['RedirectToIndex'] = true;
    }
    else
    {
       if (isset($_POST["username"]))
       {
          $result['Username'] = $_POST["username"];
       }
       if (isset($_POST["password"]))
       {
          $result['Password'] = $_POST["password"];
       }
    }
    
    if($result['RedirectToIndex'] && isset($_SESSION["logged_username"]))
    {
        $response = RegistroAccesoCalculadoraDL::insertarRegistroDeLog(0, $_SESSION["logged_username"],'Inicio Sesion');
        $response = RegistroAccesoCalculadoraDL::insertarRegistroDeLogeo($_SESSION["logged_username"],'Login');
    }
    if($result['ErrorMessage']!= "")
    {
        $response = RegistroAccesoCalculadoraDL::insertarRegistroDeLogeo($_POST["username"],$result['ErrorMessage']);
    }
    return $result;
}

function RedirectToIndex($RedirectToIndex)
{
    if($RedirectToIndex)
    {
        header('Location: index.php');
    }
}
?>
<!DOCTYPE html>

<html>
<head>
   <title>Calculadora de ARPU</title>
   <link rel="stylesheet" href='./../css/login.css' type='text/css'>
   <link rel="shortcut icon" href="./../images/favicon.ico">
</head>
<body id="pagina_login"><!--	<body id="pagina_login">
   -->
   <script>
      function validateForm() 
      {
         var username = document.getElementById("username").value;
         if (username === null) 
         {
            username = "";
         } 
         else 
         {
            username = username.trim();
         }
         var password = document.getElementById("password").value;
         if (password === null) 
         {
            password = "";
         } 
         else 
         {
            password = password.trim();
         }
         var errorMessage = "";
         if (username === "") 
         {
            errorMessage = "Usuario es un campo obligatorio, usted debe ingresarlo. ";
         }
         if (password === "") {
            errorMessage = errorMessage + "Contraseña es un campo obligatorio, usted debe ingresarlo.";
         }
         if (errorMessage === "") 
         {
            return true;
         } 
         else 
         {
            alert(errorMessage);
            return false;
         }
      }
   </script>


   <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 10em; top: 50%; left: 50%; padding: 15px;z-index: 1; font-family:Arial;">

      <tr>
         <td><div id="imagen_login" style="width: 100%; height: 7em;" align="right"/></td>
      </tr>
      <tr>
         <td><p></p></td>
      </tr>
      <tr>
         <td><p></p></td>
      </tr>
      <tr>
         <td><p></p></td>
      </tr>

      <tr align="center">
         <td><div>
               <!--		div style="position: relative; width: 300px; height: 10em; top: 50%; left: 50%; padding: 15px;z-index: 1;"-->
               <form id="formulario_login" action="" method="post" onSubmit="return validateForm();">
                  <h1>Calculadora Arpu </h1>
                  <p></p>
                  <p></p>
                  <fieldset>
                     <legend>Inicio de Sesi&oacute;n</legend>
                     <input type="hidden" id="action" name="action" value="login">
                     <div <?php if ($result['ErrorMessage'] != ""){ ?> class="mensaje_error" <?php } ?>><?php echo $result['ErrorMessage']; ?></div>
                     <p><span class="etiqueta_login">Usuario:</span>
                        <input id="username" name="username" type="text" maxlength="50" value="<?php echo $result['Username']; ?>">*</br></p>
                     <p><span class="etiqueta_login">Contrase&ntilde;a:</span>
                        <input id="password" name="password" type="password" maxlength="50" value="<?php echo $result['Password']; ?>">*</br></p>
                     <p><input type="submit" class="buttonLogin"  value="Acceder"></p>
                  </fieldset>
               </form>
            </div></td>
      </tr>
      <tr align="center"><td>
              
          <div><a href='http://10.4.40.108:8080/premium'  target='new'  style='text-decoration:none;  color: rgb(255,255,255);'>
          <div> <h2>Calculadora M&#243vil</h2></a></div>   
          </div>

          <div><a href='http://innova/prevencion/'  target='new'  style='text-decoration:none;   color: rgb(255,255,255);'>
          <div> <h2 style="margin-bottom: 5px;" >Calculadora Prevenci&#243n</h2></a></div>   
          </div>   
          
      </td></tr>
      
   </table>

</body>
</html>