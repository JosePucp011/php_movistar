<?php
header('Content-type: application/json');
require_once('../../../../../Playas/Autoload.php');

try
{
   $resultado = new stdClass();
   $resultado->Productos = ProductoDL::$cache ? ProductoDL::$cache : ProductoDL::Obtener();
   $resultado->Playas = PlayaDL::$cache ? PlayaDL::$cache : PayaDL::Obtener();
   $resultado->CantPlayas = count($resultado->Playas);
   
   echo json_encode($resultado);
}
catch(InvalidArgumentException $excepcion)
{
   die(json_encode((object)array('error' => $excepcion->getMessage())));
}
catch (ClienteNoEncontrado $excepcion)
{
   die(json_encode((object) array('error' => 'No se encuentra la Playa')));
}
catch (Exception $excepcion)
{
   die(json_encode((object) array('error' => 'Ocurrio un problema')));
}



