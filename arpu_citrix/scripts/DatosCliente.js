var DatosCliente = {};


DatosCliente.MostrarResultado = function (ResultadoConsulta)
{
    window.ResultadoConsulta = ResultadoConsulta;
    var Cliente = ResultadoConsulta.Cliente;
    window.cliente = Cliente;

    if (ResultadoConsulta.error !== undefined)
    {
        $('#error_busqueda').html(ResultadoConsulta.error);
        $('#error_busqueda').show();
        return;
    }

    $('#error_busqueda').hide();

    DatosCliente.MostrarDatosCliente(ResultadoConsulta);
    Comparacion.Mostrar(ResultadoConsulta);
    Parrilla.Mostrar(ResultadoConsulta);

};

DatosCliente.MostrarMensajes = function(mensajes,Cliente)
{
   
   var Resultado = "";
   
   
   Resultado +="<table><tr>";
   
   var despo = 0;
   
   for(var i = 0; i < mensajes.length; ++i)
   {
     
       var mensaje = mensajes[i];
       if(mensaje.Velocidad === null && mensaje.Renta === null && mensaje.Minutos === null && mensaje.Canales === null && mensaje.Fecha === null )
       {
           if(mensaje.Mensaje === 'Cliente Convergente'  )
           {
                Resultado +="<td>";
                Resultado +="<div style='width: 300px; margin: 5px;' ><a href='//10.4.40.224/movistar_total' target='_new' id='descuentoRetenciones_0' style='text-decoration:none;' ><h1 style='border-radius: 4px; background-color:#FFEFEF; border: 1px solid red;color:red;padding: 6px;' > Cliente Movistar Total <br></h1></a></div>";
                Resultado +="</td>";

            }
           else 
                if(mensaje.Mensaje === 'Prioridad Migracion HFC'  )
                {
                     if(Cliente.Internet.Cobertura.HFC === 1 )
                     {
                         Resultado +="<td>";
                         Resultado +="<div style='width: 300px; margin: 5px;' ><a href='#' id='descuentoRetenciones_0' class='linkCiudadSitiada' style='text-decoration:none;' ><h1 style='border-radius: 4px; background-color:#FFEFEF; border: 1px solid red;color:red;padding: 6px;' >" + mensaje.Mensaje + "<br></h1></a></div>";
                         Resultado +="</td>";
                         
                         despo = 1;
                     }
                 }
                 else
                {
                     Resultado +="<td>";
                     Resultado +="<div style='width: 300px; margin: 5px;' ><h1 style='border-radius: 4px; background-color:#FFEFEF; border: 1px solid red;color:red;padding: 6px;' >" + mensaje.Mensaje + "<br></h1></div>";
                     Resultado +="</td>";
                 }
       }
       else
       {
           if(Cliente.IncrementoPrecio === 1)
           {Resultado +="<td>";
           Resultado +="<div style='width: 300px;  margin: 5px;'><a href='#' id='descuentoRetenciones_0' class='linkIncrementoPrecio' style='text-decoration:none;' ><h1 style=' border-radius: 4px; background-color:#F1FFED; border: 1px solid #5bc500;color:#5bc500;padding: 6px;' > Incremento de Precio <br></h1></a></div>";
           Resultado +="</td>";}
                  
           if(mensaje.Mensaje === 'Incremento de Velocidad')
           {Resultado +="<td>";
           Resultado +="<div style='width: 300px;  margin: 5px;'><a href='#' id='descuentoRetenciones_0' class='linkIncrementoPrecio' style='text-decoration:none;' ><h1 style=' border-radius: 4px; background-color:#F1FFED; border: 1px solid #5bc500;color:#5bc500;padding: 6px;' > Incremento de Velocidad <br></h1></a></div>";
           Resultado +="</td>";}
       }
       
       if(i === 2 )
       {
           Resultado +="<tr></tr>";
       }
   }
   
   Resultado +="<td><div style='width: 300px;  margin:5px;' ><a href='#' id='descuentoRetenciones_0' class='linkPlanRegalos' style='text-decoration:none;' ><h1 style='border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;' >Beneficios Movistar<br></h1></a></div></td>";
  
   if(Cliente.Desposicionado === '1' && despo === '0')
   {
        Resultado +="<td><div style='width: 300px; margin: 5px;' ><a href='#' id='descuentoRetenciones_0' style='text-decoration:none;' ><h1 style='border-radius: 4px; background-color:#FFEFEF; border: 1px solid red;color:red;padding: 6px;' > Prioridad de Migracion <br></h1></a></div></td>";
   }
   
   if(Cliente.MovistarTotal === 1)
   {    //console.log(Cliente.Reasignacion);
        if(Cliente.Reasignacion!=undefined ){ 
            Resultado +="<td><div style='width: 300px; margin: 5px;' ><a href='#' class='linkReasignacion' id='descuentoRetenciones_0' style='text-decoration:none;' ><h1 style='border-radius: 4px; background-color:#efe; border: 1px solid #5bc500;color:#5bc500;padding: 6px;' >Cliente Movistar Total con reasignación de precios <br></h1></a></div></td>";
        }else{
            Resultado +="<td><div style='width: 300px; margin: 5px;' ><a href='./MovistarTotal.php' id='descuentoRetenciones_0' style='text-decoration:none;' ><h1 style='border-radius: 4px; background-color:#efe; border: 1px solid #5bc500;color:#5bc500;padding: 6px;' >Cliente Movistar Total<br></h1></a></div></td>";
        }              
   }   
   
   if(  
    (ResultadoConsulta.Modalidad=="MigracionDown" || ResultadoConsulta.Modalidad=="Averia" || ResultadoConsulta.Modalidad=="Baja" ) &&
     Cliente.Analgesico==1 && 
     (Cliente.AnalgesicoTipo=="4G" || Cliente.AnalgesicoTipo=="HD" || Cliente.AnalgesicoTipo=="PRIX")
 ){
 Resultado +="<td><div style='width: 300px; margin: 5px;' ><a href='#' class='linkAnalgesico' id='descuentoRetenciones_0' style='text-decoration:none;' ><h1 style='border-radius: 4px; background-color:#efe; border: 1px solid #5bc500;color:#5bc500;padding: 6px;' >Plan Analgésico <br></h1></a></div></td>";
}  
   Resultado +="</tr></table>";
   $('#MigracionMasiva').html(Resultado);
};


DatosCliente.MostrarDatosCliente = function(ResultadoConsulta) 
{
    
   
    
   var Cliente = ResultadoConsulta.Cliente;
   window.cliente = Cliente;
   
    $('#Cliente_Telefono').text(Cliente.Telefono);
    $('#Cliente_Nombre').text(Cliente.Nombre);
    $('#Cliente_ClienteCms').text(Cliente.ClienteCms);
    $('#Cliente_Ciclo').text(Cliente.Ciclo);
    $('#Cliente_Segmento').text(Cliente.Segmento);
    $('#Cliente_ReciboDigital').text(Cliente.ReciboDigital);
    $('#Cliente_ProteccionDatos').text(Cliente.ProteccionDatos);
    $('#Cliente_Desposicionado').text(Cliente.DesposicionadoTexto);
    $('#Cliente_Antiguedad').html(Cliente.Antiguedad);
    $('#Cliente_TipoBlindaje').text(Cliente.TipoBlindaje);
    $('#Cliente_Averias').html(Cliente.Averias);
    $('#Cliente_Reclamos').html(Cliente.Reclamos);
    $('#Cliente_Llamadas').html(Cliente.Llamadas);
    $('#Cliente_TiempoMigracionTecn').html(Cliente.TiempoMigracionTecn);
    $('#Cliente_TiempoAveria').html(Cliente.TiempoAveria);   
    $('#Cliente_ZonaCompetencia').html(Cliente.ZonaCompetencia === 1 ? 'SI' : 'NO');
    $('#Cliente_OperadorZonaCompetencia').html(Cliente.OperadorZonaCompetencia);
    

    if('9999-12-31' !== Cliente.Digitalizado)
    {
        $('#Cliente_Digitalizado').html(Cliente.Digitalizado);
    }
    else if(Cliente.Ubicacion.Troba == "")
    {
        $('#Cliente_Digitalizado').html('-');
    }
    else
    {
        $('#Cliente_Digitalizado').html('No Digitalizado');
    }

    if( Cliente.EsPotencialMT ){  
        var html = 'Cliente potencial para la <h3 style="display:inline-block;">venta de Movistar Total</h3>';    
        $("#modal-ClientePotencial").html(html);
        $("#modal-ClientePotencial").dialog("open");              
    }

    if( Cliente.Mensajes.length>0 &&
        ( ResultadoConsulta.Modalidad === 'Regular' ||
        ResultadoConsulta.Modalidad === 'Baja' ||
        ResultadoConsulta.Modalidad === 'MigracionDown')){ 
 
        var html = '<b>Cliente con incremento de precio en <i>'+Cliente.Mensajes[0].Mes.Destino+'</i> 2020</b>';    
        $("#modal-AjusteIncrementoPrecio").html(html);
        $("#modal-AjusteIncrementoPrecio").dialog("open");  
            
    }
    
    if(Cliente.mensaje!=undefined ){ 
        for(var i = 0; i < Cliente.mensaje.length; ++i){
            if (Cliente.mensaje[i].Tipo=='A'){
                var html = "<span class='error'><br>";              
                //html += Cliente.mensaje[i].Mensaje;
                html += "APAGADO DE COBRE. !Prioridad Cambio de Tecnología! Debemos recordarle al cliente que el beneficio del cambio de tecnología es para el mejor funcionamiento de su internet y para evitar inconvenientes en su servicio con el apagado de cobre programado para el 15/10/2019.</span>";               
                $("#modal-ApagadoCobre").html(html);
                //$("#modal-ApagadoCobre").dialog("open");  
            }else if(Cliente.mensaje[i].Tipo=='B'){
                var html = "<span class='error'><br>";              
                //html += Cliente.mensaje[i].Mensaje;
                html += "APAGADO DE COBRE. !Prioridad Cambio de Tecnología! Debemos recordarle al cliente que el beneficio del cambio de tecnología es para que no pierda comunicación con el apagado de cobre programado para el 15/10/2019. PS: 22789, 22791, 22792.</span>";                             
                $("#modal-ApagadoCobre").html(html);
                //$("#modal-ApagadoCobre").dialog("open");  
            }
        }       
        
    }else{    }     
    
    
   if( Cliente.Internet.Cobertura.RadioEnlace == 1)
   {
       var html = "<span class='error'><br>";
       if(ResultadoConsulta.Retenciones == "Ninguno")
       {
           html += "NO MIGRAR";
       }else{
           html += "NO MIGRAR";
       }
       html += "<br>El cliente se encuentra en zona de Radio-Enlace bajo supervision de Osiptel</span>";
       $("#modal-NoRetener").html(html);
       $("#modal-NoRetener").dialog("open");
   }
   else if( Cliente.ComportamientoPagoCovergente == "1" && !Cliente.EsEmpleado) // emple 2
   {
        var html = "<span class='error'><br>";
        html += "NO VENDER ALTA DE COMPONENTE, MIGRACIÓN UP O SVA";
        html += "<br>El cliente no cumple con los requisitos de la evaluación crediticia</span>";
        $("#modal-ClienteRiesgoso").html(html);
        $("#modal-ClienteRiesgoso").dialog("open");
   }
   
   
   if( Cliente.Linea.IPFija === 1)
   {
       var html = "<span class='error'><br>";
       html += "MIGRACION DE LINEA";
       html += "<br>El cliente perdera su IP Fija en caso de realizar una migracion de Paquete HFC.</span>";
       $("#modal-IPFija").html(html);
       $("#modal-IPFija").dialog("open");
   }

   if(ResultadoConsulta.Retenciones === 'MigracionDown' || ResultadoConsulta.Retenciones === 'Baja' ||
   ResultadoConsulta.Retenciones === 'Averias' || ResultadoConsulta.Retenciones === 'Online' ||
   ResultadoConsulta.Retenciones === 'Ninguno'
   ){
        if(Cliente.CompetenciaFTTH==1 && Cliente.Internet.Cobertura.FTTH == 0) {
            var html = "<span class='error' style='font-weight: lighter;'>";
            html += "<br><b>Asesor:</b> Informar al cliente que la <b>tecnología de Fibra Óptica llegará a su distrito</b> (entre marzo y abril) y que tendrá la posibilidad (si lo desea) de poder acceder a esta tecnología, con lo cual gozará de <b>una mayor velocidad manteniendo los mismos beneficios exclusivos de Movistar.</b> ¡No olvides de utilizar tus herramientas de retención!.</span>";
            $("#modal-CompetenciaFTTH").html(html);
            $("#modal-CompetenciaFTTH").dialog("open"); 
        }
        
        if(Cliente.Internet.Cobertura.HFC > 0 && !Cliente.ComportamientoPagoCovergente) {
            if((Cliente.ComportamientoPago === 'PUNTUAL' || Cliente.ComportamientoPago === 'OLVIDADIZO')
            && Cliente.Internet.Tecnologia === 'ADSL' && !Cliente.TrobasSaturadas){
                var html = "<span class='error'><br>";
                html += " ¡Prioridad Cambio de Tecnología!";
                html += "<br>Recuérdale al cliente que el beneficio del cambio de tecnología es para el mejor funcionamiento de su internet.</span>";
                $("#modal-CambioTecnologia").html(html);
                $("#modal-CambioTecnologia").dialog("open");       
            } else if(Cliente.Desposicionado){
                var html = "<span class='error'><br>";
                html += "¡Prioridad actualización de paquete!";
                html += "<br>Informar al cliente que tendrá mejores beneficios en velocidad por un menor o igual precio.</span>";
                $("#modal-Desposicionado").html(html);
                $("#modal-Desposicionado").dialog("open");              
            }
        }
        // Para Descuento Apagado permanente
        if(Cliente.DescuentoApagadoPermanente==1) {
            var html = "<span class='error' style='font-weight: lighter;'>";
            html += "<br>En esta oportunindad permítame informarle que no corresponde aplicarle el beneficio y/o descuento que se le estuvo brindando, es por ello, que se procedió a retirarlo el <b>01 de Junio.</b>"
            html +="<br>Su paquete contratado es <b>"+ Cliente.Paquete.Nombre +"</b> que tiene un importe de <b>S/."+ Cliente.RentaTotal +"</b> Cabe precisar que los descuentos en general de acuerdo a la normativa vigente tienen una duración máxima de seis meses.<br>" 
            html +="<br><b>Si el cliente manifiesta que no está conforme con lo indicado, se informará lo siguiente:</b>" 
            html +="<br>Entiendo su malestar, en todo caso usted puede migrar a un paquete más cómodo con la finalidad de seguir disfrutando de los beneficios del contenido de movistar"
            html +=".</span>";
            $("#modal-DescuentoPermanente").html(html);
            $("#modal-DescuentoPermanente").dialog("open"); 
        }

        if(Cliente.ErrorIncremento==1) {
            var html = "<span class='errorIncremento' style='font-weight: lighter;'>";
                      
            html +="Si el motivo de la llamada del cliente es para consultar sobre el error del monto facturado en su recibo, indicar lo siguiente:" ;
            html +="<br><br><b>“Identificamos un error en el recibo de su servicio Movistar Hogar de Junio por S/ "+ Cliente.MontoAjustado+" que ya fue descontado para que cancele el importe correcto. Reciba nuestras sinceras disculpas”.</b>" ;
            html +="<br><br>Si la intención de la llamada es por otro motivo, obviar el speech y continuar con la gestión de retención.";
            html +=".</span>";
            $("#modal-error-incremento").html(html);
            $("#modal-error-incremento").dialog("open"); 
        }
        
        if(Cliente.ErrorReconexion==1) {
            var html = "<span class='errorIncremento' style='font-weight: lighter;'>";
                    
            html +="Si el motivo de la llamada del cliente es para consultar sobre el error del monto facturado en su recibo, indicar lo siguiente:" ;
            html +="<br><br><b>“Identificamos un error en el recibo de su servicio Movistar Hogar de Mayo por S/10.27 que ya fue descontado para que canceles el importe correcto. Disculpe las molestias”</b>" ;
            html +="<br><br>Si la intención de la llamada es por otro motivo, obviar el speech y continuar con la gestión de retención.";
            html +=".</span>";
            $("#modal-error-reconexion").html(html);
            $("#modal-error-reconexion").dialog("open"); 
        }
        
        
   }
   
    if(ResultadoConsulta.Modalidad === 'Averia' 
            && Cliente.Inestable
            && Cliente.Internet.Tecnologia === 'ADSL'
            && Cliente.Internet.Cobertura.HFC > 0
            && !Cliente.TrobasSaturadas
            ){
            var html = "<span class='error'><br>";
            html += "¡Prioridad cambio de tecnología!";
            html += "<br>Recuérdale al cliente que el beneficio del cambio de tecnología es para el mejor funcionamiento de su internet.</span>";
            $("#modal-CambioTecnologia").html(html);
            $("#modal-CambioTecnologia").dialog("open");          
    }
   
   
   
    DatosCliente.MostrarVelocidadMaxima(Cliente.Internet,Cliente.TrobasSaturadas,ResultadoConsulta.Modalidad);
   DatosCliente.MostrarDescuentoEmpleado(Cliente);
   DatosCliente.MostrarMensajes(ResultadoConsulta.Cliente.Mensajes,Cliente);
};

DatosCliente.MostrarVelocidadMaxima = function (Internet,TrobasSaturadas,Modalidad)
{
    var texto = "";
    if( (Modalidad=='MigracionDown' || Modalidad=='Baja') &&  
        Internet.Tecnologia === 'ADSL' && (Internet.Cobertura.FTTH || Internet.Cobertura.HFC) && TrobasSaturadas ){
        texto+='';
    }else if (Internet.Cobertura.FTTH){
        texto += 'FTTH: 200 Mbps';
    } else if (Internet.Cobertura.HFC){
        texto += 'HFC: 200 Mbps';
    }
    
    if(Internet.Tecnologia === 'ADSL')
    {
        texto += '<br> ADSL:'+ Math.round(Internet.Cobertura.VelocidadMaxima / 1000, 1)+ ' Mbps';
    }
    

    $('#Cliente_VelocidadMaxima').html(texto);
};

DatosCliente.MostrarDescuentoEmpleado = function(cliente)
{
   if(cliente.EsEmpleado != 0 ){
      $("#cliente_DescuentoEmpleado").html("<span class='mensajeResaltado'>Cliente con Paquete Embajador</span>");
      $('#cliente_DescuentoEmpleado').show();
   }
   
   
   if(cliente.EstadoTecnico === 'Critico' ){
      $("#cliente_Tecnica").html("<span class='mensajeResaltado'>Migracion Prioritaria</span>");
      $('#cliente_Tecnica').show();
   }
   
};

DatosCliente.limpiar_cliente = function() 
{
   DatosCliente.limpiar_cliente_datos();
   $("#NuevoContenido").html("");
	$("#MensajeTrioSinDuoInternetTv").html("");
   
   window.cliente = null;
   window.oferta_marcada = null;
   
   Formulario.ObtenerModoPorDefecto();
   
   $('#modosDeOperacion').buttonset();
   $('#modosDeOferta').hide();
};

DatosCliente.limpiar_cliente_datos = function()
{
    $('#parrilla').html('');

    $('.cliente_dato').text('');

    $('#retenciones_migracion_down').html('');

    $('#oferta').html('');

    $('#MigracionMasiva').html('');

    $('#modal-descuento-retenciones').html("");
    $('#modal-herramienta-retenciones').html("");
    $("#modal-NoRetener").html("");
    $('#retenciones_premiumTV').html("");
};

DatosCliente.limpiarDatosPopupDescuentoRetenciones = function()
{
   $('#modal-descuento-retenciones').html("");
   $('#modal-herramienta-retenciones').html("");
};

