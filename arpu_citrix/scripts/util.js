var Util = {};

Util.registroUsoAsesor = function(PS, accion){
    var params = {};
    params.telefono = window.cliente_datos.Cliente.Telefono;
    params.ps = PS;
    params.retenciones = window.cliente_datos.Retenciones;
    params.accion = accion;

    $.ajax({
         data: params,
         url: 'Http/RegistroConsulta.php',
         method: 'GET'
     });
};

getConsultaUltActualizacion = function()
{
  $.ajax({
       async     : true,
       url       : 'Http/ConsultaUltActualizacion.php',
       data      : {},
       dataType  : "json",
       method    : 'GET',
       beforeSend: function(){$("#dv_dtfechaultactualizacion").html('<img src="images/loading.gif">');},
       success   : function(Response){
        $("#dv_dtfechaultactualizacion").html(Response.PLANTA_CALCULADORA);
        $("#dv_dtfechaultactualizacion_averias").html(Response.INFO_AVERIAS);

        //console.log(Response);
      }
   });
}

$(document).ready(function(){
  getConsultaUltActualizacion();
});

