/* global DatosCliente */

var DescuentosRetenciones = {};

DescuentosRetenciones.Mostrar = function(){
    var Cliente = window.ResultadoConsulta.Cliente;
    
    if (Cliente.Componentes['Linea'].Presente || Cliente.Componentes['Internet'].Presente )
    {
        DescuentosRetenciones.Dibujar(Cliente);
    } else
    {
        alert('No hay descuentos validos para este caso');
    }
};




DescuentosRetenciones.Dibujar = function(Cliente){
    Util.MostrarModal("Descuentos Retenciones",
            DescuentosRetenciones.TablaDescuentosMantiene(Cliente.Herramienta.Gratuidad) +
            "<table width='100%' align='center'>"+
            "<tr><font color='red'><b>Bajan el ARPU:</b></font> <br></br> <b>1. Descuentos Comerciales:</b> <br></br></tr>"+
            "<tr width='100%' align='center'>"+
            HerramientasRetenciones.TablaTenenciaDescuentos(Cliente)+
            DescuentosRetenciones.TablaDescuentos(Cliente) +
            HerramientasRetenciones.TablaDescuentosBloquesTV(Cliente)+
            "</tr></table>"    +
            "<b>2. Baja de suplementario de TV</b>");
};


DescuentosRetenciones.TablaDescuentos = function(Cliente){
    
    var html = "<td>" ;
    var Descuentos = Cliente.Herramienta.DescuentoRetenciones;

    if(Descuentos.length === 0 ||  Cliente.Descuento.ComercialesCantidad > 1){
       html += "Este cliente no accede a descuentos Comerciales.";
    }else{
       html += "<center><table cellpadding='3' cellspacing='3'>" +  
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Descuento</b></td>" +
             "<td class='cabeceraParrilla'><b>P/S</b></td>" +
             "<td class='cabeceraParrilla'><b>Uso</b></td>" +
             "</tr>";
        for (var x = 0; x < Descuentos.length; x++) {
           html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + Descuentos[x].Nombre + "</td>" +
             "<td class='cajaTexto'>" + Descuentos[x].Ps + "</td>" +
             "<td class='cajaTexto'>" + Descuentos[x].Descripcion + "</td>" +
             "</tr>";
        }
        html += "</table></center>";
    }
    html += "</td>" ;
    return html;
};
  
DescuentosRetenciones.TablaDescuentosMantiene = function(Gratuidad){
    
    var i = 1;
    
    var html = "<font color='red'><b>Mantiene el ARPU:</b></font> <br></br> <b>" + i +". Argumentario:</b>" ;
        html += "<br><font style='background-color:yellow;'>   •    Resaltar los atributos del paquete actual que tiene el cliente.</font>";
        i+=1;
        
        
        for (var x = 0; x < Gratuidad.length; x++) {
           html +="<br></br> <b>" + i +". " +  Gratuidad[x] + "</b>";
           html +="<br><font style='background-color:yellow;'>   •    Cuando se haga uso de un Free view, no se podrá ofrecer un descuento.</font>";
           i+=1;
        }
        html +="<br></br> <b>" + i +". Baja de suplementario (Multidestino, Macfee, mantenimiento, etc).</b>";
        html += "<br></br>";
    return html;
};  
  
var HerramientasRetenciones = {};

HerramientasRetenciones.Mostrar = function(){
    var Cliente = window.ResultadoConsulta.Cliente;
    
    if (Cliente.Componentes['Linea'].Presente || Cliente.Componentes['Internet'].Presente)
    {
        HerramientasRetenciones.Dibujar(Cliente);
    } else
    {
        alert('No hay herramientas validas para este caso');
    }
};


HerramientasRetenciones.Dibujar = function(Cliente){
    
    Util.MostrarModal("Herramientas Retenciones",
            HerramientasRetenciones.TablaCompensaciones(Cliente.Herramienta.Compensacion)+
            "<table width='100%' align='center'><tr width='100%' align='center'>"+
            HerramientasRetenciones.TablaTenenciaDescuentos(Cliente)+
            HerramientasRetenciones.TablaDescuentosComerciales(Cliente)+
            HerramientasRetenciones.TablaDescuentosBloquesTV(Cliente)+
            "</tr></table><tr>"+
            HerramientasRetenciones.TablaEquipamiento(Cliente.Herramienta.DecodificadorAdicional)+
            HerramientasRetenciones.TablaSolucion(Cliente.Herramienta.Solucion)+
            "</table>");
};
HerramientasRetenciones.TablaTenenciaDescuentos = function(Cliente){

       var  html =
             "<td></br><table cellpadding='3' cellspacing='3' >" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Tipo de Descuentos</b></td>" +
             "<td class='cabeceraParrilla'><b>Cantidad</b></td>" +
             "</tr>";
            html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'> Comerciales</td>" +
             "<td class='cajaTexto'> " + Cliente.Descuento.ComercialesCantidad + "</td>" +
             "</tr>";
            html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'> Bloques TV</td>" +
             "<td class='cajaTexto'> " + Cliente.Descuento.BloquesTVCantidad + "</td>" +
             "</tr>";

        html +="</table></br></td>";
        return html;
    
};

HerramientasRetenciones.TablaDescuentosComerciales = function(Cliente){
    if(Cliente.Herramienta.DescuentoRetenciones.length === 0 || Cliente.Descuento.ComercialesCantidad > 1){
       return "<td><b>Este cliente no accede a descuentos comerciales.</b></td>";
    }else{
       var html =
             "<td></br><table cellpadding='3' cellspacing='3' >" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Descuentos Comerciales</b></td>" +
             "<td class='cabeceraParrilla'><b>P/S</b></td>" +
             "</tr>";
        for (var x = 0; x < Cliente.Herramienta.DescuentoRetenciones.length; x++) {
           html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + Cliente.Herramienta.DescuentoRetenciones[x].Nombre + "</td>" +
             "<td class='cajaTexto'>" + Cliente.Herramienta.DescuentoRetenciones[x].Ps + "</td>" +
             "</tr>";
        }
        html +="</table></br></td>";
        return html;
    }
};

HerramientasRetenciones.TablaDescuentosBloquesTV = function(Cliente){
    if(Cliente.Cable.Premium.Nombre !== '' && Cliente.Descuento.BloquesTVCantidad < 2){        
       var  html =
             "<td></br><table cellpadding='3' cellspacing='3' >" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Descuento Bloque TV</b></td>" +
             "<td class='cabeceraParrilla'><b>Cantidad</b></td>" +
             "</tr>";
            html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'> Descuento de 10x6 meses</td>" +
             "<td class='cajaTexto'> 22438</td>" +
             "</tr>";
            html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'> Descuento de 20x2 meses</td>" +
             "<td class='cajaTexto'> 22277</td>" +
             "</tr>";
        html +="</table></br></td>";
        return html;
    }
    else
        return "<td><b>Este cliente no accede a descuentos de Bloques de TV.</b></td>";
};

HerramientasRetenciones.TablaGratuidades = function(Gratuidad,BonoMovil){
    if(Gratuidad.length === 0 && BonoMovil.length === 0){
       return "";
    }else{
       var html = "<td><b>Gratuidades:</b></br>";
        for (var x = 0; x < Gratuidad.length; x++) {
           html +="•    " +  Gratuidad[x] + "</br>";
        }
        if( BonoMovil.length > 0){
        html +=
             "<table cellpadding='3' cellspacing='3'>" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Bono Movil</b></td>" +
             "</tr>";
        for (var x = 0; x < BonoMovil.length; x++) {
           html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + BonoMovil[x] + "</td>" +
             "</tr>";
        }
        html +="</table></br>";}
        
        
        html += "</td>";
        
        return html;
    }
};

HerramientasRetenciones.TablaCompensaciones = function(Compensacion){
    if(Compensacion.length === 0){
       return "";
    }else{
       var    html = "<table><tr><td><b>Solucion Tecnica Priorizada <font color='red'>[Solo Averias]</font>:<br></br>Speech:</b></br></td></tr>";
              html +="<tr align='center'><td>”Veo en el sistema que ha tenido inconvenientes técnicos. Permítame consultarle si este es el motivo de su decisión?” <b>(Cliente confirma)</b></br>";
              html += "<font color='red'>Lamento el(los) inconveniente(s) que ha tenido, permítame ayudarlo.</br></font>";
              html += "<font color='red'><b>Queremos compensar</b> </font>los inconvenientes ocasionados con un descuento en su siguiente recibo por un monto de: </br></td></tr>" ;
              html += "<tr><td><table cellpadding='3' cellspacing='3' align = 'center'>" +
                "<tr class='ProductoSVA'>" +
                "<td class='cabeceraParrilla'><b>Descuento</b></td>" +
                "<td class='cabeceraParrilla'><b>P/S</b></td>" +
                "</tr>";
        for (var x = 0; x < Compensacion.length; x++) {
           html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + Compensacion[x].NOMBRE + "</td>" +
             "<td class='cajaTexto'>" + Compensacion[x].PS + "</td>" +
             "</tr>";
        }
        html += "</table></td></tr><tr><td><b>Así mismo permítame comunicarlo  con el área técnica a fin que puedan coordinar con Ud. Una visita y así solucionar definitivamente su problema.</br>" ;
        html += "<font style='background-color:yellow;'>Una vez utilizado la compensación no aplicar descuentos comerciales por incompatibilidad entre ambas.</b></font><br></br></td></tr></table>";
        return html;
    }
};

HerramientasRetenciones.TablaEquipamiento = function(DecodificadorAdicional){
    if(DecodificadorAdicional === null){
       return "";
    }else{
       var html =
             "<tr><td><b>Equipamiento:</b><table cellpadding='3' cellspacing='3'  align='center'>" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>PS</b></td>" +
             "<td class='cabeceraParrilla'><b>Nombre</b></td>" +
             "<td class='cabeceraParrilla'><b>Operacion Comercial</b></td>" +
             "<td class='cabeceraParrilla'><b>Caracteristica</b></td>" +
             "</tr>" +
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + DecodificadorAdicional.Ps.Ps + "</td>" +
             "<td class='cajaTexto'>" + DecodificadorAdicional.Ps.Nombre + "</td>" +
             "<td class='cajaTexto'>" + DecodificadorAdicional.TipoOperacionComercial + "</td>" +
             "<td class='cajaTexto'>" + DecodificadorAdicional.Caracteristica + "</td>" +
             "</tr>";
        
        html += "</table></br></td></tr>";
        return html;
    }
};

HerramientasRetenciones.TablaSolucion = function(Solucion){
    if(Solucion.length === 0){
       return "";
    }else{
       var html = "<tr><td><b>Soluciones:</b></br>";
        for (var x = 0; x < Solucion.length; x++) {
           html +="•    " +  Solucion[x] + "</br>";
        }
        html += "</td></tr>";
        
        return html;
    }
};
