
/* global DatosCliente */

var Freeview = {};

Freeview.Consultar = function(){

    var html = "";

        html += '<div style="font-size:15px"><b>Consideraciones:</b></div><br>';
        html += '<div style="font-size:15px">• Promoción sujeta a facilidades técnicas.</div><br>';
        html += '<div style="font-size:15px">• Aplica sólo para clientes que cuenten con tecnología digital (no para clientes analógicos).</div><br>';
        html += '<div style="font-size:15px">• Para aplicar al beneficio de Bloque HD es necesario contar con un decodificador HD.</div><br>';
        html += '<div style="font-size:15px">• El cliente que se registre en la promoción, no debe contar con alguna apertura de canales gratuitos al momento del registro.</div><br>';
   
    Freeview.MostrarModal("Freeview",html);
};

Freeview.MostrarModal = function(titulo,mensaje){
    Freeview.CrearDialogo("#modal",500,500,150);
    $("#modal").html(mensaje);
    $('#ui-id-1').text(titulo);
    $("#modal").dialog("open");
};

Freeview.CrearDialogo = function(Dialogo){
    $(Dialogo).dialog({
       autoOpen: false, resizable: false, draggable: false, modal: true,
       width: 700,
       maxWidth: 700,
       maxHeight: 500,
       minHeight: 500
    });
};
