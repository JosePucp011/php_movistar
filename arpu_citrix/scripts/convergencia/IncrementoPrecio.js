/* global DatosCliente */

var IncrementoPrecio = {};
IncrementoPrecio.MostrarIncrementoPrecio = function (mensaje)
{
    var Cliente = window.ResultadoConsulta.Cliente;
    var html = "";
    html += '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;"> Incremento de Precio - ' + mensaje.Fecha  +'</h1>';


    html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 15px; text-align: center;" >';
    html += '<tr>';
    html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Hasta '+ mensaje.Mes.Origen +'</th>';
    html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Desde '+ mensaje.Mes.Destino +'</th>';
    html += '</tr>';
    html += '<tr style="font-size: 20px;" >';
    html += '<td><b>S/.'+ mensaje.Renta.Origen +'</b></td>';
    html += '<td><b>S/.'+ mensaje.Renta.Destino +'</b></td>';
    html += '</tr>';
    html += '</table>';

    html += '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;"> Beneficios </h1><br>';

    html += '<table align="center" bottom="middle" style="text-align: center; ; width:100%"  ><tr>';

    var width = '30%';

    if(mensaje.Velocidad !== undefined)
    {
        html += '<td width='+ width +'; valign="top";>';

            html += '<table align="center">';
                html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:100%;">Mas Velocidad</th></tr>';
                html += '<tr><td>';
                    html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 12px; text-align: center;" >';
                    html += '<tr>';
                    html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Antes Tenia</th>';
                    html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Ahora Tiene</th>';
                    html += '</tr>';
                    html += '<tr style="font-size: 20px;" >';
                    html += '<td>'+ mensaje.Velocidad.Origen +' Mbps</td>';
                    html += '<td><b>'+ mensaje.Velocidad.Destino +' Mbps</b></td>';
                    html += '</tr>';
                    html += '</table>';
                html += '</td></tr>';
            html += '</table>';


        html += '</td>';
    }
    if(mensaje.Canales !== undefined)
    {
        var Canales = '<b>';

        if(Cliente.Cable.Categoria === 'CATV')
        {
            if(Cliente.Cable.CantidadDecosHD > 0 )
            {
                Canales += 'SD 01 Y HD 701';
            }
            else
            {
                Canales += 'SD 01';
            }
        }
        if(Cliente.Cable.Categoria === 'DTH')
        {
            if(Cliente.Cable.CantidadDecosHD > 0 )
            {
                Canales += 'SD 101 Y HD 801';
            }
            else
            {
                Canales += 'SD 101';
            }
        }

        Canales += '</b>';

        html += '<td width='+ width +'; valign="top";>';

            html += '<table>';
                html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:80%;">Mas Contenido</th></tr>';
                html += '<tr><td>';
                    html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 15px; text-align: center;" >';
                    html += '<tr>';
                    html += '<td><img src="images/MSeries.png" width=120px;></td>';
                    html += '</tr>';
                    html += '<tr>';
                    html += '<td style="font-size: 12px;">•100% series en español.<br>•12 series producidas por Movistar.<br>•Disfrútalo en el ' + Canales + '</td>';
                    html += '</tr>';
                    html += '</table>';
                html += '</td></tr>';
            html += '</table>';


        html += '</td>';
    }
    if(mensaje.MPlay !== undefined)
    {
        html += '<td width='+ width +'; valign="top";>';

            html += '<table>';
                html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:80%;">Movistar Play</th></tr>';
                html += '<tr><td>';
                    html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 15px; text-align: center;" >';
                    html += '<tr>';
                    html += '<td><img src="images/MPlay.png" width=120px;></td>';
                    html += '</tr>';
                    html += '<tr>';
                    html += '<td style="font-size: 12px;">• Canales en vivo 24 horas<br>•Los Partidos de la Copa Movistar<br>•Alquiler de Peliculas</td>';
                    html += '</tr>';
                    html += '</table>';
                html += '</td></tr>';
            html += '</table>';
        html += '</td>';
    }
    html += '</tr></table>';
    html += "<div class='boton' onclick='javascript:IncrementoPrecio.Argumentario()' > Argumentario </div>";
                       
    Util.MostrarModal("Incremento de precio",html);
};


IncrementoPrecio.Argumentario = function ()
{
      Util.MostrarModal("Incremento de precio",
   "<h1>Argumentarios</h1>"
   +"<div>El incremento de precio se debe a ajustes en nuestras tarifas"
   +" como parte de la revisión de nuestra oferta comercial y estructura de costos."
   +" Entre estos motivos se encuentran los siguientes:</div>"
   +"<table style='border-spacing: 10px;'>"
   +"<tr><td class='bullet verde'>Inflacion</td><td> La inflación de 2016 a la actualidad fue de 4.6% lo cual incrementa"
   +" las tarifas en general de todos los servicios del país. </td></tr>"
   +"<tr><td class='bullet azul'>Inversion</td><td> Inversiones para tener un mejor internet.</td></tr>"
   +"<tr><td class='bullet amarillo'>Contenido</td><td> Mayor contenido de video (Movistar Play y más canales)</td></tr>"
   +"</table>"
   );
   
};