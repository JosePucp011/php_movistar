/* global Documento */

var EstadoPedido = {};

Documento.SiguienteFormulario = EstadoPedido;

EstadoPedido.Limpiar = function(){
   $('.datoEstadoPedido').val('');
   $('#Estado_Pedido').text('');
};


EstadoPedido.Mostrar = function(data){
    var Consulta = data.respuestaConsulta;
    if(Consulta.Pedidos !== undefined && Consulta.Pedidos.length > 0){
        var Pedido = Consulta.Pedidos[0];
        
        $('#pedido_OA_ORDER_ID').html(Pedido.OA_ORDER_ID);
        $('#pedido_O_APPLICATION_DATE').html(Pedido.O_APPLICATION_DATE);
        $('#pedido_ESTADO_ORDEN').html(Pedido.ESTADO_ORDEN);
        $('#pedido_TELEFONO_MOVIL').html(Pedido.TELEFONO_MOVIL);
        $('#pedido_FECHA_ENTREGA_PACTADA').val(Pedido.FECHA_ENTREGA_PACTADA);
        $('#pedido_FECHA_ENTREGA_REAL').val(Pedido.FECHA_ENTREGA_REAL);
        $('#pedido_MOTIVO_DE_RECHAZO').val(Pedido.MOTIVO_DE_RECHAZO);
        $('#pedido_OA_ORDER_ID_1').html(Pedido.OA_ORDER_ID_1);
        $('#pedido_O_APPLICATION_DATE_1').html(Pedido.O_APPLICATION_DATE_1);
        $('#pedido_ESTADO_ORDEN_1').html(Pedido.ESTADO_ORDEN_1);
        $('#pedido_TELEFONO_MOVIL_1').html(Pedido.TELEFONO_MOVIL_1);
        $('#pedido_FECHA_ENTREGA_PACTADA_1').val(Pedido.FECHA_ENTREGA_PACTADA_1);
        $('#pedido_FECHA_ENTREGA_REAL_1').val(Pedido.FECHA_ENTREGA_REAL_1);
        $('#pedido_MOTIVO_DE_RECHAZO_1').val(Pedido.MOTIVO_DE_RECHAZO_1);
        $('#pedido_CODIGO_PEDIDO_BACK').html(Pedido.CODIGO_PEDIDO_BACK);
        $('#pedido_PET_ITE_ALT_SIS').html(Pedido.PET_ITE_ALT_SIS);
        $('#pedido_TELEFONO_FIJO').html(Pedido.TELEFONO_FIJO);
        $('#pedido_Documento').val(Pedido.Documento);
        $('#pedido_ESTADO_DE_GESTION_1_BACK').html(Pedido.ESTADO_DE_GESTION_1_BACK);
        $('#pedido_ESTADO_DE_GESTION_CAIDAS_BACK').val(Pedido.ESTADO_DE_GESTION_CAIDAS_BACK);
        $('#pedido_FECHA_GENERO_OOSS').val(Pedido.FECHA_GENERO_OOSS);
        $('#pedido_FECHA_INSTALACION').val(Pedido.FECHA_INSTALACION);
        $('#pedido_FECHA_GESTION_BACK').html(Pedido.FECHA_GESTION_BACK);
        $('#pedido_FECHA_GESTION_MOVIL').html(Pedido.FECHA_GESTION_MOVIL);
        $('#pedido_FECHA_GESTION_MOVIL_1').html(Pedido.FECHA_GESTION_MOVIL_1);
        $('#pedido_MOTIVO_FIJA').html(Pedido.MOTIVO_FIJA);
        $('#pedido_MOTIVO_MOVIL').html(Pedido.MOTIVO_MOVIL);
        $('#pedido_MOTIVO_MOVIL_1').html(Pedido.MOTIVO_MOVIL_1);
        $('#pedido_ACCION_FIJA').html(Pedido.ACCION_FIJA);
        $('#pedido_ACCION_MOVIL').html(Pedido.ACCION_MOVIL);
        $('#pedido_ACCION_MOVIL1').html(Pedido.ACCION_MOVIL1);
        $('#pedido_CANCELACION_ACCION').val(Pedido.ACCIONTORRE);
        $('#pedido_CANCELACION_MONTO').val(Pedido.PS_DESCUENTO);
        $('#pedido_CANCELACION_APLICACION').val(Pedido.APLICACION_DESCUENTO);
        $('#pedido_CANCELACION_MES').val(Pedido.MES_APLICA_DESCUENTO);

        $('#pedido_CICLO_FIJA').html(Pedido.CICLO_FIJA);
        $('#pedido_CICLO_MOVIL').html(Pedido.CICLO_MOVIL_1);
        $('#pedido_CICLO_MOVIL_1').html(Pedido.CICLO_MOVIL_2);

        $('#fieldset_PEDIDO').css("display","block");
        $('#fieldset_CANCELACION').css("display","block");
        $('#fieldset_Mensaje').css("display","block"); // ye 4
    } else {
        $('#pedido_OA_ORDER_ID').html('');
        $('#pedido_O_APPLICATION_DATE').html('');
        $('#pedido_ESTADO_ORDEN').html('');
        $('#pedido_TELEFONO_MOVIL').html('');
        $('#pedido_FECHA_ENTREGA_PACTADA').val('');
        $('#pedido_FECHA_ENTREGA_REAL').val('');
        $('#pedido_MOTIVO_DE_RECHAZO').val('');
        $('#pedido_OA_ORDER_ID_1').html('');
        $('#pedido_O_APPLICATION_DATE_1').html('');
        $('#pedido_ESTADO_ORDEN_1').html('');
        $('#pedido_TELEFONO_MOVIL_1').html('');
        $('#pedido_FECHA_ENTREGA_PACTADA_1').val('');
        $('#pedido_FECHA_ENTREGA_REAL_1').val('');
        $('#pedido_MOTIVO_DE_RECHAZO_1').val('');
        $('#pedido_CODIGO_PEDIDO_BACK').html('');
        $('#pedido_PET_ITE_ALT_SIS').html('');
        $('#pedido_TELEFONO_FIJO').html('');
        $('#pedido_Documento').val('');
        $('#pedido_ESTADO_DE_GESTION_1_BACK').html('');
        $('#pedido_ESTADO_DE_GESTION_CAIDAS_BACK').val('');
        $('#pedido_FECHA_GENERO_OOSS').val('');
        $('#pedido_FECHA_INSTALACION').val('');
        $('#pedido_FECHA_GESTION_BACK').html('');
        $('#pedido_FECHA_GESTION_MOVIL').html('');
        $('#pedido_FECHA_GESTION_MOVIL_1').html('');
        $('#pedido_MOTIVO_FIJA').html('');
        $('#pedido_MOTIVO_MOVIL').html('');
        $('#pedido_MOTIVO_MOVIL_1').html('');
        $('#pedido_ACCION_FIJA').html('');
        $('#pedido_ACCION_MOVIL').html('');
        $('#pedido_ACCION_MOVIL1').html('');
        $('#pedido_CANCELACION_ACCION').val('');
        $('#pedido_CANCELACION_MONTO').val('');
        $('#pedido_CANCELACION_APLICACION').val('');
        $('#pedido_CANCELACION_MES').val('');

        $('#pedido_CICLO_FIJA').html('');
        $('#pedido_CICLO_MOVIL').html('');
        $('#pedido_CICLO_MOVIL_1').html('');

        $('#fieldset_PEDIDO').css("display","none");
        $('#fieldset_CANCELACION').css("display","none");
        $('#fieldset_Mensaje').css("display","none"); // ye 5
        $('#Estado_Pedido').text('Cliente sin pedidos');
    }

    //Notificaciones
    /*var Notificaciones = Consulta.Notificaciones;
    var cantNotificaciones=Notificaciones.length;

    if(cantNotificaciones==0) {
        $('#fieldset_NOTIFICACION').css("display","none");
        $('#pedido_NOTIFICACION').html("");
    } else {
        var notificacionesMostrar="";

        for (var index = 0; index < cantNotificaciones; index++) {
            var notificacionesMostrar = Notificaciones[index].Mensaje;
            
            if(index != (cantNotificaciones-1)) {
                notificacionesMostrar+="<br /><br />";
            }
        }

        $('#pedido_NOTIFICACION').html(notificacionesMostrar);
        $('#fieldset_NOTIFICACION').css("display","block");
    }*/

    //Netflix
    var Notificaciones = Consulta.Notificaciones;
    var cantNotificaciones=Notificaciones.length;

    if(cantNotificaciones == 0) {
        $('#fieldset_NETFLIX').css("display","none");
    } else {
        var netflixMostrar="";
        var cantNetflix = 0;

        for (var index = 0; index < cantNotificaciones; index++) {
            if(Notificaciones[index].TipoNotificacion_pmtn.toUpperCase() != 'NETFLIX') {
                continue;
            }
            var facturacionMostrar = 'Recibo Movistar';
            if(Notificaciones[index].Billing_pmtn.toUpperCase() == 'NO') {
                facturacionMostrar = 'Tarjeta crédito/débito';
            }

            var netflixMostrarIndiv = '<tr>';
            netflixMostrarIndiv += '<td><h9 style="margin-left: 10px;">Teléfono</h9><input type="text" id="netflix_TELEFONO' + index + '" value="' + Notificaciones[index].telefono + '" style="margin-right: 10px;" disabled></td>';
            netflixMostrarIndiv += '<td><h9 style="margin-left: 10px;">Fecha de canje</h9><input type="text" id="netflix_FECHACANJE' + index + '" value="' + Notificaciones[index].Fecha_apertura_pmtn + '" style="margin-right: 10px;" disabled></td>';
            netflixMostrarIndiv += '<td><h9 style="margin-left: 10px;">Meses gratis</h9><input type="text" id="netflix_MESES' + index + '" value="' + Notificaciones[index].TipoVaucher_pmtn + '" style="margin-right: 10px;" disabled></td>';
            netflixMostrarIndiv += '<td><h9 style="margin-left: 10px;">Facturado en</h9><input type="text" id="netflix_FACTURACION' + index + '" value="' + facturacionMostrar + '" style="margin-right: 10px;width: 130px;" disabled></td>';
            netflixMostrarIndiv += '</tr>';

            netflixMostrar += netflixMostrarIndiv;

            cantNetflix++;
        }

        if(cantNetflix == 0) {
            $('#fieldset_NETFLIX').css("display","none");
        } else {
            netflixMostrar += '<tr><td colspan="4"><div style="margin-top: 12px;margin-left: 10px;"><b>* La cantidad de meses varía del tipo de plan afiliado a Netflix. Cliente debe revisar en netflix.com su "Cuenta".</b></div></td></tr>';

            $('#table_NETFLIX').html(netflixMostrar);
            $('#fieldset_NETFLIX').css("display","block");
        }
    }
    // Lineas provisionadas  ye 8
    var Provisionadas = Consulta.Provisionadas;
    var cantProvisionadas=Provisionadas.length;
    if(cantProvisionadas == 0) {
        $('#fieldset_Mensaje').css("display","none");
    } else {
        var ProvisionadasMostrar="";
        var cantProvis = 0;
        // 07370286
        var NumeroMoviles="";
        for (var index = 0; index < cantProvisionadas; index++) {           
            
            if(cantProvis==1){
                NumeroMoviles+=" - "+Provisionadas[index].movil+" ";
            }else{
                NumeroMoviles+=Provisionadas[index].movil;
            }
            cantProvis++;
        }

        var MensajeProvisionadas = 'Cliente con Bono 4G Ilimitado';            
        var ProvisionadasMostrarIndiv = '<tr>';
        ProvisionadasMostrarIndiv += '<td><h9 style="margin-left: 10px;">Bonos:</h9><input type="text" id="provisionadas_Mensaje' + index + '" value="'+ NumeroMoviles +" "+ MensajeProvisionadas + '" style="margin-right: 10px; width:30%" disabled></td>';
        ProvisionadasMostrarIndiv += '</tr>';
        ProvisionadasMostrar += ProvisionadasMostrarIndiv;

        if(cantProvisionadas == 0) {
            $('#fieldset_Mensaje').css("display","none");
        } else {           
            $('#table_Mensaje').html(ProvisionadasMostrar);
            $('#fieldset_Mensaje').css("display","block");
        }
    }
};
