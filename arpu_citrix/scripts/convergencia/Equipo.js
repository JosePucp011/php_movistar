/* global Util */

var Cliente = null;
var Equipo = {};

Equipo.Datos = null;
Equipo.Oferta = null;
Equipo.TipoCliente = null;
Equipo.PrecioFinanciado0 = 0;
Equipo.TipoFinanciamiento = null;

$(document).on("keyup","#cuotaInicial",function() { Identificadores.OnDatoActualizarMonto('#cuotaInicial'); });

Equipo.Mostrar = function(cliente, oferta, pos, orden, movil){
    Cliente = cliente;
        
    VariableMovil=(movil==1?cliente.Movil1.Categoria:cliente.Movil2.Categoria);    
    tipoPago=(movil==1?cliente.Scoring.Financiamiento[1]:cliente.Scoring.Financiamiento[2]);
   
    var tipoFinanciamiento;
    if(movil == 1){
        tipoFinanciamiento = $('#scoringmovil1_tipoFinanciamiento').val();
    }else{
        tipoFinanciamiento = $('#scoringmovil2_tipoFinanciamiento').val();
    }
    
    Equipo.Oferta = oferta;
    Equipo.TipoCliente = pos;
    Equipo.Orden = orden;
    Equipo.Movil = movil;
    Equipo.TipoFinanciamiento = tipoFinanciamiento;
    Util.ConsultaAjax('Http/ObtenerEquipos.php',{tipoPago : tipoPago,VariableMovil : VariableMovil },
        function(Consulta){Equipo.LlenarMarcas(Consulta,movil);}
    );
};


Equipo.LlenarEquipos = function(){
    Equipo.LimpiarResultadoConsultaCuotaInicial();

    var i = $('#marca').val();

    if(Equipo.Datos[i] !== undefined){
        var equipos = Equipo.Datos[i];

        var opciones = "";
        for(var j in equipos){
            var equipo = equipos[j];
            opciones += "<option value='"+equipo.Modelo+"'>"+equipo.NombreComercial+"</option>";
        }
        $('#modelo').html(opciones);
    }
    

};

Equipo.Consultar = function(){
    Equipo.LimpiarResultadoConsulta();

    var movil1Renta = 0;
    var movil2Renta = 0;
    var marca = $('#marca').val();
    var modelo = $('#modelo').val();
    var tipoPago = $('#tipoPago').val();
    var cuotaInicial = $('#cuotaInicial').val();
    var cuotaMes = $('#CuotaMeses').val();

    var arrModelos = Equipo.Datos[marca];
    var indice = document.getElementById("modelo").selectedIndex;
    Equipo.PrecioFinanciado0 = arrModelos[indice].PrecioFinanciado0;

    if(Cliente.Movil1.Renta !== undefined) {
        movil1Renta = Cliente.Movil1.Renta;
        if(movil1Renta == '' || movil1Renta == '-') {
            movil1Renta = 0;
        }
    }

    if(Cliente.Movil2.Renta !== undefined) {
        movil2Renta = Cliente.Movil2.Renta;
        if(movil2Renta == '' || movil2Renta == '-') {
            movil2Renta = 0;
        }
    }
    var limiteCredito = Cliente.Scoring.LimiteCrediticio;
    var movilTotalRenta = Equipo.Oferta.MovilTotal_Renta;
    var cuotaMensualMaxima = (parseFloat(movil1Renta)+parseFloat(movil2Renta)+parseFloat(limiteCredito)-parseFloat(movilTotalRenta)).toFixed(2);

    var financiamiento = window.respuesta.Cliente.Scoring.Financiamiento[Equipo.Movil];   
    var tipoCliente = Equipo.TipoCliente;
    var precioFinanciado0 = Equipo.PrecioFinanciado0;
    var modoCuatroyOnline= window.respuesta.Resultado.Retenciones;

    var rango = Equipo.Oferta.Rango;
    
    if(rango == undefined) {
        var rango = window.respuesta.Cliente.Paquete.Rango;
        var movilTotalRenta = window.respuesta.Cliente.Paquete.MovilTotal_Renta;
        var cuotaMensualMaxima = (parseFloat(movil1Renta)+parseFloat(movil2Renta)+parseFloat(limiteCredito)-parseFloat(movilTotalRenta)).toFixed(2);
    }

    Util.ConsultaAjax('Http/ConsultarOfertaEquipo.php',
    {modelo: modelo, 
        precioFinanciado0 : precioFinanciado0,
        tipoPago : tipoPago, 
        tipoCliente : tipoCliente, 
        cuotaInicial : cuotaInicial, 
        rango : rango, 
        financiamiento : financiamiento,
        cuotaMensualMaxima : cuotaMensualMaxima,
        cuotaMes    :   cuotaMes,
        TipoFinanciamiento : Equipo.TipoFinanciamiento
    }
    ,Equipo.ResultadoConsulta);
};

Equipo.ResultadoConsulta = function(Resultado){
    $('#cuotaInicial').val(Resultado.CuotaInicial);
    $('#MontoFinanciado').val(Resultado.MontoFinanciado);
    $('#CuotaMensual').val(Resultado.CuotaMensual);
    $('#PrecioEquipo').val(Resultado.PrecioEquipo);

    if(Resultado.LCInsuficiente) {
        alert('No Aplica por Límite de Crédito Insuficiente.');
    }
};

Equipo.LimpiarResultadoConsultaCuotaInicial = function() {
    $('#cuotaInicial').val('');
    Equipo.LimpiarResultadoConsulta();
};

Equipo.DefinirCuota = function() {

    var cuota = $('#CuotaMeses').val();
    
    if(cuota === "12"){
        $('#cuotaMes').val('por 12 meses');
    }else{
        $('#cuotaMes').val('por 18 meses');
    }
};

Equipo.LimpiarResultadoConsulta = function() {
    //$('#cuotaInicial').val('');
    $('#PrecioEquipo').val('');
    $('#MontoFinanciado').val('');
    $('#CuotaMensual').val('');
};

Equipo.LlenarMarcas = function(Marcas,movil){
    Equipo.Datos = Marcas;
    
    var opciones = "";
    var primeraOpcion = "";
    for(var i in Marcas){
        opciones += "<option value='"+i+"'>"+i+"</option>";
        if(primeraOpcion == "") {
            primeraOpcion = i;
        }
    }

    var equipos = Equipo.Datos[primeraOpcion];

    var opcionesModelo = "";
    for(var j in equipos){
        var equipo = equipos[j];
        opcionesModelo += "<option value='"+equipo.Modelo+"'>"+equipo.NombreComercial+"</option>";
    }
    
    var financiamiento = window.respuesta.Cliente.Scoring.Financiamiento[movil]; 
    
    var contenido =         
                    "<h1>EQUIPOS</h1><form onsubmit='return false;' style='margin:10px'>" +
                    "<form onsubmit='return false;'><tr>";
    
        contenido +="<fieldset style='margin-left: 15px;margin-right: 15px;'>" +
                    "<legend>Seleccionar Equipo</legend>" +
                    "<table>" +
                        "<tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='marca' style='margin-left: 5px;'>Marca </label>" +
                                "<select id='marca' onchange='Equipo.LlenarEquipos();'>"+opciones+"</select>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='modelo' style='margin-left: 5px;'>Modelo </label>" +
                                "<select onchange='Equipo.LimpiarResultadoConsultaCuotaInicial();' id='modelo'>" + opcionesModelo + "</select>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='tipoPago'>Tipo Pago </label>" +
                                "<select onchange='Equipo.LimpiarResultadoConsultaCuotaInicial();' id='tipoPago'>" 
                                if(financiamiento == 1){                                       
                                    contenido +="<option value='1'>Financiado</option>";
                                }
                                if(financiamiento == 2){  
                                    contenido +="<option value='2'>Financiado</option>";
                                } 
                                if(financiamiento == 4){  
                                    contenido +="<option value='4'>Financiado</option>";
                                } 
                                if(financiamiento == 5){  
                                    contenido +="<option value='5'>Financiado</option>";
                                }                                    
                                
    contenido +=            "<option value='3'>Contado</option><select>"+
                            "</td>";
        
        contenido += "<td  style='width: auto;'> <label for='CuotaMeses' style='margin-left: 5px;'>Cuotas </label>"+
                            "<select onchange='Equipo.DefinirCuota();' id='CuotaMeses'>" +
                                "<option value='12'>12</option>" +
                    "</td>" +    
                            
                            "<td  style='width: auto;'>" +
                                "<label for='cuotaInicial' style='margin-left: 5px;'>Cuota Inicial </label>" +
                                "<input onkeyup='Equipo.LimpiarResultadoConsulta();' type='text' id='cuotaInicial'>" +
                            "</td>" +
                             "<td  style='width: auto;'><input type='submit' onclick='Equipo.Consultar();' value='Consultar'></form></td>" +
                        "</tr>" +
                    "</table>" +
                "</fieldset><br>";
            
        contenido +="<fieldset style='margin-left: 15px;margin-right: 15px;'>" +
                    "<legend>Resultado</legend>" +            
                        "<table>" +
                            "<tr>" +
                                "<td  style='width: auto;'>" +
                                    "<label for='PrecioEquipo' style='margin-left: 5px;'>Precio Equipo </label>" +
                                    "<input type='text' id='PrecioEquipo' disabled style='width: 74px'>" +
                                "</td>" +
                                "<td  style='width: auto;'>" +
                                    "<label for='MontoFinanciado' style='margin-left: 5px;'>Monto Financiado </label>" +
                                    "<input type='text' id='MontoFinanciado' disabled style='width: 74px'>" +
                                "</td>" +
                                "<td  style='width: auto;'>" +
                                    "<label for='CuotaMensual' style='margin-left: 5px;'>Cuota Mensual: </label>" +
                                    "<input type='text'  id='CuotaMensual' disabled style='width: 60px'>" +
                                    "<input type='text' id='cuotaMes' disabled style='width: 90px' value='por 18 meses'>" +
                                "</td>" +
                                "<td  style='width: auto;'>" +
                                    "<input type='submit' onclick='Equipo.Guardar()' value='Guardar' >" +
                                "</td>" +
                        "</tr>" +
                    "</table>" +
                    "</fieldset><br>";         
    Util.MostrarModal("",contenido);
};

Equipo.Guardar = function(){    
    var PrecioEquipo = $('#PrecioEquipo').val();
    
    if(PrecioEquipo === '' || PrecioEquipo === 'No Aplica'){
        alert('El equipo seleccionado no es valido');
        return;
    }
    
    var equipo = $('#modelo').val();
    var cuotaMensual = $('#CuotaMensual').val();
    
    if(Equipo.Movil === '1'){
        Equipo.Oferta.Movil1.Equipo = equipo;
        Equipo.Oferta.Movil1.OperacionComercial = 'CAEQ';
        Equipo.Oferta.Movil1.Financiamiento = cuotaMensual;
    }
    if(Equipo.Movil === '2'){
        Equipo.Oferta.Movil2.Equipo = equipo;
        Equipo.Oferta.Movil2.OperacionComercial = 'CAEQ';
        Equipo.Oferta.Movil2.Financiamiento = cuotaMensual;
    }

    $("#Equipo_Texto" + Equipo.Orden + Equipo.Movil).text(equipo);

    var Movil1Financiamiento = Equipo.Oferta.Movil1.Financiamiento == undefined ? 0 : Equipo.Oferta.Movil1.Financiamiento;
    var Movil2Financiamiento = Equipo.Oferta.Movil2.Financiamiento == undefined ? 0 : Equipo.Oferta.Movil2.Financiamiento;
    
    var Financiamiento = parseFloat(Movil1Financiamiento) + parseFloat(Movil2Financiamiento);

    Equipo.Oferta.FinanciamientoMovil = Financiamiento;
    $("#RentaTotFinan"+ Equipo.Orden).text(Financiamiento);

    var SaltoPaquete = Equipo.Oferta.SaltoPaquete == undefined ? 0 : Equipo.Oferta.SaltoPaquete;

    var Salto = parseFloat(SaltoPaquete) + parseFloat(Financiamiento);
    Equipo.Oferta.Salto = Salto;
    $("#RentaTotSalto"+Equipo.Orden).text(Salto);

    var RentaSinAdic = Equipo.Oferta.RentaSinAdicionales == undefined ? Equipo.Oferta.PrincipalTotal : Equipo.Oferta.RentaSinAdicionales;
    var RentaAdic = Equipo.Oferta.Adicionales == undefined ? Equipo.Oferta.RentaAdicional: Equipo.Oferta.Adicionales;
    var RentaTotal = parseFloat(RentaSinAdic) + parseFloat(RentaAdic) + parseFloat(Financiamiento);
    Equipo.Oferta.RentaTotal = RentaTotal;
    $("#RentaTotal"+ Equipo.Orden).text(RentaTotal);
};