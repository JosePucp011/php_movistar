/* global Identificadores, Documento */
var Hogar = {};
Documento.SiguienteFormulario = Hogar;

Hogar.Limpiar = function(){
    $('#Formulario_Hogar').hide();
    $('#Formulario_Hogar').html('');
    
    if(Hogar.SiguienteFormulario !== undefined){
        Hogar.SiguienteFormulario.Limpiar();
    }
    
};



Hogar.Mostrar = function(data){
    if(data.tipo === 'Alta Nueva'){
        if(Hogar.SiguienteFormulario !== undefined){
            Hogar.SiguienteFormulario.Mostrar(data);
        }
    } else {
        Hogar.MostrarSeleccion(data);
    }
};

Hogar.OnCheckboxMovilCambio = function () {
    if ($('.checkboxMovil:checked').length <= 2) {
        $('#error_hogar_moviles').text('');
    } else {
        $('#error_hogar_moviles').text('Debe seleccionar como maximo 2 moviles');

    }
};

Hogar.OnCheckboxFijoCambio = function () {
    if ($('.checkboxFijo:checked').length <= 1) {
        $('#error_hogar_fijos').text('');
    } else {
        $('#error_hogar_fijos').text('Debe seleccionar como maximo 1 fijo');
    }
};

Hogar.CrearConsulta = function (Consulta) {
    var NuevaConsulta = {
        Fijo: {Presente: 0},
        Movil1: {Presente: 0},
        Movil2: {Presente: 0}
    };

    var fijo = $('.checkboxFijo:checked')[0];
    if (fijo !== undefined) {
        var idObjeto = $(fijo).attr('id');
        var id = parseInt(idObjeto.replace('fijos_', ''));

        if (Consulta.Fijos[id] !== undefined) {
            NuevaConsulta.Fijo = Consulta.Fijos[id];
        }
    }    
    var moviles = $('.checkboxMovil:checked');

    
    if (moviles[0] !== undefined) {

        var idObjeto = $(moviles[0]).attr('id');
        var id = parseInt(idObjeto.replace('moviles_', ''));

        if (Consulta.Moviles[id] !== undefined) {
            NuevaConsulta.Movil1 = Consulta.Moviles[id];
        }
    }

    if (moviles[1] !== undefined) {
        var idObjeto = $(moviles[1]).attr('id');
        var id = parseInt(idObjeto.replace('moviles_', ''));

        if (Consulta.Moviles[id] !== undefined) {
            NuevaConsulta.Movil2 = Consulta.Moviles[id];
        }
    }
    
    
    return NuevaConsulta;

};

Hogar.CrearConsultaDeHogar = function(Consulta){
     var NuevaConsulta = {
        Fijo: {Presente: 0},
        Movil1: {Presente: 0},
        Movil2: {Presente: 0}
    };
    
    if(Consulta.Hogar.Fijos[0] !== undefined){
        NuevaConsulta.Fijo = Consulta.Hogar.Fijos[0];
    }
    
    if(Consulta.Hogar.Moviles[0] !== undefined){
        NuevaConsulta.Movil1 = Consulta.Hogar.Moviles[0];
    }
    
    if(Consulta.Hogar.Moviles[1] !== undefined){
        NuevaConsulta.Movil2 = Consulta.Hogar.Moviles[1];
    }
    
    if(Consulta.Hogar.Moviles.length === 1){
        
        var moviles = $('.checkboxMovil:checked');
        
        if(moviles.length > 1){
            alert('Seleccionar solo 1 Movil');
            return;
        }else{
            if (moviles[0] !== undefined) {

                var idObjeto = $(moviles[0]).attr('id');
                var id = parseInt(idObjeto.replace('moviles_', ''));

                if (Consulta.Moviles[id] !== undefined) {
                    NuevaConsulta.Movil2 = Consulta.Moviles[id];
                }
            }
        }
    }
    return NuevaConsulta;
};

Hogar.OnClickConsultar = function (Consulta) {
    Identificadores.movil1_portabilidad = false;
    Identificadores.movil2_portabilidad = false;

    if (Consulta.Hogar.Presente) {
        if (Consulta.Hogar.Fijos.length === 1 && Consulta.Hogar.Moviles.length >= 1 ){
            var NuevaConsulta = Hogar.CrearConsultaDeHogar(Consulta);
            if(Hogar.SiguienteFormulario !== undefined){
                Hogar.SiguienteFormulario.Mostrar(NuevaConsulta);
            }
        }else{
            alert('Esperar que se consoliden los pedidos en vuelo');
        }
        
    } else if ($('.checkboxFijo:checked').length >= 2) {
        alert('Verifique la cantidad de numeros fijos seleccionados');
    } else if ($('.checkboxMovil:checked').length >= 3) {
        alert('Verifique la cantidad de numeros moviles seleccionados');
    } else {
        var NuevaConsulta = Hogar.CrearConsulta(Consulta);

        if(Hogar.SiguienteFormulario !== undefined){
            Hogar.SiguienteFormulario.Mostrar(NuevaConsulta);
        }
    }
};


Hogar.MostrarSeleccion = function(data){
    
    var Consulta = data.respuestaConsulta;
    Hogar.MostrarContenido(data.respuestaConsulta);
    
    $('.checkboxMovil').change(Hogar.OnCheckboxMovilCambio);
    $('.checkboxFijo').change(Hogar.OnCheckboxFijoCambio);
    
    $('#consultarHogar').click(function(){
        Hogar.OnClickConsultar(Consulta);
    });
    
};

Hogar.MostrarContenido = function(Consulta){
    var contenido = "<fieldset id='servicioCliente'><legend>Servicio Cliente</legend>";
    
    var hogar = Hogar.MostrarHogar(Consulta.Hogar);
    var moviles = Hogar.MostrarMoviles(Consulta.Moviles,Consulta.Hogar);
    var fijos = Hogar.MostrarFijos(Consulta.Fijos,Consulta.Hogar);
    
    contenido += hogar+moviles+fijos;
    contenido += '<br><input type="submit" id="consultarHogar" value="Consultar" style="margin-top: 5px;">';
    contenido += "</fieldset>";
    
    
    $('#Formulario_Hogar').html(contenido);
    $('#Formulario_Hogar').show();
};




Hogar.MostrarFijos = function(Fijos,Hogar){
    if(Fijos.length === 0){
        return "";
    }
    
    var contenido = "<fieldset id='fijos'><legend>Fijos</legend>";
    var checked = "checked";
    var limite = 1;
      
    for(var i in Fijos){   
        if(Fijos[i].Valido){
            
            if(!Hogar.Presente ||
                (Hogar.Presente && Hogar.Fijos.length === 0 ) ){
                
                if(limite === 0){
                    checked = "";
                }

                var identificadorCheckBox = 'fijos_'+i;
                var identificadorLabel = 'fijos_'+i+'_etiqueta';
                contenido += "<input type=checkbox id='"+identificadorCheckBox+"' class='checkboxFijo' "+checked+">";
                contenido += "<label for='"+identificadorCheckBox+"' id='"+identificadorLabel+"'>"+Fijos[i].Descripcion+"</label>";
                contenido += "<br>";
                --limite;
            }
            else{
                contenido += "<span style='text-decoration: line-through'>"+Fijos[i].Descripcion+"</span><br>";
            }
        } else {
            contenido += "<span style='text-decoration: line-through'>"+Fijos[i].Descripcion+"</span><br>";
        }
        
        
    }
    contenido += "<span id='error_hogar_fijos' class='error'></span>";
    contenido +="</fieldset>";
    return contenido;
    
};


Hogar.MostrarHogar = function(Hogar){
    
    if(!Hogar.Presente){
        return "";
    }
    
    var contenido = "<fieldset id='hogar'><legend>Hogar</legend>";
    
    for(var i in Hogar.Fijos){
        contenido += Hogar.Fijos[i].Descripcion+"<br>";
    }
    
    for(var i in Hogar.Moviles){
        contenido += Hogar.Moviles[i].DescripcionMT+"<br>";
    }
    
    contenido +="</fieldset>";
    return contenido;
};

Hogar.MostrarMoviles = function(Moviles,Hogar){
    

    
    if(Moviles.length === 0){
        return "";
    }
    
    var checked = "checked";
    var limite = 2;
    
    var checkedHogar = "checked";
    var limiteHogar = 0;
    
    if(Hogar.Moviles.length === 0){
        limiteHogar = 2;
    }
    if(Hogar.Moviles.length === 1){
        limiteHogar = 1;
    }
        
        
    var contenido = "<fieldset id='moviles'><legend>Moviles</legend>";
    for (var i in Moviles){
        if(Moviles[i].Valido){
            
            if(!Hogar.Presente){
                if(limite === 0){
                    checked = "";
                }

                var identificadorCheckBox = 'moviles_'+i;
                var identificadorLabel = 'moviles_'+i+'_etiqueta';
                contenido += "<input type=checkbox id='"+identificadorCheckBox+"' class='checkboxMovil' "+checked+">";
                contenido += "<label for='"+identificadorCheckBox+"' id='"+identificadorLabel+"'>"+Moviles[i].Descripcion+"</label>";
                contenido += "<br>";
                --limite;
            } else {
                
                if(limiteHogar === 0){
                    checkedHogar = "";
                }
                
                var identificadorCheckBoxHogar = 'moviles_'+i;
                var identificadorLabelHogar = 'moviles_'+i+'_etiqueta';
                contenido += "<input type=checkbox id='"+identificadorCheckBoxHogar+"' class='checkboxMovil' "+checkedHogar+">";
                contenido += "<label for='"+identificadorCheckBoxHogar+"' id='"+identificadorLabelHogar+"'>"+Moviles[i].Descripcion+"</label>";
                contenido += "<br>";
                
                --limiteHogar;
            }
        }else{
            contenido += "<span style='text-decoration: line-through'>"+Moviles[i].Descripcion+"</span><br>";
        }
        
    }
    contenido += "<span id='error_hogar_moviles' class='error'></span>";
    contenido +="</fieldset>";
    return contenido;
};