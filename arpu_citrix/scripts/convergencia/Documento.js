/* global Util, Identificadores, Hogar */
var Documento = {};
Documento.documento = null;
Documento.tipoDocumento = null;
Documento.ultimoDocumento = null;
Documento.EstaConsultado = false;
Documento.EsMovistarTotal = 0;
Documento.Consulta = null;
Documento.Nombre = null;

Documento.Inicializar = function(){
    Documento.ConfigurarDocumento();
    Documento.Limpiar();
};

Documento.ExisteSiguienteFormulario = function(){
    return Documento.SiguienteFormulario !== undefined;
};

Documento.Limpiar = function(){
    Documento.ultimoDocumento = null;
    Documento.EstaConsultado = false;
    $('#nombre').text('');
    $('#empleado').text('');
    $('#movitar_total').text('');
    $('#reasignacion').text('');
    $('#AltaNueva').hide();
    $('#error_consultaDocumento').text('');
    $('#error_consultaDocumento').hide();
    
    $('#fieldset_NOTIFICACION').css("display","none");
    $('#fieldset_NETFLIX').css("display","none");
    $('#fieldset_PEDIDO').css("display","none");
    $('#fieldset_CANCELACION').css("display","none");
    $('#fieldset_Mensaje').css("display","none"); // ye 2

    if(Documento.SiguienteFormulario !== undefined){
        Documento.SiguienteFormulario.Limpiar();
    }
};

Documento.ConfigurarDocumento = function(){
    $('#documento').keyup(Documento.CambioDocumento);
    $('#AltaNueva').click(function(){
        if(Documento.SiguienteFormulario !== undefined){
            Documento.SiguienteFormulario.Mostrar({tipo : 'Alta Nueva'});            
        }
    });
};

Documento.CambioDocumento = function(){    
    Documento.LeerDocumento();
    Documento.ProcesarCambio();
};

Documento.LeerDocumento = function(){
    var documento = Documento.OnDatoActualizarDocumento('#documento');

    Documento.tipoDocumento = $('#tipoDocumento').val();
    Documento.documento = Documento.ValidarDocumento(documento);
};

Documento.ValidarDocumento = function(Texto) {
   return (Texto.length !== 8 && Texto.length !== 11 && Texto.length !== 9 ? null : Util.ValidarNumero(Texto));
};

Documento.ProcesarCambio = function(){
    var tipoCambioDocumento = Util.TipoCambio(Documento.ultimoDocumento,Documento.documento);
   
    if(tipoCambioDocumento === 'Sin Cambio'){
        return;
    } else if(tipoCambioDocumento === 'Invalido' && ! Documento.EstaConsultado) {
        return;
    } else if(tipoCambioDocumento === 'Valido' && Documento.EstaConsultado) {
        Documento.Limpiar();
        Documento.Consultar();
    } else if(tipoCambioDocumento === 'Valido' && !Documento.EstaConsultado){
        Documento.Consultar();
    } else if(tipoCambioDocumento === 'Invalido' && Documento.EstaConsultado) {
        Documento.Limpiar();
    } 
};

Documento.URL = 'Http/DocumentoRESTInterno.php';

Documento.Consultar = function(){
    $('#fieldset_NOTIFICACION').css("display","none");
    $('#fieldset_NETFLIX').css("display","none");
    $('#fieldset_PEDIDO').css("display","none");
    $('#fieldset_CANCELACION').css("display","none");
    $('#fieldset_Mensaje').css("display","none"); // ye 3
    
    Documento.ultimoDocumento = Documento.documento;
    Documento.EstaConsultado = true;
    Util.ConsultaAjax(
        Documento.URL,
        Documento.CrearData(),
        Documento.Mostrar);
    
};

Documento.MostrarError = function(error){
    $('#error_consultaDocumento').text(error);
    $('#error_consultaDocumento').show();
};

Documento.Mostrar = function(Consulta){
    Documento.Consulta=Consulta;
    Documento.Nombre=Consulta.Nombre;
    Documento.EsMovistarTotal = 0;
    if(Consulta.error === 'Documento no encontrado'){
        $('#AltaNueva').show();
        Documento.MostrarError(Consulta.error);
    } else if(Consulta.error !== undefined){
        Documento.MostrarError(Consulta.error);
    } else {
        $('#nombre').text(Consulta.Nombre);
        
        if(Consulta.Empleado){
            $('#empleado').text(" - CLIENTE EMPLEADO");
        }
        
        if(Consulta.Hogar.Presente){
            Documento.EsMovistarTotal = 1;
            if(Consulta.Hogar.Tipo === 2){

                if( Consulta.Reasignacion.length>0) {
                   // $('#movitar_total').text(" - CLIENTE MOVISTAR TOTAL CON REASIGNACION DE PRECIOS");
                    $('#movitar_total').html(" - CLIENTE MOVISTAR TOTAL");
                    $('#reasignacion').html("<div  onclick='Documento.MostrarReasignacionPrecio();' class='Equipamiento' > CON REASIGNACION DE PRECIOS</div>");
                    
                   /*  if($("#Tipo4play").val() == "CuatroPlay") {
                        Documento.MostrarReasignacionPrecio(Consulta);// Validar mas casos
                       
                        console.log(Consulta);
                    } */
                }else{
                    $('#movitar_total').text(" - CLIENTE MOVISTAR TOTAL");
                }                
            }else {
                if( Consulta.Reasignacion.length>0) {
                    $('#movitar_total').html(" - CLIENTE MOVISTAR TOTAL EN VUELO");
                    $('#reasignacion').html("<div  onclick='Documento.MostrarReasignacionPrecio();' class='Equipamiento' > CON REASIGNACION DE PRECIOS</div>");
                    //$('#movitar_total').text(" - CLIENTE MOVISTAR TOTAL CON REASIGNACION DE PRECIOS");
                    /* if($("#Tipo4play").val() == "CuatroPlay") {
                        Documento.MostrarReasignacionPrecio(Consulta);
                       
                        console.log(Consulta);                      
                    } */
                }else{
                    $('#movitar_total').text(" - CLIENTE MOVISTAR TOTAL EN VUELO"); 
                }
                  
            }
        }
    
        if(Documento.SiguienteFormulario !== undefined){
            Documento.SiguienteFormulario.Mostrar({
                tipo : 'Planta',
                tipoDocumento : Documento.tipoDocumento,
                documento : Documento.documento,
                respuestaConsulta : Consulta
            });            
        }
    }
};

Documento.CrearData = function(){
    return {
        documento : Documento.documento
    };
};

Documento.OnDatoActualizarDocumento = function(NombreInput){
    var Texto = $(NombreInput).val();
    var TextoLimpio = "";
    for (var i = 0; i < Texto.length; ++i) {
       var caracter = Texto.charAt(i);
       if (caracter != ' ') {
          TextoLimpio += caracter;
       }
    }
    if(TextoLimpio !== Texto){
        $(NombreInput).val(TextoLimpio);
    }

    return TextoLimpio;
};

Documento.formato=function(texto){
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
};

Documento.MostrarReasignacionPrecio = function(){
    var Consulta=Documento.Consulta;
    var nombre=Consulta.Nombre;
    if( (Consulta.Reasignacion.length>0)) {
        var monto=Consulta.Reasignacion[0].Monto_Total_MT_NUEVO;
        var m1=Consulta.Reasignacion[0].Monto_plan_movil_actual1;
        var m2=Consulta.Reasignacion[0].RENTA_MOVIL_NUEVO1;
        var m3=Consulta.Reasignacion[0].TARIFA_INTERNET_FIJO_ACTUAL;
        var m4=Consulta.Reasignacion[0].TARIFA_INTERNET_FIJO_NUEVA;
    }else{
        var monto=0;
        var m1=0;
        var m2=0;
        var m3=0;
        var m4=0;
    }
    
        var html= "";

        var fechaformateada="";    
        fechaformateada=Documento.formato(Consulta.Reasignacion[0].fecha);      
        var dia = parseInt( fechaformateada.substr(0, 2));

        if(Consulta.Reasignacion[0].Tipo=='A'){
        var html= "";
        html += '<h1 style=" border-radius: 4px; background-color:#efe; nborder: 1px solid #1C94C4;color: #1C94C4;padding: 6px;"> Migración de Parrilla - '+ fechaformateada + '</h1><br>';
       
        html+="<b>"+nombre +" </b>, Por modificaciones tecnológicas en nuestros plantes tenemos que hacer cambios en los precios de los componentes de su paquete Movistar Total. Reafirmamos que con estos cambios no se verá afectado el monto total que venía pagando por su paquete contratado.<br><br>";
        html+="A partir del <b>" +dia +"</b> de Julio se realizará un ajuste del precio en su internet Fijo y en su plan Móvil de su paquete Movistar total, pero su precio total del paquete no se verá afectado y usted seguirá pagando lo mismo.<br>";
            html+="<br>Recuerde que usted seguirá pagando <b>S/."+ monto +"</b> por su paquete Movistar Total contratado.<br>";
            html+="<br>*Recuerda que, si tienes contratado servicios adicionales a tu paquete, el costo de estos será adicional al costo de tu paquete de Movistar Total.";
        html+="<br>*Te recordamos que, si tienes afiliada alguna de tus líneas al débito automático, revises con tu banco el monto límite de cobro (en caso sea necesario modificarlo) para que no se generen problemas de cobro posteriores.";
        html+="<br>*De no estar de acuerdo, tiene derecho al término de su contrato conforme a norma vigente.<br>";
    
        html += '<br><h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;"> Beneficios </h1><br>';
        html += '<table align="center">';
        html += '<tr>';
        html += '<td>';
                    html += '<table align="center">';
                    html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:100%;">Su Plan Móvil Disminuye de </th></tr>';
                    html += '<tr><td>';
                        html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 12px; text-align: center;" >';
                        html += '<tr>';
                        html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Actual</th>';
                        html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> A Partir de Julio</th>';
                        html += '</tr>';
                        html += '<tr style="font-size: 20px;" >';
                        html += '<td><b>S/.'+ m1 +' </td>';
                        html += '<td><b>S/.'+ m2 +' </b></td>';
                        html += '</tr>';
                        html += '</table>';
                    html += '</td></tr>';
                    html += '</table>';
        html += '</td>';
        html += '<td>';
        html += '<table align="center">';
        html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:100%;">Su Internet Fijo Aumenta de </th></tr>';
        html += '<tr><td>';
            html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 12px; text-align: center;" >';
            html += '<tr>';
            html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Actual</th>';
                        html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> A Partir de Julio</th>';
            html += '</tr>';
            html += '<tr style="font-size: 20px;" >';
            html += '<td><b>S/.'+ m3 +'</b></td>';
            html += '<td><b>S/.'+ m4 +'</b></td>';      
            html += '</tr>';
            html += '</table>';
        html += '</td></tr>';
        html += '</table>';
        html += '</td>';
        html += '</tr>';
        html += '</table>';
        }else{
            html += "<h1 style=' border-radius: 4px; background-color:#efe; nborder: 1px solid #1C94C4;color: #1C94C4;padding: 6px;'> Migración de Parrilla - "+fechaformateada+"</h1><br>";       
            html+="<b>"+nombre +" </b>, Por modificaciones tecnológicas en nuestros plantes tenemos que hacer cambios en los precios de los componentes de su paquete Movistar Total.<br><br>";
            
            if(Consulta.Reasignacion[0].Tipo=='B'){

                html+="<b>"+Consulta.Reasignacion[0].Mensaje+"</b><br><br><br>";
                html+="<b>Incremento: S/. ";
                html+=Consulta.Reasignacion[0].Ajuste+"</b><br><br>";

            }else if(Consulta.Reasignacion[0].Tipo=='C'){

                if((Consulta.Reasignacion[0].Mensaje!==null && Consulta.Reasignacion[0].Mensaje!=='') && Consulta.Reasignacion[0].Movil_1!=='' ){
                html+="<b>"+Consulta.Reasignacion[0].Movil_1+ " - "+Consulta.Reasignacion[0].Mensaje+"</b><br>";
                html+="<b>Ajuste: S/. ";
                html+=Consulta.Reasignacion[0].Ajuste+"</b><br>";
                }

                if((Consulta.Reasignacion[0].Mensaje1!==null && Consulta.Reasignacion[0].Mensaje1!=='') && Consulta.Reasignacion[0].Movil_2!=='' ){
                    html+="<b>"+Consulta.Reasignacion[0].Movil_2+ " - "+Consulta.Reasignacion[0].Mensaje1+"</b><br>";
                    html+="<b>Ajuste: S/. ";
                    html+=Consulta.Reasignacion[0].Ajuste1+"</b><br>";
                }
                
            }
            
            html+="<br>*Te recordamos que estos nuevos montos se verán reflejados en tu facturación de agosto.";
            html+="<br>*Recuerda que, si tienes contratado servicios adicionales a tu paquete, el costo de estos será adicional al costo de tu paquete de Movistar Total.";
            html+="<br>*Te recordamos que, si tienes afiliada alguna de tus líneas al débito automático, revises con tu banco el monto límite de cobro (en caso sea necesario modificarlo) para que no se generen problemas de cobro posteriores.";
            html+="<br>*De no estar de acuerdo, tiene derecho al término de su contrato conforme a norma vigente.<br><br><br><br><br><br><br>";        
            }
        
        Util.MostrarModalReasignacion("",html);          
        
    }