/* global Formulario */

var Convergente = {};
Convergente.htmlOpciones = null;

Convergente.Mostrar = function(){
    Convergente.MostrarOpciones();
    Convergente.ProgramarConsulta();
    $('#OpcionesConvergente').html(Convergente.htmlOpciones);
    
    $('#consultarMultiple').click(function(){
       Convergente.Consultar();
    });
};

Convergente.Consultar = function(){
    var identificador = [];
    $('input:checked[name=mulMovil]').each(function(){identificador.push({Tipo: 'Movil', Valor: $(this).val()});});
    $('input:checked[name=mulFijo]').each(function(){identificador.push({Tipo: 'Telefono', Valor: $(this).val()});});
    $('input:checked[name=mulCompetencia]').each(function()
    {identificador.push({Tipo: 'Competencia', Valor: JSON.parse($(this).val())})
        ;});
    if(identificador.length > 0){
        Formulario.identificadores = identificador;
        Formulario.Consultar(true);
    }
    
};

Convergente.ProgramarConsulta = function(){
    Convergente.htmlOpciones += "<div id='consultarMultiple' class='boton'>Consultar</div>";
};

Convergente.MostrarOpciones = function(){
    var multiple = window.ResultadoConsulta.Identificador;
    Convergente.htmlOpciones = "";
    for(var i = 0; i < multiple.length; ++i){
        Convergente.ImprimirOpcion(multiple[i]);
    }
};

Convergente.ImprimirOpcion = function(Identificador){
    
    
    var valor = Identificador.Identificador;
    
    if(typeof valor === 'object'){
        this.htmlOpciones += "<input type='checkbox' name='mul"
                +Identificador.Tipo
                +"' value='"+JSON.stringify(valor)+"' checked />"
                +Identificador.Tipo+" - "+ Identificador.Identificador.Movil+" <br>";
    } else {
    
    this.htmlOpciones += "<input type='checkbox' name='mul"
                +Identificador.Tipo
                +"' value='"+Identificador.Identificador+"' checked />"
                +Identificador.Tipo+" - "+ Identificador.Identificador+" <br>";
    }
    
};

Convergente.Estructura = function(){
    
};