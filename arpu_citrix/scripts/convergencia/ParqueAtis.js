var ParqueAtis = {};


ParqueAtis.URL = '/bus/INT/OSB/SRV/RESTConsultaATIS/ConsultaParqueEnATIS';

ParqueAtis.Request = {
"HeaderIn":{
"country":"PE",
"lang":"es",
"entity":"TDP",
"system":"APPVENTASFIJA",
"subsystem":"APPVENTASFIJA",
"originator":"PE:TDP:APPVENTASFIJA:APPVENTASFIJA",
"sender":"OracleServiceBus",
"userId":"AppUser",
"wsId":"BluemixEveris",
"wsIp":"10.11.11.11",
"operation":"Consulta Parque en ATIS",
"destination":"ES:BUS:CMS:CMS",
"execId":"550e8400-e29b-41d4-a716-446655440003",
"timestamp":"2016-09-08T06:28:00.233+01:00",
"msgType":"REQUEST"
},
"BodyIn" : {
"TipoDocumento" : "DNI",
"NumeroDocumento" : "47819666"
}
};

ParqueAtis.Callback = null;


ParqueAtis.Llamar = function(TipoDocumento,NumeroDocumento){
    
    ParqueAtis.Request.BodyIn.TipoDocumento = TipoDocumento;
    ParqueAtis.Request.BodyIn.NumeroDocumento = NumeroDocumento;
    
    
    $.ajax({
	url: ParqueAtis.URL,
	data : JSON.stringify(ParqueAtis.Request),
	type : 'POST',
	contentType: "application/json",
        dataType: 'json',
	timeout : 5000,
	async : true,
        success: function (data, textStatus, jqXHR) {
            ParqueAtis.ProcesarDatos(data);
        }
    });
    
};

ParqueAtis.ProcesarDatos = function(data){
    
    var resultado = [];
    
    if(data.BodyOut === undefined || data.BodyOut.DetalleParque === undefined || data.BodyOut.DetalleParque.length === 0){
        ParqueAtis.Callback([]);
        return;
    }
    for(var i = 0; i < data.BodyOut.DetalleParque.length; ++i){
        var servicio = data.BodyOut.DetalleParque[i];
        
        if(servicio.Telefono !== null){
            resultado.push(servicio);
        }
    }
    
    ParqueAtis.Callback(resultado);
};


