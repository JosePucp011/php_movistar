
var ParqueMovil = {};


ParqueMovil.URL_Parque = '/bus8211/CustomerInformationManagement/CustomerInformation/V1?wsdl';
ParqueMovil.URL_Parque_Detalle = '/bus8211/ProductCatalogManagement/ProductCatalog/V1?wsdl';


ParqueMovil.CrearRequest = function(TipoDocumento,NumeroDocumento){

return '<soap-env:Envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">'
+'    <soap-env:Body xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">'
+'        <ns2:SearchCustomerRequest xmlns:ns2="http://telefonica.pe/CustomerInformationManagement/CustomerInformation/V1/types">'
+'            <ns3:TefHeaderReq xmlns:ns3="http://telefonica.pe/TefRequestHeader/V1">'
+'                <ns3:userLogin>osblegados</ns3:userLogin>'
+'                <ns3:serviceChannel>CustomerInformation</ns3:serviceChannel>'
+'                <ns3:application>OSBLEGADOS</ns3:application>'
+'                <ns3:ipAddress>10.4.65.40</ns3:ipAddress>'
+'                <ns3:functionalityCode>RetrieveIndividual</ns3:functionalityCode>'
+'                <ns3:transactionTimestamp>2016-05-16T05:24:00</ns3:transactionTimestamp>'
+'                <ns3:serviceName>Calculadora</ns3:serviceName>'
+'                <ns3:version>1.0</ns3:version>'
+'            </ns3:TefHeaderReq>'
+'            <ns2:SearchCustomerRequestData>'
+'                <ns2:customerIdentification>'
+'                    <ns2:userContactPrimaryId>'+NumeroDocumento+'</ns2:userContactPrimaryId>'
+'                    <ns2:userContactPrimaryType>'+TipoDocumento+'</ns2:userContactPrimaryType>'
+'                </ns2:customerIdentification><!--Optional:-->'
+'                <ns2:isToGetCycleCode>false</ns2:isToGetCycleCode>'
+'            </ns2:SearchCustomerRequestData>'
+'        </ns2:SearchCustomerRequest>'
+'    </soap-env:Body>'
+'</soap-env:Envelope>';
};


ParqueMovil.CrearRequest_Detalle = function(Numero){
return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:typ="http://telefonica.pe/ProductCatalogManagement/ProductCatalog/V1/types" xmlns:v1="http://telefonica.pe/TefRequestHeader/V1">'
+'   <soapenv:Header/>'
+'   <soapenv:Body>'
+'      <typ:RetrievePlanInfoRequest>'
+'         <v1:TefHeaderReq>'
+'            <v1:userLogin>USR_USSD</v1:userLogin>'
+'            <v1:serviceChannel>USSD</v1:serviceChannel>'
+'            <v1:application>USSD</v1:application>'
+'            <v1:ipAddress>10.10.4.132</v1:ipAddress>'
+'            <v1:transactionTimestamp>2016-09-20T09:19:47.000-05:00</v1:transactionTimestamp>'
+'            <v1:serviceName>retrievePlanInfo</v1:serviceName>'
+'            <v1:version>1.0</v1:version>'
+'         </v1:TefHeaderReq>'
+'         <typ:RetrievePlanInfoRequestData>'
+'            <typ:msisdn>'
+'               <typ:number>'+Numero+'</typ:number>'
+'            </typ:msisdn>'
+'         </typ:RetrievePlanInfoRequestData>'
+'      </typ:RetrievePlanInfoRequest>'
+'   </soapenv:Body>'
+'</soapenv:Envelope>';
};

ParqueMovil.Callback = null;
ParqueMovil.retry = false;

ParqueMovil.Consultar = function(TipoDocumento,NumeroDocumento){
    return $.ajax({
        url: ParqueMovil.URL_Parque,
        data : ParqueMovil.CrearRequest(TipoDocumento,NumeroDocumento),
        type : 'POST',
        contentType: "application/xml",
        dataType: "text",
        timeout : 5000,
        async : true,
        success: function (data, textStatus, jqXHR) {

            var texto = data.replace(/soapenv:/g,"").replace(/ns2:/g,"");
            var resultado = $.xml2json(texto).Body.SearchCustomerResponse.SearchCustomerResponseData.searchCustomerResultsList.searchCustomerResults;


            var numeros = [];
            var estados = [];

            if(resultado.length === undefined){
                numeros.push(resultado.subscriberInfo.telephoneNumber.number);
            } else {
                for(var i = 0; i < resultado.length; ++i){
                    numeros.push(resultado[i].subscriberInfo.telephoneNumber.number);
                    estados.push(resultado[i].subscriberInfo.statusTitle);
                }
            }

            var consultas = [];

            for(var i = 0 ; i < numeros.length; ++i){

                consultas.push($.ajax({
                                url: ParqueMovil.URL_Parque_Detalle,
                                data : ParqueMovil.CrearRequest_Detalle(numeros[i]),
                                type : 'POST',
                                contentType: "application/xml",
                                dataType: "text",
                                timeout : 5000,
                                async : true }));
            }
            
            

            $.when.apply($,consultas).then(function(){
                
                ParqueMovil.retry = false;
                
                var celulares = [];
                
                for(var i = 0; i < arguments.length; ++i){
                    var texto = arguments[i][0].replace(/soapenv:/g,"").replace(/ns4:/g,"");
                    var resultado_detalle = $.xml2json(texto).Body.RetrievePlanInfoResponse.RetrievePlanInfoResponseData.offerId.billingOfferId;
                    
                    celulares.push({
                        celular: numeros[i],
                        billingOfferId : resultado_detalle,
                        estado : estados[i]
                    });
                }
                ParqueMovil.Callback(celulares);
                
                
            }).fail(function(){
                
                if(ParqueMovil.retry){
                    return;
                } else {
                    ParqueMovil.retry = true;
                    ParqueMovil.Consultar(TipoDocumento,NumeroDocumento);
                }
                
            });


        }   
    });
};