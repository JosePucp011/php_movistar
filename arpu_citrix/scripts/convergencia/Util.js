var Util = {};


Util.PresionarCheck = function(checked,componente){
    
    if(checked){
        $(componente).val('');
        $(componente).hide();
        
    } else {
        $(componente).val('');
        $(componente).show();
    }
};



Util.ValidarNumero = function(Texto) {
    for (var i = 0; i < Texto.length; ++i) {
      var caracter = Texto.charAt(i);
      if (!(caracter >= '0' && caracter <= '9')) {
         return null;
      }
   }
   return Texto;
};

Util.ValidarNumeroMonoTV = function(Texto) {
    
    if(Texto.charAt(0) !== '-'){
        return null;
    }
    
    for (var i = 1; i < Texto.length; ++i) {
      var caracter = Texto.charAt(i);
      if (!(caracter >= '0' && caracter <= '9')) {
         return null;
      }
   }
   return Texto;
};

Util.TipoCambio = function(Antiguo,Nuevo){
    if(Antiguo === Nuevo){
        return 'Sin Cambio';
    } else if(Nuevo === null){
        return 'Invalido';
    } else {
        return 'Valido';
    }
};

Util.ConsultaAjax = function(url,data,funcionResultado){
    return $.ajax({
         url: url,
         context: document.body,
         type: 'GET',
         data:  data,
         datatype: 'json',
         success: funcionResultado
      });
};

Util.ConsultaAjaxp = function(url,data,funcionResultado){
    return $.ajax({
         url: url,
         context: document.body,
		 crossDomain: true,
         type: 'GET',
         data:  data,
         datatype: 'jsonp',
         success: funcionResultado
      });
};


Util.ConsultaAjaxPostObjeto = function(url,objeto,funcionResultado){
    return $.ajax({
         url: url,
         context: document.body,
         type: 'POST',
         data:  JSON.stringify(objeto),
         datatype: 'json',
         success: funcionResultado
      });
};




Util.ConsultaAjaxSinResultado = function(url,data){
    return $.ajax({
         url: url,
         context: document.body,
         type: 'GET',
         data:  data,
         datatype: 'json'
      });
};


Util.PostAjax = function(url,data,funcionResultado){
    $.ajax({
         url: url,
         context: document.body,
         type: 'POST',
         data:  data,
         datatype: 'json',
         contentType: 'application/json',
         success: funcionResultado
      });
};

Util.CrearDialogo = function(Dialogo){
    $(Dialogo).dialog({
       autoOpen: false, resizable: false, draggable: false, modal: true,
       width: 700,
       maxWidth: 700,
       maxHeight: 700,
       minHeight: 100
    });
};

Util.MostrarModalError = function(titulo,mensaje){
    Util.CrearDialogo("#modal");
    $("#modal").html("<div class='error'>"+mensaje+"</div>");
    $('#modal').css('display','block');
};


Util.MostrarModal = function(titulo,mensaje){
    $('#titulo_modal').text(titulo);
    $("#modal-body").html(mensaje);
    $('#modal').css('display','block');
};

Util.MostrarModalReasignacion = function(titulo,mensaje){
    Util.CrearDialogo("#modal1"); 
    $("#modal1").dialog("open"); 
    $('#titulo_modal1').text(titulo);
    $("#modal-body1").html(mensaje);
   $('#modal1').css('display','block'); 
};