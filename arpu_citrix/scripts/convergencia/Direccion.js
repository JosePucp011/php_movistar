var departamentos = [
  {
    "name": "Huánuco",
    "sku": "10",
    "provincias": [
      {
        "name": "Huánuco",
        "sku": "01",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Huánuco",
            "sku": "01",
            "skuDepPro": "1001",
            "ubigeo": "100101"
          },
          {
            "name": "Amarilis",
            "sku": "02",
            "skuDepPro": "1001",
            "ubigeo": "100102"
          },
          {
            "name": "Chinchao",
            "sku": "03",
            "skuDepPro": "1001",
            "ubigeo": "100103"
          },
          {
            "name": "Churubamba",
            "sku": "04",
            "skuDepPro": "1001",
            "ubigeo": "100104"
          },
          {
            "name": "Margos",
            "sku": "05",
            "skuDepPro": "1001",
            "ubigeo": "100105"
          },
          {
            "name": "Quisqui (Kichki)",
            "sku": "06",
            "skuDepPro": "1001",
            "ubigeo": "100106"
          },
          {
            "name": "San Francisco de Cayran",
            "sku": "07",
            "skuDepPro": "1001",
            "ubigeo": "100107"
          },
          {
            "name": "San Pedro de Chaulan",
            "sku": "08",
            "skuDepPro": "1001",
            "ubigeo": "100108"
          },
          {
            "name": "Santa Maria del Valle",
            "sku": "09",
            "skuDepPro": "1001",
            "ubigeo": "100109"
          },
          {
            "name": "Yarumayo",
            "sku": "10",
            "skuDepPro": "1001",
            "ubigeo": "100110"
          },
          {
            "name": "Pillco Marca",
            "sku": "11",
            "skuDepPro": "1001",
            "ubigeo": "100111"
          },
          {
            "name": "Yacus",
            "sku": "12",
            "skuDepPro": "1001",
            "ubigeo": "100112"
          },
          {
            "name": "San Pablo de Pillao",
            "sku": "13",
            "skuDepPro": "1001",
            "ubigeo": "100113"
          }
        ]
      },
      {
        "name": "Ambo",
        "sku": "02",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Ambo",
            "sku": "01",
            "skuDepPro": "1002",
            "ubigeo": "100201"
          },
          {
            "name": "Cayna",
            "sku": "02",
            "skuDepPro": "1002",
            "ubigeo": "100202"
          },
          {
            "name": "Colpas",
            "sku": "03",
            "skuDepPro": "1002",
            "ubigeo": "100203"
          },
          {
            "name": "Conchamarca",
            "sku": "04",
            "skuDepPro": "1002",
            "ubigeo": "100204"
          },
          {
            "name": "Huacar",
            "sku": "05",
            "skuDepPro": "1002",
            "ubigeo": "100205"
          },
          {
            "name": "San Francisco",
            "sku": "06",
            "skuDepPro": "1002",
            "ubigeo": "100206"
          },
          {
            "name": "San Rafael",
            "sku": "07",
            "skuDepPro": "1002",
            "ubigeo": "100207"
          },
          {
            "name": "Tomay Kichwa",
            "sku": "08",
            "skuDepPro": "1002",
            "ubigeo": "100208"
          }
        ]
      },
      {
        "name": "Dos de Mayo",
        "sku": "03",
        "skuDep": "10",
        "distritos": [
          {
            "name": "La Union",
            "sku": "01",
            "skuDepPro": "1003",
            "ubigeo": "100301"
          },
          {
            "name": "Chuquis",
            "sku": "07",
            "skuDepPro": "1003",
            "ubigeo": "100307"
          },
          {
            "name": "Marias",
            "sku": "11",
            "skuDepPro": "1003",
            "ubigeo": "100311"
          },
          {
            "name": "Pachas",
            "sku": "13",
            "skuDepPro": "1003",
            "ubigeo": "100313"
          },
          {
            "name": "Quivilla",
            "sku": "16",
            "skuDepPro": "1003",
            "ubigeo": "100316"
          },
          {
            "name": "Ripan",
            "sku": "17",
            "skuDepPro": "1003",
            "ubigeo": "100317"
          },
          {
            "name": "Shunqui",
            "sku": "21",
            "skuDepPro": "1003",
            "ubigeo": "100321"
          },
          {
            "name": "Sillapata",
            "sku": "22",
            "skuDepPro": "1003",
            "ubigeo": "100322"
          },
          {
            "name": "Yanas",
            "sku": "23",
            "skuDepPro": "1003",
            "ubigeo": "100323"
          }
        ]
      },
      {
        "name": "Huacaybamba",
        "sku": "04",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Huacaybamba",
            "sku": "01",
            "skuDepPro": "1004",
            "ubigeo": "100401"
          },
          {
            "name": "Canchabamba",
            "sku": "02",
            "skuDepPro": "1004",
            "ubigeo": "100402"
          },
          {
            "name": "Cochabamba",
            "sku": "03",
            "skuDepPro": "1004",
            "ubigeo": "100403"
          },
          {
            "name": "Pinra",
            "sku": "04",
            "skuDepPro": "1004",
            "ubigeo": "100404"
          }
        ]
      },
      {
        "name": "Huamalies",
        "sku": "05",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Llata",
            "sku": "01",
            "skuDepPro": "1005",
            "ubigeo": "100501"
          },
          {
            "name": "Arancay",
            "sku": "02",
            "skuDepPro": "1005",
            "ubigeo": "100502"
          },
          {
            "name": "Chavin de Pariarca",
            "sku": "03",
            "skuDepPro": "1005",
            "ubigeo": "100503"
          },
          {
            "name": "Jacas Grande",
            "sku": "04",
            "skuDepPro": "1005",
            "ubigeo": "100504"
          },
          {
            "name": "Jircan",
            "sku": "05",
            "skuDepPro": "1005",
            "ubigeo": "100505"
          },
          {
            "name": "Miraflores",
            "sku": "06",
            "skuDepPro": "1005",
            "ubigeo": "100506"
          },
          {
            "name": "Monzon",
            "sku": "07",
            "skuDepPro": "1005",
            "ubigeo": "100507"
          },
          {
            "name": "Punchao",
            "sku": "08",
            "skuDepPro": "1005",
            "ubigeo": "100508"
          },
          {
            "name": "Puños",
            "sku": "09",
            "skuDepPro": "1005",
            "ubigeo": "100509"
          },
          {
            "name": "Singa",
            "sku": "10",
            "skuDepPro": "1005",
            "ubigeo": "100510"
          },
          {
            "name": "Tantamayo",
            "sku": "11",
            "skuDepPro": "1005",
            "ubigeo": "100511"
          }
        ]
      },
      {
        "name": "Leoncio Prado",
        "sku": "06",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Rupa-Rupa",
            "sku": "01",
            "skuDepPro": "1006",
            "ubigeo": "100601"
          },
          {
            "name": "Daniel Alomia Robles",
            "sku": "02",
            "skuDepPro": "1006",
            "ubigeo": "100602"
          },
          {
            "name": "Hermilio Valdizan",
            "sku": "03",
            "skuDepPro": "1006",
            "ubigeo": "100603"
          },
          {
            "name": "Jose Crespo y Castillo",
            "sku": "04",
            "skuDepPro": "1006",
            "ubigeo": "100604"
          },
          {
            "name": "Luyando",
            "sku": "05",
            "skuDepPro": "1006",
            "ubigeo": "100605"
          },
          {
            "name": "Mariano Damaso Beraun",
            "sku": "06",
            "skuDepPro": "1006",
            "ubigeo": "100606"
          },
          {
            "name": "Pucayacu",
            "sku": "07",
            "skuDepPro": "1006",
            "ubigeo": "100607"
          },
          {
            "name": "Castillo Grande",
            "sku": "08",
            "skuDepPro": "1006",
            "ubigeo": "100608"
          },
          {
            "name": "Pueblo Nuevo",
            "sku": "09",
            "skuDepPro": "1006",
            "ubigeo": "100609"
          },
          {
            "name": "Santo Domingo de Anda",
            "sku": "10",
            "skuDepPro": "1006",
            "ubigeo": "100610"
          }
        ]
      },
      {
        "name": "Marañon",
        "sku": "07",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Huacrachuco",
            "sku": "01",
            "skuDepPro": "1007",
            "ubigeo": "100701"
          },
          {
            "name": "Cholon",
            "sku": "02",
            "skuDepPro": "1007",
            "ubigeo": "100702"
          },
          {
            "name": "San Buenaventura",
            "sku": "03",
            "skuDepPro": "1007",
            "ubigeo": "100703"
          },
          {
            "name": "La Morada",
            "sku": "04",
            "skuDepPro": "1007",
            "ubigeo": "100704"
          },
          {
            "name": "Santa Rosa de Alto Yanajanca",
            "sku": "05",
            "skuDepPro": "1007",
            "ubigeo": "100705"
          }
        ]
      },
      {
        "name": "Pachitea",
        "sku": "08",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Panao",
            "sku": "01",
            "skuDepPro": "1008",
            "ubigeo": "100801"
          },
          {
            "name": "Chaglla",
            "sku": "02",
            "skuDepPro": "1008",
            "ubigeo": "100802"
          },
          {
            "name": "Molino",
            "sku": "03",
            "skuDepPro": "1008",
            "ubigeo": "100803"
          },
          {
            "name": "Umari",
            "sku": "04",
            "skuDepPro": "1008",
            "ubigeo": "100804"
          }
        ]
      },
      {
        "name": "Puerto Inca",
        "sku": "09",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Puerto Inca",
            "sku": "01",
            "skuDepPro": "1009",
            "ubigeo": "100901"
          },
          {
            "name": "Codo del Pozuzo",
            "sku": "02",
            "skuDepPro": "1009",
            "ubigeo": "100902"
          },
          {
            "name": "Honoria",
            "sku": "03",
            "skuDepPro": "1009",
            "ubigeo": "100903"
          },
          {
            "name": "Tournavista",
            "sku": "04",
            "skuDepPro": "1009",
            "ubigeo": "100904"
          },
          {
            "name": "Yuyapichis",
            "sku": "05",
            "skuDepPro": "1009",
            "ubigeo": "100905"
          }
        ]
      },
      {
        "name": "Lauricocha",
        "sku": "10",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Jesus",
            "sku": "01",
            "skuDepPro": "1010",
            "ubigeo": "101001"
          },
          {
            "name": "Baños",
            "sku": "02",
            "skuDepPro": "1010",
            "ubigeo": "101002"
          },
          {
            "name": "Jivia",
            "sku": "03",
            "skuDepPro": "1010",
            "ubigeo": "101003"
          },
          {
            "name": "Queropalca",
            "sku": "04",
            "skuDepPro": "1010",
            "ubigeo": "101004"
          },
          {
            "name": "Rondos",
            "sku": "05",
            "skuDepPro": "1010",
            "ubigeo": "101005"
          },
          {
            "name": "San Francisco de Asis",
            "sku": "06",
            "skuDepPro": "1010",
            "ubigeo": "101006"
          },
          {
            "name": "San Miguel de Cauri",
            "sku": "07",
            "skuDepPro": "1010",
            "ubigeo": "101007"
          }
        ]
      },
      {
        "name": "Yarowilca",
        "sku": "11",
        "skuDep": "10",
        "distritos": [
          {
            "name": "Chavinillo",
            "sku": "01",
            "skuDepPro": "1011",
            "ubigeo": "101101"
          },
          {
            "name": "Cahuac",
            "sku": "02",
            "skuDepPro": "1011",
            "ubigeo": "101102"
          },
          {
            "name": "Chacabamba",
            "sku": "03",
            "skuDepPro": "1011",
            "ubigeo": "101103"
          },
          {
            "name": "Aparicio Pomares",
            "sku": "04",
            "skuDepPro": "1011",
            "ubigeo": "101104"
          },
          {
            "name": "Jacas Chico",
            "sku": "05",
            "skuDepPro": "1011",
            "ubigeo": "101105"
          },
          {
            "name": "Obas",
            "sku": "06",
            "skuDepPro": "1011",
            "ubigeo": "101106"
          },
          {
            "name": "Pampamarca",
            "sku": "07",
            "skuDepPro": "1011",
            "ubigeo": "101107"
          },
          {
            "name": "Choras",
            "sku": "08",
            "skuDepPro": "1011",
            "ubigeo": "101108"
          }
        ]
      }
    ]
  },
  {
    "name": "Ica",
    "sku": "11",
    "provincias": [
      {
        "name": "Ica",
        "sku": "01",
        "skuDep": "11",
        "distritos": [
          {
            "name": "Ica",
            "sku": "01",
            "skuDepPro": "1101",
            "ubigeo": "110101"
          },
          {
            "name": "La Tinguiña",
            "sku": "02",
            "skuDepPro": "1101",
            "ubigeo": "110102"
          },
          {
            "name": "Los Aquijes",
            "sku": "03",
            "skuDepPro": "1101",
            "ubigeo": "110103"
          },
          {
            "name": "Ocucaje",
            "sku": "04",
            "skuDepPro": "1101",
            "ubigeo": "110104"
          },
          {
            "name": "Pachacutec",
            "sku": "05",
            "skuDepPro": "1101",
            "ubigeo": "110105"
          },
          {
            "name": "Parcona",
            "sku": "06",
            "skuDepPro": "1101",
            "ubigeo": "110106"
          },
          {
            "name": "Pueblo Nuevo",
            "sku": "07",
            "skuDepPro": "1101",
            "ubigeo": "110107"
          },
          {
            "name": "Salas",
            "sku": "08",
            "skuDepPro": "1101",
            "ubigeo": "110108"
          },
          {
            "name": "San Jose de Los Molinos",
            "sku": "09",
            "skuDepPro": "1101",
            "ubigeo": "110109"
          },
          {
            "name": "San Juan Bautista",
            "sku": "10",
            "skuDepPro": "1101",
            "ubigeo": "110110"
          },
          {
            "name": "Santiago",
            "sku": "11",
            "skuDepPro": "1101",
            "ubigeo": "110111"
          },
          {
            "name": "Subtanjalla",
            "sku": "12",
            "skuDepPro": "1101",
            "ubigeo": "110112"
          },
          {
            "name": "Tate",
            "sku": "13",
            "skuDepPro": "1101",
            "ubigeo": "110113"
          },
          {
            "name": "Yauca del Rosario",
            "sku": "14",
            "skuDepPro": "1101",
            "ubigeo": "110114"
          }
        ]
      },
      {
        "name": "Chincha",
        "sku": "02",
        "skuDep": "11",
        "distritos": [
          {
            "name": "Chincha Alta",
            "sku": "01",
            "skuDepPro": "1102",
            "ubigeo": "110201"
          },
          {
            "name": "Alto Laran",
            "sku": "02",
            "skuDepPro": "1102",
            "ubigeo": "110202"
          },
          {
            "name": "Chavin",
            "sku": "03",
            "skuDepPro": "1102",
            "ubigeo": "110203"
          },
          {
            "name": "Chincha Baja",
            "sku": "04",
            "skuDepPro": "1102",
            "ubigeo": "110204"
          },
          {
            "name": "El Carmen",
            "sku": "05",
            "skuDepPro": "1102",
            "ubigeo": "110205"
          },
          {
            "name": "Grocio Prado",
            "sku": "06",
            "skuDepPro": "1102",
            "ubigeo": "110206"
          },
          {
            "name": "Pueblo Nuevo",
            "sku": "07",
            "skuDepPro": "1102",
            "ubigeo": "110207"
          },
          {
            "name": "San Juan de Yanac",
            "sku": "08",
            "skuDepPro": "1102",
            "ubigeo": "110208"
          },
          {
            "name": "San Pedro de Huacarpana",
            "sku": "09",
            "skuDepPro": "1102",
            "ubigeo": "110209"
          },
          {
            "name": "Sunampe",
            "sku": "10",
            "skuDepPro": "1102",
            "ubigeo": "110210"
          },
          {
            "name": "Tambo de Mora",
            "sku": "11",
            "skuDepPro": "1102",
            "ubigeo": "110211"
          }
        ]
      },
      {
        "name": "Nasca",
        "sku": "03",
        "skuDep": "11",
        "distritos": [
          {
            "name": "Nasca",
            "sku": "01",
            "skuDepPro": "1103",
            "ubigeo": "110301"
          },
          {
            "name": "Changuillo",
            "sku": "02",
            "skuDepPro": "1103",
            "ubigeo": "110302"
          },
          {
            "name": "El Ingenio",
            "sku": "03",
            "skuDepPro": "1103",
            "ubigeo": "110303"
          },
          {
            "name": "Marcona",
            "sku": "04",
            "skuDepPro": "1103",
            "ubigeo": "110304"
          },
          {
            "name": "Vista Alegre",
            "sku": "05",
            "skuDepPro": "1103",
            "ubigeo": "110305"
          }
        ]
      },
      {
        "name": "Palpa",
        "sku": "04",
        "skuDep": "11",
        "distritos": [
          {
            "name": "Palpa",
            "sku": "01",
            "skuDepPro": "1104",
            "ubigeo": "110401"
          },
          {
            "name": "Llipata",
            "sku": "02",
            "skuDepPro": "1104",
            "ubigeo": "110402"
          },
          {
            "name": "Rio Grande",
            "sku": "03",
            "skuDepPro": "1104",
            "ubigeo": "110403"
          },
          {
            "name": "Santa Cruz",
            "sku": "04",
            "skuDepPro": "1104",
            "ubigeo": "110404"
          },
          {
            "name": "Tibillo",
            "sku": "05",
            "skuDepPro": "1104",
            "ubigeo": "110405"
          }
        ]
      },
      {
        "name": "Pisco",
        "sku": "05",
        "skuDep": "11",
        "distritos": [
          {
            "name": "Pisco",
            "sku": "01",
            "skuDepPro": "1105",
            "ubigeo": "110501"
          },
          {
            "name": "Huancano",
            "sku": "02",
            "skuDepPro": "1105",
            "ubigeo": "110502"
          },
          {
            "name": "Humay",
            "sku": "03",
            "skuDepPro": "1105",
            "ubigeo": "110503"
          },
          {
            "name": "Independencia",
            "sku": "04",
            "skuDepPro": "1105",
            "ubigeo": "110504"
          },
          {
            "name": "Paracas",
            "sku": "05",
            "skuDepPro": "1105",
            "ubigeo": "110505"
          },
          {
            "name": "San Andres",
            "sku": "06",
            "skuDepPro": "1105",
            "ubigeo": "110506"
          },
          {
            "name": "San Clemente",
            "sku": "07",
            "skuDepPro": "1105",
            "ubigeo": "110507"
          },
          {
            "name": "Tupac Amaru Inca",
            "sku": "08",
            "skuDepPro": "1105",
            "ubigeo": "110508"
          }
        ]
      }
    ]
  },
  {
    "name": "Junín",
    "sku": "12",
    "provincias": [
      {
        "name": "Huancayo",
        "sku": "01",
        "skuDep": "12",
        "distritos": [
          {
            "name": "Huancayo",
            "sku": "01",
            "skuDepPro": "1201",
            "ubigeo": "120101"
          },
          {
            "name": "Carhuacallanga",
            "sku": "04",
            "skuDepPro": "1201",
            "ubigeo": "120104"
          },
          {
            "name": "Chacapampa",
            "sku": "05",
            "skuDepPro": "1201",
            "ubigeo": "120105"
          },
          {
            "name": "Chicche",
            "sku": "06",
            "skuDepPro": "1201",
            "ubigeo": "120106"
          },
          {
            "name": "Chilca",
            "sku": "07",
            "skuDepPro": "1201",
            "ubigeo": "120107"
          },
          {
            "name": "Chongos Alto",
            "sku": "08",
            "skuDepPro": "1201",
            "ubigeo": "120108"
          },
          {
            "name": "Chupuro",
            "sku": "11",
            "skuDepPro": "1201",
            "ubigeo": "120111"
          },
          {
            "name": "Colca",
            "sku": "12",
            "skuDepPro": "1201",
            "ubigeo": "120112"
          },
          {
            "name": "Cullhuas",
            "sku": "13",
            "skuDepPro": "1201",
            "ubigeo": "120113"
          },
          {
            "name": "El Tambo",
            "sku": "14",
            "skuDepPro": "1201",
            "ubigeo": "120114"
          },
          {
            "name": "Huacrapuquio",
            "sku": "16",
            "skuDepPro": "1201",
            "ubigeo": "120116"
          },
          {
            "name": "Hualhuas",
            "sku": "17",
            "skuDepPro": "1201",
            "ubigeo": "120117"
          },
          {
            "name": "Huancan",
            "sku": "19",
            "skuDepPro": "1201",
            "ubigeo": "120119"
          },
          {
            "name": "Huasicancha",
            "sku": "20",
            "skuDepPro": "1201",
            "ubigeo": "120120"
          },
          {
            "name": "Huayucachi",
            "sku": "21",
            "skuDepPro": "1201",
            "ubigeo": "120121"
          },
          {
            "name": "Ingenio",
            "sku": "22",
            "skuDepPro": "1201",
            "ubigeo": "120122"
          },
          {
            "name": "Pariahuanca",
            "sku": "24",
            "skuDepPro": "1201",
            "ubigeo": "120124"
          },
          {
            "name": "Pilcomayo",
            "sku": "25",
            "skuDepPro": "1201",
            "ubigeo": "120125"
          },
          {
            "name": "Pucara",
            "sku": "26",
            "skuDepPro": "1201",
            "ubigeo": "120126"
          },
          {
            "name": "Quichuay",
            "sku": "27",
            "skuDepPro": "1201",
            "ubigeo": "120127"
          },
          {
            "name": "Quilcas",
            "sku": "28",
            "skuDepPro": "1201",
            "ubigeo": "120128"
          },
          {
            "name": "San Agustin",
            "sku": "29",
            "skuDepPro": "1201",
            "ubigeo": "120129"
          },
          {
            "name": "San Jeronimo de Tunan",
            "sku": "30",
            "skuDepPro": "1201",
            "ubigeo": "120130"
          },
          {
            "name": "Saño",
            "sku": "32",
            "skuDepPro": "1201",
            "ubigeo": "120132"
          },
          {
            "name": "Sapallanga",
            "sku": "33",
            "skuDepPro": "1201",
            "ubigeo": "120133"
          },
          {
            "name": "Sicaya",
            "sku": "34",
            "skuDepPro": "1201",
            "ubigeo": "120134"
          },
          {
            "name": "Santo Domingo de Acobamba",
            "sku": "35",
            "skuDepPro": "1201",
            "ubigeo": "120135"
          },
          {
            "name": "Viques",
            "sku": "36",
            "skuDepPro": "1201",
            "ubigeo": "120136"
          }
        ]
      },
      {
        "name": "Concepción",
        "sku": "02",
        "skuDep": "12",
        "distritos": [
          {
            "name": "Concepción",
            "sku": "01",
            "skuDepPro": "1202",
            "ubigeo": "120201"
          },
          {
            "name": "Aco",
            "sku": "02",
            "skuDepPro": "1202",
            "ubigeo": "120202"
          },
          {
            "name": "Andamarca",
            "sku": "03",
            "skuDepPro": "1202",
            "ubigeo": "120203"
          },
          {
            "name": "Chambara",
            "sku": "04",
            "skuDepPro": "1202",
            "ubigeo": "120204"
          },
          {
            "name": "Cochas",
            "sku": "05",
            "skuDepPro": "1202",
            "ubigeo": "120205"
          },
          {
            "name": "Comas",
            "sku": "06",
            "skuDepPro": "1202",
            "ubigeo": "120206"
          },
          {
            "name": "Heroinas Toledo",
            "sku": "07",
            "skuDepPro": "1202",
            "ubigeo": "120207"
          },
          {
            "name": "Manzanares",
            "sku": "08",
            "skuDepPro": "1202",
            "ubigeo": "120208"
          },
          {
            "name": "Mariscal Castilla",
            "sku": "09",
            "skuDepPro": "1202",
            "ubigeo": "120209"
          },
          {
            "name": "Matahuasi",
            "sku": "10",
            "skuDepPro": "1202",
            "ubigeo": "120210"
          },
          {
            "name": "Mito",
            "sku": "11",
            "skuDepPro": "1202",
            "ubigeo": "120211"
          },
          {
            "name": "Nueve de Julio",
            "sku": "12",
            "skuDepPro": "1202",
            "ubigeo": "120212"
          },
          {
            "name": "Orcotuna",
            "sku": "13",
            "skuDepPro": "1202",
            "ubigeo": "120213"
          },
          {
            "name": "San Jose de Quero",
            "sku": "14",
            "skuDepPro": "1202",
            "ubigeo": "120214"
          },
          {
            "name": "Santa Rosa de Ocopa",
            "sku": "15",
            "skuDepPro": "1202",
            "ubigeo": "120215"
          }
        ]
      },
      {
        "name": "Chanchamayo",
        "sku": "03",
        "skuDep": "12",
        "distritos": [
          {
            "name": "Chanchamayo",
            "sku": "01",
            "skuDepPro": "1203",
            "ubigeo": "120301"
          },
          {
            "name": "Perene",
            "sku": "02",
            "skuDepPro": "1203",
            "ubigeo": "120302"
          },
          {
            "name": "Pichanaqui",
            "sku": "03",
            "skuDepPro": "1203",
            "ubigeo": "120303"
          },
          {
            "name": "San Luis de Shuaro",
            "sku": "04",
            "skuDepPro": "1203",
            "ubigeo": "120304"
          },
          {
            "name": "San Ramon",
            "sku": "05",
            "skuDepPro": "1203",
            "ubigeo": "120305"
          },
          {
            "name": "Vitoc",
            "sku": "06",
            "skuDepPro": "1203",
            "ubigeo": "120306"
          }
        ]
      },
      {
        "name": "Jauja",
        "sku": "04",
        "skuDep": "12",
        "distritos": [
          {
            "name": "Jauja",
            "sku": "01",
            "skuDepPro": "1204",
            "ubigeo": "120401"
          },
          {
            "name": "Acolla",
            "sku": "02",
            "skuDepPro": "1204",
            "ubigeo": "120402"
          },
          {
            "name": "Apata",
            "sku": "03",
            "skuDepPro": "1204",
            "ubigeo": "120403"
          },
          {
            "name": "Ataura",
            "sku": "04",
            "skuDepPro": "1204",
            "ubigeo": "120404"
          },
          {
            "name": "Canchayllo",
            "sku": "05",
            "skuDepPro": "1204",
            "ubigeo": "120405"
          },
          {
            "name": "Curicaca",
            "sku": "06",
            "skuDepPro": "1204",
            "ubigeo": "120406"
          },
          {
            "name": "El Mantaro",
            "sku": "07",
            "skuDepPro": "1204",
            "ubigeo": "120407"
          },
          {
            "name": "Huamali",
            "sku": "08",
            "skuDepPro": "1204",
            "ubigeo": "120408"
          },
          {
            "name": "Huaripampa",
            "sku": "09",
            "skuDepPro": "1204",
            "ubigeo": "120409"
          },
          {
            "name": "Huertas",
            "sku": "10",
            "skuDepPro": "1204",
            "ubigeo": "120410"
          },
          {
            "name": "Janjaillo",
            "sku": "11",
            "skuDepPro": "1204",
            "ubigeo": "120411"
          },
          {
            "name": "Julcán",
            "sku": "12",
            "skuDepPro": "1204",
            "ubigeo": "120412"
          },
          {
            "name": "Leonor Ordoñez",
            "sku": "13",
            "skuDepPro": "1204",
            "ubigeo": "120413"
          },
          {
            "name": "Llocllapampa",
            "sku": "14",
            "skuDepPro": "1204",
            "ubigeo": "120414"
          },
          {
            "name": "Marco",
            "sku": "15",
            "skuDepPro": "1204",
            "ubigeo": "120415"
          },
          {
            "name": "Masma",
            "sku": "16",
            "skuDepPro": "1204",
            "ubigeo": "120416"
          },
          {
            "name": "Masma Chicche",
            "sku": "17",
            "skuDepPro": "1204",
            "ubigeo": "120417"
          },
          {
            "name": "Molinos",
            "sku": "18",
            "skuDepPro": "1204",
            "ubigeo": "120418"
          },
          {
            "name": "Monobamba",
            "sku": "19",
            "skuDepPro": "1204",
            "ubigeo": "120419"
          },
          {
            "name": "Muqui",
            "sku": "20",
            "skuDepPro": "1204",
            "ubigeo": "120420"
          },
          {
            "name": "Muquiyauyo",
            "sku": "21",
            "skuDepPro": "1204",
            "ubigeo": "120421"
          },
          {
            "name": "Paca",
            "sku": "22",
            "skuDepPro": "1204",
            "ubigeo": "120422"
          },
          {
            "name": "Paccha",
            "sku": "23",
            "skuDepPro": "1204",
            "ubigeo": "120423"
          },
          {
            "name": "Pancan",
            "sku": "24",
            "skuDepPro": "1204",
            "ubigeo": "120424"
          },
          {
            "name": "Parco",
            "sku": "25",
            "skuDepPro": "1204",
            "ubigeo": "120425"
          },
          {
            "name": "Pomacancha",
            "sku": "26",
            "skuDepPro": "1204",
            "ubigeo": "120426"
          },
          {
            "name": "Ricran",
            "sku": "27",
            "skuDepPro": "1204",
            "ubigeo": "120427"
          },
          {
            "name": "San Lorenzo",
            "sku": "28",
            "skuDepPro": "1204",
            "ubigeo": "120428"
          },
          {
            "name": "San Pedro de Chunan",
            "sku": "29",
            "skuDepPro": "1204",
            "ubigeo": "120429"
          },
          {
            "name": "Sausa",
            "sku": "30",
            "skuDepPro": "1204",
            "ubigeo": "120430"
          },
          {
            "name": "Sincos",
            "sku": "31",
            "skuDepPro": "1204",
            "ubigeo": "120431"
          },
          {
            "name": "Tunan Marca",
            "sku": "32",
            "skuDepPro": "1204",
            "ubigeo": "120432"
          },
          {
            "name": "Yauli",
            "sku": "33",
            "skuDepPro": "1204",
            "ubigeo": "120433"
          },
          {
            "name": "Yauyos",
            "sku": "34",
            "skuDepPro": "1204",
            "ubigeo": "120434"
          }
        ]
      },
      {
        "name": "Junín",
        "sku": "05",
        "skuDep": "12",
        "distritos": [
          {
            "name": "Junín",
            "sku": "01",
            "skuDepPro": "1205",
            "ubigeo": "120501"
          },
          {
            "name": "Carhuamayo",
            "sku": "02",
            "skuDepPro": "1205",
            "ubigeo": "120502"
          },
          {
            "name": "Ondores",
            "sku": "03",
            "skuDepPro": "1205",
            "ubigeo": "120503"
          },
          {
            "name": "Ulcumayo",
            "sku": "04",
            "skuDepPro": "1205",
            "ubigeo": "120504"
          }
        ]
      },
      {
        "name": "Satipo",
        "sku": "06",
        "skuDep": "12",
        "distritos": [
          {
            "name": "Satipo",
            "sku": "01",
            "skuDepPro": "1206",
            "ubigeo": "120601"
          },
          {
            "name": "Coviriali",
            "sku": "02",
            "skuDepPro": "1206",
            "ubigeo": "120602"
          },
          {
            "name": "Llaylla",
            "sku": "03",
            "skuDepPro": "1206",
            "ubigeo": "120603"
          },
          {
            "name": "Mazamari",
            "sku": "04",
            "skuDepPro": "1206",
            "ubigeo": "120604"
          },
          {
            "name": "Pampa Hermosa",
            "sku": "05",
            "skuDepPro": "1206",
            "ubigeo": "120605"
          },
          {
            "name": "Pangoa",
            "sku": "06",
            "skuDepPro": "1206",
            "ubigeo": "120606"
          },
          {
            "name": "Rio Negro",
            "sku": "07",
            "skuDepPro": "1206",
            "ubigeo": "120607"
          },
          {
            "name": "Rio Tambo",
            "sku": "08",
            "skuDepPro": "1206",
            "ubigeo": "120608"
          },
          {
            "name": "Vizcatan del Ene",
            "sku": "09",
            "skuDepPro": "1206",
            "ubigeo": "120609"
          }
        ]
      },
      {
        "name": "Tarma",
        "sku": "07",
        "skuDep": "12",
        "distritos": [
          {
            "name": "Tarma",
            "sku": "01",
            "skuDepPro": "1207",
            "ubigeo": "120701"
          },
          {
            "name": "Acobamba",
            "sku": "02",
            "skuDepPro": "1207",
            "ubigeo": "120702"
          },
          {
            "name": "Huaricolca",
            "sku": "03",
            "skuDepPro": "1207",
            "ubigeo": "120703"
          },
          {
            "name": "Huasahuasi",
            "sku": "04",
            "skuDepPro": "1207",
            "ubigeo": "120704"
          },
          {
            "name": "La Union",
            "sku": "05",
            "skuDepPro": "1207",
            "ubigeo": "120705"
          },
          {
            "name": "Palca",
            "sku": "06",
            "skuDepPro": "1207",
            "ubigeo": "120706"
          },
          {
            "name": "Palcamayo",
            "sku": "07",
            "skuDepPro": "1207",
            "ubigeo": "120707"
          },
          {
            "name": "San Pedro de Cajas",
            "sku": "08",
            "skuDepPro": "1207",
            "ubigeo": "120708"
          },
          {
            "name": "Tapo",
            "sku": "09",
            "skuDepPro": "1207",
            "ubigeo": "120709"
          }
        ]
      },
      {
        "name": "Yauli",
        "sku": "08",
        "skuDep": "12",
        "distritos": [
          {
            "name": "La Oroya",
            "sku": "01",
            "skuDepPro": "1208",
            "ubigeo": "120801"
          },
          {
            "name": "Chacapalpa",
            "sku": "02",
            "skuDepPro": "1208",
            "ubigeo": "120802"
          },
          {
            "name": "Huay-Huay",
            "sku": "03",
            "skuDepPro": "1208",
            "ubigeo": "120803"
          },
          {
            "name": "Marcapomacocha",
            "sku": "04",
            "skuDepPro": "1208",
            "ubigeo": "120804"
          },
          {
            "name": "Morococha",
            "sku": "05",
            "skuDepPro": "1208",
            "ubigeo": "120805"
          },
          {
            "name": "Paccha",
            "sku": "06",
            "skuDepPro": "1208",
            "ubigeo": "120806"
          },
          {
            "name": "Santa Barbara de Carhuacayan",
            "sku": "07",
            "skuDepPro": "1208",
            "ubigeo": "120807"
          },
          {
            "name": "Santa Rosa de Sacco",
            "sku": "08",
            "skuDepPro": "1208",
            "ubigeo": "120808"
          },
          {
            "name": "Suitucancha",
            "sku": "09",
            "skuDepPro": "1208",
            "ubigeo": "120809"
          },
          {
            "name": "Yauli",
            "sku": "10",
            "skuDepPro": "1208",
            "ubigeo": "120810"
          }
        ]
      },
      {
        "name": "Chupaca",
        "sku": "09",
        "skuDep": "12",
        "distritos": [
          {
            "name": "Chupaca",
            "sku": "01",
            "skuDepPro": "1209",
            "ubigeo": "120901"
          },
          {
            "name": "Ahuac",
            "sku": "02",
            "skuDepPro": "1209",
            "ubigeo": "120902"
          },
          {
            "name": "Chongos Bajo",
            "sku": "03",
            "skuDepPro": "1209",
            "ubigeo": "120903"
          },
          {
            "name": "Huachac",
            "sku": "04",
            "skuDepPro": "1209",
            "ubigeo": "120904"
          },
          {
            "name": "Huamancaca Chico",
            "sku": "05",
            "skuDepPro": "1209",
            "ubigeo": "120905"
          },
          {
            "name": "San Juan de Iscos",
            "sku": "06",
            "skuDepPro": "1209",
            "ubigeo": "120906"
          },
          {
            "name": "San Juan de Jarpa",
            "sku": "07",
            "skuDepPro": "1209",
            "ubigeo": "120907"
          },
          {
            "name": "Tres de Diciembre",
            "sku": "08",
            "skuDepPro": "1209",
            "ubigeo": "120908"
          },
          {
            "name": "Yanacancha",
            "sku": "09",
            "skuDepPro": "1209",
            "ubigeo": "120909"
          }
        ]
      }
    ]
  },
  {
    "name": "La Libertad",
    "sku": "13",
    "provincias": [
      {
        "name": "Trujillo",
        "sku": "01",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Trujillo",
            "sku": "01",
            "skuDepPro": "1301",
            "ubigeo": "130101"
          },
          {
            "name": "El Porvenir",
            "sku": "02",
            "skuDepPro": "1301",
            "ubigeo": "130102"
          },
          {
            "name": "Florencia de Mora",
            "sku": "03",
            "skuDepPro": "1301",
            "ubigeo": "130103"
          },
          {
            "name": "Huanchaco",
            "sku": "04",
            "skuDepPro": "1301",
            "ubigeo": "130104"
          },
          {
            "name": "La Esperanza",
            "sku": "05",
            "skuDepPro": "1301",
            "ubigeo": "130105"
          },
          {
            "name": "Laredo",
            "sku": "06",
            "skuDepPro": "1301",
            "ubigeo": "130106"
          },
          {
            "name": "Moche",
            "sku": "07",
            "skuDepPro": "1301",
            "ubigeo": "130107"
          },
          {
            "name": "Poroto",
            "sku": "08",
            "skuDepPro": "1301",
            "ubigeo": "130108"
          },
          {
            "name": "Salaverry",
            "sku": "09",
            "skuDepPro": "1301",
            "ubigeo": "130109"
          },
          {
            "name": "Simbal",
            "sku": "10",
            "skuDepPro": "1301",
            "ubigeo": "130110"
          },
          {
            "name": "Victor Larco Herrera",
            "sku": "11",
            "skuDepPro": "1301",
            "ubigeo": "130111"
          }
        ]
      },
      {
        "name": "Ascope",
        "sku": "02",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Ascope",
            "sku": "01",
            "skuDepPro": "1302",
            "ubigeo": "130201"
          },
          {
            "name": "Chicama",
            "sku": "02",
            "skuDepPro": "1302",
            "ubigeo": "130202"
          },
          {
            "name": "Chocope",
            "sku": "03",
            "skuDepPro": "1302",
            "ubigeo": "130203"
          },
          {
            "name": "Magdalena de Cao",
            "sku": "04",
            "skuDepPro": "1302",
            "ubigeo": "130204"
          },
          {
            "name": "Paijan",
            "sku": "05",
            "skuDepPro": "1302",
            "ubigeo": "130205"
          },
          {
            "name": "Razuri",
            "sku": "06",
            "skuDepPro": "1302",
            "ubigeo": "130206"
          },
          {
            "name": "Santiago de Cao",
            "sku": "07",
            "skuDepPro": "1302",
            "ubigeo": "130207"
          },
          {
            "name": "Casa Grande",
            "sku": "08",
            "skuDepPro": "1302",
            "ubigeo": "130208"
          }
        ]
      },
      {
        "name": "Bolívar",
        "sku": "03",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Bolívar",
            "sku": "01",
            "skuDepPro": "1303",
            "ubigeo": "130301"
          },
          {
            "name": "Bambamarca",
            "sku": "02",
            "skuDepPro": "1303",
            "ubigeo": "130302"
          },
          {
            "name": "Condormarca",
            "sku": "03",
            "skuDepPro": "1303",
            "ubigeo": "130303"
          },
          {
            "name": "Longotea",
            "sku": "04",
            "skuDepPro": "1303",
            "ubigeo": "130304"
          },
          {
            "name": "Uchumarca",
            "sku": "05",
            "skuDepPro": "1303",
            "ubigeo": "130305"
          },
          {
            "name": "Ucuncha",
            "sku": "06",
            "skuDepPro": "1303",
            "ubigeo": "130306"
          }
        ]
      },
      {
        "name": "Chepén",
        "sku": "04",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Chepén",
            "sku": "01",
            "skuDepPro": "1304",
            "ubigeo": "130401"
          },
          {
            "name": "Pacanga",
            "sku": "02",
            "skuDepPro": "1304",
            "ubigeo": "130402"
          },
          {
            "name": "Pueblo Nuevo",
            "sku": "03",
            "skuDepPro": "1304",
            "ubigeo": "130403"
          }
        ]
      },
      {
        "name": "Julcán",
        "sku": "05",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Julcán",
            "sku": "01",
            "skuDepPro": "1305",
            "ubigeo": "130501"
          },
          {
            "name": "Calamarca",
            "sku": "02",
            "skuDepPro": "1305",
            "ubigeo": "130502"
          },
          {
            "name": "Carabamba",
            "sku": "03",
            "skuDepPro": "1305",
            "ubigeo": "130503"
          },
          {
            "name": "Huaso",
            "sku": "04",
            "skuDepPro": "1305",
            "ubigeo": "130504"
          }
        ]
      },
      {
        "name": "Otuzco",
        "sku": "06",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Otuzco",
            "sku": "01",
            "skuDepPro": "1306",
            "ubigeo": "130601"
          },
          {
            "name": "Agallpampa",
            "sku": "02",
            "skuDepPro": "1306",
            "ubigeo": "130602"
          },
          {
            "name": "Charat",
            "sku": "04",
            "skuDepPro": "1306",
            "ubigeo": "130604"
          },
          {
            "name": "Huaranchal",
            "sku": "05",
            "skuDepPro": "1306",
            "ubigeo": "130605"
          },
          {
            "name": "La Cuesta",
            "sku": "06",
            "skuDepPro": "1306",
            "ubigeo": "130606"
          },
          {
            "name": "Mache",
            "sku": "08",
            "skuDepPro": "1306",
            "ubigeo": "130608"
          },
          {
            "name": "Paranday",
            "sku": "10",
            "skuDepPro": "1306",
            "ubigeo": "130610"
          },
          {
            "name": "Salpo",
            "sku": "11",
            "skuDepPro": "1306",
            "ubigeo": "130611"
          },
          {
            "name": "Sinsicap",
            "sku": "13",
            "skuDepPro": "1306",
            "ubigeo": "130613"
          },
          {
            "name": "Usquil",
            "sku": "14",
            "skuDepPro": "1306",
            "ubigeo": "130614"
          }
        ]
      },
      {
        "name": "Pacasmayo",
        "sku": "07",
        "skuDep": "13",
        "distritos": [
          {
            "name": "San Pedro de Lloc",
            "sku": "01",
            "skuDepPro": "1307",
            "ubigeo": "130701"
          },
          {
            "name": "Guadalupe",
            "sku": "02",
            "skuDepPro": "1307",
            "ubigeo": "130702"
          },
          {
            "name": "Jequetepeque",
            "sku": "03",
            "skuDepPro": "1307",
            "ubigeo": "130703"
          },
          {
            "name": "Pacasmayo",
            "sku": "04",
            "skuDepPro": "1307",
            "ubigeo": "130704"
          },
          {
            "name": "San Jose",
            "sku": "05",
            "skuDepPro": "1307",
            "ubigeo": "130705"
          }
        ]
      },
      {
        "name": "Pataz",
        "sku": "08",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Tayabamba",
            "sku": "01",
            "skuDepPro": "1308",
            "ubigeo": "130801"
          },
          {
            "name": "Buldibuyo",
            "sku": "02",
            "skuDepPro": "1308",
            "ubigeo": "130802"
          },
          {
            "name": "Chillia",
            "sku": "03",
            "skuDepPro": "1308",
            "ubigeo": "130803"
          },
          {
            "name": "Huancaspata",
            "sku": "04",
            "skuDepPro": "1308",
            "ubigeo": "130804"
          },
          {
            "name": "Huaylillas",
            "sku": "05",
            "skuDepPro": "1308",
            "ubigeo": "130805"
          },
          {
            "name": "Huayo",
            "sku": "06",
            "skuDepPro": "1308",
            "ubigeo": "130806"
          },
          {
            "name": "Ongon",
            "sku": "07",
            "skuDepPro": "1308",
            "ubigeo": "130807"
          },
          {
            "name": "Parcoy",
            "sku": "08",
            "skuDepPro": "1308",
            "ubigeo": "130808"
          },
          {
            "name": "Pataz",
            "sku": "09",
            "skuDepPro": "1308",
            "ubigeo": "130809"
          },
          {
            "name": "Pias",
            "sku": "10",
            "skuDepPro": "1308",
            "ubigeo": "130810"
          },
          {
            "name": "Santiago de Challas",
            "sku": "11",
            "skuDepPro": "1308",
            "ubigeo": "130811"
          },
          {
            "name": "Taurija",
            "sku": "12",
            "skuDepPro": "1308",
            "ubigeo": "130812"
          },
          {
            "name": "Urpay",
            "sku": "13",
            "skuDepPro": "1308",
            "ubigeo": "130813"
          }
        ]
      },
      {
        "name": "Sánchez Carrión",
        "sku": "09",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Huamachuco",
            "sku": "01",
            "skuDepPro": "1309",
            "ubigeo": "130901"
          },
          {
            "name": "Chugay",
            "sku": "02",
            "skuDepPro": "1309",
            "ubigeo": "130902"
          },
          {
            "name": "Cochorco",
            "sku": "03",
            "skuDepPro": "1309",
            "ubigeo": "130903"
          },
          {
            "name": "Curgos",
            "sku": "04",
            "skuDepPro": "1309",
            "ubigeo": "130904"
          },
          {
            "name": "Marcabal",
            "sku": "05",
            "skuDepPro": "1309",
            "ubigeo": "130905"
          },
          {
            "name": "Sanagoran",
            "sku": "06",
            "skuDepPro": "1309",
            "ubigeo": "130906"
          },
          {
            "name": "Sarin",
            "sku": "07",
            "skuDepPro": "1309",
            "ubigeo": "130907"
          },
          {
            "name": "Sartimbamba",
            "sku": "08",
            "skuDepPro": "1309",
            "ubigeo": "130908"
          }
        ]
      },
      {
        "name": "Santiago de Chuco",
        "sku": "10",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Santiago de Chuco",
            "sku": "01",
            "skuDepPro": "1310",
            "ubigeo": "131001"
          },
          {
            "name": "Angasmarca",
            "sku": "02",
            "skuDepPro": "1310",
            "ubigeo": "131002"
          },
          {
            "name": "Cachicadan",
            "sku": "03",
            "skuDepPro": "1310",
            "ubigeo": "131003"
          },
          {
            "name": "Mollebamba",
            "sku": "04",
            "skuDepPro": "1310",
            "ubigeo": "131004"
          },
          {
            "name": "Mollepata",
            "sku": "05",
            "skuDepPro": "1310",
            "ubigeo": "131005"
          },
          {
            "name": "Quiruvilca",
            "sku": "06",
            "skuDepPro": "1310",
            "ubigeo": "131006"
          },
          {
            "name": "Santa Cruz de Chuca",
            "sku": "07",
            "skuDepPro": "1310",
            "ubigeo": "131007"
          },
          {
            "name": "Sitabamba",
            "sku": "08",
            "skuDepPro": "1310",
            "ubigeo": "131008"
          }
        ]
      },
      {
        "name": "Gran Chimú",
        "sku": "11",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Cascas",
            "sku": "01",
            "skuDepPro": "1311",
            "ubigeo": "131101"
          },
          {
            "name": "Lucma",
            "sku": "02",
            "skuDepPro": "1311",
            "ubigeo": "131102"
          },
          {
            "name": "Marmot",
            "sku": "03",
            "skuDepPro": "1311",
            "ubigeo": "131103"
          },
          {
            "name": "Sayapullo",
            "sku": "04",
            "skuDepPro": "1311",
            "ubigeo": "131104"
          }
        ]
      },
      {
        "name": "Virú",
        "sku": "12",
        "skuDep": "13",
        "distritos": [
          {
            "name": "Virú",
            "sku": "01",
            "skuDepPro": "1312",
            "ubigeo": "131201"
          },
          {
            "name": "Chao",
            "sku": "02",
            "skuDepPro": "1312",
            "ubigeo": "131202"
          },
          {
            "name": "Guadalupito",
            "sku": "03",
            "skuDepPro": "1312",
            "ubigeo": "131203"
          }
        ]
      }
    ]
  },
  {
    "name": "Lambayeque",
    "sku": "14",
    "provincias": [
      {
        "name": "Chiclayo",
        "sku": "01",
        "skuDep": "14",
        "distritos": [
          {
            "name": "Chiclayo",
            "sku": "01",
            "skuDepPro": "1401",
            "ubigeo": "140101"
          },
          {
            "name": "Chongoyape",
            "sku": "02",
            "skuDepPro": "1401",
            "ubigeo": "140102"
          },
          {
            "name": "Eten",
            "sku": "03",
            "skuDepPro": "1401",
            "ubigeo": "140103"
          },
          {
            "name": "Eten Puerto",
            "sku": "04",
            "skuDepPro": "1401",
            "ubigeo": "140104"
          },
          {
            "name": "Jose Leonardo Ortiz",
            "sku": "05",
            "skuDepPro": "1401",
            "ubigeo": "140105"
          },
          {
            "name": "La Victoria",
            "sku": "06",
            "skuDepPro": "1401",
            "ubigeo": "140106"
          },
          {
            "name": "Lagunas",
            "sku": "07",
            "skuDepPro": "1401",
            "ubigeo": "140107"
          },
          {
            "name": "Monsefu",
            "sku": "08",
            "skuDepPro": "1401",
            "ubigeo": "140108"
          },
          {
            "name": "Nueva Arica",
            "sku": "09",
            "skuDepPro": "1401",
            "ubigeo": "140109"
          },
          {
            "name": "Oyotun",
            "sku": "10",
            "skuDepPro": "1401",
            "ubigeo": "140110"
          },
          {
            "name": "Picsi",
            "sku": "11",
            "skuDepPro": "1401",
            "ubigeo": "140111"
          },
          {
            "name": "Pimentel",
            "sku": "12",
            "skuDepPro": "1401",
            "ubigeo": "140112"
          },
          {
            "name": "Reque",
            "sku": "13",
            "skuDepPro": "1401",
            "ubigeo": "140113"
          },
          {
            "name": "Santa Rosa",
            "sku": "14",
            "skuDepPro": "1401",
            "ubigeo": "140114"
          },
          {
            "name": "Saña",
            "sku": "15",
            "skuDepPro": "1401",
            "ubigeo": "140115"
          },
          {
            "name": "Cayalti",
            "sku": "16",
            "skuDepPro": "1401",
            "ubigeo": "140116"
          },
          {
            "name": "Patapo",
            "sku": "17",
            "skuDepPro": "1401",
            "ubigeo": "140117"
          },
          {
            "name": "Pomalca",
            "sku": "18",
            "skuDepPro": "1401",
            "ubigeo": "140118"
          },
          {
            "name": "Pucala",
            "sku": "19",
            "skuDepPro": "1401",
            "ubigeo": "140119"
          },
          {
            "name": "Tuman",
            "sku": "20",
            "skuDepPro": "1401",
            "ubigeo": "140120"
          }
        ]
      },
      {
        "name": "Ferreñafe",
        "sku": "02",
        "skuDep": "14",
        "distritos": [
          {
            "name": "Ferreñafe",
            "sku": "01",
            "skuDepPro": "1402",
            "ubigeo": "140201"
          },
          {
            "name": "Cañaris",
            "sku": "02",
            "skuDepPro": "1402",
            "ubigeo": "140202"
          },
          {
            "name": "Incahuasi",
            "sku": "03",
            "skuDepPro": "1402",
            "ubigeo": "140203"
          },
          {
            "name": "Manuel Antonio Mesones Muro",
            "sku": "04",
            "skuDepPro": "1402",
            "ubigeo": "140204"
          },
          {
            "name": "Pitipo",
            "sku": "05",
            "skuDepPro": "1402",
            "ubigeo": "140205"
          },
          {
            "name": "Pueblo Nuevo",
            "sku": "06",
            "skuDepPro": "1402",
            "ubigeo": "140206"
          }
        ]
      },
      {
        "name": "Lambayeque",
        "sku": "03",
        "skuDep": "14",
        "distritos": [
          {
            "name": "Lambayeque",
            "sku": "01",
            "skuDepPro": "1403",
            "ubigeo": "140301"
          },
          {
            "name": "Chochope",
            "sku": "02",
            "skuDepPro": "1403",
            "ubigeo": "140302"
          },
          {
            "name": "Illimo",
            "sku": "03",
            "skuDepPro": "1403",
            "ubigeo": "140303"
          },
          {
            "name": "Jayanca",
            "sku": "04",
            "skuDepPro": "1403",
            "ubigeo": "140304"
          },
          {
            "name": "Mochumi",
            "sku": "05",
            "skuDepPro": "1403",
            "ubigeo": "140305"
          },
          {
            "name": "Morrope",
            "sku": "06",
            "skuDepPro": "1403",
            "ubigeo": "140306"
          },
          {
            "name": "Motupe",
            "sku": "07",
            "skuDepPro": "1403",
            "ubigeo": "140307"
          },
          {
            "name": "Olmos",
            "sku": "08",
            "skuDepPro": "1403",
            "ubigeo": "140308"
          },
          {
            "name": "Pacora",
            "sku": "09",
            "skuDepPro": "1403",
            "ubigeo": "140309"
          },
          {
            "name": "Salas",
            "sku": "10",
            "skuDepPro": "1403",
            "ubigeo": "140310"
          },
          {
            "name": "San Jose",
            "sku": "11",
            "skuDepPro": "1403",
            "ubigeo": "140311"
          },
          {
            "name": "Tucume",
            "sku": "12",
            "skuDepPro": "1403",
            "ubigeo": "140312"
          }
        ]
      }
    ]
  },
  {
    "name": "Lima",
    "sku": "15",
    "provincias": [
      {
        "name": "Lima",
        "sku": "01",
        "skuDep": "15",
        "distritos": [
          {
            "name": "Lima",
            "sku": "01",
            "skuDepPro": "1501",
            "ubigeo": "150101"
          },
          {
            "name": "Ancon",
            "sku": "02",
            "skuDepPro": "1501",
            "ubigeo": "150102"
          },
          {
            "name": "Ate",
            "sku": "03",
            "skuDepPro": "1501",
            "ubigeo": "150103"
          },
          {
            "name": "Barranco",
            "sku": "04",
            "skuDepPro": "1501",
            "ubigeo": "150104"
          },
          {
            "name": "Breña",
            "sku": "05",
            "skuDepPro": "1501",
            "ubigeo": "150105"
          },
          {
            "name": "Carabayllo",
            "sku": "06",
            "skuDepPro": "1501",
            "ubigeo": "150106"
          },
          {
            "name": "Chaclacayo",
            "sku": "07",
            "skuDepPro": "1501",
            "ubigeo": "150107"
          },
          {
            "name": "Chorrillos",
            "sku": "08",
            "skuDepPro": "1501",
            "ubigeo": "150108"
          },
          {
            "name": "Cieneguilla",
            "sku": "09",
            "skuDepPro": "1501",
            "ubigeo": "150109"
          },
          {
            "name": "Comas",
            "sku": "10",
            "skuDepPro": "1501",
            "ubigeo": "150110"
          },
          {
            "name": "El Agustino",
            "sku": "11",
            "skuDepPro": "1501",
            "ubigeo": "150111"
          },
          {
            "name": "Independencia",
            "sku": "12",
            "skuDepPro": "1501",
            "ubigeo": "150112"
          },
          {
            "name": "Jesus Maria",
            "sku": "13",
            "skuDepPro": "1501",
            "ubigeo": "150113"
          },
          {
            "name": "La Molina",
            "sku": "14",
            "skuDepPro": "1501",
            "ubigeo": "150114"
          },
          {
            "name": "La Victoria",
            "sku": "15",
            "skuDepPro": "1501",
            "ubigeo": "150115"
          },
          {
            "name": "Lince",
            "sku": "16",
            "skuDepPro": "1501",
            "ubigeo": "150116"
          },
          {
            "name": "Los Olivos",
            "sku": "17",
            "skuDepPro": "1501",
            "ubigeo": "150117"
          },
          {
            "name": "Lurigancho",
            "sku": "18",
            "skuDepPro": "1501",
            "ubigeo": "150118"
          },
          {
            "name": "Lurin",
            "sku": "19",
            "skuDepPro": "1501",
            "ubigeo": "150119"
          },
          {
            "name": "Magdalena del Mar",
            "sku": "20",
            "skuDepPro": "1501",
            "ubigeo": "150120"
          },
          {
            "name": "Pueblo Libre",
            "sku": "21",
            "skuDepPro": "1501",
            "ubigeo": "150121"
          },
          {
            "name": "Miraflores",
            "sku": "22",
            "skuDepPro": "1501",
            "ubigeo": "150122"
          },
          {
            "name": "Pachacamac",
            "sku": "23",
            "skuDepPro": "1501",
            "ubigeo": "150123"
          },
          {
            "name": "Pucusana",
            "sku": "24",
            "skuDepPro": "1501",
            "ubigeo": "150124"
          },
          {
            "name": "Puente Piedra",
            "sku": "25",
            "skuDepPro": "1501",
            "ubigeo": "150125"
          },
          {
            "name": "Punta Hermosa",
            "sku": "26",
            "skuDepPro": "1501",
            "ubigeo": "150126"
          },
          {
            "name": "Punta Negra",
            "sku": "27",
            "skuDepPro": "1501",
            "ubigeo": "150127"
          },
          {
            "name": "Rimac",
            "sku": "28",
            "skuDepPro": "1501",
            "ubigeo": "150128"
          },
          {
            "name": "San Bartolo",
            "sku": "29",
            "skuDepPro": "1501",
            "ubigeo": "150129"
          },
          {
            "name": "San Borja",
            "sku": "30",
            "skuDepPro": "1501",
            "ubigeo": "150130"
          },
          {
            "name": "San Isidro",
            "sku": "31",
            "skuDepPro": "1501",
            "ubigeo": "150131"
          },
          {
            "name": "San Juan de Lurigancho",
            "sku": "32",
            "skuDepPro": "1501",
            "ubigeo": "150132"
          },
          {
            "name": "San Juan de Miraflores",
            "sku": "33",
            "skuDepPro": "1501",
            "ubigeo": "150133"
          },
          {
            "name": "San Luis",
            "sku": "34",
            "skuDepPro": "1501",
            "ubigeo": "150134"
          },
          {
            "name": "San Martín de Porres",
            "sku": "35",
            "skuDepPro": "1501",
            "ubigeo": "150135"
          },
          {
            "name": "San Miguel",
            "sku": "36",
            "skuDepPro": "1501",
            "ubigeo": "150136"
          },
          {
            "name": "Santa Anita",
            "sku": "37",
            "skuDepPro": "1501",
            "ubigeo": "150137"
          },
          {
            "name": "Santa Maria del Mar",
            "sku": "38",
            "skuDepPro": "1501",
            "ubigeo": "150138"
          },
          {
            "name": "Santa Rosa",
            "sku": "39",
            "skuDepPro": "1501",
            "ubigeo": "150139"
          },
          {
            "name": "Santiago de Surco",
            "sku": "40",
            "skuDepPro": "1501",
            "ubigeo": "150140"
          },
          {
            "name": "Surquillo",
            "sku": "41",
            "skuDepPro": "1501",
            "ubigeo": "150141"
          },
          {
            "name": "Villa El Salvador",
            "sku": "42",
            "skuDepPro": "1501",
            "ubigeo": "150142"
          },
          {
            "name": "Villa Maria del Triunfo",
            "sku": "43",
            "skuDepPro": "1501",
            "ubigeo": "150143"
          }
        ]
      },
      {
        "name": "Barranca",
        "sku": "02",
        "skuDep": "15",
        "distritos": [
          {
            "name": "Barranca",
            "sku": "01",
            "skuDepPro": "1502",
            "ubigeo": "150201"
          },
          {
            "name": "Paramonga",
            "sku": "02",
            "skuDepPro": "1502",
            "ubigeo": "150202"
          },
          {
            "name": "Pativilca",
            "sku": "03",
            "skuDepPro": "1502",
            "ubigeo": "150203"
          },
          {
            "name": "Supe",
            "sku": "04",
            "skuDepPro": "1502",
            "ubigeo": "150204"
          },
          {
            "name": "Supe Puerto",
            "sku": "05",
            "skuDepPro": "1502",
            "ubigeo": "150205"
          }
        ]
      },
      {
        "name": "Cajatambo",
        "sku": "03",
        "skuDep": "15",
        "distritos": [
          {
            "name": "Cajatambo",
            "sku": "01",
            "skuDepPro": "1503",
            "ubigeo": "150301"
          },
          {
            "name": "Copa",
            "sku": "02",
            "skuDepPro": "1503",
            "ubigeo": "150302"
          },
          {
            "name": "Gorgor",
            "sku": "03",
            "skuDepPro": "1503",
            "ubigeo": "150303"
          },
          {
            "name": "Huancapon",
            "sku": "04",
            "skuDepPro": "1503",
            "ubigeo": "150304"
          },
          {
            "name": "Manas",
            "sku": "05",
            "skuDepPro": "1503",
            "ubigeo": "150305"
          }
        ]
      },
      {
        "name": "Canta",
        "sku": "04",
        "skuDep": "15",
        "distritos": [
          {
            "name": "Canta",
            "sku": "01",
            "skuDepPro": "1504",
            "ubigeo": "150401"
          },
          {
            "name": "Arahuay",
            "sku": "02",
            "skuDepPro": "1504",
            "ubigeo": "150402"
          },
          {
            "name": "Huamantanga",
            "sku": "03",
            "skuDepPro": "1504",
            "ubigeo": "150403"
          },
          {
            "name": "Huaros",
            "sku": "04",
            "skuDepPro": "1504",
            "ubigeo": "150404"
          },
          {
            "name": "Lachaqui",
            "sku": "05",
            "skuDepPro": "1504",
            "ubigeo": "150405"
          },
          {
            "name": "San Buenaventura",
            "sku": "06",
            "skuDepPro": "1504",
            "ubigeo": "150406"
          },
          {
            "name": "Santa Rosa de Quives",
            "sku": "07",
            "skuDepPro": "1504",
            "ubigeo": "150407"
          }
        ]
      },
      {
        "name": "Cañete",
        "sku": "05",
        "skuDep": "15",
        "distritos": [
          {
            "name": "San Vicente de Cañete",
            "sku": "01",
            "skuDepPro": "1505",
            "ubigeo": "150501"
          },
          {
            "name": "Asia",
            "sku": "02",
            "skuDepPro": "1505",
            "ubigeo": "150502"
          },
          {
            "name": "Calango",
            "sku": "03",
            "skuDepPro": "1505",
            "ubigeo": "150503"
          },
          {
            "name": "Cerro Azul",
            "sku": "04",
            "skuDepPro": "1505",
            "ubigeo": "150504"
          },
          {
            "name": "Chilca",
            "sku": "05",
            "skuDepPro": "1505",
            "ubigeo": "150505"
          },
          {
            "name": "Coayllo",
            "sku": "06",
            "skuDepPro": "1505",
            "ubigeo": "150506"
          },
          {
            "name": "Imperial",
            "sku": "07",
            "skuDepPro": "1505",
            "ubigeo": "150507"
          },
          {
            "name": "Lunahuana",
            "sku": "08",
            "skuDepPro": "1505",
            "ubigeo": "150508"
          },
          {
            "name": "Mala",
            "sku": "09",
            "skuDepPro": "1505",
            "ubigeo": "150509"
          },
          {
            "name": "Nuevo Imperial",
            "sku": "10",
            "skuDepPro": "1505",
            "ubigeo": "150510"
          },
          {
            "name": "Pacaran",
            "sku": "11",
            "skuDepPro": "1505",
            "ubigeo": "150511"
          },
          {
            "name": "Quilmana",
            "sku": "12",
            "skuDepPro": "1505",
            "ubigeo": "150512"
          },
          {
            "name": "San Antonio",
            "sku": "13",
            "skuDepPro": "1505",
            "ubigeo": "150513"
          },
          {
            "name": "San Luis",
            "sku": "14",
            "skuDepPro": "1505",
            "ubigeo": "150514"
          },
          {
            "name": "Santa Cruz de Flores",
            "sku": "15",
            "skuDepPro": "1505",
            "ubigeo": "150515"
          },
          {
            "name": "Zuñiga",
            "sku": "16",
            "skuDepPro": "1505",
            "ubigeo": "150516"
          }
        ]
      },
      {
        "name": "Huaral",
        "sku": "06",
        "skuDep": "15",
        "distritos": [
          {
            "name": "Huaral",
            "sku": "01",
            "skuDepPro": "1506",
            "ubigeo": "150601"
          },
          {
            "name": "Atavillos Alto",
            "sku": "02",
            "skuDepPro": "1506",
            "ubigeo": "150602"
          },
          {
            "name": "Atavillos Bajo",
            "sku": "03",
            "skuDepPro": "1506",
            "ubigeo": "150603"
          },
          {
            "name": "Aucallama",
            "sku": "04",
            "skuDepPro": "1506",
            "ubigeo": "150604"
          },
          {
            "name": "Chancay",
            "sku": "05",
            "skuDepPro": "1506",
            "ubigeo": "150605"
          },
          {
            "name": "Ihuari",
            "sku": "06",
            "skuDepPro": "1506",
            "ubigeo": "150606"
          },
          {
            "name": "Lampian",
            "sku": "07",
            "skuDepPro": "1506",
            "ubigeo": "150607"
          },
          {
            "name": "Pacaraos",
            "sku": "08",
            "skuDepPro": "1506",
            "ubigeo": "150608"
          },
          {
            "name": "San Miguel de Acos",
            "sku": "09",
            "skuDepPro": "1506",
            "ubigeo": "150609"
          },
          {
            "name": "Santa Cruz de Andamarca",
            "sku": "10",
            "skuDepPro": "1506",
            "ubigeo": "150610"
          },
          {
            "name": "Sumbilca",
            "sku": "11",
            "skuDepPro": "1506",
            "ubigeo": "150611"
          },
          {
            "name": "Veintisiete de Noviembre",
            "sku": "12",
            "skuDepPro": "1506",
            "ubigeo": "150612"
          }
        ]
      },
      {
        "name": "Huarochirí",
        "sku": "07",
        "skuDep": "15",
        "distritos": [
          {
            "name": "Matucana",
            "sku": "01",
            "skuDepPro": "1507",
            "ubigeo": "150701"
          },
          {
            "name": "Antioquia",
            "sku": "02",
            "skuDepPro": "1507",
            "ubigeo": "150702"
          },
          {
            "name": "Callahuanca",
            "sku": "03",
            "skuDepPro": "1507",
            "ubigeo": "150703"
          },
          {
            "name": "Carampoma",
            "sku": "04",
            "skuDepPro": "1507",
            "ubigeo": "150704"
          },
          {
            "name": "Chicla",
            "sku": "05",
            "skuDepPro": "1507",
            "ubigeo": "150705"
          },
          {
            "name": "Cuenca",
            "sku": "06",
            "skuDepPro": "1507",
            "ubigeo": "150706"
          },
          {
            "name": "Huachupampa",
            "sku": "07",
            "skuDepPro": "1507",
            "ubigeo": "150707"
          },
          {
            "name": "Huanza",
            "sku": "08",
            "skuDepPro": "1507",
            "ubigeo": "150708"
          },
          {
            "name": "Huarochirí",
            "sku": "09",
            "skuDepPro": "1507",
            "ubigeo": "150709"
          },
          {
            "name": "Lahuaytambo",
            "sku": "10",
            "skuDepPro": "1507",
            "ubigeo": "150710"
          },
          {
            "name": "Langa",
            "sku": "11",
            "skuDepPro": "1507",
            "ubigeo": "150711"
          },
          {
            "name": "Laraos",
            "sku": "12",
            "skuDepPro": "1507",
            "ubigeo": "150712"
          },
          {
            "name": "Mariatana",
            "sku": "13",
            "skuDepPro": "1507",
            "ubigeo": "150713"
          },
          {
            "name": "Ricardo Palma",
            "sku": "14",
            "skuDepPro": "1507",
            "ubigeo": "150714"
          },
          {
            "name": "San Andres de Tupicocha",
            "sku": "15",
            "skuDepPro": "1507",
            "ubigeo": "150715"
          },
          {
            "name": "San Antonio",
            "sku": "16",
            "skuDepPro": "1507",
            "ubigeo": "150716"
          },
          {
            "name": "San Bartolome",
            "sku": "17",
            "skuDepPro": "1507",
            "ubigeo": "150717"
          },
          {
            "name": "San Damian",
            "sku": "18",
            "skuDepPro": "1507",
            "ubigeo": "150718"
          },
          {
            "name": "San Juan de Iris",
            "sku": "19",
            "skuDepPro": "1507",
            "ubigeo": "150719"
          },
          {
            "name": "San Juan de Tantaranche",
            "sku": "20",
            "skuDepPro": "1507",
            "ubigeo": "150720"
          },
          {
            "name": "San Lorenzo de Quinti",
            "sku": "21",
            "skuDepPro": "1507",
            "ubigeo": "150721"
          },
          {
            "name": "San Mateo",
            "sku": "22",
            "skuDepPro": "1507",
            "ubigeo": "150722"
          },
          {
            "name": "San Mateo de Otao",
            "sku": "23",
            "skuDepPro": "1507",
            "ubigeo": "150723"
          },
          {
            "name": "San Pedro de Casta",
            "sku": "24",
            "skuDepPro": "1507",
            "ubigeo": "150724"
          },
          {
            "name": "San Pedro de Huancayre",
            "sku": "25",
            "skuDepPro": "1507",
            "ubigeo": "150725"
          },
          {
            "name": "Sangallaya",
            "sku": "26",
            "skuDepPro": "1507",
            "ubigeo": "150726"
          },
          {
            "name": "Santa Cruz de Cocachacra",
            "sku": "27",
            "skuDepPro": "1507",
            "ubigeo": "150727"
          },
          {
            "name": "Santa Eulalia",
            "sku": "28",
            "skuDepPro": "1507",
            "ubigeo": "150728"
          },
          {
            "name": "Santiago de Anchucaya",
            "sku": "29",
            "skuDepPro": "1507",
            "ubigeo": "150729"
          },
          {
            "name": "Santiago de Tuna",
            "sku": "30",
            "skuDepPro": "1507",
            "ubigeo": "150730"
          },
          {
            "name": "Santo Domingo de Los Olleros",
            "sku": "31",
            "skuDepPro": "1507",
            "ubigeo": "150731"
          },
          {
            "name": "Surco",
            "sku": "32",
            "skuDepPro": "1507",
            "ubigeo": "150732"
          }
        ]
      },
      {
        "name": "Huaura",
        "sku": "08",
        "skuDep": "15",
        "distritos": [
          {
            "name": "Huacho",
            "sku": "01",
            "skuDepPro": "1508",
            "ubigeo": "150801"
          },
          {
            "name": "Ambar",
            "sku": "02",
            "skuDepPro": "1508",
            "ubigeo": "150802"
          },
          {
            "name": "Caleta de Carquin",
            "sku": "03",
            "skuDepPro": "1508",
            "ubigeo": "150803"
          },
          {
            "name": "Checras",
            "sku": "04",
            "skuDepPro": "1508",
            "ubigeo": "150804"
          },
          {
            "name": "Hualmay",
            "sku": "05",
            "skuDepPro": "1508",
            "ubigeo": "150805"
          },
          {
            "name": "Huaura",
            "sku": "06",
            "skuDepPro": "1508",
            "ubigeo": "150806"
          },
          {
            "name": "Leoncio Prado",
            "sku": "07",
            "skuDepPro": "1508",
            "ubigeo": "150807"
          },
          {
            "name": "Paccho",
            "sku": "08",
            "skuDepPro": "1508",
            "ubigeo": "150808"
          },
          {
            "name": "Santa Leonor",
            "sku": "09",
            "skuDepPro": "1508",
            "ubigeo": "150809"
          },
          {
            "name": "Santa Maria",
            "sku": "10",
            "skuDepPro": "1508",
            "ubigeo": "150810"
          },
          {
            "name": "Sayan",
            "sku": "11",
            "skuDepPro": "1508",
            "ubigeo": "150811"
          },
          {
            "name": "Vegueta",
            "sku": "12",
            "skuDepPro": "1508",
            "ubigeo": "150812"
          }
        ]
      },
      {
        "name": "Oyón",
        "sku": "09",
        "skuDep": "15",
        "distritos": [
          {
            "name": "Oyón",
            "sku": "01",
            "skuDepPro": "1509",
            "ubigeo": "150901"
          },
          {
            "name": "Andajes",
            "sku": "02",
            "skuDepPro": "1509",
            "ubigeo": "150902"
          },
          {
            "name": "Caujul",
            "sku": "03",
            "skuDepPro": "1509",
            "ubigeo": "150903"
          },
          {
            "name": "Cochamarca",
            "sku": "04",
            "skuDepPro": "1509",
            "ubigeo": "150904"
          },
          {
            "name": "Navan",
            "sku": "05",
            "skuDepPro": "1509",
            "ubigeo": "150905"
          },
          {
            "name": "Pachangara",
            "sku": "06",
            "skuDepPro": "1509",
            "ubigeo": "150906"
          }
        ]
      },
      {
        "name": "Yauyos",
        "sku": "10",
        "skuDep": "15",
        "distritos": [
          {
            "name": "Yauyos",
            "sku": "01",
            "skuDepPro": "1510",
            "ubigeo": "151001"
          },
          {
            "name": "Alis",
            "sku": "02",
            "skuDepPro": "1510",
            "ubigeo": "151002"
          },
          {
            "name": "Allauca",
            "sku": "03",
            "skuDepPro": "1510",
            "ubigeo": "151003"
          },
          {
            "name": "Ayaviri",
            "sku": "04",
            "skuDepPro": "1510",
            "ubigeo": "151004"
          },
          {
            "name": "Azángaro",
            "sku": "05",
            "skuDepPro": "1510",
            "ubigeo": "151005"
          },
          {
            "name": "Cacra",
            "sku": "06",
            "skuDepPro": "1510",
            "ubigeo": "151006"
          },
          {
            "name": "Carania",
            "sku": "07",
            "skuDepPro": "1510",
            "ubigeo": "151007"
          },
          {
            "name": "Catahuasi",
            "sku": "08",
            "skuDepPro": "1510",
            "ubigeo": "151008"
          },
          {
            "name": "Chocos",
            "sku": "09",
            "skuDepPro": "1510",
            "ubigeo": "151009"
          },
          {
            "name": "Cochas",
            "sku": "10",
            "skuDepPro": "1510",
            "ubigeo": "151010"
          },
          {
            "name": "Colonia",
            "sku": "11",
            "skuDepPro": "1510",
            "ubigeo": "151011"
          },
          {
            "name": "Hongos",
            "sku": "12",
            "skuDepPro": "1510",
            "ubigeo": "151012"
          },
          {
            "name": "Huampara",
            "sku": "13",
            "skuDepPro": "1510",
            "ubigeo": "151013"
          },
          {
            "name": "Huancaya",
            "sku": "14",
            "skuDepPro": "1510",
            "ubigeo": "151014"
          },
          {
            "name": "Huangascar",
            "sku": "15",
            "skuDepPro": "1510",
            "ubigeo": "151015"
          },
          {
            "name": "Huantan",
            "sku": "16",
            "skuDepPro": "1510",
            "ubigeo": "151016"
          },
          {
            "name": "Huañec",
            "sku": "17",
            "skuDepPro": "1510",
            "ubigeo": "151017"
          },
          {
            "name": "Laraos",
            "sku": "18",
            "skuDepPro": "1510",
            "ubigeo": "151018"
          },
          {
            "name": "Lincha",
            "sku": "19",
            "skuDepPro": "1510",
            "ubigeo": "151019"
          },
          {
            "name": "Madean",
            "sku": "20",
            "skuDepPro": "1510",
            "ubigeo": "151020"
          },
          {
            "name": "Miraflores",
            "sku": "21",
            "skuDepPro": "1510",
            "ubigeo": "151021"
          },
          {
            "name": "Omas",
            "sku": "22",
            "skuDepPro": "1510",
            "ubigeo": "151022"
          },
          {
            "name": "Putinza",
            "sku": "23",
            "skuDepPro": "1510",
            "ubigeo": "151023"
          },
          {
            "name": "Quinches",
            "sku": "24",
            "skuDepPro": "1510",
            "ubigeo": "151024"
          },
          {
            "name": "Quinocay",
            "sku": "25",
            "skuDepPro": "1510",
            "ubigeo": "151025"
          },
          {
            "name": "San Joaquin",
            "sku": "26",
            "skuDepPro": "1510",
            "ubigeo": "151026"
          },
          {
            "name": "San Pedro de Pilas",
            "sku": "27",
            "skuDepPro": "1510",
            "ubigeo": "151027"
          },
          {
            "name": "Tanta",
            "sku": "28",
            "skuDepPro": "1510",
            "ubigeo": "151028"
          },
          {
            "name": "Tauripampa",
            "sku": "29",
            "skuDepPro": "1510",
            "ubigeo": "151029"
          },
          {
            "name": "Tomas",
            "sku": "30",
            "skuDepPro": "1510",
            "ubigeo": "151030"
          },
          {
            "name": "Tupe",
            "sku": "31",
            "skuDepPro": "1510",
            "ubigeo": "151031"
          },
          {
            "name": "Viñac",
            "sku": "32",
            "skuDepPro": "1510",
            "ubigeo": "151032"
          },
          {
            "name": "Vitis",
            "sku": "33",
            "skuDepPro": "1510",
            "ubigeo": "151033"
          }
        ]
      }
    ]
  },
  {
    "name": "Loreto",
    "sku": "16",
    "provincias": [
      {
        "name": "Maynas",
        "sku": "01",
        "skuDep": "16",
        "distritos": [
          {
            "name": "Iquitos",
            "sku": "01",
            "skuDepPro": "1601",
            "ubigeo": "160101"
          },
          {
            "name": "Alto Nanay",
            "sku": "02",
            "skuDepPro": "1601",
            "ubigeo": "160102"
          },
          {
            "name": "Fernando Lores",
            "sku": "03",
            "skuDepPro": "1601",
            "ubigeo": "160103"
          },
          {
            "name": "Indiana",
            "sku": "04",
            "skuDepPro": "1601",
            "ubigeo": "160104"
          },
          {
            "name": "Las Amazonas",
            "sku": "05",
            "skuDepPro": "1601",
            "ubigeo": "160105"
          },
          {
            "name": "Mazan",
            "sku": "06",
            "skuDepPro": "1601",
            "ubigeo": "160106"
          },
          {
            "name": "Napo",
            "sku": "07",
            "skuDepPro": "1601",
            "ubigeo": "160107"
          },
          {
            "name": "Punchana",
            "sku": "08",
            "skuDepPro": "1601",
            "ubigeo": "160108"
          },
          {
            "name": "Torres Causana",
            "sku": "10",
            "skuDepPro": "1601",
            "ubigeo": "160110"
          },
          {
            "name": "Belen",
            "sku": "12",
            "skuDepPro": "1601",
            "ubigeo": "160112"
          },
          {
            "name": "San Juan Bautista",
            "sku": "13",
            "skuDepPro": "1601",
            "ubigeo": "160113"
          }
        ]
      },
      {
        "name": "Alto Amazonas",
        "sku": "02",
        "skuDep": "16",
        "distritos": [
          {
            "name": "Yurimaguas",
            "sku": "01",
            "skuDepPro": "1602",
            "ubigeo": "160201"
          },
          {
            "name": "Balsapuerto",
            "sku": "02",
            "skuDepPro": "1602",
            "ubigeo": "160202"
          },
          {
            "name": "Jeberos",
            "sku": "05",
            "skuDepPro": "1602",
            "ubigeo": "160205"
          },
          {
            "name": "Lagunas",
            "sku": "06",
            "skuDepPro": "1602",
            "ubigeo": "160206"
          },
          {
            "name": "Santa Cruz",
            "sku": "10",
            "skuDepPro": "1602",
            "ubigeo": "160210"
          },
          {
            "name": "Teniente Cesar Lopez Rojas",
            "sku": "11",
            "skuDepPro": "1602",
            "ubigeo": "160211"
          }
        ]
      },
      {
        "name": "Loreto",
        "sku": "03",
        "skuDep": "16",
        "distritos": [
          {
            "name": "Nauta",
            "sku": "01",
            "skuDepPro": "1603",
            "ubigeo": "160301"
          },
          {
            "name": "Parinari",
            "sku": "02",
            "skuDepPro": "1603",
            "ubigeo": "160302"
          },
          {
            "name": "Tigre",
            "sku": "03",
            "skuDepPro": "1603",
            "ubigeo": "160303"
          },
          {
            "name": "Trompeteros",
            "sku": "04",
            "skuDepPro": "1603",
            "ubigeo": "160304"
          },
          {
            "name": "Urarinas",
            "sku": "05",
            "skuDepPro": "1603",
            "ubigeo": "160305"
          }
        ]
      },
      {
        "name": "Mariscal Ramón Castilla",
        "sku": "04",
        "skuDep": "16",
        "distritos": [
          {
            "name": "Ramon Castilla",
            "sku": "01",
            "skuDepPro": "1604",
            "ubigeo": "160401"
          },
          {
            "name": "Pebas",
            "sku": "02",
            "skuDepPro": "1604",
            "ubigeo": "160402"
          },
          {
            "name": "Yavari",
            "sku": "03",
            "skuDepPro": "1604",
            "ubigeo": "160403"
          },
          {
            "name": "San Pablo",
            "sku": "04",
            "skuDepPro": "1604",
            "ubigeo": "160404"
          }
        ]
      },
      {
        "name": "Requena",
        "sku": "05",
        "skuDep": "16",
        "distritos": [
          {
            "name": "Requena",
            "sku": "01",
            "skuDepPro": "1605",
            "ubigeo": "160501"
          },
          {
            "name": "Alto Tapiche",
            "sku": "02",
            "skuDepPro": "1605",
            "ubigeo": "160502"
          },
          {
            "name": "Capelo",
            "sku": "03",
            "skuDepPro": "1605",
            "ubigeo": "160503"
          },
          {
            "name": "Emilio San Martín",
            "sku": "04",
            "skuDepPro": "1605",
            "ubigeo": "160504"
          },
          {
            "name": "Maquia",
            "sku": "05",
            "skuDepPro": "1605",
            "ubigeo": "160505"
          },
          {
            "name": "Puinahua",
            "sku": "06",
            "skuDepPro": "1605",
            "ubigeo": "160506"
          },
          {
            "name": "Saquena",
            "sku": "07",
            "skuDepPro": "1605",
            "ubigeo": "160507"
          },
          {
            "name": "Soplin",
            "sku": "08",
            "skuDepPro": "1605",
            "ubigeo": "160508"
          },
          {
            "name": "Tapiche",
            "sku": "09",
            "skuDepPro": "1605",
            "ubigeo": "160509"
          },
          {
            "name": "Jenaro Herrera",
            "sku": "10",
            "skuDepPro": "1605",
            "ubigeo": "160510"
          },
          {
            "name": "Yaquerana",
            "sku": "11",
            "skuDepPro": "1605",
            "ubigeo": "160511"
          }
        ]
      },
      {
        "name": "Ucayali",
        "sku": "06",
        "skuDep": "16",
        "distritos": [
          {
            "name": "Contamana",
            "sku": "01",
            "skuDepPro": "1606",
            "ubigeo": "160601"
          },
          {
            "name": "Inahuaya",
            "sku": "02",
            "skuDepPro": "1606",
            "ubigeo": "160602"
          },
          {
            "name": "Padre Marquez",
            "sku": "03",
            "skuDepPro": "1606",
            "ubigeo": "160603"
          },
          {
            "name": "Pampa Hermosa",
            "sku": "04",
            "skuDepPro": "1606",
            "ubigeo": "160604"
          },
          {
            "name": "Sarayacu",
            "sku": "05",
            "skuDepPro": "1606",
            "ubigeo": "160605"
          },
          {
            "name": "Vargas Guerra",
            "sku": "06",
            "skuDepPro": "1606",
            "ubigeo": "160606"
          }
        ]
      },
      {
        "name": "Datem Del Marañón",
        "sku": "07",
        "skuDep": "16",
        "distritos": [
          {
            "name": "Barranca",
            "sku": "01",
            "skuDepPro": "1607",
            "ubigeo": "160701"
          },
          {
            "name": "Cahuapanas",
            "sku": "02",
            "skuDepPro": "1607",
            "ubigeo": "160702"
          },
          {
            "name": "Manseriche",
            "sku": "03",
            "skuDepPro": "1607",
            "ubigeo": "160703"
          },
          {
            "name": "Morona",
            "sku": "04",
            "skuDepPro": "1607",
            "ubigeo": "160704"
          },
          {
            "name": "Pastaza",
            "sku": "05",
            "skuDepPro": "1607",
            "ubigeo": "160705"
          },
          {
            "name": "Andoas",
            "sku": "06",
            "skuDepPro": "1607",
            "ubigeo": "160706"
          }
        ]
      },
      {
        "name": "Putumayo",
        "sku": "08",
        "skuDep": "16",
        "distritos": [
          {
            "name": "Putumayo",
            "sku": "01",
            "skuDepPro": "1608",
            "ubigeo": "160801"
          },
          {
            "name": "Rosa Panduro",
            "sku": "02",
            "skuDepPro": "1608",
            "ubigeo": "160802"
          },
          {
            "name": "Teniente Manuel Clavero",
            "sku": "03",
            "skuDepPro": "1608",
            "ubigeo": "160803"
          },
          {
            "name": "Yaguas",
            "sku": "04",
            "skuDepPro": "1608",
            "ubigeo": "160804"
          }
        ]
      }
    ]
  },
  {
    "name": "Madre de Dios",
    "sku": "17",
    "provincias": [
      {
        "name": "Tambopata",
        "sku": "01",
        "skuDep": "17",
        "distritos": [
          {
            "name": "Tambopata",
            "sku": "01",
            "skuDepPro": "1701",
            "ubigeo": "170101"
          },
          {
            "name": "Inambari",
            "sku": "02",
            "skuDepPro": "1701",
            "ubigeo": "170102"
          },
          {
            "name": "Las Piedras",
            "sku": "03",
            "skuDepPro": "1701",
            "ubigeo": "170103"
          },
          {
            "name": "Laberinto",
            "sku": "04",
            "skuDepPro": "1701",
            "ubigeo": "170104"
          }
        ]
      },
      {
        "name": "Manu",
        "sku": "02",
        "skuDep": "17",
        "distritos": [
          {
            "name": "Manu",
            "sku": "01",
            "skuDepPro": "1702",
            "ubigeo": "170201"
          },
          {
            "name": "Fitzcarrald",
            "sku": "02",
            "skuDepPro": "1702",
            "ubigeo": "170202"
          },
          {
            "name": "Madre de Dios",
            "sku": "03",
            "skuDepPro": "1702",
            "ubigeo": "170203"
          },
          {
            "name": "Huepetuhe",
            "sku": "04",
            "skuDepPro": "1702",
            "ubigeo": "170204"
          }
        ]
      },
      {
        "name": "Tahuamanu",
        "sku": "03",
        "skuDep": "17",
        "distritos": [
          {
            "name": "Iñapari",
            "sku": "01",
            "skuDepPro": "1703",
            "ubigeo": "170301"
          },
          {
            "name": "Iberia",
            "sku": "02",
            "skuDepPro": "1703",
            "ubigeo": "170302"
          },
          {
            "name": "Tahuamanu",
            "sku": "03",
            "skuDepPro": "1703",
            "ubigeo": "170303"
          }
        ]
      }
    ]
  },
  {
    "name": "Moquegua",
    "sku": "18",
    "provincias": [
      {
        "name": "Mariscal Nieto",
        "sku": "01",
        "skuDep": "18",
        "distritos": [
          {
            "name": "Moquegua",
            "sku": "01",
            "skuDepPro": "1801",
            "ubigeo": "180101"
          },
          {
            "name": "Carumas",
            "sku": "02",
            "skuDepPro": "1801",
            "ubigeo": "180102"
          },
          {
            "name": "Cuchumbaya",
            "sku": "03",
            "skuDepPro": "1801",
            "ubigeo": "180103"
          },
          {
            "name": "Samegua",
            "sku": "04",
            "skuDepPro": "1801",
            "ubigeo": "180104"
          },
          {
            "name": "San Cristobal",
            "sku": "05",
            "skuDepPro": "1801",
            "ubigeo": "180105"
          },
          {
            "name": "Torata",
            "sku": "06",
            "skuDepPro": "1801",
            "ubigeo": "180106"
          }
        ]
      },
      {
        "name": "General Sánchez Cerro",
        "sku": "02",
        "skuDep": "18",
        "distritos": [
          {
            "name": "Omate",
            "sku": "01",
            "skuDepPro": "1802",
            "ubigeo": "180201"
          },
          {
            "name": "Chojata",
            "sku": "02",
            "skuDepPro": "1802",
            "ubigeo": "180202"
          },
          {
            "name": "Coalaque",
            "sku": "03",
            "skuDepPro": "1802",
            "ubigeo": "180203"
          },
          {
            "name": "Ichuña",
            "sku": "04",
            "skuDepPro": "1802",
            "ubigeo": "180204"
          },
          {
            "name": "La Capilla",
            "sku": "05",
            "skuDepPro": "1802",
            "ubigeo": "180205"
          },
          {
            "name": "Lloque",
            "sku": "06",
            "skuDepPro": "1802",
            "ubigeo": "180206"
          },
          {
            "name": "Matalaque",
            "sku": "07",
            "skuDepPro": "1802",
            "ubigeo": "180207"
          },
          {
            "name": "Puquina",
            "sku": "08",
            "skuDepPro": "1802",
            "ubigeo": "180208"
          },
          {
            "name": "Quinistaquillas",
            "sku": "09",
            "skuDepPro": "1802",
            "ubigeo": "180209"
          },
          {
            "name": "Ubinas",
            "sku": "10",
            "skuDepPro": "1802",
            "ubigeo": "180210"
          },
          {
            "name": "Yunga",
            "sku": "11",
            "skuDepPro": "1802",
            "ubigeo": "180211"
          }
        ]
      },
      {
        "name": "Ilo",
        "sku": "03",
        "skuDep": "18",
        "distritos": [
          {
            "name": "Ilo",
            "sku": "01",
            "skuDepPro": "1803",
            "ubigeo": "180301"
          },
          {
            "name": "El Algarrobal",
            "sku": "02",
            "skuDepPro": "1803",
            "ubigeo": "180302"
          },
          {
            "name": "Pacocha",
            "sku": "03",
            "skuDepPro": "1803",
            "ubigeo": "180303"
          }
        ]
      }
    ]
  },
  {
    "name": "Pasco",
    "sku": "19",
    "provincias": [
      {
        "name": "Pasco",
        "sku": "01",
        "skuDep": "19",
        "distritos": [
          {
            "name": "Chaupimarca",
            "sku": "01",
            "skuDepPro": "1901",
            "ubigeo": "190101"
          },
          {
            "name": "Huachon",
            "sku": "02",
            "skuDepPro": "1901",
            "ubigeo": "190102"
          },
          {
            "name": "Huariaca",
            "sku": "03",
            "skuDepPro": "1901",
            "ubigeo": "190103"
          },
          {
            "name": "Huayllay",
            "sku": "04",
            "skuDepPro": "1901",
            "ubigeo": "190104"
          },
          {
            "name": "Ninacaca",
            "sku": "05",
            "skuDepPro": "1901",
            "ubigeo": "190105"
          },
          {
            "name": "Pallanchacra",
            "sku": "06",
            "skuDepPro": "1901",
            "ubigeo": "190106"
          },
          {
            "name": "Paucartambo",
            "sku": "07",
            "skuDepPro": "1901",
            "ubigeo": "190107"
          },
          {
            "name": "San Francisco de Asis de Yarusyacan",
            "sku": "08",
            "skuDepPro": "1901",
            "ubigeo": "190108"
          },
          {
            "name": "Simon Bolívar",
            "sku": "09",
            "skuDepPro": "1901",
            "ubigeo": "190109"
          },
          {
            "name": "Ticlacayan",
            "sku": "10",
            "skuDepPro": "1901",
            "ubigeo": "190110"
          },
          {
            "name": "Tinyahuarco",
            "sku": "11",
            "skuDepPro": "1901",
            "ubigeo": "190111"
          },
          {
            "name": "Vicco",
            "sku": "12",
            "skuDepPro": "1901",
            "ubigeo": "190112"
          },
          {
            "name": "Yanacancha",
            "sku": "13",
            "skuDepPro": "1901",
            "ubigeo": "190113"
          }
        ]
      },
      {
        "name": "Daniel Alcides Carrión",
        "sku": "02",
        "skuDep": "19",
        "distritos": [
          {
            "name": "Yanahuanca",
            "sku": "01",
            "skuDepPro": "1902",
            "ubigeo": "190201"
          },
          {
            "name": "Chacayan",
            "sku": "02",
            "skuDepPro": "1902",
            "ubigeo": "190202"
          },
          {
            "name": "Goyllarisquizga",
            "sku": "03",
            "skuDepPro": "1902",
            "ubigeo": "190203"
          },
          {
            "name": "Paucar",
            "sku": "04",
            "skuDepPro": "1902",
            "ubigeo": "190204"
          },
          {
            "name": "San Pedro de Pillao",
            "sku": "05",
            "skuDepPro": "1902",
            "ubigeo": "190205"
          },
          {
            "name": "Santa Ana de Tusi",
            "sku": "06",
            "skuDepPro": "1902",
            "ubigeo": "190206"
          },
          {
            "name": "Tapuc",
            "sku": "07",
            "skuDepPro": "1902",
            "ubigeo": "190207"
          },
          {
            "name": "Vilcabamba",
            "sku": "08",
            "skuDepPro": "1902",
            "ubigeo": "190208"
          }
        ]
      },
      {
        "name": "Oxapampa",
        "sku": "03",
        "skuDep": "19",
        "distritos": [
          {
            "name": "Oxapampa",
            "sku": "01",
            "skuDepPro": "1903",
            "ubigeo": "190301"
          },
          {
            "name": "Chontabamba",
            "sku": "02",
            "skuDepPro": "1903",
            "ubigeo": "190302"
          },
          {
            "name": "Huancabamba",
            "sku": "03",
            "skuDepPro": "1903",
            "ubigeo": "190303"
          },
          {
            "name": "Palcazu",
            "sku": "04",
            "skuDepPro": "1903",
            "ubigeo": "190304"
          },
          {
            "name": "Pozuzo",
            "sku": "05",
            "skuDepPro": "1903",
            "ubigeo": "190305"
          },
          {
            "name": "Puerto Bermudez",
            "sku": "06",
            "skuDepPro": "1903",
            "ubigeo": "190306"
          },
          {
            "name": "Villa Rica",
            "sku": "07",
            "skuDepPro": "1903",
            "ubigeo": "190307"
          },
          {
            "name": "Constitucion",
            "sku": "08",
            "skuDepPro": "1903",
            "ubigeo": "190308"
          }
        ]
      }
    ]
  },
  {
    "name": "Piura",
    "sku": "20",
    "provincias": [
      {
        "name": "Piura",
        "sku": "01",
        "skuDep": "20",
        "distritos": [
          {
            "name": "Piura",
            "sku": "01",
            "skuDepPro": "2001",
            "ubigeo": "200101"
          },
          {
            "name": "Castilla",
            "sku": "04",
            "skuDepPro": "2001",
            "ubigeo": "200104"
          },
          {
            "name": "Catacaos",
            "sku": "05",
            "skuDepPro": "2001",
            "ubigeo": "200105"
          },
          {
            "name": "Cura Mori",
            "sku": "07",
            "skuDepPro": "2001",
            "ubigeo": "200107"
          },
          {
            "name": "El Tallan",
            "sku": "08",
            "skuDepPro": "2001",
            "ubigeo": "200108"
          },
          {
            "name": "La Arena",
            "sku": "09",
            "skuDepPro": "2001",
            "ubigeo": "200109"
          },
          {
            "name": "La Union",
            "sku": "10",
            "skuDepPro": "2001",
            "ubigeo": "200110"
          },
          {
            "name": "Las Lomas",
            "sku": "11",
            "skuDepPro": "2001",
            "ubigeo": "200111"
          },
          {
            "name": "Tambo Grande",
            "sku": "14",
            "skuDepPro": "2001",
            "ubigeo": "200114"
          },
          {
            "name": "Veintiseis de Octubre",
            "sku": "15",
            "skuDepPro": "2001",
            "ubigeo": "200115"
          }
        ]
      },
      {
        "name": "Ayabaca",
        "sku": "02",
        "skuDep": "20",
        "distritos": [
          {
            "name": "Ayabaca",
            "sku": "01",
            "skuDepPro": "2002",
            "ubigeo": "200201"
          },
          {
            "name": "Frias",
            "sku": "02",
            "skuDepPro": "2002",
            "ubigeo": "200202"
          },
          {
            "name": "Jilili",
            "sku": "03",
            "skuDepPro": "2002",
            "ubigeo": "200203"
          },
          {
            "name": "Lagunas",
            "sku": "04",
            "skuDepPro": "2002",
            "ubigeo": "200204"
          },
          {
            "name": "Montero",
            "sku": "05",
            "skuDepPro": "2002",
            "ubigeo": "200205"
          },
          {
            "name": "Pacaipampa",
            "sku": "06",
            "skuDepPro": "2002",
            "ubigeo": "200206"
          },
          {
            "name": "Paimas",
            "sku": "07",
            "skuDepPro": "2002",
            "ubigeo": "200207"
          },
          {
            "name": "Sapillica",
            "sku": "08",
            "skuDepPro": "2002",
            "ubigeo": "200208"
          },
          {
            "name": "Sicchez",
            "sku": "09",
            "skuDepPro": "2002",
            "ubigeo": "200209"
          },
          {
            "name": "Suyo",
            "sku": "10",
            "skuDepPro": "2002",
            "ubigeo": "200210"
          }
        ]
      },
      {
        "name": "Huancabamba",
        "sku": "03",
        "skuDep": "20",
        "distritos": [
          {
            "name": "Huancabamba",
            "sku": "01",
            "skuDepPro": "2003",
            "ubigeo": "200301"
          },
          {
            "name": "Canchaque",
            "sku": "02",
            "skuDepPro": "2003",
            "ubigeo": "200302"
          },
          {
            "name": "El Carmen de la Frontera",
            "sku": "03",
            "skuDepPro": "2003",
            "ubigeo": "200303"
          },
          {
            "name": "Huarmaca",
            "sku": "04",
            "skuDepPro": "2003",
            "ubigeo": "200304"
          },
          {
            "name": "Lalaquiz",
            "sku": "05",
            "skuDepPro": "2003",
            "ubigeo": "200305"
          },
          {
            "name": "San Miguel de El Faique",
            "sku": "06",
            "skuDepPro": "2003",
            "ubigeo": "200306"
          },
          {
            "name": "Sondor",
            "sku": "07",
            "skuDepPro": "2003",
            "ubigeo": "200307"
          },
          {
            "name": "Sondorillo",
            "sku": "08",
            "skuDepPro": "2003",
            "ubigeo": "200308"
          }
        ]
      },
      {
        "name": "Morropón",
        "sku": "04",
        "skuDep": "20",
        "distritos": [
          {
            "name": "Chulucanas",
            "sku": "01",
            "skuDepPro": "2004",
            "ubigeo": "200401"
          },
          {
            "name": "Buenos Aires",
            "sku": "02",
            "skuDepPro": "2004",
            "ubigeo": "200402"
          },
          {
            "name": "Chalaco",
            "sku": "03",
            "skuDepPro": "2004",
            "ubigeo": "200403"
          },
          {
            "name": "La Matanza",
            "sku": "04",
            "skuDepPro": "2004",
            "ubigeo": "200404"
          },
          {
            "name": "Morropón",
            "sku": "05",
            "skuDepPro": "2004",
            "ubigeo": "200405"
          },
          {
            "name": "Salitral",
            "sku": "06",
            "skuDepPro": "2004",
            "ubigeo": "200406"
          },
          {
            "name": "San Juan de Bigote",
            "sku": "07",
            "skuDepPro": "2004",
            "ubigeo": "200407"
          },
          {
            "name": "Santa Catalina de Mossa",
            "sku": "08",
            "skuDepPro": "2004",
            "ubigeo": "200408"
          },
          {
            "name": "Santo Domingo",
            "sku": "09",
            "skuDepPro": "2004",
            "ubigeo": "200409"
          },
          {
            "name": "Yamango",
            "sku": "10",
            "skuDepPro": "2004",
            "ubigeo": "200410"
          }
        ]
      },
      {
        "name": "Paita",
        "sku": "05",
        "skuDep": "20",
        "distritos": [
          {
            "name": "Paita",
            "sku": "01",
            "skuDepPro": "2005",
            "ubigeo": "200501"
          },
          {
            "name": "Amotape",
            "sku": "02",
            "skuDepPro": "2005",
            "ubigeo": "200502"
          },
          {
            "name": "Arenal",
            "sku": "03",
            "skuDepPro": "2005",
            "ubigeo": "200503"
          },
          {
            "name": "Colan",
            "sku": "04",
            "skuDepPro": "2005",
            "ubigeo": "200504"
          },
          {
            "name": "La Huaca",
            "sku": "05",
            "skuDepPro": "2005",
            "ubigeo": "200505"
          },
          {
            "name": "Tamarindo",
            "sku": "06",
            "skuDepPro": "2005",
            "ubigeo": "200506"
          },
          {
            "name": "Vichayal",
            "sku": "07",
            "skuDepPro": "2005",
            "ubigeo": "200507"
          }
        ]
      },
      {
        "name": "Sullana",
        "sku": "06",
        "skuDep": "20",
        "distritos": [
          {
            "name": "Sullana",
            "sku": "01",
            "skuDepPro": "2006",
            "ubigeo": "200601"
          },
          {
            "name": "Bellavista",
            "sku": "02",
            "skuDepPro": "2006",
            "ubigeo": "200602"
          },
          {
            "name": "Ignacio Escudero",
            "sku": "03",
            "skuDepPro": "2006",
            "ubigeo": "200603"
          },
          {
            "name": "Lancones",
            "sku": "04",
            "skuDepPro": "2006",
            "ubigeo": "200604"
          },
          {
            "name": "Marcavelica",
            "sku": "05",
            "skuDepPro": "2006",
            "ubigeo": "200605"
          },
          {
            "name": "Miguel Checa",
            "sku": "06",
            "skuDepPro": "2006",
            "ubigeo": "200606"
          },
          {
            "name": "Querecotillo",
            "sku": "07",
            "skuDepPro": "2006",
            "ubigeo": "200607"
          },
          {
            "name": "Salitral",
            "sku": "08",
            "skuDepPro": "2006",
            "ubigeo": "200608"
          }
        ]
      },
      {
        "name": "Talara",
        "sku": "07",
        "skuDep": "20",
        "distritos": [
          {
            "name": "Pariñas",
            "sku": "01",
            "skuDepPro": "2007",
            "ubigeo": "200701"
          },
          {
            "name": "El Alto",
            "sku": "02",
            "skuDepPro": "2007",
            "ubigeo": "200702"
          },
          {
            "name": "La Brea",
            "sku": "03",
            "skuDepPro": "2007",
            "ubigeo": "200703"
          },
          {
            "name": "Lobitos",
            "sku": "04",
            "skuDepPro": "2007",
            "ubigeo": "200704"
          },
          {
            "name": "Los Organos",
            "sku": "05",
            "skuDepPro": "2007",
            "ubigeo": "200705"
          },
          {
            "name": "Mancora",
            "sku": "06",
            "skuDepPro": "2007",
            "ubigeo": "200706"
          }
        ]
      },
      {
        "name": "Sechura",
        "sku": "08",
        "skuDep": "20",
        "distritos": [
          {
            "name": "Sechura",
            "sku": "01",
            "skuDepPro": "2008",
            "ubigeo": "200801"
          },
          {
            "name": "Bellavista de la Union",
            "sku": "02",
            "skuDepPro": "2008",
            "ubigeo": "200802"
          },
          {
            "name": "Bernal",
            "sku": "03",
            "skuDepPro": "2008",
            "ubigeo": "200803"
          },
          {
            "name": "Cristo Nos Valga",
            "sku": "04",
            "skuDepPro": "2008",
            "ubigeo": "200804"
          },
          {
            "name": "Vice",
            "sku": "05",
            "skuDepPro": "2008",
            "ubigeo": "200805"
          },
          {
            "name": "Rinconada Llicuar",
            "sku": "06",
            "skuDepPro": "2008",
            "ubigeo": "200806"
          }
        ]
      }
    ]
  },
  {
    "name": "Puno",
    "sku": "21",
    "provincias": [
      {
        "name": "Puno",
        "sku": "01",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Puno",
            "sku": "01",
            "skuDepPro": "2101",
            "ubigeo": "210101"
          },
          {
            "name": "Acora",
            "sku": "02",
            "skuDepPro": "2101",
            "ubigeo": "210102"
          },
          {
            "name": "Amantani",
            "sku": "03",
            "skuDepPro": "2101",
            "ubigeo": "210103"
          },
          {
            "name": "Atuncolla",
            "sku": "04",
            "skuDepPro": "2101",
            "ubigeo": "210104"
          },
          {
            "name": "Capachica",
            "sku": "05",
            "skuDepPro": "2101",
            "ubigeo": "210105"
          },
          {
            "name": "Chucuito",
            "sku": "06",
            "skuDepPro": "2101",
            "ubigeo": "210106"
          },
          {
            "name": "Coata",
            "sku": "07",
            "skuDepPro": "2101",
            "ubigeo": "210107"
          },
          {
            "name": "Huata",
            "sku": "08",
            "skuDepPro": "2101",
            "ubigeo": "210108"
          },
          {
            "name": "Mañazo",
            "sku": "09",
            "skuDepPro": "2101",
            "ubigeo": "210109"
          },
          {
            "name": "Paucarcolla",
            "sku": "10",
            "skuDepPro": "2101",
            "ubigeo": "210110"
          },
          {
            "name": "Pichacani",
            "sku": "11",
            "skuDepPro": "2101",
            "ubigeo": "210111"
          },
          {
            "name": "Plateria",
            "sku": "12",
            "skuDepPro": "2101",
            "ubigeo": "210112"
          },
          {
            "name": "San Antonio",
            "sku": "13",
            "skuDepPro": "2101",
            "ubigeo": "210113"
          },
          {
            "name": "Tiquillaca",
            "sku": "14",
            "skuDepPro": "2101",
            "ubigeo": "210114"
          },
          {
            "name": "Vilque",
            "sku": "15",
            "skuDepPro": "2101",
            "ubigeo": "210115"
          }
        ]
      },
      {
        "name": "Azángaro",
        "sku": "02",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Azángaro",
            "sku": "01",
            "skuDepPro": "2102",
            "ubigeo": "210201"
          },
          {
            "name": "Achaya",
            "sku": "02",
            "skuDepPro": "2102",
            "ubigeo": "210202"
          },
          {
            "name": "Arapa",
            "sku": "03",
            "skuDepPro": "2102",
            "ubigeo": "210203"
          },
          {
            "name": "Asillo",
            "sku": "04",
            "skuDepPro": "2102",
            "ubigeo": "210204"
          },
          {
            "name": "Caminaca",
            "sku": "05",
            "skuDepPro": "2102",
            "ubigeo": "210205"
          },
          {
            "name": "Chupa",
            "sku": "06",
            "skuDepPro": "2102",
            "ubigeo": "210206"
          },
          {
            "name": "Jose Domingo Choquehuanca",
            "sku": "07",
            "skuDepPro": "2102",
            "ubigeo": "210207"
          },
          {
            "name": "Muñani",
            "sku": "08",
            "skuDepPro": "2102",
            "ubigeo": "210208"
          },
          {
            "name": "Potoni",
            "sku": "09",
            "skuDepPro": "2102",
            "ubigeo": "210209"
          },
          {
            "name": "Saman",
            "sku": "10",
            "skuDepPro": "2102",
            "ubigeo": "210210"
          },
          {
            "name": "San Anton",
            "sku": "11",
            "skuDepPro": "2102",
            "ubigeo": "210211"
          },
          {
            "name": "San Jose",
            "sku": "12",
            "skuDepPro": "2102",
            "ubigeo": "210212"
          },
          {
            "name": "San Juan de Salinas",
            "sku": "13",
            "skuDepPro": "2102",
            "ubigeo": "210213"
          },
          {
            "name": "Santiago de Pupuja",
            "sku": "14",
            "skuDepPro": "2102",
            "ubigeo": "210214"
          },
          {
            "name": "Tirapata",
            "sku": "15",
            "skuDepPro": "2102",
            "ubigeo": "210215"
          }
        ]
      },
      {
        "name": "Carabaya",
        "sku": "03",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Macusani",
            "sku": "01",
            "skuDepPro": "2103",
            "ubigeo": "210301"
          },
          {
            "name": "Ajoyani",
            "sku": "02",
            "skuDepPro": "2103",
            "ubigeo": "210302"
          },
          {
            "name": "Ayapata",
            "sku": "03",
            "skuDepPro": "2103",
            "ubigeo": "210303"
          },
          {
            "name": "Coasa",
            "sku": "04",
            "skuDepPro": "2103",
            "ubigeo": "210304"
          },
          {
            "name": "Corani",
            "sku": "05",
            "skuDepPro": "2103",
            "ubigeo": "210305"
          },
          {
            "name": "Crucero",
            "sku": "06",
            "skuDepPro": "2103",
            "ubigeo": "210306"
          },
          {
            "name": "Ituata",
            "sku": "07",
            "skuDepPro": "2103",
            "ubigeo": "210307"
          },
          {
            "name": "Ollachea",
            "sku": "08",
            "skuDepPro": "2103",
            "ubigeo": "210308"
          },
          {
            "name": "San Gaban",
            "sku": "09",
            "skuDepPro": "2103",
            "ubigeo": "210309"
          },
          {
            "name": "Usicayos",
            "sku": "10",
            "skuDepPro": "2103",
            "ubigeo": "210310"
          }
        ]
      },
      {
        "name": "Chucuito",
        "sku": "04",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Juli",
            "sku": "01",
            "skuDepPro": "2104",
            "ubigeo": "210401"
          },
          {
            "name": "Desaguadero",
            "sku": "02",
            "skuDepPro": "2104",
            "ubigeo": "210402"
          },
          {
            "name": "Huacullani",
            "sku": "03",
            "skuDepPro": "2104",
            "ubigeo": "210403"
          },
          {
            "name": "Kelluyo",
            "sku": "04",
            "skuDepPro": "2104",
            "ubigeo": "210404"
          },
          {
            "name": "Pisacoma",
            "sku": "05",
            "skuDepPro": "2104",
            "ubigeo": "210405"
          },
          {
            "name": "Pomata",
            "sku": "06",
            "skuDepPro": "2104",
            "ubigeo": "210406"
          },
          {
            "name": "Zepita",
            "sku": "07",
            "skuDepPro": "2104",
            "ubigeo": "210407"
          }
        ]
      },
      {
        "name": "El Collao",
        "sku": "05",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Ilave",
            "sku": "01",
            "skuDepPro": "2105",
            "ubigeo": "210501"
          },
          {
            "name": "Capazo",
            "sku": "02",
            "skuDepPro": "2105",
            "ubigeo": "210502"
          },
          {
            "name": "Pilcuyo",
            "sku": "03",
            "skuDepPro": "2105",
            "ubigeo": "210503"
          },
          {
            "name": "Santa Rosa",
            "sku": "04",
            "skuDepPro": "2105",
            "ubigeo": "210504"
          },
          {
            "name": "Conduriri",
            "sku": "05",
            "skuDepPro": "2105",
            "ubigeo": "210505"
          }
        ]
      },
      {
        "name": "Huancané",
        "sku": "06",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Huancané",
            "sku": "01",
            "skuDepPro": "2106",
            "ubigeo": "210601"
          },
          {
            "name": "Cojata",
            "sku": "02",
            "skuDepPro": "2106",
            "ubigeo": "210602"
          },
          {
            "name": "Huatasani",
            "sku": "03",
            "skuDepPro": "2106",
            "ubigeo": "210603"
          },
          {
            "name": "Inchupalla",
            "sku": "04",
            "skuDepPro": "2106",
            "ubigeo": "210604"
          },
          {
            "name": "Pusi",
            "sku": "05",
            "skuDepPro": "2106",
            "ubigeo": "210605"
          },
          {
            "name": "Rosaspata",
            "sku": "06",
            "skuDepPro": "2106",
            "ubigeo": "210606"
          },
          {
            "name": "Taraco",
            "sku": "07",
            "skuDepPro": "2106",
            "ubigeo": "210607"
          },
          {
            "name": "Vilque Chico",
            "sku": "08",
            "skuDepPro": "2106",
            "ubigeo": "210608"
          }
        ]
      },
      {
        "name": "Lampa",
        "sku": "07",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Lampa",
            "sku": "01",
            "skuDepPro": "2107",
            "ubigeo": "210701"
          },
          {
            "name": "Cabanilla",
            "sku": "02",
            "skuDepPro": "2107",
            "ubigeo": "210702"
          },
          {
            "name": "Calapuja",
            "sku": "03",
            "skuDepPro": "2107",
            "ubigeo": "210703"
          },
          {
            "name": "Nicasio",
            "sku": "04",
            "skuDepPro": "2107",
            "ubigeo": "210704"
          },
          {
            "name": "Ocuviri",
            "sku": "05",
            "skuDepPro": "2107",
            "ubigeo": "210705"
          },
          {
            "name": "Palca",
            "sku": "06",
            "skuDepPro": "2107",
            "ubigeo": "210706"
          },
          {
            "name": "Paratia",
            "sku": "07",
            "skuDepPro": "2107",
            "ubigeo": "210707"
          },
          {
            "name": "Pucara",
            "sku": "08",
            "skuDepPro": "2107",
            "ubigeo": "210708"
          },
          {
            "name": "Santa Lucia",
            "sku": "09",
            "skuDepPro": "2107",
            "ubigeo": "210709"
          },
          {
            "name": "Vilavila",
            "sku": "10",
            "skuDepPro": "2107",
            "ubigeo": "210710"
          }
        ]
      },
      {
        "name": "Melgar",
        "sku": "08",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Ayaviri",
            "sku": "01",
            "skuDepPro": "2108",
            "ubigeo": "210801"
          },
          {
            "name": "Antauta",
            "sku": "02",
            "skuDepPro": "2108",
            "ubigeo": "210802"
          },
          {
            "name": "Cupi",
            "sku": "03",
            "skuDepPro": "2108",
            "ubigeo": "210803"
          },
          {
            "name": "Llalli",
            "sku": "04",
            "skuDepPro": "2108",
            "ubigeo": "210804"
          },
          {
            "name": "Macari",
            "sku": "05",
            "skuDepPro": "2108",
            "ubigeo": "210805"
          },
          {
            "name": "Nuñoa",
            "sku": "06",
            "skuDepPro": "2108",
            "ubigeo": "210806"
          },
          {
            "name": "Orurillo",
            "sku": "07",
            "skuDepPro": "2108",
            "ubigeo": "210807"
          },
          {
            "name": "Santa Rosa",
            "sku": "08",
            "skuDepPro": "2108",
            "ubigeo": "210808"
          },
          {
            "name": "Umachiri",
            "sku": "09",
            "skuDepPro": "2108",
            "ubigeo": "210809"
          }
        ]
      },
      {
        "name": "Moho",
        "sku": "09",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Moho",
            "sku": "01",
            "skuDepPro": "2109",
            "ubigeo": "210901"
          },
          {
            "name": "Conima",
            "sku": "02",
            "skuDepPro": "2109",
            "ubigeo": "210902"
          },
          {
            "name": "Huayrapata",
            "sku": "03",
            "skuDepPro": "2109",
            "ubigeo": "210903"
          },
          {
            "name": "Tilali",
            "sku": "04",
            "skuDepPro": "2109",
            "ubigeo": "210904"
          }
        ]
      },
      {
        "name": "San Antonio de Putina",
        "sku": "10",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Putina",
            "sku": "01",
            "skuDepPro": "2110",
            "ubigeo": "211001"
          },
          {
            "name": "Ananea",
            "sku": "02",
            "skuDepPro": "2110",
            "ubigeo": "211002"
          },
          {
            "name": "Pedro Vilca Apaza",
            "sku": "03",
            "skuDepPro": "2110",
            "ubigeo": "211003"
          },
          {
            "name": "Quilcapuncu",
            "sku": "04",
            "skuDepPro": "2110",
            "ubigeo": "211004"
          },
          {
            "name": "Sina",
            "sku": "05",
            "skuDepPro": "2110",
            "ubigeo": "211005"
          }
        ]
      },
      {
        "name": "San Roman",
        "sku": "11",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Juliaca",
            "sku": "01",
            "skuDepPro": "2111",
            "ubigeo": "211101"
          },
          {
            "name": "Cabana",
            "sku": "02",
            "skuDepPro": "2111",
            "ubigeo": "211102"
          },
          {
            "name": "Cabanillas",
            "sku": "03",
            "skuDepPro": "2111",
            "ubigeo": "211103"
          },
          {
            "name": "Caracoto",
            "sku": "04",
            "skuDepPro": "2111",
            "ubigeo": "211104"
          },
          {
            "name": "San Miguel",
            "sku": "05",
            "skuDepPro": "2111",
            "ubigeo": "211105"
          }
        ]
      },
      {
        "name": "Sandia",
        "sku": "12",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Sandia",
            "sku": "01",
            "skuDepPro": "2112",
            "ubigeo": "211201"
          },
          {
            "name": "Cuyocuyo",
            "sku": "02",
            "skuDepPro": "2112",
            "ubigeo": "211202"
          },
          {
            "name": "Limbani",
            "sku": "03",
            "skuDepPro": "2112",
            "ubigeo": "211203"
          },
          {
            "name": "Patambuco",
            "sku": "04",
            "skuDepPro": "2112",
            "ubigeo": "211204"
          },
          {
            "name": "Phara",
            "sku": "05",
            "skuDepPro": "2112",
            "ubigeo": "211205"
          },
          {
            "name": "Quiaca",
            "sku": "06",
            "skuDepPro": "2112",
            "ubigeo": "211206"
          },
          {
            "name": "San Juan del Oro",
            "sku": "07",
            "skuDepPro": "2112",
            "ubigeo": "211207"
          },
          {
            "name": "Yanahuaya",
            "sku": "08",
            "skuDepPro": "2112",
            "ubigeo": "211208"
          },
          {
            "name": "Alto Inambari",
            "sku": "09",
            "skuDepPro": "2112",
            "ubigeo": "211209"
          },
          {
            "name": "San Pedro de Putina Punco",
            "sku": "10",
            "skuDepPro": "2112",
            "ubigeo": "211210"
          }
        ]
      },
      {
        "name": "Yunguyo",
        "sku": "13",
        "skuDep": "21",
        "distritos": [
          {
            "name": "Yunguyo",
            "sku": "01",
            "skuDepPro": "2113",
            "ubigeo": "211301"
          },
          {
            "name": "Anapia",
            "sku": "02",
            "skuDepPro": "2113",
            "ubigeo": "211302"
          },
          {
            "name": "Copani",
            "sku": "03",
            "skuDepPro": "2113",
            "ubigeo": "211303"
          },
          {
            "name": "Cuturapi",
            "sku": "04",
            "skuDepPro": "2113",
            "ubigeo": "211304"
          },
          {
            "name": "Ollaraya",
            "sku": "05",
            "skuDepPro": "2113",
            "ubigeo": "211305"
          },
          {
            "name": "Tinicachi",
            "sku": "06",
            "skuDepPro": "2113",
            "ubigeo": "211306"
          },
          {
            "name": "Unicachi",
            "sku": "07",
            "skuDepPro": "2113",
            "ubigeo": "211307"
          }
        ]
      }
    ]
  },
  {
    "name": "San Martín",
    "sku": "22",
    "provincias": [
      {
        "name": "Moyobamba",
        "sku": "01",
        "skuDep": "22",
        "distritos": [
          {
            "name": "Moyobamba",
            "sku": "01",
            "skuDepPro": "2201",
            "ubigeo": "220101"
          },
          {
            "name": "Calzada",
            "sku": "02",
            "skuDepPro": "2201",
            "ubigeo": "220102"
          },
          {
            "name": "Habana",
            "sku": "03",
            "skuDepPro": "2201",
            "ubigeo": "220103"
          },
          {
            "name": "Jepelacio",
            "sku": "04",
            "skuDepPro": "2201",
            "ubigeo": "220104"
          },
          {
            "name": "Soritor",
            "sku": "05",
            "skuDepPro": "2201",
            "ubigeo": "220105"
          },
          {
            "name": "Yantalo",
            "sku": "06",
            "skuDepPro": "2201",
            "ubigeo": "220106"
          }
        ]
      },
      {
        "name": "Bellavista",
        "sku": "02",
        "skuDep": "22",
        "distritos": [
          {
            "name": "Bellavista",
            "sku": "01",
            "skuDepPro": "2202",
            "ubigeo": "220201"
          },
          {
            "name": "Alto Biavo",
            "sku": "02",
            "skuDepPro": "2202",
            "ubigeo": "220202"
          },
          {
            "name": "Bajo Biavo",
            "sku": "03",
            "skuDepPro": "2202",
            "ubigeo": "220203"
          },
          {
            "name": "Huallaga",
            "sku": "04",
            "skuDepPro": "2202",
            "ubigeo": "220204"
          },
          {
            "name": "San Pablo",
            "sku": "05",
            "skuDepPro": "2202",
            "ubigeo": "220205"
          },
          {
            "name": "San Rafael",
            "sku": "06",
            "skuDepPro": "2202",
            "ubigeo": "220206"
          }
        ]
      },
      {
        "name": "El Dorado",
        "sku": "03",
        "skuDep": "22",
        "distritos": [
          {
            "name": "San Jose de Sisa",
            "sku": "01",
            "skuDepPro": "2203",
            "ubigeo": "220301"
          },
          {
            "name": "Agua Blanca",
            "sku": "02",
            "skuDepPro": "2203",
            "ubigeo": "220302"
          },
          {
            "name": "San Martín",
            "sku": "03",
            "skuDepPro": "2203",
            "ubigeo": "220303"
          },
          {
            "name": "Santa Rosa",
            "sku": "04",
            "skuDepPro": "2203",
            "ubigeo": "220304"
          },
          {
            "name": "Shatoja",
            "sku": "05",
            "skuDepPro": "2203",
            "ubigeo": "220305"
          }
        ]
      },
      {
        "name": "Huallaga",
        "sku": "04",
        "skuDep": "22",
        "distritos": [
          {
            "name": "Saposoa",
            "sku": "01",
            "skuDepPro": "2204",
            "ubigeo": "220401"
          },
          {
            "name": "Alto Saposoa",
            "sku": "02",
            "skuDepPro": "2204",
            "ubigeo": "220402"
          },
          {
            "name": "El Eslabon",
            "sku": "03",
            "skuDepPro": "2204",
            "ubigeo": "220403"
          },
          {
            "name": "Piscoyacu",
            "sku": "04",
            "skuDepPro": "2204",
            "ubigeo": "220404"
          },
          {
            "name": "Sacanche",
            "sku": "05",
            "skuDepPro": "2204",
            "ubigeo": "220405"
          },
          {
            "name": "Tingo de Saposoa",
            "sku": "06",
            "skuDepPro": "2204",
            "ubigeo": "220406"
          }
        ]
      },
      {
        "name": "Lamas",
        "sku": "05",
        "skuDep": "22",
        "distritos": [
          {
            "name": "Lamas",
            "sku": "01",
            "skuDepPro": "2205",
            "ubigeo": "220501"
          },
          {
            "name": "Alonso de Alvarado",
            "sku": "02",
            "skuDepPro": "2205",
            "ubigeo": "220502"
          },
          {
            "name": "Barranquita",
            "sku": "03",
            "skuDepPro": "2205",
            "ubigeo": "220503"
          },
          {
            "name": "Caynarachi",
            "sku": "04",
            "skuDepPro": "2205",
            "ubigeo": "220504"
          },
          {
            "name": "Cuñumbuqui",
            "sku": "05",
            "skuDepPro": "2205",
            "ubigeo": "220505"
          },
          {
            "name": "Pinto Recodo",
            "sku": "06",
            "skuDepPro": "2205",
            "ubigeo": "220506"
          },
          {
            "name": "Rumisapa",
            "sku": "07",
            "skuDepPro": "2205",
            "ubigeo": "220507"
          },
          {
            "name": "San Roque de Cumbaza",
            "sku": "08",
            "skuDepPro": "2205",
            "ubigeo": "220508"
          },
          {
            "name": "Shanao",
            "sku": "09",
            "skuDepPro": "2205",
            "ubigeo": "220509"
          },
          {
            "name": "Tabalosos",
            "sku": "10",
            "skuDepPro": "2205",
            "ubigeo": "220510"
          },
          {
            "name": "Zapatero",
            "sku": "11",
            "skuDepPro": "2205",
            "ubigeo": "220511"
          }
        ]
      },
      {
        "name": "Mariscal Caceres",
        "sku": "06",
        "skuDep": "22",
        "distritos": [
          {
            "name": "Juanjui",
            "sku": "01",
            "skuDepPro": "2206",
            "ubigeo": "220601"
          },
          {
            "name": "Campanilla",
            "sku": "02",
            "skuDepPro": "2206",
            "ubigeo": "220602"
          },
          {
            "name": "Huicungo",
            "sku": "03",
            "skuDepPro": "2206",
            "ubigeo": "220603"
          },
          {
            "name": "Pachiza",
            "sku": "04",
            "skuDepPro": "2206",
            "ubigeo": "220604"
          },
          {
            "name": "Pajarillo",
            "sku": "05",
            "skuDepPro": "2206",
            "ubigeo": "220605"
          }
        ]
      },
      {
        "name": "Picota",
        "sku": "07",
        "skuDep": "22",
        "distritos": [
          {
            "name": "Picota",
            "sku": "01",
            "skuDepPro": "2207",
            "ubigeo": "220701"
          },
          {
            "name": "Buenos Aires",
            "sku": "02",
            "skuDepPro": "2207",
            "ubigeo": "220702"
          },
          {
            "name": "Caspisapa",
            "sku": "03",
            "skuDepPro": "2207",
            "ubigeo": "220703"
          },
          {
            "name": "Pilluana",
            "sku": "04",
            "skuDepPro": "2207",
            "ubigeo": "220704"
          },
          {
            "name": "Pucacaca",
            "sku": "05",
            "skuDepPro": "2207",
            "ubigeo": "220705"
          },
          {
            "name": "San Cristobal",
            "sku": "06",
            "skuDepPro": "2207",
            "ubigeo": "220706"
          },
          {
            "name": "San Hilarion",
            "sku": "07",
            "skuDepPro": "2207",
            "ubigeo": "220707"
          },
          {
            "name": "Shamboyacu",
            "sku": "08",
            "skuDepPro": "2207",
            "ubigeo": "220708"
          },
          {
            "name": "Tingo de Ponasa",
            "sku": "09",
            "skuDepPro": "2207",
            "ubigeo": "220709"
          },
          {
            "name": "Tres Unidos",
            "sku": "10",
            "skuDepPro": "2207",
            "ubigeo": "220710"
          }
        ]
      },
      {
        "name": "Rioja",
        "sku": "08",
        "skuDep": "22",
        "distritos": [
          {
            "name": "Rioja",
            "sku": "01",
            "skuDepPro": "2208",
            "ubigeo": "220801"
          },
          {
            "name": "Awajun",
            "sku": "02",
            "skuDepPro": "2208",
            "ubigeo": "220802"
          },
          {
            "name": "Elias Soplin Vargas",
            "sku": "03",
            "skuDepPro": "2208",
            "ubigeo": "220803"
          },
          {
            "name": "Nueva Cajamarca",
            "sku": "04",
            "skuDepPro": "2208",
            "ubigeo": "220804"
          },
          {
            "name": "Pardo Miguel",
            "sku": "05",
            "skuDepPro": "2208",
            "ubigeo": "220805"
          },
          {
            "name": "Posic",
            "sku": "06",
            "skuDepPro": "2208",
            "ubigeo": "220806"
          },
          {
            "name": "San Fernando",
            "sku": "07",
            "skuDepPro": "2208",
            "ubigeo": "220807"
          },
          {
            "name": "Yorongos",
            "sku": "08",
            "skuDepPro": "2208",
            "ubigeo": "220808"
          },
          {
            "name": "Yuracyacu",
            "sku": "09",
            "skuDepPro": "2208",
            "ubigeo": "220809"
          }
        ]
      },
      {
        "name": "San Martín",
        "sku": "09",
        "skuDep": "22",
        "distritos": [
          {
            "name": "Tarapoto",
            "sku": "01",
            "skuDepPro": "2209",
            "ubigeo": "220901"
          },
          {
            "name": "Alberto Leveau",
            "sku": "02",
            "skuDepPro": "2209",
            "ubigeo": "220902"
          },
          {
            "name": "Cacatachi",
            "sku": "03",
            "skuDepPro": "2209",
            "ubigeo": "220903"
          },
          {
            "name": "Chazuta",
            "sku": "04",
            "skuDepPro": "2209",
            "ubigeo": "220904"
          },
          {
            "name": "Chipurana",
            "sku": "05",
            "skuDepPro": "2209",
            "ubigeo": "220905"
          },
          {
            "name": "El Porvenir",
            "sku": "06",
            "skuDepPro": "2209",
            "ubigeo": "220906"
          },
          {
            "name": "Huimbayoc",
            "sku": "07",
            "skuDepPro": "2209",
            "ubigeo": "220907"
          },
          {
            "name": "Juan Guerra",
            "sku": "08",
            "skuDepPro": "2209",
            "ubigeo": "220908"
          },
          {
            "name": "La Banda de Shilcayo",
            "sku": "09",
            "skuDepPro": "2209",
            "ubigeo": "220909"
          },
          {
            "name": "Morales",
            "sku": "10",
            "skuDepPro": "2209",
            "ubigeo": "220910"
          },
          {
            "name": "Papaplaya",
            "sku": "11",
            "skuDepPro": "2209",
            "ubigeo": "220911"
          },
          {
            "name": "San Antonio",
            "sku": "12",
            "skuDepPro": "2209",
            "ubigeo": "220912"
          },
          {
            "name": "Sauce",
            "sku": "13",
            "skuDepPro": "2209",
            "ubigeo": "220913"
          },
          {
            "name": "Shapaja",
            "sku": "14",
            "skuDepPro": "2209",
            "ubigeo": "220914"
          }
        ]
      },
      {
        "name": "Tocache",
        "sku": "10",
        "skuDep": "22",
        "distritos": [
          {
            "name": "Tocache",
            "sku": "01",
            "skuDepPro": "2210",
            "ubigeo": "221001"
          },
          {
            "name": "Nuevo Progreso",
            "sku": "02",
            "skuDepPro": "2210",
            "ubigeo": "221002"
          },
          {
            "name": "Polvora",
            "sku": "03",
            "skuDepPro": "2210",
            "ubigeo": "221003"
          },
          {
            "name": "Shunte",
            "sku": "04",
            "skuDepPro": "2210",
            "ubigeo": "221004"
          },
          {
            "name": "Uchiza",
            "sku": "05",
            "skuDepPro": "2210",
            "ubigeo": "221005"
          }
        ]
      }
    ]
  },
  {
    "name": "Tacna",
    "sku": "23",
    "provincias": [
      {
        "name": "Tacna",
        "sku": "01",
        "skuDep": "23",
        "distritos": [
          {
            "name": "Tacna",
            "sku": "01",
            "skuDepPro": "2301",
            "ubigeo": "230101"
          },
          {
            "name": "Alto de la Alianza",
            "sku": "02",
            "skuDepPro": "2301",
            "ubigeo": "230102"
          },
          {
            "name": "Calana",
            "sku": "03",
            "skuDepPro": "2301",
            "ubigeo": "230103"
          },
          {
            "name": "Ciudad Nueva",
            "sku": "04",
            "skuDepPro": "2301",
            "ubigeo": "230104"
          },
          {
            "name": "Inclan",
            "sku": "05",
            "skuDepPro": "2301",
            "ubigeo": "230105"
          },
          {
            "name": "Pachia",
            "sku": "06",
            "skuDepPro": "2301",
            "ubigeo": "230106"
          },
          {
            "name": "Palca",
            "sku": "07",
            "skuDepPro": "2301",
            "ubigeo": "230107"
          },
          {
            "name": "Pocollay",
            "sku": "08",
            "skuDepPro": "2301",
            "ubigeo": "230108"
          },
          {
            "name": "Sama",
            "sku": "09",
            "skuDepPro": "2301",
            "ubigeo": "230109"
          },
          {
            "name": "Coronel Gregorio Albarracin Lanchipa",
            "sku": "10",
            "skuDepPro": "2301",
            "ubigeo": "230110"
          },
          {
            "name": "La Yarada los Palos",
            "sku": "11",
            "skuDepPro": "2301",
            "ubigeo": "230111"
          }
        ]
      },
      {
        "name": "Candarave",
        "sku": "02",
        "skuDep": "23",
        "distritos": [
          {
            "name": "Candarave",
            "sku": "01",
            "skuDepPro": "2302",
            "ubigeo": "230201"
          },
          {
            "name": "Cairani",
            "sku": "02",
            "skuDepPro": "2302",
            "ubigeo": "230202"
          },
          {
            "name": "Camilaca",
            "sku": "03",
            "skuDepPro": "2302",
            "ubigeo": "230203"
          },
          {
            "name": "Curibaya",
            "sku": "04",
            "skuDepPro": "2302",
            "ubigeo": "230204"
          },
          {
            "name": "Huanuara",
            "sku": "05",
            "skuDepPro": "2302",
            "ubigeo": "230205"
          },
          {
            "name": "Quilahuani",
            "sku": "06",
            "skuDepPro": "2302",
            "ubigeo": "230206"
          }
        ]
      },
      {
        "name": "Jorge Basadre",
        "sku": "03",
        "skuDep": "23",
        "distritos": [
          {
            "name": "Locumba",
            "sku": "01",
            "skuDepPro": "2303",
            "ubigeo": "230301"
          },
          {
            "name": "Ilabaya",
            "sku": "02",
            "skuDepPro": "2303",
            "ubigeo": "230302"
          },
          {
            "name": "Ite",
            "sku": "03",
            "skuDepPro": "2303",
            "ubigeo": "230303"
          }
        ]
      },
      {
        "name": "Tarata",
        "sku": "04",
        "skuDep": "23",
        "distritos": [
          {
            "name": "Tarata",
            "sku": "01",
            "skuDepPro": "2304",
            "ubigeo": "230401"
          },
          {
            "name": "Heroes Albarracin",
            "sku": "02",
            "skuDepPro": "2304",
            "ubigeo": "230402"
          },
          {
            "name": "Estique",
            "sku": "03",
            "skuDepPro": "2304",
            "ubigeo": "230403"
          },
          {
            "name": "Estique-Pampa",
            "sku": "04",
            "skuDepPro": "2304",
            "ubigeo": "230404"
          },
          {
            "name": "Sitajara",
            "sku": "05",
            "skuDepPro": "2304",
            "ubigeo": "230405"
          },
          {
            "name": "Susapaya",
            "sku": "06",
            "skuDepPro": "2304",
            "ubigeo": "230406"
          },
          {
            "name": "Tarucachi",
            "sku": "07",
            "skuDepPro": "2304",
            "ubigeo": "230407"
          },
          {
            "name": "Ticaco",
            "sku": "08",
            "skuDepPro": "2304",
            "ubigeo": "230408"
          }
        ]
      }
    ]
  },
  {
    "name": "Tumbes",
    "sku": "24",
    "provincias": [
      {
        "name": "Tumbes",
        "sku": "01",
        "skuDep": "24",
        "distritos": [
          {
            "name": "Tumbes",
            "sku": "01",
            "skuDepPro": "2401",
            "ubigeo": "240101"
          },
          {
            "name": "Corrales",
            "sku": "02",
            "skuDepPro": "2401",
            "ubigeo": "240102"
          },
          {
            "name": "La Cruz",
            "sku": "03",
            "skuDepPro": "2401",
            "ubigeo": "240103"
          },
          {
            "name": "Pampas de Hospital",
            "sku": "04",
            "skuDepPro": "2401",
            "ubigeo": "240104"
          },
          {
            "name": "San Jacinto",
            "sku": "05",
            "skuDepPro": "2401",
            "ubigeo": "240105"
          },
          {
            "name": "San Juan de la Virgen",
            "sku": "06",
            "skuDepPro": "2401",
            "ubigeo": "240106"
          }
        ]
      },
      {
        "name": "Contralmirante Villar",
        "sku": "02",
        "skuDep": "24",
        "distritos": [
          {
            "name": "Zorritos",
            "sku": "01",
            "skuDepPro": "2402",
            "ubigeo": "240201"
          },
          {
            "name": "Casitas",
            "sku": "02",
            "skuDepPro": "2402",
            "ubigeo": "240202"
          },
          {
            "name": "Canoas de Punta Sal",
            "sku": "03",
            "skuDepPro": "2402",
            "ubigeo": "240203"
          }
        ]
      },
      {
        "name": "Zarumilla",
        "sku": "03",
        "skuDep": "24",
        "distritos": [
          {
            "name": "Zarumilla",
            "sku": "01",
            "skuDepPro": "2403",
            "ubigeo": "240301"
          },
          {
            "name": "Aguas Verdes",
            "sku": "02",
            "skuDepPro": "2403",
            "ubigeo": "240302"
          },
          {
            "name": "Matapalo",
            "sku": "03",
            "skuDepPro": "2403",
            "ubigeo": "240303"
          },
          {
            "name": "Papayal",
            "sku": "04",
            "skuDepPro": "2403",
            "ubigeo": "240304"
          }
        ]
      }
    ]
  },
  {
    "name": "Ucayali",
    "sku": "25",
    "provincias": [
      {
        "name": "Coronel Portillo",
        "sku": "01",
        "skuDep": "25",
        "distritos": [
          {
            "name": "Calleria",
            "sku": "01",
            "skuDepPro": "2501",
            "ubigeo": "250101"
          },
          {
            "name": "Campoverde",
            "sku": "02",
            "skuDepPro": "2501",
            "ubigeo": "250102"
          },
          {
            "name": "Iparia",
            "sku": "03",
            "skuDepPro": "2501",
            "ubigeo": "250103"
          },
          {
            "name": "Masisea",
            "sku": "04",
            "skuDepPro": "2501",
            "ubigeo": "250104"
          },
          {
            "name": "Yarinacocha",
            "sku": "05",
            "skuDepPro": "2501",
            "ubigeo": "250105"
          },
          {
            "name": "Nueva Requena",
            "sku": "06",
            "skuDepPro": "2501",
            "ubigeo": "250106"
          },
          {
            "name": "Manantay",
            "sku": "07",
            "skuDepPro": "2501",
            "ubigeo": "250107"
          }
        ]
      },
      {
        "name": "Atalaya",
        "sku": "02",
        "skuDep": "25",
        "distritos": [
          {
            "name": "Raymondi",
            "sku": "01",
            "skuDepPro": "2502",
            "ubigeo": "250201"
          },
          {
            "name": "Sepahua",
            "sku": "02",
            "skuDepPro": "2502",
            "ubigeo": "250202"
          },
          {
            "name": "Tahuania",
            "sku": "03",
            "skuDepPro": "2502",
            "ubigeo": "250203"
          },
          {
            "name": "Yurua",
            "sku": "04",
            "skuDepPro": "2502",
            "ubigeo": "250204"
          }
        ]
      },
      {
        "name": "Padre Abad",
        "sku": "03",
        "skuDep": "25",
        "distritos": [
          {
            "name": "Padre Abad",
            "sku": "01",
            "skuDepPro": "2503",
            "ubigeo": "250301"
          },
          {
            "name": "Irazola",
            "sku": "02",
            "skuDepPro": "2503",
            "ubigeo": "250302"
          },
          {
            "name": "Curimana",
            "sku": "03",
            "skuDepPro": "2503",
            "ubigeo": "250303"
          },
          {
            "name": "Neshuya",
            "sku": "04",
            "skuDepPro": "2503",
            "ubigeo": "250304"
          },
          {
            "name": "Alexander Von Humboldt",
            "sku": "05",
            "skuDepPro": "2503",
            "ubigeo": "250305"
          }
        ]
      },
      {
        "name": "Purus",
        "sku": "04",
        "skuDep": "25",
        "distritos": [
          {
            "name": "Purus",
            "sku": "01",
            "skuDepPro": "2504",
            "ubigeo": "250401"
          }
        ]
      }
    ]
  },
  {
    "name": "Amazonas",
    "sku": "01",
    "provincias": [
      {
        "name": "Chachapoyas",
        "sku": "01",
        "skuDep": "01",
        "distritos": [
          {
            "name": "Chachapoyas",
            "sku": "01",
            "skuDepPro": "0101",
            "ubigeo": "010101"
          },
          {
            "name": "Asuncion",
            "sku": "02",
            "skuDepPro": "0101",
            "ubigeo": "010102"
          },
          {
            "name": "Balsas",
            "sku": "03",
            "skuDepPro": "0101",
            "ubigeo": "010103"
          },
          {
            "name": "Cheto",
            "sku": "04",
            "skuDepPro": "0101",
            "ubigeo": "010104"
          },
          {
            "name": "Chiliquin",
            "sku": "05",
            "skuDepPro": "0101",
            "ubigeo": "010105"
          },
          {
            "name": "Chuquibamba",
            "sku": "06",
            "skuDepPro": "0101",
            "ubigeo": "010106"
          },
          {
            "name": "Granada",
            "sku": "07",
            "skuDepPro": "0101",
            "ubigeo": "010107"
          },
          {
            "name": "Huancas",
            "sku": "08",
            "skuDepPro": "0101",
            "ubigeo": "010108"
          },
          {
            "name": "La Jalca",
            "sku": "09",
            "skuDepPro": "0101",
            "ubigeo": "010109"
          },
          {
            "name": "Leimebamba",
            "sku": "10",
            "skuDepPro": "0101",
            "ubigeo": "010110"
          },
          {
            "name": "Levanto",
            "sku": "11",
            "skuDepPro": "0101",
            "ubigeo": "010111"
          },
          {
            "name": "Magdalena",
            "sku": "12",
            "skuDepPro": "0101",
            "ubigeo": "010112"
          },
          {
            "name": "Mariscal Castilla",
            "sku": "13",
            "skuDepPro": "0101",
            "ubigeo": "010113"
          },
          {
            "name": "Molinopampa",
            "sku": "14",
            "skuDepPro": "0101",
            "ubigeo": "010114"
          },
          {
            "name": "Montevideo",
            "sku": "15",
            "skuDepPro": "0101",
            "ubigeo": "010115"
          },
          {
            "name": "Olleros",
            "sku": "16",
            "skuDepPro": "0101",
            "ubigeo": "010116"
          },
          {
            "name": "Quinjalca",
            "sku": "17",
            "skuDepPro": "0101",
            "ubigeo": "010117"
          },
          {
            "name": "San Francisco de Daguas",
            "sku": "18",
            "skuDepPro": "0101",
            "ubigeo": "010118"
          },
          {
            "name": "San Isidro de Maino",
            "sku": "19",
            "skuDepPro": "0101",
            "ubigeo": "010119"
          },
          {
            "name": "Soloco",
            "sku": "20",
            "skuDepPro": "0101",
            "ubigeo": "010120"
          },
          {
            "name": "Sonche",
            "sku": "21",
            "skuDepPro": "0101",
            "ubigeo": "010121"
          }
        ]
      },
      {
        "name": "Bagua",
        "sku": "02",
        "skuDep": "01",
        "distritos": [
          {
            "name": "Bagua",
            "sku": "01",
            "skuDepPro": "0102",
            "ubigeo": "010201"
          },
          {
            "name": "Aramango",
            "sku": "02",
            "skuDepPro": "0102",
            "ubigeo": "010202"
          },
          {
            "name": "Copallin",
            "sku": "03",
            "skuDepPro": "0102",
            "ubigeo": "010203"
          },
          {
            "name": "El Parco",
            "sku": "04",
            "skuDepPro": "0102",
            "ubigeo": "010204"
          },
          {
            "name": "Imaza",
            "sku": "05",
            "skuDepPro": "0102",
            "ubigeo": "010205"
          },
          {
            "name": "La Peca",
            "sku": "06",
            "skuDepPro": "0102",
            "ubigeo": "010206"
          }
        ]
      },
      {
        "name": "Bongara",
        "sku": "03",
        "skuDep": "01",
        "distritos": [
          {
            "name": "Jumbilla",
            "sku": "01",
            "skuDepPro": "0103",
            "ubigeo": "010301"
          },
          {
            "name": "Chisquilla",
            "sku": "02",
            "skuDepPro": "0103",
            "ubigeo": "010302"
          },
          {
            "name": "Churuja",
            "sku": "03",
            "skuDepPro": "0103",
            "ubigeo": "010303"
          },
          {
            "name": "Corosha",
            "sku": "04",
            "skuDepPro": "0103",
            "ubigeo": "010304"
          },
          {
            "name": "Cuispes",
            "sku": "05",
            "skuDepPro": "0103",
            "ubigeo": "010305"
          },
          {
            "name": "Florida",
            "sku": "06",
            "skuDepPro": "0103",
            "ubigeo": "010306"
          },
          {
            "name": "Jazan",
            "sku": "07",
            "skuDepPro": "0103",
            "ubigeo": "010307"
          },
          {
            "name": "Recta",
            "sku": "08",
            "skuDepPro": "0103",
            "ubigeo": "010308"
          },
          {
            "name": "San Carlos",
            "sku": "09",
            "skuDepPro": "0103",
            "ubigeo": "010309"
          },
          {
            "name": "Shipasbamba",
            "sku": "10",
            "skuDepPro": "0103",
            "ubigeo": "010310"
          },
          {
            "name": "Valera",
            "sku": "11",
            "skuDepPro": "0103",
            "ubigeo": "010311"
          },
          {
            "name": "Yambrasbamba",
            "sku": "12",
            "skuDepPro": "0103",
            "ubigeo": "010312"
          }
        ]
      },
      {
        "name": "Condorcanqui",
        "sku": "04",
        "skuDep": "01",
        "distritos": [
          {
            "name": "Nieva",
            "sku": "01",
            "skuDepPro": "0104",
            "ubigeo": "010401"
          },
          {
            "name": "El Cenepa",
            "sku": "02",
            "skuDepPro": "0104",
            "ubigeo": "010402"
          },
          {
            "name": "Rio Santiago",
            "sku": "03",
            "skuDepPro": "0104",
            "ubigeo": "010403"
          }
        ]
      },
      {
        "name": "Luya",
        "sku": "05",
        "skuDep": "01",
        "distritos": [
          {
            "name": "Lamud",
            "sku": "01",
            "skuDepPro": "0105",
            "ubigeo": "010501"
          },
          {
            "name": "Camporredondo",
            "sku": "02",
            "skuDepPro": "0105",
            "ubigeo": "010502"
          },
          {
            "name": "Cocabamba",
            "sku": "03",
            "skuDepPro": "0105",
            "ubigeo": "010503"
          },
          {
            "name": "Colcamar",
            "sku": "04",
            "skuDepPro": "0105",
            "ubigeo": "010504"
          },
          {
            "name": "Conila",
            "sku": "05",
            "skuDepPro": "0105",
            "ubigeo": "010505"
          },
          {
            "name": "Inguilpata",
            "sku": "06",
            "skuDepPro": "0105",
            "ubigeo": "010506"
          },
          {
            "name": "Longuita",
            "sku": "07",
            "skuDepPro": "0105",
            "ubigeo": "010507"
          },
          {
            "name": "Lonya Chico",
            "sku": "08",
            "skuDepPro": "0105",
            "ubigeo": "010508"
          },
          {
            "name": "Luya",
            "sku": "09",
            "skuDepPro": "0105",
            "ubigeo": "010509"
          },
          {
            "name": "Luya Viejo",
            "sku": "10",
            "skuDepPro": "0105",
            "ubigeo": "010510"
          },
          {
            "name": "Maria",
            "sku": "11",
            "skuDepPro": "0105",
            "ubigeo": "010511"
          },
          {
            "name": "Ocalli",
            "sku": "12",
            "skuDepPro": "0105",
            "ubigeo": "010512"
          },
          {
            "name": "Ocumal",
            "sku": "13",
            "skuDepPro": "0105",
            "ubigeo": "010513"
          },
          {
            "name": "Pisuquia",
            "sku": "14",
            "skuDepPro": "0105",
            "ubigeo": "010514"
          },
          {
            "name": "Providencia",
            "sku": "15",
            "skuDepPro": "0105",
            "ubigeo": "010515"
          },
          {
            "name": "San Cristobal",
            "sku": "16",
            "skuDepPro": "0105",
            "ubigeo": "010516"
          },
          {
            "name": "San Francisco de Yeso",
            "sku": "17",
            "skuDepPro": "0105",
            "ubigeo": "010517"
          },
          {
            "name": "San Jeronimo",
            "sku": "18",
            "skuDepPro": "0105",
            "ubigeo": "010518"
          },
          {
            "name": "San Juan de Lopecancha",
            "sku": "19",
            "skuDepPro": "0105",
            "ubigeo": "010519"
          },
          {
            "name": "Santa Catalina",
            "sku": "20",
            "skuDepPro": "0105",
            "ubigeo": "010520"
          },
          {
            "name": "Santo Tomas",
            "sku": "21",
            "skuDepPro": "0105",
            "ubigeo": "010521"
          },
          {
            "name": "Tingo",
            "sku": "22",
            "skuDepPro": "0105",
            "ubigeo": "010522"
          },
          {
            "name": "Trita",
            "sku": "23",
            "skuDepPro": "0105",
            "ubigeo": "010523"
          }
        ]
      },
      {
        "name": "Rodriguez de Mendoza",
        "sku": "06",
        "skuDep": "01",
        "distritos": [
          {
            "name": "San Nicolas",
            "sku": "01",
            "skuDepPro": "0106",
            "ubigeo": "010601"
          },
          {
            "name": "Chirimoto",
            "sku": "02",
            "skuDepPro": "0106",
            "ubigeo": "010602"
          },
          {
            "name": "Cochamal",
            "sku": "03",
            "skuDepPro": "0106",
            "ubigeo": "010603"
          },
          {
            "name": "Huambo",
            "sku": "04",
            "skuDepPro": "0106",
            "ubigeo": "010604"
          },
          {
            "name": "Limabamba",
            "sku": "05",
            "skuDepPro": "0106",
            "ubigeo": "010605"
          },
          {
            "name": "Longar",
            "sku": "06",
            "skuDepPro": "0106",
            "ubigeo": "010606"
          },
          {
            "name": "Mariscal Benavides",
            "sku": "07",
            "skuDepPro": "0106",
            "ubigeo": "010607"
          },
          {
            "name": "Milpuc",
            "sku": "08",
            "skuDepPro": "0106",
            "ubigeo": "010608"
          },
          {
            "name": "Omia",
            "sku": "09",
            "skuDepPro": "0106",
            "ubigeo": "010609"
          },
          {
            "name": "Santa Rosa",
            "sku": "10",
            "skuDepPro": "0106",
            "ubigeo": "010610"
          },
          {
            "name": "Totora",
            "sku": "11",
            "skuDepPro": "0106",
            "ubigeo": "010611"
          },
          {
            "name": "Vista Alegre",
            "sku": "12",
            "skuDepPro": "0106",
            "ubigeo": "010612"
          }
        ]
      },
      {
        "name": "Utcubamba",
        "sku": "07",
        "skuDep": "01",
        "distritos": [
          {
            "name": "Bagua Grande",
            "sku": "01",
            "skuDepPro": "0107",
            "ubigeo": "010701"
          },
          {
            "name": "Cajaruro",
            "sku": "02",
            "skuDepPro": "0107",
            "ubigeo": "010702"
          },
          {
            "name": "Cumba",
            "sku": "03",
            "skuDepPro": "0107",
            "ubigeo": "010703"
          },
          {
            "name": "El Milagro",
            "sku": "04",
            "skuDepPro": "0107",
            "ubigeo": "010704"
          },
          {
            "name": "Jamalca",
            "sku": "05",
            "skuDepPro": "0107",
            "ubigeo": "010705"
          },
          {
            "name": "Lonya Grande",
            "sku": "06",
            "skuDepPro": "0107",
            "ubigeo": "010706"
          },
          {
            "name": "Yamon",
            "sku": "07",
            "skuDepPro": "0107",
            "ubigeo": "010707"
          }
        ]
      }
    ]
  },
  {
    "name": "Ancash",
    "sku": "02",
    "provincias": [
      {
        "name": "Huaraz",
        "sku": "01",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Huaraz",
            "sku": "01",
            "skuDepPro": "0201",
            "ubigeo": "020101"
          },
          {
            "name": "Cochabamba",
            "sku": "02",
            "skuDepPro": "0201",
            "ubigeo": "020102"
          },
          {
            "name": "Colcabamba",
            "sku": "03",
            "skuDepPro": "0201",
            "ubigeo": "020103"
          },
          {
            "name": "Huanchay",
            "sku": "04",
            "skuDepPro": "0201",
            "ubigeo": "020104"
          },
          {
            "name": "Independencia",
            "sku": "05",
            "skuDepPro": "0201",
            "ubigeo": "020105"
          },
          {
            "name": "Jangas",
            "sku": "06",
            "skuDepPro": "0201",
            "ubigeo": "020106"
          },
          {
            "name": "La Libertad",
            "sku": "07",
            "skuDepPro": "0201",
            "ubigeo": "020107"
          },
          {
            "name": "Olleros",
            "sku": "08",
            "skuDepPro": "0201",
            "ubigeo": "020108"
          },
          {
            "name": "Pampas Grande",
            "sku": "09",
            "skuDepPro": "0201",
            "ubigeo": "020109"
          },
          {
            "name": "Pariacoto",
            "sku": "10",
            "skuDepPro": "0201",
            "ubigeo": "020110"
          },
          {
            "name": "Pira",
            "sku": "11",
            "skuDepPro": "0201",
            "ubigeo": "020111"
          },
          {
            "name": "Tarica",
            "sku": "12",
            "skuDepPro": "0201",
            "ubigeo": "020112"
          }
        ]
      },
      {
        "name": "Aija",
        "sku": "02",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Aija",
            "sku": "01",
            "skuDepPro": "0202",
            "ubigeo": "020201"
          },
          {
            "name": "Coris",
            "sku": "02",
            "skuDepPro": "0202",
            "ubigeo": "020202"
          },
          {
            "name": "Huacllan",
            "sku": "03",
            "skuDepPro": "0202",
            "ubigeo": "020203"
          },
          {
            "name": "La Merced",
            "sku": "04",
            "skuDepPro": "0202",
            "ubigeo": "020204"
          },
          {
            "name": "Succha",
            "sku": "05",
            "skuDepPro": "0202",
            "ubigeo": "020205"
          }
        ]
      },
      {
        "name": "Antonio Raymondi",
        "sku": "03",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Llamellin",
            "sku": "01",
            "skuDepPro": "0203",
            "ubigeo": "020301"
          },
          {
            "name": "Aczo",
            "sku": "02",
            "skuDepPro": "0203",
            "ubigeo": "020302"
          },
          {
            "name": "Chaccho",
            "sku": "03",
            "skuDepPro": "0203",
            "ubigeo": "020303"
          },
          {
            "name": "Chingas",
            "sku": "04",
            "skuDepPro": "0203",
            "ubigeo": "020304"
          },
          {
            "name": "Mirgas",
            "sku": "05",
            "skuDepPro": "0203",
            "ubigeo": "020305"
          },
          {
            "name": "San Juan de Rontoy",
            "sku": "06",
            "skuDepPro": "0203",
            "ubigeo": "020306"
          }
        ]
      },
      {
        "name": "Asuncion",
        "sku": "04",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Chacas",
            "sku": "01",
            "skuDepPro": "0204",
            "ubigeo": "020401"
          },
          {
            "name": "Acochaca",
            "sku": "02",
            "skuDepPro": "0204",
            "ubigeo": "020402"
          }
        ]
      },
      {
        "name": "Bolognesi",
        "sku": "05",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Chiquian",
            "sku": "01",
            "skuDepPro": "0205",
            "ubigeo": "020501"
          },
          {
            "name": "Abelardo Pardo Lezameta",
            "sku": "02",
            "skuDepPro": "0205",
            "ubigeo": "020502"
          },
          {
            "name": "Antonio Raymondi",
            "sku": "03",
            "skuDepPro": "0205",
            "ubigeo": "020503"
          },
          {
            "name": "Aquia",
            "sku": "04",
            "skuDepPro": "0205",
            "ubigeo": "020504"
          },
          {
            "name": "Cajacay",
            "sku": "05",
            "skuDepPro": "0205",
            "ubigeo": "020505"
          },
          {
            "name": "Canis",
            "sku": "06",
            "skuDepPro": "0205",
            "ubigeo": "020506"
          },
          {
            "name": "Colquioc",
            "sku": "07",
            "skuDepPro": "0205",
            "ubigeo": "020507"
          },
          {
            "name": "Huallanca",
            "sku": "08",
            "skuDepPro": "0205",
            "ubigeo": "020508"
          },
          {
            "name": "Huasta",
            "sku": "09",
            "skuDepPro": "0205",
            "ubigeo": "020509"
          },
          {
            "name": "Huayllacayan",
            "sku": "10",
            "skuDepPro": "0205",
            "ubigeo": "020510"
          },
          {
            "name": "La Primavera",
            "sku": "11",
            "skuDepPro": "0205",
            "ubigeo": "020511"
          },
          {
            "name": "Mangas",
            "sku": "12",
            "skuDepPro": "0205",
            "ubigeo": "020512"
          },
          {
            "name": "Pacllon",
            "sku": "13",
            "skuDepPro": "0205",
            "ubigeo": "020513"
          },
          {
            "name": "San Miguel de Corpanqui",
            "sku": "14",
            "skuDepPro": "0205",
            "ubigeo": "020514"
          },
          {
            "name": "Ticllos",
            "sku": "15",
            "skuDepPro": "0205",
            "ubigeo": "020515"
          }
        ]
      },
      {
        "name": "Carhuaz",
        "sku": "06",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Carhuaz",
            "sku": "01",
            "skuDepPro": "0206",
            "ubigeo": "020601"
          },
          {
            "name": "Acopampa",
            "sku": "02",
            "skuDepPro": "0206",
            "ubigeo": "020602"
          },
          {
            "name": "Amashca",
            "sku": "03",
            "skuDepPro": "0206",
            "ubigeo": "020603"
          },
          {
            "name": "Anta",
            "sku": "04",
            "skuDepPro": "0206",
            "ubigeo": "020604"
          },
          {
            "name": "Ataquero",
            "sku": "05",
            "skuDepPro": "0206",
            "ubigeo": "020605"
          },
          {
            "name": "Marcara",
            "sku": "06",
            "skuDepPro": "0206",
            "ubigeo": "020606"
          },
          {
            "name": "Pariahuanca",
            "sku": "07",
            "skuDepPro": "0206",
            "ubigeo": "020607"
          },
          {
            "name": "San Miguel de Aco",
            "sku": "08",
            "skuDepPro": "0206",
            "ubigeo": "020608"
          },
          {
            "name": "Shilla",
            "sku": "09",
            "skuDepPro": "0206",
            "ubigeo": "020609"
          },
          {
            "name": "Tinco",
            "sku": "10",
            "skuDepPro": "0206",
            "ubigeo": "020610"
          },
          {
            "name": "Yungar",
            "sku": "11",
            "skuDepPro": "0206",
            "ubigeo": "020611"
          }
        ]
      },
      {
        "name": "Carlos Fermin Fitzcarrald",
        "sku": "07",
        "skuDep": "02",
        "distritos": [
          {
            "name": "San Luis",
            "sku": "01",
            "skuDepPro": "0207",
            "ubigeo": "020701"
          },
          {
            "name": "San Nicolas",
            "sku": "02",
            "skuDepPro": "0207",
            "ubigeo": "020702"
          },
          {
            "name": "Yauya",
            "sku": "03",
            "skuDepPro": "0207",
            "ubigeo": "020703"
          }
        ]
      },
      {
        "name": "Casma",
        "sku": "08",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Casma",
            "sku": "01",
            "skuDepPro": "0208",
            "ubigeo": "020801"
          },
          {
            "name": "Buena Vista Alta",
            "sku": "02",
            "skuDepPro": "0208",
            "ubigeo": "020802"
          },
          {
            "name": "Comandante Noel",
            "sku": "03",
            "skuDepPro": "0208",
            "ubigeo": "020803"
          },
          {
            "name": "Yautan",
            "sku": "04",
            "skuDepPro": "0208",
            "ubigeo": "020804"
          }
        ]
      },
      {
        "name": "Corongo",
        "sku": "09",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Corongo",
            "sku": "01",
            "skuDepPro": "0209",
            "ubigeo": "020901"
          },
          {
            "name": "Aco",
            "sku": "02",
            "skuDepPro": "0209",
            "ubigeo": "020902"
          },
          {
            "name": "Bambas",
            "sku": "03",
            "skuDepPro": "0209",
            "ubigeo": "020903"
          },
          {
            "name": "Cusca",
            "sku": "04",
            "skuDepPro": "0209",
            "ubigeo": "020904"
          },
          {
            "name": "La Pampa",
            "sku": "05",
            "skuDepPro": "0209",
            "ubigeo": "020905"
          },
          {
            "name": "Yanac",
            "sku": "06",
            "skuDepPro": "0209",
            "ubigeo": "020906"
          },
          {
            "name": "Yupan",
            "sku": "07",
            "skuDepPro": "0209",
            "ubigeo": "020907"
          }
        ]
      },
      {
        "name": "Huari",
        "sku": "10",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Huari",
            "sku": "01",
            "skuDepPro": "0210",
            "ubigeo": "021001"
          },
          {
            "name": "Anra",
            "sku": "02",
            "skuDepPro": "0210",
            "ubigeo": "021002"
          },
          {
            "name": "Cajay",
            "sku": "03",
            "skuDepPro": "0210",
            "ubigeo": "021003"
          },
          {
            "name": "Chavin de Huantar",
            "sku": "04",
            "skuDepPro": "0210",
            "ubigeo": "021004"
          },
          {
            "name": "Huacachi",
            "sku": "05",
            "skuDepPro": "0210",
            "ubigeo": "021005"
          },
          {
            "name": "Huacchis",
            "sku": "06",
            "skuDepPro": "0210",
            "ubigeo": "021006"
          },
          {
            "name": "Huachis",
            "sku": "07",
            "skuDepPro": "0210",
            "ubigeo": "021007"
          },
          {
            "name": "Huantar",
            "sku": "08",
            "skuDepPro": "0210",
            "ubigeo": "021008"
          },
          {
            "name": "Masin",
            "sku": "09",
            "skuDepPro": "0210",
            "ubigeo": "021009"
          },
          {
            "name": "Paucas",
            "sku": "10",
            "skuDepPro": "0210",
            "ubigeo": "021010"
          },
          {
            "name": "Ponto",
            "sku": "11",
            "skuDepPro": "0210",
            "ubigeo": "021011"
          },
          {
            "name": "Rahuapampa",
            "sku": "12",
            "skuDepPro": "0210",
            "ubigeo": "021012"
          },
          {
            "name": "Rapayan",
            "sku": "13",
            "skuDepPro": "0210",
            "ubigeo": "021013"
          },
          {
            "name": "San Marcos",
            "sku": "14",
            "skuDepPro": "0210",
            "ubigeo": "021014"
          },
          {
            "name": "San Pedro de Chana",
            "sku": "15",
            "skuDepPro": "0210",
            "ubigeo": "021015"
          },
          {
            "name": "Uco",
            "sku": "16",
            "skuDepPro": "0210",
            "ubigeo": "021016"
          }
        ]
      },
      {
        "name": "Huarmey",
        "sku": "11",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Huarmey",
            "sku": "01",
            "skuDepPro": "0211",
            "ubigeo": "021101"
          },
          {
            "name": "Cochapeti",
            "sku": "02",
            "skuDepPro": "0211",
            "ubigeo": "021102"
          },
          {
            "name": "Culebras",
            "sku": "03",
            "skuDepPro": "0211",
            "ubigeo": "021103"
          },
          {
            "name": "Huayan",
            "sku": "04",
            "skuDepPro": "0211",
            "ubigeo": "021104"
          },
          {
            "name": "Malvas",
            "sku": "05",
            "skuDepPro": "0211",
            "ubigeo": "021105"
          }
        ]
      },
      {
        "name": "Huaylas",
        "sku": "12",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Caraz",
            "sku": "01",
            "skuDepPro": "0212",
            "ubigeo": "021201"
          },
          {
            "name": "Huallanca",
            "sku": "02",
            "skuDepPro": "0212",
            "ubigeo": "021202"
          },
          {
            "name": "Huata",
            "sku": "03",
            "skuDepPro": "0212",
            "ubigeo": "021203"
          },
          {
            "name": "Huaylas",
            "sku": "04",
            "skuDepPro": "0212",
            "ubigeo": "021204"
          },
          {
            "name": "Mato",
            "sku": "05",
            "skuDepPro": "0212",
            "ubigeo": "021205"
          },
          {
            "name": "Pamparomas",
            "sku": "06",
            "skuDepPro": "0212",
            "ubigeo": "021206"
          },
          {
            "name": "Pueblo Libre",
            "sku": "07",
            "skuDepPro": "0212",
            "ubigeo": "021207"
          },
          {
            "name": "Santa Cruz",
            "sku": "08",
            "skuDepPro": "0212",
            "ubigeo": "021208"
          },
          {
            "name": "Santo Toribio",
            "sku": "09",
            "skuDepPro": "0212",
            "ubigeo": "021209"
          },
          {
            "name": "Yuracmarca",
            "sku": "10",
            "skuDepPro": "0212",
            "ubigeo": "021210"
          }
        ]
      },
      {
        "name": "Mariscal Luzuriaga",
        "sku": "13",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Piscobamba",
            "sku": "01",
            "skuDepPro": "0213",
            "ubigeo": "021301"
          },
          {
            "name": "Casca",
            "sku": "02",
            "skuDepPro": "0213",
            "ubigeo": "021302"
          },
          {
            "name": "Eleazar Guzman Barron",
            "sku": "03",
            "skuDepPro": "0213",
            "ubigeo": "021303"
          },
          {
            "name": "Fidel Olivas Escudero",
            "sku": "04",
            "skuDepPro": "0213",
            "ubigeo": "021304"
          },
          {
            "name": "Llama",
            "sku": "05",
            "skuDepPro": "0213",
            "ubigeo": "021305"
          },
          {
            "name": "Llumpa",
            "sku": "06",
            "skuDepPro": "0213",
            "ubigeo": "021306"
          },
          {
            "name": "Lucma",
            "sku": "07",
            "skuDepPro": "0213",
            "ubigeo": "021307"
          },
          {
            "name": "Musga",
            "sku": "08",
            "skuDepPro": "0213",
            "ubigeo": "021308"
          }
        ]
      },
      {
        "name": "Ocros",
        "sku": "14",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Ocros",
            "sku": "01",
            "skuDepPro": "0214",
            "ubigeo": "021401"
          },
          {
            "name": "Acas",
            "sku": "02",
            "skuDepPro": "0214",
            "ubigeo": "021402"
          },
          {
            "name": "Cajamarquilla",
            "sku": "03",
            "skuDepPro": "0214",
            "ubigeo": "021403"
          },
          {
            "name": "Carhuapampa",
            "sku": "04",
            "skuDepPro": "0214",
            "ubigeo": "021404"
          },
          {
            "name": "Cochas",
            "sku": "05",
            "skuDepPro": "0214",
            "ubigeo": "021405"
          },
          {
            "name": "Congas",
            "sku": "06",
            "skuDepPro": "0214",
            "ubigeo": "021406"
          },
          {
            "name": "Llipa",
            "sku": "07",
            "skuDepPro": "0214",
            "ubigeo": "021407"
          },
          {
            "name": "San Cristobal de Rajan",
            "sku": "08",
            "skuDepPro": "0214",
            "ubigeo": "021408"
          },
          {
            "name": "San Pedro",
            "sku": "09",
            "skuDepPro": "0214",
            "ubigeo": "021409"
          },
          {
            "name": "Santiago de Chilcas",
            "sku": "10",
            "skuDepPro": "0214",
            "ubigeo": "021410"
          }
        ]
      },
      {
        "name": "Pallasca",
        "sku": "15",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Cabana",
            "sku": "01",
            "skuDepPro": "0215",
            "ubigeo": "021501"
          },
          {
            "name": "Bolognesi",
            "sku": "02",
            "skuDepPro": "0215",
            "ubigeo": "021502"
          },
          {
            "name": "Conchucos",
            "sku": "03",
            "skuDepPro": "0215",
            "ubigeo": "021503"
          },
          {
            "name": "Huacaschuque",
            "sku": "04",
            "skuDepPro": "0215",
            "ubigeo": "021504"
          },
          {
            "name": "Huandoval",
            "sku": "05",
            "skuDepPro": "0215",
            "ubigeo": "021505"
          },
          {
            "name": "Lacabamba",
            "sku": "06",
            "skuDepPro": "0215",
            "ubigeo": "021506"
          },
          {
            "name": "Llapo",
            "sku": "07",
            "skuDepPro": "0215",
            "ubigeo": "021507"
          },
          {
            "name": "Pallasca",
            "sku": "08",
            "skuDepPro": "0215",
            "ubigeo": "021508"
          },
          {
            "name": "Pampas",
            "sku": "09",
            "skuDepPro": "0215",
            "ubigeo": "021509"
          },
          {
            "name": "Santa Rosa",
            "sku": "10",
            "skuDepPro": "0215",
            "ubigeo": "021510"
          },
          {
            "name": "Tauca",
            "sku": "11",
            "skuDepPro": "0215",
            "ubigeo": "021511"
          }
        ]
      },
      {
        "name": "Pomabamba",
        "sku": "16",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Pomabamba",
            "sku": "01",
            "skuDepPro": "0216",
            "ubigeo": "021601"
          },
          {
            "name": "Huayllan",
            "sku": "02",
            "skuDepPro": "0216",
            "ubigeo": "021602"
          },
          {
            "name": "Parobamba",
            "sku": "03",
            "skuDepPro": "0216",
            "ubigeo": "021603"
          },
          {
            "name": "Quinuabamba",
            "sku": "04",
            "skuDepPro": "0216",
            "ubigeo": "021604"
          }
        ]
      },
      {
        "name": "Recuay",
        "sku": "17",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Recuay",
            "sku": "01",
            "skuDepPro": "0217",
            "ubigeo": "021701"
          },
          {
            "name": "Catac",
            "sku": "02",
            "skuDepPro": "0217",
            "ubigeo": "021702"
          },
          {
            "name": "Cotaparaco",
            "sku": "03",
            "skuDepPro": "0217",
            "ubigeo": "021703"
          },
          {
            "name": "Huayllapampa",
            "sku": "04",
            "skuDepPro": "0217",
            "ubigeo": "021704"
          },
          {
            "name": "Llacllin",
            "sku": "05",
            "skuDepPro": "0217",
            "ubigeo": "021705"
          },
          {
            "name": "Marca",
            "sku": "06",
            "skuDepPro": "0217",
            "ubigeo": "021706"
          },
          {
            "name": "Pampas Chico",
            "sku": "07",
            "skuDepPro": "0217",
            "ubigeo": "021707"
          },
          {
            "name": "Pararin",
            "sku": "08",
            "skuDepPro": "0217",
            "ubigeo": "021708"
          },
          {
            "name": "Tapacocha",
            "sku": "09",
            "skuDepPro": "0217",
            "ubigeo": "021709"
          },
          {
            "name": "Ticapampa",
            "sku": "10",
            "skuDepPro": "0217",
            "ubigeo": "021710"
          }
        ]
      },
      {
        "name": "Santa",
        "sku": "18",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Chimbote",
            "sku": "01",
            "skuDepPro": "0218",
            "ubigeo": "021801"
          },
          {
            "name": "Caceres del Peru",
            "sku": "02",
            "skuDepPro": "0218",
            "ubigeo": "021802"
          },
          {
            "name": "Coishco",
            "sku": "03",
            "skuDepPro": "0218",
            "ubigeo": "021803"
          },
          {
            "name": "Macate",
            "sku": "04",
            "skuDepPro": "0218",
            "ubigeo": "021804"
          },
          {
            "name": "Moro",
            "sku": "05",
            "skuDepPro": "0218",
            "ubigeo": "021805"
          },
          {
            "name": "Nepeña",
            "sku": "06",
            "skuDepPro": "0218",
            "ubigeo": "021806"
          },
          {
            "name": "Samanco",
            "sku": "07",
            "skuDepPro": "0218",
            "ubigeo": "021807"
          },
          {
            "name": "Santa",
            "sku": "08",
            "skuDepPro": "0218",
            "ubigeo": "021808"
          },
          {
            "name": "Nuevo Chimbote",
            "sku": "09",
            "skuDepPro": "0218",
            "ubigeo": "021809"
          }
        ]
      },
      {
        "name": "Sihuas",
        "sku": "19",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Sihuas",
            "sku": "01",
            "skuDepPro": "0219",
            "ubigeo": "021901"
          },
          {
            "name": "Acobamba",
            "sku": "02",
            "skuDepPro": "0219",
            "ubigeo": "021902"
          },
          {
            "name": "Alfonso Ugarte",
            "sku": "03",
            "skuDepPro": "0219",
            "ubigeo": "021903"
          },
          {
            "name": "Cashapampa",
            "sku": "04",
            "skuDepPro": "0219",
            "ubigeo": "021904"
          },
          {
            "name": "Chingalpo",
            "sku": "05",
            "skuDepPro": "0219",
            "ubigeo": "021905"
          },
          {
            "name": "Huayllabamba",
            "sku": "06",
            "skuDepPro": "0219",
            "ubigeo": "021906"
          },
          {
            "name": "Quiches",
            "sku": "07",
            "skuDepPro": "0219",
            "ubigeo": "021907"
          },
          {
            "name": "Ragash",
            "sku": "08",
            "skuDepPro": "0219",
            "ubigeo": "021908"
          },
          {
            "name": "San Juan",
            "sku": "09",
            "skuDepPro": "0219",
            "ubigeo": "021909"
          },
          {
            "name": "Sicsibamba",
            "sku": "10",
            "skuDepPro": "0219",
            "ubigeo": "021910"
          }
        ]
      },
      {
        "name": "Yungay",
        "sku": "20",
        "skuDep": "02",
        "distritos": [
          {
            "name": "Yungay",
            "sku": "01",
            "skuDepPro": "0220",
            "ubigeo": "022001"
          },
          {
            "name": "Cascapara",
            "sku": "02",
            "skuDepPro": "0220",
            "ubigeo": "022002"
          },
          {
            "name": "Mancos",
            "sku": "03",
            "skuDepPro": "0220",
            "ubigeo": "022003"
          },
          {
            "name": "Matacoto",
            "sku": "04",
            "skuDepPro": "0220",
            "ubigeo": "022004"
          },
          {
            "name": "Quillo",
            "sku": "05",
            "skuDepPro": "0220",
            "ubigeo": "022005"
          },
          {
            "name": "Ranrahirca",
            "sku": "06",
            "skuDepPro": "0220",
            "ubigeo": "022006"
          },
          {
            "name": "Shupluy",
            "sku": "07",
            "skuDepPro": "0220",
            "ubigeo": "022007"
          },
          {
            "name": "Yanama",
            "sku": "08",
            "skuDepPro": "0220",
            "ubigeo": "022008"
          }
        ]
      }
    ]
  },
  {
    "name": "Apurímac",
    "sku": "03",
    "provincias": [
      {
        "name": "Abancay",
        "sku": "01",
        "skuDep": "03",
        "distritos": [
          {
            "name": "Abancay",
            "sku": "01",
            "skuDepPro": "0301",
            "ubigeo": "030101"
          },
          {
            "name": "Chacoche",
            "sku": "02",
            "skuDepPro": "0301",
            "ubigeo": "030102"
          },
          {
            "name": "Circa",
            "sku": "03",
            "skuDepPro": "0301",
            "ubigeo": "030103"
          },
          {
            "name": "Curahuasi",
            "sku": "04",
            "skuDepPro": "0301",
            "ubigeo": "030104"
          },
          {
            "name": "Huanipaca",
            "sku": "05",
            "skuDepPro": "0301",
            "ubigeo": "030105"
          },
          {
            "name": "Lambrama",
            "sku": "06",
            "skuDepPro": "0301",
            "ubigeo": "030106"
          },
          {
            "name": "Pichirhua",
            "sku": "07",
            "skuDepPro": "0301",
            "ubigeo": "030107"
          },
          {
            "name": "San Pedro de Cachora",
            "sku": "08",
            "skuDepPro": "0301",
            "ubigeo": "030108"
          },
          {
            "name": "Tamburco",
            "sku": "09",
            "skuDepPro": "0301",
            "ubigeo": "030109"
          }
        ]
      },
      {
        "name": "Andahuaylas",
        "sku": "02",
        "skuDep": "03",
        "distritos": [
          {
            "name": "Andahuaylas",
            "sku": "01",
            "skuDepPro": "0302",
            "ubigeo": "030201"
          },
          {
            "name": "Andarapa",
            "sku": "02",
            "skuDepPro": "0302",
            "ubigeo": "030202"
          },
          {
            "name": "Chiara",
            "sku": "03",
            "skuDepPro": "0302",
            "ubigeo": "030203"
          },
          {
            "name": "Huancarama",
            "sku": "04",
            "skuDepPro": "0302",
            "ubigeo": "030204"
          },
          {
            "name": "Huancaray",
            "sku": "05",
            "skuDepPro": "0302",
            "ubigeo": "030205"
          },
          {
            "name": "Huayana",
            "sku": "06",
            "skuDepPro": "0302",
            "ubigeo": "030206"
          },
          {
            "name": "Kishuara",
            "sku": "07",
            "skuDepPro": "0302",
            "ubigeo": "030207"
          },
          {
            "name": "Pacobamba",
            "sku": "08",
            "skuDepPro": "0302",
            "ubigeo": "030208"
          },
          {
            "name": "Pacucha",
            "sku": "09",
            "skuDepPro": "0302",
            "ubigeo": "030209"
          },
          {
            "name": "Pampachiri",
            "sku": "10",
            "skuDepPro": "0302",
            "ubigeo": "030210"
          },
          {
            "name": "Pomacocha",
            "sku": "11",
            "skuDepPro": "0302",
            "ubigeo": "030211"
          },
          {
            "name": "San Antonio de Cachi",
            "sku": "12",
            "skuDepPro": "0302",
            "ubigeo": "030212"
          },
          {
            "name": "San Jeronimo",
            "sku": "13",
            "skuDepPro": "0302",
            "ubigeo": "030213"
          },
          {
            "name": "San Miguel de Chaccrampa",
            "sku": "14",
            "skuDepPro": "0302",
            "ubigeo": "030214"
          },
          {
            "name": "Santa Maria de Chicmo",
            "sku": "15",
            "skuDepPro": "0302",
            "ubigeo": "030215"
          },
          {
            "name": "Talavera",
            "sku": "16",
            "skuDepPro": "0302",
            "ubigeo": "030216"
          },
          {
            "name": "Tumay Huaraca",
            "sku": "17",
            "skuDepPro": "0302",
            "ubigeo": "030217"
          },
          {
            "name": "Turpo",
            "sku": "18",
            "skuDepPro": "0302",
            "ubigeo": "030218"
          },
          {
            "name": "Kaquiabamba",
            "sku": "19",
            "skuDepPro": "0302",
            "ubigeo": "030219"
          },
          {
            "name": "Jose Maria Arguedas",
            "sku": "20",
            "skuDepPro": "0302",
            "ubigeo": "030220"
          }
        ]
      },
      {
        "name": "Antabamba",
        "sku": "03",
        "skuDep": "03",
        "distritos": [
          {
            "name": "Antabamba",
            "sku": "01",
            "skuDepPro": "0303",
            "ubigeo": "030301"
          },
          {
            "name": "El Oro",
            "sku": "02",
            "skuDepPro": "0303",
            "ubigeo": "030302"
          },
          {
            "name": "Huaquirca",
            "sku": "03",
            "skuDepPro": "0303",
            "ubigeo": "030303"
          },
          {
            "name": "Juan Espinoza Medrano",
            "sku": "04",
            "skuDepPro": "0303",
            "ubigeo": "030304"
          },
          {
            "name": "Oropesa",
            "sku": "05",
            "skuDepPro": "0303",
            "ubigeo": "030305"
          },
          {
            "name": "Pachaconas",
            "sku": "06",
            "skuDepPro": "0303",
            "ubigeo": "030306"
          },
          {
            "name": "Sabaino",
            "sku": "07",
            "skuDepPro": "0303",
            "ubigeo": "030307"
          }
        ]
      },
      {
        "name": "Aymaraes",
        "sku": "04",
        "skuDep": "03",
        "distritos": [
          {
            "name": "Chalhuanca",
            "sku": "01",
            "skuDepPro": "0304",
            "ubigeo": "030401"
          },
          {
            "name": "Capaya",
            "sku": "02",
            "skuDepPro": "0304",
            "ubigeo": "030402"
          },
          {
            "name": "Caraybamba",
            "sku": "03",
            "skuDepPro": "0304",
            "ubigeo": "030403"
          },
          {
            "name": "Chapimarca",
            "sku": "04",
            "skuDepPro": "0304",
            "ubigeo": "030404"
          },
          {
            "name": "Colcabamba",
            "sku": "05",
            "skuDepPro": "0304",
            "ubigeo": "030405"
          },
          {
            "name": "Cotaruse",
            "sku": "06",
            "skuDepPro": "0304",
            "ubigeo": "030406"
          },
          {
            "name": "Ihuayllo",
            "sku": "07",
            "skuDepPro": "0304",
            "ubigeo": "030407"
          },
          {
            "name": "Justo Apu Sahuaraura",
            "sku": "08",
            "skuDepPro": "0304",
            "ubigeo": "030408"
          },
          {
            "name": "Lucre",
            "sku": "09",
            "skuDepPro": "0304",
            "ubigeo": "030409"
          },
          {
            "name": "Pocohuanca",
            "sku": "10",
            "skuDepPro": "0304",
            "ubigeo": "030410"
          },
          {
            "name": "San Juan de Chacña",
            "sku": "11",
            "skuDepPro": "0304",
            "ubigeo": "030411"
          },
          {
            "name": "Sañayca",
            "sku": "12",
            "skuDepPro": "0304",
            "ubigeo": "030412"
          },
          {
            "name": "Soraya",
            "sku": "13",
            "skuDepPro": "0304",
            "ubigeo": "030413"
          },
          {
            "name": "Tapairihua",
            "sku": "14",
            "skuDepPro": "0304",
            "ubigeo": "030414"
          },
          {
            "name": "Tintay",
            "sku": "15",
            "skuDepPro": "0304",
            "ubigeo": "030415"
          },
          {
            "name": "Toraya",
            "sku": "16",
            "skuDepPro": "0304",
            "ubigeo": "030416"
          },
          {
            "name": "Yanaca",
            "sku": "17",
            "skuDepPro": "0304",
            "ubigeo": "030417"
          }
        ]
      },
      {
        "name": "Cotabambas",
        "sku": "05",
        "skuDep": "03",
        "distritos": [
          {
            "name": "Tambobamba",
            "sku": "01",
            "skuDepPro": "0305",
            "ubigeo": "030501"
          },
          {
            "name": "Cotabambas",
            "sku": "02",
            "skuDepPro": "0305",
            "ubigeo": "030502"
          },
          {
            "name": "Coyllurqui",
            "sku": "03",
            "skuDepPro": "0305",
            "ubigeo": "030503"
          },
          {
            "name": "Haquira",
            "sku": "04",
            "skuDepPro": "0305",
            "ubigeo": "030504"
          },
          {
            "name": "Mara",
            "sku": "05",
            "skuDepPro": "0305",
            "ubigeo": "030505"
          },
          {
            "name": "Challhuahuacho",
            "sku": "06",
            "skuDepPro": "0305",
            "ubigeo": "030506"
          }
        ]
      },
      {
        "name": "Chincheros",
        "sku": "06",
        "skuDep": "03",
        "distritos": [
          {
            "name": "Chincheros",
            "sku": "01",
            "skuDepPro": "0306",
            "ubigeo": "030601"
          },
          {
            "name": "Anco_Huallo",
            "sku": "02",
            "skuDepPro": "0306",
            "ubigeo": "030602"
          },
          {
            "name": "Cocharcas",
            "sku": "03",
            "skuDepPro": "0306",
            "ubigeo": "030603"
          },
          {
            "name": "Huaccana",
            "sku": "04",
            "skuDepPro": "0306",
            "ubigeo": "030604"
          },
          {
            "name": "Ocobamba",
            "sku": "05",
            "skuDepPro": "0306",
            "ubigeo": "030605"
          },
          {
            "name": "Ongoy",
            "sku": "06",
            "skuDepPro": "0306",
            "ubigeo": "030606"
          },
          {
            "name": "Uranmarca",
            "sku": "07",
            "skuDepPro": "0306",
            "ubigeo": "030607"
          },
          {
            "name": "Ranracancha",
            "sku": "08",
            "skuDepPro": "0306",
            "ubigeo": "030608"
          },
          {
            "name": "Rocchacc",
            "sku": "09",
            "skuDepPro": "0306",
            "ubigeo": "030609"
          },
          {
            "name": "El Porvenir",
            "sku": "10",
            "skuDepPro": "0306",
            "ubigeo": "030610"
          },
          {
            "name": "Los Chankas",
            "sku": "11",
            "skuDepPro": "0306",
            "ubigeo": "030611"
          }
        ]
      },
      {
        "name": "Grau",
        "sku": "07",
        "skuDep": "03",
        "distritos": [
          {
            "name": "Chuquibambilla",
            "sku": "01",
            "skuDepPro": "0307",
            "ubigeo": "030701"
          },
          {
            "name": "Curpahuasi",
            "sku": "02",
            "skuDepPro": "0307",
            "ubigeo": "030702"
          },
          {
            "name": "Gamarra",
            "sku": "03",
            "skuDepPro": "0307",
            "ubigeo": "030703"
          },
          {
            "name": "Huayllati",
            "sku": "04",
            "skuDepPro": "0307",
            "ubigeo": "030704"
          },
          {
            "name": "Mamara",
            "sku": "05",
            "skuDepPro": "0307",
            "ubigeo": "030705"
          },
          {
            "name": "Micaela Bastidas",
            "sku": "06",
            "skuDepPro": "0307",
            "ubigeo": "030706"
          },
          {
            "name": "Pataypampa",
            "sku": "07",
            "skuDepPro": "0307",
            "ubigeo": "030707"
          },
          {
            "name": "Progreso",
            "sku": "08",
            "skuDepPro": "0307",
            "ubigeo": "030708"
          },
          {
            "name": "San Antonio",
            "sku": "09",
            "skuDepPro": "0307",
            "ubigeo": "030709"
          },
          {
            "name": "Santa Rosa",
            "sku": "10",
            "skuDepPro": "0307",
            "ubigeo": "030710"
          },
          {
            "name": "Turpay",
            "sku": "11",
            "skuDepPro": "0307",
            "ubigeo": "030711"
          },
          {
            "name": "Vilcabamba",
            "sku": "12",
            "skuDepPro": "0307",
            "ubigeo": "030712"
          },
          {
            "name": "Virundo",
            "sku": "13",
            "skuDepPro": "0307",
            "ubigeo": "030713"
          },
          {
            "name": "Curasco",
            "sku": "14",
            "skuDepPro": "0307",
            "ubigeo": "030714"
          }
        ]
      }
    ]
  },
  {
    "name": "Arequipa",
    "sku": "04",
    "provincias": [
      {
        "name": "Arequipa",
        "sku": "01",
        "skuDep": "04",
        "distritos": [
          {
            "name": "Arequipa",
            "sku": "01",
            "skuDepPro": "0401",
            "ubigeo": "040101"
          },
          {
            "name": "Alto Selva Alegre",
            "sku": "02",
            "skuDepPro": "0401",
            "ubigeo": "040102"
          },
          {
            "name": "Cayma",
            "sku": "03",
            "skuDepPro": "0401",
            "ubigeo": "040103"
          },
          {
            "name": "Cerro Colorado",
            "sku": "04",
            "skuDepPro": "0401",
            "ubigeo": "040104"
          },
          {
            "name": "Characato",
            "sku": "05",
            "skuDepPro": "0401",
            "ubigeo": "040105"
          },
          {
            "name": "Chiguata",
            "sku": "06",
            "skuDepPro": "0401",
            "ubigeo": "040106"
          },
          {
            "name": "Jacobo Hunter",
            "sku": "07",
            "skuDepPro": "0401",
            "ubigeo": "040107"
          },
          {
            "name": "La Joya",
            "sku": "08",
            "skuDepPro": "0401",
            "ubigeo": "040108"
          },
          {
            "name": "Mariano Melgar",
            "sku": "09",
            "skuDepPro": "0401",
            "ubigeo": "040109"
          },
          {
            "name": "Miraflores",
            "sku": "10",
            "skuDepPro": "0401",
            "ubigeo": "040110"
          },
          {
            "name": "Mollebaya",
            "sku": "11",
            "skuDepPro": "0401",
            "ubigeo": "040111"
          },
          {
            "name": "Paucarpata",
            "sku": "12",
            "skuDepPro": "0401",
            "ubigeo": "040112"
          },
          {
            "name": "Pocsi",
            "sku": "13",
            "skuDepPro": "0401",
            "ubigeo": "040113"
          },
          {
            "name": "Polobaya",
            "sku": "14",
            "skuDepPro": "0401",
            "ubigeo": "040114"
          },
          {
            "name": "Quequeña",
            "sku": "15",
            "skuDepPro": "0401",
            "ubigeo": "040115"
          },
          {
            "name": "Sabandia",
            "sku": "16",
            "skuDepPro": "0401",
            "ubigeo": "040116"
          },
          {
            "name": "Sachaca",
            "sku": "17",
            "skuDepPro": "0401",
            "ubigeo": "040117"
          },
          {
            "name": "San Juan de Siguas",
            "sku": "18",
            "skuDepPro": "0401",
            "ubigeo": "040118"
          },
          {
            "name": "San Juan de Tarucani",
            "sku": "19",
            "skuDepPro": "0401",
            "ubigeo": "040119"
          },
          {
            "name": "Santa Isabel de Siguas",
            "sku": "20",
            "skuDepPro": "0401",
            "ubigeo": "040120"
          },
          {
            "name": "Santa Rita de Siguas",
            "sku": "21",
            "skuDepPro": "0401",
            "ubigeo": "040121"
          },
          {
            "name": "Socabaya",
            "sku": "22",
            "skuDepPro": "0401",
            "ubigeo": "040122"
          },
          {
            "name": "Tiabaya",
            "sku": "23",
            "skuDepPro": "0401",
            "ubigeo": "040123"
          },
          {
            "name": "Uchumayo",
            "sku": "24",
            "skuDepPro": "0401",
            "ubigeo": "040124"
          },
          {
            "name": "Vitor",
            "sku": "25",
            "skuDepPro": "0401",
            "ubigeo": "040125"
          },
          {
            "name": "Yanahuara",
            "sku": "26",
            "skuDepPro": "0401",
            "ubigeo": "040126"
          },
          {
            "name": "Yarabamba",
            "sku": "27",
            "skuDepPro": "0401",
            "ubigeo": "040127"
          },
          {
            "name": "Yura",
            "sku": "28",
            "skuDepPro": "0401",
            "ubigeo": "040128"
          },
          {
            "name": "Jose Luis Bustamante Y Rivero",
            "sku": "29",
            "skuDepPro": "0401",
            "ubigeo": "040129"
          }
        ]
      },
      {
        "name": "Camana",
        "sku": "02",
        "skuDep": "04",
        "distritos": [
          {
            "name": "Camana",
            "sku": "01",
            "skuDepPro": "0402",
            "ubigeo": "040201"
          },
          {
            "name": "Jose Maria Quimper",
            "sku": "02",
            "skuDepPro": "0402",
            "ubigeo": "040202"
          },
          {
            "name": "Mariano Nicolas Valcarcel",
            "sku": "03",
            "skuDepPro": "0402",
            "ubigeo": "040203"
          },
          {
            "name": "Mariscal Caceres",
            "sku": "04",
            "skuDepPro": "0402",
            "ubigeo": "040204"
          },
          {
            "name": "Nicolas de Pierola",
            "sku": "05",
            "skuDepPro": "0402",
            "ubigeo": "040205"
          },
          {
            "name": "Ocoña",
            "sku": "06",
            "skuDepPro": "0402",
            "ubigeo": "040206"
          },
          {
            "name": "Quilca",
            "sku": "07",
            "skuDepPro": "0402",
            "ubigeo": "040207"
          },
          {
            "name": "Samuel Pastor",
            "sku": "08",
            "skuDepPro": "0402",
            "ubigeo": "040208"
          }
        ]
      },
      {
        "name": "Caraveli",
        "sku": "03",
        "skuDep": "04",
        "distritos": [
          {
            "name": "Caraveli",
            "sku": "01",
            "skuDepPro": "0403",
            "ubigeo": "040301"
          },
          {
            "name": "Acari",
            "sku": "02",
            "skuDepPro": "0403",
            "ubigeo": "040302"
          },
          {
            "name": "Atico",
            "sku": "03",
            "skuDepPro": "0403",
            "ubigeo": "040303"
          },
          {
            "name": "Atiquipa",
            "sku": "04",
            "skuDepPro": "0403",
            "ubigeo": "040304"
          },
          {
            "name": "Bella Union",
            "sku": "05",
            "skuDepPro": "0403",
            "ubigeo": "040305"
          },
          {
            "name": "Cahuacho",
            "sku": "06",
            "skuDepPro": "0403",
            "ubigeo": "040306"
          },
          {
            "name": "Chala",
            "sku": "07",
            "skuDepPro": "0403",
            "ubigeo": "040307"
          },
          {
            "name": "Chaparra",
            "sku": "08",
            "skuDepPro": "0403",
            "ubigeo": "040308"
          },
          {
            "name": "Huanuhuanu",
            "sku": "09",
            "skuDepPro": "0403",
            "ubigeo": "040309"
          },
          {
            "name": "Jaqui",
            "sku": "10",
            "skuDepPro": "0403",
            "ubigeo": "040310"
          },
          {
            "name": "Lomas",
            "sku": "11",
            "skuDepPro": "0403",
            "ubigeo": "040311"
          },
          {
            "name": "Quicacha",
            "sku": "12",
            "skuDepPro": "0403",
            "ubigeo": "040312"
          },
          {
            "name": "Yauca",
            "sku": "13",
            "skuDepPro": "0403",
            "ubigeo": "040313"
          }
        ]
      },
      {
        "name": "Castilla",
        "sku": "04",
        "skuDep": "04",
        "distritos": [
          {
            "name": "Aplao",
            "sku": "01",
            "skuDepPro": "0404",
            "ubigeo": "040401"
          },
          {
            "name": "Andagua",
            "sku": "02",
            "skuDepPro": "0404",
            "ubigeo": "040402"
          },
          {
            "name": "Ayo",
            "sku": "03",
            "skuDepPro": "0404",
            "ubigeo": "040403"
          },
          {
            "name": "Chachas",
            "sku": "04",
            "skuDepPro": "0404",
            "ubigeo": "040404"
          },
          {
            "name": "Chilcaymarca",
            "sku": "05",
            "skuDepPro": "0404",
            "ubigeo": "040405"
          },
          {
            "name": "Choco",
            "sku": "06",
            "skuDepPro": "0404",
            "ubigeo": "040406"
          },
          {
            "name": "Huancarqui",
            "sku": "07",
            "skuDepPro": "0404",
            "ubigeo": "040407"
          },
          {
            "name": "Machaguay",
            "sku": "08",
            "skuDepPro": "0404",
            "ubigeo": "040408"
          },
          {
            "name": "Orcopampa",
            "sku": "09",
            "skuDepPro": "0404",
            "ubigeo": "040409"
          },
          {
            "name": "Pampacolca",
            "sku": "10",
            "skuDepPro": "0404",
            "ubigeo": "040410"
          },
          {
            "name": "Tipan",
            "sku": "11",
            "skuDepPro": "0404",
            "ubigeo": "040411"
          },
          {
            "name": "Uñon",
            "sku": "12",
            "skuDepPro": "0404",
            "ubigeo": "040412"
          },
          {
            "name": "Uraca",
            "sku": "13",
            "skuDepPro": "0404",
            "ubigeo": "040413"
          },
          {
            "name": "Viraco",
            "sku": "14",
            "skuDepPro": "0404",
            "ubigeo": "040414"
          }
        ]
      },
      {
        "name": "Caylloma",
        "sku": "05",
        "skuDep": "04",
        "distritos": [
          {
            "name": "Chivay",
            "sku": "01",
            "skuDepPro": "0405",
            "ubigeo": "040501"
          },
          {
            "name": "Achoma",
            "sku": "02",
            "skuDepPro": "0405",
            "ubigeo": "040502"
          },
          {
            "name": "Cabanaconde",
            "sku": "03",
            "skuDepPro": "0405",
            "ubigeo": "040503"
          },
          {
            "name": "Callalli",
            "sku": "04",
            "skuDepPro": "0405",
            "ubigeo": "040504"
          },
          {
            "name": "Caylloma",
            "sku": "05",
            "skuDepPro": "0405",
            "ubigeo": "040505"
          },
          {
            "name": "Coporaque",
            "sku": "06",
            "skuDepPro": "0405",
            "ubigeo": "040506"
          },
          {
            "name": "Huambo",
            "sku": "07",
            "skuDepPro": "0405",
            "ubigeo": "040507"
          },
          {
            "name": "Huanca",
            "sku": "08",
            "skuDepPro": "0405",
            "ubigeo": "040508"
          },
          {
            "name": "Ichupampa",
            "sku": "09",
            "skuDepPro": "0405",
            "ubigeo": "040509"
          },
          {
            "name": "Lari",
            "sku": "10",
            "skuDepPro": "0405",
            "ubigeo": "040510"
          },
          {
            "name": "Lluta",
            "sku": "11",
            "skuDepPro": "0405",
            "ubigeo": "040511"
          },
          {
            "name": "Maca",
            "sku": "12",
            "skuDepPro": "0405",
            "ubigeo": "040512"
          },
          {
            "name": "Madrigal",
            "sku": "13",
            "skuDepPro": "0405",
            "ubigeo": "040513"
          },
          {
            "name": "San Antonio de Chuca",
            "sku": "14",
            "skuDepPro": "0405",
            "ubigeo": "040514"
          },
          {
            "name": "Sibayo",
            "sku": "15",
            "skuDepPro": "0405",
            "ubigeo": "040515"
          },
          {
            "name": "Tapay",
            "sku": "16",
            "skuDepPro": "0405",
            "ubigeo": "040516"
          },
          {
            "name": "Tisco",
            "sku": "17",
            "skuDepPro": "0405",
            "ubigeo": "040517"
          },
          {
            "name": "Tuti",
            "sku": "18",
            "skuDepPro": "0405",
            "ubigeo": "040518"
          },
          {
            "name": "Yanque",
            "sku": "19",
            "skuDepPro": "0405",
            "ubigeo": "040519"
          },
          {
            "name": "Majes",
            "sku": "20",
            "skuDepPro": "0405",
            "ubigeo": "040520"
          }
        ]
      },
      {
        "name": "Condesuyos",
        "sku": "06",
        "skuDep": "04",
        "distritos": [
          {
            "name": "Chuquibamba",
            "sku": "01",
            "skuDepPro": "0406",
            "ubigeo": "040601"
          },
          {
            "name": "Andaray",
            "sku": "02",
            "skuDepPro": "0406",
            "ubigeo": "040602"
          },
          {
            "name": "Cayarani",
            "sku": "03",
            "skuDepPro": "0406",
            "ubigeo": "040603"
          },
          {
            "name": "Chichas",
            "sku": "04",
            "skuDepPro": "0406",
            "ubigeo": "040604"
          },
          {
            "name": "Iray",
            "sku": "05",
            "skuDepPro": "0406",
            "ubigeo": "040605"
          },
          {
            "name": "Rio Grande",
            "sku": "06",
            "skuDepPro": "0406",
            "ubigeo": "040606"
          },
          {
            "name": "Salamanca",
            "sku": "07",
            "skuDepPro": "0406",
            "ubigeo": "040607"
          },
          {
            "name": "Yanaquihua",
            "sku": "08",
            "skuDepPro": "0406",
            "ubigeo": "040608"
          }
        ]
      },
      {
        "name": "Islay",
        "sku": "07",
        "skuDep": "04",
        "distritos": [
          {
            "name": "Mollendo",
            "sku": "01",
            "skuDepPro": "0407",
            "ubigeo": "040701"
          },
          {
            "name": "Cocachacra",
            "sku": "02",
            "skuDepPro": "0407",
            "ubigeo": "040702"
          },
          {
            "name": "Dean Valdivia",
            "sku": "03",
            "skuDepPro": "0407",
            "ubigeo": "040703"
          },
          {
            "name": "Islay",
            "sku": "04",
            "skuDepPro": "0407",
            "ubigeo": "040704"
          },
          {
            "name": "Mejia",
            "sku": "05",
            "skuDepPro": "0407",
            "ubigeo": "040705"
          },
          {
            "name": "Punta de Bombon",
            "sku": "06",
            "skuDepPro": "0407",
            "ubigeo": "040706"
          }
        ]
      },
      {
        "name": "La Unión",
        "sku": "08",
        "skuDep": "04",
        "distritos": [
          {
            "name": "Cotahuasi",
            "sku": "01",
            "skuDepPro": "0408",
            "ubigeo": "040801"
          },
          {
            "name": "Alca",
            "sku": "02",
            "skuDepPro": "0408",
            "ubigeo": "040802"
          },
          {
            "name": "Charcana",
            "sku": "03",
            "skuDepPro": "0408",
            "ubigeo": "040803"
          },
          {
            "name": "Huaynacotas",
            "sku": "04",
            "skuDepPro": "0408",
            "ubigeo": "040804"
          },
          {
            "name": "Pampamarca",
            "sku": "05",
            "skuDepPro": "0408",
            "ubigeo": "040805"
          },
          {
            "name": "Puyca",
            "sku": "06",
            "skuDepPro": "0408",
            "ubigeo": "040806"
          },
          {
            "name": "Quechualla",
            "sku": "07",
            "skuDepPro": "0408",
            "ubigeo": "040807"
          },
          {
            "name": "Sayla",
            "sku": "08",
            "skuDepPro": "0408",
            "ubigeo": "040808"
          },
          {
            "name": "Tauria",
            "sku": "09",
            "skuDepPro": "0408",
            "ubigeo": "040809"
          },
          {
            "name": "Tomepampa",
            "sku": "10",
            "skuDepPro": "0408",
            "ubigeo": "040810"
          },
          {
            "name": "Toro",
            "sku": "11",
            "skuDepPro": "0408",
            "ubigeo": "040811"
          }
        ]
      }
    ]
  },
  {
    "name": "Ayacucho",
    "sku": "05",
    "provincias": [
      {
        "name": "Huamanga",
        "sku": "01",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Ayacucho",
            "sku": "01",
            "skuDepPro": "0501",
            "ubigeo": "050101"
          },
          {
            "name": "Acocro",
            "sku": "02",
            "skuDepPro": "0501",
            "ubigeo": "050102"
          },
          {
            "name": "Acos Vinchos",
            "sku": "03",
            "skuDepPro": "0501",
            "ubigeo": "050103"
          },
          {
            "name": "Carmen Alto",
            "sku": "04",
            "skuDepPro": "0501",
            "ubigeo": "050104"
          },
          {
            "name": "Chiara",
            "sku": "05",
            "skuDepPro": "0501",
            "ubigeo": "050105"
          },
          {
            "name": "Ocros",
            "sku": "06",
            "skuDepPro": "0501",
            "ubigeo": "050106"
          },
          {
            "name": "Pacaycasa",
            "sku": "07",
            "skuDepPro": "0501",
            "ubigeo": "050107"
          },
          {
            "name": "Quinua",
            "sku": "08",
            "skuDepPro": "0501",
            "ubigeo": "050108"
          },
          {
            "name": "San Jose de Ticllas",
            "sku": "09",
            "skuDepPro": "0501",
            "ubigeo": "050109"
          },
          {
            "name": "San Juan Bautista",
            "sku": "10",
            "skuDepPro": "0501",
            "ubigeo": "050110"
          },
          {
            "name": "Santiago de Pischa",
            "sku": "11",
            "skuDepPro": "0501",
            "ubigeo": "050111"
          },
          {
            "name": "Socos",
            "sku": "12",
            "skuDepPro": "0501",
            "ubigeo": "050112"
          },
          {
            "name": "Tambillo",
            "sku": "13",
            "skuDepPro": "0501",
            "ubigeo": "050113"
          },
          {
            "name": "Vinchos",
            "sku": "14",
            "skuDepPro": "0501",
            "ubigeo": "050114"
          },
          {
            "name": "Jesus Nazareno",
            "sku": "15",
            "skuDepPro": "0501",
            "ubigeo": "050115"
          },
          {
            "name": "Andres Avelino Caceres Dorregaray",
            "sku": "16",
            "skuDepPro": "0501",
            "ubigeo": "050116"
          }
        ]
      },
      {
        "name": "Cangallo",
        "sku": "02",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Cangallo",
            "sku": "01",
            "skuDepPro": "0502",
            "ubigeo": "050201"
          },
          {
            "name": "Chuschi",
            "sku": "02",
            "skuDepPro": "0502",
            "ubigeo": "050202"
          },
          {
            "name": "Los Morochucos",
            "sku": "03",
            "skuDepPro": "0502",
            "ubigeo": "050203"
          },
          {
            "name": "Maria Parado de Bellido",
            "sku": "04",
            "skuDepPro": "0502",
            "ubigeo": "050204"
          },
          {
            "name": "Paras",
            "sku": "05",
            "skuDepPro": "0502",
            "ubigeo": "050205"
          },
          {
            "name": "Totos",
            "sku": "06",
            "skuDepPro": "0502",
            "ubigeo": "050206"
          }
        ]
      },
      {
        "name": "Huanca Sancos",
        "sku": "03",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Sancos",
            "sku": "01",
            "skuDepPro": "0503",
            "ubigeo": "050301"
          },
          {
            "name": "Carapo",
            "sku": "02",
            "skuDepPro": "0503",
            "ubigeo": "050302"
          },
          {
            "name": "Sacsamarca",
            "sku": "03",
            "skuDepPro": "0503",
            "ubigeo": "050303"
          },
          {
            "name": "Santiago de Lucanamarca",
            "sku": "04",
            "skuDepPro": "0503",
            "ubigeo": "050304"
          }
        ]
      },
      {
        "name": "Huanta",
        "sku": "04",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Huanta",
            "sku": "01",
            "skuDepPro": "0504",
            "ubigeo": "050401"
          },
          {
            "name": "Ayahuanco",
            "sku": "02",
            "skuDepPro": "0504",
            "ubigeo": "050402"
          },
          {
            "name": "Huamanguilla",
            "sku": "03",
            "skuDepPro": "0504",
            "ubigeo": "050403"
          },
          {
            "name": "Iguain",
            "sku": "04",
            "skuDepPro": "0504",
            "ubigeo": "050404"
          },
          {
            "name": "Luricocha",
            "sku": "05",
            "skuDepPro": "0504",
            "ubigeo": "050405"
          },
          {
            "name": "Santillana",
            "sku": "06",
            "skuDepPro": "0504",
            "ubigeo": "050406"
          },
          {
            "name": "Sivia",
            "sku": "07",
            "skuDepPro": "0504",
            "ubigeo": "050407"
          },
          {
            "name": "Llochegua",
            "sku": "08",
            "skuDepPro": "0504",
            "ubigeo": "050408"
          },
          {
            "name": "Canayre",
            "sku": "09",
            "skuDepPro": "0504",
            "ubigeo": "050409"
          },
          {
            "name": "Uchuraccay",
            "sku": "10",
            "skuDepPro": "0504",
            "ubigeo": "050410"
          },
          {
            "name": "Pucacolpa",
            "sku": "11",
            "skuDepPro": "0504",
            "ubigeo": "050411"
          },
          {
            "name": "Chaca",
            "sku": "12",
            "skuDepPro": "0504",
            "ubigeo": "050412"
          }
        ]
      },
      {
        "name": "La Mar",
        "sku": "05",
        "skuDep": "05",
        "distritos": [
          {
            "name": "San Miguel",
            "sku": "01",
            "skuDepPro": "0505",
            "ubigeo": "050501"
          },
          {
            "name": "Anco",
            "sku": "02",
            "skuDepPro": "0505",
            "ubigeo": "050502"
          },
          {
            "name": "Ayna",
            "sku": "03",
            "skuDepPro": "0505",
            "ubigeo": "050503"
          },
          {
            "name": "Chilcas",
            "sku": "04",
            "skuDepPro": "0505",
            "ubigeo": "050504"
          },
          {
            "name": "Chungui",
            "sku": "05",
            "skuDepPro": "0505",
            "ubigeo": "050505"
          },
          {
            "name": "Luis Carranza",
            "sku": "06",
            "skuDepPro": "0505",
            "ubigeo": "050506"
          },
          {
            "name": "Santa Rosa",
            "sku": "07",
            "skuDepPro": "0505",
            "ubigeo": "050507"
          },
          {
            "name": "Tambo",
            "sku": "08",
            "skuDepPro": "0505",
            "ubigeo": "050508"
          },
          {
            "name": "Samugari",
            "sku": "09",
            "skuDepPro": "0505",
            "ubigeo": "050509"
          },
          {
            "name": "Anchihuay",
            "sku": "10",
            "skuDepPro": "0505",
            "ubigeo": "050510"
          },
          {
            "name": "Oronccoy",
            "sku": "11",
            "skuDepPro": "0505",
            "ubigeo": "050511"
          }
        ]
      },
      {
        "name": "Lucanas",
        "sku": "06",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Puquio",
            "sku": "01",
            "skuDepPro": "0506",
            "ubigeo": "050601"
          },
          {
            "name": "Aucara",
            "sku": "02",
            "skuDepPro": "0506",
            "ubigeo": "050602"
          },
          {
            "name": "Cabana",
            "sku": "03",
            "skuDepPro": "0506",
            "ubigeo": "050603"
          },
          {
            "name": "Carmen Salcedo",
            "sku": "04",
            "skuDepPro": "0506",
            "ubigeo": "050604"
          },
          {
            "name": "Chaviña",
            "sku": "05",
            "skuDepPro": "0506",
            "ubigeo": "050605"
          },
          {
            "name": "Chipao",
            "sku": "06",
            "skuDepPro": "0506",
            "ubigeo": "050606"
          },
          {
            "name": "Huac-Huas",
            "sku": "07",
            "skuDepPro": "0506",
            "ubigeo": "050607"
          },
          {
            "name": "Laramate",
            "sku": "08",
            "skuDepPro": "0506",
            "ubigeo": "050608"
          },
          {
            "name": "Leoncio Prado",
            "sku": "09",
            "skuDepPro": "0506",
            "ubigeo": "050609"
          },
          {
            "name": "Llauta",
            "sku": "10",
            "skuDepPro": "0506",
            "ubigeo": "050610"
          },
          {
            "name": "Lucanas",
            "sku": "11",
            "skuDepPro": "0506",
            "ubigeo": "050611"
          },
          {
            "name": "Ocaña",
            "sku": "12",
            "skuDepPro": "0506",
            "ubigeo": "050612"
          },
          {
            "name": "Otoca",
            "sku": "13",
            "skuDepPro": "0506",
            "ubigeo": "050613"
          },
          {
            "name": "Saisa",
            "sku": "14",
            "skuDepPro": "0506",
            "ubigeo": "050614"
          },
          {
            "name": "San Cristobal",
            "sku": "15",
            "skuDepPro": "0506",
            "ubigeo": "050615"
          },
          {
            "name": "San Juan",
            "sku": "16",
            "skuDepPro": "0506",
            "ubigeo": "050616"
          },
          {
            "name": "San Pedro",
            "sku": "17",
            "skuDepPro": "0506",
            "ubigeo": "050617"
          },
          {
            "name": "San Pedro de Palco",
            "sku": "18",
            "skuDepPro": "0506",
            "ubigeo": "050618"
          },
          {
            "name": "Sancos",
            "sku": "19",
            "skuDepPro": "0506",
            "ubigeo": "050619"
          },
          {
            "name": "Santa Ana de Huaycahuacho",
            "sku": "20",
            "skuDepPro": "0506",
            "ubigeo": "050620"
          },
          {
            "name": "Santa Lucia",
            "sku": "21",
            "skuDepPro": "0506",
            "ubigeo": "050621"
          }
        ]
      },
      {
        "name": "Parinacochas",
        "sku": "07",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Coracora",
            "sku": "01",
            "skuDepPro": "0507",
            "ubigeo": "050701"
          },
          {
            "name": "Chumpi",
            "sku": "02",
            "skuDepPro": "0507",
            "ubigeo": "050702"
          },
          {
            "name": "Coronel Castañeda",
            "sku": "03",
            "skuDepPro": "0507",
            "ubigeo": "050703"
          },
          {
            "name": "Pacapausa",
            "sku": "04",
            "skuDepPro": "0507",
            "ubigeo": "050704"
          },
          {
            "name": "Pullo",
            "sku": "05",
            "skuDepPro": "0507",
            "ubigeo": "050705"
          },
          {
            "name": "Puyusca",
            "sku": "06",
            "skuDepPro": "0507",
            "ubigeo": "050706"
          },
          {
            "name": "San Francisco de Ravacayco",
            "sku": "07",
            "skuDepPro": "0507",
            "ubigeo": "050707"
          },
          {
            "name": "Upahuacho",
            "sku": "08",
            "skuDepPro": "0507",
            "ubigeo": "050708"
          }
        ]
      },
      {
        "name": "Páucar del Sara Sara",
        "sku": "08",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Pausa",
            "sku": "01",
            "skuDepPro": "0508",
            "ubigeo": "050801"
          },
          {
            "name": "Colta",
            "sku": "02",
            "skuDepPro": "0508",
            "ubigeo": "050802"
          },
          {
            "name": "Corculla",
            "sku": "03",
            "skuDepPro": "0508",
            "ubigeo": "050803"
          },
          {
            "name": "Lampa",
            "sku": "04",
            "skuDepPro": "0508",
            "ubigeo": "050804"
          },
          {
            "name": "Marcabamba",
            "sku": "05",
            "skuDepPro": "0508",
            "ubigeo": "050805"
          },
          {
            "name": "Oyolo",
            "sku": "06",
            "skuDepPro": "0508",
            "ubigeo": "050806"
          },
          {
            "name": "Pararca",
            "sku": "07",
            "skuDepPro": "0508",
            "ubigeo": "050807"
          },
          {
            "name": "San Javier de Alpabamba",
            "sku": "08",
            "skuDepPro": "0508",
            "ubigeo": "050808"
          },
          {
            "name": "San Jose de Ushua",
            "sku": "09",
            "skuDepPro": "0508",
            "ubigeo": "050809"
          },
          {
            "name": "Sara Sara",
            "sku": "10",
            "skuDepPro": "0508",
            "ubigeo": "050810"
          }
        ]
      },
      {
        "name": "Sucre",
        "sku": "09",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Querobamba",
            "sku": "01",
            "skuDepPro": "0509",
            "ubigeo": "050901"
          },
          {
            "name": "Belen",
            "sku": "02",
            "skuDepPro": "0509",
            "ubigeo": "050902"
          },
          {
            "name": "Chalcos",
            "sku": "03",
            "skuDepPro": "0509",
            "ubigeo": "050903"
          },
          {
            "name": "Chilcayoc",
            "sku": "04",
            "skuDepPro": "0509",
            "ubigeo": "050904"
          },
          {
            "name": "Huacaña",
            "sku": "05",
            "skuDepPro": "0509",
            "ubigeo": "050905"
          },
          {
            "name": "Morcolla",
            "sku": "06",
            "skuDepPro": "0509",
            "ubigeo": "050906"
          },
          {
            "name": "Paico",
            "sku": "07",
            "skuDepPro": "0509",
            "ubigeo": "050907"
          },
          {
            "name": "San Pedro de Larcay",
            "sku": "08",
            "skuDepPro": "0509",
            "ubigeo": "050908"
          },
          {
            "name": "San Salvador de Quije",
            "sku": "09",
            "skuDepPro": "0509",
            "ubigeo": "050909"
          },
          {
            "name": "Santiago de Paucaray",
            "sku": "10",
            "skuDepPro": "0509",
            "ubigeo": "050910"
          },
          {
            "name": "Soras",
            "sku": "11",
            "skuDepPro": "0509",
            "ubigeo": "050911"
          }
        ]
      },
      {
        "name": "Victor Fajardo",
        "sku": "10",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Huancapi",
            "sku": "01",
            "skuDepPro": "0510",
            "ubigeo": "051001"
          },
          {
            "name": "Alcamenca",
            "sku": "02",
            "skuDepPro": "0510",
            "ubigeo": "051002"
          },
          {
            "name": "Apongo",
            "sku": "03",
            "skuDepPro": "0510",
            "ubigeo": "051003"
          },
          {
            "name": "Asquipata",
            "sku": "04",
            "skuDepPro": "0510",
            "ubigeo": "051004"
          },
          {
            "name": "Canaria",
            "sku": "05",
            "skuDepPro": "0510",
            "ubigeo": "051005"
          },
          {
            "name": "Cayara",
            "sku": "06",
            "skuDepPro": "0510",
            "ubigeo": "051006"
          },
          {
            "name": "Colca",
            "sku": "07",
            "skuDepPro": "0510",
            "ubigeo": "051007"
          },
          {
            "name": "Huamanquiquia",
            "sku": "08",
            "skuDepPro": "0510",
            "ubigeo": "051008"
          },
          {
            "name": "Huancaraylla",
            "sku": "09",
            "skuDepPro": "0510",
            "ubigeo": "051009"
          },
          {
            "name": "Huaya",
            "sku": "10",
            "skuDepPro": "0510",
            "ubigeo": "051010"
          },
          {
            "name": "Sarhua",
            "sku": "11",
            "skuDepPro": "0510",
            "ubigeo": "051011"
          },
          {
            "name": "Vilcanchos",
            "sku": "12",
            "skuDepPro": "0510",
            "ubigeo": "051012"
          }
        ]
      },
      {
        "name": "Vilcas Huaman",
        "sku": "11",
        "skuDep": "05",
        "distritos": [
          {
            "name": "Vilcas Huaman",
            "sku": "01",
            "skuDepPro": "0511",
            "ubigeo": "051101"
          },
          {
            "name": "Accomarca",
            "sku": "02",
            "skuDepPro": "0511",
            "ubigeo": "051102"
          },
          {
            "name": "Carhuanca",
            "sku": "03",
            "skuDepPro": "0511",
            "ubigeo": "051103"
          },
          {
            "name": "Concepción",
            "sku": "04",
            "skuDepPro": "0511",
            "ubigeo": "051104"
          },
          {
            "name": "Huambalpa",
            "sku": "05",
            "skuDepPro": "0511",
            "ubigeo": "051105"
          },
          {
            "name": "Independencia",
            "sku": "06",
            "skuDepPro": "0511",
            "ubigeo": "051106"
          },
          {
            "name": "Saurama",
            "sku": "07",
            "skuDepPro": "0511",
            "ubigeo": "051107"
          },
          {
            "name": "Vischongo",
            "sku": "08",
            "skuDepPro": "0511",
            "ubigeo": "051108"
          }
        ]
      }
    ]
  },
  {
    "name": "Cajamarca",
    "sku": "06",
    "provincias": [
      {
        "name": "Cajamarca",
        "sku": "01",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Cajamarca",
            "sku": "01",
            "skuDepPro": "0601",
            "ubigeo": "060101"
          },
          {
            "name": "Asuncion",
            "sku": "02",
            "skuDepPro": "0601",
            "ubigeo": "060102"
          },
          {
            "name": "Chetilla",
            "sku": "03",
            "skuDepPro": "0601",
            "ubigeo": "060103"
          },
          {
            "name": "Cospan",
            "sku": "04",
            "skuDepPro": "0601",
            "ubigeo": "060104"
          },
          {
            "name": "Encañada",
            "sku": "05",
            "skuDepPro": "0601",
            "ubigeo": "060105"
          },
          {
            "name": "Jesus",
            "sku": "06",
            "skuDepPro": "0601",
            "ubigeo": "060106"
          },
          {
            "name": "Llacanora",
            "sku": "07",
            "skuDepPro": "0601",
            "ubigeo": "060107"
          },
          {
            "name": "Los Baños del Inca",
            "sku": "08",
            "skuDepPro": "0601",
            "ubigeo": "060108"
          },
          {
            "name": "Magdalena",
            "sku": "09",
            "skuDepPro": "0601",
            "ubigeo": "060109"
          },
          {
            "name": "Matara",
            "sku": "10",
            "skuDepPro": "0601",
            "ubigeo": "060110"
          },
          {
            "name": "Namora",
            "sku": "11",
            "skuDepPro": "0601",
            "ubigeo": "060111"
          },
          {
            "name": "San Juan",
            "sku": "12",
            "skuDepPro": "0601",
            "ubigeo": "060112"
          }
        ]
      },
      {
        "name": "Cajabamba",
        "sku": "02",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Cajabamba",
            "sku": "01",
            "skuDepPro": "0602",
            "ubigeo": "060201"
          },
          {
            "name": "Cachachi",
            "sku": "02",
            "skuDepPro": "0602",
            "ubigeo": "060202"
          },
          {
            "name": "Condebamba",
            "sku": "03",
            "skuDepPro": "0602",
            "ubigeo": "060203"
          },
          {
            "name": "Sitacocha",
            "sku": "04",
            "skuDepPro": "0602",
            "ubigeo": "060204"
          }
        ]
      },
      {
        "name": "Celendin",
        "sku": "03",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Celendin",
            "sku": "01",
            "skuDepPro": "0603",
            "ubigeo": "060301"
          },
          {
            "name": "Chumuch",
            "sku": "02",
            "skuDepPro": "0603",
            "ubigeo": "060302"
          },
          {
            "name": "Cortegana",
            "sku": "03",
            "skuDepPro": "0603",
            "ubigeo": "060303"
          },
          {
            "name": "Huasmin",
            "sku": "04",
            "skuDepPro": "0603",
            "ubigeo": "060304"
          },
          {
            "name": "Jorge Chavez",
            "sku": "05",
            "skuDepPro": "0603",
            "ubigeo": "060305"
          },
          {
            "name": "Jose Galvez",
            "sku": "06",
            "skuDepPro": "0603",
            "ubigeo": "060306"
          },
          {
            "name": "Miguel Iglesias",
            "sku": "07",
            "skuDepPro": "0603",
            "ubigeo": "060307"
          },
          {
            "name": "Oxamarca",
            "sku": "08",
            "skuDepPro": "0603",
            "ubigeo": "060308"
          },
          {
            "name": "Sorochuco",
            "sku": "09",
            "skuDepPro": "0603",
            "ubigeo": "060309"
          },
          {
            "name": "Sucre",
            "sku": "10",
            "skuDepPro": "0603",
            "ubigeo": "060310"
          },
          {
            "name": "Utco",
            "sku": "11",
            "skuDepPro": "0603",
            "ubigeo": "060311"
          },
          {
            "name": "La Libertad de Pallan",
            "sku": "12",
            "skuDepPro": "0603",
            "ubigeo": "060312"
          }
        ]
      },
      {
        "name": "Chota",
        "sku": "04",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Chota",
            "sku": "01",
            "skuDepPro": "0604",
            "ubigeo": "060401"
          },
          {
            "name": "Anguia",
            "sku": "02",
            "skuDepPro": "0604",
            "ubigeo": "060402"
          },
          {
            "name": "Chadin",
            "sku": "03",
            "skuDepPro": "0604",
            "ubigeo": "060403"
          },
          {
            "name": "Chiguirip",
            "sku": "04",
            "skuDepPro": "0604",
            "ubigeo": "060404"
          },
          {
            "name": "Chimban",
            "sku": "05",
            "skuDepPro": "0604",
            "ubigeo": "060405"
          },
          {
            "name": "Choropampa",
            "sku": "06",
            "skuDepPro": "0604",
            "ubigeo": "060406"
          },
          {
            "name": "Cochabamba",
            "sku": "07",
            "skuDepPro": "0604",
            "ubigeo": "060407"
          },
          {
            "name": "Conchan",
            "sku": "08",
            "skuDepPro": "0604",
            "ubigeo": "060408"
          },
          {
            "name": "Huambos",
            "sku": "09",
            "skuDepPro": "0604",
            "ubigeo": "060409"
          },
          {
            "name": "Lajas",
            "sku": "10",
            "skuDepPro": "0604",
            "ubigeo": "060410"
          },
          {
            "name": "Llama",
            "sku": "11",
            "skuDepPro": "0604",
            "ubigeo": "060411"
          },
          {
            "name": "Miracosta",
            "sku": "12",
            "skuDepPro": "0604",
            "ubigeo": "060412"
          },
          {
            "name": "Paccha",
            "sku": "13",
            "skuDepPro": "0604",
            "ubigeo": "060413"
          },
          {
            "name": "Pion",
            "sku": "14",
            "skuDepPro": "0604",
            "ubigeo": "060414"
          },
          {
            "name": "Querocoto",
            "sku": "15",
            "skuDepPro": "0604",
            "ubigeo": "060415"
          },
          {
            "name": "San Juan de Licupis",
            "sku": "16",
            "skuDepPro": "0604",
            "ubigeo": "060416"
          },
          {
            "name": "Tacabamba",
            "sku": "17",
            "skuDepPro": "0604",
            "ubigeo": "060417"
          },
          {
            "name": "Tocmoche",
            "sku": "18",
            "skuDepPro": "0604",
            "ubigeo": "060418"
          },
          {
            "name": "Chalamarca",
            "sku": "19",
            "skuDepPro": "0604",
            "ubigeo": "060419"
          }
        ]
      },
      {
        "name": "Contumaza",
        "sku": "05",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Contumaza",
            "sku": "01",
            "skuDepPro": "0605",
            "ubigeo": "060501"
          },
          {
            "name": "Chilete",
            "sku": "02",
            "skuDepPro": "0605",
            "ubigeo": "060502"
          },
          {
            "name": "Cupisnique",
            "sku": "03",
            "skuDepPro": "0605",
            "ubigeo": "060503"
          },
          {
            "name": "Guzmango",
            "sku": "04",
            "skuDepPro": "0605",
            "ubigeo": "060504"
          },
          {
            "name": "San Benito",
            "sku": "05",
            "skuDepPro": "0605",
            "ubigeo": "060505"
          },
          {
            "name": "Santa Cruz de Toledo",
            "sku": "06",
            "skuDepPro": "0605",
            "ubigeo": "060506"
          },
          {
            "name": "Tantarica",
            "sku": "07",
            "skuDepPro": "0605",
            "ubigeo": "060507"
          },
          {
            "name": "Yonan",
            "sku": "08",
            "skuDepPro": "0605",
            "ubigeo": "060508"
          }
        ]
      },
      {
        "name": "Cutervo",
        "sku": "06",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Cutervo",
            "sku": "01",
            "skuDepPro": "0606",
            "ubigeo": "060601"
          },
          {
            "name": "Callayuc",
            "sku": "02",
            "skuDepPro": "0606",
            "ubigeo": "060602"
          },
          {
            "name": "Choros",
            "sku": "03",
            "skuDepPro": "0606",
            "ubigeo": "060603"
          },
          {
            "name": "Cujillo",
            "sku": "04",
            "skuDepPro": "0606",
            "ubigeo": "060604"
          },
          {
            "name": "La Ramada",
            "sku": "05",
            "skuDepPro": "0606",
            "ubigeo": "060605"
          },
          {
            "name": "Pimpingos",
            "sku": "06",
            "skuDepPro": "0606",
            "ubigeo": "060606"
          },
          {
            "name": "Querocotillo",
            "sku": "07",
            "skuDepPro": "0606",
            "ubigeo": "060607"
          },
          {
            "name": "San Andres de Cutervo",
            "sku": "08",
            "skuDepPro": "0606",
            "ubigeo": "060608"
          },
          {
            "name": "San Juan de Cutervo",
            "sku": "09",
            "skuDepPro": "0606",
            "ubigeo": "060609"
          },
          {
            "name": "San Luis de Lucma",
            "sku": "10",
            "skuDepPro": "0606",
            "ubigeo": "060610"
          },
          {
            "name": "Santa Cruz",
            "sku": "11",
            "skuDepPro": "0606",
            "ubigeo": "060611"
          },
          {
            "name": "Santo Domingo de la Capilla",
            "sku": "12",
            "skuDepPro": "0606",
            "ubigeo": "060612"
          },
          {
            "name": "Santo Tomas",
            "sku": "13",
            "skuDepPro": "0606",
            "ubigeo": "060613"
          },
          {
            "name": "Socota",
            "sku": "14",
            "skuDepPro": "0606",
            "ubigeo": "060614"
          },
          {
            "name": "Toribio Casanova",
            "sku": "15",
            "skuDepPro": "0606",
            "ubigeo": "060615"
          }
        ]
      },
      {
        "name": "Hualgayoc",
        "sku": "07",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Bambamarca",
            "sku": "01",
            "skuDepPro": "0607",
            "ubigeo": "060701"
          },
          {
            "name": "Chugur",
            "sku": "02",
            "skuDepPro": "0607",
            "ubigeo": "060702"
          },
          {
            "name": "Hualgayoc",
            "sku": "03",
            "skuDepPro": "0607",
            "ubigeo": "060703"
          }
        ]
      },
      {
        "name": "Jaen",
        "sku": "08",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Jaen",
            "sku": "01",
            "skuDepPro": "0608",
            "ubigeo": "060801"
          },
          {
            "name": "Bellavista",
            "sku": "02",
            "skuDepPro": "0608",
            "ubigeo": "060802"
          },
          {
            "name": "Chontali",
            "sku": "03",
            "skuDepPro": "0608",
            "ubigeo": "060803"
          },
          {
            "name": "Colasay",
            "sku": "04",
            "skuDepPro": "0608",
            "ubigeo": "060804"
          },
          {
            "name": "Huabal",
            "sku": "05",
            "skuDepPro": "0608",
            "ubigeo": "060805"
          },
          {
            "name": "Las Pirias",
            "sku": "06",
            "skuDepPro": "0608",
            "ubigeo": "060806"
          },
          {
            "name": "Pomahuaca",
            "sku": "07",
            "skuDepPro": "0608",
            "ubigeo": "060807"
          },
          {
            "name": "Pucara",
            "sku": "08",
            "skuDepPro": "0608",
            "ubigeo": "060808"
          },
          {
            "name": "Sallique",
            "sku": "09",
            "skuDepPro": "0608",
            "ubigeo": "060809"
          },
          {
            "name": "San Felipe",
            "sku": "10",
            "skuDepPro": "0608",
            "ubigeo": "060810"
          },
          {
            "name": "San Jose del Alto",
            "sku": "11",
            "skuDepPro": "0608",
            "ubigeo": "060811"
          },
          {
            "name": "Santa Rosa",
            "sku": "12",
            "skuDepPro": "0608",
            "ubigeo": "060812"
          }
        ]
      },
      {
        "name": "San Ignacio",
        "sku": "09",
        "skuDep": "06",
        "distritos": [
          {
            "name": "San Ignacio",
            "sku": "01",
            "skuDepPro": "0609",
            "ubigeo": "060901"
          },
          {
            "name": "Chirinos",
            "sku": "02",
            "skuDepPro": "0609",
            "ubigeo": "060902"
          },
          {
            "name": "Huarango",
            "sku": "03",
            "skuDepPro": "0609",
            "ubigeo": "060903"
          },
          {
            "name": "La Coipa",
            "sku": "04",
            "skuDepPro": "0609",
            "ubigeo": "060904"
          },
          {
            "name": "Namballe",
            "sku": "05",
            "skuDepPro": "0609",
            "ubigeo": "060905"
          },
          {
            "name": "San Jose de Lourdes",
            "sku": "06",
            "skuDepPro": "0609",
            "ubigeo": "060906"
          },
          {
            "name": "Tabaconas",
            "sku": "07",
            "skuDepPro": "0609",
            "ubigeo": "060907"
          }
        ]
      },
      {
        "name": "San Marcos",
        "sku": "10",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Pedro Galvez",
            "sku": "01",
            "skuDepPro": "0610",
            "ubigeo": "061001"
          },
          {
            "name": "Chancay",
            "sku": "02",
            "skuDepPro": "0610",
            "ubigeo": "061002"
          },
          {
            "name": "Eduardo Villanueva",
            "sku": "03",
            "skuDepPro": "0610",
            "ubigeo": "061003"
          },
          {
            "name": "Gregorio Pita",
            "sku": "04",
            "skuDepPro": "0610",
            "ubigeo": "061004"
          },
          {
            "name": "Ichocan",
            "sku": "05",
            "skuDepPro": "0610",
            "ubigeo": "061005"
          },
          {
            "name": "Jose Manuel Quiroz",
            "sku": "06",
            "skuDepPro": "0610",
            "ubigeo": "061006"
          },
          {
            "name": "Jose Sabogal",
            "sku": "07",
            "skuDepPro": "0610",
            "ubigeo": "061007"
          }
        ]
      },
      {
        "name": "San Miguel",
        "sku": "11",
        "skuDep": "06",
        "distritos": [
          {
            "name": "San Miguel",
            "sku": "01",
            "skuDepPro": "0611",
            "ubigeo": "061101"
          },
          {
            "name": "Bolívar",
            "sku": "02",
            "skuDepPro": "0611",
            "ubigeo": "061102"
          },
          {
            "name": "Calquis",
            "sku": "03",
            "skuDepPro": "0611",
            "ubigeo": "061103"
          },
          {
            "name": "Catilluc",
            "sku": "04",
            "skuDepPro": "0611",
            "ubigeo": "061104"
          },
          {
            "name": "El Prado",
            "sku": "05",
            "skuDepPro": "0611",
            "ubigeo": "061105"
          },
          {
            "name": "La Florida",
            "sku": "06",
            "skuDepPro": "0611",
            "ubigeo": "061106"
          },
          {
            "name": "Llapa",
            "sku": "07",
            "skuDepPro": "0611",
            "ubigeo": "061107"
          },
          {
            "name": "Nanchoc",
            "sku": "08",
            "skuDepPro": "0611",
            "ubigeo": "061108"
          },
          {
            "name": "Niepos",
            "sku": "09",
            "skuDepPro": "0611",
            "ubigeo": "061109"
          },
          {
            "name": "San Gregorio",
            "sku": "10",
            "skuDepPro": "0611",
            "ubigeo": "061110"
          },
          {
            "name": "San Silvestre de Cochan",
            "sku": "11",
            "skuDepPro": "0611",
            "ubigeo": "061111"
          },
          {
            "name": "Tongod",
            "sku": "12",
            "skuDepPro": "0611",
            "ubigeo": "061112"
          },
          {
            "name": "Union Agua Blanca",
            "sku": "13",
            "skuDepPro": "0611",
            "ubigeo": "061113"
          }
        ]
      },
      {
        "name": "San Pablo",
        "sku": "12",
        "skuDep": "06",
        "distritos": [
          {
            "name": "San Pablo",
            "sku": "01",
            "skuDepPro": "0612",
            "ubigeo": "061201"
          },
          {
            "name": "San Bernardino",
            "sku": "02",
            "skuDepPro": "0612",
            "ubigeo": "061202"
          },
          {
            "name": "San Luis",
            "sku": "03",
            "skuDepPro": "0612",
            "ubigeo": "061203"
          },
          {
            "name": "Tumbaden",
            "sku": "04",
            "skuDepPro": "0612",
            "ubigeo": "061204"
          }
        ]
      },
      {
        "name": "Santa Cruz",
        "sku": "13",
        "skuDep": "06",
        "distritos": [
          {
            "name": "Santa Cruz",
            "sku": "01",
            "skuDepPro": "0613",
            "ubigeo": "061301"
          },
          {
            "name": "Andabamba",
            "sku": "02",
            "skuDepPro": "0613",
            "ubigeo": "061302"
          },
          {
            "name": "Catache",
            "sku": "03",
            "skuDepPro": "0613",
            "ubigeo": "061303"
          },
          {
            "name": "Chancaybaños",
            "sku": "04",
            "skuDepPro": "0613",
            "ubigeo": "061304"
          },
          {
            "name": "La Esperanza",
            "sku": "05",
            "skuDepPro": "0613",
            "ubigeo": "061305"
          },
          {
            "name": "Ninabamba",
            "sku": "06",
            "skuDepPro": "0613",
            "ubigeo": "061306"
          },
          {
            "name": "Pulan",
            "sku": "07",
            "skuDepPro": "0613",
            "ubigeo": "061307"
          },
          {
            "name": "Saucepampa",
            "sku": "08",
            "skuDepPro": "0613",
            "ubigeo": "061308"
          },
          {
            "name": "Sexi",
            "sku": "09",
            "skuDepPro": "0613",
            "ubigeo": "061309"
          },
          {
            "name": "Uticyacu",
            "sku": "10",
            "skuDepPro": "0613",
            "ubigeo": "061310"
          },
          {
            "name": "Yauyucan",
            "sku": "11",
            "skuDepPro": "0613",
            "ubigeo": "061311"
          }
        ]
      }
    ]
  },
  {
    "name": "Callao",
    "sku": "07",
    "provincias": [
      {
        "name": "Callao",
        "sku": "01",
        "skuDep": "07",
        "distritos": [
          {
            "name": "Callao",
            "sku": "01",
            "skuDepPro": "0701",
            "ubigeo": "070101"
          },
          {
            "name": "Bellavista",
            "sku": "02",
            "skuDepPro": "0701",
            "ubigeo": "070102"
          },
          {
            "name": "Carmen de la Legua Reynoso",
            "sku": "03",
            "skuDepPro": "0701",
            "ubigeo": "070103"
          },
          {
            "name": "La Perla",
            "sku": "04",
            "skuDepPro": "0701",
            "ubigeo": "070104"
          },
          {
            "name": "La Punta",
            "sku": "05",
            "skuDepPro": "0701",
            "ubigeo": "070105"
          },
          {
            "name": "Ventanilla",
            "sku": "06",
            "skuDepPro": "0701",
            "ubigeo": "070106"
          },
          {
            "name": "Mi Peru",
            "sku": "07",
            "skuDepPro": "0701",
            "ubigeo": "070107"
          }
        ]
      }
    ]
  },
  {
    "name": "Cusco",
    "sku": "08",
    "provincias": [
      {
        "name": "Cusco",
        "sku": "01",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Cusco",
            "sku": "01",
            "skuDepPro": "0801",
            "ubigeo": "080101"
          },
          {
            "name": "Ccorca",
            "sku": "02",
            "skuDepPro": "0801",
            "ubigeo": "080102"
          },
          {
            "name": "Poroy",
            "sku": "03",
            "skuDepPro": "0801",
            "ubigeo": "080103"
          },
          {
            "name": "San Jeronimo",
            "sku": "04",
            "skuDepPro": "0801",
            "ubigeo": "080104"
          },
          {
            "name": "San Sebastian",
            "sku": "05",
            "skuDepPro": "0801",
            "ubigeo": "080105"
          },
          {
            "name": "Santiago",
            "sku": "06",
            "skuDepPro": "0801",
            "ubigeo": "080106"
          },
          {
            "name": "Saylla",
            "sku": "07",
            "skuDepPro": "0801",
            "ubigeo": "080107"
          },
          {
            "name": "Wanchaq",
            "sku": "08",
            "skuDepPro": "0801",
            "ubigeo": "080108"
          }
        ]
      },
      {
        "name": "Acomayo",
        "sku": "02",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Acomayo",
            "sku": "01",
            "skuDepPro": "0802",
            "ubigeo": "080201"
          },
          {
            "name": "Acopia",
            "sku": "02",
            "skuDepPro": "0802",
            "ubigeo": "080202"
          },
          {
            "name": "Acos",
            "sku": "03",
            "skuDepPro": "0802",
            "ubigeo": "080203"
          },
          {
            "name": "Mosoc Llacta",
            "sku": "04",
            "skuDepPro": "0802",
            "ubigeo": "080204"
          },
          {
            "name": "Pomacanchi",
            "sku": "05",
            "skuDepPro": "0802",
            "ubigeo": "080205"
          },
          {
            "name": "Rondocan",
            "sku": "06",
            "skuDepPro": "0802",
            "ubigeo": "080206"
          },
          {
            "name": "Sangarara",
            "sku": "07",
            "skuDepPro": "0802",
            "ubigeo": "080207"
          }
        ]
      },
      {
        "name": "Anta",
        "sku": "03",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Anta",
            "sku": "01",
            "skuDepPro": "0803",
            "ubigeo": "080301"
          },
          {
            "name": "Ancahuasi",
            "sku": "02",
            "skuDepPro": "0803",
            "ubigeo": "080302"
          },
          {
            "name": "Cachimayo",
            "sku": "03",
            "skuDepPro": "0803",
            "ubigeo": "080303"
          },
          {
            "name": "Chinchaypujio",
            "sku": "04",
            "skuDepPro": "0803",
            "ubigeo": "080304"
          },
          {
            "name": "Huarocondo",
            "sku": "05",
            "skuDepPro": "0803",
            "ubigeo": "080305"
          },
          {
            "name": "Limatambo",
            "sku": "06",
            "skuDepPro": "0803",
            "ubigeo": "080306"
          },
          {
            "name": "Mollepata",
            "sku": "07",
            "skuDepPro": "0803",
            "ubigeo": "080307"
          },
          {
            "name": "Pucyura",
            "sku": "08",
            "skuDepPro": "0803",
            "ubigeo": "080308"
          },
          {
            "name": "Zurite",
            "sku": "09",
            "skuDepPro": "0803",
            "ubigeo": "080309"
          }
        ]
      },
      {
        "name": "Calca",
        "sku": "04",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Calca",
            "sku": "01",
            "skuDepPro": "0804",
            "ubigeo": "080401"
          },
          {
            "name": "Coya",
            "sku": "02",
            "skuDepPro": "0804",
            "ubigeo": "080402"
          },
          {
            "name": "Lamay",
            "sku": "03",
            "skuDepPro": "0804",
            "ubigeo": "080403"
          },
          {
            "name": "Lares",
            "sku": "04",
            "skuDepPro": "0804",
            "ubigeo": "080404"
          },
          {
            "name": "Pisac",
            "sku": "05",
            "skuDepPro": "0804",
            "ubigeo": "080405"
          },
          {
            "name": "San Salvador",
            "sku": "06",
            "skuDepPro": "0804",
            "ubigeo": "080406"
          },
          {
            "name": "Taray",
            "sku": "07",
            "skuDepPro": "0804",
            "ubigeo": "080407"
          },
          {
            "name": "Yanatile",
            "sku": "08",
            "skuDepPro": "0804",
            "ubigeo": "080408"
          }
        ]
      },
      {
        "name": "Canas",
        "sku": "05",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Yanaoca",
            "sku": "01",
            "skuDepPro": "0805",
            "ubigeo": "080501"
          },
          {
            "name": "Checca",
            "sku": "02",
            "skuDepPro": "0805",
            "ubigeo": "080502"
          },
          {
            "name": "Kunturkanki",
            "sku": "03",
            "skuDepPro": "0805",
            "ubigeo": "080503"
          },
          {
            "name": "Langui",
            "sku": "04",
            "skuDepPro": "0805",
            "ubigeo": "080504"
          },
          {
            "name": "Layo",
            "sku": "05",
            "skuDepPro": "0805",
            "ubigeo": "080505"
          },
          {
            "name": "Pampamarca",
            "sku": "06",
            "skuDepPro": "0805",
            "ubigeo": "080506"
          },
          {
            "name": "Quehue",
            "sku": "07",
            "skuDepPro": "0805",
            "ubigeo": "080507"
          },
          {
            "name": "Tupac Amaru",
            "sku": "08",
            "skuDepPro": "0805",
            "ubigeo": "080508"
          }
        ]
      },
      {
        "name": "Canchis",
        "sku": "06",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Sicuani",
            "sku": "01",
            "skuDepPro": "0806",
            "ubigeo": "080601"
          },
          {
            "name": "Checacupe",
            "sku": "02",
            "skuDepPro": "0806",
            "ubigeo": "080602"
          },
          {
            "name": "Combapata",
            "sku": "03",
            "skuDepPro": "0806",
            "ubigeo": "080603"
          },
          {
            "name": "Marangani",
            "sku": "04",
            "skuDepPro": "0806",
            "ubigeo": "080604"
          },
          {
            "name": "Pitumarca",
            "sku": "05",
            "skuDepPro": "0806",
            "ubigeo": "080605"
          },
          {
            "name": "San Pablo",
            "sku": "06",
            "skuDepPro": "0806",
            "ubigeo": "080606"
          },
          {
            "name": "San Pedro",
            "sku": "07",
            "skuDepPro": "0806",
            "ubigeo": "080607"
          },
          {
            "name": "Tinta",
            "sku": "08",
            "skuDepPro": "0806",
            "ubigeo": "080608"
          }
        ]
      },
      {
        "name": "Chumbivilcas",
        "sku": "07",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Santo Tomas",
            "sku": "01",
            "skuDepPro": "0807",
            "ubigeo": "080701"
          },
          {
            "name": "Capacmarca",
            "sku": "02",
            "skuDepPro": "0807",
            "ubigeo": "080702"
          },
          {
            "name": "Chamaca",
            "sku": "03",
            "skuDepPro": "0807",
            "ubigeo": "080703"
          },
          {
            "name": "Colquemarca",
            "sku": "04",
            "skuDepPro": "0807",
            "ubigeo": "080704"
          },
          {
            "name": "Livitaca",
            "sku": "05",
            "skuDepPro": "0807",
            "ubigeo": "080705"
          },
          {
            "name": "Llusco",
            "sku": "06",
            "skuDepPro": "0807",
            "ubigeo": "080706"
          },
          {
            "name": "Quiñota",
            "sku": "07",
            "skuDepPro": "0807",
            "ubigeo": "080707"
          },
          {
            "name": "Velille",
            "sku": "08",
            "skuDepPro": "0807",
            "ubigeo": "080708"
          }
        ]
      },
      {
        "name": "Espinar",
        "sku": "08",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Espinar",
            "sku": "01",
            "skuDepPro": "0808",
            "ubigeo": "080801"
          },
          {
            "name": "Condoroma",
            "sku": "02",
            "skuDepPro": "0808",
            "ubigeo": "080802"
          },
          {
            "name": "Coporaque",
            "sku": "03",
            "skuDepPro": "0808",
            "ubigeo": "080803"
          },
          {
            "name": "Ocoruro",
            "sku": "04",
            "skuDepPro": "0808",
            "ubigeo": "080804"
          },
          {
            "name": "Pallpata",
            "sku": "05",
            "skuDepPro": "0808",
            "ubigeo": "080805"
          },
          {
            "name": "Pichigua",
            "sku": "06",
            "skuDepPro": "0808",
            "ubigeo": "080806"
          },
          {
            "name": "Suyckutambo",
            "sku": "07",
            "skuDepPro": "0808",
            "ubigeo": "080807"
          },
          {
            "name": "Alto Pichigua",
            "sku": "08",
            "skuDepPro": "0808",
            "ubigeo": "080808"
          }
        ]
      },
      {
        "name": "La Convencion",
        "sku": "09",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Santa Ana",
            "sku": "01",
            "skuDepPro": "0809",
            "ubigeo": "080901"
          },
          {
            "name": "Echarate",
            "sku": "02",
            "skuDepPro": "0809",
            "ubigeo": "080902"
          },
          {
            "name": "Huayopata",
            "sku": "03",
            "skuDepPro": "0809",
            "ubigeo": "080903"
          },
          {
            "name": "Maranura",
            "sku": "04",
            "skuDepPro": "0809",
            "ubigeo": "080904"
          },
          {
            "name": "Ocobamba",
            "sku": "05",
            "skuDepPro": "0809",
            "ubigeo": "080905"
          },
          {
            "name": "Quellouno",
            "sku": "06",
            "skuDepPro": "0809",
            "ubigeo": "080906"
          },
          {
            "name": "Kimbiri",
            "sku": "07",
            "skuDepPro": "0809",
            "ubigeo": "080907"
          },
          {
            "name": "Santa Teresa",
            "sku": "08",
            "skuDepPro": "0809",
            "ubigeo": "080908"
          },
          {
            "name": "Vilcabamba",
            "sku": "09",
            "skuDepPro": "0809",
            "ubigeo": "080909"
          },
          {
            "name": "Pichari",
            "sku": "10",
            "skuDepPro": "0809",
            "ubigeo": "080910"
          },
          {
            "name": "Inkawasi",
            "sku": "11",
            "skuDepPro": "0809",
            "ubigeo": "080911"
          },
          {
            "name": "Villa Virgen",
            "sku": "12",
            "skuDepPro": "0809",
            "ubigeo": "080912"
          },
          {
            "name": "Villa Kintiarina",
            "sku": "13",
            "skuDepPro": "0809",
            "ubigeo": "080913"
          },
          {
            "name": "Megantoni",
            "sku": "14",
            "skuDepPro": "0809",
            "ubigeo": "080914"
          }
        ]
      },
      {
        "name": "Paruro",
        "sku": "10",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Paruro",
            "sku": "01",
            "skuDepPro": "0810",
            "ubigeo": "081001"
          },
          {
            "name": "Accha",
            "sku": "02",
            "skuDepPro": "0810",
            "ubigeo": "081002"
          },
          {
            "name": "Ccapi",
            "sku": "03",
            "skuDepPro": "0810",
            "ubigeo": "081003"
          },
          {
            "name": "Colcha",
            "sku": "04",
            "skuDepPro": "0810",
            "ubigeo": "081004"
          },
          {
            "name": "Huanoquite",
            "sku": "05",
            "skuDepPro": "0810",
            "ubigeo": "081005"
          },
          {
            "name": "Omacha",
            "sku": "06",
            "skuDepPro": "0810",
            "ubigeo": "081006"
          },
          {
            "name": "Paccaritambo",
            "sku": "07",
            "skuDepPro": "0810",
            "ubigeo": "081007"
          },
          {
            "name": "Pillpinto",
            "sku": "08",
            "skuDepPro": "0810",
            "ubigeo": "081008"
          },
          {
            "name": "Yaurisque",
            "sku": "09",
            "skuDepPro": "0810",
            "ubigeo": "081009"
          }
        ]
      },
      {
        "name": "Paucartambo",
        "sku": "11",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Paucartambo",
            "sku": "01",
            "skuDepPro": "0811",
            "ubigeo": "081101"
          },
          {
            "name": "Caicay",
            "sku": "02",
            "skuDepPro": "0811",
            "ubigeo": "081102"
          },
          {
            "name": "Challabamba",
            "sku": "03",
            "skuDepPro": "0811",
            "ubigeo": "081103"
          },
          {
            "name": "Colquepata",
            "sku": "04",
            "skuDepPro": "0811",
            "ubigeo": "081104"
          },
          {
            "name": "Huancarani",
            "sku": "05",
            "skuDepPro": "0811",
            "ubigeo": "081105"
          },
          {
            "name": "Kosñipata",
            "sku": "06",
            "skuDepPro": "0811",
            "ubigeo": "081106"
          }
        ]
      },
      {
        "name": "Quispicanchi",
        "sku": "12",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Urcos",
            "sku": "01",
            "skuDepPro": "0812",
            "ubigeo": "081201"
          },
          {
            "name": "Andahuaylillas",
            "sku": "02",
            "skuDepPro": "0812",
            "ubigeo": "081202"
          },
          {
            "name": "Camanti",
            "sku": "03",
            "skuDepPro": "0812",
            "ubigeo": "081203"
          },
          {
            "name": "Ccarhuayo",
            "sku": "04",
            "skuDepPro": "0812",
            "ubigeo": "081204"
          },
          {
            "name": "Ccatca",
            "sku": "05",
            "skuDepPro": "0812",
            "ubigeo": "081205"
          },
          {
            "name": "Cusipata",
            "sku": "06",
            "skuDepPro": "0812",
            "ubigeo": "081206"
          },
          {
            "name": "Huaro",
            "sku": "07",
            "skuDepPro": "0812",
            "ubigeo": "081207"
          },
          {
            "name": "Lucre",
            "sku": "08",
            "skuDepPro": "0812",
            "ubigeo": "081208"
          },
          {
            "name": "Marcapata",
            "sku": "09",
            "skuDepPro": "0812",
            "ubigeo": "081209"
          },
          {
            "name": "Ocongate",
            "sku": "10",
            "skuDepPro": "0812",
            "ubigeo": "081210"
          },
          {
            "name": "Oropesa",
            "sku": "11",
            "skuDepPro": "0812",
            "ubigeo": "081211"
          },
          {
            "name": "Quiquijana",
            "sku": "12",
            "skuDepPro": "0812",
            "ubigeo": "081212"
          }
        ]
      },
      {
        "name": "Urubamba",
        "sku": "13",
        "skuDep": "08",
        "distritos": [
          {
            "name": "Urubamba",
            "sku": "01",
            "skuDepPro": "0813",
            "ubigeo": "081301"
          },
          {
            "name": "Chinchero",
            "sku": "02",
            "skuDepPro": "0813",
            "ubigeo": "081302"
          },
          {
            "name": "Huayllabamba",
            "sku": "03",
            "skuDepPro": "0813",
            "ubigeo": "081303"
          },
          {
            "name": "Machupicchu",
            "sku": "04",
            "skuDepPro": "0813",
            "ubigeo": "081304"
          },
          {
            "name": "Maras",
            "sku": "05",
            "skuDepPro": "0813",
            "ubigeo": "081305"
          },
          {
            "name": "Ollantaytambo",
            "sku": "06",
            "skuDepPro": "0813",
            "ubigeo": "081306"
          },
          {
            "name": "Yucay",
            "sku": "07",
            "skuDepPro": "0813",
            "ubigeo": "081307"
          }
        ]
      }
    ]
  },
  {
    "name": "Huancavelica",
    "sku": "09",
    "provincias": [
      {
        "name": "Huancavelica",
        "sku": "01",
        "skuDep": "09",
        "distritos": [
          {
            "name": "Huancavelica",
            "sku": "01",
            "skuDepPro": "0901",
            "ubigeo": "090101"
          },
          {
            "name": "Acobambilla",
            "sku": "02",
            "skuDepPro": "0901",
            "ubigeo": "090102"
          },
          {
            "name": "Acoria",
            "sku": "03",
            "skuDepPro": "0901",
            "ubigeo": "090103"
          },
          {
            "name": "Conayca",
            "sku": "04",
            "skuDepPro": "0901",
            "ubigeo": "090104"
          },
          {
            "name": "Cuenca",
            "sku": "05",
            "skuDepPro": "0901",
            "ubigeo": "090105"
          },
          {
            "name": "Huachocolpa",
            "sku": "06",
            "skuDepPro": "0901",
            "ubigeo": "090106"
          },
          {
            "name": "Huayllahuara",
            "sku": "07",
            "skuDepPro": "0901",
            "ubigeo": "090107"
          },
          {
            "name": "Izcuchaca",
            "sku": "08",
            "skuDepPro": "0901",
            "ubigeo": "090108"
          },
          {
            "name": "Laria",
            "sku": "09",
            "skuDepPro": "0901",
            "ubigeo": "090109"
          },
          {
            "name": "Manta",
            "sku": "10",
            "skuDepPro": "0901",
            "ubigeo": "090110"
          },
          {
            "name": "Mariscal Caceres",
            "sku": "11",
            "skuDepPro": "0901",
            "ubigeo": "090111"
          },
          {
            "name": "Moya",
            "sku": "12",
            "skuDepPro": "0901",
            "ubigeo": "090112"
          },
          {
            "name": "Nuevo Occoro",
            "sku": "13",
            "skuDepPro": "0901",
            "ubigeo": "090113"
          },
          {
            "name": "Palca",
            "sku": "14",
            "skuDepPro": "0901",
            "ubigeo": "090114"
          },
          {
            "name": "Pilchaca",
            "sku": "15",
            "skuDepPro": "0901",
            "ubigeo": "090115"
          },
          {
            "name": "Vilca",
            "sku": "16",
            "skuDepPro": "0901",
            "ubigeo": "090116"
          },
          {
            "name": "Yauli",
            "sku": "17",
            "skuDepPro": "0901",
            "ubigeo": "090117"
          },
          {
            "name": "Ascension",
            "sku": "18",
            "skuDepPro": "0901",
            "ubigeo": "090118"
          },
          {
            "name": "Huando",
            "sku": "19",
            "skuDepPro": "0901",
            "ubigeo": "090119"
          }
        ]
      },
      {
        "name": "Acobamba",
        "sku": "02",
        "skuDep": "09",
        "distritos": [
          {
            "name": "Acobamba",
            "sku": "01",
            "skuDepPro": "0902",
            "ubigeo": "090201"
          },
          {
            "name": "Andabamba",
            "sku": "02",
            "skuDepPro": "0902",
            "ubigeo": "090202"
          },
          {
            "name": "Anta",
            "sku": "03",
            "skuDepPro": "0902",
            "ubigeo": "090203"
          },
          {
            "name": "Caja",
            "sku": "04",
            "skuDepPro": "0902",
            "ubigeo": "090204"
          },
          {
            "name": "Marcas",
            "sku": "05",
            "skuDepPro": "0902",
            "ubigeo": "090205"
          },
          {
            "name": "Paucara",
            "sku": "06",
            "skuDepPro": "0902",
            "ubigeo": "090206"
          },
          {
            "name": "Pomacocha",
            "sku": "07",
            "skuDepPro": "0902",
            "ubigeo": "090207"
          },
          {
            "name": "Rosario",
            "sku": "08",
            "skuDepPro": "0902",
            "ubigeo": "090208"
          }
        ]
      },
      {
        "name": "Angaraes",
        "sku": "03",
        "skuDep": "09",
        "distritos": [
          {
            "name": "Lircay",
            "sku": "01",
            "skuDepPro": "0903",
            "ubigeo": "090301"
          },
          {
            "name": "Anchonga",
            "sku": "02",
            "skuDepPro": "0903",
            "ubigeo": "090302"
          },
          {
            "name": "Callanmarca",
            "sku": "03",
            "skuDepPro": "0903",
            "ubigeo": "090303"
          },
          {
            "name": "Ccochaccasa",
            "sku": "04",
            "skuDepPro": "0903",
            "ubigeo": "090304"
          },
          {
            "name": "Chincho",
            "sku": "05",
            "skuDepPro": "0903",
            "ubigeo": "090305"
          },
          {
            "name": "Congalla",
            "sku": "06",
            "skuDepPro": "0903",
            "ubigeo": "090306"
          },
          {
            "name": "Huanca-Huanca",
            "sku": "07",
            "skuDepPro": "0903",
            "ubigeo": "090307"
          },
          {
            "name": "Huayllay Grande",
            "sku": "08",
            "skuDepPro": "0903",
            "ubigeo": "090308"
          },
          {
            "name": "Julcamarca",
            "sku": "09",
            "skuDepPro": "0903",
            "ubigeo": "090309"
          },
          {
            "name": "San Antonio de Antaparco",
            "sku": "10",
            "skuDepPro": "0903",
            "ubigeo": "090310"
          },
          {
            "name": "Santo Tomas de Pata",
            "sku": "11",
            "skuDepPro": "0903",
            "ubigeo": "090311"
          },
          {
            "name": "Secclla",
            "sku": "12",
            "skuDepPro": "0903",
            "ubigeo": "090312"
          }
        ]
      },
      {
        "name": "Castrovirreyna",
        "sku": "04",
        "skuDep": "09",
        "distritos": [
          {
            "name": "Castrovirreyna",
            "sku": "01",
            "skuDepPro": "0904",
            "ubigeo": "090401"
          },
          {
            "name": "Arma",
            "sku": "02",
            "skuDepPro": "0904",
            "ubigeo": "090402"
          },
          {
            "name": "Aurahua",
            "sku": "03",
            "skuDepPro": "0904",
            "ubigeo": "090403"
          },
          {
            "name": "Capillas",
            "sku": "04",
            "skuDepPro": "0904",
            "ubigeo": "090404"
          },
          {
            "name": "Chupamarca",
            "sku": "05",
            "skuDepPro": "0904",
            "ubigeo": "090405"
          },
          {
            "name": "Cocas",
            "sku": "06",
            "skuDepPro": "0904",
            "ubigeo": "090406"
          },
          {
            "name": "Huachos",
            "sku": "07",
            "skuDepPro": "0904",
            "ubigeo": "090407"
          },
          {
            "name": "Huamatambo",
            "sku": "08",
            "skuDepPro": "0904",
            "ubigeo": "090408"
          },
          {
            "name": "Mollepampa",
            "sku": "09",
            "skuDepPro": "0904",
            "ubigeo": "090409"
          },
          {
            "name": "San Juan",
            "sku": "10",
            "skuDepPro": "0904",
            "ubigeo": "090410"
          },
          {
            "name": "Santa Ana",
            "sku": "11",
            "skuDepPro": "0904",
            "ubigeo": "090411"
          },
          {
            "name": "Tantara",
            "sku": "12",
            "skuDepPro": "0904",
            "ubigeo": "090412"
          },
          {
            "name": "Ticrapo",
            "sku": "13",
            "skuDepPro": "0904",
            "ubigeo": "090413"
          }
        ]
      },
      {
        "name": "Churcampa",
        "sku": "05",
        "skuDep": "09",
        "distritos": [
          {
            "name": "Churcampa",
            "sku": "01",
            "skuDepPro": "0905",
            "ubigeo": "090501"
          },
          {
            "name": "Anco",
            "sku": "02",
            "skuDepPro": "0905",
            "ubigeo": "090502"
          },
          {
            "name": "Chinchihuasi",
            "sku": "03",
            "skuDepPro": "0905",
            "ubigeo": "090503"
          },
          {
            "name": "El Carmen",
            "sku": "04",
            "skuDepPro": "0905",
            "ubigeo": "090504"
          },
          {
            "name": "La Merced",
            "sku": "05",
            "skuDepPro": "0905",
            "ubigeo": "090505"
          },
          {
            "name": "Locroja",
            "sku": "06",
            "skuDepPro": "0905",
            "ubigeo": "090506"
          },
          {
            "name": "Paucarbamba",
            "sku": "07",
            "skuDepPro": "0905",
            "ubigeo": "090507"
          },
          {
            "name": "San Miguel de Mayocc",
            "sku": "08",
            "skuDepPro": "0905",
            "ubigeo": "090508"
          },
          {
            "name": "San Pedro de Coris",
            "sku": "09",
            "skuDepPro": "0905",
            "ubigeo": "090509"
          },
          {
            "name": "Pachamarca",
            "sku": "10",
            "skuDepPro": "0905",
            "ubigeo": "090510"
          },
          {
            "name": "Cosme",
            "sku": "11",
            "skuDepPro": "0905",
            "ubigeo": "090511"
          }
        ]
      },
      {
        "name": "Huaytara",
        "sku": "06",
        "skuDep": "09",
        "distritos": [
          {
            "name": "Huaytara",
            "sku": "01",
            "skuDepPro": "0906",
            "ubigeo": "090601"
          },
          {
            "name": "Ayavi",
            "sku": "02",
            "skuDepPro": "0906",
            "ubigeo": "090602"
          },
          {
            "name": "Cordova",
            "sku": "03",
            "skuDepPro": "0906",
            "ubigeo": "090603"
          },
          {
            "name": "Huayacundo Arma",
            "sku": "04",
            "skuDepPro": "0906",
            "ubigeo": "090604"
          },
          {
            "name": "Laramarca",
            "sku": "05",
            "skuDepPro": "0906",
            "ubigeo": "090605"
          },
          {
            "name": "Ocoyo",
            "sku": "06",
            "skuDepPro": "0906",
            "ubigeo": "090606"
          },
          {
            "name": "Pilpichaca",
            "sku": "07",
            "skuDepPro": "0906",
            "ubigeo": "090607"
          },
          {
            "name": "Querco",
            "sku": "08",
            "skuDepPro": "0906",
            "ubigeo": "090608"
          },
          {
            "name": "Quito-Arma",
            "sku": "09",
            "skuDepPro": "0906",
            "ubigeo": "090609"
          },
          {
            "name": "San Antonio de Cusicancha",
            "sku": "10",
            "skuDepPro": "0906",
            "ubigeo": "090610"
          },
          {
            "name": "San Francisco de Sangayaico",
            "sku": "11",
            "skuDepPro": "0906",
            "ubigeo": "090611"
          },
          {
            "name": "San Isidro",
            "sku": "12",
            "skuDepPro": "0906",
            "ubigeo": "090612"
          },
          {
            "name": "Santiago de Chocorvos",
            "sku": "13",
            "skuDepPro": "0906",
            "ubigeo": "090613"
          },
          {
            "name": "Santiago de Quirahuara",
            "sku": "14",
            "skuDepPro": "0906",
            "ubigeo": "090614"
          },
          {
            "name": "Santo Domingo de Capillas",
            "sku": "15",
            "skuDepPro": "0906",
            "ubigeo": "090615"
          },
          {
            "name": "Tambo",
            "sku": "16",
            "skuDepPro": "0906",
            "ubigeo": "090616"
          }
        ]
      },
      {
        "name": "Tayacaja",
        "sku": "07",
        "skuDep": "09",
        "distritos": [
          {
            "name": "Pampas",
            "sku": "01",
            "skuDepPro": "0907",
            "ubigeo": "090701"
          },
          {
            "name": "Acostambo",
            "sku": "02",
            "skuDepPro": "0907",
            "ubigeo": "090702"
          },
          {
            "name": "Acraquia",
            "sku": "03",
            "skuDepPro": "0907",
            "ubigeo": "090703"
          },
          {
            "name": "Ahuaycha",
            "sku": "04",
            "skuDepPro": "0907",
            "ubigeo": "090704"
          },
          {
            "name": "Colcabamba",
            "sku": "05",
            "skuDepPro": "0907",
            "ubigeo": "090705"
          },
          {
            "name": "Daniel Hernandez",
            "sku": "06",
            "skuDepPro": "0907",
            "ubigeo": "090706"
          },
          {
            "name": "Huachocolpa",
            "sku": "07",
            "skuDepPro": "0907",
            "ubigeo": "090707"
          },
          {
            "name": "Huaribamba",
            "sku": "09",
            "skuDepPro": "0907",
            "ubigeo": "090709"
          },
          {
            "name": "ñahuimpuquio",
            "sku": "10",
            "skuDepPro": "0907",
            "ubigeo": "090710"
          },
          {
            "name": "Pazos",
            "sku": "11",
            "skuDepPro": "0907",
            "ubigeo": "090711"
          },
          {
            "name": "Quishuar",
            "sku": "13",
            "skuDepPro": "0907",
            "ubigeo": "090713"
          },
          {
            "name": "Salcabamba",
            "sku": "14",
            "skuDepPro": "0907",
            "ubigeo": "090714"
          },
          {
            "name": "Salcahuasi",
            "sku": "15",
            "skuDepPro": "0907",
            "ubigeo": "090715"
          },
          {
            "name": "San Marcos de Rocchac",
            "sku": "16",
            "skuDepPro": "0907",
            "ubigeo": "090716"
          },
          {
            "name": "Surcubamba",
            "sku": "17",
            "skuDepPro": "0907",
            "ubigeo": "090717"
          },
          {
            "name": "Tintay Puncu",
            "sku": "18",
            "skuDepPro": "0907",
            "ubigeo": "090718"
          },
          {
            "name": "Quichuas",
            "sku": "19",
            "skuDepPro": "0907",
            "ubigeo": "090719"
          },
          {
            "name": "Andaymarca",
            "sku": "20",
            "skuDepPro": "0907",
            "ubigeo": "090720"
          },
          {
            "name": "Roble",
            "sku": "21",
            "skuDepPro": "0907",
            "ubigeo": "090721"
          },
          {
            "name": "Pichos",
            "sku": "22",
            "skuDepPro": "0907",
            "ubigeo": "090722"
          },
          {
            "name": "Santiago de Tucuma",
            "sku": "23",
            "skuDepPro": "0907",
            "ubigeo": "090723"
          }
        ]
      }
    ]
  }
];
