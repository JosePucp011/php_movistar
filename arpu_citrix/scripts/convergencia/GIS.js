/* global Util */

var GIS = {};
GIS.URL = 'Http/ConsultarGIS.php';

GIS.Consultar = function(x,y,documento){
    Util.ConsultaAjax(GIS.URL,GIS.CrearData(x,y,documento),GIS.MostrarResultado);
};

GIS.ConsultarSinMapa = function(){
    Util.ConsultaAjax(GIS.URL,GIS.CrearData($('#direccion_x').val(),$('#direccion_y').val(),$('#documento').val()),GIS.MostrarResultado);
};



GIS.CrearData = function(x,y,documento){
    return {x: x, y: y,documento:documento};
};

GIS.MostrarResultado = function(ResultadoConsulta){
    if(ResultadoConsulta.WSSIGFFTT_FFTT_AVEResult.GPON_TEC === "SI"){
        $('#cobertura').val('FTTH');
    } else if(ResultadoConsulta.WSSIGFFTT_FFTT_AVEResult.HFC_TEC === "SI"){
        $('#cobertura').val('HFC');
    } else if(ResultadoConsulta.WSSIGFFTT_FFTT_AVEResult.XDSL_TEC === "SI"){
        $('#cobertura').val('ADSL');
    } else {
        alert('El Cliente no esta en Cobertura de Internet');
        $('#cobertura').val('Sin cobertura de Internet');
    }
};
