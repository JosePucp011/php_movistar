/* global Comparacion*/
/* global Parrilla*/
/* global Convergente, IncrementoPrecio, CiudadSitiada, Util*/
var DatosCliente = {};

DatosCliente.MostrarClienteRegular = function(){
    DatosCliente.MostrarDatosCliente(window.ResultadoConsulta);
    Parrilla.Mostrar(window.ResultadoConsulta);
    Comparacion.Mostrar(window.ResultadoConsulta);
};

DatosCliente.MostrarMensajeIquitos = function(){
    if (window.ResultadoConsulta.Ofertas.length === 0 && window.ResultadoConsulta.Retenciones !== "Ninguno" 
            && window.ResultadoConsulta.Cliente.Internet.Cobertura.Iquitos === 1)
    {
        alert("El cliente se encuentra en Iquitos no retener");
    }
};


DatosCliente.MostrarResultado = function (ResultadoConsulta)
{
   
    window.ResultadoConsulta = ResultadoConsulta;
    
    if(ResultadoConsulta.actualizacion !== undefined){
        alert('Actualizando a la version : '+ResultadoConsulta.actualizacion+ '. Volver a consultar luego de la actualizacion');
        location.reload();
    }else if(ResultadoConsulta.error !== undefined){
        $('#error_busqueda').html(window.ResultadoConsulta.error);
    }
    else {
        DatosCliente.MostrarClienteRegular(ResultadoConsulta);
    }
};

DatosCliente.MostrarMensajes = function(mensajes)
{
   var Resultado = "<table id='mensajes'><tr>";
   for(var i in mensajes)
   {
       var mensaje = mensajes[i];
       var id = mensaje.Mensaje.replace(/\s/g,'');
       Resultado += "<td class='mensaje' id='"+id+"' onclick='javascript:DatosCliente.MostrarMensaje("+i+")'>"+mensaje.Mensaje+"</td>";
   }
   Resultado +="<td class='mensaje' onclick='javascript:Freeview.Consultar()'>Beneficios Movistar</td>";
   Resultado +="<td class='mensaje' onclick='javascript:Freeview.Consultar()'>Reclamos</td>";
   Resultado +="</tr></table>";
   $('#MigracionMasiva').html(Resultado);
};

DatosCliente.MostrarMensaje = function(posicion){
    
    var mensaje = window.ResultadoConsulta.Cliente.Mensajes[posicion];
    
    if(mensaje.Mensaje === 'Incremento de Precio'){
        IncrementoPrecio.MostrarIncrementoPrecio(mensaje);
    } else if(mensaje.Mensaje === 'Prioridad Migracion HFC'){
        CiudadSitiada.Consultar(mensaje);
    }
};

DatosCliente.MostrarDatosCliente = function(ResultadoConsulta) 
{
   this.Cliente = ResultadoConsulta.Cliente;
   DatosCliente.MostrarDatosBasicos();
   DatosCliente.MostrarMensajesError();
   DatosCliente.MostrarMensajes(ResultadoConsulta.Cliente.Mensajes);
  
};

DatosCliente.MostrarMensajesError = function () {
    DatosCliente.MostrarRadioEnlace();
    DatosCliente.MostrarComportamientoPago();
    DatosCliente.MostrarIpFija();
    DatosCliente.MostrarMensajeIquitos();
};

DatosCliente.MostrarDatosBasicos = function(){
    
    $('#Cliente_TipoSpeech').text(this.Cliente.Telefono % 2 > 0 ? 'Renta Total' : 'Salto');
    
    $('#Cliente_Telefono').text(this.Cliente.Telefono);
    $('#Cliente_Movil').text(this.Cliente.Movil);
    $('#Cliente_Nombre').text(this.Cliente.Nombre);
    $('#Cliente_ClienteCms').text(this.Cliente.ClienteCms);
    $('#Cliente_Ciclo').text(this.Cliente.Ciclo);
    $('#Cliente_Segmento').text(this.Cliente.Segmento);
    $('#Cliente_ReciboDigital').text(this.Cliente.ReciboDigital);
    $('#Cliente_ProteccionDatos').text(this.Cliente.ProteccionDatos);
    $('#Cliente_Desposicionado').text(this.Cliente.DesposicionadoTexto);
    $('#Cliente_Antiguedad').html(this.Cliente.Antiguedad);
    $('#Cliente_TipoBlindaje').text(this.Cliente.TipoBlindaje);
    $('#Cliente_Averias').html(this.Cliente.Averias);
    $('#Cliente_Reclamos').html(this.Cliente.Reclamos);
    $('#Cliente_Llamadas').html(this.Cliente.Llamadas);
    $('#Cliente_TiempoMigracionTecn').html(this.Cliente.TiempoMigracionTecn);
    $('#Cliente_TiempoAveria').html(this.Cliente.TiempoAveria);
    $('#Cliente_ZonaCompetencia').html(this.Cliente.ZonaCompetencia === "1" ? 'SI' : 'NO');
    if('9999-12-31' !== this.Cliente.Digitalizado)
    {
        $('#Cliente_Digitalizado').html(this.Cliente.Digitalizado);
    }
    DatosCliente.MostrarVelocidadMaxima(this.Cliente.Internet);
    DatosCliente.MostrarDescuentoEmpleado(this.Cliente);
    
};

DatosCliente.MostrarComportamientoPago = function(){
   if( this.Cliente.ComportamientoPagoCovergente === 1)
   {
       Util.MostrarModalError("NO VENDER ALTA DE COMPONENTE, MIGRACIÓN UP O SVA",
       "El cliente no cumple con los requisitos de la evaluación crediticia");
   }
};

DatosCliente.MostrarRadioEnlace = function(){
    if(this.Cliente.Internet.Cobertura.RadioEnlace === 1){
        Util.MostrarModalError("NO MIGRAR","El cliente se encuentra en zona de Radio-Enlace bajo supervision de Osiptel");
    }
};

DatosCliente.MostrarIpFija = function(){
       Util.MostrarModalError("PIERDE IP FIJA","El cliente perdera su IP Fija en caso de realizar una migracion");
};



DatosCliente.MostrarVelocidadMaxima = function (Internet)
{
    var texto = "";

    if (Internet.Cobertura.FTTH)
    {
        texto += 'FTTH: 200 Mbps';
    } else if (Internet.Cobertura.HFC)
    {
        texto += 'HFC: 200 Mbps';
    }
    
    if(Internet.Tecnologia === 'ADSL')
    {
        texto += '<br> ADSL:'+ Math.round(Internet.Cobertura.VelocidadMaxima / 1000, 1)+ ' Mbps';
    }
    

    $('#Cliente_VelocidadMaxima').html(texto);
};

DatosCliente.MostrarDescuentoEmpleado = function(cliente)
{
   if(cliente.EsEmpleado !== 0 ){
      $("#cliente_DescuentoEmpleado").text("Cliente con Paquete Embajador");
   }
   
   if(cliente.EstadoTecnico === 'Critico' ){
      $("#cliente_Tecnica").text("Migracion Prioritaria");
   }
};

DatosCliente.limpiar_cliente = function() 
{
   DatosCliente.limpiar_cliente_datos();
   $("#NuevoContenido").html("");
};

DatosCliente.limpiar_cliente_datos = function()
{
    $('#parrilla').html('');
    $('.cliente_dato').text('');
    $('#oferta').html('');
    $('#MigracionMasiva').html('');
};
