/*global CiudadSitiada, DescuentosRetenciones, HerramientasRetenciones, Util, IncrementoPrecio, Freeview, Parrilla, DatosCliente*/

var Comparacion = {};

Comparacion.Mostrar = function ()
{
    Comparacion.CrearEstructuraHTML();
    Comparacion.LlenarInformacionCliente();
    Comparacion.LlenarOpciones();
};

Comparacion.Limpiar = function() {
    $("#NuevoContenido").html("");
};

Comparacion.LlenarInformacionCliente = function () {

    var Cliente = window.ResultadoConsulta.Cliente;
    $("#Cliente_TelefonoFijo").html(Cliente.Telefono);
    $("#Cliente_Paquete").html(Comparacion.PaqueteNombre(Cliente.Paquete));
    $("#Cliente_Descuento").html(Comparacion.DescuentosCliente());
    
    $("#Cliente_Linea").html(Comparacion.ComponenteCliente(Cliente.Componentes['Linea']));
    $("#Cliente_Plan").html(Comparacion.ComponenteCliente(Cliente.Componentes['Plan']));
    $("#Cliente_Cable").html(Comparacion.ComponenteCliente(Cliente.Componentes['Cable']));
    $("#Cliente_Internet").html(Comparacion.ComponenteCliente(Cliente.Componentes["Internet"]));
    $("#Cliente_Premium").html(Comparacion.ComponenteCliente(Cliente.Componentes["Cable"].Premium));
    
    $("#Cliente_Decos").html(Comparacion.Decos(Cliente.Componentes["Cable"].Decos));
    $("#Cliente_Modems").html(Comparacion.Modems(Cliente));

    Comparacion.ImprimirMoviles();
    


    $("#Cliente_Subtotal").html("S/" + Cliente.RentaTotal);
    $("#Cliente_Adicionales").html("S/" + Cliente.RentasAdicionales);
    $("#Cliente_Total").html("S/" + Cliente.RentaTotalConAdicionales);

};


Comparacion.ImprimirMoviles = function(){
    var Cliente = window.ResultadoConsulta.Cliente;
    for (var i = 0; i < 10; ++i) {
        var componenteMovil = 'Movil' + i;
        if (Cliente.Componentes[componenteMovil] === undefined) {
            break;
        }
        var componente = Cliente.Componentes[componenteMovil];
        $("#Cliente_Movil" + i).html(Comparacion.ComponenteCliente(Cliente.Componentes[componenteMovil]));
        $("#Cliente_EquipoMovil" + i).html(componente.Equipo);
        $("#Cliente_Gigas" + i).html( Math.round(componente.Nivel / 1024) + " GB" );
        $('#Cliente_NumeroCelular'+i).html(componente.Movil + " | " + componente.Ciclo);
    }

};
Comparacion.LlenarOpciones = function ()
{
    var opciones = window.ResultadoConsulta.Opciones;
    for (var i in opciones)
    {
        Comparacion.LlenarInformacionOferta(opciones[i], i);
    }
};


Comparacion.LlenarInformacionOferta = function (Opcion, Posicion)
{
    var Cliente = window.ResultadoConsulta.Cliente;


    $("#Oferta" + Posicion + '_Operacion').html(Opcion.tipoOperacionComercial);
    $("#Oferta" + Posicion + '_Paquete').html(Comparacion.PaqueteNombre(Opcion.producto.Paquete));
    $("#Oferta" + Posicion + '_Linea').html(Comparacion.Nombre('Linea', Opcion));
    $("#Oferta" + Posicion + '_Plan').html(Comparacion.Nombre('Plan', Opcion));
    $("#Oferta" + Posicion + '_Cable').html(Comparacion.Nombre('Cable', Opcion));
    $("#Oferta" + Posicion + '_Internet').html(Comparacion.Nombre('Internet', Opcion));

    var Premium = Comparacion.PremiumNombre(Opcion.producto.Componentes["Cable"].Premium,
            Opcion.movimiento["Premium"],
            Opcion.ComponentesMono["Premium"]);


    for (var i = 0; i < 10; ++i) {
        var componenteMovil = 'Movil' + i;
        if (Opcion.producto.Componentes[componenteMovil] === undefined) {
            break;
        }
        $("#Oferta" + Posicion + '_'+componenteMovil).html(Comparacion.Nombre(componenteMovil, Opcion));
        $("#Oferta" + Posicion + '_Gigas'+i).html(Math.round(Opcion.producto.Componentes[componenteMovil].Nivel / 1000) + "GB");
        
        if(Opcion.CAM[componenteMovil] !== undefined){
            $("#Oferta" + Posicion + '_EquipoMovil'+i).html("<div id='Equipo"+Posicion+""+i+"'></div>"
+"<div class='boton' onclick='Comparacion.VerCAM("+Posicion+","+i+");'> Ver CAM aqui</div>");
        }
    }


    $("#Oferta" + Posicion + '_Descuento').html(Comparacion.ComercialDescuentos(Opcion.Descuentos));
    $("#Oferta" + Posicion + '_Premium').html(Premium);
    $("#Oferta" + Posicion + '_Decos').html(Comparacion.Decos(Opcion.Decos));
    $("#Oferta" + Posicion + '_Modems').html(Opcion.Modem);

    $("#Oferta" + Posicion + '_Salto').html("S/" + Opcion.salto_arpu);
    $("#Oferta" + Posicion + '_Subtotal').html("S/" + Opcion.cargo_fijo_destino);
    $("#Oferta" + Posicion + '_Adicionales').html("S/" + Cliente.RentasAdicionales);
    $("#Oferta" + Posicion + '_Total').html("S/" + Opcion.cargo_fijo_destino_conAdicionales);
};


Comparacion.VerCAM = function(Posicion,i){
    var componenteMovil = 'Movil' + i;
    var CAM = window.ResultadoConsulta.Opciones[Posicion].CAM[componenteMovil];
    var TextoEquipo = Posicion+""+i;
    
    
    var mensaje = "<table>";
    
    for(var i in CAM){
        var equipo = '"'+i+'"';
        mensaje +=
                "<tr>"+
                "<td>"+i+"</td>"+
                "<td>"+CAM[i]+"</td>"+
                "<td><div class='boton' onclick='Comparacion.MostrarEquipo("+TextoEquipo+","+equipo+")' >Agregar</td>"+
                "</tr>";
        
    }
    
    mensaje += "</table>";
    
    Util.MostrarModal("CAM",mensaje);
    $(TextoEquipo).text(JSON.stringify(CAM));
};

Comparacion.MostrarEquipo = function(Equipo,nombreEquipo){
    var textoEquipo = '#Equipo'+Equipo;
    $(textoEquipo).text(nombreEquipo);
    $("#modal").dialog("close");
};


Comparacion.DescuentosCliente = function () {
    if (window.ResultadoConsulta.Retenciones === 'Baja')
    {
        return "<span onclick='javascript:HerramientasRetenciones.Mostrar()'>Herramientas<span>";
    } else if (window.ResultadoConsulta.Retenciones === 'Migracion Down') {
        return  "<span onclick='javascript:DescuentosRetenciones.Mostrar()'>Descuentos<span>";
    }
};

Comparacion.ComercialDescuentos = function (descuentos)
{
    var resultado = "";
    for (var i in descuentos)
    {
        resultado += descuentos[i];
    }
    return resultado;
};

Comparacion.ComponenteCliente = function (Componente) {
    var descripcion = Componente.Nombre;
    var renta = Componente.RentaMonoproducto > 0 ? "S/" + Componente.RentaMonoproducto : "";
    return Comparacion.NombreRenta(descripcion,renta);
};

Comparacion.Nombre = function (ComponenteNombre, Opcion) {
    var Movimiento = "";
    var Nombre = "";
    var Renta = "";


    if (Opcion.movimiento[ComponenteNombre] === "Mantiene Monoproducto")
    {
        var componenteMono = Opcion.ComponentesMono[ComponenteNombre];
        Movimiento = "";
        Nombre = componenteMono.Nombre;
        Renta = "S/" + componenteMono.RentaMonoproducto;

    }
    else if (Opcion.producto.Componentes[ComponenteNombre].Presente)
    {
        var componente = Opcion.producto.Componentes[ComponenteNombre];
        Movimiento = Opcion.movimiento[ComponenteNombre];
        Nombre = componente.Nombre;
        Renta = componente.RentaMonoproducto > 0 ? "S/" + componente.RentaMonoproducto : "";
    }

    return Comparacion.MovimientoNombreRenta(Movimiento,Nombre,Renta);
};

Comparacion.MovimientoNombreRenta = function(Movimiento,Nombre,Renta){
    Movimiento = Comparacion.Movimiento(Movimiento);
    
    if(Movimiento === ''){
        return Comparacion.NombreRenta(Nombre,Renta);
    }
    
    
    return '<table><tr>'+
        '<td class="movimiento">'+Comparacion.Movimiento(Movimiento)+'</td>'+
        '<td class="nombre_3">'+Nombre+'</td>'+
        '<td class="renta">'+Renta+'</td>'+
        '</tr></table>';
};

Comparacion.Movimiento = function(movimiento){
   return movimiento === 'No Evalua Componente' ? '' : movimiento === 'Mantiene' ? '' : movimiento;
};


Comparacion.PaqueteNombre = function (Paquete)
{
    var PaqueteRenta = Paquete.Renta > 0 ? "S/" + Paquete.Renta : "";
    var PaqueteNombre = Paquete.Nombre !== null ? Paquete.Nombre : "";
    return Comparacion.NombreRenta(PaqueteNombre,PaqueteRenta);
};

Comparacion.NombreRenta = function(nombre,renta){
    
    if(renta === ""){
        return Comparacion.SoloNombre(nombre);
    }
    
    return '<table><tr>'+
        '<td class="nombre_2">'+nombre+'</td>'+
        '<td class="renta">'+renta+'</td>'+
        '</tr></table>';
};

Comparacion.SoloNombre = function(nombre){
    return nombre;
};


Comparacion.PremiumNombre = function (Componente, movimiento, ComponenteMono) {

    var Costo = "";
    var descripcion = "";

    if (ComponenteMono !== undefined && ComponenteMono !== null)
    {
        descripcion += '<div class="estelar">'+ComponenteMono.Nombre + "</div>";
        Costo = "S/" + ComponenteMono.RentaMonoproducto;
    }

    if (Componente.Presente)
    {
        descripcion += Componente.Nombre;
    }


    if (Componente.RentaMonoproducto > 0)
    {
        Costo = "S/" + Componente.RentaMonoproducto;
    }
    return Comparacion.MovimientoNombreRenta(movimiento,descripcion,Costo);

};

Comparacion.PremiumNombreCliente = function (Componente) {

    if (Componente.Presente)
    {
        var Costo = Componente.RentaMonoproducto > 0 ? "S/" + Componente.RentaMonoproducto : "";
        var Premium = Componente.Nombre;
        return Premium + ' ' + Costo;
    }
    return "";
};

Comparacion.Modems = function (Cliente) {
    var Modems = Cliente.Internet.ModemNombre;

    if (Cliente.SmartWifi === "1") {
        Modems += "<br><span class='error'>Compatible App Smart Wifi</span>";
    }
    return Modems;
};


Comparacion.CajasTexto = function (Componente, Clase)
{
    var resultado = "<td class='" + Clase + "' id ='Cliente_" + Componente + "'></td>";
    resultado += "<td class='" + Clase + "' id='Oferta1_" + Componente + "'></td>";
    resultado += "<td class='" + Clase + "' id='Oferta2_" + Componente + "'></td>";
    resultado += "<td class='" + Clase + "' id='Oferta3_" + Componente + "'></td>";
    return resultado;
};

Comparacion.ImprimirFila = function (Componente) {
    return "<tr>" + Parrilla.Etiqueta(Componente) + Comparacion.CajasTexto(Componente, "elementoComparacion") + "</tr>";
};

Comparacion.ImprimirTotal = function (Componente) {
    return "<tr>" + Parrilla.Etiqueta(Componente) + Comparacion.CajasTexto(Componente, "totalComparacion") + "</tr>";
};

Comparacion.ImprimirDescuento = function (Componente) {
    return "<tr>" + Parrilla.Etiqueta(Componente) + Comparacion.CajasTexto(Componente, "elementoComparacion mensajeResaltado") + "</tr>";
};



Comparacion.ImprimirBotones = function () {
    var resultado =
            "<tr>" +
            "<td></td>" +
            "<td></td>";
    resultado += Comparacion.ImprimirBoton(1);
    resultado += Comparacion.ImprimirBoton(2);
    resultado += Comparacion.ImprimirBoton(3);
    resultado += "</tr>";
    return resultado;
};

Comparacion.ImprimirBoton = function (id) {
    return "<td class='boton' onclick='javascript:Comparacion.MostrarDatosRegistro(" + id + ");'>Mostrar Registro</td>";
};

Comparacion.HTML = "";

Comparacion.ItemCabecera = function (texto) {
    return "<th class='cabeceraComparacion'>" + texto + "</td>";
};

Comparacion.CrearEstructuraHTML = function () {

    Comparacion.HTML = "";
    Comparacion.HTML += "<table id='TablaComparacion'>";
    Comparacion.HTML += "<tr><td></td>" +
            Comparacion.ItemCabecera("Actual") +
            Comparacion.ItemCabecera("Oferta 1") +
            Comparacion.ItemCabecera("Oferta 2") +
            Comparacion.ItemCabecera("Producto Seleccionado") + "</tr>" +
            Comparacion.ImprimirFila("TelefonoFijo") +
            Comparacion.ImprimirFila("Operacion") +
            Comparacion.ImprimirFila("Paquete") +
            Comparacion.ImprimirFila("Linea") +
            Comparacion.ImprimirFila("Plan") +
            Comparacion.ImprimirFila("Internet") +
            Comparacion.ImprimirFila("Modems") +
            Comparacion.ImprimirFila("Cable") +
            Comparacion.ImprimirFila("Premium") +
            Comparacion.ImprimirFila("Decos") +
            Comparacion.FilasMoviles() +
            
            Comparacion.ImprimirDescuento("Descuento") +
            Comparacion.ImprimirTotal("Subtotal") +
            Comparacion.ImprimirTotal("Adicionales") +
            Comparacion.ImprimirTotal("Total") +
            Comparacion.ImprimirTotal("Salto") +
            Comparacion.ImprimirBotones("Registro") +
            "</table>";

    $("#NuevoContenido").html(Comparacion.HTML);
};


Comparacion.FilasMoviles = function () {
    var cliente = window.ResultadoConsulta.Cliente;
    var resultado = "";
    
    var hay4Play = Parrilla.Categorias["4Play CATV HFC"].ofertas.length > 0 ? true : false;
    
    for (var i = 0; i < 10; ++i) {
        
        var breakNow = cliente.Componentes["Movil" + i] === undefined;
        if(hay4Play && i < 2) {
            breakNow = false;
        }
        
        if (breakNow ) {
            break;
        }
        resultado += Comparacion.ImprimirFila("NumeroCelular" + i);
        resultado += Comparacion.ImprimirFila("Movil" + i);
        resultado += Comparacion.ImprimirFila("Gigas" + i);
        resultado += Comparacion.ImprimirFila("EquipoMovil" + i);
    }
    return resultado;
};

Comparacion.MostrarDatosRegistro = function (Posicion)
{
    if (window.ResultadoConsulta.Opciones[Posicion] !== undefined) {
        var Opcion = window.ResultadoConsulta.Opciones[Posicion];
        Util.MostrarModal("Registro",Comparacion.MostrarCartillaDeRegistro(Opcion.Registro));
    }
};

Comparacion.MostrarCartillaDeRegistro = function (Registro)
{
    var PsRegistro = "";
    for (var i in Registro)
    {
        PsRegistro += '<tr>'
                + '<td class="elementoComparacion">' + Registro[i].Ps.Ps + '</td>'
                + '<td class="elementoComparacion">' + Registro[i].Ps.Nombre + '</td>'
                + '<td class="elementoComparacion">' + Registro[i].TipoOperacionComercial + '</td>'
                + '<td class="elementoComparacion">' + Registro[i].Caracteristica + '</td>'
                + '</tr>';
    }

    $ofertaPrincipal = '<table><tr>' +
            Parrilla.ItemCabecera("Ps") +
            Parrilla.ItemCabecera("Nombre") +
            Parrilla.ItemCabecera("Operacion Comercial") +
            Parrilla.ItemCabecera("Caracteristica") +
            '</tr>' + PsRegistro + '</table>' + '<br>';

    return $ofertaPrincipal;
};



Comparacion.Decos = function (InfoDecos)
{
    var DecosAgrupados = [];
    var Decos = "";

    for (var i in InfoDecos) {
        var Tipo = InfoDecos[i].Tipo;
        if (DecosAgrupados[Tipo] === undefined) {
            DecosAgrupados[Tipo] = 0;
        }
        ++DecosAgrupados[Tipo];
    }

    for (var Tipo in DecosAgrupados) {
        var Cantidad = DecosAgrupados[Tipo];
        var Texto = Cantidad > 1 ? ' Decos ' : ' Deco ';
        Decos += DecosAgrupados[Tipo] + Texto + Tipo + "<br>";
    }

    return Decos;
};
