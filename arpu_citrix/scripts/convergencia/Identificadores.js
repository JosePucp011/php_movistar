
/* Flag para utilizar el botón */
var blBotonConsultar=1;

/* global Util, Comparacion, Parrilla, Documento, departamentos, Hogar, moment, google */
var Direccion = {};

/*Cambio 001*/
var scoringMovil1Request;
var scoringMovil1Response;
var scoringMovilPorDefecto = [];

var CodigoScoringFijo='';
var CodigoScoringMovil1='';
var CodigoScoringMovil2='';

/*Cambio 002*/
scoringMovilPorDefecto[0] = 
'<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
+'<soap-env:Header xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">'
+'        <ns0:HeaderOut xmlns:ns0="http://telefonica.com/globalIntegration/header">'
+'            <ns0:originator>PE:CMS:CMS:CMS</ns0:originator>'
+'            <ns0:destination>PE:TDP:APPCalculadoraMT:APPCalculadoraMT</ns0:destination>'
+'            <ns0:execId>a2aa1107-e75c-44ed-90fa-2f96faf01e86</ns0:execId>'
+'            <ns0:timestamp>2019-02-19T18:23:52.711-05:00</ns0:timestamp>'
+'            <ns0:msgType>RESPONSE</ns0:msgType>'
+'        </ns0:HeaderOut>'
+'    </soap-env:Header>'
+'    <soapenv:Body>'
+'        <ns2:obtenerScoreCrediticioResponse xmlns:ns2="http://integracion.osb.telefonica.com/services/64003">'
+'            <codError>0000</codError>'
+'            <desError>0000</desError>'
+'            <codTipError>0000</codTipError>'
+'            <desTipError>0000</desTipError>'
+'            <Score>9999</Score>'
+'            <menScore>WA 0000</menScore>'
+'            <idConScoring>999999999999</idConScoring>'
+'            <limCredito>999</limCredito>'
+'            <canLineas>1</canLineas>'
+'            <restricciones>Precio de Equipo: Todos los Cargo Fijo con PPF  /  Precio de Chip: S/. 1  /  Para CAEQ: UPSELL máximo de S/. 20.0  Código de Financiamiento: 3</restricciones>'
+'            <accion>APROBAR</accion>'
+'        </ns2:obtenerScoreCrediticioResponse>'
+'    </soapenv:Body>'
+'</soapenv:Envelope>';

/*Cambio 003*/	
var scoringFijoPorDefecto = [];

scoringFijoPorDefecto[0] = 
    {
        "CodConsulta": "90009000999999999",
        "Accion": "CLIENTE NUEVO",
        "DeudaAtis": 221.75,
        "DeudaCms": 0,
        "OperacionComercial": [
            {
                "CodOpe": "ALTA PURA",
                "Renta": "700",
                "Segmento": "B",
                "ContadorDetalle": 6,
                "Detalle": [
                    {
                        "CodProd": "102",
                        "DesProd": "Trío",
                        "CuotaFin": 0,
                        "MesesFin": 0,
                        "PagoAdelantado": 0,
                        "NroDecosAdic": 2
                    },
                    {
                        "CodProd": "101",
                        "DesProd": "Dúo BA",
                        "CuotaFin": 5,
                        "MesesFin": 6,
                        "PagoAdelantado": 0,
                        "NroDecosAdic": 0
                    },
                    {
                        "CodProd": "104",
                        "DesProd": "Dúo TV",
                        "CuotaFin": 5,
                        "MesesFin": 6,
                        "PagoAdelantado": 0,
                        "NroDecosAdic": 2
                    },
                    {
                        "CodProd": "106",
                        "DesProd": "Mono BA",
                        "CuotaFin": 5,
                        "MesesFin": 6,
                        "PagoAdelantado": 0,
                        "NroDecosAdic": 0
                    },
                    {
                        "CodProd": "105",
                        "DesProd": "Mono TV",
                        "CuotaFin": 5,
                        "MesesFin": 6,
                        "PagoAdelantado": 120,
                        "NroDecosAdic": 2
                    },
                    {
                        "CodProd": "107",
                        "DesProd": "Mono Línea",
                        "CuotaFin": 5,
                        "MesesFin": 6,
                        "PagoAdelantado": 0,
                        "NroDecosAdic": 0
                    }
                ]
            },
            {
                "CodOpe": "ALTA COMPONENTE",
                "Renta": "0",
                "Segmento": "B",
                "ContadorDetalle": 0
            }
        ]
    };



Direccion.GenerarOpciones = function(arreglo){
    var opciones = "";
    for(var i in arreglo){
opciones += "<option value='"+i+"'>"+arreglo[i].name+"</option>";
     }
     return opciones;
};

Direccion.CambiarProvincia = function(){
    var i = $('#departamento').val();
    var j = $('#provincia').val();
    
    Direccion.ActualizarDistritosParaProvincia(i,j);
    
};

/**
 * Actualiza el combo distrito cuando cambia la provincia
 * @param {int} codigoDepartamento
 * @param {int} codigoProvincia
 */
Direccion.ActualizarDistritosParaProvincia = function(codigoDepartamento,codigoProvincia){
    if(departamentos[codigoDepartamento] !== undefined){
        var departamento = departamentos[codigoDepartamento];
        if(departamento.provincias[codigoProvincia] !== undefined){
   var provincia = departamento.provincias[codigoProvincia];
   $('#distrito').html(Direccion.GenerarOpciones(provincia.distritos));
        }
    }
};

/**
 * Actualiza el combo provincia en funcion al cambio del departamento
 * @param {int} codigoDepartamento
 */
Direccion.ActualizarProvinciasParaDepartamento = function(codigoDepartamento){
    if (departamentos[codigoDepartamento] !== undefined) {
        var departamento = departamentos[codigoDepartamento];
        $('#provincia').html(Direccion.GenerarOpciones(departamento.provincias));
        Direccion.CambiarProvincia();
    }
};


Direccion.CambiarDepartamento = function () {
    var i = $('#departamento').val();
    Direccion.ActualizarProvinciasParaDepartamento(i);
};



$(function() {    
      
    $('#close_modal,#close_modal1').click(function(){
        $(this).parent().parent().parent().parent().parent().parent().parent().css('display','none');
        $(this).parent().parent().parent().parent().parent().parent().parent().dialog("close");
    });    
    
    departamentos.sort(function(a,b){

        if(a.name < b.name){
   return -1;
        } else if(a.name === b.name){
   return 0;
        } else {
   return 1;
        }
    });
    
    
     $('#departamento').html(Direccion.GenerarOpciones(departamentos));
     
     $('#departamento').val("0");
     Direccion.ActualizarProvinciasParaDepartamento(0);
     Direccion.ActualizarDistritosParaProvincia(0,0);
     
     $('#departamento').change(Direccion.CambiarDepartamento);
     $('#provincia').change(Direccion.CambiarProvincia);
 });
 
 
 


var Identificadores = {};

Identificadores.URL = 'Http/ConvergenteRESTInterno.php';


var menos1Mes = moment().format('YYYYMMDD');
var menos8Meses = moment().add(-7,'months').format('YYYYMMDD');
var ocho8Meses = moment().add(-24,'months').format('YYYYMMDD');

var menos4Meses = moment().add(-3,'months').format('YYYYMMDD');
var menos16Meses = moment().add(-15,'months').format('YYYYMMDD');
var dieciseis16Meses = moment().add(-17,'months').format('YYYYMMDD');


Hogar.SiguienteFormulario = Identificadores;

Identificadores.EstaConsultado = false;
Identificadores.identificador = null;
Identificadores.ScoringMovil = null;
Identificadores.ScoringFijo = null;


Identificadores.Limpiar = function(){
    $('#error_movil1').html('');
    $('#error_movil2').html('');    
    $('#Formulario_Identificadores').hide();
    $('#Display_Ofertas').hide();
    $('#AvisoLimite').hide(); // aviso
    $('#CodigoUltimaConsulta').text('');
    
    Identificadores.RequiereConsultaGIS = true;
    Identificadores.RequiereConsultaABD1 = true;
    Identificadores.RequiereConsultaABD2 = true;
    Identificadores.RequiereConsultaAlta1 = false;
    Identificadores.RequiereConsultaAlta2 = false;
    Identificadores.RequiereConsultaScoringFijo = true;
    Identificadores.ultimaConsulta = null;
    Identificadores.EstaConsultado = false;
    Identificadores.ScoringMovil = null;
    Identificadores.ScoringFijo = null;
    Identificadores.distrito_texto = "";
    Identificadores.provincia_texto = "";    

    Identificadores.movil1_portabilidad = false;
    Identificadores.movil2_portabilidad = false;

    $('#telefono').val('');
    $('#departamento').val("0");
     Direccion.ActualizarProvinciasParaDepartamento(0);
     Direccion.ActualizarDistritosParaProvincia(0,0);
    
    $('#telefono').show();
    $('#telefono').prop('disabled',false);
    $('#telefono_notengo').prop('checked',false);
    $('#telefono_notengo').hide();
    $('#telefono_notengo_etiqueta').hide();
    $('#departamento').hide();
    $('#departamento_etiqueta').hide();
    
    $('#provincia').hide();
    $('#provincia_etiqueta').hide();    
    
    $('#distrito').hide();
    $('#distrito_etiqueta').hide();
    $('#error_telefonoFijo').hide();
    
    Identificadores.OcultarMapa();
    
    
    $('#movil1').show();
    $('#movil1').prop('disabled',false);
    $('#movil1_notengo').prop('checked',false);
    $('#movil1_notengo').hide();
    $('#movil1_notengo_etiqueta').hide();
    $('#movil1_operador').hide();
    $('#movil1_operador_etiqueta').hide();
    $('#movil1_tipo_etiqueta').hide();
    $('#movil1_tipo').hide();
    $('#movil1_cargofijo_etiqueta').hide();
    $('#movil1_cargofijo').hide();    
    $('#error_movil1').hide();
    $('#error_movil1').html('');
    
    $('#movil1_fechaAlta').hide();
    $('#movil1_fechaAlta_etiqueta').hide();

    $('#movil1_fechaActivacion').hide();
    $('#movil1_fechaActivacion_etiqueta').hide();
    
    $('#cliente_movil1_renta').val('');
    
    $('#movil2').val('');
    $('#movil2').show();
    $('#movil2').prop('disabled',false);
    $('#movil2_notengo').prop('checked',false);
    $('#movil2_notengo_etiqueta').hide();
    $('#movil2_notengo').hide();
    $('#movil2_operador').hide();
    $('#movil2_operador_etiqueta').hide();
    $('#movil2_tipo_etiqueta').hide();
    $('#movil2_tipo').hide();
    $('#movil2_cargofijo_etiqueta').hide();
    $('#movil2_cargofijo').hide();
    
    $('#movil2_fechaAlta').hide();
    $('#movil2_fechaAlta_etiqueta').hide();
    $('#movil2_fechaActivacion').hide();
    $('#movil2_fechaActivacion_etiqueta').hide();
    
    $('#cliente_movil2_renta').val('');
    
    $('#error_movil2').hide();
    $('#error_movil2').html('');
    $('#cobertura').val('');
    
    $('#scoringfijo').val('');
    $('#scoringmovil1').val('');
    $('#scoringmovil1_detalle').val('');
    $('#scoringmovil1_restriccion').val('');
    $('#scoringmovil1_score').val('');
    $('#scoringmovil2').val('');
    $('#scoringmovil2_detalle').val('');
    $('#scoringmovil2_restriccion').val('');
    $('#scoringmovil2_score').val('');
    $('#Formulario_Validacion').hide();
    
    Identificadores.LimpiarHijos();
};


Identificadores.Inicializar = function(){
    Identificadores.Limpiar();
    Identificadores.ConfigurarCheckbox();
    Identificadores.ConfigurarComboOperadores();
    Identificadores.ConfigurarBoton();
};

Identificadores.ConfigurarBoton = function(){
    $('#consultar').click(Identificadores.Consultar);
    $('#direccion_x').keyup(function(){Identificadores.OnDatoActualizarCoordenada('#direccion_x');});
    $('#direccion_y').keyup(function(){Identificadores.OnDatoActualizarCoordenada('#direccion_y');});
};

Identificadores.Consultar = function(){    
    if(blBotonConsultar==1) {
        blBotonConsultar=0;

        $('#CodigoUltimaConsulta').text('');
        $('#Display_Ofertas').hide();
        $('#AvisoLimite').hide();// aviso
        Identificadores.LeerDatosConsulta();
        
        if(!Identificadores.ValidarFormulario()){
            blBotonConsultar=1;
            return;
        }

        Identificadores.LimpiarScoring();
        $('#Formulario_Validacion').show();
        Identificadores.ConsultarOferta();
    } else {
        alert("Procesando Consulta de Evaluación Crediticia...");
    }
};

/** Validate que este lleno telefono1, movil1, movil2 y cobertura */
Identificadores.ValidarFormulario = function(){
    if(Identificadores.EsTelefonoInvalido()){
        alert('Llenar el telefono fijo o indicar que no tiene telefono fijo');
        return false;
    }
    
    if(Identificadores.EsMovil1Invalido()){
        alert('Llenar el movil #1 o indicar que no tiene');
        return false;
    }
    
    if(Identificadores.EsMovil2Invalido()){
        alert('Llenar el movil #2 o indicar que no tiene');
        return false;
    }
    
    if($('#error_movil1>span').html() == 'Cliente Movistar MT' && !Identificadores.movil1_notengo){
        alert('Ingresar correctamente el movil #1 o indicar que no tiene');
        return false;
    }

    if($('#error_movil2>span').html() == 'Cliente Movistar MT' && !Identificadores.movil2_notengo){
        alert('Ingresar correctamente el movil #2 o indicar que no tiene');
        return false;
    }
    
    if(Identificadores.telefono_notengo && Identificadores.EsUbicacionInvalida()){
        alert('Llene el departamento,provincia,distrito para poder hacer la consulta de scoring');
        return false;
    }
    
    
    if($('#cobertura').val() === ""){
       alert('No ha llenado la direccion del cliente, por favor, ingresar la direccion');
       return false; 
    }
    
    if($('#direccion_texto').val() === "" || $('#direccion_texto').val() === "-"){
       alert('No ha llenado la direccion del cliente para registro, por favor, ingresar la direccion');
       return false; 
    }  

    var coordenadaX = $('#direccion_x').val().toUpperCase();
    var coordenadaY = $('#direccion_y').val().toUpperCase();
    
    coordenadaX = trimStr(coordenadaX);
    coordenadaY = trimStr(coordenadaY);

    //Validar Coordenadas: Caja de Texto Vacío
    if(coordenadaX === "" || coordenadaX === "-"
            || coordenadaY === "" || coordenadaY === "-"){
       alert('No ha llenado las coordenadas del cliente para registro, por favor, ingresar las coordenadas');
       return false; 
    }

    //Validar Coordenadas: Caracteres No Permitidos
    if(!(coordenadaX == "X:" && coordenadaY == "Y:")) {
        var mensajeCorregirCoorX = 'Ingresar correctamente la Coordenada X, ej.: -77.02647969999998';
        var mensajeCorregirCoorY = 'Ingresar correctamente la Coordenada Y, ej.: -12.0966401';
        var resultadoX = '';
        var resultadoY = '';
        
        resultadoX = coordenadaX.replace(/-/,'');
        resultadoX = resultadoX.replace(/\./,'');
        if(resultadoX.match(/[^0-9]/) != null) {
            alert(mensajeCorregirCoorX);
            return false; 
        }

        resultadoY = coordenadaY.replace(/-/,'');
        resultadoY = resultadoY.replace(/\./,'');
        if(resultadoY.match(/[^0-9]/) != null) {
            alert(mensajeCorregirCoorY);
            return false; 
        }

        var coordenadaXInt = parseInt(coordenadaX);
        if(isNaN(coordenadaXInt)) {
            alert(mensajeCorregirCoorX);
            return false; 
        } else {
            coordenadaXIntSinNeg = -1*coordenadaXInt;
            if(!(coordenadaXIntSinNeg>=0) || coordenadaXInt == coordenadaX) {
                alert(mensajeCorregirCoorX);
                return false; 
            }
        }

        var coordenadaYInt = parseInt(coordenadaY);
        if(isNaN(coordenadaYInt)) {
            alert(mensajeCorregirCoorY);
            return false; 
        } else {
            coordenadaYIntSinNeg = -1*coordenadaYInt;
            if(!(coordenadaYIntSinNeg>=0) || coordenadaYInt == coordenadaY) {
                alert(mensajeCorregirCoorY);
                return false;
            }
        }
    }
    
    if(Identificadores.cobertura === "HFC" || Identificadores.cobertura === "FTTH"){
        return true;
    } else {
        alert('El cliente no esta en cobertura HFC/FTTH y no aplica a ofertas Movistar Total');
        return false;
    }
    
};


/** Llamar a los servicios de scoring y luego enviar la consulta convergente */
/*Cambio 004*/	
Identificadores.ConsultarOferta = function () {
    var scoringFijo = Identificadores.ConsultarScoringFijo();

    if (scoringFijo === undefined) {
        $.when(Identificadores.ConsultarScoringMovil1())
        .done(function(scoringMovil1) {
            scoringMovil1Response=scoringMovil1;
            $.when(Identificadores.ConsultarScoringMovil2())
            .done(function(scoringMovil2) {
                $.when(Identificadores.ConsultarCalculadoraSinFija(scoringMovil1,scoringMovil2))
                .done(function() {
                    blBotonConsultar=1;
                })
                .fail(function() {
                    blBotonConsultar=1;
                });
            })
            .fail(function (a){
                $.when(Identificadores.ConsultarCalculadoraSinFija(scoringMovilPorDefecto[0],scoringMovilPorDefecto[0]))
                .done(function() {
                    blBotonConsultar=1;
                })
                .fail(function() {
                    blBotonConsultar=1;
                });
            });
        })
        .fail(function (a){
            $.when(Identificadores.ConsultarCalculadoraSinFija(scoringMovilPorDefecto[0],scoringMovilPorDefecto[0]))
            .done(function() {
                blBotonConsultar=1;
            })
            .fail(function() {
                blBotonConsultar=1;
            });
        });
    } else {
        $.when(scoringFijo,Identificadores.ConsultarScoringMovil1())
        .done(function(scoringFija,scoringMovil1) {
            scoringMovil1Response=scoringMovil1;
            $.when(Identificadores.ConsultarScoringMovil2())
            .done(function(scoringMovil2) {
                $.when(Identificadores.ConsultarCalculadoraConFija(scoringFija,scoringMovil1,scoringMovil2))
                .done(function() {
                    blBotonConsultar=1;
                })
                .fail(function() {
                    blBotonConsultar=1;
                });
            })
            .fail(function (a) {
                $.when(Identificadores.ConsultarCalculadoraConFija(scoringFijoPorDefecto,scoringMovilPorDefecto,scoringMovilPorDefecto[0]))
                .done(function() {
                    blBotonConsultar=1;
                })
                .fail(function() {
                    blBotonConsultar=1;
                });
            });
        })
        .fail(function (a,b) {
            $.when(Identificadores.ConsultarCalculadoraConFija(scoringFijoPorDefecto,scoringMovilPorDefecto,scoringMovilPorDefecto[0]))
            .done(function() {
                blBotonConsultar=1;
            })
            .fail(function() {
                blBotonConsultar=1;
            });
        });
    }
};


Identificadores.UnificarScoring = function(scoring1,scoring2){
    var scoreUnificado = Math.max(parseInt(scoring1.Score),parseInt(scoring2.Score));
    
    var accionUnificada = 'APROBAR';
    if(scoring1.accion !== 'APROBAR'){
        accionUnificada = scoring1.accion;
    }
    
    if(scoring2.accion !== 'APROBAR'){
        accionUnificada = scoring2.accion;
    }

    var movil1renta = $('#cliente_movil1_renta').val();
    var movil2renta = $('#cliente_movil2_renta').val();
    
    if(movil1renta === ''){
        movil1renta = 0;
    }
    if(movil2renta === ''){
        movil2renta = 0;
    }
    
    
    var RentaUnificado = Math.min(parseInt(movil1renta),parseInt(movil2renta));
 
    
    var canLineasUnificada = Math.min(parseInt(scoring1.canLineas),parseInt(scoring2.canLineas));
    var limCreditoUnificado = Math.max(parseInt(scoring1.limCredito),parseInt(scoring2.limCredito));
    
    var limCredito = limCreditoUnificado;
    
    var scoring = {
        Score : scoreUnificado,
        accion : accionUnificada,
        canLineas : canLineasUnificada,
        codError : scoring1.codError,
        codTipError : scoring1.codTipError,
        desError : scoring1.desError,
        idConScoring : scoring1.idConScoring,
        limCredito : limCredito,
        menScore1 : scoring1.menScore,
        menScore2 : scoring2.menScore,
        restricciones1 : scoring1.restricciones,
        restricciones2 : scoring2.restricciones
    };
    return scoring;
    
};

/*Cambio 005*/	
Identificadores.ConsultarCalculadoraSinFija = function(scoringMovil1,scoringMovil2){
    try {
        var mensajeError1 = "Error no controlado en el proveedor de servicios";
        var mensajeError2 = "Error de negocio producido en la lógica del servicio invocado";
        var mensajeError3 = "El proveedor no responde";
        var errorResp1 = -1;
        try{
            errorResp1 = scoringMovil1.indexOf(mensajeError1);
            if(errorResp1 == -1) {
                errorResp1 = scoringMovil1.indexOf(mensajeError2);
            }
            if(errorResp1 == -1) {
                errorResp1 = scoringMovil1.indexOf(mensajeError3); // Nooo 
            }
        } catch(e) {
            errorResp1 = scoringMovil1[0].indexOf(mensajeError1);
            if(errorResp1 == -1) {
                errorResp1 = scoringMovil1[0].indexOf(mensajeError2);
            }
            if(errorResp1 == -1) {
                errorResp1 = scoringMovil1[0].indexOf(mensajeError3); // Nooo
            }
        }

        var errorResp2 = -1;
        try{
            errorResp2 = scoringMovil2.indexOf(mensajeError1);
            if(errorResp2 == -1) {
                errorResp2 = scoringMovil2.indexOf(mensajeError2);
            }
            if(errorResp2 == -1) {
                errorResp2 = scoringMovil2.indexOf(mensajeError3); // Nooo
            }
        } catch(e){
            errorResp2 = scoringMovil2[0].indexOf(mensajeError1);
            if(errorResp2 == -1) {
                errorResp2 = scoringMovil2[0].indexOf(mensajeError2);
            }
            if(errorResp2 == -1) {
                errorResp2 = scoringMovil2[0].indexOf(mensajeError3); // Nooo
            }
        }
        
        if(errorResp1>=0 || errorResp2>=0) {
            scoringMovil1 = scoringMovilPorDefecto[0];
            scoringMovil2 = scoringMovilPorDefecto[0];
        }
     
        window.scoringT1 = scoringMovil1;
        window.scoringT2 = scoringMovil2;
        
        var scoring1 = null;
        var scoring2 = null;

        CodigoScoringFijo='';
        CodigoScoringMovil1='';
        CodigoScoringMovil2='';
        
        //if(scoringMovil1[0] === undefined){
            var xml = $.xml2json(scoringMovil1);
            
            if(xml.Body === undefined){
                scoring1 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"];
                CodigoScoringMovil1 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"]["idConScoring"];
            } else {
                scoring1 = xml.Body.obtenerScoreCrediticioResponse;
                CodigoScoringMovil1 = xml.Body.obtenerScoreCrediticioResponse.idConScoring;
            }
        /*} else {
            
            var xml = $.xml2json(scoringMovil1[0]);
            
            if(xml.Body === undefined){
                scoring1 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"];
                CodigoScoringMovil1 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"]["idConScoring"];
            } else {
                scoring1 = xml.Body.obtenerScoreCrediticioResponse;
                CodigoScoringMovil1 = xml.Body.obtenerScoreCrediticioResponse.idConScoring;
            }
        }*/
        
        
        //if(scoringMovil2[0] === undefined){
            var xml = $.xml2json(scoringMovil2);
            
            if(xml.Body === undefined){
                scoring2 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"];
                CodigoScoringMovil2 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"]["idConScoring"];
            } else {
                scoring2 = xml.Body.obtenerScoreCrediticioResponse;
                CodigoScoringMovil2 = xml.Body.obtenerScoreCrediticioResponse.idConScoring;
            }
        /*} else {
            var xml = $.xml2json(scoringMovil2[0]);
            
            if(xml.Body === undefined){
                scoring2 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"];
                CodigoScoringMovil2 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"]["idConScoring"];
            } else {
                scoring2 = xml.Body.obtenerScoreCrediticioResponse;
                CodigoScoringMovil2 = xml.Body.obtenerScoreCrediticioResponse.idConScoring;
            }
        }*/

        if(scoring1.menScore == "WA 0000"){
            scoring1.menScore = 'No Aplica a financiamiento';
            
            if(Identificadores.movil1_portabilidad || Identificadores.movil1 == null) {
                scoring1.Score = '1905';
                scoring1.limCredito = 120;
            } else {
                scoring1.Score = '2700';
                scoring1.limCredito = 200;
            }
        }
        
        if(scoring2.menScore == "WA 0000"){
            scoring2.menScore = 'No Aplica a financiamiento';
            
            if(Identificadores.movil2_portabilidad || Identificadores.movil2 == null) {
                scoring2.Score = '1905';
                scoring2.limCredito = 120;
            } else {
                scoring2.Score = '2700';
                scoring2.limCredito = 200;
            }
        }

        var scoring = Identificadores.UnificarScoring(scoring1,scoring2);
        Identificadores.MostrarScoringMovil(scoring);
        Identificadores.ConsultarCalculadora();
    } catch(err) {
        blBotonConsultar=1;
    }
};


/*Cambio 006*/	
Identificadores.ConsultarCalculadoraConFija = function(scoringFija,scoringMovil1,scoringMovil2){
	try {
        var mensajeError1 = "Error no controlado en el proveedor de servicios";
        var mensajeError2 = "Error de negocio producido en la lógica del servicio invocado";
        var mensajeError3 = "El proveedor no responde";
        var mensajeError4 = "Error de negocio producido en la lógica del servicio invocado";
        var mensajeError5 = "Business Error";
        var errorResp1 = -1;

        try{
            errorResp1 = scoringFija[0].ClientException.exceptionMsg.indexOf(mensajeError1);
            if(errorResp1 == -1) {
                errorResp1 = scoringFija[0].ClientException.exceptionMsg.indexOf(mensajeError2);
            }
            if(errorResp1 == -1) {
                errorResp1 = scoringFija[0].ClientException.exceptionMsg.indexOf(mensajeError3);
            }
            if(errorResp1 == -1) {
                errorResp1 = scoringFija[0].ClientException.exceptionMsg.indexOf(mensajeError4);
            }
            if(errorResp1 == -1) {
                errorResp1 = scoringFija[0].ClientException.exceptionMsg.indexOf(mensajeError5);
            }
        } catch(e) {
            errorResp1 = -1;
        }

        var errorResp2 = -1;
        if(scoringMovil1[0] === undefined){
            errorResp2 = scoringMovil1.indexOf(mensajeError1);
            if(errorResp2 == -1) {
                errorResp2 = scoringMovil1.indexOf(mensajeError2);
            }
        } else {
            errorResp2 = scoringMovil1[0].indexOf(mensajeError1);
            if(errorResp2 == -1) {
                errorResp2 = scoringMovil1[0].indexOf(mensajeError2);
            }
        }

        var errorResp3 = -1;
        if(scoringMovil2[0] === undefined){
            errorResp3 = scoringMovil2.indexOf(mensajeError1);
            if(errorResp3 == -1) {
                errorResp3 = scoringMovil2.indexOf(mensajeError2);
            }
        } else {
            errorResp3 = scoringMovil2[0].indexOf(mensajeError1);
            if(errorResp3 == -1) {
                errorResp3 = scoringMovil2[0].indexOf(mensajeError2);
            }
        }

        if(errorResp1>=0) {
            scoringFija = scoringFijoPorDefecto;
        }

        if(errorResp2>=0 || errorResp3>=0) {
            scoringMovil1 = scoringMovilPorDefecto;
            scoringMovil2 = scoringMovilPorDefecto;
        }

        window.scoringT1 = scoringMovil1;
        window.scoringT2 = scoringMovil2;
        
        var scoring1 = null;
        var scoring2 = null;

        CodigoScoringFijo='';
        CodigoScoringMovil1='';
        CodigoScoringMovil2='';
        
        if(scoringMovil1[0] === undefined){
            var xml = $.xml2json(scoringMovil1);

            if(xml.Body === undefined){
                scoring1 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"];
                CodigoScoringMovil1 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"]["idConScoring"];
            } else {
                scoring1 = xml.Body.obtenerScoreCrediticioResponse;
                CodigoScoringMovil1 = xml.Body.obtenerScoreCrediticioResponse.idConScoring;
            }
        } else {
            
            var xml = $.xml2json(scoringMovil1[0]);
            
            if(xml.Body === undefined){
                scoring1 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"];
                CodigoScoringMovil1 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"]["idConScoring"];
            } else {
                scoring1 = xml.Body.obtenerScoreCrediticioResponse;
                CodigoScoringMovil1 = xml.Body.obtenerScoreCrediticioResponse.idConScoring;
            }
        }
        
        
        if(scoringMovil2[0] === undefined){
            var xml = $.xml2json(scoringMovil2);

            if(xml.Body === undefined){
                scoring2 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"];
                CodigoScoringMovil2 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"]["idConScoring"];
            } else {
                scoring2 = xml.Body.obtenerScoreCrediticioResponse;
                CodigoScoringMovil2 = xml.Body.obtenerScoreCrediticioResponse.idConScoring;
            }
        } else {
            
			try{
				var xml = $.xml2json(scoringMovil2[0]);
            }catch(e){
				var xml = $.xml2json(scoringMovil2);                
            }

            if(xml.Body === undefined){
                scoring2 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"];
                CodigoScoringMovil2 = xml["soapenv:Body"]["ns2:obtenerScoreCrediticioResponse"]["idConScoring"];
            } else {
                scoring2 = xml.Body.obtenerScoreCrediticioResponse;
                CodigoScoringMovil2 = xml.Body.obtenerScoreCrediticioResponse.idConScoring;
            }
        }
        
        if(scoring1.menScore == "WA 0000"){
            scoring1.menScore = 'No Aplica a financiamiento';
            
            if(Identificadores.movil1_portabilidad || Identificadores.movil1 == null) {
                scoring1.Score = '1905';
                scoring1.limCredito = 120;
            } else {
                scoring1.Score = '2700';
                scoring1.limCredito = 200;
            }
        }
        
        if(scoring2.menScore == "WA 0000"){
            scoring2.menScore = 'No Aplica a financiamiento';
            
            if(Identificadores.movil2_portabilidad || Identificadores.movil2 == null) {
                scoring2.Score = '1905';
                scoring2.limCredito = 120;
            } else {
                scoring2.Score = '2700';
                scoring2.limCredito = 200;
            }
        }
        
        CodigoScoringFijo=scoringFija[0].CodConsulta;
        Identificadores.MostrarScoringFijo(scoringFija[0]);
        var scoring = Identificadores.UnificarScoring(scoring1,scoring2);
        Identificadores.MostrarScoringMovil(scoring);
        Identificadores.ConsultarCalculadora();
    } catch(err) {
        blBotonConsultar=1;
    }
};

Identificadores.ConsultarCalculadora = function(){
    
    $('#CodigoUltimaConsulta').text('');
    
    var data = new Object();
    
    if(Identificadores.telefono !== null){
        data.telefono = Identificadores.telefono;
    }
    
    if(Identificadores.movil1 !== null){
        data.movil1 = Identificadores.movil1;
        
    }
    
    if(Identificadores.movil2 !== null){
        data.movil2 = Identificadores.movil2;
        
    }
    data.fechaAlta1 = $('#movil1_fechaAlta').val();
        data.codigoSegmento1 = $('#movil1_tipo').val();
    data.fechaAlta2 = $('#movil2_fechaAlta').val();
        data.codigoSegmento2 = $('#movil2_tipo').val();
    
    if(Documento.documento !== null){
        data.documento = Documento.documento;
        data.tipoDocumento = Documento.tipoDocumento;
    }
    
    var scoringFijo = $('#scoringfijo').val();
    var scoringMovil = $('#scoringmovil').val();
    
    $('#scoringconv_fijo').val(scoringFijo);
    $('#scoringconv_movil').val(scoringMovil);
    
    if(scoringFijo === 'APROBAR' && scoringMovil === 'APROBAR'){
        $('#scoringconv').val('APROBAR');
    } else if(scoringFijo !== 'APROBAR'){
        $('#scoringconv').val(scoringFijo);
    } else {
        $('#scoringconv').val(scoringMovil);
    }
    
    
    
    data.Tipo4play = $('#Tipo4play').val();
    data.scoringFijo = $('#scoringfijo_renta').val();
    data.scoringMovil1 = $('#scoringmovil_score').val();
    data.limiteCrediticio = $('#scoringmovil_limite').val();
    data.scoringFijoResultado = $('#scoringfijo').val();
    data.scoringMovilResultado = $('#scoringmovil').val();
    data.scoringFijoCodigo = $('#scoringfijo_codigo').val();
    data.scoringMovilCodigo = $('#scoringmovil_codigo').val();
    data.scoringmovil1_mensaje = $('#scoringmovil1_mensaje').val();
    data.scoringmovil1_detalle = $('#scoringmovil1_detalle').val();
    data.movil1_cargofijo = $('#movil1_cargofijo').val();
    data.scoringmovil2_mensaje = $('#scoringmovil2_mensaje').val();
    data.scoringmovil2_detalle = $('#scoringmovil2_detalle').val();
    data.movil2_cargofijo = $('#movil2_cargofijo').val();
    data.y = $('#direccion_y').val();
    data.x = $('#direccion_x').val();
    
    data.departamento = Identificadores.departamento_texto;
    data.provincia = Identificadores.provincia_texto;
    data.distrito = Identificadores.distrito_texto;
    data.direccion = $('#direccion_texto').val();
    data.cobertura = Identificadores.cobertura;

    Util.ConsultaAjax(Identificadores.URL,data,Identificadores.RespuestaConsulta);
};

Identificadores.ImprimirSeleccionarOferta = function(){

    var html = '';
    
    html += "<table><tr><td  style='width: auto;'>" +
       "<label for='seleccionProducto'>Ofertas Movistar Total:  </label>" +
       "<select id='seleccionProducto' onchange='Identificadores.MostrarOfertaSeleccionada();'>" +
  "<option value='-'>-</option>" +
       "</select>" +
   "</td></tr></table>";
    
    return html;
};

Identificadores.ImprimirOfertaCabecera = function(){

    var html = "<tr><td></td><td></td><th><h3>Tenencia</h3></th>";
    for(var i = 1; i <= 4; ++i){
        if(i !== 4)
        {
   html += "<th><h3>Oferta " + i + "</h3></th>";
        }else{
   html += "<th><h10> Adicional </h10></th>";
        }
    }   
    html += "</tr>";
    
    return html;
};

Identificadores.ImprimirOfertaComponenteFijo = function(respuesta,componente){

    var componenteNombre = componente;
    
    if(componente === 'Bloque'){
        componenteNombre = 'Bloques TV Adicionales';
    }
    if(componente === 'Adicional'){
        componenteNombre = 'Servicios Adicionales';
    }   
    var html = "<tr>";
    
    if(componente === 'Linea'){
        var cantRowSpan = 9; 
        if(Documento.EsMovistarTotal) {
            cantRowSpan = 8;
        }
        html += "<td rowspan='" + cantRowSpan + "'  class='titulo_seccion'>" +
        "<div>Telefono Fijo</div>" +
        "<div> "+ respuesta.Cliente.Fijo.Numero +" </div>" +
        "<div> "+ respuesta.Cliente.Fijo.Categoria +" </div>" +
        "<div> "+ respuesta.Cliente.Fijo.Titular +" </div>" +
    "</td>";     
    }
    
    if(componente !== 'Renta'){
        if(componente === 'Adicional'){
            var styleAdic = '';
            var classAdic = '';
            if(respuesta.Cliente.Fijo.MovistarTotal) {
                var styleAdic = 'border-bottom: 1px solid rgb(0,176,240);';
                var classAdic =  "class='separacion'";
            }

            html += "<th class='etiqueta' style='" + styleAdic + "'>" + componenteNombre + "</th>";
            html += "<td " + classAdic + ">"+ respuesta.Cliente.Fijo[componente] +"</td>";

            for(var i = 1; i <= 4; ++i){ 
                var Oferta = "Oferta"+i;
                if(respuesta.Cliente.Fijo.MovistarTotal) {
                    if(i === 4) {
                        html += "<td class='separacion_seleccionada'>" + respuesta[Oferta].Fijo[componente] +"</td>";
                    } else {
                        html += "<td class='separacion'>" + respuesta[Oferta].Fijo[componente] +"</td>";
                    }
                } else {
                    html += "<td>" + respuesta[Oferta].Fijo[componente] +"</td>";
                }
            }
        } else {
            html += "<th class='etiqueta'>" + componenteNombre + "</th>" +
            "<td>"+ respuesta.Cliente.Fijo[componente] +"</td>";

            for(var i = 1; i <= 4; ++i){ 
                var Oferta = "Oferta"+i;
                html += "<td>" + respuesta[Oferta].Fijo[componente] +"</td>";
            }
        }
    }else{
        if(!respuesta.Cliente.Fijo.MovistarTotal) {
            html += "<th class='etiqueta' style='border-bottom: 1px solid rgb(0,176,240);'> Renta Servicio Fijo</th>" +
            "<td class='separacion' >"+ respuesta.Cliente.Fijo[componente] +"</td>";

            for(var i = 1; i <= 4; ++i){ 
                if(i === 4){
                    html += "<td class='separacion_seleccionada'></td>";
                }else{
                    html += "<td class='separacion'></td>";
                }
            }
        } else {
            return "";
        }
    }
    html += "</tr>";
    
    return html;
};

Identificadores.ImprimirOfertaComponenteMovil = function(respuesta,id,componente){
    
    var componenteNombre = componente;
    var MovilCliente = respuesta.Cliente.Movil1;
    if(id === 2){
        MovilCliente = respuesta.Cliente.Movil2;
    }
    var html = "<tr>";
    
    if(componente === 'AppsDescripcion'){
        componenteNombre = 'Apps Ilimitadas';
    } 
    if(componente === 'DatosInternacionales'){
        componenteNombre = 'Datos Internacionales';
    } 
    if(componente === 'WhatsappIlimitado'){
        componenteNombre = 'Whatsapp Ilimitado Internacional';
    } 

    
    if(componente === 'Minutos'){
        var cantRowSpan = 8;
        if(MovilCliente.MovistarTotal) {
            cantRowSpan = 7; // pasa 1
        }
        html += "<td rowspan='" + cantRowSpan + "' class='titulo_seccion'>" +
            "<div>Movil #" + id + "</div>" +
            "<div> " + MovilCliente.Numero + "</div>" +
            "<div> " + MovilCliente.Categoria + "</div>" +
            "<div> " + MovilCliente.Titular + "</div>" +
        "</td>" ;
    }
    
    var BonoDuplica = '';
    var Bono4GigasIlimitados = '';

    var style = "";

    if(componente === 'Equipamiento' && MovilCliente.MovistarTotal) {
        style = " style='border-bottom: 1px solid rgb(0,176,240);'";
    }   

    if(respuesta.Cliente.Fijo.MovistarTotal 
   && respuesta.Cliente.Movil1.MovistarTotal
   && !respuesta.Cliente.Movil2.MovistarTotal
   && id === 2 ){
        if(componente === 'Equipamiento' && MovilCliente.MovistarTotal) {
            style = " style='color:#00a9e0;border-bottom: 1px solid rgb(0,176,240);'";
        } else {
            style = " style='color:#00a9e0;'";
        }
    }
    
    if(componente !== 'Renta'){
        var styleEquipo = '';
        var classEquipo = '';
        var classEquipoTD = '';
        if(componente === 'Equipamiento' && MovilCliente.MovistarTotal) {
            styleEquipo = "style='border-bottom: 1px solid rgb(0,176,240);'";
            classEquipo = "class='separacion'";
        }

        html += "<th " + styleEquipo + " class='etiqueta_datos etiqueta'>" + componenteNombre + "</th>";
        if(componente === 'Equipamiento' && Documento.EsMovistarTotal && 
          ((id === 1 && respuesta.Cliente.Movil1.MovistarTotal) || 
           (id === 2 && respuesta.Cliente.Movil2.MovistarTotal))) {
                Movil = "Movil"+(id-1);
                GSV = Identificadores.ObtenerGVS(window.respuesta.Resultado.Cliente,Movil);
                opcionSeleccionar = "<div id=\'Equipo_Texto0" + id + "\' onclick=\"Equipo.Mostrar(window.respuesta.Cliente,window.respuesta.Cliente,\'" + GSV + "\',\'0\',\'" + id + "\');\" class=\'Equipamiento\'>Seleccionar</div>";
                html += "<td " + style + ">" + opcionSeleccionar  + "</td>";
        } else {
            html += "<td " + style + ">" + MovilCliente[componente] + "</td>";
        }
        
        for(var i = 1; i <= 4; ++i){ 
            var Oferta = "Oferta"+i;

            if(componente === 'Equipamiento' && MovilCliente.MovistarTotal) {
                if(i == 4) { 
                    classEquipoTD = "class='separacion_seleccionada'";
                } else {
                    classEquipoTD = "class='separacion'";
                }
            }

            if(id === 1){
                if (componente === 'Datos') {     // r 2               
                    if(respuesta[Oferta].Movil1[componente] === '-'){
                        Bono4GigasIlimitados = '';
                    }else{                        
                        Bono4GigasIlimitados=' + <b>4G ILIMITADO x3meses</b>';                       
                    } 
                }
                
                if(componente === 'Datos' 
                && ( MovilCliente.Titular === 'Portabilidad' )){  
                        if(respuesta[Oferta].Movil1[componente] === '-') {
                            BonoDuplica = '';
                        } else {
                            if(respuesta[Oferta].RentaSinAdicionales < 300) {
                                BonoDuplica = ' + <b>Bono Duplica x 3 Meses</b>';
                            } else {
                                BonoDuplica = ' + <b>Bono Duplica x 6 Meses</b>';
                            }      
                        } 
                }

                html += "<td " + classEquipoTD + ">" + respuesta[Oferta].Movil1[componente]  + Bono4GigasIlimitados  +"</td>";
            }else{
                if (componente === 'Datos') {  // r 3                  
                    if(respuesta[Oferta].Movil2[componente] === '-'){
                        Bono4GigasIlimitados = '';
                    }else{                        
                        Bono4GigasIlimitados=' + <b>4G ILIMITADO x3meses</b>';                       
                    } 
                }

                if(componente === 'Datos' 
                && ( MovilCliente.Titular === 'Portabilidad' )){
            
                        if(respuesta[Oferta].Movil2[componente] === '-'){
                            BonoDuplica = '';
                        }else{
                            if(respuesta[Oferta].RentaSinAdicionales < 300){
                                BonoDuplica = ' + <b>Bono Duplica x 3 Meses</b>';
                            }else{
                                BonoDuplica = ' + <b>Bono Duplica x 6 Meses</b>';
                            }      
                        } 
                }

                html += "<td " + classEquipoTD + ">" + respuesta[Oferta].Movil2[componente] + Bono4GigasIlimitados + "</td>";
            }
        }
    }else{
        if(MovilCliente.MovistarTotal) {
            return '';
        }

        html += "<th class='etiqueta' style='border-bottom: 1px solid rgb(0,176,240);'> Renta Servicio Movil </th>" +
       "<td class='separacion' " + style + ">" + MovilCliente[componente] + "</td>";

        for(var i = 1; i <= 4; ++i){ 
            if(i === 4){
                html += "<td id='Equipo_Renta"+i+id+"' class='separacion_seleccionada'></td>";
            }else{
                html += "<td id='Equipo_Renta"+i+id+"' class='separacion'></td>";
            }
        }
    }
    html += "</tr>";
    
    return html;
};

Identificadores.ObtenerGVS = function(cliente,componente) {
    var movil = null;
    if(componente == 'Movil0') {
        if(!Identificadores.isset(cliente.Componentes.Movil0)){
            return 'ALTA';
        }
        movil = cliente.Componentes.Movil0;
    } else {
        if(!Identificadores.isset(cliente.Componentes.Movil1)){
            return 'ALTA';
        }
        movil = cliente.Componentes.Movil1;
    }
    
    if(movil.Producto === 'Competencia' && movil.MayorIgual8Meses == true){
        return 'PORTA_ESPECIAL'  ;
    } else if(movil.Producto === 'Competencia'){
      return 'PORTA_GENERICA';
    } else {
        gvs = movil.GVS;
        validos = ['POS1','POS2','POS3'];

        for (var nCant = 0; nCant < validos.length; nCant++) {
            var elemento = validos[nCant];

            if(elemento.indexOf(gvs) >= 0){
                return gvs;
            }
        }
        
        prepago = ['PRE1','PRE2','PRE3'];
        
        for (var nCant = 0; nCant < prepago.length; nCant++) {
            var elemento = prepago[nCant];

            if(elemento.indexOf(gvs) >= 0){
                return 'POS2';
            }
        }

        return 'POS1';
    }
};

Identificadores.isset = function(variable){
    if(typeof(variable) != "undefined") {  
        return true;
    }else{
        return false;
    }
};

Identificadores.ImprimirOfertaRentas = function(respuesta){
    var ActivarDiasMovistar=true;
    var f1 = new Date();
    var f2 = new Date(2019,9,31); // El mes empieza de 0 a 11 , RECORDAR
    var anioActual=f1.getFullYear();
    var mesActual=f1.getMonth();
    var diaActual=f1.getDate();
    var anioPromo=f2.getFullYear();
    var mesPromo=f2.getMonth();
    var diaPromo=f2.getDate();
    
    var fechaActual=anioActual+mesActual+diaActual;
    var fechaFinPromo=anioPromo+mesPromo+diaPromo;
   
    if(mesActual>10){     
        ActivarDiasMovistar=false;
        //console.log('TERMINO LA PROMOCION');
      }else{
        ActivarDiasMovistar=true;
        //console.log('CONTINUA LA PROMOCION');
      }
      
      ActivarDiasMovistar=true;

    var MovistarTotal = false;
    if(respuesta.Cliente.Fijo.MovistarTotal && respuesta.Cliente.Movil1.MovistarTotal){
        MovistarTotal = true;
    }
    
    var html = '<tr><th class="RentalTotal"><div>Renta Total</div></th>'; 
    
        html += '<td>';
       
        if(MovistarTotal){
            html += "<div  class='RentalTotal'>Movistar Total</div><div  class='RentalTotal'>Movil Adicional</div>";
        }else{
            html += "<div  class='RentalTotal'>Renta Servicios</div>";
        }      
        html += "<div class='RentalTotal'>Servicios Adicionales</div><div class='RentalTotal'>Financiamiento Movil</div><div  class='RentalTotal'>Total</div><div  class='RentalTotal'>Salto</div>";
        if(ActivarDiasMovistar){
            html += "<div class='RentalTotal'>Dscto Dias Movistar</div>";
        }
        html += "</td>";
        html += "<td>";
       if(MovistarTotal){
            var RentaMovil = respuesta.Cliente.Movil2.Renta ;

            if(RentaMovil === '-' 
            || (respuesta.Cliente.Fijo.MovistarTotal    
            && respuesta.Cliente.Movil1.MovistarTotal
            && respuesta.Cliente.Movil2.MovistarTotal )){
                RentaMovil = 0;
            }
    
            html += "<div  class='RentalTotal'>" + parseFloat(respuesta.Cliente.PrincipalTotal - RentaMovil).toFixed(0) + "</div>" +
            "<div  class='RentalTotal'>" + RentaMovil + "</div>";
       }else{
            html += "<div  class='RentalTotal'>" + respuesta.Cliente.PrincipalTotal + "</div>";
       }
       
        html += "<div class='RentalTotal'>" + respuesta.Cliente.RentaAdicional + "</div>" +
       "<div class='RentalTotal' id='RentaTotFinan0'>" + respuesta.Cliente.FinanciamientoMovil + "</div>" +
       "<div class='RentalTotal' id='RentaTotal0'>" + respuesta.Cliente.RentaTotal + "</div>" +
       "<div class='RentalTotal' id='RentaTotSalto0'><span> - </span></div>";

       if(ActivarDiasMovistar){
           html +="<div class='RentalTotal' id='RentaTotSalto0'><span> - </span></div>" ; // ActivarDiasMovistar
       }

       html += "</td>";

        for(var i = 1; i <= 4; ++i){
            var Oferta = "Oferta"+i;
                    
            html += "<td class='RentalTotal'>" ;
            html += "<div id='RentaTotSinAdic"+i+"'>" +  respuesta[Oferta].RentaSinAdicionales + "</div>";

            if(MovistarTotal){
                html += "<div><span>0</span></div>";
            }
                
            html += "<div id='RentaTotAdic"+i+"'>" + respuesta[Oferta].Adicionales + "</div>" +
                "<div id='RentaTotFinan"+i+"'>" + respuesta[Oferta].FinanciamientoMovil + "</div>" +
                "<div id='RentaTotal"+i+"'>" + respuesta[Oferta].RentaTotal + "</div>" +
                "<div id='RentaTotSalto"+i+"'>" + respuesta[Oferta].Salto + "</div>";

                if( ActivarDiasMovistar==true /* && respuesta[Oferta].RentaSinAdicionales>199 */ && respuesta[Oferta].Salto>=0 ){
                html += "<div id='RentaTotSalto"+i+"'>" +" - "+ "</div>" ;
            } else{
                if(ActivarDiasMovistar){
                    html += "<div id='RentaTotSalto"+i+"'><span> - </span></div>" ; // ActivarDiasMovistar
                }   
            }         
                            
            html +="</td>";
        }
    
        html += '</tr>'; 
    return html;
};

Identificadores.ImprimirOfertaBotones = function(respuesta){
    var html = "<tr><td rowspan='4'></td><td></td>";
    if(Documento.EsMovistarTotal) {
        html += "<td><div onclick='Identificadores.MostrarInformacionRegistro(window.respuesta.Cliente,0);' class='btnGuardar'>Registro</div></td>";
    } else {
        html += "<td></td>";
    }
    for(var i = 1; i <= 4; ++i){
        var Oferta = "Oferta"+i;
        html += "<td>" + respuesta[Oferta].Registro + "</td>"; 
    }
    html += '</tr>'; 
   
    if(respuesta.Usuario.Canal === 'TIENDAS' ||  respuesta.Usuario.Canal === 'ADMIN'){
        html += "<tr><td rowspan='4'></td><td></td>";    

        for(var i = 1; i <= 4; ++i){
            var Oferta = "Oferta"+i;
            html += "<td>" + respuesta[Oferta].Guardar + "</td>"; 
        }   
        html += '</tr>';     
    }
    return html;
};

Identificadores.MostrarInformacionRegistro = function(Cliente,Oferta){
    var contenido = "<h1>Oferta</h1>";
    var ActivarDiasMovistar=true;
    var f1 = new Date();
    var f2 = new Date(2019,9,31); // El mes empieza de 0 a 11 , RECORDAR
    var anioActual=f1.getFullYear();
    var mesActual=f1.getMonth();
    var diaActual=f1.getDate();
    var anioPromo=f2.getFullYear();
    var mesPromo=f2.getMonth();
    var diaPromo=f2.getDate();
    
    var fechaActual=anioActual+mesActual+diaActual;
    var fechaFinPromo=anioPromo+mesPromo+diaPromo;
   
    if(mesActual>10){     
        ActivarDiasMovistar=false;
        //console.log('TERMINO LA PROMOCION');
      }else{
        ActivarDiasMovistar=true;
        //console.log('CONTINUA LA PROMOCION');
      }
      ActivarDiasMovistar=true;

    contenido += "<table class='Parrilla' style='border-spacing: 0px 2px; width: 100%'>";
    
    contenido += "<tr><td rowspan='10' class='titulo_seccion' style='width: 30px;'><div>Datos del cliente Titular</div></td>" +
   "<th class='etiqueta'> Nombre Completo</th><td><span>" + Cliente.Nombre + "</span></td></tr>";  
    
    contenido += "<tr><th class='etiqueta'> Documento </th><td><span>"+ Cliente.Documento +"</span></td></tr>" +
        "<tr><th class='etiqueta'> Numero Fijo </th><td><span>"+ Cliente.Fijo.Numero +"</span></td></tr>" +
        "<tr><th class='etiqueta'> Numero Movil </th><td><span>"+ Cliente.Moviles +"</span></td></tr>" +
        "<tr><th class='etiqueta'> Ciclo Facturacion Fija </th><td><span>"+ Cliente.Fijo.Ciclo +"</span></td></tr>" +
        "<tr><th class='etiqueta'> Ciclo Facturacion Movil </th><td><span>"+ Cliente.CicloMovil +"</span></td></tr>" +
        "<tr><th class='etiqueta'> Correo Electronico </th><td><span>"+ Cliente.Correo +"</span></td></tr>" +
        "<tr><th class='etiqueta'> Cobertura (X,Y) </th><td><span>"+ Cliente.Fijo.Cobertura +"</span></td></tr>" +
        "<tr><th class='etiqueta'> Direccion de Instalacion </th><td><span>"+ Cliente.Fijo.Direccion +"</span></td></tr>" +
        "<tr><th class='etiqueta separacion'> Distrito / Provincia / Departamento </th><td class='separacion'><span>"+ Cliente.Fijo.Ubicacion +"</span></td></tr>";  
   
    if(Oferta != 0) {
        var OfertaDiasMovistar = Oferta.DiasMovistar == undefined ? '' : Oferta.DiasMovistar;

         contenido += "<tr><td rowspan='13' class='titulo_seccion' style='border: none; width: 30px;'><div>Oferta</div></td>" +
            "<th class='etiqueta'>Oferta Movistar Total</th><td><span>"+ Oferta.Nombre + "</span></td></tr>"; 

        contenido += "<tr><th class='etiqueta'> Plan Movil </th><td><span>"+ Oferta.Plan +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Scoring Fijo </th><td><span>"+ Oferta.scoringFijoCodigo +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Scoring Movil </th><td><span>"+ Oferta.scoringMovilCodigo +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Equipamiento Fijo </th><td><span>"+ Oferta.Equipamiento +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Equipamiento Movil#1 </th><td><span>"+ Oferta.Movil1.Equipo +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Equipamiento Movil#2 </th><td><span>"+ Oferta.Movil2.Equipo +"</span></td></tr>" ;

        if(OfertaDiasMovistar!="" && ActivarDiasMovistar==true){ 
            contenido +=" <tr><th class='etiqueta'> Dscto Dias Movistar </th><td><span>"+ OfertaDiasMovistar  +"</span></td></tr>";
        }

        contenido += "<tr><th class='etiqueta'> Renta Total </th><td><span>"+ Oferta.RentaTotal +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Salto </th><td><span>"+ Oferta.Salto +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Operación comercial Fija </th><td><span>"+ Oferta.Fijo.OperacionComercial +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Operación comercial Móvil#1 </th><td><span>"+ Oferta.Movil1.OperacionComercial +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Operación comercial Móvil#2 </th><td><span>"+ Oferta.Movil2.OperacionComercial +"</span></td></tr>" ;
    } else {
        Oferta = window.respuesta.Cliente;
        
        var Movil1OC = Oferta.Movil1.OperacionComercial == undefined ? 'Mantiene' : Oferta.Movil1.OperacionComercial;
        var Movil2OC = Oferta.Movil2.OperacionComercial == undefined ? 'Mantiene' : Oferta.Movil2.OperacionComercial;
        var Movil1Equipo = Oferta.Movil1.Equipo == undefined ? '-' : Oferta.Movil1.Equipo;
        var Movil2Equipo = Oferta.Movil2.Equipo == undefined ? '-' : Oferta.Movil2.Equipo;
        var OfertaSalto = Oferta.Salto == undefined ? '0' : Oferta.Salto;
        var OfertaDiasMovistar = Oferta.DiasMovistar == undefined ? '' : Oferta.DiasMovistar;

        contenido += "<tr><td rowspan='12' class='titulo_seccion' style='border: none; width: 30px;'><div>Oferta</div></td>" +
            "<th class='etiqueta'>Oferta Movistar Total</th><td><span>"+ Oferta.Paquete.Nombre + "</span></td></tr>"; 

        contenido += "<tr><th class='etiqueta'> Plan Movil </th><td><span>"+ Oferta.Paquete.CodigoMovil + " - " + Oferta.Paquete.NombreMovil +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Scoring Fijo </th><td><span>"+ Oferta.Scoring.scoringFijoCodigo +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Scoring Movil </th><td><span>"+ Oferta.Scoring.scoringMovilCodigo +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Equipamiento Fijo </th><td><span>Mantiene</span></td></tr>" +
            "<tr><th class='etiqueta'> Equipamiento Movil#1 </th><td><span>"+ Movil1Equipo +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Equipamiento Movil#2 </th><td><span>"+ Movil2Equipo +"</span></td></tr>" ;
            if(OfertaDiasMovistar!="" && ActivarDiasMovistar==true){
                contenido +=" <tr><th class='etiqueta'> Dscto Dias Movistar </th><td><span>"+ OfertaDiasMovistar  +"</span></td></tr>";
            }
            contenido +="<tr><th class='etiqueta'> Renta Total </th><td><span>"+ Oferta.RentaTotal +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Salto </th><td><span>"+ OfertaSalto +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Operación comercial Fija </th><td><span>Mantiene</span></td></tr>" +
            "<tr><th class='etiqueta'> Operación comercial Móvil#1 </th><td><span>"+ Movil1OC +"</span></td></tr>" +
            "<tr><th class='etiqueta'> Operación comercial Móvil#2 </th><td><span>"+ Movil2OC +"</span></td></tr>" ;
    }
    contenido += "</table>";
    Util.MostrarModal("",contenido);
};

Identificadores.ImprimirParrilla = function(respuesta){
    
    var html = "<table class='Parrilla'>";
    
    html += Identificadores.ImprimirOfertaCabecera();
    
    var ComponenteFijo = ["Linea", "Velocidad", "Internet", "Modem", "Television", "Decodificador", "Bloque", "Adicional", "Renta"];
    
    for (i = 0; i < ComponenteFijo.length; i++) {
        html += Identificadores.ImprimirOfertaComponenteFijo(respuesta,ComponenteFijo[i]);
    }
    
    var ComponenteMovil = ["Minutos","Datos","AppsDescripcion","WhatsappIlimitado","Pasagigas","DatosInternacionales","Equipamiento","Renta"]; // pasa 2
    
    
    for (j = 1; j <= 2; j++) {
        for (i = 0; i < ComponenteMovil.length; i++) {
            html += Identificadores.ImprimirOfertaComponenteMovil(respuesta,j,ComponenteMovil[i]);
        }
    }
    
    html += Identificadores.ImprimirOfertaRentas(respuesta);
    
    html += Identificadores.ImprimirOfertaBotones(respuesta);
    
    html += "</table>";
    
    return html;    
};

Identificadores.ImprimirOferta = function(respuesta){

    var html = "";
    
    html += "<fieldset><legend>Ofertas Movistar Total</legend><center>";
    
    html += Identificadores.ImprimirSeleccionarOferta();
    html += Identificadores.ImprimirParrilla(respuesta);
    
    html += "</center></fieldset>";
    
    
    $('#Parrilla_MT').html(html);
    $('#Parrilla_MT').show();
};

Identificadores.RespuestaConsulta = function(respuesta){
    window.respuesta = respuesta;
    
    if(respuesta.CodigoConsulta !== undefined){
        
        var Codigo = 'Codigo de Consulta: ';
        Codigo += respuesta.CodigoConsulta;
        
        $('#CodigoUltimaConsulta').text(Codigo);
        
        
    }
    Identificadores.ImprimirOferta(respuesta);       
    Identificadores.LlenarProducto();

    //if(respuesta.Oferta1.Plan === '-' && respuesta.Oferta2.Plan === '-' && respuesta.Oferta3.Plan === '-' ) {
    if(respuesta.Resultado.Ofertas.length == 0) {
       
      /*   $('#scoringconv').val('RECHAZAR'); 
        $('#scoringconv_fijo').val('RECHAZAR'); 
        $('#scoringconv_movil').val('RECHAZAR'); 
        $('#scoringfijo').val('RECHAZAR'); 
        $('#scoringmovil').val('RECHAZAR'); */

        var htm='';
        
        if(respuesta.Cliente.Scoring.LimiteCrediticio<=62){                         
            htm+='<span style="color:red;font-weight:bold">  LÍMITE DE CRÉDITO INSUFICIENTE</span>';
           
        }else{                     
            htm+='<span style="color:red;font-weight:bold">   CLIENTE NO APLICA A OFERTAS MT</span>';
        }   
        
        $('#AvisoLimite').html(htm);      
        $('#AvisoLimite').show();
        setTimeout ("alert('Cliente no aplica a Ofertas MT');", 1000);     
        
    }else{
        if(Documento.EsMovistarTotal) {
            Util.ConsultaAjax('Http/ConsultarClienteCatalogoProductos.php',
            {Paquete_CodigoMovil: window.respuesta.Cliente.Movil1.IdPlan}
            ,function(Resultado) {
                window.respuesta.Cliente.Paquete = Resultado;
            });
        }

        $('#Display_Ofertas').show();
    }        
    $('#oferta'+i+'_fijo_rentatotal').text("-");

};



Identificadores.ConsultarScoringFijo = function(){
    if(Identificadores.telefono_notengo){
        $('#scoringfijo').val('Consultando'); 
        return Util.ConsultaAjaxSinResultado('Http/ConsultarScoringFijo.php',Identificadores.DataScoringFijo());
    } else {
        $('#scoringfijo').val('APROBAR');
        $('#scoringfijo_detalle').val('Cliente actual');
    }
};

Identificadores.DataScoringFijo = function(){
    
    var i = Identificadores.departamento;
    var j = Identificadores.provincia;
    var k = Identificadores.distrito;
    
    
    Identificadores.departamento_texto = departamentos[i].name;
    Identificadores.provincia_texto  = departamentos[i].provincias[j].name;
    Identificadores.distrito_texto  = departamentos[i].provincias[j].distritos[k].name;
    var ubigeo = departamentos[i].provincias[j].distritos[k].ubigeo;
    
    return {
        departamento: Identificadores.departamento_texto,
        provincia: Identificadores.provincia_texto,
        distrito: Identificadores.distrito_texto,
        ubigeo: ubigeo,
        tipoDocumento : Documento.tipoDocumento,
        documento: Documento.documento
    };
    
};


/*Cambio 008*/
Identificadores.ConsultarScoringMovil1 = function(){
    $('#scoringmovil').val('Consultando');
	
	var data = {};
	data.tipDocumento = Documento.tipoDocumento;
    data.numDocumento = Documento.documento;
    data.sisOrigen = "APPCalculadoraMT";
    data.modDecisional= "MV";
    data.tipMoneda= "1";
    data.tlfPortado= "";
    data.idTipSustento= "";
    data.portabilidad= "";
    data.operador= "";
    data.nombreOperador= "";
    data.fechaAlta= "";
    data.codigoSegmento= "";
    data.tipoAccion= 'CH';
    data.msisdn= "";
    data.sumaCargoRecurrente= 0;
    
    data.tipoAccion = 'CH';
    if(Identificadores.movil1_notengo){
        data.tipoAccion = 'PR';
    } else {
		data.msisdn = $('#movil1').val();
		data.sumaCargoRecurrente = Math.round($('#cliente_movil1_renta').val()/1.18,2);
	}
	
	if(Identificadores.RequiereConsultaABD1){
		data.tlfPortado= data.msisdn;
		data.portabilidad = 1;
		data.tipoAccion = 'PR';
		data.operador = $('#movil1_operador').val();
                
                var NombreOperador = '';
                
                if($('#movil1_operador').val() === '20'){
                    NombreOperador = 'Entel';
                }
                if($('#movil1_operador').val() === '21'){
                    NombreOperador = 'Claro';
                }
                
		data.nombreOperador = NombreOperador;
		data.codigoSegmento = $('#movil1_tipo').val();
                
                var fecha1 = moment($('#movil1_fechaActivacion').val(), 'YYYY-MM-dd');
                fecha1 = fecha1.format('YYYYMMDD');
                
		data.fechaAlta = fecha1;
		data.sumaCargoRecurrente = 0;
	}
	
	var xml = 
'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:head="http://telefonica.com/globalIntegration/header" xmlns:ns="http://integracion.osb.telefonica.com/services/64003">'+
'<soapenv:Header>'+
'<head:HeaderIn>'+
'<head:country>PE</head:country>'+
'<head:lang>es</head:lang>'+
'<head:entity>TDP</head:entity>'+
'<head:system>APPCalculadoraMT</head:system>'+
'<head:subsystem>APPCalculadoraMT</head:subsystem>'+
'<head:originator>PE:TDP:APPCalculadoraMT:APPCalculadoraMT</head:originator>'+
'<head:sender>BEAPPCalculadoraMT</head:sender>'+
'<head:userId>APPCalculadoraMT</head:userId>'+
'<head:operation>RespuestaDeEnvio</head:operation>'+
'<head:destination>PE:CMS:CMS:CMS</head:destination>'+
'<head:execId>a2aa1107-e75c-44ed-90fa-2f96faf01e86</head:execId>'+
'<head:timestamp>2017-01-05T17:52:16.547-05:00</head:timestamp>'+
'<head:msgType>REQUEST</head:msgType>'+
'</head:HeaderIn>'+
'</soapenv:Header>'+
'<soapenv:Body>'+
'<ns:obtenerScoreCrediticioRequest>'+
'<tipDocumento>'+data.tipDocumento+'</tipDocumento>'+
'<numDocumento>'+data.numDocumento+'</numDocumento>'+
'<sisOrigen>APPCalculadoraMT</sisOrigen>'+
'<modDecisional>MV</modDecisional>'+
'<tlfPortado>'+data.tlfPortado+'</tlfPortado>' +
'<tipMoneda>1</tipMoneda>'+
'<portabilidad>'+data.portabilidad+'</portabilidad>' +
'<operador>'+data.operador+'</operador>' +
'<nombreOperador>'+data.nombreOperador+'</nombreOperador>' +
'<fechaAlta>'+data.fechaAlta+'</fechaAlta>' +
'<codigoSegmento>'+data.codigoSegmento+'</codigoSegmento>' +
'<tipoAccion>'+data.tipoAccion+'</tipoAccion>' +
'<msisdn>'+data.msisdn+'</msisdn>' +
'<sumaCargoRecurrente>'+data.sumaCargoRecurrente+'</sumaCargoRecurrente>' +
'</ns:obtenerScoreCrediticioRequest>' +
'</soapenv:Body>'+
'</soapenv:Envelope>	';

window.scoring1Cargo = data.sumaCargoRecurrente;

scoringMovil1Request=xml;

return $.ajax({
	url: '/bus/INT/OSB/SRV/WSCustomerInformation?wsdl',
	data : xml,
	type : 'POST',
	contentType: "application/xml",
	dataType: "text",
	timeout : 6000,
	async : true
});


    
};

/*Cambio 007*/
Identificadores.ConsultarScoringMovil2 = function(){
	 $('#scoringmovil').val('Consultando');
	
	var data = {};
	data.tipDocumento = Documento.tipoDocumento;
    data.numDocumento = Documento.documento;
    data.sisOrigen = "APPCalculadoraMT";
    data.modDecisional= "MV";
    data.tipMoneda= "1";
    data.tlfPortado= "";
    data.idTipSustento= "";
    data.portabilidad= "";
    data.operador= "";
    data.nombreOperador= "";
    data.fechaAlta= "";
    data.codigoSegmento= "";
    data.tipoAccion= 'CH';
    data.msisdn= "";
    data.sumaCargoRecurrente= 0;
    
    data.tipoAccion = 'CH';
    if(Identificadores.movil2_notengo){
        data.tipoAccion = 'PR';
    } else {
		data.msisdn = $('#movil2').val();
		data.sumaCargoRecurrente = Math.round($('#cliente_movil2_renta').val()/1.18,2);
	}
	
	if(Identificadores.RequiereConsultaABD2){
		data.tlfPortado= data.msisdn;
		data.portabilidad = 1;
		data.tipoAccion = 'PR';
		data.operador = $('#movil2_operador').val();
                
                var NombreOperador = '';
                
                if($('#movil2_operador').val() === '20'){
                    NombreOperador = 'Entel';
                }
                if($('#movil2_operador').val() === '21'){
                    NombreOperador = 'Claro';
                }
                
		data.nombreOperador = NombreOperador;
                
		data.codigoSegmento = $('#movil2_tipo').val();

                var fecha2 = moment($('#movil2_fechaActivacion').val(), 'YYYY-MM-dd');
                fecha2 = fecha2.format('YYYYMMDD');
                
		data.fechaAlta = fecha2;

		data.sumaCargoRecurrente = 0;
	}
	
	var xml = 
'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:head="http://telefonica.com/globalIntegration/header" xmlns:ns="http://integracion.osb.telefonica.com/services/64003">'+
'<soapenv:Header>'+
'<head:HeaderIn>'+
'<head:country>PE</head:country>'+
'<head:lang>es</head:lang>'+
'<head:entity>TDP</head:entity>'+
'<head:system>APPCalculadoraMT</head:system>'+
'<head:subsystem>APPCalculadoraMT</head:subsystem>'+
'<head:originator>PE:TDP:APPCalculadoraMT:APPCalculadoraMT</head:originator>'+
'<head:sender>BEAPPCalculadoraMT</head:sender>'+
'<head:userId>APPCalculadoraMT</head:userId>'+
'<head:operation>RespuestaDeEnvio</head:operation>'+
'<head:destination>PE:CMS:CMS:CMS</head:destination>'+
'<head:execId>a2aa1107-e75c-44ed-90fa-2f96faf01e86</head:execId>'+
'<head:timestamp>2017-01-05T17:52:16.547-05:00</head:timestamp>'+
'<head:msgType>REQUEST</head:msgType>'+
'</head:HeaderIn>'+
'</soapenv:Header>'+
'<soapenv:Body>'+
'<ns:obtenerScoreCrediticioRequest>'+
'<tipDocumento>'+data.tipDocumento+'</tipDocumento>'+
'<numDocumento>'+data.numDocumento+'</numDocumento>'+
'<sisOrigen>APPCalculadoraMT</sisOrigen>'+
'<modDecisional>MV</modDecisional>'+
'<tlfPortado>'+data.tlfPortado+'</tlfPortado>' +
'<tipMoneda>1</tipMoneda>'+
'<portabilidad>'+data.portabilidad+'</portabilidad>' +
'<operador>'+data.operador+'</operador>' +
'<nombreOperador>'+data.nombreOperador+'</nombreOperador>' +
'<fechaAlta>'+data.fechaAlta+'</fechaAlta>' +
'<codigoSegmento>'+data.codigoSegmento+'</codigoSegmento>' +
'<tipoAccion>'+data.tipoAccion+'</tipoAccion>' +
'<msisdn>'+data.msisdn+'</msisdn>' +
'<sumaCargoRecurrente>'+data.sumaCargoRecurrente+'</sumaCargoRecurrente>' +
'</ns:obtenerScoreCrediticioRequest>' +
'</soapenv:Body>'+
'</soapenv:Envelope>	';

window.scoring2Cargo = data.sumaCargoRecurrente;

if(xml==scoringMovil1Request) {
    return scoringMovil1Response;
}

return $.ajax({
	url: '/bus/INT/OSB/SRV/WSCustomerInformation?wsdl',
	data : xml,
	type : 'POST',
	contentType: "application/xml",
	dataType: "text",
	timeout : 6000,
	async : true
});	
};


Identificadores.LimpiarScoring = function(){
    $('.datosScoring').val('');
};

Identificadores.MostrarScoringMovil = function(ResultadoConsulta){
    $('#scoringmovil').val(ResultadoConsulta.accion);
    $('#scoringmovil_score').val(ResultadoConsulta.Score);
    $('#scoringmovil_codigo').val(ResultadoConsulta.idConScoring);
    
    
    $('#scoringmovil_maxlinea').val(ResultadoConsulta.canLineas);
    $('#scoringmovil_limite').val(ResultadoConsulta.limCredito);
    
    
    $('#scoringmovil1_detalle').val(ResultadoConsulta.restricciones1);
    $('#scoringmovil1_mensaje').val(ResultadoConsulta.menScore1);
    
    

    $('#scoringmovil2_detalle').val(ResultadoConsulta.restricciones2);
    $('#scoringmovil2_mensaje').val(ResultadoConsulta.menScore2);
    
    
    
    if(ResultadoConsulta.restricciones1.indexOf('PORTA ESPECIAL') > -1){
    
        
        var fecha1 = moment($('#movil1_fechaActivacion').val(), 'YYYY-MM-dd');
        fecha1 = fecha1.format('MMMM D, YYYY');
        
        var MesesAntiguedad1 = moment().diff(fecha1, 'months');
       
       if(ResultadoConsulta.restricciones1.indexOf('PORTA ESPECIAL FINANCIADO Y CONTADO') > -1){
        $('#scoringmovil1_restriccion').val('Cliente aplica a precio especial en venta financiado y contado por tener antigüedad de 12 meses a mas');   
        $('#scoringmovil1_tipoFinanciamiento').val('1');
        }else{
         $('#scoringmovil1_restriccion').val('Cliente aplica a precio especial solo en venta contado por tener antigüedad menor a 12 meses. RECUERDA : SI FINANCIA TENDRA EL PRECIO DE LA GENERICA');  
        $('#scoringmovil1_tipoFinanciamiento').val('2');
        }
    
    }else{
        if(ResultadoConsulta.restricciones1.indexOf('POS 1 Especial') > -1){
            $('#scoringmovil1_restriccion').val('POS 1 Especial: Equipo con descuento.');
            $('#scoringmovil1_tipoFinanciamiento').val('3');
            
        }else{
            $('#scoringmovil1_restriccion').val('Cliente aplica a precio GENERICO');
            $('#scoringmovil1_tipoFinanciamiento').val('4');
        }
    }
    
    
    if(ResultadoConsulta.restricciones2.indexOf('PORTA ESPECIAL') > -1){
        
        var fecha2 = moment($('#movil2_fechaActivacion').val(), 'YYYY-MM-dd');
            fecha2 = fecha2.format('MMMM D, YYYY');
        
        var MesesAntiguedad = moment().diff(fecha2, 'months');
       
       if(ResultadoConsulta.restricciones2.indexOf('PORTA ESPECIAL FINANCIADO Y CONTADO') > -1){
        $('#scoringmovil2_restriccion').val('Cliente aplica a precio especial en venta financiado y contado por tener antigüedad de 12 meses a mas');   
        $('#scoringmovil2_tipoFinanciamiento').val('1');
       }else{
           
         $('#scoringmovil2_restriccion').val('Cliente aplica a precio especial solo en venta contado por tener antigüedad menor a 12 meses. RECUERDA : SI FINANCIA TENDRA EL PRECIO DE LA GENERICA');  
         $('#scoringmovil2_tipoFinanciamiento').val('2');
        }
        
    }else{
        if(ResultadoConsulta.restricciones2.indexOf('POS 1 Especial') > -1){
            $('#scoringmovil2_restriccion').val('POS 1 Especial: Equipo con descuento.');
            $('#scoringmovil2_tipoFinanciamiento').val('3');
        }else{
        $('#scoringmovil2_restriccion').val('Cliente aplica a precio GENERICO');
        $('#scoringmovil2_tipoFinanciamiento').val('4');
        }
    }
    
};


Identificadores.FinGIS = function(){
    $('#gis').html('<span style="color:green;font-weight:bold">Cliente con facilidades tecnicas HFC</span>');
    $('#scoringFijo').html("<img src='loading.gif' style='height:25px'>Consultando scoring fijo");
    setTimeout(Identificadores.FinScoringFijo,3000);
};

Identificadores.FinScoringFijo = function(){
    $('#scoringFijo').html('<span style="color:green;font-weight:bold">Scoring Fijo Ok</span>');
    $('#verificacionABD').html("<img src='loading.gif' style='height:25px'>Consultando ABD");
    setTimeout(Identificadores.FinABD,3000);
};

Identificadores.FinABD = function(){
    $('#verificacionABD').html('<span style="color:green;font-weight:bold">Sin restricciones para portabilidad</span>');
    $('#scoringMovil').html("<img src='loading.gif' style='height:25px'>Consultando Scoring Movil");
    setTimeout(Identificadores.FinScoringMovil,3000);
    
};

Identificadores.FinScoringMovil = function(){
    $('#scoringMovil').html('<span style="color:green;font-weight:bold">Scoring Movil Ok</span>');
    $('#Display_Ofertas').show();
};



Identificadores.CrearData = function(){
    return {
   identificador : Identificadores.consulta,
   gestion: 'CuatroPlay',
   version : 1000
};
};



Identificadores.MostrarScoringFijo = function(ResultadoScoring){
    
    $('#scoringfijo_codigo').val(ResultadoScoring.CodConsulta);
    
    
    if(Identificadores.EncontrarTrioScoring(ResultadoScoring)){
        $('#scoringfijo').val('APROBAR');
    } else {
        $('#scoringfijo').val('RECHAZAR');
        $('#scoringfijo_detalle').val('No aplica a Trio');
    }  
};


Identificadores.EncontrarTrioScoring = function(ResultadoScoring){
  
    for(var i in ResultadoScoring.OperacionComercial){
        
        var op = ResultadoScoring.OperacionComercial[i];
        
        if(op.CodOpe === "ALTA PURA"){
   
   for(var j in op.Detalle){
       var fila = op.Detalle[j];
       if(fila.CodProd === "102" && fila.PagoAdelantado === 0){
  $('#scoringfijo_renta').val(op.Renta.substring(4));
  $('#scoringfijo_detalle').val('');
  return true;
       }
       if(fila.CodProd === "102" && fila.PagoAdelantado !== 0){
  $('#scoringfijo_renta').val(fila.PagoAdelantado);
  $('#scoringfijo_detalle').val('UP-FRONT');
  return false;
       }
   }
   
   break;
        } else {
   continue;
        }
    } 
    return 0;
    
    
};

Identificadores.MostrarHijos = function(ResultadoConsulta){
    window.ResultadoConsulta = ResultadoConsulta;
    Parrilla.Mostrar();
    Comparacion.Mostrar();
};


Identificadores.CrearConsulta = function(){
    if(Identificadores.HayDatosInvalidos()){
        console.log('Datos Invalidos');
        Identificadores.consulta = null;
    } else {
        console.log('Datos Validos');
        Identificadores.ConvertirIdentificadoresEnConsulta();
    }
};

Identificadores.ConvertirIdentificadoresEnConsulta = function(){
    Identificadores.identificador = [];
    Identificadores.AgregarIdentificador('Telefono',Identificadores.telefono);
    Identificadores.AgregarIdentificador('Movil',Identificadores.movil1);
    Identificadores.AgregarIdentificador('Movil',Identificadores.movil2);
    Identificadores.consulta = JSON.stringify(Identificadores.identificador);
};


Identificadores.HayDatosInvalidos = function(){
    return Identificadores.EsTelefonoInvalido() ||
   Identificadores.EsMovil1Invalido() ||
   Identificadores.EsMovil2Invalido() ||
   Identificadores.EsScoringInvalido();
};




Identificadores.AgregarIdentificador = function(TipoIdentificador,ValorIdentificador){
    if(ValorIdentificador !== null){
        Identificadores.identificador.push({Tipo: TipoIdentificador, Valor: ValorIdentificador});
    }
};

Identificadores.EsScoringInvalido = function(){
    return Identificadores.ScoringMovil === null || Identificadores.ScoringFijo === null;
};

Identificadores.EsTelefonoInvalido = function(){
    return Identificadores.telefono === null && !Identificadores.telefono_notengo;
};

Identificadores.EsMovil1Invalido = function(){
    return Identificadores.movil1 === null && !Identificadores.movil1_notengo;
};

Identificadores.EsMovil2Invalido = function(){
    return Identificadores.movil2 === null && !Identificadores.movil2_notengo;
};

Identificadores.EsUbicacionInvalida = function(){
    return Identificadores.departamento === null ||
   Identificadores.provincia === null || 
   Identificadores.distrito === null;
};

Identificadores.LeerUbicacionTexto = function(){
    
    Identificadores.departamento = Identificadores.LeerDepartamento();
    Identificadores.provincia = Identificadores.LeerProvincia();
    Identificadores.distrito = Identificadores.LeerDistrito();
    
    
    if(Identificadores.departamento !== null && Identificadores.provincia !== null && Identificadores.distrito !== null){
        Identificadores.departamento_texto = departamentos[Identificadores.departamento].name;
        Identificadores.provincia_texto  = departamentos[Identificadores.departamento].provincias[Identificadores.provincia].name;
        Identificadores.distrito_texto  = departamentos[Identificadores.departamento].provincias[Identificadores.provincia ].distritos[Identificadores.distrito].name;
    } else {
        Identificadores.departamento_texto = "";
        Identificadores.provincia_texto  = "";
        Identificadores.distrito_texto  = "";
    }
    
    
};

Identificadores.LeerDatosConsulta = function(){
    Identificadores.telefono = Identificadores.ValidarTelefono($('#telefono').val());
    
    if(Identificadores.telefono === null){
        Identificadores.telefono = Identificadores.ValidarMonoTV($('#telefono').val());
    }
    
    Identificadores.movil1 = Identificadores.ValidarCelular($('#movil1').val());
    Identificadores.movil2 = Identificadores.ValidarCelular($('#movil2').val());
    Identificadores.telefono_notengo = $('#telefono_notengo').prop('checked');
    Identificadores.movil1_notengo = $('#movil1_notengo').prop('checked');
    Identificadores.movil2_notengo = $('#movil2_notengo').prop('checked');
    Identificadores.cobertura = $('#cobertura').val();
    
    Identificadores.LeerUbicacionTexto();
    
    Identificadores.tipoDocumento = $('#tipoDocumento').val();
    Identificadores.direccion = $('#direccion_texto').val();

};

Identificadores.LeerDepartamento = function(){
    var i = $('#departamento').val();
    if(departamentos[i] !== undefined){
        return i;
    } else {
        return null;
    }
};


Identificadores.LeerProvincia = function(){
    
    if(Identificadores.departamento !== null){
        var j = $('#provincia').val();
        
        if(departamentos[Identificadores.departamento].provincias[j] !== undefined){
   return j;
        } else {
   return null;
        }
    } else {
        return null;
    }
};

Identificadores.LeerDistrito = function(){
    
    if(Identificadores.departamento !== null && Identificadores.provincia !== null){
        var k = $('#distrito').val();
        
        if(departamentos[Identificadores.departamento].provincias[Identificadores.provincia].distritos[k] !== undefined){
   return k;
        } else {
   return null;
        }
    } else {
        return null;
    }
    
};



Identificadores.ValidarScoring = function(Valor){
    if(Valor.length === 0){
        return null;
    } else {
        return Valor;
    }
};


Identificadores.ValidarTelefono = function(Texto) {
   if(Texto.length !== 8){
       return null;
   } else {
       return Util.ValidarNumero(Texto);
   }
};

Identificadores.ValidarMonoTV = function(Texto) {
   if(Texto.length > 8){
       return null;
   } else {
       return Util.ValidarNumeroMonoTV(Texto);
   }
};

Identificadores.ValidarCelular = function(Texto) {
   if(Texto.length !== 9){
       return null;
   } else {
       return Util.ValidarNumero(Texto);
   }
};

Identificadores.ClienteNuevo = function(){
    $('#departamento_etiqueta').show();
    $('#departamento').show();
    $('#provincia_etiqueta').show();
    $('#provincia').show();
    $('#distrito_etiqueta').show();
    $('#distrito').show();
    $('#error_telefonoFijo').hide();
    Identificadores.RequiereConsultaScoringFijo = true;
};

Identificadores.FechaAltaPostpago = 
    "<option value='"+menos1Mes+"'>Menos 1 mes</option>"+
  "<option value='"+menos4Meses+"'>Menos 4 meses</option>"+
  "<option value='"+menos8Meses+"'>Menos 8 meses</option>"+
  "<option value='"+ocho8Meses+"'>8 a mas meses</option>";

Identificadores.FechaAltaPrepago = "<option value='"+menos4Meses+"'>Menos 4 mes</option>"+
  "<option value='"+menos16Meses+"'>Menos 16 meses</option>"+
  "<option value='"+dieciseis16Meses+"'>16 a mas meses</option>";

Identificadores.AsignarFechaAlta = function (IdValor, IdFecha) {
    if ($(IdValor).val() === '02') {
        $(IdFecha).html(Identificadores.FechaAltaPostpago);
    } else {
        $(IdFecha).html(Identificadores.FechaAltaPrepago);
    }
};





Identificadores.ConfigurarComboOperadores = function(){
    
    $('#movil1_fechaAlta').html(Identificadores.FechaAltaPostpago);
    $('#movil2_fechaAlta').html(Identificadores.FechaAltaPostpago);

    $('#movil1_tipo').change(function(){
        Identificadores.AsignarFechaAlta('#movil1_tipo','#movil1_fechaAlta');
    });
    
    $('#movil2_tipo').change(function(){
        Identificadores.AsignarFechaAlta('#movil2_tipo','#movil2_fechaAlta');
    });
};



Identificadores.ConfigurarCheckbox = function () {
    $("#telefono_notengo").change(function () {
        Identificadores.PresionarCheck(this.checked, '#telefono');

        if (this.checked) {
   $('#mensajeDireccion').html("<span style='color:red;font-weight:bold'>Cliente nuevo - verificar Direccion</span>");
   $('#cobertura').val('');
   Identificadores.MostrarMapa();
   Identificadores.ClienteNuevo();
        } else {
   Identificadores.OcultarMapa();
   $('#departamento_etiqueta').hide();
   $('#departamento').hide();
   $('#provincia_etiqueta').hide();
   $('#provincia').hide();
   $('#distrito_etiqueta').hide();
   $('#distrito').hide();
   $('#cobertura').val('');
   
        }
    });

    $("#movil1_notengo").change(function () {
        Identificadores.movil1_portabilidad = false;

        Identificadores.PresionarCheck(this.checked, '#movil1');

        if (this.checked) {
            Identificadores.RequiereConsultaABD1 = false;
            $('#movil1_operador').hide();
            $('#movil1_operador_etiqueta').hide();
            $('#movil1_tipo').hide();
            $('#movil1_tipo_etiqueta').hide();
            $('#movil1_fechaAlta_etiqueta').hide();
            $('#movil1_fechaAlta').hide();
            $('#movil1_fechaActivacion_etiqueta').hide();
            $('#movil1_fechaActivacion').hide();
            
            $('#movil1_cargofijo_etiqueta').hide();
            $('#movil1_cargofijo').hide();        
            $('#movil1_cargofijo').val('');    
            $('#error_movil1').hide();
            $('#error_movil1').html('');
            $('#cliente_movil1_renta').val('');
        } 
    });

    $("#movil2_notengo").change(function () {
        Identificadores.movil2_portabilidad = false;

        Identificadores.PresionarCheck(this.checked, '#movil2');

        if (this.checked) {
            Identificadores.RequiereConsultaABD2 = false;
            $('#movil2_operador').hide();
            $('#movil2_operador_etiqueta').hide();
            $('#movil2_tipo').hide();
            $('#movil2_tipo_etiqueta').hide();
            $('#movil2_fechaAlta_etiqueta').hide();
            $('#movil2_fechaActivacion_etiqueta').hide();
            $('#movil2_fechaAlta').hide();
            $('#movil2_fechaActivacion').hide();
            $('#movil2_cargofijo_etiqueta').hide();
            $('#movil2_cargofijo').hide();      
            $('#movil2_cargofijo').val('');    
            $('#error_movil2').hide();
            $('#error_movil2').html("");
            $('#cliente_movil2_renta').val('');
        } 
    });
};

Identificadores.MostrarMapa = function(){
    
if(window.google === undefined){
        $("#cobertura").prop("disabled", false);
        $('#direccion_texto').val('');
        $('#direccion_texto').show();
        $('#direccion_texto_etiqueta').show();
        $('#direccion_x_etiqueta').show();
        $('#direccion_x').val('');
        $('#direccion_x').show();
        $('#direccion_y_etiqueta').show();
        $('#direccion_y').val('');
        $('#direccion_y').show();
    } else {
        $('#mensajeDireccion').show();
        $('#pac-input').show();
        $('#pac-input').val('');
        $('#map').show();
        $('#direccion_texto').show();
        $('#consultarxy').show();
        $('#direccion_texto_etiqueta').show();
        $('#direccion_x_etiqueta').show();
        $('#direccion_x').val('');
        $('#direccion_x').show();
        $('#direccion_y_etiqueta').show();
        $('#direccion_y').val('');
        $('#direccion_y').show();
        $('#direccion_texto').val('');
        $("#cobertura").prop("disabled", true);
    }
};

Identificadores.OcultarMapa = function(){
    $('#mensajeDireccion').html("");
    $('#mensajeDireccion').hide();
    $('#pac-input').hide();
    $('#map').hide();
    $('#pac-input').val('');
    $('#direccion_texto').hide();
    $('#consultarxy').hide();
    $('#direccion_texto').val('');
    $('#direccion_x_etiqueta').hide();
    $('#direccion_x').hide();
    $('#direccion_x').val('');
    $('#direccion_y').hide();
    $('#direccion_y').val('');
    $('#direccion_y_etiqueta').hide();
    $('#direccion_texto_etiqueta').hide();
};


Identificadores.PresionarCheck = function(checked,componente){
    
    if(checked){
        $(componente).val('');
        $(componente).hide();
        
    } else {
        $(componente).val('');
        $(componente).show();
    }
};

var Telefono = {};
Telefono.ultimoTelefono = null;

Identificadores.VerificarTelefono = function(){
    var telefono = Identificadores.ValidarTelefono(Identificadores.OnDatoActualizarDigito('#telefono',8));
    //var telefono = Identificadores.ValidarTelefono($('#telefono').val());
 
    if(telefono !== null){
    //if(telefono !== null && Telefono.ultimoTelefono !== telefono){
        Telefono.ultimoTelefono = telefono;
        Util.ConsultaAjax('Http/ConsultarTelefono.php',{telefono: telefono},Identificadores.MostrarTelefono);
    } else {
        Telefono.ultimoTelefono = null;
        $('#error_telefonoFijo').hide();
        $('#cobertura').val('');
        Identificadores.OcultarMapa();
        
    }
};

Identificadores.MostrarTelefono = function(Consulta){
    if(Consulta.ok !== undefined){
        $('#error_telefonoFijo').html('<span style="color:green;font-weight:bold">Cliente fijo hogar</span>');
        $('#error_telefonoFijo').show();
        
        
        if(!Consulta.Fijo.EsHFC){
            $('#mensajeDireccion').html('<span style="color:red;font-weight:bold">Cliente ADSL o sin Internet - Por favor, verificar direccion</span>' );
            Identificadores.MostrarMapa();
        } else {
            Identificadores.EsClienteHFC(Consulta.Fijo.Internet.Tecnologia);
        }
    } else if(Consulta.error !== undefined){
        $('#error_telefonoFijo').text(Consulta.error);
        $('#error_telefonoFijo').show();
    }
};


var Movil1 = {};
Movil1.ultimoMovil1 = null;

Identificadores.VerificarMovil1 = function(){
    Identificadores.movil1_portabilidad = false;

    //var movil = Identificadores.ValidarCelular($('#movil1').val());
    var movil = Identificadores.ValidarCelular(Identificadores.OnDatoActualizarDigito('#movil1',9));
    if(movil !== null){
    //if(movil !== null && Movil1.ultimoMovil1 !== movil){
        Movil1.ultimoMovil1 = movil;
        Util.ConsultaAjax('Http/ConsultarMovil.php',{movil: movil},Identificadores.MostrarMovil1);
    } else {
        Movil1.ultimoMovil1 = null;
        $('#error_movil1').hide();
        $('#error_movil1').html("");
        $('#movil1_operador_etiqueta').hide();
        $('#movil1_operador').hide();
        $('#movil1_tipo_etiqueta').hide();
        $('#movil1_tipo').hide();
        $('#movil1_fechaAlta_etiqueta').hide();
        $('#movil1_fechaAlta').hide();
        $('#movil1_fechaActivacion_etiqueta').hide();
        $('#movil1_fechaActivacion').hide();
        $('#movil1_cargofijo_etiqueta').hide();
        $('#movil1_cargofijo').hide();
        $('#cliente_movil1_renta').val('');
    }
};

Identificadores.MostrarMovil1 = function(Consulta){
    if(Consulta.ok !== undefined){
        if(Consulta.ok == 'Celular Movistar MT') {
            $('#error_movil1').html('<span style="color:red;font-weight:bold">Cliente Movistar MT</span>');
            $('#error_movil1').show();

            $('#movil1_operador_etiqueta').hide();
            $('#movil1_operador').hide();
            $('#movil1_tipo_etiqueta').hide();
            $('#movil1_tipo').hide();
            $('#movil1_fechaAlta_etiqueta').hide();
            $('#movil1_fechaAlta').hide();
            $('#movil1_fechaActivacion_etiqueta').hide();
            $('#movil1_fechaActivacion').hide();
            $('#movil1_cargofijo_etiqueta').hide();
            $('#movil1_cargofijo').hide();
            $('#cliente_movil1_renta').val('');

            Identificadores.RequiereConsultaABD1 = false;
        } else {
            $('#error_movil1').html('<span style="color:green;font-weight:bold">Cliente Movistar - No es Titular de la Linea</span>');
            $('#cliente_movil1_renta').val(Consulta.Renta);
            $('#error_movil1').show();
            Identificadores.RequiereConsultaABD1 = false;
        }
    } else if(Consulta.error !== undefined){
        Identificadores.RequiereConsultaABD1 = true;
        Identificadores.movil1_portabilidad = true;

        $('#error_movil1').text(Consulta.error);
        $('#error_movil1').show();
        $('#movil1_operador_etiqueta').show();
        $('#movil1_operador').show();
        $('#movil1_tipo_etiqueta').show();
        $('#movil1_tipo').show();
        $('#movil1_fechaAlta_etiqueta').show();
        $('#movil1_fechaAlta').show();
        $('#movil1_fechaActivacion_etiqueta').show();
        $('#movil1_fechaActivacion').show();
        $('#movil1_cargofijo_etiqueta').show();
        $('#movil1_cargofijo').show();
        $('#cliente_movil1_renta').val('');
    }
};

var Movil2 = {};
Movil2.ultimoMovil2 = null;

Identificadores.VerificarMovil2 = function(){
    Identificadores.movil2_portabilidad = false;

    //var movil = Identificadores.ValidarCelular($('#movil2').val());
    var movil = Identificadores.ValidarCelular(Identificadores.OnDatoActualizarDigito('#movil2',9));
    
    if(movil !== null){
    //if(movil !== null && Movil2.ultimoMovil2 !== movil){
        Movil2.ultimoMovil1 = movil;
        Util.ConsultaAjax('Http/ConsultarMovil.php',{movil: movil},Identificadores.MostrarMovil2);
    } else {
        Movil2.ultimoMovil2 = null;
        $('#error_movil2').hide();
        $('#error_movil2').html('');
        $('#movil2_operador_etiqueta').hide();
        $('#movil2_operador').hide();
        $('#movil2_tipo_etiqueta').hide();
        $('#movil2_tipo').hide();
        $('#movil2_fechaAlta_etiqueta').hide();
        $('#movil2_fechaAlta').hide();
        $('#movil2_fechaActivacion_etiqueta').hide();
        $('#movil2_fechaActivacion').hide();
        $('#movil2_cargofijo_etiqueta').hide();
        $('#movil2_cargofijo').hide();
        $('#cliente_movil2_renta').val('');
    }
};

Identificadores.MostrarMovil2 = function(Consulta){
    if(Consulta.ok !== undefined){
        if(Consulta.ok == 'Celular Movistar MT') {
            $('#error_movil2').html('<span style="color:red;font-weight:bold">Cliente Movistar MT</span>');
            $('#error_movil2').show();

            $('#movil2_operador_etiqueta').hide();
            $('#movil2_operador').hide();
            $('#movil2_tipo_etiqueta').hide();
            $('#movil2_tipo').hide();
            $('#movil2_fechaAlta_etiqueta').hide();
            $('#movil2_fechaAlta').hide();
            $('#movil2_fechaActivacion_etiqueta').hide();
            $('#movil2_fechaActivacion').hide();
            $('#movil2_cargofijo_etiqueta').hide();
            $('#movil2_cargofijo').hide();
            $('#cliente_movil2_renta').val('');

            Identificadores.RequiereConsultaABD2 = false;
        } else {
            $('#error_movil2').html('<span style="color:green;font-weight:bold">Cliente Movistar - No es Titular de la Linea</span>');
            $('#cliente_movil2_renta').val(Consulta.Renta);
            $('#error_movil2').show();
            Identificadores.RequiereConsultaABD2 = false;
        }
    } else if(Consulta.error !== undefined){
        Identificadores.RequiereConsultaABD2 = true;
        Identificadores.movil2_portabilidad = true;

        $('#error_movil2').text(Consulta.error);
        $('#error_movil2').show();
        $('#movil2_operador_etiqueta').show();
        $('#movil2_operador').show();
        $('#movil2_tipo_etiqueta').show();
        $('#movil2_tipo').show();
        $('#movil2_fechaAlta_etiqueta').show();
        $('#movil2_fechaAlta').show();
        $('#movil2_fechaActivacion_etiqueta').show();
        $('#movil2_fechaActivacion').show();
        $('#movil2_cargofijo_etiqueta').show();
        $('#movil2_cargofijo').show();
        $('#cliente_movil2_renta').val('');
    }
};


Identificadores.EsClienteHFC = function(Internet) {
    if(Internet == 'FTTH') {
        $('#mensajeDireccion').html('<span style="color:green;font-weight:bold">Cliente FTTH - No es necesario verificar cobertura</span>');
        $('#cobertura').val('FTTH');
    } else {
        $('#mensajeDireccion').html('<span style="color:green;font-weight:bold">Cliente HFC - No es necesario verificar cobertura</span>');
        $('#cobertura').val('HFC');
    }
    $('#mensajeDireccion').show();
    $('#direccion_texto').val('Direccion: ');
    $('#direccion_y').val('y: ');
    $('#direccion_x').val('x: ');
    Identificadores.RequiereConsultaGIS = false;
};

Identificadores.MostrarAltaNueva = function(){
    $('#error_movil1').html('');
    $('#error_movil2').html('');
    $('#Formulario_Identificadores').show();

    $('#telefono').hide();

    $('#telefono_notengo').show();    
    $('#telefono_notengo').prop('checked',true);
    $('#telefono_notengo_etiqueta').show();
    
    $('#mensajeDireccion').html("<span style='color:red;font-weight:bold'>Cliente nuevo - verificar Direccion</span>");
    $('#cobertura').val('');
    Identificadores.MostrarMapa();
    Identificadores.ClienteNuevo();
    
    $('#movil1').val('');
    $('#movil1').prop('disabled',false);
    $('#cliente_movil1_tipolinea').text('');
    $('#cliente_movil1_renta').val('');    

    $('#movil1_notengo').show();
    $('#movil1_notengo_etiqueta').show();
    $('#movil1').keyup(Identificadores.VerificarMovil1);
    
    $('#movil2').val('');
    $('#movil2').prop('disabled',false);
    $('#cliente_movil2_tipolinea').text('');
    $('#cliente_movil2_renta').val('');

    $('#movil2_notengo').show();
    $('#movil2_notengo_etiqueta').show();
    $('#movil2').keyup(Identificadores.VerificarMovil2);

    $('#telefono').keyup(function(){var telefono = Identificadores.OnDatoActualizarDigito('#telefono',8);});
    $('#movil1_cargofijo').keyup(function() {Identificadores.OnDatoActualizarMonto('#movil1_cargofijo');});
};

Identificadores.Mostrar = function(Consulta){
    if(Consulta.tipo === 'Alta Nueva'){
        Identificadores.MostrarAltaNueva();
        return;
    } 
    
    $('#error_movil1').html('');
    $('#error_movil2').html('');
    $('#Formulario_Identificadores').show();
    
    if(Consulta.Fijo.Presente === 1){
        
        $('#telefono').keyup(function(){});
        
        $('#telefono').val(Consulta.Fijo.Numero);
        $('#telefono').prop('disabled',true);
        
        $('#cliente_direccion_instalacion').text(Consulta.Fijo.Direccion);
        $('#direccion_texto').val('DIR: ' + Consulta.Fijo.Direccion);
        
        if(!Consulta.Fijo.EsHFC){
            $('#mensajeDireccion').html('<span style="color:red;font-weight:bold">Cliente ADSL o sin Internet - Por favor, verificar direccion</span>' );
            Identificadores.MostrarMapa();
        } else {
            Identificadores.EsClienteHFC(Consulta.Fijo.Internet.Tecnologia);
        }
    } else {
        
        $('#cliente_direccion_instalacion').text('');
        $('#telefono_notengo').show();
        $('#telefono_notengo_etiqueta').show();
        $('#telefono').keyup(Identificadores.VerificarTelefono);
    }

    if(Consulta.Movil1.Presente === 1){
        Identificadores.RequiereConsultaABD1 = false;
        $('#movil1').val(Consulta.Movil1.Numero);
        $('#movil1').prop('disabled',true);
        if(Consulta.Movil1.MovistarTotal) {
            $('#cliente_movil1_tipolinea').text(" - "+Consulta.Movil1.DescripcionMT);
        } else {
            $('#cliente_movil1_tipolinea').text(" - "+Consulta.Movil1.Descripcion);
        }
        $('#cliente_movil1_renta').val(Consulta.Movil1.RentaTotalConAdicionales);
        $('#movil1').keyup(function(){});
    } else {
        $('#movil1_notengo').show();
        $('#movil1').val('');
        $('#movil1').prop('disabled',false);
        $('#cliente_movil1_tipolinea').text('');
        $('#cliente_movil1_renta').val('');
        
        
        $('#movil1_notengo_etiqueta').show();
        $('#movil1').keyup(Identificadores.VerificarMovil1);
    }

    if(Consulta.Movil2.Presente === 1){
        Identificadores.RequiereConsultaABD2 = false;
        $('#movil2').val(Consulta.Movil2.Numero);
        $('#movil2').prop('disabled',true);
        if(Consulta.Movil2.MovistarTotal) {
            $('#cliente_movil2_tipolinea').text(" - "+Consulta.Movil2.DescripcionMT);
        } else {
            $('#cliente_movil2_tipolinea').text(" - "+Consulta.Movil2.Descripcion);
        }
        $('#cliente_movil2_renta').val(Consulta.Movil2.RentaTotalConAdicionales);
        $('#movil2').keyup(function(){});
        
    } else {
        $('#movil2_notengo').show();
        $('#movil2').val('');
        $('#movil2').prop('disabled',false);
        $('#cliente_movil2_tipolinea').text('');
        $('#cliente_movil2_renta').val('');
        $('#movil2_notengo_etiqueta').show();
        $('#movil2').keyup(Identificadores.VerificarMovil2);
    }
    
    $('#movil1_cargofijo').keyup(function() {Identificadores.OnDatoActualizarMonto('#movil1_cargofijo');});
};


Identificadores.TipoCambioConsulta = function(){
    return Util.TipoCambio(Identificadores.ultimaConsulta,Identificadores.consulta);
};

Identificadores.LimpiarHijos = function(){
    window.ResultadoConsulta = null;
    Identificadores.ultimaConsulta = null;
    Identificadores.EstaConsultado = false;
    Parrilla.Limpiar();
    Comparacion.Limpiar();
};


Identificadores.LlenarCategoria = function(){

    var opciones = "";        
    var Ofertas = window.respuesta.Resultado.Ofertas;
    var categorias = {};
    for(var oferta in Ofertas){
   categorias[Ofertas[oferta].producto.Paquete.Categoria] = true;
    }
    
    for(var categoria in categorias){
        opciones += "<option  value= '" + categoria +"'>" + categoria +"</option>";  
    }
    $('#seleccionCategoria').html(opciones);
};

Identificadores.MostrarOfertaSeleccionada = function(){

    var Identificador = $('#seleccionProducto').val();
    
    if(Identificador === '-'){ 
        return ;
    }
    
    var Ofertas = window.respuesta.Resultado.Ofertas;
    
    window.respuesta.Oferta4 = Ofertas[Identificador].DataDisplay;
    Identificadores.RespuestaConsulta(window.respuesta);

};

Identificadores.LlenarProducto = function(){

    var opciones = "";        
    var Ofertas = window.respuesta.Resultado.Ofertas;
    var Categoria = 'Movistar Total';
    
    opciones += "<option  value= '-'>-</option>";
    
    for(var oferta in Ofertas){
        if(Ofertas[oferta].producto.Paquete.Categoria === Categoria && Ofertas[oferta].mejor_up != 1)
        {
   opciones += "<option  value= '" +  oferta +"'>" + Ofertas[oferta].producto.Paquete.NombreOrigen +" - S/." + Ofertas[oferta].producto.Paquete.RentaOrigen + "</option>";
        }  
    }
    $('#seleccionProducto').html(opciones);
};

function trimStr(str) {
    return str.replace(/^\s+|\s+$/g, '');
}

Identificadores.OnDatoActualizarCoordenada = function(NombreInput){
    var Texto = $(NombreInput).val();
    var TextoLimpio = "";
    for (var i = 0; i < Texto.length; ++i) {
       var caracter = Texto.charAt(i);
       if ((caracter >= '0' && caracter <= '9') || caracter == '-' || caracter == '.') {
          TextoLimpio += caracter;
       }
    }
    if(TextoLimpio !== Texto){
        $(NombreInput).val(TextoLimpio);
    }
};
Identificadores.OnDatoActualizarDigito = function(NombreInput,cantidad){
    var Texto = $(NombreInput).val();
    var TextoLimpio = "";
    for (var i = 0; i < Texto.length; ++i) {
       var caracter = Texto.charAt(i);
       if (caracter >= '0' && caracter <= '9' && (i<cantidad || cantidad == 0)) {
          TextoLimpio += caracter;
       }
    }
    if(TextoLimpio !== Texto){
        $(NombreInput).val(TextoLimpio);
    }

    return TextoLimpio;
};
Identificadores.OnDatoActualizarMonto = function(NombreInput){
    var Texto = $(NombreInput).val();
    var TextoLimpio = "";
    for (var i = 0; i < Texto.length; ++i) {
       var caracter = Texto.charAt(i);
       if ((caracter >= '0' && caracter <= '9') || caracter == '.') {
          TextoLimpio += caracter;
       }
    }
    if(TextoLimpio !== Texto){
        $(NombreInput).val(TextoLimpio);
    }
};