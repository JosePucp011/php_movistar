/* global Util */

var Pedido = {};
Pedido.Oferta = null;
Pedido.Cliente = null;
Pedido.Usuario = null;
Pedido.Registrado = false;

$(document).on("keyup","#NumeroContacto",function() { Pedido.OnDatoActualizarDigito('#NumeroContacto',9); });
$(document).on("keyup","#ClienteMovil1Registro",function() { Pedido.OnDatoActualizarDigito('#ClienteMovil1Registro',9); });
$(document).on("keyup","#ClienteMovil2Registro",function() { Pedido.OnDatoActualizarDigito('#ClienteMovil2Registro',9); });
$(document).on("keyup","#ClienteFijoRegistro",function() { Pedido.OnDatoActualizarDigito('#ClienteFijoRegistro',8); });

Pedido.Mostrar = function(){
    Util.ConsultaAjax('Http/ObtenerPedidos.php',[],Pedido.LlenarPedidos);
};

Pedido.LlenarPedidos = function(Pedidos){

    Pedido.Datos = Pedidos;
            
    var tablero = "";
    
        tablero += "<table  style='width: 100%; border-spacing: 15px 0px; text-align: center;'><tr>" + 
                "<th class = 'ListaDePedidos'>ID</th>" +
                "<th class = 'ListaDePedidos'>Asesor Usuario</th>" +
                "<th class = 'ListaDePedidos'>Asesor Nombre</th>" +
                "<th class = 'ListaDePedidos'>Asesor DNI</th>" +
                "<th class = 'ListaDePedidos'>Fecha de Solicitud</th>" +
                "<th class = 'ListaDePedidos'>Nombre Cliente</th>" +
                "<th class = 'ListaDePedidos'>Tipo Documento</th>" +
                "<th class = 'ListaDePedidos'>Documento</th>" +
                "<th class = 'ListaDePedidos'>Numero Fijo</th>" +
                "<th class = 'ListaDePedidos'>Plan Actual Fijo</th>" +
                "<th class = 'ListaDePedidos'>Operacion Comercial Fijo</th>" +
                "<th class = 'ListaDePedidos'>Numero Movil 1</th>" +
                "<th class = 'ListaDePedidos'>Plan Actual Movil 1</th>" +
                "<th class = 'ListaDePedidos'>Operacion Comercial Movil 1</th>" +
                "<th class = 'ListaDePedidos'>Numero Movil 2</th>" +
                "<th class = 'ListaDePedidos'>Plan Actual Movil 2</th>" +
                "<th class = 'ListaDePedidos'>Operacion Comercial Movil 2</th>" +
                "<th class = 'ListaDePedidos'>Oferta</th>" +
                "<th class = 'ListaDePedidos'>Salto</th>" +
                "<th class = 'ListaDePedidos'>Linea de Contacto</th>" +
                "<th class = 'ListaDePedidos'>Correo</th>" +
                "<th class = 'ListaDePedidos'>Departamento / Provincia / Distrito </th>" +
                "<th class = 'ListaDePedidos'>Direccion</th>" +
                "<th class = 'ListaDePedidos'>Coordenada X</th>" +
                "<th class = 'ListaDePedidos'>Coordenada Y</th>" +
                "</tr>";
    
        for(var i in Pedidos){
            
            var pedido = Pedidos[i];
           tablero += "<tr>" +
                "<td   class = 'ListaDePedidos'>" + pedido.ID+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Asesor_Usuario +"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Asesor_Nombre +"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Asesor_DNI+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Fecha_Solicitud+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Cliente_Nombre+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Cliente_TipoDocumento+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Cliente_Documento+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Fijo_Numero+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Fijo_Tenencia+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Fijo_OperacionComercial+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Movil1_Numero+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Movil1_Tenencia+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Movil1_OperacionComercial+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Movil2_Numero+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Movil2_Tenencia+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Movil2_OperacionComercial+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Oferta_MT+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Salto+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Linea_de_Contacto+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Correo+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Ubicacion+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Direccion+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Coordenada_X+"</td>" +
                "<td   class = 'ListaDePedidos'>" + pedido.Coordenada_Y+"</td>" +
                "</tr>";
   }
    
    tablero += "</table>";
    
    $('#RegistroPedidos').html(tablero);
};


// Inicio del Pedido
Pedido.MostrarFormulario = function(oferta){
    Pedido.Registrado = false;
    Pedido.Oferta = oferta;
    Pedido.Cliente = window.respuesta.Cliente;
    Pedido.Usuario = window.respuesta.Usuario;
    
    //var MovilOC = ['Alta','Portabilidad','CAEQ con Upsell','CAEQ sin Upsell','Cambio Titularidad'];
    var MovilOC = ['Alta','Portabilidad','Por Paquetizar','CAEQ con Upsell','CAEQ sin Upsell'];
    var MTMovilOC = ['CAEQ'];
    //var TelefonoOC = ['Alta','Completa TV','Completa BA'];
    var TelefonoOC = ['Alta','Por Paquetizar','Completa TV','Completa BA','Cambio Titularidad'];
    var contenido = "<h1>Informacion de Registro</h1>";
    
    /* Informacion del asesor */
    
    contenido +="<fieldset style='margin-left: 15px;margin-right: 15px;'>" +
                    "<legend>Asesor</legend>" +
                    "<table>" +
                        "<tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='AsesorUsuario' style='margin-left: 5px;'>Usuario </label>" +
                                "<input type='text' id='AsesorUsuario' style='margin-right: 5px;' value='" + Pedido.Usuario.Usuario + "' disabled>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='AsesorNombre' style='margin-left: 5px;'>Nombre </label>" +
                                "<input type='text' id='AsesorNombre' style='margin-right: 5px;width: 250px;'  value='" + Pedido.Usuario.Nombre + "'  disabled>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='AsesorDocumento'>Documento </label>" +
                                "<input type='text' id='AsesorDocumento'  value='" + Pedido.Usuario.Documento + "'  disabled>" +
                            "</td>" +
                        "</tr>" +
                    "</table>" +
                "</fieldset><br>";

    /* Informacion de Cliente*/
    contenido +="<fieldset style='margin-left: 15px;margin-right: 15px;'>" +
                    "<legend>Cliente</legend>" +
                    "<table>" +
                        "<tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteNombre' style='margin-left: 5px;'>Nombre </label>" +
                                "<input type='text' id='ClienteNombre' style='margin-right: 5px;width: 245px;' disabled value='" + Pedido.Cliente.Nombre + "'>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteTipoDocumento' style='margin-left: 5px;'>Tipo Documento </label>" +
                                "<input type='text' id='ClienteTipoDocumento' style='margin-right: 5px;width: 50px;'  disabled value='" + Pedido.Cliente.TipoDocumento + "'>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteDocumento'>Documento </label>" +
                                "<input type='text' id='ClienteDocumento'  disabled value='" + Pedido.Cliente.Documento + "'>" +
                            "</td>" +
                        "</tr>" +
                    "</table>" +
                    "<table>" +
                        "<tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='CorreoCliente' style='margin-left: 5px;'>Correo  </label>" +
                                "<input type='text' id='CorreoCliente' style='margin-right: 5px;width: 385px;' value='" + Pedido.Cliente.Correo + "'>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='NumeroContacto' style='margin-left: 5px;'>Numero Contacto </label>" +
                                "<input type='text' id='NumeroContacto' style='margin-right: 5px;' value='-'>" +
                            "</td>" +
                        "</tr>" +
                    "</table>" +
                "</fieldset><br>";
        
    /* Informacion de Servicios*/    

    var CategoriaFijo = Pedido.Cliente.Fijo.Categoria;
    var CategoriaMovil1 = Pedido.Cliente.Movil1.Categoria;
    var CategoriaMovil2 = Pedido.Cliente.Movil2.Categoria;

    var DisabledFijo = 'disabled';
    var DisabledMovil1 = 'disabled';
    var DisabledMovil2 = 'disabled';
    
    if(Pedido.Cliente.Fijo.MovistarTotal){
       CategoriaFijo = 'Trio MT'; 
    }
    if(Pedido.Cliente.Movil1.MovistarTotal){
       CategoriaMovil1 = 'Postpago MT'; 
    }    
    if(Pedido.Cliente.Movil2.MovistarTotal){
       CategoriaMovil2 = 'Postpago MT'; 
    }

    if(Pedido.Cliente.Fijo.Numero == '-' || Pedido.Cliente.Fijo.Numero == '') {
        DisabledFijo = '';
    }
    if(Pedido.Cliente.Movil1.Numero == '-' || Pedido.Cliente.Movil1.Numero == '') {
        DisabledMovil1 = '';
    }
    if(Pedido.Cliente.Movil2.Numero == '-' || Pedido.Cliente.Movil2.Numero == '') {
        DisabledMovil2 = '';
    }

    contenido +="<fieldset style='margin-left: 15px;margin-right: 15px;'>" +
                    "<legend>Servicios</legend>" +
                    "<table>" +
                        "<tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteMovil1Registro' style='margin-left: 5px;'>Movil 1 </label>" +
                                "<input " + DisabledMovil1 + " type='text' id='ClienteMovil1Registro' style='margin-right: 5px;' value='" + Pedido.Cliente.Movil1.Numero + "'>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteTipoDocumento' style='margin-left: 5px;'>Servicio Actual </label>" +
                                "<input type='text' id='ClienteTipoDocumento' style='margin-right: 5px;'  disabled value='" + CategoriaMovil1  + "'>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='OperacionComercialMovil1'>Operacion Comercial </label>" +
                                "<select id='OperacionComercialMovil1' style='margin-right: 5px; width: 130px;' >";

                    if(Pedido.Cliente.Movil1.MovistarTotal && Pedido.Cliente.Fijo.MovistarTotal){
                        for (var nCant = 0; nCant < MTMovilOC.length; nCant++) {
                            var elemento = MTMovilOC[nCant];

                            selected = '';
                            if(elemento == Pedido.Oferta.Movil1.OperacionComercial) {
                                selected = 'selected';
                            }
                            contenido +="<option " + selected + " value='" + elemento + "'>" + elemento + "</option>";
                        }
                    }else{
                        var Movil1OperacionComercial = Pedido.Oferta.Movil1.OperacionComercial;
                        if(Pedido.Cliente.Movil2.Numero == '-' || Pedido.Cliente.Movil2.Numero == '') {
                            Movil1OperacionComercial = 'Alta';
                        } else if(window.respuesta.Cliente.Movil1.Categoria.toUpperCase() == 'COMPETENCIA') {
                            Movil1OperacionComercial = 'Portabilidad';
                        } else if(Pedido.Oferta.Movil1.OperacionComercial == 'CAPL') {
                            Movil1OperacionComercial = 'Por Paquetizar';
                        }

                        for (var nCant = 0; nCant < MovilOC.length; nCant++) {
                            var elemento = MovilOC[nCant];

                            selected = '';
                            if(elemento == Movil1OperacionComercial) {
                                selected = 'selected';
                            }
                            contenido +="<option " + selected + " value='" + elemento + "'>" + elemento + "</option>";
                        }
                    }
                                contenido += "</select>" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteMovil2Registro' style='margin-left: 5px;'>Movil 2 </label>" +
                                "<input " + DisabledMovil2 + " type='text' id='ClienteMovil2Registro' style='margin-right: 5px;' value='" + Pedido.Cliente.Movil2.Numero + "'>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteTipoDocumento' style='margin-left: 5px;'>Servicio Actual </label>" +
                                "<input type='text' id='ClienteTipoDocumento' style='margin-right: 5px;'  disabled value='" + CategoriaMovil2 + "'>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='OperacionComercialMovil2'>Operacion Comercial </label>" +
                                "<select id='OperacionComercialMovil2' style='margin-right: 5px; width: 130px;' >";
                    
                    var  selectedIng = 0;
                    if(Pedido.Cliente.Movil2.Numero == '-' || Pedido.Cliente.Movil2.Numero == '') {
                        contenido += "<option selected value='-'>No Desea</option>";
                        selectedIng = 1;
                    }
                    
                    if(Pedido.Cliente.Movil1.MovistarTotal 
                            && Pedido.Cliente.Movil2.MovistarTotal
                            && Pedido.Cliente.Fijo.MovistarTotal){
                            for (var nCant = 0; nCant < MTMovilOC.length; nCant++) {
                                var elemento = MTMovilOC[nCant];
    
                                selected = '';
                                if(elemento == Pedido.Oferta.Movil2.OperacionComercial && selectedIng == 0) {
                                    selected = 'selected';
                                    selectedIng = 1;
                                }
                                contenido +="<option " + selected + " value='" + elemento + "'>" + elemento + "</option>";
                            }
                    }else{
                        var Movil2OperacionComercial = Pedido.Oferta.Movil2.OperacionComercial;
                        if(window.respuesta.Cliente.Movil2.Categoria.toUpperCase() == 'COMPETENCIA') {
                            Movil2OperacionComercial = 'Portabilidad';
                        } else if(Pedido.Oferta.Movil2.OperacionComercial == 'CAPL') {
                            Movil2OperacionComercial = 'Por Paquetizar';
                        }
                        
                        for (var nCant = 0; nCant < MovilOC.length; nCant++) {
                            var elemento = MovilOC[nCant];

                            selected = '';
                            if(elemento == Movil2OperacionComercial && selectedIng == 0) {
                                selected = 'selected';
                                selectedIng = 1;
                            }
                            contenido +="<option " + selected + " value='" + elemento + "'>" + elemento + "</option>";
                        }
                    }
                     contenido +="</select>" +
                            "</td>" +
                        "</tr>" +
                                                "<tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteFijoRegistro' style='margin-left: 5px;'>Fijo </label>" +
                                "<input " + DisabledFijo + " type='text' id='ClienteFijoRegistro' style='margin-right: 5px;' value='" + Pedido.Cliente.Fijo.Numero + "'>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteTipoDocumento' style='margin-left: 5px;'>Servicio Actual </label>" +
                                "<input type='text' id='ClienteTipoDocumento' style='margin-right: 5px;'  disabled value='" + CategoriaFijo  + "'>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='OperacionComercialFijo'>Operacion Comercial </label>" +
                                "<select id='OperacionComercialFijo' style='margin-right: 5px; width: 130px;' >";
                                    
                            if(Pedido.Cliente.Fijo.MovistarTotal){
                                contenido +="<option value='-'>-</option>";
                            }else{
                                var FijoOperacionComercial = '';
                                if(Pedido.Cliente.Fijo.Titular=='Requiere cambio titularidad'){
                                    FijoOperacionComercial = 'Cambio Titularidad';
                                } else if(Pedido.Cliente.Fijo.Categoria.toUpperCase().indexOf('TRIO') >= 0 || 
                                   Pedido.Cliente.Fijo.Categoria.toUpperCase().indexOf('MONO TV') >= 0 || 
                                   Pedido.Cliente.Fijo.Categoria.toUpperCase().indexOf('NAKED') >= 0) {
                                    FijoOperacionComercial = 'Por Paquetizar';
                                } else if(Pedido.Cliente.Fijo.Categoria.toUpperCase().indexOf('DUO') >= 0) {
                                    if(Pedido.Cliente.Fijo.Categoria.toUpperCase().indexOf('TV') >= 0 || 
                                    Pedido.Cliente.Fijo.Categoria.toUpperCase().indexOf('DTH') >= 0 || 
                                    Pedido.Cliente.Fijo.Categoria.toUpperCase().indexOf('CATV') >= 0) {
                                        FijoOperacionComercial = 'Completa BA';
                                    } else {
                                        FijoOperacionComercial = 'Completa TV';
                                    }
                                }

                                //contenido +="<option value='-'>-</option>"; 
                                for (var nCant = 0; nCant < TelefonoOC.length; nCant++) {
                                    var elemento = TelefonoOC[nCant];
        
                                    selected = '';
                                    //if(elemento == Pedido.Oferta.Fijo.OperacionComercial) {
                                    if(elemento == FijoOperacionComercial) {
                                        selected = 'selected';
                                    }
                                    contenido +="<option " + selected + " value='" + elemento + "'>" + elemento + "</option>";
                                }
                            }
                                contenido +="</select>" +
                            "</td>" +
                        "</tr>" +
                    "</table>" +
                    
                    "<table>" +
                        "<tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteNombre' style='margin-left: 5px;'>Departamento / Provincia / Distrito </label>" +
                                "<input type='text' id='ClienteNombre' style='margin-right: 5px;width: 400px;' disabled value='" + Pedido.Cliente.Fijo.Ubicacion + "'>" +
                            "</td>" +
                        "</tr><tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='ClienteTipoDocumento' style='margin-left: 5px;'>Direccion </label>" +
                                "<input type='text' id='ClienteTipoDocumento' style='margin-right: 5px;width: 550px;'  disabled value='" + Pedido.Cliente.Fijo.Direccion + "'>" +
                            "</td>" +
                        "</tr>" +
                    "</table>" +
                    
                "</fieldset><br>";

     /* Informacion de Oferta*/    

    contenido +="<fieldset style='margin-left: 15px;margin-right: 15px;'>" +
                    "<legend>Oferta</legend>" +
                    "<table>" +
                        "<tr>" +
                            "<td  style='width: auto;'>" +
                                "<label for='PedidoOfertaNombre' style='margin-left: 5px;'>Oferta  </label>" +
                                "<input type='text' id='PedidoOfertaNombre' style='margin-right: 5px;width: 420px;' value='" + Pedido.Oferta.Nombre + "' disabled>" +
                            "</td>" +
                            "<td  style='width: auto;'>" +
                                "<label for='PedidoOfertaSalto'>Salto </label>" +
                                "<input type='text' id='PedidoOfertaSalto'  disabled value='" + Pedido.Oferta.Salto + "'>" +
                            "</td>" +
                        "</tr>" +
                    "</table>" +
                "</fieldset><br>";   
    
    contenido +="<input type='submit' onclick='Pedido.Guardar()' style='margin-left: 15px;' value='Guardar' ><br><br>";
    
    Util.MostrarModal("",contenido);
}; 
// FIN de pedido


Pedido.Guardar = function(){
    
    var AsesorUsuario = $('#AsesorUsuario').val();
    var AsesorNombre = $('#AsesorNombre').val();
    var AsesorDocumento = $('#AsesorDocumento').val();

    var Documento = $('#documento').val();
    
    var CorreoCliente = $('#CorreoCliente').val();
    var NumeroContacto = $('#NumeroContacto').val();
    
    var OperacionComercialFijo = $('#OperacionComercialFijo').val();
    var OperacionComercialMovil1 = $('#OperacionComercialMovil1').val();
    var OperacionComercialMovil2 = $('#OperacionComercialMovil2').val();
 
    var ClienteFijoRegistro = $('#ClienteFijoRegistro').val();
    var ClienteMovil1Registro = $('#ClienteMovil1Registro').val();
    var ClienteMovil2Registro = $('#ClienteMovil2Registro').val();
    
    
    var X = $('#direccion_x').val();
    var Y = $('#direccion_y').val();   
    
    if(CorreoCliente === '' || CorreoCliente === '-'){
        alert('Colocar correo electrónico del cliente');
        return;
    }

    if(!isValidEmail(CorreoCliente)) {
        alert('Ingresar correctamente correo electrónico del cliente');
        return;
    }
    
    if(NumeroContacto === '' || NumeroContacto === '-'){
        alert('Colocar numero de contacto del cliente');
        return;
    }

    if(isValidDigit(NumeroContacto)) {
        alert('Ingresar correctamente número de contacto del cliente');
        return;
    }

    //Validación de Alta
    if(OperacionComercialMovil1 == 'Alta') {
        if(ClienteMovil1Registro == '' || ClienteMovil1Registro == '-') {
            alert('Es obligatorio ingresar el Número de Movil 1 para Operación Comercial Alta.');
            return;
        } else {
            if(isValidDigit(ClienteMovil1Registro) || ClienteMovil1Registro.length != 9) {
                alert('Ingresar correctamente número de Móvil 1.');
                return;
            }
        }
    }
    if(OperacionComercialMovil2 == 'Alta') {
        if(ClienteMovil2Registro == '' || ClienteMovil2Registro == '-') {
            alert('Es obligatorio ingresar el Número de Movil 2 para Operación Comercial Alta.');
            return;
        } else {
            if(isValidDigit(ClienteMovil2Registro) || ClienteMovil2Registro.length != 9) {
                alert('Ingresar correctamente número de Móvil 2.');
                return;
            }
        }
    }
    
    if(Pedido.Registrado === false)
    {
        Util.ConsultaAjaxPostObjeto('Http/RegistrarOferta.php',
        {
            asesorUsuario : AsesorUsuario,
            asesorNombre : AsesorNombre,
            asesorDocumento : AsesorDocumento,
            
            Documento   :   Documento,
            correoCliente : CorreoCliente,
            numeroContacto : NumeroContacto,
            
            operacionComercialFijo : OperacionComercialFijo,
            operacionComercialMovil1 : OperacionComercialMovil1,
            operacionComercialMovil2 : OperacionComercialMovil2,
		
            ClienteFijoRegistro : ClienteFijoRegistro,
            ClienteMovil1Registro : ClienteMovil1Registro,
            ClienteMovil2Registro : ClienteMovil2Registro,

            x : X,
            y : Y,
            
            oferta : Pedido.Oferta, 
            cliente: Pedido.Cliente,

            CodigoScoringFijo: CodigoScoringFijo,
            CodigoScoringMovil1: CodigoScoringMovil1,
            CodigoScoringMovil2: CodigoScoringMovil2
        },
        Pedido.MostrarMensaje);
    }else{
        alert('Pedido ya fue registrado');
    }
};

Pedido.MostrarMensaje = function(mensaje){
  
    alert(mensaje);
    
    if(mensaje === 'Pedido Registrado'){
        Pedido.Registrado = true;
    }
};

function isValidEmail(mail) {
    emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    return emailRegex.test(mail); 
}

function isValidDigit(num) {
    Regex = /[^0-9]/;
    return Regex.test(num); 
}

Pedido.OnDatoActualizarDigito = function(NombreInput,cantidad){
    var Texto = $(NombreInput).val();
    var TextoLimpio = "";
    for (var i = 0; i < Texto.length; ++i) {
       var caracter = Texto.charAt(i);
       if (((caracter >= '0' && caracter <= '9') || caracter == '-') && (i<cantidad || cantidad == 0)) {
          TextoLimpio += caracter;
       }
    }
    if(TextoLimpio !== Texto){
        $(NombreInput).val(TextoLimpio);
    }

    return TextoLimpio;
};