/*global Comparacion*/

var Parrilla = {};
Parrilla.HTML = "";

Parrilla.MostrarParrilla = function() {
    Parrilla.AgruparProductosPorCategoria();
    Parrilla.ImprimirCategorias();
};

Parrilla.AgruparProductosPorCategoria = function(){
    for(var i = 0; i < window.ResultadoConsulta.Ofertas.length; ++i){
        var oferta = window.ResultadoConsulta.Ofertas[i];
        oferta.CorrelativoArreglo = i;
        Parrilla.Categorias[oferta.producto.Paquete.Categoria].ofertas.push(oferta);
    }
};


Parrilla.Limpiar = function(){
    $('#parrilla').html("");
    Parrilla.Categorias = [];
};

Parrilla.Mostrar = function() 
{
   Parrilla.HTML = "";
   Parrilla.IniciarCategorias();
   Parrilla.MostrarParrilla();
   $('#parrilla').html(Parrilla.HTML);
};

Parrilla.IniciarCategorias = function(){
    Parrilla.Categorias = [];
    Parrilla.Categorias["4Play CATV HFC"] = {nombre: '4Play CATV HFC', ofertas: [], imprimir: Parrilla.ImprimirTrios};
    Parrilla.Categorias["Trios CATV HFC"] = {nombre: 'Trios CATV HFC', ofertas : [], imprimir: Parrilla.ImprimirTrios};
    Parrilla.Categorias["Trios CATV ADSL"] = {nombre: 'Trios CATV ADSL',  ofertas : [], imprimir: Parrilla.ImprimirTrios};
    Parrilla.Categorias["Trios DTH ADSL"] = {nombre: 'Trios DTH ADSL',ofertas : [], imprimir: Parrilla.ImprimirTrios};
    Parrilla.Categorias["Duos BA ADSL"] = {nombre: 'Duos ADSL', ofertas : [], imprimir: Parrilla.ImprimirDuosBa};
    Parrilla.Categorias["Duos BA HFC"] = {nombre: 'Duos HFC', ofertas : [], imprimir: Parrilla.ImprimirDuosBa};
    Parrilla.Categorias["Duos CATV"] = {nombre: 'Duos CATV', ofertas : [], imprimir: Parrilla.ImprimirDuosTv};
    Parrilla.Categorias["Duos DTH"] = {nombre: 'Duos DTH', ofertas : [], imprimir: Parrilla.ImprimirDuosTv};
    Parrilla.Categorias["Linea"] = {nombre: 'Lineas',ofertas: [], imprimir: Parrilla.ImprimirLineal};
    Parrilla.Categorias["Cable"] = {nombre: 'TV Mono', ofertas: [], imprimir: Parrilla.ImprimirLineal};
    Parrilla.Categorias["Bloque"] = {nombre: 'Bloques de Television',ofertas: [], imprimir: Parrilla.ImprimirLineal};
    Parrilla.Categorias["Deco"] = {nombre: 'Decodificadores', ofertas: [], imprimir: Parrilla.ImprimirLineal};
    Parrilla.Categorias["Multidestino"] = {nombre: 'Multidestino', ofertas: [] , imprimir: Parrilla.ImprimirLineal};
    Parrilla.Categorias["RepetidorSmart"] = {nombre: 'Repetidor Smart', ofertas: [], imprimir: Parrilla.ImprimirLineal};
    Parrilla.Categorias["SeguridadTotal"] = {nombre: 'Seguridad Total', ofertas: [], imprimir: Parrilla.ImprimirLineal};
    
};

Parrilla.ImprimirTrios = function(ofertas){
    Parrilla.ImprimirGrilla(Parrilla.ObtenerInternet(ofertas),Parrilla.ObtenerBloques(ofertas),Parrilla.ObtenerInternetBloques(ofertas));
};

Parrilla.ImprimirDuosBa = function(productos){
    Parrilla.ImprimirFila(Parrilla.ObtenerInternet(productos));
};

Parrilla.ImprimirDuosTv = function(productos){
    Parrilla.ImprimirGrilla(Parrilla.ObtenerCable(productos),Parrilla.ObtenerBloques(productos),Parrilla.ObtenerCableBloques(productos));
};

Parrilla.ImprimirLineal = function(productos){
    Parrilla.ImprimirFila(Parrilla.ObtenerNombre(productos));
};


Parrilla.ObtenerInternet = function(ofertas){
    var resultado = [];
    for(var i = 0; i < ofertas.length; ++i){
        resultado[ofertas[i].producto.Internet.Nombre.replace(' ','')] = ofertas[i];
    }
    return resultado;
};

Parrilla.ObtenerBloques = function(ofertas){
    var resultado = [];
    for(var i = 0; i < ofertas.length; ++i){
        resultado[ofertas[i].producto.PremiumNombre]= ofertas[i];
    }
    return resultado;
};


Parrilla.ObtenerCable = function(ofertas){
    var resultado = [];
    for(var i = 0; i < ofertas.length; ++i){
        resultado[ofertas[i].producto.Cable.Tipo]= ofertas[i];
    }
    return resultado;
};

Parrilla.ObtenerNombre = function(ofertas){
    var resultado = [];
    for(var i = 0; i < ofertas.length; ++i){
        resultado[ofertas[i].producto.Paquete.Nombre]= ofertas[i];
    }
    return resultado;
};

Parrilla.ObtenerInternetBloques = function(ofertas){
    var resultado = [];
    for(var i = 0; i < ofertas.length; ++i){
        resultado[ofertas[i].producto.Internet.Nombre.replace(' ','') + ofertas[i].producto.PremiumNombre]= ofertas[i];
    }
    return resultado;
};

Parrilla.ObtenerCableBloques = function(ofertas){
    var resultado = [];
    for(var i = 0; i < ofertas.length; ++i){
        resultado[ofertas[i].producto.Cable.Tipo+ofertas[i].producto.PremiumNombre]= ofertas[i];
    }
    return resultado;
};

Parrilla.ImprimirGrilla = function(ejeVertical, ejeHorizontal,combinacion){
  
  Parrilla.Cabecera(ejeHorizontal);
  Parrilla.ContenidoGrilla(ejeVertical,ejeHorizontal,combinacion);
  Parrilla.HTML += "</table>";
};

Parrilla.ContenidoGrilla = function (ejeVertical, ejeHorizontal, combinacion) {
    for (var i in ejeVertical) {
        Parrilla.HTML += "<tr>";
        Parrilla.HTML += Parrilla.Etiqueta(i);
        for (var j in ejeHorizontal) {
            var oferta = combinacion[i + j];
            Parrilla.ImprimirProducto(oferta);
        }
        Parrilla.HTML += "</tr>";
    }
};

Parrilla.Etiqueta = function(texto){
    return "<td class='lateral'>" + texto + "</td>";
};


Parrilla.ImprimirProducto = function (oferta) {
    if (oferta !== undefined) {
        var id = oferta.CorrelativoArreglo;
        Parrilla.HTML += "<td "+
                "onclick='javascript:Parrilla.ProductoClick("+oferta.CorrelativoArreglo+")' "+
                "class='"+Parrilla.ObtenerClase(oferta)+"' "+
                "id='paquete_" + id + "'>" +
                oferta.producto.Paquete.Renta + "</td>";
    } else {
        Parrilla.HTML += "<td></td>";
    }
};


Parrilla.ObtenerClase = function (oferta) {
    var clase = "producto ";
    if (1 === oferta.mejor_up) {
        clase += 'mejor_oferta';
    } else if (oferta.salto_arpu >= 0) {
        clase += 'up_arpu';
    } else if (null !== oferta.orden) {
        clase += 'down_arpu_mejores';
    } else {
        clase += 'down_arpu';
    }
    return clase;
};

Parrilla.Cabecera = function(ejeHorizontal){
  Parrilla.HTML += "<table><tr><td></td>";
  for(var i in ejeHorizontal){
    Parrilla.HTML += Parrilla.ItemCabecera(i);
  }
  Parrilla.HTML += "</tr>";
};

Parrilla.ItemCabecera = function(texto){
    return "<td class='cabecera'>"+texto+"</td>";
};

Parrilla.ImprimirFila = function(ejeVertical){
  Parrilla.HTML += "<table>";
  Parrilla.ContenidoFila(ejeVertical);
  Parrilla.HTML += "</table>";
};

Parrilla.ContenidoFila = function (ejeVertical) {
    for (var i in ejeVertical) {
        Parrilla.HTML += "<tr>";
        Parrilla.HTML += Parrilla.Etiqueta(i);
        Parrilla.ImprimirProducto(ejeVertical[i]);
        Parrilla.HTML += "</tr>";
    }
};

Parrilla.ImprimirCategorias = function(){
    for(var i in  Parrilla.Categorias){
        if(Parrilla.Categorias[i].ofertas.length > 0 && Parrilla.Categorias[i].imprimir !== undefined){
            Parrilla.HTML += "<h4>" + i + "</h4>";
            Parrilla.Categorias[i].imprimir(Parrilla.Categorias[i].ofertas);
        }
    }
};

Parrilla.ProductoClick = function(id){
    var oferta = window.ResultadoConsulta.Ofertas[id];
    window.ResultadoConsulta.Opciones['3'] = oferta;
    Comparacion.LlenarInformacionOferta(oferta,3);
    window.scrollTo(0, 0);
};


