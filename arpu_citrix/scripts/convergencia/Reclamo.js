
/* global DatosCliente */

var Reclamo = {};

Reclamo.Consultar = function(){

    var html = "";

        html += '<div style="font-size:15px"><b>Lista de Reclamos:</b></div><br>';

    Reclamo.MostrarModal("Lista de Reclamos",html);
};

Reclamo.MostrarModal = function(titulo,mensaje){
    Reclamo.CrearDialogo("#modal",500,500,150);
    $("#modal").html(mensaje);
    $('#ui-id-1').text(titulo);
    $("#modal").dialog("open");
};

Reclamo.CrearDialogo = function(Dialogo){
    $(Dialogo).dialog({
       autoOpen: false, resizable: false, draggable: false, modal: true,
       width: 700,
       maxWidth: 700,
       maxHeight: 500,
       minHeight: 500
    });
};