var Residencial = {};
Residencial.Detalle_Oferta_Sugerida = {};


Residencial.Detalle_Oferta_Sugerida.calcular_detalle_movimiento = function(data) 
{
   var resultado = "<hr><div class='detalleOferta_Titulo'>Movimiento de componentes</div>";
   
   for(var componente in data.movimiento)
   {
      if(data.movimiento[componente] != 'No Evalua Componente')
      {
         resultado += data.movimiento[componente]+ " "+componente+" <br />"
      }
   }

   return resultado;
};


Residencial.Detalle_Oferta_Sugerida.ObtenerDatosBasicoOferta = function(Oferta,TipoOperacion)
{
   
   var resultado = "<div class='detalleOferta_Titulo'>" + TipoOperacion +"</div>"+
           "<table>" +
              '<tr><th>Nombre:</th><td>' + Oferta.producto.Paquete.Nombre + " </td> </tr>" +
              '<tr><th>Precio:</th><td>S/.' + Oferta.producto.Paquete.Renta + "</td> </tr>";
   resultado += "<tr><th>Salto: </th><td>S/." + Oferta.salto_arpu +"</td> </tr>";
      return resultado;
}


Residencial.Detalle_Oferta_Sugerida.MostrarTecnologia = function(data)
{
   var contenido = "";
   var at = ""
   if (data.requiereActuacionTecnica === 1) 
   {
      at="<br>(Actuaci&oacute;n T&eacute;cnica)";
   }
   
   if(data.producto.tecnologia != null)
   {
      contenido = "<div id='"+data.producto.tecnologia+"'>"+
              data.producto.tecnologia+at+
      "</div>";
   }
   return contenido;
}


Residencial.Detalle_Oferta_Sugerida.MostrarDescuento = function(descuento)
{
   var contenido = "";
   if(descuento == null)
      return contenido;
   
   if(descuento.Ps != null)
   {
      contenido += '<tr><th>PS de Descuento: </th><td>' + descuento.Ps + "</td></tr>";
   }
   
   if (descuento.glosa_descriptiva != null) 
   {
      contenido += '<tr><th>Adicional: </th><td>' + descuento.glosa_descriptiva + "</td></tr>";
   }
   return contenido;
};

Residencial.Detalle_Oferta_Sugerida.MostrarCartillaDeRegistro = function(Registro,RegistroDecos)
{
   var PsRegistro = "";
   
    
   for (var i in Registro) 
   {
      PsRegistro += '<tr>'
               +'<td class="Registro"><div class="Registro_Ps">'+Registro[i].Ps.Ps+'</div></td>'
               +'<td class="Registro">'+Registro[i].Ps.Nombre+'</td>'
               +'<td class="Registro">'+Registro[i].TipoOperacionComercial+'</td>'
               +'<td class="Registro">'+Registro[i].Caracteristica+'</td>'
            +'</tr>';
   }
   
   $ofertaPrincipal = '<table cellspacing="0px" cellpadding="2px">'+
       '<tr class="ProductoSVA">'
           +'<td class="nivel1"> PS</td>'
           +'<td class="nivel1"> Nombre</td>'
           +'<td class="nivel1"> Operacion Comercial</td>'
           +'<td class="nivel1"> Caracteristica</td>'
      +'</tr>'+PsRegistro+'</table>'+'<br>';
      
    $opc = 1;  
    if(RegistroDecos.length > 0)
    {
        for(var i in RegistroDecos)
        {
            PsRegistro = "<b>Opcion "+ $opc +":</b>";
            for(var j in RegistroDecos[i])
            {
                PsRegistro += '<tr>'
                    +'<td class="Registro"><div class="Registro_Ps">'+RegistroDecos[i][j].Ps.Ps+'</div></td>'
                    +'<td class="Registro">'+RegistroDecos[i][j].Ps.Nombre+'</td>'
                    +'<td class="Registro">'+RegistroDecos[i][j].TipoOperacionComercial+'</td>'
                    +'<td class="Registro">'+RegistroDecos[i][j].Caracteristica+'</td>'
                 +'</tr>';
            }
            
            $ofertaPrincipal += '<table cellspacing="0px" cellpadding="2px">'+
            '<tr class="ProductoSVA">'
                +'<td class="nivel1"> PS</td>'
                +'<td class="nivel1"> Nombre</td>'
                +'<td class="nivel1"> Operacion Comercial</td>'
                +'<td class="nivel1"> Caracteristica</td>'
           +'</tr>'+PsRegistro+'</table>'+'<br>';
           $opc += 1;
        }
    }
      

    
    return $ofertaPrincipal;
      
};

Residencial.Detalle_Oferta_Sugerida.mostrar_detalle = function(Oferta) {
   var resultado = Residencial.Detalle_Oferta_Sugerida.MostrarTecnologia(Oferta);

   var tipoOperacion = Oferta.tipoOperacionComercial + " " + (Oferta.salto_arpu >= 0 ? 'Up ': 'Down');
   resultado += Residencial.Detalle_Oferta_Sugerida.ObtenerDatosBasicoOferta(Oferta,tipoOperacion);
   resultado += Residencial.Detalle_Oferta_Sugerida.MostrarDescuento(Oferta.descuento);
   resultado += "</table>";
   Util.registroUsoAsesor(Oferta.producto.Paquete.Ps, "Click en producto");
   resultado +=  Residencial.Detalle_Oferta_Sugerida.calcular_detalle_movimiento(Oferta) + "<hr>";
   resultado += Residencial.Detalle_Oferta_Sugerida.MostrarCartillaDeRegistro(Oferta.Registro,Oferta.RegistroDeco);
   
   $('#oferta').html(resultado);
   $('#oferta').show();
   window.oferta_marcada = Oferta;
   
   
   
}

