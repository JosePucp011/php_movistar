var Componente = {};
Componente.Internet = 'Internet';
Componente.Cable = 'Cable';
Componente.Linea = 'Linea';
Componente.Plan = 'Plan';
Componente.Premium = 'Premium';

var Parrilla = {};

Parrilla.Debug = false;

Parrilla.Log = function(texto)
{
    if(Parrilla.Debug)
    {
        console.log(texto);
    }
};


Parrilla.Mostrar = function(ResultadoConsulta) 
{
   Parrilla.MostrarParrilla(ResultadoConsulta);
   $('td.producto').click(Parrilla.ProductoClick);
   Parrilla.ColorearOfertas(ResultadoConsulta.Ofertas);
   Parrilla.LimpiarProductos();
};

Parrilla.HTML = {};


Parrilla.MostrarParrilla = function(RespuestaConsulta) {
   window.cliente_datos = RespuestaConsulta;
   window.ofertas = RespuestaConsulta.Ofertas;
   
   Parrilla.ObtenerPremiums(RespuestaConsulta.Ofertas);
   Parrilla.ObtenerCategoriasPaquete(RespuestaConsulta.Ofertas);
   
   Parrilla.Parrilla = ""; 
   Parrilla.Cabecera();
   Parrilla.ImprimirCategorias();
   Parrilla.Dibujar();
};


Parrilla.Dibujar = function()
{
    $('#parrilla').html(Parrilla.Parrilla);
};




Parrilla.ImprimirCategorias = function()
{
   for (var i = 0; i < Parrilla.PaqueteCategoria.length; ++i) 
   {
      Parrilla.Parrilla += Parrilla.HTML.PaqueteCategoria(Parrilla.PaqueteCategoria[i]);
   }
};


Parrilla.ColorearOfertas = function(Ofertas){
    for (var i in Ofertas){
        
       var paquete = $('#paquete_' + Ofertas[i].producto.Identificador);
       {
         if (1 === Ofertas[i].mejor_up){
            paquete.addClass('mejor_oferta');
         } else if (Ofertas[i].salto_arpu >= 0){
             paquete.addClass('up_arpu');
         } else if (null !== Ofertas[i].orden){
            paquete.addClass('down_arpu_mejores');
         } else {
            paquete.addClass('down_arpu');
         }

          if (Ofertas[i].orden !== null) 
          {
             $('#orden_down_' + Ofertas[i].producto.Identificador).text('(' + Ofertas[i].orden.join(',') + ')');
          }
       }
    }

};




Parrilla.ObtenerLineas = function(Ofertas)
{
   var LineaTipo_Linea = {};
   
   for (var oferta = 0; oferta < Ofertas.length; ++oferta) 
   {
    var producto = Ofertas[oferta].producto;
    if(!Producto.esPaquete(producto))
         continue;
     
    var tipoLinea = Producto.ObtenerTipoLinea(producto);
     
    if(tipoLinea.Nombre !== null){
      LineaTipo_Linea[Producto.ObtenerLinea(producto)] = tipoLinea;
     }
   }

   return Parrilla.values(LineaTipo_Linea);
};


Parrilla.ObtenerPremiums = function(Ofertas)
{
   var PremiumTipo_Premium = {};
   
   for (var oferta = 0; oferta < Ofertas.length; ++oferta) 
   {
      var producto = Ofertas[oferta].producto;
      if(!Producto.esPaquete(producto))
         continue;
     
     var tipoPremium = Producto.ObtenerTipoPremium(producto);
     
     if(tipoPremium.Nombre !== null && tipoPremium.Nombre !== 'Estelar' && producto.Linea.Presente > 0 ){
      PremiumTipo_Premium[Producto.ObtenerPremiumFiltrado(producto)] = tipoPremium;
     }
   }
   Parrilla.TiposDePremium = Parrilla.values(PremiumTipo_Premium);
};



Parrilla.CrearPaqueteCategoria = function(categoria)
{
   return {
            paquete_categoria: categoria.nombre,
            tiposDeCableTmp: {},
            tiposDeCable: [],
            tiposDeSpeedyTmp: {},
            tiposDeSpeedy: [],
            tiposDeLinea: [],
            orden: categoria.orden,
            productos: []
         };
};

Parrilla.ObtenerCategoriasPaquete = function(Ofertas)
{
   var paqueteCategoriaTmp = {};
   var paqueteCategoria = [];
   
    for(var oferta = 0; oferta < Ofertas.length; ++oferta)
   {
      var producto = Ofertas[oferta].producto;
      
      if(Ofertas[oferta].TieneUltraWifi === true)
      {
          producto.Paquete.Renta += 12.90;
          producto.Paquete.Renta = Math.round(producto.Paquete.Renta*100)/100;
      }
      
      
      var categoria = Producto.ObtenerPaqueteCategoria(producto);      
      
      if(paqueteCategoriaTmp[categoria.nombre] === undefined)
      {
         paqueteCategoriaTmp[categoria.nombre] = Parrilla.CrearPaqueteCategoria(categoria);
      }
      
      paqueteCategoriaTmp[categoria.nombre].productos.push(producto);
      if(!producto.Paquete.EsSVA)
      {
         var cable = producto.Componentes[Componente.Cable];
         if(cable.Presente && cable.Paquetizado)
         {
            paqueteCategoriaTmp[categoria.nombre].tiposDeCableTmp[cable.Tipo] = 
            {
               Nombre: cable.Tipo,
               Nivel: cable.Nivel
            };
         }

         var internet = producto.Componentes[Componente.Internet];
         if(internet.Presente && internet.Paquetizado)
         {
            paqueteCategoriaTmp[categoria.nombre].tiposDeSpeedyTmp[Producto.ObtenerInternetUnico(producto)] = 
            {
               Nivel: internet.Velocidad,
               Nombre: Producto.ObtenerInternetUnico(producto)
            };
         }
      }
   }
   
   for (tipoDePaquete in paqueteCategoriaTmp)
   {
      paqueteCategoriaTmp[tipoDePaquete].tiposDeCable = 
              Parrilla.values(paqueteCategoriaTmp[tipoDePaquete].tiposDeCableTmp);
      
       paqueteCategoriaTmp[tipoDePaquete].tiposDeSpeedy =
               Parrilla.values(paqueteCategoriaTmp[tipoDePaquete].tiposDeSpeedyTmp);

      delete paqueteCategoriaTmp.tiposDeCableTmp;
      delete paqueteCategoriaTmp.tiposDeSpeedyTmp;

      paqueteCategoria.push(paqueteCategoriaTmp[tipoDePaquete]);
   }

   paqueteCategoria.sort(function(categoria1, categoria2)
   {
      return categoria1.orden - categoria2.orden;
   });
   
   Parrilla.PaqueteCategoria = paqueteCategoria;
};

Parrilla.Cabecera = function()
{
   var tiposDePremium = Parrilla.TiposDePremium;
   var parrilla = 
           "<div>" +
           "<table>" +
           "<tr>" + "<td><div class='cabecera'>&nbsp;</div></td>" + "";


   for(var linea = 0; linea < tiposDePremium.length; ++linea)
   {
      var cambio = '';
      
      cambio = tiposDePremium[linea].Nombre.replace("MC+HBO+HD","TOTAL","g");
      cambio = cambio.replace("MC","FOX","g");
      
      parrilla += "<td class='nivel1'><div class='nivel4'>" + cambio + "</div></td>";
   }

   parrilla += "</tr></table></div>";
   Parrilla.Parrilla += parrilla;
};


Parrilla.HTML.Producto = function(producto)
{
    
    
   return  "<td class='producto' draggable='true' id='paquete_" + producto.Identificador + "'>" +
                 "<div  class='producto'  id='div_paquete_" + producto.Identificador + "' >" +
                 "<div  id='orden_down_" + producto.Identificador + "' class='orden_down'>" +
                 "</div>" +
                 "<div class='precio' >" + producto.Paquete.Renta + "</div>" +
                 "</div>" +
                 "</td>";
};

Parrilla.EncontrarProducto = function(productos,Linea,Cable,Internet)
{
    for (var i = 0; i < productos.length; ++i) 
   {
      var paquete = productos[i];
      Parrilla.Log(paquete.Paquete.Nombre);
      Parrilla.Log(Producto.ObtenerPremiumFiltrado(paquete));
      Parrilla.Log(Linea);

      if(Producto.ObtenerInternetUnico(paquete) ===  Internet &&
         Producto.ObtenerCable(paquete) === Cable &&
         Producto.ObtenerPremiumFiltrado(paquete) === Linea)
      {
         return paquete;
      }
   }
   return null;
};

Parrilla.HTML.PaqueteCategoria = function(paqueteCategoria)
{
   var tiposDePremium = Parrilla.TiposDePremium;
   var parrilla =
              "<h4>" + paqueteCategoria.paquete_categoria + "</h4>" +
              "<table cellpadding='0' cellspacing='0' class='parrilla_productos' >";
   
   if (paqueteCategoria.tiposDeSpeedy.length > 0 &&  paqueteCategoria.tiposDeCable.length > 0) 
   {  
      parrilla += Parrilla.HTML.Trios(paqueteCategoria,tiposDePremium);
   }
   else if(paqueteCategoria.paquete_categoria === "Internet Naked")
   {
       parrilla += Parrilla.HTML.ProductoMono(paqueteCategoria);
   }
   else if (paqueteCategoria.tiposDeSpeedy.length > 0) 
   {  
      parrilla += Parrilla.HTML.DuosBa(paqueteCategoria);
   } 
   else if (paqueteCategoria.tiposDeCable.length > 0) { 
       parrilla += Parrilla.HTML.DuosTv(paqueteCategoria,tiposDePremium);
   } 
   else
   {
      parrilla += Parrilla.HTML.ProductoMono(paqueteCategoria);
   }

   parrilla +=  "</table>";
   return parrilla;
};

Parrilla.HTML.Trios = function(paqueteCategoria,tiposDeLinea)
{
   var speedy_actual_index = 0;
   var cable_actual_index = 0;
   var numeroDeSpeedys = paqueteCategoria.tiposDeSpeedy.length;
   var numeroDeFilas = paqueteCategoria.tiposDeSpeedy.length * paqueteCategoria.tiposDeCable.length;
   
   var parrilla = "";
   for (var i = 0; i < numeroDeFilas; ++i) 
   {
      if (0 === speedy_actual_index) 
      {
         Cable = paqueteCategoria.tiposDeCable[cable_actual_index++].Nombre;
      }
      
      Internet = paqueteCategoria.tiposDeSpeedy[speedy_actual_index++].Nombre;
      
      if (speedy_actual_index === numeroDeSpeedys)
      {
         speedy_actual_index = 0;
      }
      
      parrilla += Parrilla.HTML.ImprimirFila(
              Internet,
              Parrilla.ImprimirPaquetes(paqueteCategoria, tiposDeLinea, 
              Cable, 
              Internet)
      );
   };
   return parrilla;
   
};

Parrilla.ImprimirPaquetes = function(paqueteCategoria, tiposDeLinea, Cable, Internet) 
{
   var existeAlgunProducto = false;
   var parrilla = "";
   
    for (var lineaTipo = 0; lineaTipo < tiposDeLinea.length; ++lineaTipo) 
   {
      var productoAImprimir = Parrilla.EncontrarProducto(
              paqueteCategoria.productos,
               tiposDeLinea[lineaTipo].Nombre,
               Cable,
               Internet);

      if(productoAImprimir !== null)
      {
         parrilla += Parrilla.HTML.Producto(productoAImprimir);
         existeAlgunProducto = true;
      }else{
         parrilla += "<td class='vacio'><div class='vacio'></div></td>";
      }
   }
   if(existeAlgunProducto)
   {
      return parrilla;
   }
   else
   {
       Parrilla.Log("No encuentra ningun producto");
      return "";
   }
};

Parrilla.HTML.ImprimirFila = function(Etiqueta,Contenido)
{
   var parrilla = "<tr>";
   parrilla += "<td class='nivel3'><div class='nivel3'>" + Etiqueta + "</div></td>";
   parrilla += Contenido;
   parrilla += "</tr>";
   return parrilla;
};

Parrilla.HTML.DuosBa = function(paqueteCategoria){
    
    var numeroDeFilas = Parrilla.HTML.NumeroDeFilas(paqueteCategoria);
    var parrilla = "";
    
    for (var i = 0; i < numeroDeFilas; ++i)
    {
        var producto = paqueteCategoria.productos[i];
        
        if(producto.Componentes[Componente.Linea].Nombre){
            parrilla += Parrilla.HTML.DuosBaFila(i,paqueteCategoria);
        }
    }
    return parrilla;
    
};

Parrilla.HTML.DuosBaFila = function(i,paqueteCategoria){
    var Internet = paqueteCategoria.tiposDeSpeedy[i].Nombre;
    var lineas = new Array();
    lineas[0] = new Object();
    lineas[0].Nombre = "-";
    
    var filaDePaquetes = Parrilla.ImprimirPaquetes(paqueteCategoria, lineas, null, Internet);
    
    return Parrilla.HTML.ImprimirFila(Internet,filaDePaquetes);
};




Parrilla.HTML.DuosTv = function(paqueteCategoria,tiposDeLinea){
    
    var numeroDeFilas = Parrilla.HTML.NumeroDeFilas(paqueteCategoria);
    var parrilla = "";
    for (var i = 0; i < numeroDeFilas; ++i){
        var producto = paqueteCategoria.productos[i];
        var Cable = paqueteCategoria.tiposDeCable[i].Nombre;

        if(producto.Componentes[Componente.Linea].Nombre){
           parrilla += Parrilla.HTML.ImprimirFila(Cable,Parrilla.ImprimirPaquetes(paqueteCategoria, tiposDeLinea, Cable, "-")
              );
        }else{
           parrilla += Parrilla.HTML.ImprimirFila(Cable,Parrilla.HTML.Producto(producto));
        }
     }
     return parrilla;
};

Parrilla.HTML.ProductoMono = function(paqueteCategoria){
    
    var parrilla = "";
    for (var i = 0; i < paqueteCategoria.productos.length; ++i) 
    {
       var producto = paqueteCategoria.productos[i];
       if(producto.Linea.Presente)
       {
          parrilla += Parrilla.HTML.ImprimirFila(
               producto.Linea.Nombre,
               Parrilla.HTML.Producto(producto)
               );
       }
       else if(producto.Paquete.Tipo === 'Bloque')
       {
         if(producto.Paquete.Tipo === 'Bloque' && producto.Cable.Premium.Nombre=== 'MC'){ // FOXMAN 
            parrilla += Parrilla.HTML.ImprimirFila(
               'FOX',
               Parrilla.HTML.Producto(producto)
               );
         }else{
            parrilla += Parrilla.HTML.ImprimirFila(
               producto.Cable.Premium.Nombre,
               Parrilla.HTML.Producto(producto)
               );
         }
         
       }
       else if(producto.Paquete.EsSVA)
       {
          parrilla += Parrilla.HTML.ImprimirFila(
          producto.Paquete.Nombre,
          Parrilla.HTML.Producto(producto)
          );
       } else {
           parrilla += Parrilla.HTML.ImprimirFila(
          producto.Paquete.Nombre,
          Parrilla.HTML.Producto(producto)
          );
       }
    }
    return parrilla;
};

Parrilla.HTML.NumeroDeFilas = function(paqueteCategoria)
{
   var numeroDeSpeedys = paqueteCategoria.tiposDeSpeedy.length;
   var numeroDeCables = paqueteCategoria.tiposDeCable.length;

   if (numeroDeSpeedys === 0)
      numeroDeSpeedys = 1;
   if (numeroDeCables === 0)
      numeroDeCables = 1;

   return numeroDeSpeedys * numeroDeCables;
};


Parrilla.LimpiarProductos = function()
{
   $('div.producto').css('border', '3px solid transparent');
   if ($.browser.msie && $.browser.version === "6.0") 
   {
      $('div.producto').css('border-color', 'pink');
      $('div.producto').css('filter', 'chroma(color=pink)');
   }
};

Parrilla.ProductoClick = function() 
{
   if(window.ofertas === null)
      return;

   var id = parseInt($(this).attr('id').replace('paquete_', ''));
   
   Parrilla.LimpiarProductos();
   for (var i = 0; i < window.ofertas.length; ++i) 
   {
      var Oferta = window.ofertas[i];
      if (Oferta.producto.Identificador !== id)
         continue;

      $('#div_paquete_' + id).css('border', '3px solid black');

      Residencial.Detalle_Oferta_Sugerida.mostrar_detalle(Oferta);
      Comparacion.LlenarInformacionOferta(Oferta,3, window.cliente);
     window.scrollTo(0, 0);
   }

};

Parrilla.SVAClick = function() 
{
   if(window.ofertas === null)
      return;

   var id = parseInt($(this).attr('id').replace('OfertaSVALeft_', ''));
   
   Parrilla.LimpiarProductos();
   for (var i = 0; i < window.ofertas.length; ++i) 
   {
      var Oferta = window.ofertas[i];
      if (Oferta.producto.Identificador !== id)
         continue;

      $('#div_paquete_' + id).css('border', '3px solid black');

      Residencial.Detalle_Oferta_Sugerida.mostrar_detalle(Oferta);
      Comparacion.LlenarInformacionOferta(Oferta,3, window.cliente);
   }

};

Parrilla.values = function(object) {
  var values = [];
  for(var property in object) {
    values.push(object[property]);
  }
  
  values.sort(function(item1, item2){
       return -item1.Nivel + item2.Nivel;
  });
  
  return values;
};

var Producto = {};
Producto.esPaquete = function(producto)
{
   return (producto.Componentes[Componente.Internet].Presente || 
        producto.Componentes[Componente.Cable].Presente) && producto.Cable.EsSVA !== "1";
};

Producto.ObtenerLinea = function(producto)
{
   if(producto.Componentes[Componente.Plan].Presente && producto.Componentes[Componente.Plan].Paquetizado)
   {
      return producto.Componentes[Componente.Linea].Nombre + '+' +
         producto.Componentes[Componente.Plan].Nombre;
   }
   else
   {
      return producto.Componentes[Componente.Linea].Nombre;
   }
};

Producto.ObtenerPaqueteCategoria = function(producto)
{
   if(producto.Paquete.Tipo === "Bloque")
   {
      return {
         nombre: ' SVA: Bloques de Canales',
         orden: 50000
      };
   }
   else if(producto.Paquete.Tipo === "Deco")
   {
      return {
         nombre: 'SVA: Decos',
         orden: 51000
      };
   }
   else if(producto.Paquete.Tipo === "Multidestino")
   {
      return {
         nombre: 'SVA: Paquetes Multidestino',
         orden: 52000
      };
   }
   else if(producto.Paquete.Tipo === "Internet")
   {
      return {
         nombre: 'SVA: Internet',
         orden: 53000
      };
   }
   else if("Linea" === producto.Paquete.Tipo)
   {
      return {
         nombre: 'Lineas',
         orden: 40000
      };
   }
   else if( "Cable" === producto.Paquete.Tipo)
   {
      return {
         nombre: 'TV Mono',
         orden: 30000
      };
   }
   else if( "Naked" === producto.Paquete.Tipo)
   {
      return {
         nombre: 'Internet Naked',
         orden: 20000
      };
   }
   else if(producto.Internet.Presente && producto.Cable.Presente && producto.Linea.Presente)
   {
      
      return {
         nombre: 'Trio ' 
              + producto.Cable.Categoria + ' '
              + producto.Cable.Tipo + ' '
              + producto.Internet.Tecnologia,
         orden: 1000 - producto.Cable.Nivel * 10
                 - (producto.Cable.Categoria === 'CATV' ? 100: 0)
                 - (producto.Internet.Tecnologia === 'HFC' ? 1: 0)
      };
   }
   else if(producto.Internet.Presente && producto.Cable.Presente) {
      return {
         nombre: 'Duo Internet Tv',
         orden: 1500
      };
   }
   else if(producto.Internet.Presente)
   {
      return {
         nombre: 'Duo Internet ' 
              + producto.Internet.Tecnologia,
         orden: 2000
      };
   }
   else if(producto.Cable.Presente)
   {
      return {
         nombre: 'Duo '
         + producto.Cable.Categoria + ' '
         + producto.Cable.Tipo,
         orden: 3000
      };
   }
   
   
};


Producto.ObtenerInternet = function(producto)
{

    if(producto.Componentes[Componente.Internet].Presente && !producto.Componentes[Componente.Internet].Paquetizado){
        return null;
    }
    
   if(producto.Componentes[Componente.Cable].Premium.Presente && 
           producto.Componentes[Componente.Cable].Premium.Paquetizado)
   {
      var PremiumNombre = Producto.ObtenerPremium(producto);
      

      return producto.Componentes[Componente.Internet].Nombre  + " " + PremiumNombre;
      
   }
   
   
   return producto.Componentes[Componente.Internet].Nombre;
};

Producto.ObtenerInternetUnico = function(producto)
{
    if(producto.Componentes[Componente.Internet].Presente && !producto.Componentes[Componente.Internet].Paquetizado){
        return null;
    }
   
   
   return producto.Componentes[Componente.Internet].Nombre;
};




Producto.ObtenerCable = function(producto)
{
    if(producto.Componentes[Componente.Cable].Tipo === null || !producto.Componentes[Componente.Cable].Paquetizado){
        return null;
    }
    else {
        return producto.Componentes[Componente.Cable].Tipo;
    }
        
};

Producto.ObtenerTipoLinea = function(producto)
{
   return {
         Nombre: Producto.ObtenerLinea(producto),
         Nivel: producto.Componentes[Componente.Linea].Nivel + 
         (producto.Componentes[Componente.Plan].Presente ? 
                  producto.Componentes[Componente.Plan].Nivel : 0)/100.0
   };
};
Producto.ObtenerTipoPremium = function(producto)
{
    
    var nivel = 0;
    
   if(Producto.ObtenerPremium(producto)=== '-'){nivel = 8;}
   if(Producto.ObtenerPremium(producto)=== 'HD'){nivel = 7;}
   if(Producto.ObtenerPremium(producto)=== 'HBO'){nivel = 6;}
   if(Producto.ObtenerPremium(producto)=== 'MC'){nivel = 5;}
   if(Producto.ObtenerPremium(producto)=== 'HD+HBO'){nivel = 4;}
   if(Producto.ObtenerPremium(producto)=== 'HD+MC'){nivel = 3;}
   if(Producto.ObtenerPremium(producto)=== 'HBO+MC'){nivel = 2;}
   if(Producto.ObtenerPremium(producto)=== 'HD+HBO+MC'){nivel = 1;}
   
   if(Producto.ObtenerPremium(producto) != 'UFC' 
           && Producto.ObtenerPremium(producto) != 'HP'
           && Producto.ObtenerPremium(producto) != 'GP' )
   {
       return {
        Nombre: Producto.ObtenerPremium(producto),
        Nivel : nivel
   };
   }
   else
   {
        return {
        Nombre: "-",
        Nivel : 8
   };
   }
};

Producto.ObtenerPremium = function(producto){
   var PremiumNombre = producto.Componentes[Componente.Cable].Premium.Nombre;
   var cambio = "";
   if(PremiumNombre === null || PremiumNombre === ""){
      PremiumNombre = "-";
   }
   cambio = PremiumNombre.replace("|HBO","+HBO","g");
   cambio = cambio.replace("|HD","+HD","g");
   cambio = cambio.replace("|MC","+MC","g");
   cambio = cambio.replace("|DVR","+DVR","g");
   
   return cambio;
};
Producto.ObtenerPremiumFiltrado = function(producto){
    
   var PremiumNombre = producto.Componentes[Componente.Cable].Premium.Nombre;
   var cambio = "";
   if(PremiumNombre === null || PremiumNombre === ""){
      PremiumNombre = "-";
   }
   if(PremiumNombre === "UFC" || PremiumNombre === "HP" || PremiumNombre === "GP" ){
      PremiumNombre = "-";
   }
   cambio = PremiumNombre.replace("|HBO","+HBO","g");
   cambio = cambio.replace("|HD","+HD","g");
   cambio = cambio.replace("|MC","+MC","g");
   cambio = cambio.replace("|DVR","+DVR","g");
   
   return cambio;
};

