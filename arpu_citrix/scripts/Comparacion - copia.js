var Comparacion = {};

Comparacion.Mostrar = function(Resultado)
{
   Comparacion.CrearEstructuraHTML(Resultado);
   Comparacion.LlenarInformacionCliente(Resultado);
   Comparacion.LlenarOpciones(Resultado);
   Comparacion.ProgramarBotones();
   Comparacion.MostrarMensajeParaTrioSinDuoInterneTv();
};



Comparacion.LlenarOpciones = function(Resultado)
{
   for(var i=1, j = 1; i<5 && j <= 3; i++)
   {
      if(Resultado.Opciones[i] !== undefined)
      {
         Comparacion.LlenarInformacionOferta(Resultado.Opciones[i], j);
         j++;
      }
   }
};

Util.llenarCasillaComponenteSinMovimiento = function(descripcion)
{
    return descripcion.join("<br>");
};


Util.llenarCasillaComponente = function(componenteActual,descripcion, renta,movimiento)
{
    var descripcionCompleta = descripcion.join("<br>");
    var response = "<div class='div_contenedor'><table class='div_contenedor' border-collapse='collapse'><tr>";
    var claseDescripcion = "anchoSoloDescripcion";
    
    if (movimiento !== undefined)
    {
        response += "<td class='anchoMovimiento'>" + Comparacion.obtenerSimboloMovimientoComponente(movimiento) + "</td>";
        claseDescripcion = "anchoDescripcion";
    }

    response += "<td class='" + claseDescripcion + "'>" + descripcionCompleta + "</td>";
    response += "<td class='datos_renta datos_renta_"+componenteActual+"'>";
    
    if (renta !== "")
    {
        response += renta;
    }
    else
    {
       response += "&nbsp;"
    }
    
    response += "</td>";
    response += "</tr>";
    response += "</table></div>";
    return response;
};

Util.llenarCasillaComponentePremium = function(componenteActual,Freeview,descripcion, renta,movimiento)
{
    var descripcionCompleta = descripcion.join("<br>");
    var response = "<div class='div_contenedor'><table class='div_contenedor' border-collapse='collapse'><tr>";
    var claseDescripcion = "anchoSoloDescripcion";
    
    if (movimiento !== undefined)
    {
        response += "<td class='anchoMovimiento'>" + Comparacion.obtenerSimboloMovimientoComponente(movimiento) + "</td>";
        claseDescripcion = "anchoDescripcion";
    }

    response += "<td class='" + claseDescripcion + "'>" + descripcionCompleta;
   
    response += "</td><td class='datos_renta datos_renta_"+componenteActual+"'>";
    
    if (renta !== "")
    {
        response += renta;
    }
    else
    {
       response += "&nbsp;"
    }
    
    response += "</td>";
    response += "</tr>";
    response += "</table></div>";
    return response;
};



Comparacion.ProgramarBotones = function()
{  
   $(".BotonRegistro").buttonset();
   $(".BotonSpeech").buttonset();
   
   $(".BotonRegistro").click(function(){
      var id = $(this).attr('id').replace("BotonRegistro","");
      Comparacion.MostrarDatosRegistro(id);
   });
   
   $(".linkRetenciones").click(function(){
      var id = $(this).attr('id').replace("descuentoRetenciones_","");
      Comparacion.MostrarDescuentoRetenciones(id);
   });
   
   $(".linkHerramientas").click(function(){
      var id = $(this).attr('id').replace("herramientaRetenciones_","");
      Comparacion.MostrarHerramientaRetenciones(id);
   });
   
    $(".linkPlanRegalos").click(function(){
      var id = $(this).attr('id').replace("descuentoRetenciones_","");
      Comparacion.MostrarFreeview(id);
   });

    $(".linkVolcadoReclamos").click(function(){
      var id = $(this).attr('id').replace("descuentoRetenciones_","");
      Comparacion.MostrarVolcadoReclamos(id);
   });    
    
    $(".linkIncrementoPrecio").click(function(){
      var id = $(this).attr('id').replace("descuentoRetenciones_","");
      Comparacion.MostrarIncrementoPrecio(id);
   });
   
    $(".linkCiudadSitiada").click(function(){
      var id = $(this).attr('id').replace("descuentoRetenciones_","");
      Comparacion.MostrarCiudadSitiada(id);
   });
   
};

Comparacion.MostrarVolcadoReclamos = function(Posicion)
{
   if(window.Opciones[Posicion] !== undefined)
   {
        Reclamo.Consultar(window.cliente);
        $("#contenedorLinkRetencionesXMigraDown").show();
    }
};


Comparacion.MostrarFreeview = function(Posicion)
{
   if(window.Opciones[Posicion] !== undefined)
   {
        var Opcion = window.Opciones[Posicion].producto;
        
        if(Opcion.Componentes['Linea'].Presente)
        {
             Freeview.Consultar(window.cliente);
             $("#contenedorLinkRetencionesXMigraDown").show();
        }
        else
        {
           alert('No hay Plan de Regalos');
        }
    }
};

Comparacion.MostrarIncrementoPrecio = function(Posicion)
{
   if(window.Opciones[Posicion] !== undefined)
   {
        var Opcion = window.Opciones[Posicion].producto;
        
        if(Opcion.Componentes['Linea'].Presente || Opcion.Componentes['Cable'].Presente)
        {
             IncrementoPrecio.Consultar(window.cliente);
             $("#contenedorLinkRetencionesXMigraDown").show();
        }
        else
        {
           alert('No hay Incremento de Precio');
        }
    }
};

Comparacion.MostrarCiudadSitiada = function(Posicion)
{
   if(window.Opciones[Posicion] !== undefined)
   {
        var Opcion = window.Opciones[Posicion].producto;
        
        if(Opcion.Componentes['Linea'].Presente)
        {
             CiudadSitiada.Consultar(window.cliente,window.ofertas);
             $("#contenedorLinkRetencionesXMigraDown").show();
        }
        else
        {
           alert('No hay Cliente en ciudad Sitiada');
        }
    }
};

Comparacion.MostrarDescuentoRetenciones = function(Posicion)
{
   if(window.Opciones[Posicion] !== undefined)
   {
        var Opcion = window.Opciones[Posicion].producto;
        
        if(Opcion.Componentes['Linea'].Presente  || Opcion.Componentes['Internet'].Presente || Opcion.Componentes['Cable'].Presente)
        {
             DescuentosRetenciones.Consultar(window.cliente);
             $("#contenedorLinkRetencionesXMigraDown").show();
        }
        else
        {
           alert('No hay descuentos validos para este caso');
        }
    }
};

Comparacion.MostrarHerramientaRetenciones = function(Posicion)
{
   var Pos = parseInt(Posicion) ;
   
   if(window.Opciones[Pos] !== undefined)
   {
        var Opcion = window.Opciones[Pos].producto;
        if(Opcion.Componentes['Linea'].Presente || Opcion.Componentes['Internet'].Presente || Opcion.Componentes['Cable'].Presente)
        {
           HerramientasRetenciones.Consultar(window.cliente);  
           $("#contenedorLinkRetencionesXMigraDown").show();
        }
        else
        {
           alert('No hay herramientas validas para este caso');
        }
    }
};

   
Comparacion.ComponenteActual = "";   
   
Comparacion.LlenarInformacionCliente = function(Resultado){
   
   var Cliente = Resultado.Cliente;
   window.Opciones[0] = {producto : Resultado.Cliente };
   $("#cliente_telefono").html(Cliente.Telefono);
   $("#cliente_nombre").html(Cliente.Nombre);
   $("#DatosClientePaquete").html(Comparacion.PaqueteNombre(Cliente.Paquete));
   $("#DatosClienteDescuento").html(Comparacion.LlenarInformacionDescuentos(0,Array()));
   
   var componentes = Array("Linea","Plan","Cable");
   for(var i in componentes)
   {
      $("#DatosCliente"+componentes[i]).html(Comparacion.Nombre(componentes[i],Cliente.Componentes[componentes[i]]));
   }
   
   $("#DatosClientePremium").html(Comparacion.PremiumNombreCliente(Cliente.Componentes["Cable"].Premium, Cliente.Freeview ));
   $("#DatosClienteInternet").html(Comparacion.InternetNombre(Cliente.Componentes["Internet"]));
   $("#DatosClienteDecos").html(Comparacion.DecosCliente(Cliente.Componentes["Cable"].Decos,Cliente.Cable.CantidadDecosNetflix));
   $("#DatosClienteModems").html(Comparacion.Modems(Cliente));
   
   $("#DatosRenta").html(Cliente.Paquete.Renta !== null ? Cliente.Paquete.Renta : "");
   $("#DatosClienteRenta").html( "S/. " + Cliente.RentaTotal);
   $("#DatosClienteAdicionales").html( "S/." + Cliente.RentasAdicionales);
   $("#DatosClienteTotal").html( "S/. " + Math.round((Cliente.RentaTotal+Cliente.RentasAdicionales)*100)/100 );
   
};


/*Uses Comercial class*/
Comparacion.LlenarInformacionDescuentos = function(identificador,descuentos)
{    
   var TipoMensaje = 3;
   var Mensaje = 0;
   var descripcion = Array();

   var j = 0;
   for(var i=0; i<descuentos.length; i++)
   {
        if(descuentos[i][TipoMensaje] === 'Descuento')
        {
            descripcion[j++] = '<span class="error">'+descuentos[i][Mensaje]+'</span>';
        }
   }

   if(window.Retenciones === 'Baja'  && identificador === 0)
   {
      descripcion[j++] = "<span id='herramientaRetenciones_"+identificador+"' class='linkHerramientas'>Herramientas<span>";
   }
   if(window.Retenciones === 'MigracionDown' && identificador === 0)
   {
       descripcion[j++] = "<span id='descuentoRetenciones_"+identificador+"' class='linkRetenciones'>Descuentos<span>";
   }
   
   
   var response = Util.llenarCasillaComponenteSinMovimiento(descripcion);
   return response;
   
};

Comparacion.Nombre = function (componenteNombre, Componente, movimiento, ComponenteMono) {
    var descripcion = Array();
    descripcion[0] = "";
    var Renta = "";

    if (movimiento === "Mantiene Monoproducto")
    {
        descripcion[0] = ComponenteMono.Nombre;
        Renta = "S/ " + ComponenteMono.RentaMonoproducto;
    } else if (Componente.Presente)
    {

        descripcion[0] = Componente.Nombre;
        Renta = Componente.RentaMonoproducto > 0 ? "S/ " + Componente.RentaMonoproducto : "";
    }


    var response = Util.llenarCasillaComponente(componenteNombre, descripcion, Renta, movimiento);
    return response;
};

Comparacion.PaqueteNombre = function(Paquete)
{
    var PaqueteRenta = Paquete.Renta > 0 ? "S/ "+ Paquete.Renta : "";
    var PaqueteNombre = Paquete.Nombre !== null ? "<b>" + Paquete.Nombre + "</b>" : "";
    var response = Util.llenarCasillaComponente("Paquete", Array(PaqueteNombre), PaqueteRenta);
    return response;  
};


Comparacion.InternetNombre = function(Componente,movimiento){
   var Internet = "";
   var Costo = "";
   
   if(Componente.Presente)
   {
      Internet = Componente.Nombre + " (" + Componente.Tecnologia + ")";
      if(!Componente.Paquetizado){
         Costo =  "S/." + Componente.RentaMonoproducto;
      } 
   }
   var i = 0;
   var descripcion = Array();
   descripcion[i] = Internet;
   i++;
   
   var response = Util.llenarCasillaComponente("Internet",descripcion, Costo, movimiento);
   return response;
};

Comparacion.PremiumNombre = function(Componente, movimiento,ComponenteMono){
   
   var Premium = "";
   var Costo = "";
   var descripcion = Array();
   
   var indice = 0;

    if(ComponenteMono !== undefined && ComponenteMono !== null)
    {
        descripcion[indice++] = Comparacion.LimpiaPremiumNombre(ComponenteMono.Nombre);
        Costo =  "S/." + ComponenteMono.RentaMonoproducto;
        
        
    }
    
   if(Componente.Presente)
   {
        Premium = Componente.Nombre;
        descripcion[indice++] = Comparacion.LimpiaPremiumNombre(Premium);
   }
    

    if(Componente.RentaMonoproducto > 0)
    {
        Costo =  "S/." + Componente.RentaMonoproducto;
    }
    return Util.llenarCasillaComponente("Premium",descripcion, Costo, movimiento);
   
};

Comparacion.PremiumNombreCliente = function(Componente,Freeview, movimiento,ComponenteMono){
   
   var Premium = "";
   var Costo = "";
   var descripcion = Array();
   
   var indice = 0;

    if(ComponenteMono !== undefined && ComponenteMono !== null)
    {
        descripcion[indice++] = Comparacion.LimpiaPremiumNombre(ComponenteMono.Nombre);
        Costo =  "S/." + ComponenteMono.RentaMonoproducto;
        
        
    }
    
   if(Componente.Presente)
   {
        Premium = Componente.Nombre;
        descripcion[indice++] = Comparacion.LimpiaPremiumNombre(Premium);
   }
    

    if(Componente.RentaMonoproducto > 0)
    {
        Costo =  "S/." + Componente.RentaMonoproducto;
    }
    return Util.llenarCasillaComponentePremium("Premium",Freeview,descripcion, Costo, movimiento);
   
};

Comparacion.LimpiaPremiumNombre = function(Premium){
    var Final = "";
    if(Premium != "" && Premium != null){
        var Tiene = Array();
        Premium.indexOf("HD");
        Tiene[0] = Premium.indexOf("HD") >= 0 ? "HD":false;
        Tiene[1] = Premium.indexOf("MC") >= 0 ? "FOX":false;
        Tiene[2] = Premium.indexOf("HBO") >= 0 ? "HBO":false;
        Tiene[3] = Premium.indexOf("HP") >= 0 ? "HP":false;
        Tiene[4] = Premium.indexOf("UFC") >= 0 ? "UFC":false;
        Tiene[5] = Premium.indexOf("GP") >= 0 ? "GP":false;
        Tiene[6] = Premium.indexOf("Estelar") >= 0 ? "<b><font color='green' style='background-color:yellow;'>Estelar</font></b>":false;

        for(var i=0; i<Tiene.length; i++)
        {
            if(Tiene[i])
            {
                if(Final != ""){
                    Final += " + ";
                }
                Final += Tiene[i];
            }
        }
    }
    
    return Final;
};

Comparacion.Modems = function(Cliente){
   var Modems = "";
   if(Cliente.Componentes["Internet"].Presente){
      if(Cliente.Componentes["Internet"].Tecnologia === "ADSL")
      {
         Modems = "Modem ADSL";
      }
      else if(Cliente.Componentes["Internet"].Tecnologia === "HFC")
      {
         Modems = Cliente.Componentes["Internet"].ModemNombre;
      }
   }
   
   if(Cliente.SmartWifi === "1")
   {
       Modems += "<br><span class='error'>Compatible App Smart Wifi</span>";
   }

   return Util.llenarCasillaComponente("Modems",[Modems],"");
};


Comparacion.obtenerSimboloMovimientoComponente = function(movimiento){
   var respuesta = "";
   if(movimiento !== "No Evalua Componente" && movimiento !== "Mantiene" && movimiento !== "Mantiene Monoproducto"){
       respuesta += "<span class='mensajeVerde'>";
       respuesta += movimiento;
       respuesta += "</span>";
   }
   return respuesta;
};

Comparacion.LlenarInformacionOferta = function(Opcion,Posicion)
{

   window.Opciones[Posicion] = Opcion;
   var Paquete = Comparacion.PaqueteNombre(Opcion.producto.Paquete,Opcion.TieneUltraWifi);
   
   
   if(Opcion.Cabecera !== null)
   {
      $('#cabecera'+Posicion).html(Opcion.Cabecera);
   }
   else if(window.Retenciones === 'MigracionDown' || window.Retenciones === 'Baja')
   {
      $('#cabecera'+Posicion).html("Producto seleccionado");
   }
   
   
   
   var componentes = Array("Linea","Plan","Cable");
   for(var i in componentes)
   {
      $("#Datos"+componentes[i]+"Oferta"+Posicion).html(Comparacion.Nombre(componentes[i],
              Opcion.producto.Componentes[componentes[i]],
              Opcion.movimiento[componentes[i]], 
              Opcion.ComponentesMono[componentes[i]]
              ));
   }
   
   var Internet = Comparacion.InternetNombre(
                      Opcion.producto.Componentes["Internet"], 
                      Opcion.movimiento["Internet"]);
   
   var Premium = Comparacion.PremiumNombre(
                      Opcion.producto.Componentes["Cable"].Premium, 
                      Opcion.movimiento["Premium"],
                      Opcion.ComponentesMono["Premium"]);
   
   var Decos = Comparacion.TransformarPresentacionComercialDecos(Opcion);
   var Modems = Comparacion.TransformarPresentacionComercialModems(Opcion);
   
   var OperacionComercial = Opcion.tipoOperacionComercial;
   
   
   $("#DatosPaqueteOferta"+Posicion).html(Paquete);
   $("#DatosDescuentoOferta"+Posicion).html(Comparacion.LlenarInformacionDescuentos(Posicion,Opcion.Comercial));
   $("#DatosInternetOferta"+Posicion).html(Internet);
   $("#DatosPremiumOferta"+Posicion).html(Premium);
   $("#DatosDecosOferta"+Posicion).html(Decos);
   $("#DatosModemsOferta"+Posicion).html(Modems);
   $("#DatosOCOferta"+Posicion).html(OperacionComercial);  
   $("#DatosSaltoOferta"+Posicion).html("S/. " + Opcion.salto_arpu);
   $("#DatosRentaOferta"+Posicion).html("S/. " + Opcion.cargo_fijo_destino);
   $("#DatosAdicionalesOferta"+Posicion).html("S/. " + window.cliente.RentasAdicionales);
   $("#DatosTotalOferta"+Posicion).html("S/. " + Math.round((Opcion.cargo_fijo_destino +window.cliente.RentasAdicionales)*100)/100 );
   
   
};


Comparacion.CajasTexto = function(Tipo,Clase)
{
   var CantidadCajas = 3;
    var resultado = "";
    resultado +="<td class = 'cajaTexto"+Clase+"' id = 'DatosCliente"+Tipo+"'></td>";
    for(var i = 1; i<=CantidadCajas; ++i)
    {
        resultado += "<td class = 'cajaTexto"+Clase+"' id = 'Datos"+Tipo+"Oferta"+i+"'>&nbsp;</td>";
    }
    return resultado;
};

Comparacion.ImprimirFila = function(Tipo,TextoLargo,Clase){
    if(Clase === undefined)
        Clase = "";
    
    return "<tr>" +
    "<td class = 'cajaTexto componentesParrilla'>" + TextoLargo + "</td>" +
    Comparacion.CajasTexto(Tipo,Clase)+
    "</tr>";
};


Comparacion.ImprimirBotones = function(Tipo){
    var resultado = 
    "<tr>" +
    "<td></td>" +
    "<td></td>";
    
    for(var i = 1 ; i <= 3; ++i)
    {
       resultado += Comparacion.ImprimirBoton(Tipo,i);
       
    }
    resultado +="</tr>";
    return resultado;
};

Comparacion.ImprimirBoton = function(Tipo,Orden)
{
   return "<td class='Boton"+Tipo+"' id ='Boton"+Tipo+Orden+"'>" +
       "<input class='boton' type='button' name='"+Tipo+Orden+"' value='Mostrar "+Tipo+"'>" +
       "</td>";
};




Comparacion.CrearEstructuraHTML = function(Resultado){
   
   window.Retenciones = Resultado.Retenciones;
   window.Opciones = new Array();
   var html = "";
   html += 
         "<table id='TablaComparacion'>" +
            "<tr>" +
               "<td>" + "&nbsp;" + "</td>" +
               "<td class = 'cajaTexto cabeceraParrilla'>" + "Actual" + "</td>" +
               "<td class = 'cajaTexto cabeceraParrilla' id='cabecera1'>" + "Oferta 1" + "</td>" +
               "<td class = 'cajaTexto cabeceraParrilla' id='cabecera2'>" + "Oferta 2" + "</td>" +
               "<td class = 'cajaTexto cabeceraParrilla' id='cabecera3'>" + "Producto Seleccionado" + "</td>" +
            "</tr>" +
            Comparacion.ImprimirFila("OC","Operacion")+
            Comparacion.ImprimirFila("Paquete","Paquete")+
            Comparacion.ImprimirFila("Descuento","Descuento")+
            Comparacion.ImprimirFila("Linea","Linea")+
            Comparacion.ImprimirFila("Plan","Plan")+
            Comparacion.ImprimirFila("Internet","Internet")+
            Comparacion.ImprimirFila("Cable", "TV")+
            Comparacion.ImprimirFila("Premium","Premium")+
            Comparacion.ImprimirFila("Decos","Decos")+
            Comparacion.ImprimirFila("Modems","Modems")+
            Comparacion.ImprimirFila("Renta","Subtotal"," rentaTotal")+
			Comparacion.ImprimirFila("Adicionales","Adicionales"," rentaTotal")+
			Comparacion.ImprimirFila("Total","Total"," rentaTotal")+
            Comparacion.ImprimirFila("Salto","Salto"," rentaTotal")+
            Comparacion.ImprimirBotones("Registro")
         "</table>";
 
   $("#NuevoContenido").html(html);
         
   

};




Comparacion.MostrarDatosRegistro = function(Posicion)
{
    if(window.Opciones[Posicion] !== undefined){
        var Opcion = window.Opciones[Posicion];
        Util.registroUsoAsesor(Opcion.producto.Paquete.Ps, "Mostrar registro");
        $("#modal-registro-oferta").html(
                Residencial.Detalle_Oferta_Sugerida.MostrarCartillaDeRegistro(Opcion.Registro,Opcion.RegistroDeco));
        $("#modal-registro-oferta").dialog("open");
    }
};




Comparacion.DecosCliente = function(InfoDecos,Netflix)
{
   var factorizados = new Array();
   var Decos = "";
   if(InfoDecos.length > 0){
      Decos = "";
      for(var i=0; i<InfoDecos.length; i++){
         var index = 'Deco '+InfoDecos[i].Tipo;
         if( !factorizados[index] ){
            factorizados[index] = 0;
         }
         factorizados[index]++;
      }      
      for(var key in factorizados){
         if(Decos !== ""){
            Decos += "<br>";
         }
         Decos += factorizados[key] + "  " + key;
      }
   }
   
   if(Netflix > 0)
   {
       Decos += '<br><span class="error">(' + Netflix + ' Deco con Netflix)</span>';
   }
    
   var Costo = "";
   var descripcion = Array();
   descripcion[0] = Decos;
   var response = Util.llenarCasillaComponente("Decos",descripcion, Costo, undefined);
   return response;
};

/*Uses Comercial Class*/
Comparacion.TransformarPresentacionComercialDecos = function(Opcion){
   var Comercial = "";
   var Contador = 0;
   var renta = "";
   var Fila = 0;
   
    if(Opcion.ComercialDeco.length > 0)
    {
        for(var i in Opcion.ComercialDeco)
        {
            for(var j in Opcion.ComercialDeco[i])
            {
                for(var k in Opcion.ComercialDeco[i][j])
                {
                    if(Contador>0)
                    {
                        Comercial += "<br> "; 
                    }
                    
                    if(Fila > 0 && Opcion.ComercialDeco[i][j][k][0] === '1 Deco HD Venta')
                    {
                        Comercial+='';
                    }
                    else
                    {
                        Comercial += Opcion.ComercialDeco[i][j][k][0];
                    }
                    
                    if(Contador>0)
                    {
                        renta += "<br>"; 
                    }

                    renta += Opcion.ComercialDeco[i][j][k][4];
                    
                    Contador += 1;
                }
           
            }
            Fila+=1;
        }
    }
   var movimiento = "";
   var descripcion = Array();
   descripcion[0] = Comercial;
   var Rentas = Array();
   Rentas[0] = renta;
   
   return Util.llenarCasillaComponente("Decos",descripcion, Rentas,undefined);
   
};

Comparacion.TransformarPresentacionComercialModems = function(Opcion){
   var Modems = "";
   if(Opcion.movimiento.Internet === "Up" || Opcion.movimiento.Internet === "Mantiene"
   || Opcion.movimiento.Internet === "Down"){
       Modems = "Mantiene Modem";
   }else{
       Modems = "";
   }
    for(var i = 0; i < Opcion.Registro.length; ++i  ) {
       var Operacion = Opcion.Registro[i];
       if(Operacion.Ps != null && ( Operacion.Ps.Ps == 20025 || Operacion.Ps.Ps == 20026 || Operacion.Ps.Ps == 9781 || Operacion.Ps.Ps == 22745 || Operacion.Ps.Ps == 23009 || Operacion.Ps.Ps == 23059 || Operacion.Ps.Ps == 23060)
       && (Operacion.TipoOperacionComercial == 'Alta' || Operacion.TipoOperacionComercial == 'Alta con Financiamiento' || Operacion.TipoOperacionComercial == 'Alta PACK CERO')) {
          Modems = Operacion.Ps.NombreComercial;
       }
    }
   
   var response = Util.llenarCasillaComponente("Modems",[Modems], "", "");
   return response;
};



Comparacion.ObtenerModoParaMensaje = function(){
   
   if ($('#modoRegular').is(':checked')){
	   return 'Regular';
   }

   if ($('#modoRetenciones').is(':checked')){
      if ($('#retenciones_migracion_down').is(':checked')){
         return 'Retencion';
      } 
      else if ($('#retenciones_baja').is(':checked')){
         return 'Retencion';
      }
   }
   
   if ($('#modoAverias').is(':checked')){
	   return 'Averias';
   }
   
   if ($('#modoOnline').is(':checked')){
	   return 'Online';
   }
  
   return 'Ninguno';
};


Comparacion.MostrarMensajeParaTrioSinDuoInterneTv = function()
{     
   var modito=this.ObtenerModoParaMensaje();
   var paqueteNom=cliente.Paquete.Categoria;  

   if(modito=='Retencion' && (paqueteNom=='Trios CATV' || paqueteNom=='Trios DTH')
    ){
      $('#MensajeTrioSinDuoInternetTv').css('display','none');
   }else if(
      (modito=='Regular' || modito=='Averias' || modito=='Online') && 
      (paqueteNom=='Trios CATV' || paqueteNom=='Trios DTH')){

         $('#MensajeTrioSinDuoInternetTv').css('display','block');
         $('#MensajeTrioSinDuoInternetTv').html('<b>Notificación: </b> Los clientes tríos deberán solicitar el producto Dúo Internet+TV a través del 104. Este producto solo está disponible en cobertura HFC/FTTH.');
       
   }else if ((modito=='Regular' || modito=='Averias' || modito=='Online' || modito=='Retencion') && 
   (paqueteNom!='Trios CATV' || paqueteNom!='Trios DTH')
   ){
      $('#MensajeTrioSinDuoInternetTv').css('display','none');
   }
  
};