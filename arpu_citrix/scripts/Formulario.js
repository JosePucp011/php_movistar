var Formulario = {};

Formulario.telefono = null;
Formulario.documento = null;
Formulario.ultimoTelefono = null;
Formulario.ultimoDocumento = null;


Formulario.ObtenerModoPorDefecto = function()
{
    $('#retenciones_opciones').hide();
    if(window.modoPorDefecto === undefined)
    {
        $("#modoRegular").attr('checked', true);
    }
    else
    {
        $('#modosDeOperacion').hide();
        $(window.modoPorDefecto).attr('checked', true);
        if(window.opcionPorDefecto !== undefined)
        {
            $('#retenciones_opciones').show();
            $(window.opcionPorDefecto).attr('checked', true);
        }
    }
};

Formulario.Inicializar = function(){
    
    if ($('#modoAverias').is(':checked') || $('#modoRetenciones').is(':checked')) {
        $('#TablaAverias').show();
    } else
    {
        $('#TablaAverias').hide();
    }

    $('#modosDeOferta').hide();
    $('#numeroDocumento').keyup(function(){Formulario.OnDatoActualizado('#telefono');});
    $('#telefono').keyup(function (){Formulario.OnDatoActualizado('#numeroDocumento');});
    

    $('input[name=modo]').change(function() {
 
       if($('#modoAverias').is(':checked')){
     
          $('#modosDeOferta').hide();
          $('#retenciones_opciones').hide();
          $('#TablaAverias').show();
       } 
       else {
        if($('#modoRetenciones').is(':checked')){
            
           $('#retenciones_opciones').show();
           
           $('#retenciones_migracion_down').attr('checked', true);

             if( window.cliente.Paquete.ProductoId === 3)
             {
               $('#modosDeOferta').show();
             }

             $('#TablaAverias').show();
        }
        else{
           $('#modosDeOferta').hide();
           $('#retenciones_opciones').hide();
           $('#TablaAverias').hide();
            }
        }
   
       Formulario.OnParametroConsultaModificado();
    });

    $('#ignorar_cable,#retenciones_migracion_down,#retenciones_baja,#modoBandaAncha,#modoTelevision').
            change(function(){Formulario.OnParametroConsultaModificado();});
    

};




Formulario.OnParametroConsultaModificado = function(){
    this.Limpiar();
    this.Actualizar(DatosCliente.limpiar_cliente_datos);
};

Formulario.OnDatoActualizado = function(OtroCampo){
    $(OtroCampo).val('');
    this.Actualizar(DatosCliente.limpiar_cliente);
 };


Formulario.ObtenerRetenciones = function(){
   if ($('#modoRetenciones').is(':checked')){
      if ($('#retenciones_migracion_down').is(':checked')){
         return 'MigracionDown';
      } 
      else if ($('#retenciones_baja').is(':checked')){
         return 'Baja';
      }
   }
   
   if ($('#modoAverias').is(':checked')){
	   return 'Averias';
   }
   
   if ($('#modoOnline').is(':checked')){
	   return 'Online';
   }
  
   return 'Ninguno';
};


Formulario.ObtenerOfertaModo = function(){
   if ($('#modoTelevision').is(':checked')){return 'Television'; }
   if ($('#modoBandaAncha').is(':checked')){return 'BandaAncha';}
   
   return 'BandaAncha';
};


Formulario.Actualizar = function(LimpiarDatos){
    this.Validar();
    if(this.RequiereActualizacion())
    {
       LimpiarDatos();
       this.Consultar();
    }
    else if(this.NoHayDatos()) 
    {
       DatosCliente.limpiar_cliente();
    }
};

Formulario.Validar = function(){
   this.ValidarTelefono();
   this.ValidarDocumento();
};

Formulario.ValidarTelefono = function(){
   var telefono = this.PermitirNumeros('#telefono');
   this.telefono = (telefono.length === 8 ? telefono : null);
};

Formulario.ValidarDocumento = function(){
   var documento = this.PermitirNumeros('#numeroDocumento');
   this.documento = ( (documento.length === 8 || documento.length === 11) ? documento : null);
};

Formulario.RequiereActualizacion = function(){
   if(this.ultimoDocumento === this.documento && this.telefono === this.ultimoTelefono
      || this.documento === null && this.telefono === null
   )
   {
      this.ultimoDocumento = this.documento;
      this.ultimoTelefono = this.telefono;
      return false;
   }
   return true;
}

Formulario.NoHayDatos = function(){
   return this.documento === null && this.telefono === null;
};

Formulario.Consultar = function(){
   $.ajax({
         url: 'Http/Consultar.php',
         context: document.body,
         type: 'GET',
         data:  {
            telefono: this.telefono,
            documento: this.documento,
            retenciones: this.ObtenerRetenciones(),
            ofertaModo: this.ObtenerOfertaModo(),
            version : 5
         },
         datatype: 'json',
         success: DatosCliente.MostrarResultado
      });
  this.ultimoTelefono = this.telefono;
  this.ultimoDocumento = this.documento;
      
};

Formulario.Limpiar = function(){
   this.ultimoDocumento = null;
   this.ultimoTelefono = null;
   this.documento = null;
   this.telefono = null;
};

Formulario.PermitirNumeros = function(NombreInput) {
   var Texto = $(NombreInput).val();
   var TextoLimpio = "";
   for (var i = 0; i < Texto.length; ++i) {
      var caracter = Texto.charAt(i);
      if (caracter >= '0' && caracter <= '9') {
         TextoLimpio += caracter;
      }
   }
   if(TextoLimpio !== Texto){
       $(NombreInput).val(TextoLimpio);
   }
   return TextoLimpio;
};


Formulario.PermitirNumerosTexto = function(Texto) {
   var TextoLimpio = "";
   for (var i = 0; i < Texto.length; ++i) 
   {
      var caracter = Texto.charAt(i);
      if (caracter >= '0' && caracter <= '9') {
         TextoLimpio += caracter;
      }
   }
   return TextoLimpio;
};