//
/*
 * Alterna de clases de estilo con JQuery
 * @Example
    fnChangeColor.elemtId     = "dv1";
    fnChangeColor.classBefore = "dv1_change1";
    fnChangeColor.classAfter  = "dv1_change2";
    fnChangeColor.fnsInterval();
*/
var fnChangeColor = {
    elemtId        : null,//Id del elemento
    classBefore    : null,//Class 1 antes
    classAfter     : null,//Class 2 despues
    timeInterval   : 1000,//tiempo de intervalo
    setChangeColor : function()
    {
      if(fnChangeColor.elemtId==null){ console.error("fnChangeColor: el objeto es null."); return;}
      if(fnChangeColor.classBefore==null){ console.error("fnChangeColor: la clase classBefore es null."); return;}
      if(fnChangeColor.classAfter==null){ console.error("fnChangeColor: la clase classAfter es null."); return;}

      var elemt = $("#"+fnChangeColor.elemtId);
      if(elemt.length==0){ console.error("fnChangeColor: el objeto no existe"); return;}
      //elemt = fnChangeColor.elemt;
      var classBefore = fnChangeColor.classBefore;
      var classAfter  = fnChangeColor.classAfter;
      if(elemt.hasClass(classBefore))
      {
        elemt.removeClass(classBefore);
        elemt.addClass(classAfter);
      }
      else
      {
        elemt.removeClass(classAfter);
        elemt.addClass(classBefore);
      }
    },
    fnsInterval    : function()
    {
      setInterval(fnChangeColor.setChangeColor, fnChangeColor.timeInterval);
    }
  }
  //end