<?php

namespace Arpu\Logic\Filtros;


class FiltrosGeneralesProducto {
    
    private $Cliente;
    /**
     * @var Arpu\Entity\Producto
     */
    private $producto;
    private $modalidadRetencion;
    
    public function __construct($Cliente,$producto,$modalidadRetencion) {  
        $this->Cliente = $Cliente;
        $this->producto = $producto;
        $this->modalidadRetencion = $modalidadRetencion;
    }
    
    public function Es_ProductoCliente_NoValido(){
        
        $this->motivoNoOferta = "";
        
        return ($this->EsMismoProducto() ||
                $this->EsPermutacion_Cliente16Mbps() ||
                $this->NoCoincideCoberturaCable() ||
                $this->DecoAdicional_ClienteSinDecos() ||
                $this->Bloque_ClienteSinEstandarEstelar() ||
                $this->Bloque_ClienteTieneSenal() || 
                
                $this->Cliente->Internet->Cobertura->RadioEnlace ||
                $this->DuoTrioIquitos_ClienteNoIquitos_oViceversa() ||
                $this->DuoTrio_ExcedeVelocidadMaxima() ||
                $this->DuoTrioADSL_CoberturaHFC() ||
                $this->DuoTrioHFC_CoberturaNoHFC() ||
                
                $this->Multidestino_ClienteSinLinea() ||
                $this->Multidestino_ClienteConPlanVoz() ||
                
                $this->BundleBloque_ClienteSinHD_oViceversa() ||
                $this->Bloque_ClienteHogarDigital() ||
                
                $this->RepetidorSmartHFC_ClienteADSL_oViceversa() ||
                $this->SvaInternet_ClienteSinInternet() ||
                $this->SeguridadTotal_ClienteYaTiene()  ||
                
                $this->EsNaked()    ||
                $this->ProductoInternetNaked()  ||
                //$this->EsMonoTv_NoMostrarDeco_DVR_SMARTHD() ||  // Bloquea el deco DVR y SMART HD Solo para Monos Tv
                
                $this->PermutacionInvalida() ||
                $this->PermutacionInvalidaMT() ||
                $this->ValidacionDTH()
                
                );
    }
    
    private function EsPermutacion_Cliente16Mbps(){
        $r =
        $this->producto->EsPermutacion &&
        ($this->producto->Paquete->Ps == 22839 || $this->producto->Paquete->Ps == 23144 )
         &&   ($this->Cliente->Internet->Cobertura->VelocidadMaxima >=16000 || $this->Cliente->Internet->Velocidad >=16000);
        
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    
    private function EsMonoTv_NoMostrarDeco_DVR_SMARTHD(){
        $r= $this->producto->Paquete->Tipo == 'Deco' && ($this->producto->Paquete->Ps == 20449 || $this->producto->Paquete->Ps == 21933 || $this->producto->Paquete->Ps == 22646) && $this->Cliente->Es_MonoTv() ;
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
     
    
    private function EsNaked(){
        $r= $this->producto->Paquete->Tipo != 'Naked' && $this->Cliente->Internet->Naked > 0;
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function Bloque_ClienteTieneSenal(){
        $r= $this->producto->Paquete->Tipo == 'Bloque' &&
                $this->Cliente->Cable->Premium->TieneSenal($this->producto->Cable->Premium->Nombre);
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function NoCoincideCoberturaCable(){

        $r= !($this->producto->Cable->Cobertura & $this->Cliente->Cable->Cobertura);
        $origen = $this->Cliente->Cable->Cobertura;
        $destino = $this->producto->Cable->Cobertura;
        
        if($r){$this->motivoNoOferta = __FUNCTION__."Origen: $origen, Destino: $destino" ;}return $r;
    }
    
    private function Bloque_ClienteSinEstandarEstelar(){
        $r= $this->producto->Paquete->Tipo == 'Bloque' && 
                !in_array($this->Cliente->Cable->Tipo, array('Estandar', 'Estelar'));
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function DecoAdicional_ClienteSinDecos(){
        $r= ($this->producto->Paquete->Tipo == 'Deco' && 5 < $this->Cliente->Cable->CantidadDeDecos)
            || ($this->producto->Paquete->Tipo == 'Deco' && !$this->Cliente->Cable->Presente); 
        

       /*  $r= $this->producto->Paquete->Tipo == 'Deco' && 0 == $this->Cliente->Cable->CantidadDeDecos 
                || $this->producto->Paquete->Tipo == 'Deco' && 5 < $this->Cliente->Cable->CantidadDeDecos; */

        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function DuoTrioHFC_CoberturaNoHFC(){
        $r= $this->producto->Internet->Tecnologia == 'HFC' && !$this->Cliente->Internet->Cobertura->HFC;
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function SvaInternet_ClienteSinInternet(){
        $r= $this->producto->Paquete->Tipo == 'Internet' && !$this->Cliente->Internet->Presente;
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function SeguridadTotal_ClienteYaTiene(){
        $r= $this->producto->Paquete->Nombre == 'Seguridad Total' && $this->Cliente->Internet->TienePAM;
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function DuoTrio_ExcedeVelocidadMaxima(){
        $r= $this->producto->Internet->Tecnologia == 'ADSL' 
              && $this->producto->Internet->Velocidad > $this->Cliente->Internet->Cobertura->VelocidadMaxima;
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    
    private function DuoTrioIquitos_ClienteNoIquitos_oViceversa(){
        $r= $this->Cliente->Internet->Cobertura->Iquitos != $this->producto->Internet->Cobertura->Iquitos;
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function BundleBloque_ClienteSinHD_oViceversa(){
       $rentaBloque = $this->Cliente->Cable->Premium->TieneSenal('HD') ? 30 : 30; // Fanny   $rentaBloque = $this->Cliente->Cable->Premium->TieneSenal('HD') ? 20 : 30;
       $bloquesConBundle = array('HP');
       $r= 'Bloque' === $this->producto->Paquete->Tipo 
                && in_array($this->producto->PremiumNombre, $bloquesConBundle) 
                && $this->producto->Paquete->Renta != $rentaBloque;
       if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function Bloqueo_BloqueHBO(){
      $r= isset($this->producto->PremiumNombre) &&
            strpos($this->producto->PremiumNombre, 'HBO') !== false
               && !$this->Cliente->Cable->Premium->TieneSenal('HBO');
      if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function Multidestino_ClienteConPlanVoz(){
        $r= $this->Cliente->Plan->Presente 
                && $this->producto->Paquete->Tipo == 'Multidestino';
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function Multidestino_ClienteSinLinea(){
        $r= !$this->Cliente->Linea->Presente && $this->producto->Paquete->Tipo == 'Multidestino';
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function EsMismoProducto(){
        $r= $this->Cliente->Paquete->Ps == $this->producto->Paquete->Ps
                && $this->Cliente->MovistarTotal == 0;
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function RepetidorSmartHFC_ClienteADSL_oViceversa() {
        $r= $this->Cliente->Internet->Tecnologia === 'HFC' && $this->producto->Paquete->Ps == 23021 || $this->Cliente->Internet->Tecnologia === 'ADSL' && $this->producto->Paquete->Ps == 23022;
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }

    private function Bloque_ClienteHogarDigital() {
        $r= $this->Cliente->Cable->Tipo === 'Hogar Digital' && $this->producto->Paquete->Nombre === 'Bloque HD';
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }

    private function DuoTrioADSL_CoberturaHFC() {
        if($this->modalidadRetencion=='MigracionDown' || $this->modalidadRetencion=='Baja' ){
            $r=$this->Cliente->Paquete->Renta <=$this->producto->Paquete->Renta 
                                                && $this->producto->Internet->Tecnologia == 'ADSL'
                                                && !$this->producto->EsSva();
            if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
            //return false;
        }else{ 
            $r=$this->Cliente->Internet->Tecnologia !=='ADSL' 
            && !$this->Cliente->Es_DuoBa()
            && $this->producto->Internet->Tecnologia === 'ADSL';
            if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
            
            /*  $r=  $this->Cliente->Internet->Cobertura->HFC 
                && $this->producto->Internet->Tecnologia === 'ADSL'
                && $this->producto->Paquete->Tipo != 'Naked';
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r; */
        }
        
    }
    
    private function ProductoInternetNaked() {
        $r = false;
        if($this->Cliente->Paquete->Categoria != 'Naked' && 
            $this->producto->Paquete->Categoria != 'Naked' && 
            $this->producto->Paquete->Tipo === 'Naked') {
            if($this->producto->Paquete->Tipo === 'Naked') {
                if($this->Cliente->Es_Naked()) {
                    $r=  !($this->producto->Internet->Tecnologia == $this->Cliente->Internet->Tecnologia && 
                    $this->Cliente->Paquete->Categoria == $this->producto->Paquete->Categoria);
                } else {
                    $r= $this->producto->Internet->Tecnologia != $this->Cliente->Internet->Tecnologia || 
                        $this->producto->Paquete->Categoria === 'Naked CMS';
                }
            }
        } else {
            $r=  ($this->producto->Internet->Tecnologia != $this->Cliente->Internet->Tecnologia && 
                  $this->producto->Paquete->Tipo === 'Naked') || 
                  $this->producto->Paquete->Categoria === 'Naked CMS';
        }

        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }   
	
	private function PermutacionInvalida(){
        $r=  ($this->producto->Paquete->Ps == 23494 || $this->producto->Paquete->Ps == 23411 )  
        && (isset($this->producto->PremiumNombre) && strpos($this->producto->PremiumNombre, 'HD') !== false);
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }
    
    private function PermutacionInvalidaMT(){
        $r = false; 
        
        if($this->producto->Paquete->Ps == 23240){
            //Cliente Tiene HD
            if(isset($this->Cliente->Premium->Canales) 
                    && strpos($this->Cliente->Premium->Canales, 'HD') !== false){
                
                        if(isset($this->producto->PremiumNombre) 
                            && strpos($this->producto->PremiumNombre, 'HD') === false){
                            $r = true;
                        }            
            }
            //Cliente no Tiene HD
            if(!(isset($this->Cliente->Premium->Canales)) 
                || (isset($this->Cliente->Premium->Canales) && strpos($this->Cliente->Premium->Canales, 'HD') === false)){
                      if(isset($this->producto->PremiumNombre) 
                                && strpos($this->producto->PremiumNombre, 'HD') !== false){ 
                            $r = true;
                        }
            }
            
        }

        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;        
    }

    private function ValidacionDTH(){
        $r=  $this->Cliente->Paquete->Categoria == 'Duos BA' 
                && $this->producto->Cable->Presente == 1
                && $this->producto->Cable->Categoria === 'DTH';
        if($r){$this->motivoNoOferta = __FUNCTION__;}return $r;
    }

}