<?php

namespace Arpu\Logic;
use Arpu\Entity\Cliente;
use Arpu\Data\CompensacionDL;
use Arpu\Data\DescuentoRetencionesDL;
use Arpu\Data\DescuentoRetencionesIncrementoDL;/* Incremento */
use Arpu\Entity\Deco;
use Arpu\Entity\Herramienta;
use Arpu\Data\MensajeDL;/* Para mensajes */


class HerramientaRetencionesBL {
    public static function Obtener(Cliente $cliente,$modo)
    {
        $resultado = new Herramienta();
        $resultado->Compensacion = CompensacionDL::Buscar($cliente);
        $resultado->DescuentoRetenciones = self::BuscarDescuento($cliente,$modo);
        if($cliente->IncrementoPrecio==1){
            $resultado->DescuentoRetencionesIncremento = self::BuscarDescuentoIncremento($cliente,$modo);
        }        
        $resultado->Gratuidad = self::BuscarFreeview($cliente,$modo);
        $resultado->DecodificadorAdicional = self::BuscarDecodificador($cliente);
        $resultado->BonoMovil = self::BuscarBonoMovil($cliente);
        //$resultado->Solucion = self::BuscarSolucionDecodificador($cliente); // Desactivar hasta que solucionen las PS
        $resultado->Equipamiento[] = 'Cambio de Telefono.';
        return $resultado;
    }
    
    private static function BuscarDescuento($cliente,$modo){
        
        if($cliente->Paquete->ProductoId >= 5)
        {
           // Para Duos Tv + BA Zona de competencia Y Zona No Competencia
           if($cliente->Es_DuoTvInt()){
            if($modo=='Baja'){
                return DescuentoRetencionesDL::Buscar($modo,$cliente->Paquete->Renta,'I',1);                                             
            }else{
               return DescuentoRetencionesDL::Buscar('MigracionDown',$cliente->Paquete->Renta,'Multiproducto',1); 
            }
              
       }
       // Para los Naked Atis Zona de competencia Y Zona No Competencia
       if($cliente->TipoBlindaje=='E' && $modo=='Baja'){ // Validar que sea solo internet presente 
           return DescuentoRetencionesDL::Buscar($modo,$cliente->Paquete->Renta,
           $cliente->ZonaCompetencia==1?'K':'L'
           ,1);
            }else{
                return DescuentoRetencionesDL::Buscar($modo,0,$cliente->TipoBlindaje,1); 
            }  
        }
        if($cliente->Paquete->ProductoId === 1)
        {
            return DescuentoRetencionesDL::Buscar('Retenciones',$cliente->Linea->RentaMonoproducto,'MonoLinea',1);    
        }   
        
        return DescuentoRetencionesDL::Buscar($modo == 'Baja' ? 'Baja' : 'MigracionDown',
                    $cliente->Paquete->ProductoId == 1 ? $cliente->Linea->RentaMonoproducto : $cliente->Paquete->Renta,
                    $modo == 'Baja' ? $cliente->TipoBlindaje : 'Multiproducto' ,
                    0);
    }
    
/* Para descuento de incremento */
private static function BuscarDescuentoIncremento($cliente,$modo){
    
    if (isset($cliente->Mensajes[0]->Renta->Origen) || 
        isset($cliente->Mensajes[0]->Renta->Destino) ){
        $destino_renta=$cliente->Mensajes[0]->Renta->Destino;/*  */
        $origen_renta=$cliente->Mensajes[0]->Renta->Origen;/*  */
        $diferencia=$cliente->Mensajes[0]->MontoIncremento;//$destino_renta-$origen_renta;
    }else{
        $destino_renta=0;
        $origen_renta=0;
        $diferencia=0;
    }

    if($cliente->Paquete->ProductoId === 6)
    {
     return DescuentoRetencionesIncrementoDL::Buscar('IncrementoPrecio',
                $diferencia,
                $cliente->TipoBlindaje,
                $cliente->IncrementoPrecio);           
    } 
    if($cliente->Paquete->ProductoId ===5) // MONO TV
    {
     return DescuentoRetencionesIncrementoDL::BuscarMonoYNaked('IncrementoPrecio',
                $destino_renta,
                $cliente->Cable->Tipo,
                1,$cliente->Cable->Categoria);           
    } 
    if(!isset($cliente->Mensajes[0]->Renta->Origen) ){/*  */
    
        return "";
    }else{
        
        return DescuentoRetencionesIncrementoDL::BuscarIncremento('IncrementoPrecio',
                $cliente->Paquete->ProductoId == 1 ? $cliente->Linea->RentaMonoproducto : $diferencia,
                1    
            );
    }
}
/* Para descuento de incremento */

    private static function BuscarFreeview($cliente, $modo) {
        $resultado = array();
        if (!$cliente->Cable->Presente || $cliente->Cable->CantidadDeDecos == 0) {
            return $resultado;
        }

        if (!$cliente->Cable->Premium->TieneSenal('HBO')) {
            if ($modo === 'Baja' && $cliente->TipoBlindaje != 'C') {
                $resultado[] = 'FreeView HBO x 3 meses.';
            } else {
                $resultado[] = 'FreeView HBO x 1 mes.';
            }
        }

        if (!$cliente->Cable->Premium->TieneSenal('HD') && 
                $modo === 'Baja' &&
                $cliente->TipoBlindaje === 'A') {
            $cliente->Herramienta->Gratuidad[] = 'FreeView HD x 1 mes.';
        }
        return $resultado;
    }
    
    private static function BuscarDecodificador($cliente) {
        if ($cliente->Cable->Presente &&
                $cliente->AntiguedadEnMeses >= 6 && $cliente->Cable->CantidadDecosHD < 2) {
            return Deco::$Decos[$cliente->Cable->Categoria.'_HD']->Registro;
        }
        return null;
    }
    
    private static function BuscarBonoMovil($cliente){
        
        $resultado = array();
        if($cliente->TipoBlindaje != 'C')
        {
            $resultado[] = ' 300 min. a Movistar x 3 meses.'; 
            $resultado[] = ' 50 min. Multidestino x 3 meses.'; 
            $resultado[] = ' 1GB de Internet x 3 meses.'; 
        }
        return $resultado;
    }
    
    private static function BuscarSolucionDecodificador($cliente) {
        $promocionBaja_DuoBaNakedAtis=self::BuscarPromo_DuoTvBA_NakedAtis($cliente);
        
        if ( ($cliente->Internet->Tecnologia == 'HFC' ||  $cliente->Internet->Tecnologia == 'FTTH') 
             && !$cliente->TrobasSaturadas
             && ( $cliente->Es_DuoTvInt() ||  ($cliente->Es_Naked() && $cliente->Telefono>0 ) )
             && $cliente->Internet->Velocidad<200000
            ) {
            return array(
                
                "
                <table cellpadding='3' cellspacing='3'><tr class='ProductoSVA'>
                <td class='cabeceraParrilla'><b>PS</b></td>
                <td class='cabeceraParrilla'><b>Velocidad</b></td>          
                </tr>
                <tr class='ProductoSVA'>".$promocionBaja_DuoBaNakedAtis.               
                "</tr>
                </table>
                "
            );
        }
        return array();
    }
    
    private static function BuscarPromo_DuoTvBA_NakedAtis($cliente) { 
        if ($cliente->Internet->Velocidad>1000 && $cliente->Internet->Velocidad<=10000){
            return "<td class='cajaTexto'>23098</td><td class='cajaTexto'>30MB X 2M</td>";
        }else if($cliente->Internet->Velocidad>10000 && $cliente->Internet->Velocidad<=20000){
            return "<td class='cajaTexto'>23100</td><td class='cajaTexto'>60MB X 2M</td>";
        }else if($cliente->Internet->Velocidad>20000 && $cliente->Internet->Velocidad<=30000){
            return "<td class='cajaTexto'>23100</td><td class='cajaTexto'>60MB X 2M</td>";
        }else if($cliente->Internet->Velocidad>30000 && $cliente->Internet->Velocidad<=40000){
            return "<td class='cajaTexto'>23101</td><td class='cajaTexto'>80MB X 2M</td>";
        }else if($cliente->Internet->Velocidad>40000 && $cliente->Internet->Velocidad<=60000){
            return "<td class='cajaTexto'>23101</td><td class='cajaTexto'>120MB X 2M</td>";
        }else if($cliente->Internet->Velocidad>60000 && $cliente->Internet->Velocidad<=120000){
            return "<td class='cajaTexto'>23101</td><td class='cajaTexto'>200MB X 2M</td>";
        }else{
            return "No Aplica";
        }     
         
    }
}
