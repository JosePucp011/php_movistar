<?php

namespace Arpu\Logic;

use Arpu\Entity\Oferta;
use Arpu\Entity\Cable;
use Arpu\Entity\Premium;
use Arpu\Entity\Operacion;
use Arpu\Entity\Componente;
use Arpu\Entity\Movimiento;

class PremiumBL
{
   public static function ComparacionPremium(Oferta $oferta,Cable $origen,Cable $destino)
   {
      if('Deco' == $destino->TipoSVA)
      {
         return;
      }
      
      $destinoContenido = array();
      
      if ($destino->Premium->Presente)
      {
         $destinoContenido = $destino->Premium->PremiumBloque[0]->PremiumContenido;
         
         if (!$origen->Premium->Presente || 'Bloque' == $destino->TipoSVA)
         {
             $oferta->AltaBloques = count($destinoContenido);
             $oferta->movimiento[Componente::Premium] = \Arpu\Entity\CA::Alta . ' ' .
                     str_replace('MC', 'FOX', implode("+", $destinoContenido));
             return;
         } 
      }

      if($origen->Premium->Presente)
      {
          $comparacion = self::MigracionPremium($oferta, $origen->Premium, $destinoContenido);

          if (count($comparacion->Bajas) > 0 || count($comparacion->Altas) > 0)
          {
              $oferta->movimiento[Componente::Premium] = '';
          }

          $oferta->BajaBloques = count($comparacion->Bajas);
          if ($oferta->BajaBloques > 0)
          {
              $oferta->movimiento[Componente::Premium] .= ' Baja ' . implode("+", $comparacion->Bajas);
              
          }
          $oferta->AltaBloques = count($comparacion->Altas);
          if ($oferta->AltaBloques > 0)
          {
              $oferta->movimiento[Componente::Premium] .= ' Alta ' . implode("+", $comparacion->Altas);
              
          }

          if (count($comparacion->Bajas) == 0 && count($comparacion->Altas) == 0 
                  && $destino->Premium->Presente)
          {
              $oferta->movimiento[Componente::Premium] = Movimiento::Mantiene;
          }

          $oferta->movimiento[Componente::Premium] = 
                  str_replace('MC','FOX',trim($oferta->movimiento[Componente::Premium]));
      }
   }
   
   private static function MigracionPremium(Oferta $oferta,Premium $origen,$destinoContenido)
   {
      $bajas = array();
      $origenContenido = array();
      $canales = null;
      
      foreach ($origen->PremiumBloque as $bloque)
      {
          $origenContenido = array_merge($origenContenido, $bloque->PremiumContenido);
          $interseccion = array_intersect($destinoContenido, $bloque->PremiumContenido);
          $diferenciaBajas = array_diff($bloque->PremiumContenido, $interseccion);
          
          $bajas = array_merge($bajas, $diferenciaBajas);
          $bajas = array_diff($bajas,array('HP','UFC','GP','EST','Estelar'));
          
          if($bloque->PremiumContenido[0] != 'HP' 
                  && $bloque->PremiumContenido[0] != 'UFC' 
                  && $bloque->PremiumContenido[0] != 'GP'
                  && $bloque->PremiumContenido[0] != 'EST'
                  && $bloque->PremiumContenido[0] != 'Estelar')
          {
            $oferta->arpu += $bloque->PremiumRenta;
          }
          else
          {
            $canales = $canales.$bloque->PremiumPs.';';
            $oferta->Impacto_ArpuSuelto +=  $bloque->PremiumRenta;
          }   

          self::EfectuarBajaPremium($bloque,$oferta); 
      }
      
      if($canales != null)
      {
        $canalestotal = substr($canales, 0, -1);
        $oferta->ComponentesMono[Componente::Premium] = \Arpu\Data\PremiumDL::Construir(false,$canalestotal);
      }
      
      $altas = array_diff($destinoContenido, $origenContenido);
    
      return new ComparacionPremium($altas,$bajas);
   }
   
   private static function EfectuarBajaPremium($bloque,Oferta $oferta)
   {
      if($bloque->Paquetizado){return;}
      if ($bloque->PremiumFuente != 'Atis'){return;}
      
      $ExistenRegistrosPorBorrar = false;
      $nuevoRegistro = array();

      foreach($oferta->Registro as $registro)
      { 
         if($registro != null && $registro->Ps != null && $registro->Ps->Ps === $bloque->PremiumPs)
         {
            $ExistenRegistrosPorBorrar = true;
            continue;
         }
         $nuevoRegistro[] = $registro;
      }
       
      if($ExistenRegistrosPorBorrar)
      {
         $oferta->Registro = $nuevoRegistro;
      }
      else
      {
          if($bloque->PremiumContenido[0] != 'HP' 
                  && $bloque->PremiumContenido[0] != 'UFC' 
                  && $bloque->PremiumContenido[0] != 'GP'
                  && $bloque->PremiumContenido[0] != 'EST')
          {
            $oferta->Registro[] = new Operacion(
                    $bloque->PremiumPs, 
                    $bloque->PremiumNombre, 
                    \Arpu\Entity\CA::ComponenteRegistro_Canales, 
                    Operacion::Baja);
          }
      }
   }
}

class ComparacionPremium
{
   public function __construct($altas,$bajas)
   {
      $this->Altas = $altas;
      $this->Bajas = $bajas;
   }
   public $Altas;
   public $Bajas;
}
