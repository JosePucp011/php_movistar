<?php

namespace Arpu\Logic;

class CableHelper
{
   /**
    * 
    * @param Cable $origen
    * @param Cable $destino
    * @return string
    */
   public static function ObtenerGrupo($origen,$destino)
   {
      if($destino->EsSVA)
      {
         return self::ObtenerGrupoSVA($origen,$destino);
      }
      if($destino->EsHDTotal)
      {
         return $destino->Categoria.' Estelar Full';
      }
      if($destino->Tipo == 'Estelar' && ($origen->Cobertura & (\Arpu\Entity\CA::Digital + \Arpu\Entity\CA::Digitalizada) 
              || $destino->Categoria == 'DTH'))
      {
         return $destino->Categoria.' Estelar';
      }
      if($destino->Tipo == 'Estandar' && $destino->Premium->Presente)
      {
         return $destino->Categoria.' Estandar HD';
      }
      if($destino->Categoria == 'DTH' && $destino->Tipo == 'Estandar')
      {
         return $destino->Categoria.' Estandar';
      }
      if($destino->Tipo == 'Estandar' && $destino->Cobertura & \Arpu\Entity\CA::Digitalizada)
      {
         return $destino->Categoria.' Estandar Digital';
      }
      
      if($destino->Tipo == 'Basico' || $destino->Tipo == 'Hogar Digital')
      {
         return $destino->Categoria.' '.$destino->Tipo;
      }

      return null;
   }
   
   /**
    * 
    * @param \Cable $origen
    * @param \Cable $destino
    * @return string
    */
   private static function ObtenerGrupoSVA($origen,$destino)
   {
      if($destino->TipoSVA != 'Bloque')
      {
         return null;
      }
      
      return $destino->Categoria.' HD';
   }
}
