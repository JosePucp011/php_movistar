<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\Cliente;
use Arpu\Entity\Oferta;
use Arpu\Entity\Componente;
use Arpu\Entity\Movimiento;

class DescuentoLineaDuoBa implements IDescuento
{

   public function AplicaDescuento(Oferta $oferta, Cliente $cliente)
   {
      if ($oferta->movimiento[Componente::Internet] == Movimiento::Alta 
              && !$oferta->producto->Cable->Presente)
      {
         return true;
      }
      return false;
   }
}
