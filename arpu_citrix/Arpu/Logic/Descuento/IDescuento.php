<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\Oferta;
use Arpu\Entity\Cliente;

interface IDescuento
{
   public function AplicaDescuento(Oferta $oferta,Cliente $cliente);
}
