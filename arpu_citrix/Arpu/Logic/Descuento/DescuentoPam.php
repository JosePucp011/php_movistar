<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\Oferta;
use Arpu\Entity\Cliente;

class DescuentoPam implements IDescuento
{
   public function AplicaDescuento(Oferta $oferta, Cliente $cliente)
   {
      if($oferta->producto->Paquete->Tipo == 'Internet')
      {
         return true;
      }
      return false;
   }

}
