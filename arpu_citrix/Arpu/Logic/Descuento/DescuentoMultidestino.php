<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\Oferta;
use Arpu\Entity\Cliente;
use Arpu\Entity\MensajeDescuento;

class DescuentoMultidestino implements IDescuento
{
   public function AplicaDescuento(Oferta $oferta, Cliente $cliente)
   {
      if($oferta->producto->Paquete->Tipo == 'Multidestino')
      {
         $oferta->Comercial[] = (new MensajeDescuento('Descuento 50% x 2 meses'))->toArray();
         return true;
      }
      return false;
   }

}
