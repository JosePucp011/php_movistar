<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\MensajeDescuento;
use Arpu\Entity\Cliente;
use Arpu\Entity\Oferta;
use Arpu\Entity\Operacion; 
use Arpu\Entity\Deco;


class DescuentoCompletaTuTrio implements IDescuento
{
   public static $mensajeDescuento;
   public static $mensajeFV;
   public static $operacion;
   
   public function AplicaDescuento(Oferta $oferta, Cliente $cliente)
   {
      if ($oferta->movimiento['Cable'] == 'Alta'
              && $oferta->movimiento['Internet'] != 'Alta' 
              && $cliente->Internet->Presente)
      {
         if($cliente->Paquete->ProductoId === 2)
         {
              
              $oferta->Registro[] = self::$operacion;
              $oferta->Comercial[] = self::$mensajeDescuento; 
              $oferta->EsCompletaTuTrio = true;
         }
         return true;
      }
      return false;
   }
      
   /**
    * 
    * @param Oferta $oferta
    * @param \Descuento[] $descuentos
    */
   private function ColocarDescuento(Oferta $oferta, $descuentos)
   {
      foreach ($descuentos as $descuento)
      {
         if ($oferta->salto_arpu >= $descuento->Minimo && $oferta->salto_arpu <= $descuento->Maximo)
         {
            $oferta->Registro[] = Operacion::RegistroDescuentoCampana($descuento->Ps, $descuento->Nombre);
            $oferta->Comercial[] = (new MensajeDescuento($descuento->Nombre))->toArray();
            break;
         }
      }
   }
   
}

DescuentoCompletaTuTrio::$mensajeDescuento = ((new MensajeDescuento('Descuento S/.20 x 3 meses'))->toArray());
DescuentoCompletaTuTrio::$mensajeFV = ((new MensajeDescuento('Freeview de HD y HBO x 2 meses'))->toArray());
DescuentoCompletaTuTrio::$operacion = new Operacion( 22012, 'DESCUENTO ALTA S/20 x 3MESES' , 'Alta', '<b><font style="background-color:yellow;">Registro Obligatorio</b>');