<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\Cliente;
use Arpu\Entity\Oferta;
use Arpu\Entity\MensajeDescuento;

class DescuentoHBO implements IDescuento
{
   public function AplicaDescuento(Oferta $oferta, Cliente $cliente)
   {
      if($oferta->producto->Cable->Premium->TieneSenal('HBO'))
      {
          $oferta->Comercial[] = DescuentoHBO::$descuento;
      }
      return false;
   }
   
   public static $descuento;
}

DescuentoHBO::$descuento = ((new MensajeDescuento('S/. 15 de descuento x 3 meses'))->toArray());


