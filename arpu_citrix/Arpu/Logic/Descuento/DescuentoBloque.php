<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\Cliente;
use Arpu\Entity\Oferta;
use Arpu\Entity\MensajeDescuento;

class DescuentoBloque implements IDescuento
{
    public function AplicaDescuento(Oferta $oferta, Cliente $cliente) {
        
        if($oferta->producto->Paquete->Tipo != 'DuoTrio'){
          if($oferta->producto->Cable->Premium->TieneSenal('HP'))
          {
            $oferta->Comercial[] = self::$DecuentoHotPack;
          }

          if($oferta->producto->Cable->Premium->TieneSenal('HBO') 
                  && !$cliente->Cable->Premium->TieneSenal('HBO') )
          {
            $oferta->Comercial[] = self::$DecuentoHBO;
          }

          if($oferta->producto->Cable->Premium->TieneSenal('MC') 
                  && !$cliente->Cable->Premium->TieneSenal('MC') )
          {
            $oferta->Comercial[] = self::$DecuentoFox;
          }

        }
    
    }
    public static $DecuentoHBO;
    public static $DecuentoFox;
    public static $DecuentoHotPack;
}

DescuentoBloque::$DecuentoHBO = ((new MensajeDescuento('S/. 15 de descuento x 3 meses'))->toArray());
DescuentoBloque::$DecuentoFox = ((new MensajeDescuento('S/. 15 de descuento x 3 meses'))->toArray());
DescuentoBloque::$DecuentoHotPack = ((new MensajeDescuento('Descuento 50% x 2 meses'))->toArray());