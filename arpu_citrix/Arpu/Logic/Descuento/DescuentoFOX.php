<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\Cliente;
use Arpu\Entity\Oferta;
use Arpu\Entity\MensajeDescuento;

class DescuentoFOX implements IDescuento
{
   public function AplicaDescuento(Oferta $oferta, Cliente $cliente)
   {
      if($oferta->producto->Cable->Premium->TieneSenal('MC'))
      {
          $oferta->Comercial[] = DescuentoFOX::$descuento;
      }
      return false;
   }
   
   public static $descuento;
}

DescuentoFOX::$descuento = ((new MensajeDescuento('S/. 15 de descuento x 3 meses'))->toArray());


