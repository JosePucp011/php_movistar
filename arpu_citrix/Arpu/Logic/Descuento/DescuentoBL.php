<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\Cliente;
use Arpu\Entity\Oferta;

class DescuentoBL
{
   public static function CalcularDescuentos(Oferta $oferta, Cliente $cliente)
   {
      foreach (self::$Descuentos as $descuento)
      {
         if ($descuento->AplicaDescuento($oferta, $cliente))
         {
            break;
         }
      }
   }
   /**
    *
    * @var \Arpu\Logic\Descuento\IDescuento[]
    */
   public static $Descuentos;

}

DescuentoBL::$Descuentos = array(
            //new DescuentoHBO(),
            new DescuentoFOX(),
            //new DescuentoPam(),
            new DescuentoMultidestino(),
            //new DescuentoBloque(),
            new DescuentoLineaDuoTv(),
            new DescuentoLineaDuoBa(),
            new DescuentoLineaTrio(),
            new DescuentoCompletaTuTrio(),
            new DescuentoMigraciones()
            
);
