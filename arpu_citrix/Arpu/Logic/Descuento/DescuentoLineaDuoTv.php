<?php

namespace Arpu\Logic\Descuento;

use Arpu\Entity\MensajeDescuento;
use Arpu\Data\DescuentoDL;
use Arpu\Entity\Cliente;
use Arpu\Entity\Oferta;
use Arpu\Entity\Componente;
use Arpu\Entity\Movimiento;


class DescuentoLineaDuoTv implements IDescuento
{
   public function AplicaDescuento(Oferta $oferta, Cliente $unused)
   {
      if($oferta->movimiento[Componente::Cable] == Movimiento::Alta &&
              !$oferta->producto->Internet->Presente)
      {
         if($oferta->producto->Paquete->Renta == 101)
         {
            $descuento = DescuentoDL::BuscarDescuento(22018);
            $oferta->Registro[] = Operacion::RegistroDescuentoCampana($descuento->Ps, $descuento->Nombre);
            $oferta->Comercial[] = (new MensajeDescuento($descuento->Nombre))->toArray();
         }
         return true;         
      }
      return false;
   }
}


