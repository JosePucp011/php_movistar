<?php


namespace Arpu\Logic\Descuento;

use Arpu\Entity\Cliente;
use Arpu\Entity\Oferta;
use Arpu\Entity\Componente;
use Arpu\Entity\Movimiento;
use \Arpu\Entity\MensajeDescuento;

/**
 * Description of DescuentoMigraciones
 *
 * @author dvictoriad
 */
class DescuentoMigraciones
{
   public function AplicaDescuento(Oferta $oferta, Cliente $cliente)
   {
      if ($this->EsMigracion($oferta))
      {
         if($cliente->MigracionesPromo != '')
         {
            $oferta->Comercial[] = (new MensajeDescuento($cliente->MigracionesPromo))->toArray();
         }
         return true;
      }

      return false;
   }
   
   private function EsMigracion(Oferta $oferta)
   {  
      if($oferta->salto_arpu < 0)
      {
         return false;
      }
      
      if(in_array($oferta->movimiento[Componente::Cable],array(Movimiento::Alta,Movimiento::Baja)))
      {
         return false;
      }
      
      if(in_array($oferta->movimiento[Componente::Internet],array(Movimiento::Alta,Movimiento::Baja)))
      {
         return false;
      }
      
      if( $oferta->producto->Paquete->Tipo !== 'DuoTrio')
      {
         return false;
      }
      return true;
   }
}
