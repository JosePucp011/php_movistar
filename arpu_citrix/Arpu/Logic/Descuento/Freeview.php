<?php

namespace Arpu\Logic\Descuento;
use Arpu\Entity\Oferta;
class Freeview
{
   public static function AplicarRetenciones(Oferta $oferta, \Cliente $cliente) {
        if (!($cliente->Cable->Premium->TieneSenal('HBO'))) {
            $oferta->Comercial[] = array("Freeview HBO x 1 mes", '', 'Freeview', \Arpu\Entity\CA::ComponenteRegistro_Descuento);
            return true;
        }

        return false;
    }

    public static function IdentificarPs(Oferta $oferta,$senal)
   {  
      $categoria = null;
      if('HD' == $senal)
      {
         
         $categoria = join(' ',array(
             $oferta->producto->Cable->Categoria,
             $senal,
             $oferta->producto->Cable->Tipo));
      }
      else
      {
         $categoria = $oferta->producto->Cable->Categoria.' '.$senal;
      }
      
      if(isset(self::$POSIBLES_PS[$categoria]))
      {
         return self::$POSIBLES_PS[$categoria];
      }
      return null;
   }
   
   private static $POSIBLES_PS = array(
          'CATV HD Estandar' => 21149,
          'CATV HD Estelar' => 22491,
          'CATV MC' => 22494,
          'CATV HBO' => 22495,
          
          'DTH HD Estandar' => 21162,
          'DTH HD Estelar' => 21163,
          'DTH MC' => 22624,
          'DTH HBO' => 22625);
   
}
