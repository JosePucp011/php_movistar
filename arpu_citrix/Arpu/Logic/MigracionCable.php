<?php

namespace Arpu\Logic;
use Arpu\Data\ClienteDL ;
use Arpu\Entity\Oferta;
use Arpu\Entity\Operacion;
use Arpu\Entity\Deco;



class MigracionCable
{
   public static $esModoRetenciones = false;
   public static $ClaseOferta;
   public static function Ejecutar($origen,$destino,Oferta $oferta)
   {
      $cliente = ClienteDL::$cliente;
      if($cliente->Cable->TenenciaDecos['HD'] == 0 && $oferta->producto->Paquete->Tipo !== 'DuoTrio')
      {
         $categoria = $destino->Categoria;
         $oferta->AgregarDeco(Deco::$Decos["${categoria}_HD"]);
         if('CATV' == $categoria)
         {
            $oferta->Registro[] = Operacion::$PuntoDigital;
         }
         
         if($oferta->ClaseOferta == 'Arpu\Logic\Modo\ModoMigracionDown'
                 || $oferta->ClaseOferta == 'Arpu\Logic\Modo\ModoBajas')
         {
            $oferta->AgregarDeco(Deco::$Decos["${categoria}_HD_Alquiler"]);
            $oferta->EntregaDecosAnalogicos = true;
         }
         else
         {
            $oferta->AgregarDeco(Deco::$Decos["${categoria}_HD"]);
         }
         
         if('CATV' == $categoria)
         {
            $oferta->Registro[] = Operacion::$PuntoDigital;
         }
         
      }
      
      PremiumBL::ComparacionPremium($oferta, $origen, $destino);
   }
}
