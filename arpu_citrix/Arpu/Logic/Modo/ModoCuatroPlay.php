<?php
namespace Arpu\Logic\Modo;

use Arpu\Logic\Priorizacion\Criterio_CuatroPlay;
use Arpu\Logic\Priorizacion\Criterio_CuatroPlay_Especifico;
use Arpu\Logic\Priorizacion\Criterio_CuatroPlay_Alta;
use Arpu\Logic\Descuento\DescuentoBL;
use Arpu\Entity\Componente;
use Arpu\Data\ProductoDL;

class ModoCuatroPlay extends IModo {

    public function __construct($cliente,$modo) {
        parent::__construct($cliente,$modo);
        $this->Retenciones = 'CuatroPlay';
    }
    
    public function ProcesarProductos() {

        $productos = ProductoDL::ObtenerConvergente($this->Cliente,$this->Retenciones);
        foreach ($productos as $producto) {
            $this->AgregarOferta($producto);
        }
        $this->PriorizarOfertas();
    }
    
    protected function EjecutarDiagnostico(){
        $noOferta = new \stdClass();
        $noOferta->producto = $this->producto->Paquete->Nombre;
        $noOferta->motivoNoOferta = $this->motivoNoOferta;
        
        $this->Diagnostico[] = $noOferta;
    }
    
     protected function EsProductoValido() {
        $this->motivoNoOferta = "";
        if ($this->Cliente->Alta) {

            if ($this->NoEsCuatroPlay()) {
                return false;
            }

            if (!$this->EsPlan199()) {
                return false;
            }

            if ($this->NoPasaScoringFijo()) {
                return false;
            }

            if ($this->NoPasaScoringMovil()) {
                return false;
            }

            if ($this->EsPermutacion()) {
                return false;
            }         

        } else {
            if ($this->NoEsCuatroPlay()) {
                return false;
            }

            if (!parent::EsProductoValido()) {
                return false;
            }


//            if ($this->EsBajaInternet()) {
//                return false;
//            }
            
            if (!$this->EsPlan199()) {
                return false;
            }
            
            if ($this->EsBajaCable()) {
                return false;
            }

            if ($this->EsRetenciones()) {
                return false;
            }

            if ($this->NoPasaScoringFijo()) {
                return false;
            }
            if ($this->NoPasaScoringMovil()) {
                return false;
            }
           
            /*if ($this->EsDownVelocidad()) {
                return false;
            }*/

            if ($this->EsAltaConPremium()) {
                return false;
            }
			
			
			/*if($this->EsOfertaConRepetidor()){
                return false;
            }*/
            
            /*if($this->EsOfertaFTTH()){
                return false;
            }*/
        }
        return true;
    }

    private function NoEsCuatroPlay() {
        $resultado = $this->producto->Paquete->Tipo != '4Play';
        if($resultado){
            $this->motivoNoOferta = "No es CuatroPlay";
        }
        return $resultado;
    }
    
    protected function ProcesarProducto() {
        parent::ProcesarProducto();
        DescuentoBL::CalcularDescuentos($this->oferta, $this->Cliente);
    }
 
    protected function EsPermutacion() {
        if($this->producto->EsPermutacion){
            $this->motivoNoOferta = 'Permutacion';
            return true;
        }
        return false;
    }
    
    protected function EsOfertaValida() {
        $salto = -500;
        
        if($this->oferta->salto_arpu <= $salto){
            $this->motivoNoOferta = 'Sin Salto Valido';
            $this->EjecutarDiagnostico();
            return false;
        }
        
        if ($this->EsDownDatos()) {
            $this->motivoNoOferta = 'Es Down de datos';
            $this->EjecutarDiagnostico();
                return false;
        }
        
        //Validar si el Cliente es MT
        if($this->Cliente->MovistarTotal) {
            $cantMovPaqueteIgual = 0;

            //Validar si Tenencia de Cliente ya tiene 2 Móviles y es el mismo Paquete que la Oferta
            if($this->oferta->producto->Paquete->Ps == $this->Cliente->Paquete->Ps) {
                if(isset($this->Cliente->Componentes[Componente::Movil0])) {
                    if($this->Cliente->Componentes[Componente::Movil0]->MovistarTotal && 
                       $this->oferta->producto->Paquete->CodigoMovil == $this->Cliente->Componentes[Componente::Movil0]->IdPlan) {
                        $cantMovPaqueteIgual++;
                    }
                }

                if(isset($this->Cliente->Componentes[Componente::Movil1])) {
                    if($this->Cliente->Componentes[Componente::Movil1]->MovistarTotal && 
                       $this->oferta->producto->Paquete->CodigoMovil == $this->Cliente->Componentes[Componente::Movil1]->IdPlan) {
                        $cantMovPaqueteIgual++;
                    }
                }

                if($cantMovPaqueteIgual === 2) {
                    $this->motivoNoOferta = 'Es el Mismo Producto MT';
                    $this->EjecutarDiagnostico();
                    return false;
                }
            }

            //Calcular Suma de Movil Adicional
            $sumaMovilAdic = 0;

            if(isset($this->Cliente->Componentes[Componente::Movil0])) {
                if(!$this->Cliente->Componentes[Componente::Movil0]->MovistarTotal) {
                    $sumaMovilAdic += $this->Cliente->Componentes[Componente::Movil0]->RentaTotal;
                }
            }

            if(isset($this->Cliente->Componentes[Componente::Movil1])) {
                if(!$this->Cliente->Componentes[Componente::Movil1]->MovistarTotal) {
                    $sumaMovilAdic += $this->Cliente->Componentes[Componente::Movil1]->RentaTotal;
                }
            }

            //Validar si es Migración Down MT
            $SaltoArpuTotal = round($this->oferta->salto_arpu+$sumaMovilAdic,2);

            if($SaltoArpuTotal < 0 && $cantMovPaqueteIgual === 0) {
                $this->motivoNoOferta = 'Es Migración Down MT';
                $this->EjecutarDiagnostico();
                return false;
            }
        }
        
        if($this->oferta->producto->Cable->Presente 
                && $this->oferta->movimiento[Componente::Premium] != \Arpu\Entity\CA::Mantiene
                && $this->oferta->movimiento[Componente::Premium] != \Arpu\Entity\CA::NoEvalua
                && $this->oferta->movimiento[Componente::Premium] != 'Alta HD'
                && $this->oferta->movimiento[Componente::Premium] != 'Alta'){
            $this->motivoNoOferta = 'Oferta con Permutacion';
            $this->EjecutarDiagnostico();
            return false;
        }
        
        return true;
    }
    
    public function PriorizarOfertas() {
        if($this->Cliente->Alta){
            $this->Oferta_AltaNueva(0);
        }else{
            if($this->Cliente->MovistarTotal){
                if(isset($this->Cliente->Componentes[Componente::Movil.'0']) 
                    && isset($this->Cliente->Componentes[Componente::Movil.'1']) ){
                    $this->Oferta_MovistarTotal_2_Movil(0);  
                }
                elseif(isset($this->Cliente->Componentes[Componente::Movil.'0'])){
                    $this->Oferta_MovistarTotal_1_Movil(0);   
                }
            }else{
                // PARA DESPOSICIONADO vez 4 // PARA LA VELOCIDAD CONSIDERAR SOLO LOS QUE TIENEN INTERNET PRESENTE
                if ( (isset($this->Cliente->Componentes[Componente::Movil.'0']) && $this->Cliente->Componentes[Componente::Movil.'0']->RentaTotal>=149) 
                    || (isset($this->Cliente->Componentes[Componente::Movil.'1']) && $this->Cliente->Componentes[Componente::Movil.'1']->RentaTotal>=149) 
                    || $this->Cliente->Desposicionado){                   
                    $this->Oferta_Cliente_Deposicionado(-500);
                }   // 
                else if ( ($this->Cliente->Es_DuoBa() || $this->Cliente->Es_Trio()) 
                        && ((isset($this->Cliente->Componentes[Componente::Movil.'0']) && ( ($this->Cliente->Componentes[Componente::Movil.'0']->Producto=="Prepago") || ($this->Cliente->Componentes[Componente::Movil.'0']->Producto=="Postpago") )) 
                        || (isset($this->Cliente->Componentes[Componente::Movil.'1']) && ( ($this->Cliente->Componentes[Componente::Movil.'1']->Producto=="Prepago") || ($this->Cliente->Componentes[Componente::Movil.'1']->Producto=="Postpago") ))  ) ){
                                                          
                    $this->Oferta_CompletaMT_1_o_2_Movil_MenorLimite_229(-500);                          
                }
                else if(isset($this->Cliente->Componentes[Componente::Movil.'0']) 
                    && isset($this->Cliente->Componentes[Componente::Movil.'1'])
                    && isset($this->Cliente->Telefono)){
                    $RentaFijaTotal = round($this->Cliente->Paquete->Renta + $this->Cliente->Componentes[Componente::Movil.'0']->RentaTotal + $this->Cliente->Componentes[Componente::Movil.'1']->RentaTotal, 2);

                    if($RentaFijaTotal <= 480) {
                        if($RentaFijaTotal <= 229){
                            $this->Oferta_CompletaMT_1_o_2_Movil_MenorLimite_229(-500); // mar 5
                        }else{
                            $this->Oferta_CompletaMT_1_o_2_Movil_MenorLimite(0);
                        }     
                    } else {
                        $this->Oferta_CompletaMT_1_o_2_Movil_MayorLimite(0);
                    }
                }
                elseif(isset($this->Cliente->Componentes[Componente::Movil.'0']) 
                    && isset($this->Cliente->Telefono)){
                    $RentaFijaTotal = round($this->Cliente->Paquete->Renta + $this->Cliente->Componentes[Componente::Movil.'0']->RentaTotal, 2);

                    if($RentaFijaTotal <= 310) {
                        if($RentaFijaTotal <= 229){
                            $this->Oferta_CompletaMT_1_o_2_Movil_MenorLimite_229(-500); // mar 3
                        }else{
                            $this->Oferta_CompletaMT_1_o_2_Movil_MenorLimite(0);
                        }   
                    } else {
                        $this->Oferta_CompletaMT_1_o_2_Movil_MayorLimite(0);
                    }
                }
                elseif(isset($this->Cliente->Componentes[Componente::Movil.'0']) 
                    && isset($this->Cliente->Componentes[Componente::Movil.'1'])){
                        $RentaFijaTotal = round($this->Cliente->Componentes[Componente::Movil.'0']->RentaTotal + $this->Cliente->Componentes[Componente::Movil.'1']->RentaTotal, 2);
                        if($RentaFijaTotal <= 229){
                            $this->Oferta_CompletaMT_Solo_2Movil_229(-500); // mar 10
                        }else{
                            $this->Oferta_CompletaMT_Solo_2Movil(0);
                        }    
                }            
                elseif(isset($this->Cliente->Componentes[Componente::Movil.'0'])){
                    $RentaFijaTotal = round($this->Cliente->Componentes[Componente::Movil.'0']->RentaTotal, 2);
                    if($RentaFijaTotal <= 229){
                        $this->Oferta_CompletaMT_Solo_1Movil_229(-500); // mar 6
                    }else{
                        $this->Oferta_CompletaMT_Solo_1Movil(0);
                    }  
                }  
                elseif(isset($this->Cliente->Telefono)){
                    $RentaFijaTotal = round($this->Cliente->Paquete->Renta, 2);
                    if($RentaFijaTotal <= 229){
                        $this->Oferta_CompletaMT_Solo_Fija_229(-500); // mar 11
                    }else{
                        $this->Oferta_CompletaMT_Solo_Fija(0);
                    }  
                }
            } 
        }

        //En el caso que no tenga Ofertas Priorizadas 
        if($this->orden == 0) {
            $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(-500));
        }
        if($this->orden == 0) {
            $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        }
    }
    
    private function Oferta_AltaNueva($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay_Alta($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay_Alta($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay_Alta($salto));
    }
    
    private function Oferta_MovistarTotal_2_Movil($salto) {
        
        if($this->Cliente->Componentes[Componente::Movil.'0']->MovistarTotal 
                && !$this->Cliente->Componentes[Componente::Movil.'1']->MovistarTotal){
        
            $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay_Especifico($this->Cliente->Paquete->Ps,$this->Cliente->Componentes[Componente::Movil.'0']->IdPlan));
        }else{
            $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        }
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(-500));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
    }
    
    private function Oferta_MovistarTotal_1_Movil($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }
    
    private function Oferta_CompletaMT_1_o_2_Movil_MenorLimite($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }
    private function Oferta_Cliente_Deposicionado($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }
    private function Oferta_CompletaMT_1_o_2_Movil_MenorLimite_229($salto) { // mar 4
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));       
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }

    private function Oferta_CompletaMT_1_o_2_Movil_MayorLimite($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }

    private function Oferta_CompletaMT_2_Movil($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }

    private function Oferta_CompletaMT_1_Movil($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }    

    private function Oferta_CompletaMT_Solo_2Movil($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }   

    private function Oferta_CompletaMT_Solo_2Movil_229($salto) { // mar 9
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));       
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }   

    private function Oferta_CompletaMT_Solo_1Movil($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    }   

    private function Oferta_CompletaMT_Solo_1Movil_229($salto) { // mar 7
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
    } 
     
    private function Oferta_CompletaMT_Solo_Fija($salto) {
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        
    } 
    private function Oferta_CompletaMT_Solo_Fija_229($salto) { // mar 8
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay($salto));
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));       
        $this->BuscarVelocidadFiltro(new Criterio_CuatroPlay(0));
        
    }        
    
    private function Replicar_Oferta($Opcion){
     
        if(!is_object($Opcion)){
            return;
        }
        
        $Opcion2 = clone $Opcion;
        $Opcion2->Solo1Movil = 1;
        
        $this->AgregarOpcion($Opcion);
        $this->AgregarOpcion($Opcion2);
    }


    
    private function EsRetenciones() {
        $resultado = 'Retenciones' === $this->producto->TipoComercializacion;
        
        if($resultado){
            $this->motivoNoOferta = 'Retenciones';
        }
        
        return $resultado;
    }
    
    private function EsDownVelocidad() {
        $resultado = $this->producto->Internet->Presente 
                && $this->producto->Internet->Velocidad < $this->Cliente->Internet->Velocidad;
        
        if($resultado){
            $this->motivoNoOferta = 'Es Down velocidad';
        }
        
        return $resultado;
    }    

    
    private function EsDownDatos() {
        $TenenciaDatos = 0;
        if(isset($this->Cliente->Componentes[Componente::Movil.'0']) && isset($this->Cliente->Componentes[Componente::Movil.'0']) != 999999)
        {
            $TenenciaDatos += $this->Cliente->Componentes[Componente::Movil . '0']->Nivel;            
        }
        if(isset($this->Cliente->Componentes[Componente::Movil.'1'])  && isset($this->Cliente->Componentes[Componente::Movil.'1']) != 999999)
        {
            $TenenciaDatos += $this->Cliente->Componentes[Componente::Movil . '1']->Nivel;
        }
        
        if(( $this->producto->Movil0->Nivel + $this->producto->Movil1->Nivel ) < $TenenciaDatos 
            &&    $this->oferta->salto_arpu >= 0
                ){
            $resultado = true;
        } else {
            $resultado = false;
        }
        
        return $resultado;
    }      
    
    private function EsBajaInternet() {
        $resultado = 
                $this->Cliente->Internet->Paquetizado && !$this->producto->Internet->Presente 
                && !$this->producto->EsSva();
        if($resultado){
            $this->motivoNoOferta = 'Es Baja de Internet';
        }
        
        return $resultado;
    }

    private function EsBajaCable() {
        $resultado = $this->Cliente->Cable->Paquetizado && !$this->producto->Cable->Presente 
                && !$this->producto->EsSva();
        if($resultado){
            $this->motivoNoOferta = 'Es Baja de Cable';
        }
        return $resultado;
    }
    private function NoPasaScoringFijo(){
        $resultado = ( $this->Cliente->Linea->Presente && 
                intval($this->Cliente->Scoring['Fijo']) < $this->producto->Paquete->RentaFija )
                || $this->Cliente->Scoring['FijoResultado'] != 'APROBAR';
        
        if($resultado){
            $this->motivoNoOferta = 'No pasa scoring fijo';
        }
        
    
        return $resultado;
    }   

    private function NoPasaScoringMovil(){
	    if($this->Cliente->EsEmpleado > 0
                || (isset($this->Cliente->Componentes[Componente::Movil.'0']) && $this->Cliente->Componentes[Componente::Movil.'0']->EsEmpleado) 
                || (isset($this->Cliente->Componentes[Componente::Movil.'1']) && $this->Cliente->Componentes[Componente::Movil.'1']->EsEmpleado)){
            return false;
        }else{
            $RentaTotalMovil = 0;
            if(isset($this->Cliente->Componentes[Componente::Movil.'0']))
            {
                $RentaTotalMovil += $this->Cliente->Componentes[Componente::Movil . '0']->RentaTotal;
            }
            if(isset($this->Cliente->Componentes[Componente::Movil.'1']))
            {
                $RentaTotalMovil += $this->Cliente->Componentes[Componente::Movil . '1']->RentaTotal;
            }

            $OfertaTotalMovil = intval($this->producto->Movil0->Renta) + intval($this->producto->Movil1->Renta);
            $tresDigitos = intval(substr($this->Cliente->Scoring['Movil1'], 0, 3));
            $LimiteCredito = intval($this->Cliente->Scoring['LimiteCrediticio']);

            $resultado = $OfertaTotalMovil > $LimiteCredito
                        || ($this->Cliente->Scoring['MovilResultado'] != 'APROBAR' && $this->Cliente->Scoring['MovilResultado'] != 'OBSERVAR');

            if($resultado){
                $this->motivoNoOferta = 'No pasa scoring movil';
            }

            return $resultado;
        }  
    }
    
    protected function NoEsTipoComercializacionValida() {
        
        $resultado = $this->producto->TipoComercializacion == 'Bajas';
        if($resultado){
            $this->motivoNoOferta = 'No es tipo comercializacion valida';
        }
        return $resultado;
        
    }
    
    protected function EsAltaConPremium() {
        $resultado = false;

        if($this->Cliente->Cable->Presente == 0 || $this->Cliente->Cable->Presente == null) {
            if ($this->producto->Paquete->CodigoMovil == '4471668' && $this->producto->Paquete->Ps == '23240') {
                if($this->producto->PremiumNombre == '') {
                    return false;
                }
                
                if($this->producto->PremiumNombre == 'HD') {
                    $this->motivoNoOferta = 'Es Alta con Premium';

                    return true;
                }
            }

            $resultado = $this->producto->PremiumNombre != 'HD';
            
            if($resultado){
                $this->motivoNoOferta = 'Es Alta con Premium';
            }
        }
        
        return $resultado;
    }
	
	protected function EsOfertaConRepetidor() {
        return $this->Cliente->Internet->Tecnologia === 'HFC'
                && $this->producto->Paquete->RentaSinModificar >= 299;
    }
    
    protected function EsOfertaFTTH() {
        return ($this->Cliente->Internet->Tecnologia === 'FTTH' || $this->Cliente->Internet->Cobertura->FTTH==1)
                && $this->producto->Paquete->RentaSinModificar >= 269;
    }

    protected function EsPlan199() {
        // Validar si es Plan 199
        if(!($this->producto->Paquete->CodigoMovil == '4471668' && 
             $this->producto->Paquete->Ps == '23240')) { return true; }
        
/*         // Alta Fija - (Con o Sin Móvil)
        if(!$this->Cliente->Internet->Presente && 
           !$this->Cliente->Linea->Presente && 
           !$this->Cliente->Cable->Presente) {
            return true;
        }

        // Mono TV - (Con o Sin Móvil)
        if(!$this->Cliente->Internet->Presente && 
           !$this->Cliente->Linea->Presente && 
           $this->Cliente->Cable->Presente) {
            return true;
        }

        // Naked BA - (Con o Sin Móvil)
        if($this->Cliente->Internet->Presente && 
           !$this->Cliente->Linea->Presente && 
           !$this->Cliente->Cable->Presente) {
            return true;
        }
    
        // Duos BA
        if($this->Cliente->Internet->Presente && 
           $this->Cliente->Linea->Presente && 
           !$this->Cliente->Cable->Presente) {
            // Con Móvil
            if(isset($this->Cliente->Componentes['Movil0']) || 
               isset($this->Cliente->Componentes['Movil1'])) {
                $RentaTotalCliente = $this->Cliente->Paquete->Renta;
                if(isset($this->Cliente->Componentes['Movil0'])) {
                    $RentaTotalCliente += $this->Cliente->Componentes['Movil0']->RentaTotal;
                }
                if(isset($this->Cliente->Componentes['Movil1'])) {
                    $RentaTotalCliente += $this->Cliente->Componentes['Movil1']->RentaTotal;
                }
                $RentaTotalCliente = round($RentaTotalCliente,2);

                return $this->producto->Paquete->RentaFija >= $this->Cliente->Paquete->Renta && 
                       $this->producto->Paquete->RentaSinModificar >= $RentaTotalCliente;
            } else {
                // Sin Móvil
                return $this->producto->Paquete->RentaFija >= $this->Cliente->Paquete->Renta;
            }
        }

        // Trios <= 135
        if($this->Cliente->Internet->Presente && 
           $this->Cliente->Linea->Presente && 
           $this->Cliente->Cable->Presente && 
           $this->Cliente->Paquete->Renta <= 135) {
            // Con Móvil
            if(isset($this->Cliente->Componentes['Movil0']) || 
               isset($this->Cliente->Componentes['Movil1'])) {
                $RentaTotalCliente = $this->Cliente->Paquete->Renta;
                if(isset($this->Cliente->Componentes['Movil0'])) {
                    $RentaTotalCliente += $this->Cliente->Componentes['Movil0']->RentaTotal;
                }
                if(isset($this->Cliente->Componentes['Movil1'])) {
                    $RentaTotalCliente += $this->Cliente->Componentes['Movil1']->RentaTotal;
                }
                $RentaTotalCliente = round($RentaTotalCliente,2);

                return $this->producto->Paquete->RentaSinModificar >= $RentaTotalCliente;
            } else {
                // Sin Móvil
                return true;
            }

        } */

        $RentaTotalCliente = $this->Cliente->Paquete->Renta + $this->Cliente->RentaHD + $this->Cliente->RentaHBO + $this->Cliente->RentaFox;
                
        if(isset($this->Cliente->Componentes['Movil0']) || 
               isset($this->Cliente->Componentes['Movil1'])) {
                
                if(isset($this->Cliente->Componentes['Movil0'])) {
                    $RentaTotalCliente += $this->Cliente->Componentes['Movil0']->RentaTotal;
                }
                if(isset($this->Cliente->Componentes['Movil1'])) {
                    $RentaTotalCliente += $this->Cliente->Componentes['Movil1']->RentaTotal;
                }
                $RentaTotalCliente = round($RentaTotalCliente,2);            
        }

        if($RentaTotalCliente<=199 ){
            return true;
        }



        return false;
    }
}