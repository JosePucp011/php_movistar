<?php
namespace Arpu\Logic\Modo;

class ModoCuatroPlayOnline extends ModoCuatroPlay {

    public function __construct($cliente,$modo) {
        parent::__construct($cliente,$modo);
        $this->Retenciones = 'CuatroPlayOnline';
    }
}