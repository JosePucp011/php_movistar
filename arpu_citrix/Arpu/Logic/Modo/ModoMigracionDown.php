<?php

namespace Arpu\Logic\Modo;

use Arpu\Entity\CA;
use Arpu\Logic\Priorizacion\Criterio_MigracionDown_SaltoMaximo;
use Arpu\Logic\Priorizacion\Criterio_MigracionDown_SaltoMaximo_DuoTvInt_Trio;
use Arpu\Logic\Priorizacion\Criterio_MigracionDown_SaltoMaximo_DuoTvInt_Trio_General;
use Arpu\Logic\Priorizacion\Criterio_CompletaDown_DuoTvInt_Trio;
use Arpu\Logic\Priorizacion\Criterio_CompletaDown_Naked_DuoBa;
use Arpu\Logic\Priorizacion\Criterio_MigracionDown_SaltoMaximo_Naked_DuoBa_General;
use Arpu\Logic\Priorizacion\Criterio_MigracionDown_SaltoMaximo_Naked_DuoBa;
use Arpu\Logic\Priorizacion\Criterio_MigraTrio_DuoBA;

class ModoMigracionDown extends IModo
{
   public function __construct($cliente, $modo)
   {
      parent::__construct($cliente);
      $this->Retenciones = $modo;
      $this->Modalidad = $modo;
   }
   
   protected function EsProductoValido() {
        if (!parent::EsProductoValido() ||
                $this->ProductoDTH_ZonaNoDTH() ||
                $this->EsRetenciones() ||
                $this->EsADSLCoberturaHFCTrobaSaturada() || 
                $this->Cliente->Internet->Cobertura->Iquitos) {
            return false;
        }
        return true;
    }

    

    public function PriorizarOfertas() {
        $this->EliminarPaquetesAdicionales();
        $this->EliminarPaquetesIguales();
        
        if($this->Cliente->Es_Trio()) {
            $this->ReglasTrios();
        } else if ($this->Cliente->Es_DuoBa()) {
            $this->ReglasDuosBa();
        } else if ($this->Cliente->Es_DuoTv()) {
            $this->ReglasDuosTv();
        } elseif($this->Cliente->Es_MonoTv()){
            $this->ReglasMonoTV();
        }elseif($this->Cliente->Es_Naked()){
            $this->ReglasNaked();
        }elseif($this->Cliente->Es_MonoLinea()){
            $this->ReglasNaked();
        }
    }

    public function ReglasMonoTV() {
        $this->Buscar(new Criterio_MigracionDown_SaltoMaximo(0.1));
    }
    public function ReglasMonoLinea() {
        $this->Buscar(new Criterio_MigracionDown_SaltoMaximo(0.1));
    }
    public function ReglasNaked() {
        $this->Buscar(new Criterio_MigracionDown_SaltoMaximo(1));
       $this->Buscar(new Criterio_MigracionDown_SaltoMaximo($this->Salto_OpcionAnterior()));
    }
    public function ReglasTrios() {
        $this->Buscar(new Criterio_CompletaDown_DuoTvInt_Trio(0));

        //Validar si la Primera Oferta es: TRIO - DUO TV BA
        if($this->orden == 1) {
            if($this->ultimaMejor->producto->EsDuoTvInt()) {
                $this->ordenDuoTVBa = 1;
            }
        }
        
        $this->Buscar(new Criterio_MigracionDown_SaltoMaximo_DuoTvInt_Trio($this->Salto_OpcionAnterior(),$this->ordenDuoTVBa));
        $this->Buscar(new Criterio_MigracionDown_SaltoMaximo_DuoTvInt_Trio_General($this->Salto_OpcionAnterior(),1));
        
        if($this->orden <= 2){
			$this->Buscar(new Criterio_MigracionDown_SaltoMaximo_DuoTvInt_Trio($this->Salto_OpcionAnterior(),0));
            //$this->Buscar(new Criterio_MigracionDown_SaltoMaximo_Naked_DuoBa_General($this->Salto_OpcionAnterior(),0,$this->Cliente->Internet->Tecnologia,$this->Cliente->Internet->Cobertura->HFC));
        
        }
        $this->Buscar(new Criterio_MigraTrio_DuoBA(-100));

    }
    public function ReglasDuosBa() {
        if($this->Cliente->Paquete->Renta >= 135) {  
            $this->Buscar(new Criterio_MigracionDown_SaltoMaximo_Naked_DuoBa_General(0,0,$this->Cliente->Internet->Tecnologia,$this->Cliente->Internet->Cobertura->HFC));
            $this->Buscar(new Criterio_MigracionDown_SaltoMaximo_Naked_DuoBa_General($this->Salto_OpcionAnterior(),0,$this->Cliente->Internet->Tecnologia,$this->Cliente->Internet->Cobertura->HFC));
        } else {
            $this->Buscar(new Criterio_CompletaDown_Naked_DuoBa(0,$this->Cliente->Internet->Tecnologia,$this->Cliente->Internet->Cobertura->HFC));

            //Validar si la Primera Oferta es: NAKED - DUO BA
            if($this->orden == 1) {
                if($this->ultimaMejor->producto->EsDuoBa()) {
                    $this->ordenDuoBA = 1;
                }
            }

            $this->Buscar(new Criterio_MigracionDown_SaltoMaximo_Naked_DuoBa($this->Salto_OpcionAnterior(),$this->ordenDuoBA,$this->Cliente->Internet->Tecnologia,$this->Cliente->Internet->Cobertura->HFC));
        }

        if($this->orden <= 3){
            $this->Buscar(new Criterio_MigracionDown_SaltoMaximo_Naked_DuoBa_General($this->Salto_OpcionAnterior(),0,$this->Cliente->Internet->Tecnologia,$this->Cliente->Internet->Cobertura->HFC));
        }
    }
    public function ReglasDuosTv() {
        $this->Buscar(new Criterio_MigracionDown_SaltoMaximo(0));
        $this->Buscar(new Criterio_MigracionDown_SaltoMaximo($this->Salto_OpcionAnterior()));
        $this->Buscar(new Criterio_MigracionDown_SaltoMaximo($this->Salto_OpcionAnterior()));
    }
   
   private function Salto_OpcionAnterior(){
       return (isset($this->ultimaMejor) ? $this->ultimaMejor->salto_arpu : 0.1) - 0.1;
   }
   
   private function ProductoDTH_ZonaNoDTH() {
        return 'DTH' == $this->producto->Cable->Categoria 
                && $this->Cliente->Cable->Cobertura != CA::DTH;
   }
   
   private function EsRetenciones() {
        return 'Regular' === $this->producto->TipoComercializacion;
    }

    private function EsADSLCoberturaHFCTrobaSaturada(){   
         return  $this->Cliente->Internet->Cobertura->HFC>0
                &&  $this->Cliente->Internet->Tecnologia=='ADSL' 
                &&  $this->Cliente->TrobasSaturadas
                &&  ($this->producto->Internet->Tecnologia == 'HFC' ||  $this->producto->Internet->Tecnologia == 'FTTH')
                ;
    }
    private $ordenDuoBA = 0;
    private $ordenDuoTVBa = 0;
}
