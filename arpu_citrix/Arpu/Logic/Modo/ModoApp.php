<?php
namespace Arpu\Logic\Modo;

class ModoApp extends ModoRegular {
    public function PriorizarOfertas() {
        $this->EliminarPaquetesIguales();
        //$this->EliminarPaquetesAdicionales();

        if(!($this->Cliente->BarrioSitiado) && 
           $this->Cliente->Telefono != '15962120' && 
            !($this->EsClusterBlindajeInestable())) {
            //Gestión Regular
            if($this->EsGestionRegular()) {
                if ($this->Cliente->Es_DuoBa() || 
                    $this->Cliente->Es_Trio() ||
                    $this->Cliente->Es_DuoTvInt()) {
                    if(!($this->Cliente->Es_ADSL_Cobertura_UBB() || 
                       $this->Cliente->Es_UBB())) {
                        if(!($this->Cliente->ContactoNoEfectivo != '' && 
                           ($this->Cliente->Es_DuoBa() || 
                           $this->Cliente->Es_Trio()))) {
                            $this->EliminarPaquetesAdicionales();
                        }
                    }
                }
            }
        }

        $this->OfertaTrios();
        $this->RestoClientes();
    }
    
    public function __construct($cliente,$modo) {
        parent::__construct($cliente,$modo);
    }
}