<?php
namespace Arpu\Logic\Modo;

use Arpu\Entity\Cliente;
use Arpu\Logic\Priorizacion\Criterio_X_Naked;
use Arpu\Logic\Priorizacion\Criterio_Trio_Trio_Repetidor_Decos_Bloques;
use Arpu\Logic\Priorizacion\Criterio_Trio_Trio_Bloques;
use Arpu\Logic\Priorizacion\Criterio_Trio_Trio_SinAdicionales;
use Arpu\Logic\Priorizacion\Criterio_DuoBa_DuoBa;
use Arpu\Logic\Priorizacion\Criterio_DuoBa_Trio;
use Arpu\Logic\Priorizacion\Criterio_RepetidorSmart;
use Arpu\Logic\Priorizacion\Criterio_Bloque;
use Arpu\Logic\Priorizacion\Criterio_PuntoAdicionalSmartHD;
use Arpu\Logic\Priorizacion\Criterio_PlanMultidestino;
use Arpu\Logic\Priorizacion\Criterio_SeguridadTotal;
use Arpu\Logic\Priorizacion\Criterio_X_Trio;
use Arpu\Logic\Priorizacion\Criterio_X_Trio_Desposicionado;
use Arpu\Logic\Priorizacion\Criterio_X_DuoBa;
use Arpu\Logic\Priorizacion\Criterio_X_DuoBa_Desposicionado;
use Arpu\Logic\Priorizacion\Criterio_X_DuoTv;
use Arpu\Logic\Priorizacion\Criterio_X_DuoBaTv;
use Arpu\Logic\Descuento\DescuentoBL;


class ModoRegular extends IModo {

    public function __construct($cliente,$modo) {
        parent::__construct($cliente,$modo);
        $this->Retenciones = 'Ninguno';
        $this->Modalidad = $modo;
    }
    
     protected function EsProductoValido() {
        if (!parent::EsProductoValido() ||
                $this->EsBajaInternet() ||
                $this->EsBajaCable() ||
                $this->EsVelocidadMenor()  ||
                $this->SaltoMinimoCompleta()    ||
                $this->SoloCompleta()    ||
                $this->EsMalPagador() ||
                $this->NoMantieneBloques() ||               
                $this->EsLimaADSL() ||
                $this->EsADSL() ||
                $this->EsRentaActualMayor() ||
                $this->EsDuoBA_BajaLinea() ||
                $this->NoEsDuoTrioHFCFTTH() ||       
                $this->EsMonotv_NoDuoTvInternet() ||       
                $this->EsProductoDuoTVBA()
                ) {
            return false;
        }
        return true;
    }
    
    protected function ProcesarProducto() {
        parent::ProcesarProducto();
        DescuentoBL::CalcularDescuentos($this->oferta, $this->Cliente);
    }
    
    protected function EsOfertaValida() {
        $blMontoMaximo = 0;
        $montoMaximo = 20;
        
        if(($this->Cliente->Es_Trio() ||  
           $this->Cliente->Es_DuoTvInt()) && 
           $this->Cliente->Paquete->Renta >= 215 &&
           $blMontoMaximo == 0) {
            $montoMaximo = 7.9;
            $blMontoMaximo = 1;
        }

        if($this->Cliente->Es_DuoBa() && 
           $this->Cliente->Paquete->Renta >= 135 &&
           $blMontoMaximo == 0) {
            $montoMaximo = 7.9;
            $blMontoMaximo = 1;
        }

        //Blindaje - Inestable
        if($this->EsClusterBlindajeInestable() && 
           ($this->Cliente->Es_DuoBa() || 
            $this->Cliente->Es_Trio() ||
            $this->Cliente->Es_DuoTvInt()) && 
            $blMontoMaximo == 0) {
                $montoMaximo = 0;
                $blMontoMaximo = 1;
        }

        //Barrio Sitiado
        if($this->Cliente->BarrioSitiado && 
           ($this->Cliente->Es_DuoBa() || 
            $this->Cliente->Es_Trio() ||
            $this->Cliente->Es_DuoTvInt()) && 
            $blMontoMaximo == 0) {
            $montoMaximo = 6;
            $blMontoMaximo = 1;
        }

        //Contacto No Efectivo
        if($this->Cliente->ContactoNoEfectivo != '' && 
           $blMontoMaximo == 0) {
            $montoMaximo = 15;
            $blMontoMaximo = 1;
        }

        //Gestión Regular
        if($this->EsGestionRegular() && 
           $blMontoMaximo == 0) {
            if ($this->Cliente->Es_DuoBa() || 
                $this->Cliente->Es_Trio() ||
                $this->Cliente->Es_DuoTvInt()) {
                if($this->Cliente->Es_ADSL_Cobertura_UBB()) {
                    $montoMaximo = 10;
                    $blMontoMaximo = 1;
                } else if($this->Cliente->Es_UBB()) {
                    $montoMaximo = 15;
                    $blMontoMaximo = 1;
                }
            }
        }
        
        return $this->oferta->salto_arpu >= ($this->Cliente->Desposicionado ? 1: $montoMaximo) || 
        $this->oferta->producto->Paquete->EsSVA === 1;
    }

    public function PriorizarOfertas() {
        $this->EliminarPaquetesIguales();
        //$this->EliminarPaquetesAdicionales();

        /*if(!($this->Cliente->Telefono == '15962120')) {
            $this->EliminarPaquetesAdicionales();
        }*/

        if(!($this->Cliente->BarrioSitiado) && 
           $this->Cliente->Telefono != '15962120' && 
            !($this->EsClusterBlindajeInestable())) {
            //Gestión Regular
            if($this->EsGestionRegular()) {
                if ($this->Cliente->Es_DuoBa() || 
                    $this->Cliente->Es_Trio() ||
                    $this->Cliente->Es_DuoTvInt()) {
                    if(!($this->Cliente->Es_ADSL_Cobertura_UBB() || 
                       $this->Cliente->Es_UBB())) {
                        if(!($this->Cliente->ContactoNoEfectivo != '' && 
                           ($this->Cliente->Es_DuoBa() || 
                           $this->Cliente->Es_Trio()))) {
                            $this->EliminarPaquetesAdicionales();
                        }
                    }
                }
            }
        }

        $this->OfertaDesposicionado();
        $this->OfertaTrios();
        $this->RestoClientes();
    }
    
    protected function OfertaDesposicionado() {
        
        if($this->Cliente->Desposicionado){
            if ($this->Cliente->Es_Trio()) {
                $this->Buscar(new Criterio_X_Trio_Desposicionado());
            }else{
                $this->Buscar(new Criterio_X_DuoBa_Desposicionado());
            }
        }
    }
    protected function RestoClientes() {
        if ($this->Cliente->Es_DuoBa()) {
            $this->OfertasDuosBa();
        }
        
        $this->EliminarPaquetesAdicionales();

        if ($this->Cliente->Es_MonoLinea()) {
            $this->OfertaLinea();
        } elseif ($this->Cliente->Es_DuoTv()){
            $this->OfertasDuoTV();
        } elseif( $this->Cliente->Es_MonoTv()){
            $this->OfertasMonoTv();
        }elseif( $this->Cliente->Es_Naked()){
            $this->OfertasNaked();
        }
    }
    
    protected function OfertasDuosBa() {
        if($this->Cliente->TrobasSaturadas) {
            $this->Ejecutar_OfertasSVAS(1);
        } elseif($this->EsClusterBlindajeInestable()) {
            $this->Buscar(new Criterio_DuoBa_DuoBa(0,15.50));

            $this->EliminarPaquetesAdicionales();

            if($this->Cliente->TraficoCero) {
                $this->Buscar(new Criterio_X_DuoBaTv(40,65.50));
            } else {
                $this->Buscar(new Criterio_X_Trio(40,65.50));
            }

            $this->Ejecutar_OfertasSVAS(1);
        } elseif($this->Cliente->BarrioSitiado) {
            if($this->Cliente->Es_DuoBa() && 
            ($this->Cliente->Paquete->Renta >=69 && $this->Cliente->Paquete->Renta<=73)){  
                $this->EliminarPaquetesAdicionales();              
                if($this->Cliente->TraficoCero) {                   // jho 3
                    $this->Buscar(new Criterio_X_DuoBaTv(40,65));
                } else {
                    $this->Buscar(new Criterio_X_Trio(40,65));
                }
                $this->Buscar(new Criterio_DuoBa_DuoBa(6,10));
                $this->Ejecutar_OfertasSVAS(1);

        }else{
            $this->Buscar(new Criterio_DuoBa_DuoBa(12,28));
            $this->EliminarPaquetesAdicionales();   
            if($this->orden == 1) {
                if($this->Cliente->TraficoCero) {
                    $this->Buscar(new Criterio_X_DuoBaTv(40,65));
                } else {
                    $this->Buscar(new Criterio_X_Trio(40,65));
                }         
                
                $this->Ejecutar_OfertasSVAS(1);
            }else{
                if($this->Cliente->TraficoCero) {
                    $this->Buscar(new Criterio_X_DuoBaTv(40,65));
                } else {
                    $this->Buscar(new Criterio_X_Trio(40,65));
                }   
               
                $this->Buscar(new Criterio_DuoBa_DuoBa(6,12));
                $this->Ejecutar_OfertasSVAS(1);
            }
        }    

        } elseif($this->Cliente->ContactoNoEfectivo == 'MIGRACION' || 
                 $this->Cliente->ContactoNoEfectivo == 'REPETIDOR' || 
                 $this->Cliente->ContactoNoEfectivo == 'ALTA TV' || 
                 $this->Cliente->ContactoNoEfectivo == 'MIGRACION,REPETIDOR' || 
                 $this->Cliente->ContactoNoEfectivo == 'MIGRACION,ALTA TV' || 
                 $this->Cliente->ContactoNoEfectivo == 'ALTA TV,REPETIDOR' || 
                 $this->Cliente->ContactoNoEfectivo == 'MIGRACION,ALTA TV,REPETIDOR') {
            if($this->Cliente->ContactoNoEfectivo == 'MIGRACION') {
                //SIN FFTT
                if(!($this->Cliente->Internet->Cobertura->HFC >= 1 || 
                    $this->Cliente->Internet->Cobertura->FTTH == 1)) {
                        $this->Ejecutar_OfertasSVAS(1);
                //CON FFTT
                } else {
                    $this->EliminarPaquetesAdicionales();

                    $this->Buscar(new Criterio_X_Trio(55,65.50));
                    $this->Buscar(new Criterio_X_DuoBaTv(55,65.50));

                    $this->Ejecutar_OfertasSVAS(1);
                }
            } elseif($this->Cliente->ContactoNoEfectivo == 'REPETIDOR') {
                //SIN FFTT
                if(!($this->Cliente->Internet->Cobertura->HFC >= 1 || 
                $this->Cliente->Internet->Cobertura->FTTH == 1)) {
                    $this->Ejecutar_OfertasSVAS(0);
                //CON FFTT
                } else {
                    if($this->Cliente->Es_DuoBa() && 
                       $this->Cliente->Paquete->Renta >= 135) {
                        $LimiteMinimo = 7.9;
                    } else {
                        $LimiteMinimo = 15;
                    }

                    $this->Buscar(new Criterio_DuoBa_DuoBa($LimiteMinimo,30));

                    $this->EliminarPaquetesAdicionales();

                    $this->Buscar(new Criterio_X_Trio(15,30));

                    $this->Ejecutar_OfertasSVAS(0);
                }
            } elseif($this->Cliente->ContactoNoEfectivo == 'ALTA TV') {
                //SIN FFTT
                if(!($this->Cliente->Internet->Cobertura->HFC >= 1 || 
                $this->Cliente->Internet->Cobertura->FTTH == 1)) {
                    $this->Ejecutar_OfertasSVAS(1);
                //CON FFTT
                } else {
                    if($this->Cliente->Es_DuoBa() && 
                       $this->Cliente->Paquete->Renta >= 135) {
                        $LimiteMinimo = 7.9;
                    } else {
                        $LimiteMinimo = 12.90;
                    }

                    $this->Buscar(new Criterio_DuoBa_DuoBa($LimiteMinimo,28));

                    $this->EliminarPaquetesAdicionales();

                    $this->Ejecutar_OfertasSVAS(1);
                }
            } elseif($this->Cliente->ContactoNoEfectivo == 'MIGRACION,REPETIDOR') {
                //SIN FFTT
                if(!($this->Cliente->Internet->Cobertura->HFC >= 1 || 
                    $this->Cliente->Internet->Cobertura->FTTH == 1)) {
                    $this->Ejecutar_OfertasSVAS(0);
                //CON FFTT
                } else {
                    $this->EliminarPaquetesAdicionales();

                    $this->Buscar(new Criterio_X_Trio(15,30));
                    $this->Buscar(new Criterio_X_DuoBaTv(15,30));

                    $this->Ejecutar_OfertasSVAS(0);
                }
            } elseif($this->Cliente->ContactoNoEfectivo == 'MIGRACION,ALTA TV') {
                $this->Ejecutar_OfertasSVAS(1);
            } elseif($this->Cliente->ContactoNoEfectivo == 'ALTA TV,REPETIDOR') {
                //SIN FFTT
                if(!($this->Cliente->Internet->Cobertura->HFC >= 1 || 
                $this->Cliente->Internet->Cobertura->FTTH == 1)) {
                    $this->Ejecutar_OfertasSVAS(0);
                //CON FFTT
                } else {
                    if($this->Cliente->Es_DuoBa() && 
                       $this->Cliente->Paquete->Renta >= 135) {
                        $LimiteMinimo = 7.9;
                    } else {
                        $LimiteMinimo = 15;
                    }

                    $this->Buscar(new Criterio_DuoBa_DuoBa($LimiteMinimo,30));

                    $this->EliminarPaquetesAdicionales();

                    $this->Ejecutar_OfertasSVAS(0);
                }
            } elseif($this->Cliente->ContactoNoEfectivo == 'MIGRACION,ALTA TV,REPETIDOR') {
                $this->Ejecutar_OfertasSVAS(0);
            }
        } elseif($this->Cliente->MalaCoberturaSenal) {
            $this->Ejecutar_OfertasSVAS(1);
        } elseif($this->Cliente->TraficoCero) {
            $this->Buscar(new Criterio_X_DuoBaTv(40,65.50));
            
            $this->Ejecutar_OfertasSVAS(1);
        } else {
            /*$this->Buscar(new Criterio_DuoBa_Trio());
            $this->Buscar(new Criterio_DuoBa_DuoBa(0,1000));
            $this->Buscar(new Criterio_RepetidorSmart());*/
            $this->Ejecutar_ADSL_ADSL() ||
            $this->Ejecutar_ADSL_HFC() ||
            $this->Ejecutar_HFC_HFC();
        }
    }

    private function OfertaLinea() {
        if ($this->Cliente->Linea->Nombre != 'PRE') {
            if($this->Cliente->TrobasSaturadas) {
                $this->Ejecutar_OfertasSVAS(1);
            } elseif($this->EsClusterBlindajeInestable()) {
                $this->Buscar(new Criterio_X_DuoBa(40,65.50));
                $this->Buscar(new Criterio_X_DuoTv(40,65.50));
            } elseif($this->Cliente->BarrioSitiado) {
                $this->Buscar(new Criterio_X_DuoBa(40,65.50));
                $this->Buscar(new Criterio_X_DuoTv(40,65.50));
            } elseif($this->Cliente->ContactoNoEfectivo == 'ALTA BA') {
                //SIN FFTT
                if(!($this->Cliente->Internet->Cobertura->HFC >= 1 || 
                    $this->Cliente->Internet->Cobertura->FTTH == 1)) {
                    $this->Ejecutar_OfertasSVAS(1);
                //CON FFTT
                } else {
                    $this->Buscar(new Criterio_X_DuoTv(15,22.50));

                    $this->Ejecutar_OfertasSVAS(1);
                }            
            } else {
                $this->Buscar(new Criterio_X_DuoBa(20,1000));
                $this->Buscar(new Criterio_X_DuoTv(20,1000));
            }
        }
    }
    
    protected function OfertasDuoTV(){
        if($this->Cliente->TrobasSaturadas) {
            $this->Ejecutar_OfertasSVAS(1);
        } elseif($this->EsClusterBlindajeInestable()) {
            $this->Buscar(new Criterio_X_Trio(40,65.50));
            $this->Buscar(new Criterio_X_DuoBaTv(40,65.50));

            $this->Ejecutar_OfertasSVAS(1);
        } elseif($this->Cliente->BarrioSitiado) {
            $this->Buscar(new Criterio_X_Trio(40,65.50));
            $this->Buscar(new Criterio_X_DuoBaTv(40,65.50));

            $this->Ejecutar_OfertasSVAS(1);

        } elseif($this->Cliente->ContactoNoEfectivo == 'ALTA BA') {
            $this->Ejecutar_OfertasSVAS(1);
        } elseif($this->Cliente->TraficoCero) {
            $this->Buscar(new Criterio_X_DuoBaTv(40,65.50));
            
            $this->Ejecutar_OfertasSVAS(1);
        } else {
            $this->Buscar(new Criterio_X_Trio(40,80.50));
            $this->Ejecutar_OfertasSVAS(2);
        }
    }
    
    protected function OfertasMonoTv(){
        $this->Buscar(new Criterio_X_Trio(40,80.50));
        $this->Buscar(new Criterio_X_DuoTv(20,1000));
    }

    protected function OfertasNaked(){
        if($this->Cliente->TrobasSaturadas) { 
            $this->Ejecutar_OfertasSVAS(1);
        }  elseif($this->EsClusterBlindajeInestable()) {
            $this->Buscar(new Criterio_X_Trio(40,65.50));
            $this->Buscar(new Criterio_X_DuoBaTv(40,65.50));
            $this->Buscar(new Criterio_X_DuoBa(40,65.50));
        } elseif($this->Cliente->BarrioSitiado) {
            $this->Buscar(new Criterio_X_Trio(40,65.50));
            $this->Buscar(new Criterio_X_DuoBaTv(40,65.50));
            $this->Buscar(new Criterio_X_DuoBa(40,65.50));
        } elseif($this->Cliente->MalaCoberturaSenal) {
            $this->Ejecutar_OfertasSVAS(1);
        } else {
            $this->Buscar(new Criterio_X_Naked());
        }
    }    
    
    protected function OfertaTrios() {
        if ($this->Cliente->Es_Trio() || $this->Cliente->Es_DuoTvInt()) {
            if($this->Cliente->TrobasSaturadas) { 
                $this->Ejecutar_RestoOfertasTrios();
            } elseif($this->EsClusterBlindajeInestable()) {
                $this->EliminarPaquetesAdicionales();

                if($this->Cliente->Es_Trio()) {
                    $this->Buscar(new Criterio_Trio_Trio_SinAdicionales(0,10.50)) ||
                        $this->Buscar(new Criterio_Trio_Trio_Bloques(0,10.50));
                } else {
                    $this->Buscar(new Criterio_X_DuoBaTv(0,10.50));
                }

                $this->Buscar(new Criterio_X_DuoBaTv(12.90,30)); // 40 65.50 segun fany --> 12.90 30 A
                $this->Ejecutar_RestoOfertasTrios();

            } elseif($this->Cliente->BarrioSitiado) {
                if(($this->Cliente->Es_Trio() ||  
                $this->Cliente->Es_DuoTvInt()) && 
                $this->Cliente->Paquete->Renta >= 215) {
                    $LimiteMinimo = 7.9;
                } else {
                    $LimiteMinimo = 12.90;
                }

                if($this->Cliente->Es_Trio()) {
                    $this->Buscar(new Criterio_Trio_Trio_SinAdicionales($LimiteMinimo,30)) ||
                        $this->Buscar(new Criterio_Trio_Trio_Bloques($LimiteMinimo,30));
                } else {
                    $this->Buscar(new Criterio_X_DuoBaTv($LimiteMinimo,30));
                }

                $this->EliminarPaquetesAdicionales();

                $this->Buscar(new Criterio_X_DuoBaTv(12,31));//

                $this->Ejecutar_RestoOfertasTrios();
            } elseif($this->Cliente->ContactoNoEfectivo == 'MIGRACION' || 
                     $this->Cliente->ContactoNoEfectivo == 'REPETIDOR' || 
                     $this->Cliente->ContactoNoEfectivo == 'MIGRACION,REPETIDOR') {
                if($this->Cliente->ContactoNoEfectivo == 'MIGRACION') {
                    //SIN FFTT
                    if(!($this->Cliente->Internet->Cobertura->HFC >= 1 || 
                       $this->Cliente->Internet->Cobertura->FTTH == 1)) {
                        $this->Ejecutar_OfertasSVAS(1);
                    //CON FFTT
                    } else {
                        $this->EliminarPaquetesAdicionales();

                        if($this->Cliente->Es_Trio()) {
                            $this->Buscar(new Criterio_X_DuoBaTv(10,30));//55,65.50 fany 10 30
                        } else {
                            $this->Buscar(new Criterio_X_Trio(55,65.50));
                        }

                        $this->Ejecutar_OfertasSVAS(1);
                    }
                } elseif($this->Cliente->ContactoNoEfectivo == 'REPETIDOR') {
                    //SIN FFTT
                    if(!($this->Cliente->Internet->Cobertura->HFC >= 1 || 
                    $this->Cliente->Internet->Cobertura->FTTH == 1)) {
                        $this->Ejecutar_OfertasSVAS(0);
                    //CON FFTT
                    } else {
                        if($this->Cliente->Es_Trio()) {
                            $this->Buscar(new Criterio_Trio_Trio_SinAdicionales(15,30)) ||
                                $this->Buscar(new Criterio_Trio_Trio_Bloques(15,30));
                            
                                $this->EliminarPaquetesAdicionales();
                        } else {
                            $this->EliminarPaquetesAdicionales();

                            $this->Buscar(new Criterio_X_DuoBaTv(15,30));
                        }

                        $this->Ejecutar_OfertasSVAS(0);
                    }
                } elseif($this->Cliente->ContactoNoEfectivo == 'MIGRACION,REPETIDOR') {
                    //SIN FFTT
                    if(!($this->Cliente->Internet->Cobertura->HFC >= 1 || 
                    $this->Cliente->Internet->Cobertura->FTTH == 1)) {
                        $this->Ejecutar_OfertasSVAS(0);
                    //CON FFTT
                    } else {
                        $this->EliminarPaquetesAdicionales();

                        if($this->Cliente->Es_Trio()) {
                            $this->Buscar(new Criterio_X_DuoBaTv(15,30));
                        } else {
                            $this->Buscar(new Criterio_X_Trio(15,30));
                        }

                        $this->Ejecutar_OfertasSVAS(0);
                    }
                }
            } elseif($this->Cliente->MalaCoberturaSenal) {
                $this->Ejecutar_RestoOfertasTrios();
            } elseif($this->Cliente->TraficoCero) {
                $this->Buscar(new Criterio_X_DuoBaTv(15,30));

                $this->Ejecutar_RestoOfertasTrios();
            } else {
                $this->Ejecutar_ADSL_ADSL() ||
                $this->Ejecutar_ADSL_HFC() ||
                $this->Ejecutar_HFC_HFC();
            }
        }
    }

    private function Ejecutar_ADSL_ADSL() {
        if ($this->Cliente->Es_ADSL_Cobertura_ADSL()) {
            if($this->Cliente->Es_Trio() || 
               $this->Cliente->Es_DuoTvInt()) {
                $this->Ejecutar_RestoOfertasTrios();
            } elseif($this->Cliente->Es_DuoBa()) {
                $this->Buscar(new Criterio_X_Trio(45,75.50));
                $this->Ejecutar_RestoOfertasTrios();
            }

            return true;
        }
        return false;
    }

    private function Ejecutar_ADSL_HFC() {
        if ($this->Cliente->Es_ADSL_Cobertura_UBB()) {
            if($this->Cliente->Es_Trio()) {
                if($this->Cliente->Paquete->Renta >= 215) {
                    $LimiteMinimo = 7.9;
                } else {
                    $LimiteMinimo = 12.90;
                }

                $this->Buscar(new Criterio_Trio_Trio_SinAdicionales($LimiteMinimo,30)) ||
                    $this->Buscar(new Criterio_Trio_Trio_Repetidor_Decos_Bloques($LimiteMinimo,30));

                $this->EliminarPaquetesAdicionales();
            } elseif($this->Cliente->Es_DuoTvInt()) {
                if($this->Cliente->Paquete->Renta >= 215) {
                    $LimiteMinimo = 7.9;
                } else {
                    $LimiteMinimo = 12.90;
                }

                $this->Buscar(new Criterio_X_DuoBaTv($LimiteMinimo,30));

                $this->EliminarPaquetesAdicionales();
            } elseif($this->Cliente->Es_DuoBa()) {
                $this->Buscar(new Criterio_X_Trio(40,65.50));

                $this->EliminarPaquetesAdicionales();

                if($this->orden == 1) {
                    if($this->Cliente->Paquete->Renta >= 135) {
                        $LimiteMinimo = 7.9;
                    } else {
                        $LimiteMinimo = 12.90;
                    }

                    $this->Buscar(new Criterio_X_DuoBa($LimiteMinimo,30));
                } else {
                    if($this->Cliente->Paquete->Renta >= 135) {
                        $LimiteMinimo = 7.9;
                    } else {
                        $LimiteMinimo = 12;
                    }

                    $this->Buscar(new Criterio_X_DuoBa($LimiteMinimo,28));
                }
            }

            $this->Ejecutar_RestoOfertasTrios();
            
            return true;
        }
        return false;
    }
    
    private function Ejecutar_HFC_HFC() {
        if ($this->Cliente->Es_UBB()) {
            if($this->Cliente->Es_Trio()) {
                if($this->Cliente->Paquete->Renta >= 215) {
                    $LimiteMinimo = 7.9;
                } else {
                    $LimiteMinimo = 17.90;
                }

                $this->Buscar(new Criterio_Trio_Trio_SinAdicionales($LimiteMinimo,30)) ||
                    $this->Buscar(new Criterio_Trio_Trio_Repetidor_Decos_Bloques($LimiteMinimo,30));

                $this->EliminarPaquetesAdicionales();
            } elseif($this->Cliente->Es_DuoTvInt()) {
                if($this->Cliente->Paquete->Renta >= 215) {
                    $LimiteMinimo = 7.9;
                } else {
                    $LimiteMinimo = 17.90;
                }

                $this->Buscar(new Criterio_X_DuoBaTv($LimiteMinimo,30));

                $this->EliminarPaquetesAdicionales();
            } elseif($this->Cliente->Es_DuoBa()) {
                $this->Buscar(new Criterio_X_Trio(45,85.50));
                
                $this->EliminarPaquetesAdicionales();

                if($this->orden == 1) {
                    if($this->Cliente->Paquete->Renta >= 135) {
                        $LimiteMinimo = 7.9;
                    } else {
                        $LimiteMinimo = 15;
                    }

                    $this->Buscar(new Criterio_X_DuoBa($LimiteMinimo,30));
                } else {
                    if($this->Cliente->Paquete->Renta >= 135) {
                        $LimiteMinimo = 7.9;
                    } else {
                        $LimiteMinimo = 12;
                    }
                    
                    $this->Buscar(new Criterio_X_DuoBa($LimiteMinimo,28));
                }
            }
            $this->Ejecutar_RestoOfertasTrios();
            return true;
        }
        return false;
    }
    
    protected function Ejecutar_RestoOfertasTrios(){
        //$this->Buscar(new Criterio_Trio_Trio_SinAdicionales(0,35));
        $this->Ejecutar_OfertasSVAS(1);
    }
    
    private function EsNaked() {
        return $this->producto->Paquete->Tipo != 'Naked' && $this->Cliente->Internet->Naked > 0;
    }
    
    private function EsRetenciones() {
        return 'Retenciones' === $this->producto->TipoComercializacion;
    }
    private function EsMultidestino15() {
        return $this->producto->Paquete->Ps == 21106;
    }

    private function EsBajaInternet() {
        return $this->Cliente->Internet->Paquetizado && !$this->producto->Internet->Presente && !$this->producto->EsSva();
    }

    private function EsBajaCable() {
        return $this->Cliente->Cable->Paquetizado && !$this->producto->Cable->Presente && !$this->producto->EsSva();
    }

    private function EsMalPagador(){
        return $this->Cliente->ComportamientoPagoCovergente === 1;
    }
    private function EsRentaActualMayor(){
        return $this->Cliente->Paquete->Renta >$this->producto->Paquete->Renta && !$this->producto->EsSva();
    }
    private function NoEsDuoTrioHFCFTTH(){ 
        return  (!($this->Cliente->Es_DuoBa() &&  ($this->Cliente->Internet->Tecnologia == 'HFC' || $this->Cliente->Internet->Tecnologia == 'FTTH')) &&
                  !($this->Cliente->Es_Trio() &&  ($this->Cliente->Internet->Tecnologia == 'HFC' || $this->Cliente->Internet->Tecnologia == 'FTTH'))            
                )
                && 
                ($this->producto->EsSva() && ($this->producto->Paquete->Ps == 23021 || $this->producto->Paquete->Ps == 23026));        
    } 
    private function EsMonotv_NoDuoTvInternet(){
        return $this->Cliente->Es_MonoTv() && $this->producto->EsDuoTvInt() ;
    } 
    private function EsDuoBA_BajaLinea(){
        return $this->Cliente->Es_DuoBa() && 
        ($this->producto->Internet->Presente && !$this->producto->Cable->Presente && !$this->producto->Linea->Presente)  
        && !$this->producto->EsSva();
    } 
    
    private function EsVelocidadMenor(){
//        return ($this->Cliente->Paquete->ProductoId === 3 
//                || ($this->Cliente->Paquete->ProductoId === 2 
//                && $this->producto->Paquete->Tipo === 'DuoTrio'
//                && !$this->producto->Cable->Presente
//                ))
//                        && $this->Cliente->Internet->Velocidad > $this->producto->Internet->Velocidad
//                        && $this->producto->Internet->Presente;
      
if($this->Cliente->Es_DuoBa()){            
    return $this->producto->Paquete->Tipo === 'DuoTrio'
            && !$this->producto->Cable->Presente
            && $this->Cliente->Internet->Velocidad > $this->producto->Internet->Velocidad
            && $this->producto->Internet->Presente;
}else{
    return $this->Cliente->Internet->Presente 
    && $this->producto->Internet->Presente
    && $this->Cliente->Internet->Velocidad > $this->producto->Internet->Velocidad;
}
    }
    
    private function SaltoMinimoCompleta(){
        return $this->Cliente->Paquete->ProductoId === 2 
                && $this->producto->Paquete->Tipo === 'DuoTrio'
                && $this->Cliente->Internet->Velocidad > 0
                && $this->producto->Cable->Presente
                    && ( round(($this->Cliente->Internet->Velocidad - $this->producto->Internet->Velocidad)  / $this->Cliente->Internet->Velocidad ,2)  > 0.33
                    ||  $this->producto->Paquete->Renta - $this->Cliente->Paquete->Renta < 50 );
    }
    
    private function SoloCompleta(){
        return ( $this->Cliente->Paquete->ProductoId === 3 && $this->producto->TipoComercializacion === 'Retenciones' )
                || ($this->Cliente->Paquete->ProductoId === 2 && $this->producto->TipoComercializacion === 'Retenciones' && !$this->producto->Cable->Presente);
    }
    
    private function NoMantieneBloques(){   
        
        if($this->producto->Paquete->EsSVA){
            return false;
        }
        if($this->Cliente->Cable->Presente && $this->Cliente->Cable->Premium->TieneSenal('HD')
                && $this->producto->Cable->Presente && !$this->producto->Cable->Premium->TieneSenal('HD'))
        {
            return true;
        }
        if($this->Cliente->Cable->Presente && $this->Cliente->Cable->Premium->TieneSenal('HBO')
                && $this->producto->Cable->Presente && !$this->producto->Cable->Premium->TieneSenal('HBO'))
        {
            return true;
        }
        if($this->Cliente->Cable->Presente && $this->Cliente->Cable->Premium->TieneSenal('MC')
                && $this->producto->Cable->Presente && !$this->producto->Cable->Premium->TieneSenal('MC'))
        {
            return true;
        }
        return false;
    }
    
    private function EsLimaADSL(){
        return  $this->producto->Paquete->Tipo == 'DuoTrio'
                && $this->producto->Internet->Tecnologia == 'ADSL'
                && substr($this->Cliente->Telefono, 0, 1)   == 1;
    }
    private function EsADSL(){
        return  $this->Cliente->SinOfertaADSL == 1 
                && $this->Cliente->Internet->Cobertura->HFC == 0
                && $this->Cliente->Internet->Tecnologia == 'ADSL' 
                && $this->producto->Internet->Tecnologia == 'ADSL' 
                && (($this->Cliente->Es_DuoBa() && $this->producto->EsDuoBa()) 
                    || ($this->Cliente->Es_Trio() && $this->producto->EsTrio()) 
                    || ($this->Cliente->Es_DuoBa() && $this->producto->EsTrio() && 
                        $this->producto->Internet->Velocidad > $this->Cliente->Internet->Velocidad));
    }
    private function EsProductoDuoTVBA(){
        return  $this->producto->Paquete->Tipo == 'DuoTrio' && 
                $this->producto->Internet->Presente && 
                !$this->producto->Linea->Presente && 
                $this->producto->Cable->Presente && 
                $this->Cliente->Es_Trio() && 
                $this->Modalidad != 'Regular' && 
                $this->Modalidad != 'App';
    }
    protected function EsClusterBlindajeInestable() {
        return ($this->Cliente->Inestable && 
        $this->Cliente->ZonaCompetencia && 
        ($this->Cliente->OperadorZonaCompetencia=='Claro,Bitel' || 
         $this->Cliente->OperadorZonaCompetencia=='Bitel')
         ) ||
         $this->Cliente->ZonaBlindaje==1                
         ;
    }
    protected function EsGestionRegular() {
        return  !($this->Cliente->TrobasSaturadas || 
                $this->EsClusterBlindajeInestable() || 
                $this->Cliente->BarrioSitiado || 
                $this->Cliente->ContactoNoEfectivo != '' || 
                $this->Cliente->MalaCoberturaSenal || 
                $this->Cliente->TraficoCero);
    }
    protected function Ejecutar_OfertasSVAS($RepetidorSmart){
        if($RepetidorSmart) {
            $this->Buscar_Simple(new Criterio_RepetidorSmart());
        }

        $this->Buscar_Simple(new Criterio_PuntoAdicionalSmartHD());
        $this->Buscar_Simple(new Criterio_Bloque('HD'));
        $this->Buscar_Simple(new Criterio_Bloque('HBO'));
        $this->Buscar_Simple(new Criterio_Bloque('MC'));
        /*$this->Buscar_Simple(new Criterio_PlanMultidestino());
        $this->Buscar_Simple(new Criterio_SeguridadTotal());*/
    }
}