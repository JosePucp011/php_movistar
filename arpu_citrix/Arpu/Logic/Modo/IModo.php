<?php
namespace Arpu\Logic\Modo;

use Arpu\Entity\Cliente;
use Arpu\Entity\Movil;
use Arpu\Entity\Oferta;
use Arpu\Entity\Operacion;
use Arpu\Entity\Producto;
use Arpu\Entity\Componente;
use Arpu\Entity\Movimiento;
use Arpu\Entity\CA;

use Arpu\Data\ProductoDL;
use Arpu\Data\PremiumDL;
use Arpu\Logic\Filtros\FiltrosGeneralesProducto;
use Arpu\Entity\ResultadoConsulta;


abstract class IModo {
    
    public function ObtenerResultadoConsulta(){
        $resultado = new ResultadoConsulta();
        $resultado->Cliente = $this->Cliente;
        $resultado->Retenciones = $this->Retenciones;
        $resultado->Modalidad = $this->Modalidad;
        $resultado->Opciones = $this->Opciones;
        $resultado->Ofertas = $this->Ofertas;
        $resultado->Diagnostico = $this->Diagnostico;
        
        return $resultado;
    }
    
    protected $Diagnostico = [];

    public function ProcesarProductos() {
        $productos = $this->ObtenerProductos();
        
        foreach ($productos as $producto) {
            $this->AgregarOferta($producto);
        }
        $this->PriorizarOfertas();
    }
    
    protected function ObtenerProductos(){
        return ProductoDL::Obtener($this->Cliente);
    }

    protected function AgregarOferta($producto) {
        $this->producto = $producto;
        if (!$this->EsProductoValido()) {
            $this->EjecutarDiagnostico();
            return;
        }
        $this->ProcesarProducto();
        
        if (!$this->EsOfertaValida()) {
            return;
        }        
        $this->Ofertas[] = $this->oferta;
    }
    
    protected function EjecutarDiagnostico(){
        
    }
    
    
    
    protected function EsProductoValido() {
        $filtros = new FiltrosGeneralesProducto($this->Cliente, $this->producto,$this->Modalidad);
        
        if($filtros->Es_ProductoCliente_NoValido()){
            $this->motivoNoOferta = $filtros->motivoNoOferta;
            
            return false;
        } elseif($this->NoEsTipoComercializacionValida()) {
            return false;
        }
        
        
        
        return true;
    }
    
    protected function ProcesarProducto() {
        $this->oferta = new Oferta($this->producto);
        $this->oferta->ClaseOferta = get_class($this);
        
        $this->CalcularMovimientos();
        $this->CalcularOperacionComercial();
        $this->EliminarDescuentosPermanentes();
        $this->TransformarGPON();
		$this->AsignarPromocionVelocidad();
        $this->AsignarMigracionTecnica();
        $this->AsignarPromocionEquipamientoOnline();
        $this->PromocionMigracion();
    }
    
    private function EliminarDescuentosPermanentes(){
        if($this->producto->Paquete->Tipo === 'DuoTrio'){
            foreach($this->Cliente->Paquete->DescuentoPermanente as $ps){
                $this->oferta->Registro[] = Operacion::BajaDescuentoPermanente($ps);
            }
        }
    }
    
    protected function EsOfertaValida() {
        return true;
    }

    private function CalcularMovimientos() {
        if($this->producto->Paquete->Tipo === 'DuoTrio'){
            $this->ProcesarDuoTrio();
        } elseif($this->producto->Paquete->Tipo === 'Deco'){
            $this->ProcesarSVATv();
        } elseif($this->producto->Paquete->Tipo === 'Multidestino'){
            $this->ProcesarSva();
        } elseif($this->producto->Paquete->Tipo === 'Bloque'){
            $this->ProcesarSVATv();
        } elseif($this->producto->Paquete->Tipo === 'Internet'){
            $this->ProcesarSva();
        } elseif($this->producto->Paquete->Tipo === '4Play') {
            $this->Procesar4Play();
        } else {
            $this->ProcesarDuoTrio();
        }
        
        $this->AsignarMonoproductos();
        $this->AgregarBloqueEstelar();
    }
    
    private function Procesar4Play(){
        $this->oferta->arpu = $this->Cliente->Paquete->Renta;
        $componentes = array(Componente::Linea, Componente::Cable, Componente::Internet, Componente::Plan);

        foreach ($componentes as $Tipo) {
            $origen = $this->Cliente->Componentes[$Tipo];
            $destino = $this->producto->Componentes[$Tipo];
            $origen->Comparar($Tipo, $this->oferta, $destino);
        }
        
        
        for($i = 0; ;++$i){
            $variable = "Movil$i";
            if(isset($this->producto->Componentes[$variable])){
                $origen = isset($this->Cliente->Componentes[$variable]) ? $this->Cliente->Componentes[$variable] : new Movil();
                $destino = $this->producto->Componentes[$variable];
                $origen->Comparar($variable,$this->oferta,$destino);
                //$this->CalcularCAM($variable,$destino);
            } else {
                break;
            }
        }
            
        $this->oferta->salto_arpu = round($this->producto->Paquete->Renta - $this->oferta->arpu, 2);
        $this->oferta->cargo_fijo_destino = round($this->oferta->Impacto_ArpuSuelto + $this->oferta->arpu + $this->oferta->salto_arpu, 2);
    }
    
    private function ProcesarSva() {
        $this->oferta->arpu = 0;
        $this->oferta->salto_arpu = $this->oferta->producto->Paquete->Renta;
        $this->oferta->cargo_fijo_destino = round($this->Cliente->RentaTotal + $this->oferta->salto_arpu, 2);
    }

    private function ProcesarSVATv() {
        $this->oferta->arpu = 0;
        $this->oferta->salto_arpu = $this->oferta->producto->Paquete->Renta;
        $this->oferta->cargo_fijo_destino = $this->Cliente->RentaTotal + $this->oferta->salto_arpu;
        $this->Cliente->Cable->Comparar('Cable', $this->oferta, $this->oferta->producto->Cable);
        $this->oferta->movimiento[Componente::Cable] = Movimiento::NoEvalua;
        $this->oferta->cargo_fijo_destino = round($this->oferta->cargo_fijo_destino, 2);
    }

    private function ProcesarDuoTrio(){
        $this->oferta->arpu = $this->Cliente->Paquete->Renta;
        $componentes = array(Componente::Linea, Componente::Cable, Componente::Internet, Componente::Plan);

        foreach ($componentes as $Tipo) {
            $origen = $this->Cliente->Componentes[$Tipo];
            $destino = $this->producto->Componentes[$Tipo];
            $origen->Comparar($Tipo, $this->oferta, $destino);
        }
            
        $this->oferta->salto_arpu = round($this->producto->Paquete->Renta - $this->oferta->arpu, 2);
        $this->oferta->cargo_fijo_destino = round($this->oferta->Impacto_ArpuSuelto + $this->oferta->arpu + $this->oferta->salto_arpu, 2);
    }

    private function TransformarGPON() {
        if ($this->oferta->producto->Internet->Tecnologia === 'HFC' 
                && $this->Cliente->Internet->Cobertura->FTTH) {
            $this->oferta->Tecnologia = 'FTTH';
        }
    }
    private function AsignarMigracionTecnica() {
        if($this->Retenciones === 'MigracionDown' && ($this->Cliente->TiempoMigracionTecn === '48 horas' || $this->Cliente->TiempoMigracionTecn === '7 dias' )){
            $this->Cliente->TiempoMigracionTecn = '5 dias'  ; // 7 dias
        }elseif ($this->Retenciones === 'Baja' && ($this->Cliente->TiempoMigracionTecn === '48 horas' || $this->Cliente->TiempoMigracionTecn === '7 dias' )) {
            $this->Cliente->TiempoMigracionTecn = '5 dias'  ;
        }

    }
    
    private function AsignarPromocionVelocidad() {
        if (($this->oferta->producto->Internet->Tecnologia === 'HFC' || $this->oferta->producto->Internet->Tecnologia === 'FTTH' )
                && $this->oferta->producto->Paquete->Tipo === 'DuoTrio'
                && $this->Retenciones === 'Ninguno') {
            if ($this->oferta->producto->Internet->Velocidad < 60000) {
                $this->oferta->Comercial[] = array('60 Mbps x 2 meses', '', 'Velocidad', CA::ComponenteRegistro_Descuento);
                $this->oferta->Registro[] = new Operacion('23100', '60 Mbps x 2 meses', '', 'Alta', '');
            }
        }
    }

    private function AsignarPromocionEquipamientoOnline() {
        
        if( $this->Modalidad === 'Online' 
                && ($this->oferta->producto->Paquete->Ps == 22646
                || $this->oferta->producto->Paquete->Ps == 21933
                || $this->oferta->producto->Paquete->Ps == 23021
                || $this->oferta->producto->Paquete->Ps == 23025)){
            
                $this->oferta->Comercial[] = array('S/. 10 de descuento x 1 mes', '', 'Velocidad', CA::ComponenteRegistro_Descuento);
                $this->oferta->Registro[] = new Operacion('22160', 'S/. 10 de descuento x 1 mes', '', 'Alta', '');
				
        }
    }

    private function PromocionMigracion(){
        $blMostrarDescuento = 0;

        if($blMostrarDescuento == 0) {
            $blValDesc02072019Al05072019 = false;// activar segun fany
            if(($this->Modalidad === 'Online' || $this->Modalidad === 'Regular') && $this->producto->Paquete->Tipo === 'DuoTrio' && $blValDesc02072019Al05072019){
                if($this->Cliente->Es_Trio() && $this->producto->EsTrio() && $blMostrarDescuento == 0) {
                        $this->oferta->Comercial[] = array('Desc S/10 x 3 primeros meses', '', 'Velocidad', CA::ComponenteRegistro_Descuento);

                        $blMostrarDescuento = 1;
                }

                if($this->Cliente->Es_DuoBa() && $this->producto->EsDuoBa() && $blMostrarDescuento == 0) {
                        $this->oferta->Comercial[] = array('Desc S/10 x 3 primeros meses', '', 'Velocidad', CA::ComponenteRegistro_Descuento);

                        $blMostrarDescuento = 1;
                }
            }
        }

        if($blMostrarDescuento == 0) {
            $blValDesc15032019Al19032019 = false;
            if($this->Modalidad === 'Online' && $this->producto->Paquete->Tipo === 'DuoTrio' && $blValDesc15032019Al19032019){
                if(($this->Cliente->Es_Trio() && $this->producto->EsTrio()) || 
                    ($this->Cliente->Es_DuoBa() && $this->producto->EsDuoBa())) {

                    $this->oferta->Comercial[] = array('S/. 10 de descuento x 6 mes', '', 'Velocidad', CA::ComponenteRegistro_Descuento);
                        
                    $blMostrarDescuento = 1;
                }
            }
        }

        if($this->Modalidad === 'Online' && $this->producto->Paquete->Tipo === 'DuoTrio'){
            if((!$this->producto->Cable->Presente && $this->producto->Internet->Presente && $this->producto->Linea->Presente && $this->Cliente->Paquete->ProductoId == 2)
                    || ($this->producto->Cable->Presente && $this->producto->Internet->Presente && $this->producto->Linea->Presente && $this->Cliente->Paquete->ProductoId == 3)){
                if($blMostrarDescuento == 0) {
                    $this->oferta->Comercial[] = array('S/. 10 de descuento x 3 mes', '', 'Velocidad', CA::ComponenteRegistro_Descuento);
                    $this->oferta->Registro[] = new Operacion('22437', 'S/. 10 de descuento x 3 mes', '', 'Alta', '');        
                
                    $blMostrarDescuento = 1;
                }
            }
        }
    }    
	
    private function AsignarMonoproductos() {
        foreach ($this->Cliente->Componentes as $Tipo => $componenteOrigen) {
            if ($this->oferta->movimiento[$Tipo] == Movimiento::MantieneMono) {
                $this->AsignarComponenteMonoproducto($Tipo, $componenteOrigen);
            }
        }
    }
    
    private function AsignarComponenteMonoproducto($Tipo, Componente $componenteOrigen) {
        $this->oferta->ComponentesMono[$Tipo] = $componenteOrigen;
        if ($Tipo == Componente::Cable) {
            if ($componenteOrigen->Premium->Presente && $componenteOrigen->Premium->RentaMonoproducto > 0) {
                $this->oferta->movimiento[Componente::Premium] = Movimiento::MantieneMono;
                $this->oferta->ComponentesMono[Componente::Premium] = $componenteOrigen->Premium;
            }
        }
    }

    private function CalcularOperacionComercial() {
        if (in_array($this->oferta->producto->Paquete->Tipo, array('Multidestino', 'Internet', 'Bloque', 'Deco'))) {
            $this->oferta->setOperacionComercial('SVA');
        } elseif ($this->oferta->movimiento['Linea'] == 'Alta' && $this->oferta->movimiento['Internet'] == 'Alta' && $this->oferta->movimiento['Cable'] == 'Alta') {
            $this->oferta->setOperacionComercial('Alta');
        } elseif ($this->oferta->movimiento['Linea'] == 'Alta' && $this->oferta->movimiento['Internet'] == 'Alta') {
            $this->oferta->setOperacionComercial('Completa Linea y BA');
        } elseif ($this->oferta->movimiento['Internet'] == 'Alta' && $this->oferta->movimiento['Cable'] == 'Alta') {
            $this->oferta->setOperacionComercial('Completa TV y BA');
        } elseif ($this->oferta->movimiento['Linea'] == 'Baja' && $this->oferta->movimiento['Cable'] == 'Alta') {
            $this->oferta->setOperacionComercial('Baja Linea y Completa TV');
        } elseif ($this->oferta->movimiento['Linea'] == 'Baja' && $this->oferta->movimiento['Internet'] == 'Alta') {
            $this->oferta->setOperacionComercial('Baja Linea y Completa BA');
        } elseif ($this->oferta->movimiento['Linea'] == 'Baja' &&( $this->oferta->movimiento['Internet'] == 'Up' || $this->oferta->movimiento['Internet'] == 'Mantiene' ) ) {
            $this->oferta->setOperacionComercial('Baja Linea y Mantiene Internet');
        }elseif ($this->oferta->movimiento['Cable'] == 'Alta' && $this->oferta->movimiento['Linea'] == 'Alta') { //
            $this->oferta->setOperacionComercial('Completa TV y Linea');
        }elseif ($this->oferta->movimiento['Linea'] == 'Alta') {
            $this->oferta->setOperacionComercial('Completa Linea');
        } elseif ($this->oferta->movimiento['Cable'] == 'Alta') {
            $this->oferta->setOperacionComercial('Completa TV');
        } elseif ($this->oferta->movimiento['Internet'] == 'Alta') {
            $this->oferta->setOperacionComercial('Completa BA');
        } elseif ($this->oferta->movimiento['Internet'] == 'Baja' || $this->oferta->movimiento['Cable'] == 'Baja' ||     $this->oferta->movimiento['Linea'] == 'Baja') {
            $this->oferta->setOperacionComercial('Baja Componente');
        } else {
            $this->oferta->setOperacionComercial('Migracion');
        }
    }

    private function AgregarBloqueEstelar()
    {
        if($this->Cliente->Cable->Tipo === 'Estelar' 
                && $this->oferta->producto->Cable->Presente 
                && $this->oferta->producto->Paquete->Tipo != 'Bloque')
        {
            $bloquePs = $this->Cliente->Cable->Categoria != 'CATV' ? 21165 : 21152;
            $Premium = PremiumDL::Construir(false,$bloquePs);
            $this->oferta->ComponentesMono['Premium'] = $Premium;
            
            $this->oferta->Impacto_ArpuSuelto +=  $Premium->RentaMonoproducto;
            $this->oferta->salto_arpu += $Premium->RentaMonoproducto;
            $this->oferta->cargo_fijo_destino += $Premium->RentaMonoproducto;
            $this->oferta->salto_arpu = round($this->oferta->salto_arpu,2);
            $this->oferta->cargo_fijo_destino = round($this->oferta->cargo_fijo_destino,2);
            $this->oferta->Registro[] = new Operacion($bloquePs,$Premium->PremiumBloque[0]->PremiumNombre, '', 'Alta');  
        }
    }
   
    public function AgregarOpcion($opcion) {
        if (null != $opcion && $this->orden < 3 ) {
            ++$this->orden;
            $opcion->mejor_up = 1;
            $opcion->orden = array($this->orden);
            $this->Opciones[$this->orden] = $opcion;
            $this->ultimaMejor = $opcion;

            return true;
        }
        return false;
    }
    
    protected function Buscar($criterio){
        $mejor = null;
        foreach ($this->Ofertas as $actual) {
            if(!($this->Modalidad !== 'Baja' && 
              $this->Modalidad !== 'MigracionDown' && 
              $this->Modalidad !== 'Regular' && 
              $this->Modalidad !== 'App' && 
              $actual->producto->Cable->Presente &&
              $actual->producto->Internet->Presente && 
              !$actual->producto->Linea->Presente)) {
                if ($criterio->Es($actual) && $criterio->EsMejor_o_Nulo($actual,$mejor)) {
                    $mejor = $actual;
                }
            }
        }
        return $this->AgregarOpcion($mejor);
    }
    protected function BuscarVelocidadFiltro($criterio){ // vez 1
        $mejor = null;        
        foreach ($this->Ofertas as $actual) {
            if(!($this->Modalidad !== 'Baja' && 
              $this->Modalidad !== 'MigracionDown' && 
              $this->Modalidad !== 'Regular' && 
              $this->Modalidad !== 'App' && 
              $actual->producto->Cable->Presente &&
              $actual->producto->Internet->Presente && 
              !$actual->producto->Linea->Presente)) {
                if ($criterio->Es($actual) && $criterio->EsMejor_o_Nulo($actual,$mejor)) {                    
                    if ($this->VerificarVelocidad($actual)){
                        $mejor = $actual;
                    }  
                    
                }
            }
        }
        return $this->AgregarOpcion($mejor);
    }

    private function VerificarVelocidad($ofertactual){ // vez 1
        $velocidadCliente=($this->Cliente->Internet->Velocidad/1000);
        $velocidadProducto=($ofertactual->producto->Internet->Velocidad/1000);

        if($velocidadCliente>=0 && $velocidadCliente<=35 && ($velocidadProducto) <20 ){ 
            return false;
         }
         elseif ($velocidadCliente>=36 && $velocidadCliente<=45 && ($velocidadProducto) <30 ) { 
             return false;
         }
         elseif ($velocidadCliente>=36 && $velocidadCliente<=45 && ($velocidadProducto) <30 ) { 
             return false;
         }
         elseif ($velocidadCliente>=46 && $velocidadCliente<=49 && ($velocidadProducto) <40 ) { 
             return false;
         }
         elseif ($velocidadCliente==60 && ($velocidadProducto) <40 ) { 
             return false;
         }
         elseif ($velocidadCliente>=61 && $velocidadCliente<=80 && ($velocidadProducto) <60 ) { 
              
             return false;
         }
         elseif ($velocidadCliente>=81 && $velocidadCliente<=99 && ($velocidadProducto) <120 ) { 
             return false;
         }
         elseif ($velocidadCliente==100 && ($velocidadProducto) <60 ) { 
             return false;
         }
         elseif ($velocidadCliente>=101 && $velocidadCliente<=119 && ($velocidadProducto) <120 ) { 
             return false;
         }
         elseif ($velocidadCliente==120 && ($velocidadProducto) <60 ) { 
             return false;
         }
         elseif ($velocidadCliente>=121 && $velocidadCliente<=180 &&  ($velocidadProducto) <120 ) { 
             return false;
         }
         elseif ($velocidadCliente>=181 && $velocidadCliente<=199 && ($velocidadProducto) <200 ) { 
             return false;
         }
         elseif ($velocidadCliente==200 && ($velocidadProducto) <120 ) { 
             return false;
         }else{
             return true;
         }

    }

    protected function Buscar_Devolver($criterio){
        $mejor = null;
        foreach ($this->Ofertas as $actual) {
            if ($criterio->Es($actual) && $criterio->EsMejor_o_Nulo($actual,$mejor)) {
                $mejor = $actual;
            }
        }
        return $mejor;
    }    
    
    protected function Buscar_Simple($criterio) {
        foreach ($this->Ofertas as $oferta) {
            if ($criterio->Es($oferta)) {
                return $this->AgregarOpcion($oferta);
            }
        }
    }

    abstract public function PriorizarOfertas();

    public static $modos = array('Ninguno', 'Baja', 'MigracionDown', 'Averias', 'Online', 'App');
    protected $orden = 0;
    protected $productos;
    protected $blEliminarPaqAdicionales = 0;
    /**
     * @var Cliente 
     */
    public $Cliente;

    /**
     * @var Oferta
     */
    public $oferta;
    public $motivoNoOferta;
    
    /**
     * @var Oferta
     */
    public $ultimaMejor;

    
    /**
     *
     * @var Producto
     */
    
    public $producto;
    public $modo;
    public $Modalidad;
    public $Retenciones;
    public $Ofertas = array();

    /**
     * @var Ofertas[]
     */
    public $Opciones;
    public $Mensaje;

    public function __construct($cliente) {
        $this->Cliente = $cliente;
        $this->Ofertas = array();
        $this->Opciones = Array();
    }
    
    protected function EliminarPaquetesAdicionales() {
        if($this->blEliminarPaqAdicionales == 0) {
            $ofertas = array();
            foreach ($this->Ofertas as $oferta) {
                if ($oferta->producto->DecosAdicionales > 0 || $oferta->producto->Ultrawifi > 0) {
                    continue;
                }
                $ofertas[] = $oferta;
            }
            $this->Ofertas = $ofertas;

            $this->blEliminarPaqAdicionales = 1;
        }
    }
    protected function EliminarPaquetesIguales() {
        
        $ofertas = array();
        foreach ($this->Ofertas as $oferta) {
            if ($oferta->movimiento['Cable'] == 'Mantiene'
                    && $oferta->movimiento['Internet'] == 'Mantiene' 
                    && $oferta->movimiento['Linea'] == 'Mantiene' 
                    && $oferta->movimiento['Premium'] == 'Mantiene' ) {
                continue;
            }
            if ($oferta->movimiento['Cable'] == 'Mantiene'
                    && $oferta->movimiento['Internet'] == 'Mantiene' 
                    && $oferta->movimiento['Linea'] == 'Mantiene' 
                    && $oferta->movimiento['Premium'] == 'No Evalua Componente' ) {
                continue;
            }            
            $ofertas[] = $oferta;
        }
        $this->Ofertas = $ofertas;
    }

    protected function NoEsTipoComercializacionValida() {
        $this->motivoNoOferta = 'No es tipo comercializacion valida';
        return $this->producto->TipoComercializacion == 'Bajas';
    }
}
