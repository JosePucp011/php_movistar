<?php

namespace Arpu\Logic\Modo;

use Arpu\Logic\Priorizacion\Criterio_DuoBa_DuoBa;
use Arpu\Logic\Priorizacion\Criterio_DuoBa_Trio;
use Arpu\Logic\Priorizacion\Criterio_RepetidorSmart;
use Arpu\Logic\Priorizacion\Criterio_Trio_Trio_SinAdicionales;
use Arpu\Logic\Priorizacion\Criterio_Bloque;
use Arpu\Logic\Priorizacion\Criterio_PuntoAdicionalSmartHD;
use Arpu\Logic\Priorizacion\Criterio_Trio_Trio;

class ModoOnline extends ModoRegular{

    public function __construct($cliente,$modo) {
        parent::__construct($cliente,$modo);
    }

    protected function OfertasDuosBa() {
        if ($this->Cliente->Es_DuoBa()) {
        $this->Buscar(new Criterio_DuoBa_DuoBa(0,1000,$this->Modalidad,0,0,$this->Cliente->SaltoCero));
        $this->Buscar(new Criterio_DuoBa_Trio());
        $this->Buscar(new Criterio_RepetidorSmart());}
    }
    
    protected function OfertaTrios() {
        if ($this->Cliente->Es_Trio()) {
            $this->Ejecutar_RestoOfertasTrios();
        }
    }    
    
    protected function Ejecutar_RestoOfertasTrios(){
        $this->Buscar(new Criterio_Trio_Trio());
        $this->Buscar(new Criterio_Trio_Trio_SinAdicionales(0,35,$this->Modalidad,0,0,$this->Cliente->SaltoCero));
        
        $this->Buscar_Simple(new Criterio_Bloque('HD'));
        $this->Buscar_Simple(new Criterio_Bloque('MC'));
        $this->Buscar_Simple(new Criterio_Bloque('HBO'));
        $this->Buscar_Simple(new Criterio_PuntoAdicionalSmartHD());
        $this->Buscar_Simple(new Criterio_RepetidorSmart());
    }
    
    
    public function EsOfertaValida() {
        return $this->oferta->salto_arpu >= (14);
    }
}
