<?php

namespace Arpu\Logic\Modo;

use Arpu\Logic\Priorizacion\Criterio_DuoBa_DuoBa;
use Arpu\Logic\Priorizacion\Criterio_RepetidorSmart;
use Arpu\Logic\Priorizacion\Criterio_Trio_Trio_SinAdicionales;
use Arpu\Logic\Priorizacion\Criterio_Bloque;
use Arpu\Logic\Priorizacion\Criterio_PuntoAdicionalSmartHD;


class ModoAverias extends ModoRegular {
    
    public function __construct($cliente,$modo) {
        parent::__construct($cliente,$modo);
    }
    
    protected function OfertasDuosBa() {
        $this->Buscar(new Criterio_DuoBa_DuoBa(0,1000,$this->Modalidad,0,0,$this->Cliente->SaltoCero));
        $this->Buscar(new Criterio_RepetidorSmart());
    }
    
    protected function Ejecutar_RestoOfertasTrios(){
        $this->Buscar(new Criterio_Trio_Trio_SinAdicionales(0,35,$this->Modalidad,0,0,$this->Cliente->SaltoCero));
        
        $this->Buscar_Simple(new Criterio_RepetidorSmart());
        $this->Buscar_Simple(new Criterio_Bloque('HD'));
        $this->Buscar_Simple(new Criterio_Bloque('MC'));
        $this->Buscar_Simple(new Criterio_Bloque('HBO'));
        $this->Buscar_Simple(new Criterio_PuntoAdicionalSmartHD());
    }
	
	public function EsOfertaValida() {
        if($this->Cliente->Inestable || $this->Cliente->SaltoCero){            
            return $this->oferta->salto_arpu >= (0);
        }
        else{
            return $this->oferta->salto_arpu >= (12);
        }
    }
}
