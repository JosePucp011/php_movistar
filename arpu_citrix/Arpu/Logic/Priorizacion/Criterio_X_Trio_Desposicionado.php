<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_X_Trio_Desposicionado extends Criterio
{
    public function __construct($saltoArpuLimInf,$saltoArpuLimSup,$canal,$activarSaltoCero,$activarSaltoceroModo,$clienteMarcaCero) {
        $this->saltoArpuLimInf = $saltoArpuLimInf;
        $this->saltoArpuLimSup = $saltoArpuLimSup;

        $this->canal = $canal;     
        $this->activarSaltoCero = $activarSaltoCero;
        $this->activarSaltoceroModo = $activarSaltoceroModo;
        $this->clienteMarcaCero = $clienteMarcaCero;
    }

    public function Es($oferta){
        if($this->activarSaltoCero){ // Salto cero para todos
            return $oferta->EnRangoArpu(0,1000) &&
            $oferta->producto->EsTrio();
        }
        else if($this->canal=='Averia' && $this->activarSaltoceroModo){ //  Salto cero solo para Averia
            return $oferta->EnRangoArpu(0,1000) &&
            $oferta->producto->EsTrio();
        }
        else if($this->canal=='Averia' && $this->clienteMarcaCero){ // Solo para cliente con Marca SaltoCero                   
            return $oferta->EnRangoArpu(0,1000) &&
                $oferta->producto->EsTrio();
        }
        else{ // Para los demas casos (REGULAR Y SUS OTROS HEREDEROS)           
            return $oferta->EnRangoArpu($this->saltoArpuLimInf,$this->saltoArpuLimSup) &&
            $oferta->producto->EsTrio();      
        } 

    }

    private $saltoArpuLimInf;
    private $saltoArpuLimSup;

    private $canal;   
    private $activarSaltocero;
    private $activarSaltoceroModo;
    private $clienteMarcaCero;
}