<?php

namespace Arpu\Logic\Priorizacion;



class Criterio_CompletaDown extends Criterio {
    
    public function __construct($saltoArpu) {
        $this->saltoArpu = $saltoArpu;
    }
    
    public function EsMejor($actual,$mejor) {
        return $actual->salto_arpu > $mejor->salto_arpu ||
            $actual->salto_arpu == $mejor->salto_arpu;
    }

    public function Es($oferta){
        return $oferta->producto->Paquete->Tipo == 'DuoTrio' &&
                $oferta->producto->Cable->Presente &&
               $oferta->EnRangoArpu(-40,0) &&
               $oferta->mejor_up == null;
    }
    
    private $saltoArpu;
}
