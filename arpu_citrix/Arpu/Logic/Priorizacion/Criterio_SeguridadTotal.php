<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_SeguridadTotal extends Criterio
{
    public function Es($oferta){
        return $oferta->producto->Paquete->Tipo == 'Internet' &&
                    $oferta->producto->Paquete->Nombre == 'Seguridad Total';
    }
}

