<?php

namespace Arpu\Logic\Priorizacion;



class Criterio_MigracionDown_SaltoMaximo_Naked_DuoBa extends Criterio {
    public function __construct($saltoArpu,$ordenDuoBA,$tecnologia,$coberturaHFC) {
        $this->saltoArpu = $saltoArpu;
        $this->ordenDuoBA = $ordenDuoBA;
        $this->coberturaHFC = $coberturaHFC;
        $this->tecnologia = $tecnologia;
    }
    
    public function EsMejor($actual,$mejor) {
        return $actual->salto_arpu >= $mejor->salto_arpu ||
            ($actual->salto_arpu == $mejor->salto_arpu && $actual->producto->Internet->Velocidad > $mejor->producto->Internet->Velocidad);;
    }

    public function Es($oferta) {
        if( $this->tecnologia == 'ADSL' && $this->coberturaHFC>0 ){
            return  $oferta->producto->Internet->Tecnologia != 'ADSL' &&
                    $oferta->EnRangoArpu(-40,$this->saltoArpu) &&
                    $oferta->BajaBloques >= 0 &&
                    (($this->ordenDuoBA &&
                    !$oferta->producto->Cable->Presente
                    && $oferta->producto->Internet->Presente
                    && !$oferta->producto->Linea->Presente) 
                    || 
                    (!$this->ordenDuoBA 
                    && !$oferta->producto->Cable->Presente 
                    && $oferta->producto->Internet->Presente 
                    && $oferta->producto->Linea->Presente)) 
                    &&
                    $oferta->mejor_up == null; 
        }
        else{
            return  $oferta->EnRangoArpu(-40,$this->saltoArpu) &&
                    $oferta->BajaBloques >= 0 &&
                    (($this->ordenDuoBA &&
                    !$oferta->producto->Cable->Presente
                    && $oferta->producto->Internet->Presente
                    && !$oferta->producto->Linea->Presente) 
                    || 
                    (!$this->ordenDuoBA 
                    && !$oferta->producto->Cable->Presente 
                    && $oferta->producto->Internet->Presente 
                    && $oferta->producto->Linea->Presente)) 
                    &&
                    $oferta->mejor_up == null;
        }
        
    }
    private $saltoArpu;
    private $ordenDuoBA;
    private $coberturaHFC;
    private $tecnologia;
}
