<?php

namespace Arpu\Logic\Priorizacion;



class Criterio_MigracionDown_SaltoMaximo extends Criterio {
    
    public function __construct($saltoArpu) {
        $this->saltoArpu = $saltoArpu;
    }
    
    public function EsMejor($actual,$mejor) {
        return $actual->salto_arpu > $mejor->salto_arpu;
    }

    public function Es($oferta) {
        return $oferta->EnRangoArpu(-40,$this->saltoArpu) &&
               $oferta->BajaBloques >= 0 &&
                $oferta->tipoOperacionComercial == 'Migracion' &&
                $oferta->mejor_up == null;
    }
    private $saltoArpu;
}
