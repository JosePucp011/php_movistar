<?php

namespace Arpu\Logic\Priorizacion;

abstract class Criterio
{
    public abstract function Es($oferta);
    public function EsMejor($actual,$mejor){

        return $actual->salto_arpu < $mejor->salto_arpu;
    }
    
    public function EsMejor_o_Nulo($actual,$mejor){
        return $mejor == null || $this->EsMejor($actual, $mejor); 
    }
}
