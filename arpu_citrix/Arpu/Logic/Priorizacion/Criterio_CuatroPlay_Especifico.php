<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_CuatroPlay_Especifico extends Criterio {
    
    public function __construct($PaquetePs,$MovilID) {
        $this->PaquetePs = $PaquetePs;
        $this->MovilID = $MovilID;
    }    
    
    public function Es($oferta){
        return $oferta->producto->Paquete->Ps == $this->PaquetePs  
                && $oferta->producto->Paquete->CodigoMovil == $this->MovilID;
    }   
}
