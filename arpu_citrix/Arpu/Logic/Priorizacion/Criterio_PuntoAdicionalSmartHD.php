<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_PuntoAdicionalSmartHD extends Criterio
{
    public function Es($oferta){
        return $oferta->producto->Paquete->Tipo == 'Deco' &&
                $oferta->producto->Paquete->Nombre == 'Punto Adicional Smart HD';
    }
}
