<?php

namespace Arpu\Logic\Priorizacion;



class Criterio_CompletaDown_Naked_DuoBa extends Criterio {
    
    public function __construct($saltoArpu,$tecnologia,$coberturaHFC) {
        $this->saltoArpu = $saltoArpu;
        $this->coberturaHFC = $coberturaHFC;
        $this->tecnologia = $tecnologia;
    }
    
    public function EsMejor($actual,$mejor) {
        return $actual->salto_arpu >= $mejor->salto_arpu ||
            ($actual->salto_arpu == $mejor->salto_arpu && $actual->producto->Internet->Velocidad > $mejor->producto->Internet->Velocidad);;
    }

    public function Es($oferta){
        if( $this->tecnologia == 'ADSL' && $this->coberturaHFC>0 ){
            return $oferta->producto->Internet->Tecnologia != 'ADSL' &&
            ($oferta->EnRangoArpu(-40,0) && 
            (($oferta->producto->Linea->Presente && $oferta->producto->Internet->Presente && !$oferta->producto->Cable->Presente) || (!$oferta->producto->Linea->Presente && $oferta->producto->Internet->Presente && !$oferta->producto->Cable->Presente)) &&
            $oferta->mejor_up == null);
        }
        else{
            return ($oferta->EnRangoArpu(-40,0) && 
            (($oferta->producto->Linea->Presente && $oferta->producto->Internet->Presente && !$oferta->producto->Cable->Presente) || (!$oferta->producto->Linea->Presente && $oferta->producto->Internet->Presente && !$oferta->producto->Cable->Presente)) &&
            $oferta->mejor_up == null);
        }
      
    }
    
    private $saltoArpu;
    private $coberturaHFC;
    private $tecnologia;
}
