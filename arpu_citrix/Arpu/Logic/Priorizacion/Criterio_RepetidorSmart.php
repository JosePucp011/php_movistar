<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_RepetidorSmart extends Criterio
{
    public function Es($oferta){
        return $oferta->producto->Paquete->Tipo == 'Internet' &&
                    $oferta->producto->Paquete->Nombre == 'Repetidor Smart Wifi';
    }
}

