<?php
namespace Arpu\Logic\Priorizacion;


class Criterio_Trio_Trio extends Criterio
{
    public function Es($oferta){
        return $oferta->producto->Ultrawifi == 0 &&
                $oferta->producto->DecosAdicionales == 0 &&
                $oferta->producto->Paquete->Tipo == 'DuoTrio' &&
                $oferta->producto->Cable->Presente &&
                $oferta->producto->Linea->Presente &&
                $oferta->producto->Internet->Presente &&
                strpos($oferta->movimiento['Premium'], 'HBO') === FALSE &&
                strpos($oferta->movimiento['Premium'], 'Fox') === FALSE &&
                $oferta->movimiento['Internet'] == 'Up' &&
                $oferta->mejor_up == null; 
    }
}

