<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_X_Naked extends Criterio
{   
    public function __construct($canal,$activarSaltoCero,$activarSaltoceroModo,$clienteMarcaCero) {
        $this->canal = $canal;     
        $this->activarSaltoCero = $activarSaltoCero;
        $this->activarSaltoceroModo = $activarSaltoceroModo;
        $this->clienteMarcaCero = $clienteMarcaCero;
    }
    public function Es($oferta){

        if($this->activarSaltoCero){ // Salto cero para todos
            return $oferta->EnRangoArpu(0,1000) &&
            $oferta->producto->Paquete->Tipo === 'Naked';
        }
        else if($this->canal=='Averia' && $this->activarSaltoceroModo){ //  Salto cero solo para Averia
            return $oferta->EnRangoArpu(0,1000) &&
            $oferta->producto->Paquete->Tipo === 'Naked';
        }
        else if($this->canal=='Averia' && $this->clienteMarcaCero){ // Solo para cliente con Marca SaltoCero                   
            return $oferta->EnRangoArpu(0,1000) &&
            $oferta->producto->Paquete->Tipo === 'Naked';
        }
        else{ // Para los demas casos (REGULAR Y SUS OTROS HEREDEROS)           
            return $oferta->EnRangoArpu(9,41) &&
            $oferta->producto->Paquete->Tipo === 'Naked';
        }       
    }

    private $canal;   
    private $activarSaltocero;
    private $activarSaltoceroModo;
    private $clienteMarcaCero;
}