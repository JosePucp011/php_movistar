<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_CuatroPlay extends Criterio {
    
    public function __construct($saltoArpu) {
        $this->saltoArpu = $saltoArpu;
    }
    
    public function EsMejor($actual,$mejor){
        if($this->saltoArpu < 0){
            return $actual->salto_arpu > $mejor->salto_arpu;
        } else {
            return $actual->salto_arpu < $mejor->salto_arpu;
        }
        
    }
    
    public function Es($actual){
        
        if($this->saltoArpu < 0 && $actual->salto_arpu >= 0){
            return false;
        }

        return $actual->producto->Paquete->Tipo == '4Play' && 
                strpos($actual->movimiento['Premium'], 'HBO') === FALSE && 
                strpos($actual->movimiento['Premium'], 'FOX') === FALSE &&
                $actual->BajaBloques == 0 &&
                $actual->mejor_up == null &&
                $actual->EnRangoArpu($this->saltoArpu,1000);
    }   
}
