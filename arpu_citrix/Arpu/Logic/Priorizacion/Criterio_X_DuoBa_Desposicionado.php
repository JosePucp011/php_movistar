<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_X_DuoBa_Desposicionado extends Criterio
{   
    public function __construct($canal,$activarSaltoCero,$activarSaltoceroModo,$clienteMarcaCero) {
     
        $this->canal = $canal;     
        $this->activarSaltoCero = $activarSaltoCero;
        $this->activarSaltoceroModo = $activarSaltoceroModo;
        $this->clienteMarcaCero = $clienteMarcaCero;
    }
    
    public function Es($actual){

        if($this->activarSaltoCero){ // Salto cero para todos
            return $actual->producto->EsDuoBa() && $actual->salto_arpu >= 0;
        }
        else if($this->canal=='Averia' && $this->activarSaltoceroModo){ //  Salto cero solo para Averia
            return $actual->producto->EsDuoBa() && $actual->salto_arpu >= 0;
        }
        else if($this->canal=='Averia' && $this->clienteMarcaCero){ // Solo para cliente con Marca SaltoCero                   
            return $actual->producto->EsDuoBa() && $actual->salto_arpu >= 0;
        }
        else{ // Para los demas casos (REGULAR Y SUS OTROS HEREDEROS)           
            return $actual->producto->EsDuoBa() && $actual->salto_arpu >= 10;     
        }            
    }

    private $canal;   
    private $activarSaltocero;
    private $activarSaltoceroModo;
    private $clienteMarcaCero;
}