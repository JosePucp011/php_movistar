<?php


namespace Arpu\Logic\Priorizacion;

class Criterio_X_DuoTv extends Criterio {
    public function __construct($saltoArpuLimInf,$saltoArpuLimSup,$canal,$activarSaltoCero,$activarSaltoceroModo,$clienteMarcaCero) {
        $this->saltoArpuLimInf = $saltoArpuLimInf;
        $this->saltoArpuLimSup = $saltoArpuLimSup;

        $this->canal = $canal;     
        $this->activarSaltoCero = $activarSaltoCero;
        $this->activarSaltoceroModo = $activarSaltoceroModo;
        $this->clienteMarcaCero = $clienteMarcaCero;
    }
    
    public function Es($actual){
        
        if($this->activarSaltoCero){ // Salto cero para todos
            return $actual->producto->EsDuoTv() &&            
            $actual->EnRangoArpu(0,1000) &&
            $actual->mejor_up == null; 
        }
        else if($this->canal=='Averia' && $this->activarSaltoceroModo){ //  Salto cero solo para Averia
            return $actual->producto->EsDuoTv() &&            
            $actual->EnRangoArpu(0,1000) &&
            $actual->mejor_up == null; 
        }
        else if($this->canal=='Averia' && $this->clienteMarcaCero){ // Solo para cliente con Marca SaltoCero                   
            return $actual->producto->EsDuoTv() &&            
            $actual->EnRangoArpu(0,1000) &&
            $actual->mejor_up == null; 
        }
        else{ // Para los demas casos (REGULAR Y SUS OTROS HEREDEROS)           
            return $actual->producto->EsDuoTv() && 
            //$actual->salto_arpu >= 20 &&
            //$actual->EnRangoArpu(40,65) &&
            $actual->EnRangoArpu($this->saltoArpuLimInf,$this->saltoArpuLimSup) &&
            $actual->mejor_up == null;       
        }    

    }

    private $saltoArpuLimInf;
    private $saltoArpuLimSup;

    private $canal;   
    private $activarSaltocero;
    private $activarSaltoceroModo;
    private $clienteMarcaCero;
}