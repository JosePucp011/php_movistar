<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_DuoBa_Trio extends Criterio
{
    public function Es($oferta){
        return $oferta->producto->Paquete->Tipo == 'DuoTrio' &&
                $oferta->producto->Cable->Presente &&
                $oferta->producto->Linea->Presente &&
                $oferta->producto->Internet->Presente &&
               $oferta->EnRangoArpu(20,80) &&
//               ($oferta->movimiento['Internet'] == 'Up' || $oferta->movimiento['Internet'] == 'Mantiene') &&
               $oferta->mejor_up == null;
    }
    
    public function EsMejor($actual,$mejor){
        return $actual->salto_arpu < $mejor->salto_arpu ||
                $actual->salto_arpu == $mejor->salto_arpu ;
//                && $actual->producto->Internet->Velocidad > $mejor->producto->Internet->Velocidad;
    }
}
