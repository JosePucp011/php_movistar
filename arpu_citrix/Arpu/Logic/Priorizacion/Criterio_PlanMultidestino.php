<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_PlanMultidestino extends Criterio
{
    public function Es($oferta){
        return $oferta->producto->Paquete->Tipo == 'Multidestino' && 
               $oferta->mejor_up == null;
    }
}
