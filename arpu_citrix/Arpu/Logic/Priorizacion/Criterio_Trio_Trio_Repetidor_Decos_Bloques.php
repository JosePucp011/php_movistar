<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_Trio_Trio_Repetidor_Decos_Bloques extends Criterio {

   public function __construct($saltoArpuLimInf,$saltoArpuLimSup,$canal,$activarSaltoCero,$activarSaltoceroModo,$clienteMarcaCero) {
        $this->saltoArpuLimInf = $saltoArpuLimInf;
        $this->saltoArpuLimSup = $saltoArpuLimSup;

        $this->canal = $canal;     
        $this->activarSaltoCero = $activarSaltoCero;
        $this->activarSaltoceroModo = $activarSaltoceroModo;
        $this->clienteMarcaCero = $clienteMarcaCero;
    }

    public function Es($oferta){

        if($this->activarSaltoCero){ // Salto cero para todos
            return $oferta->producto->Paquete->Tipo == 'DuoTrio' &&
            $oferta->producto->Cable->Presente &&
            $oferta->producto->Linea->Presente &&
            $oferta->producto->Internet->Presente &&
            $oferta->producto->RentaAdicionales() <= 50 &&
            $oferta->movimiento['Internet'] == 'Up' &&           
            $oferta->EnRangoArpu(0,1000) &&
            $oferta->mejor_up == null;  
        }
        else if($this->canal=='Averia' && $this->activarSaltoceroModo){ //  Salto cero solo para Averia
            return $oferta->producto->Paquete->Tipo == 'DuoTrio' &&
            $oferta->producto->Cable->Presente &&
            $oferta->producto->Linea->Presente &&
            $oferta->producto->Internet->Presente &&
            $oferta->producto->RentaAdicionales() <= 50 &&
            $oferta->movimiento['Internet'] == 'Up' &&          
            $oferta->EnRangoArpu(0,1000) &&
            $oferta->mejor_up == null; 
        }
        else if($this->canal=='Averia' && $this->clienteMarcaCero){ // Solo para cliente con Marca SaltoCero                   
            return $oferta->producto->Paquete->Tipo == 'DuoTrio' &&
            $oferta->producto->Cable->Presente &&
            $oferta->producto->Linea->Presente &&
            $oferta->producto->Internet->Presente &&
            $oferta->producto->RentaAdicionales() <= 50 &&
            $oferta->movimiento['Internet'] == 'Up' &&          
            $oferta->EnRangoArpu(0,1000) &&
            $oferta->mejor_up == null; 
        }
        else{ // Para los demas casos (REGULAR Y SUS OTROS HEREDEROS)           
            return $oferta->producto->Paquete->Tipo == 'DuoTrio' &&
            $oferta->producto->Cable->Presente &&
            $oferta->producto->Linea->Presente &&
            $oferta->producto->Internet->Presente &&
            $oferta->producto->RentaAdicionales() <= 50 &&
            $oferta->movimiento['Internet'] == 'Up' &&            
            $oferta->EnRangoArpu($this->saltoArpuLimInf,$this->saltoArpuLimSup) &&
            $oferta->mejor_up == null;       
        }    
       
    }

    public function EsMejor($actual,$mejor){
        return 
        $mejor->salto_arpu - $actual->salto_arpu >= 5 ||
        abs($mejor->salto_arpu - $actual->salto_arpu) < 5 &&
        ($mejor->producto->DecosAdicionales > $actual->producto->DecosAdicionales 
                || $mejor->producto->Ultrawifi < $actual->producto->Ultrawifi 
                || $mejor->AltaBloques > $actual->AltaBloques);
    }

    private $saltoArpuLimInf;
    private $saltoArpuLimSup;

    private $canal;   
    private $activarSaltocero;
    private $activarSaltoceroModo;
    private $clienteMarcaCero;
}