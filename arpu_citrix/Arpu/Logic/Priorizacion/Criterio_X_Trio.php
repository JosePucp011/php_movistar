<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_X_Trio extends Criterio {
    public function __construct($saltoArpuLimInf,$saltoArpuLimSup,$canal,$activarSaltoCero,$activarSaltoceroModo,$clienteMarcaCero) {
        $this->saltoArpuLimInf = $saltoArpuLimInf;
        $this->saltoArpuLimSup = $saltoArpuLimSup;

        $this->canal = $canal;     
        $this->activarSaltoCero = $activarSaltoCero;
        $this->activarSaltoceroModo = $activarSaltoceroModo;
        $this->clienteMarcaCero = $clienteMarcaCero;
    }

    public function Es($oferta){

        if($this->activarSaltoCero){ // Salto cero para todos
            return  $oferta->producto->EsTrio() &&           
            $oferta->EnRangoArpu(0,1000) &&
            $oferta->mejor_up == null;  
        }
        else if($this->canal=='Averia' && $this->activarSaltoceroModo){ //  Salto cero solo para Averia
            return  $oferta->producto->EsTrio() &&           
            $oferta->EnRangoArpu(0,1000) &&
            $oferta->mejor_up == null;  
        }
        else if($this->canal=='Averia' && $this->clienteMarcaCero){ // Solo para cliente con Marca SaltoCero                   
            return  $oferta->producto->EsTrio() &&           
            $oferta->EnRangoArpu(0,1000) &&
            $oferta->mejor_up == null;  
        }
        else{ // Para los demas casos (REGULAR Y SUS OTROS HEREDEROS)           
            return  $oferta->producto->EsTrio() &&           
            $oferta->EnRangoArpu($this->saltoArpuLimInf,$this->saltoArpuLimSup) &&
            $oferta->mejor_up == null;             
        }    
       
    }

    private $saltoArpuLimInf;
    private $saltoArpuLimSup;

    private $canal;   
    private $activarSaltocero;
    private $activarSaltoceroModo;
    private $clienteMarcaCero;
}