<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_Bloque extends Criterio
{
    public function __construct($texto) {
        $this->texto = $texto;
    }
    
    public function Es($actual){
        return isset($actual->producto->PremiumNombre) &&
                    $actual->producto->PremiumNombre == $this->texto &&
                    $actual->producto->Paquete->Tipo == 'Bloque';
    }
    
    private $texto;
}