<?php

namespace Arpu\Logic\Priorizacion;



class Criterio_MigraTrio_DuoBA extends Criterio {
    
    public function __construct($saltoArpu) {
        $this->saltoArpu = $saltoArpu;
    }
    
    public function EsMejor($actual,$mejor) {
        return $actual->salto_arpu > $mejor->salto_arpu ||
            $actual->salto_arpu == $mejor->salto_arpu;
    }

    public function Es($oferta){
        return $oferta->producto->Paquete->Tipo == 'DuoTrio' &&
                $oferta->producto->Internet->Presente &&
                $oferta->producto->Linea->Presente &&
               $oferta->EnRangoArpu(-100,0) &&
               $oferta->mejor_up == null;
    }
    
    private $saltoArpu;
}
