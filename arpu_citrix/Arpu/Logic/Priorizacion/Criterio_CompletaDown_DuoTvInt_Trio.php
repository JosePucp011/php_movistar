<?php

namespace Arpu\Logic\Priorizacion;



class Criterio_CompletaDown_DuoTvInt_Trio extends Criterio {
    
    public function __construct($saltoArpu) {
        $this->saltoArpu = $saltoArpu;
    }
    
    public function EsMejor($actual,$mejor) {
        return  $actual->salto_arpu > $mejor->salto_arpu || 
                ( $actual->salto_arpu == $mejor->salto_arpu && $actual->producto->Internet->Velocidad > $mejor->producto->Internet->Velocidad );
    }

    public function Es($oferta){
        return $oferta->producto->Paquete->Tipo == 'DuoTrio' &&
               $oferta->producto->Cable->Presente &&
               $oferta->producto->Internet->Presente &&
               $oferta->EnRangoArpu(-40,0) &&
              ($oferta->movimiento['Premium'] == 'Up' 
                       || $oferta->movimiento['Premium'] == 'Mantiene' 
                       || $oferta->movimiento['Premium'] == 'No Evalua Componente'
                       || $oferta->movimiento['Premium'] == 'Alta HD'  ) &&
               $oferta->mejor_up == null;
    }
    
    private $saltoArpu;
}
