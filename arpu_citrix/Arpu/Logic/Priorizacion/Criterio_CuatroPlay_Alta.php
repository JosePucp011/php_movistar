<?php

namespace Arpu\Logic\Priorizacion;

class Criterio_CuatroPlay_Alta extends Criterio {
    
    public function __construct($saltoArpu) {
        $this->saltoArpu = $saltoArpu;
    }
    
    public function Es($actual){
        return $actual->producto->Paquete->Tipo == '4Play' && 
                $actual->producto->EsPermutacion == 0 &&
                $actual->BajaBloques == 0 &&
                $actual->mejor_up == null &&
                $actual->EnRangoArpu($this->saltoArpu,1000);
    }   
}
