<?php

namespace Arpu\Logic\Priorizacion;



class Criterio_MigracionDown_SaltoMaximo_DuoTvInt_Trio_General extends Criterio {
    public function __construct($saltoArpu,$ordenDuoTVBa) {
        $this->saltoArpu = $saltoArpu;
        $this->ordenDuoTVBa = $ordenDuoTVBa;
    }
    
    public function EsMejor($actual,$mejor) {
        return $actual->salto_arpu > $mejor->salto_arpu ||
            ($actual->salto_arpu == $mejor->salto_arpu && $actual->producto->Internet->Velocidad > $mejor->producto->Internet->Velocidad);
    }

    public function Es($oferta) {
        return  $oferta->EnRangoArpu(-40,$this->saltoArpu) &&
                $oferta->BajaBloques >= 0 &&
                ($oferta->movimiento['Premium'] == 'Up' 
                       || $oferta->movimiento['Premium'] == 'Mantiene' 
                       || $oferta->movimiento['Premium'] == 'No Evalua Componente' 
                       || $oferta->movimiento['Premium'] == 'Alta HD' ) &&
                $oferta->producto->Internet->Presente  && 
                $oferta->producto->Cable->Presente &&
                $oferta->mejor_up == null;
    }
    private $saltoArpu;
    private $ordenDuoTVBa;
}
