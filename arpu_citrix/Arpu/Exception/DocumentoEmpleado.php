<?php

namespace Arpu\Exception;

class DocumentoEmpleado extends \Exception {
    public function __construct(){
        parent::__construct('CLIENTE EMPLEADO');
    }
}
