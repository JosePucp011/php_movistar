<?php



namespace Arpu\Exception;

class QueryNoEjecutado extends \Exception{
    
    public function __construct(){
        parent::__construct('Query no ejecutado');
    }

}
