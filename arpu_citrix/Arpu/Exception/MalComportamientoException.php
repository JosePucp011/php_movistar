<?php

namespace Arpu\Exception;

class MalComportamientoException extends \Exception {
    public function __construct(){
        parent::__construct('Cliente con mal comportamiento de pago fijo');
    }
}
