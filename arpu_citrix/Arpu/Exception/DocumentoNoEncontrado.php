<?php

namespace Arpu\Exception;

if(basename($_SERVER['SCRIPT_FILENAME'])==basename(__FILE__)){
    header("HTTP/1.0 404 Not Found");
    exit(0);
}


class DocumentoNoEncontrado extends \Exception{
    public function __construct(){
        parent::__construct('Documento no encontrado');
    }
}
