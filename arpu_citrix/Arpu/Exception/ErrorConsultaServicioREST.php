<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Arpu\Exception;


class ErrorConsultaServicioREST extends \Exception {
    public function __construct(){
        parent::__construct('No se pudo ejecutar el servicio REST');
    }
}
