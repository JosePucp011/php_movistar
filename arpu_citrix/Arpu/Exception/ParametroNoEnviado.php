<?php


namespace Arpu\Exception;

if(basename($_SERVER['SCRIPT_FILENAME'])==basename(__FILE__)){
    header("HTTP/1.0 404 Not Found");
    exit(0);
}

class ParametroNoEnviado extends \Exception{
    public function __construct($parametro){
        parent::__construct("$parametro no enviado");
    }
}
