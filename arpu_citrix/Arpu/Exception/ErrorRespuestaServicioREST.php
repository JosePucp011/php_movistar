<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Arpu\Exception;

/**
 * Description of ErrorRespuestaServicioREST
 *
 * @author dvictoriad
 */
class ErrorRespuestaServicioREST extends \Exception {
    public function __construct(){
        parent::__construct('La respuesta del servicio REST no se encuentra en el formato correcto');
    }
}
