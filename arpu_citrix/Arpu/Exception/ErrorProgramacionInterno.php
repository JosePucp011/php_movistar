<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Arpu\Exception;

/**
 * Description of ErrorProgramacionInterno
 *
 * @author dvictoriad
 */
class ErrorProgramacionInterno extends \Exception {
    public function __construct(){
        parent::__construct('Inconsistencia en el codigo interno');
    }
}
