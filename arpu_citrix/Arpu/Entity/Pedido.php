<?php


namespace Arpu\Entity;

class Pedido
{
    public $EstadioFija;
    public $EstadoFija;
    public $MotivoQuiebreFija;
    public $StatusFija;
    public $EstadioMovil;
    public $EstadoMovil;
    public $MotivoQuiebreMovil;
    public $StatusMovil;
    public $SpeechDentroPlazoCabecera;
    public $SpeechDentroPlazoDescripcion;
    public $SpeechFueraPlazoCabecera1;
    public $SpeechFueraPlazoDescripcion1;
    public $SpeechFueraPlazoCabecera2;
    public $SpeechFueraPlazoDescripcion2;
}
