<?php
namespace Arpu\Entity;

class Mensaje
{
   public $Fecha;
   public $FechaMensaje;  // Fecha para mensaje
   public $Mensaje;
   
   /**
    *
    * @var Cambio
    */
   public $Renta;
   /**
    *
    * @var Cambio
    */
   public $Velocidad;
   
   
   /**
    *
    * @var Cambio
    */
   public $Minutos;
   
   public $Canales;
   public $Devolucion;
   
   public $Prix;
   public $MPlay;
   
}
