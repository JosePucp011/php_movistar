<?php 
namespace Arpu\Entity;
use Arpu\Data\DescuentoPermanenteDL;
use Arpu\Data\DecodificadorDL;
use Arpu\Data\PremiumDL;

class Operacion
{
   
   
   public function __construct(
      $Ps=null,
      $PsNombre=null,
      $Componente=null,
      $TipoOperacionComercial=Operacion::Vacia,
      $Caracteristica = Operacion::Vacia,
      $NombreComercial = null,
      $Renta = '0')
   {
      $this->Ps = new ProductoServicio($Ps,$PsNombre,$NombreComercial);
      $this->TipoOperacionComercial = $TipoOperacionComercial;
      $this->Caracteristica = $Caracteristica;
      $this->Componente = $Componente;
      $this->Renta = $Renta;
   }
   
   public static function BajaDescuentoPermanente($ps){
       return new Operacion($ps,DescuentoPermanenteDL::$descuentos[$ps]->DescuentoNombre,"",self::Baja);
   }
   
   public static function RegistroPsAdicionales($ps)
   {
      if(isset(DescuentoPermanenteDL::$descuentos[$ps]))
      {
         return new Operacion($ps,DescuentoPermanenteDL::$descuentos[$ps]->DescuentoNombre,"",self::Alta);
      }
      elseif(isset(PremiumDL::$cache[$ps]))
      { 
         return new Operacion( 
              PremiumDL::$cache[$ps]->PremiumPs, 
              PremiumDL::$cache[$ps]->PremiumNombre, "", Operacion::Alta );
      }
      else
      {
         return new Operacion($ps,$ps,"",Operacion::Alta);
      }
   }
   
   public static function AltaAdministrativaDecoSagem2Alquiler()
   {
      return new Operacion('20449','DECODER HD PVR ALQUILER',\Arpu\Entity\CA::ComponenteRegistro_Deco,Operacion::AltaAdministrativa);
   }
   
   public static function AltaAdministrativaDecoSagem2Comodato()
   {
      return new Operacion('20450','DECODER HD PVR COMODATO',\Arpu\Entity\CA::ComponenteRegistro_Deco,Operacion::AltaAdministrativa);
   }
   
   public static function AltaAdministrativaDecoSagem1Comodato()
   {
      return new Operacion('21930','DECODER HD SAGEM 1 COMODATO',\Arpu\Entity\CA::ComponenteRegistro_Deco,Operacion::AltaAdministrativa);
   }
   
   public static function AltaAdministrativaDecoZapperComodato()
   {
      return new Operacion('21932','DECODER HD ZAPPER COMODATO',\Arpu\Entity\CA::ComponenteRegistro_Deco,Operacion::AltaAdministrativa);
   }
   
   
   public static function BajaBBHD21149()
   {
      return new Operacion(21149, "BB HD ESTANDAR CATV", \Arpu\Entity\CA::ComponenteRegistro_Canales,Operacion::Baja);
   }
   
   public static function RegistroDescuentoCampana($Ps, $Descripcion)
   {
       return new Operacion($Ps, $Descripcion, \Arpu\Entity\CA::ComponenteRegistro_Descuento, Operacion::Alta );
   }
   
   public static function ComercialDescuentoCampana($Descripcion, $Monto, $Nombre){
       return array($Descripcion, '', $Nombre, \Arpu\Entity\CA::ComponenteRegistro_Descuento);
   }
   
   
   
   const Alta = "Alta";
   const AltaAdministrativa = "Alta Administrativa";
   const AltaGratuidad = "Alta con Gratuidad de producto";
   const AltaPackCero = "Alta Pack Cero";
   const AltaCostoCero = "Alta Costo Cero";
   const AltaConGratuidad30Dias="ALTA CON GRATUIDAD 30 DIAS";
   const AltaConGratuidad90Dias="ALTA CON GRATUIDAD 90 DIAS";
   
   const Migracion = 'Migracion P/S';
   const Baja = "Baja";
   const Vacia = "";
   
   public static $BajaPuntoDigital18995;
   public static $RegistroDescuentoCampanaIquitos70x2m;
   public static $RegistroDescuentoCampanaIquitos100x2m;
   public static $ComercialDescuentoCampanaIquitos70x2m;
   public static $ComercialDescuentoCampanaIquitos100x2m;

   public static $PuntoAnalogico;
   public static $AltaAdministrativaDeco19026;
   public static $AntenaDTH;
   
   public static $AltaComodatoDeco19026;
   public static $AltaVentaDeco21251;
   
   public static $AltaAdministrativaPuntoDigital18995;
   public static $BajaDeco19026;
   public static $PuntoDigital;
   public static $PuntoConCobro;
   
   
   public $Ps;
   public $TipoOperacionComercial;
   public $Caracteristica;
   public $Componente;
   public $Renta;
   public static $CuotaInstalacion;
}


Operacion::$AltaAdministrativaDeco19026 = 
   new Operacion(
      DecodificadorDL::$cache[19026]->Ps,
      DecodificadorDL::$cache[19026]->Nombre,
      \Arpu\Entity\CA::ComponenteRegistro_Deco,
      Operacion::AltaAdministrativa);


Operacion::$BajaPuntoDigital18995 = 
        new Operacion(18995, "Punto Digital", \Arpu\Entity\CA::ComponenteRegistro_Punto,Operacion::Baja);

Operacion::$CuotaInstalacion = 
        new Operacion(23166, "Cuota Instalacion", \Arpu\Entity\CA::ComponenteRegistro_Deco,'Alta');


Operacion::$PuntoDigital = 
   new Operacion(18995, "Punto Digital",\Arpu\Entity\CA::ComponenteRegistro_Punto,Operacion::AltaGratuidad);

Operacion::$PuntoConCobro =
        new Operacion(22786,'Instalacion Tv Analogico',\Arpu\Entity\CA::ComponenteRegistro_Punto,Operacion::Alta);

Operacion::$PuntoAnalogico = 
   new Operacion(21956,'Punto Analogico',\Arpu\Entity\CA::ComponenteRegistro_Punto,Operacion::Alta);
   
Operacion::$AltaAdministrativaPuntoDigital18995 = 
   new Operacion(18995, "Punto Digital",\Arpu\Entity\CA::ComponenteRegistro_Punto,Operacion::AltaAdministrativa);

Operacion::$AntenaDTH = 
        new Operacion(22623, 'Antena Satelital', \Arpu\Entity\CA::ComponenteRegistro_Antena, Operacion::Alta );

Operacion::$BajaDeco19026 = 
   new Operacion(
      DecodificadorDL::$cache[19026]->Ps,
      DecodificadorDL::$cache[19026]->Nombre,
      \Arpu\Entity\CA::ComponenteRegistro_Deco,
      Operacion::Baja);
      


