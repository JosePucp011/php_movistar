<?php

namespace Arpu\Entity;
use Arpu\Data\DecodificadorDL;
use Arpu\Entity\Operacion;

class OperacionDeco extends Operacion
{
   public function __construct($Ps,$Pack)
   {
      parent::__construct(
         DecodificadorDL::$cache[$Ps]->Ps,
         DecodificadorDL::$cache[$Ps]->Nombre,
         \Arpu\Entity\CA::ComponenteRegistro_Deco,
         Operacion::Alta, 
         $Pack);
   }
}
