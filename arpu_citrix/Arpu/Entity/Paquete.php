<?php

namespace Arpu\Entity;
class Paquete {
    public $Ps;
    public $Tipo;
    public $NombreAtis;
    public $PsAdicionales;
    public $PsDescuento;
    public $Categoria;
    public $Nombre;
    public $RentaOrigen;
    public $Renta;
    public $EsSVA;
    
    public function __construct()
    {
       $this->PsAdicionales = array();
       $this->EsSVA = 0;
    }
}
