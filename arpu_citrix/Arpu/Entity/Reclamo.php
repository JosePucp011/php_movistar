<?php


namespace Arpu\Entity;

class Reclamo
{
   public $Cluster;
   public $Tipo;
   public $Fecha;
   public $Motivo;
   public $Importe;
   public $LiquidacionFecha ;
   public $LiquidacionImporte;
   public $Estado;
   public $Respuesta;
   
}
