<?php

namespace Arpu\Entity;
use Arpu\Logic\MigracionCable;


class Cable extends Componente
{
   public function __construct()
   {
      $this->Premium = new Premium();
      $this->Decos = array();
      $this->Deco = new \stdClass();
      $this->Deco->Nombre = "";
      $this->OfertaRetenciones = array();
   }

   public function Baja($Tipo, Oferta &$oferta)
   {
      parent::Baja($Tipo, $oferta);
      if ($this->Premium->Presente)
      {
         $oferta->arpu = round($oferta->arpu + $this->Premium->RentaMonoproducto,2);
         $oferta->movimiento[Componente::Premium] = \Arpu\Entity\CA::Baja;
      }
   }

   public function Alta($Tipo, Oferta $oferta, $origen)
   {
      parent::Alta($Tipo, $oferta, $origen);
      $categoria = $this->Categoria;
      
      if($oferta->producto->Internet->Velocidad < 60000 ){
        $oferta->AgregarDeco(Deco::$Decos["${categoria}_HD"]);
        $oferta->AgregarDeco(Deco::$Decos["${categoria}_HD"]);
      }else{
        $oferta->AgregarDeco(Deco::$Decos["${categoria}_SMART"]);
        $oferta->AgregarDeco(Deco::$Decos["${categoria}_HD"]);
      }
      
      if ($this->Categoria == 'CATV')
      {
         $oferta->Registro[] = Operacion::$PuntoDigital;
      }
      elseif($this->Categoria == 'DTH')
      {
         $oferta->Registro[] = Operacion::$AntenaDTH;
      }

      if ($this->Premium->Presente)
      {
         $oferta->movimiento['Premium'] = 'Alta';
      }      
      
   }
   
   public function Migracion($Tipo, Oferta $oferta, $destino)
   {
      parent::Migracion($Tipo, $oferta, $destino);
      MigracionCable::Ejecutar($this, $destino, $oferta);
   }

   

   public function RentaMonoproducto()
   {
      return $this->RentaMonoproducto + $this->Premium->RentaMonoproducto;
   }
   
   public function __toString()
   {
      $respuesta = '';
      if ($this->Presente)
      {
         $respuesta .= ' ';
         
         if(!$this->EsSVA)
         {
            if($this->Tipo == 'Estandar')
            {
               $respuesta .= "STD";
            }
            elseif($this->Tipo == 'Estelar')
            {
               $respuesta .= "Estelar";
            }
            else
            {
               $respuesta .= $this->Tipo;
            }
         }
         
         
         if($this->Categoria == 'DTH')
         {
            $respuesta .= ' '.$this->Categoria;
         }
         
         if ($this->Premium->Presente)
         {
            if(!$this->EsSVA)
            {
               $respuesta .= ' ';
            }
            $respuesta .= $this->Premium->NombreUSSD();
         }
      }
      return $respuesta;
   }

   const CATV = 'CATV';
   const DTH = 'DTH';
   const ESTANDAR_ANALOGICO = 2;
   const ESTELAR_DIGITAL = 15;
   const ESTELAR_ANALOGICO = 1;

   
   
   public function __clone()
   {
      $this->Premium = clone $this->Premium;
   }
   
   public $EsHDTotal;
   public $Cobertura;

   /**
    *
    * @var Premium
    */
   public $Premium;
   public $Categoria;
   public $TieneDeco20448;
   public $CantidadDeDecos;
   public $CantidadDeBloques;
   public $ClaseServicio;
   public $FechaAlta;
   public $OfertaRetenciones;

   /**
    * @var Decodificador[]
    */
   public $Decos;
   public $Tipo;
   public $PuntosDigitales;


   public $TieneSagem2;
   public $CantidadDecosSD;
   public $Tiene6Meses;
   public $Tiene1Anio;
   public $EsSVA;
   public $TipoSVA;
   public $Deco;
   public $Descripcion;
   
   public $TenenciaDecos = array('DVR' => 0,'HD' => 0,'SD' => 0);
   public $TiposDecos = array('HD' => 0,'Digital' => 0,'DVR' => 0,'Smart' => 0);

}
