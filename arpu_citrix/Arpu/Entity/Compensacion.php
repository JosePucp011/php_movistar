<?php


namespace Arpu\Entity;

class Compensacion
{
   /**
    *
    * @var string[]
    */
   public $Ps;  
   public $Nombre;
   public $Campana;
   public $Monto;
   public $Minimo;
   public $Maximo;
}
