<?php

namespace Arpu\Entity;

class Cliente
{
    public function Es_ADSL_Cobertura_ADSL(){
        return 0 == $this->Internet->Cobertura->HFC
                && $this->Internet->Tecnologia == 'ADSL';
    }
    
    public function Es_ADSL_Cobertura_UBB(){
        return $this->Internet->Cobertura->HFC > 0 &&
                $this->Internet->Tecnologia == 'ADSL';
    }
    
   public function Es_UBB(){
       return $this->Internet->Tecnologia == 'HFC' ||
                $this->Internet->Tecnologia == 'FTTH';
   }
    
   public function Es_MonoTv(){
       return !$this->Internet->Presente && !$this->Linea->Presente && $this->Cable->Presente;
   }
   
   public function Es_MonoLinea(){
       return !$this->Internet->Presente && $this->Linea->Presente && !$this->Cable->Presente;
   }
   public function Es_Naked(){
       return $this->Internet->Presente && !$this->Linea->Presente && !$this->Cable->Presente;
   }
      
   public function Es_DuoTv(){
       return !$this->Internet->Presente && $this->Linea->Presente && $this->Cable->Presente;
   }
   
   public function Es_DuoBa(){
       return $this->Internet->Presente && $this->Linea->Presente && ! $this->Cable->Presente;
   }
   
   public function Es_DuoTvInt() {
    return $this->Internet->Presente &&
            !$this->Linea->Presente &&
            $this->Cable->Presente;
   }

   public function Es_Trio(){
       return $this->Internet->Presente && $this->Linea->Presente && $this->Cable->Presente;
   }
   
   public function Es_DuoTrio(){
       return $this->Es_Trio() || $this->Es_DuoTvInt() || $this->Es_DuoBa() || $this->Es_DuoTv();
   }
           

   public function __construct()
   {
      $this->Paquete = new Paquete();
      $this->Linea = new Linea();
      $this->Internet = new Internet();
      $this->Cable = new Cable();
      $this->Plan = new Plan();
      $this->Ubicacion = new Ubicacion();
      
      $this->Freeview = new Freeview();
      $this->Pedido = new Pedido();
      
      $this->Premium = new Premium();
      
      $this->Herramienta = new Herramienta();

      $this->Componentes = array(
          Componente::Linea => $this->Linea,
          Componente::Internet => $this->Internet,
          Componente::Cable => $this->Cable,
          Componente::Plan => $this->Plan
      );
   }
   
   public function TipoOrigen() {
        if ($this->Internet->Presente && $this->Cable->Presente && $this->Linea->Presente) {
            return 'Trio';
        } elseif ($this->Internet->Presente && $this->Linea->Presente) {
            return 'Duo Ba';
        } elseif ($this->Cable->Presente && $this->Linea->Presente) {
            return 'Duo Tv';
        } elseif ($this->Cable->Presente) {
            return 'Mono Tv';
        } elseif ($this->Linea->Presente) {
            return 'Mono Voz';
        } else {
            return 'Error';
        }
    }

   public $MovistarTotal;
   public $Ciclo = '01';
   public $TieneDeco20448;
   public $ClusterUso;
   public $RestriccionesComerciales;
   public $Telefono;
   public $Nombre;
   public $TipoCliente;
   public $Componentes;
   public $EsBuenPagador;
   public $AntiguedadEnMeses;
   public $Antiguedad;
   public $MigracionesPromo;
   public $ReciboDigital;
   public $SinOfertaADSL;
   public $PaginasBlancas;
   public $ProteccionDatos;
   public $EsEmpleado;
   public $Inestable;
   public $DescuentoApagadoPermanente;
   public $ErrorIncremento;
   public $ErrorReconexion;
   public $MontoAjustado;
   public $ZonaCompetencia;
   public $OperadorZonaCompetencia;
   public $Digitalizado;
   public $Predigitalizado;
   public $EsVOIP;
   
   public $OfertaDeco;
   public $IncrementoPrecio = 0;

   public $AplicaPromoHBO;
   public $AdicionalesFijo;
   
   public $cargoFijo1 = 0;
   public $cargoFijo2 = 0;
   public $AntiguedadLinea1 = 0;
   public $AntiguedadLinea2 = 0;   
   
   /**
    *
    * @var Paquete
    */
   public $Paquete;

   /**
    *
    * @var Internet
    */
   public $Internet;
   
   /**
    *
    * @var Pedido
    */
   public $Pedido;

   /**
    *
    * @var Cable
    */
   public $Cable;
   /**
    *
    * @var Linea
    */
   
   public $Linea;
   public $Plan;
   public $RentaTotal;
     
   
   public $Desposicionado;
   public $PromocionHFC;
   public $PromocionADSL;
   public $PromoDigitalizacion;
   public $PromoVelocidad;
   public $MarcaPromocion;
   
   public $Averias;
   public $ComportamientoPagoCovergente;
   
   public $PromocionHBO;
   public $Tecnica;
   
   public $Mensajes = array();
   public $ListaReclamos = array();
   public $CAM = array();
   
   public $RentaTotalConAdicionales = 0;
   public $RentaHD = 0;
   public $RentaHBO = 0;
   public $RentaFOX = 0;
   public $RentaFox = 0;
   public $TipoDocumento = 'DNI';
   public $Documento = '';
   public $Alta = false;
   
   /**
    *
    * @var Ubicacion 
    */
   public $Ubicacion;
   
   public $Herramienta;  
   
   public $Premium;  
   
   
   public $Presente = 1;
   public $Numero;
   public $Titulo = '';
   public $Subtitulo = '';
   /*Nueva funcionalidad*/
   public $Valido = false;
   public $Descripcion = '';
   
   public $SaltoCero = 0;

}
