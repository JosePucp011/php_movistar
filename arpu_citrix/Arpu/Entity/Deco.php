<?php


namespace Arpu\Entity;

use Arpu\Data\ClienteDL ;


class Deco
{
   public $Registro;
   public $Comercial;
   public $Impacto;
   
   public function AsignarComercial($Tipo,$Pago,$TipoAdquisicion,$ignorar,$PagoComercial)
   {
      if($Tipo === 'Ultra Wifi')
      {
            $texto = $Tipo;   
      }
      else{
       $texto = "1 Deco $Tipo $TipoAdquisicion";   
      }
      
          
      $this->Comercial = 
            array(
                $texto,
                $ignorar,
                "1 $Tipo", 
                \Arpu\Entity\CA::ComponenteRegistro_Deco,
                $PagoComercial,
                $Pago);
   }
   
   public function __construct($ignorar,$Ps,$Costo,$Tipo,$EsAlquiler = false)
   {
      $cliente = ClienteDL::$cliente;
      
      if($EsAlquiler)
      {
         $this->Registro = new OperacionDeco($Ps,'');
         $this->Registro->TipoOperacionComercial = 'ALTA PACK CERO';
         $Pago = $Costo;
         $PagoComercial = "S/.$Costo";
         
      }
      else
      {
         $this->Registro = new OperacionDeco($Ps,'PACK '.$Costo);
         if(0==$Costo)
         {
            $Pago = $Costo;
            $PagoComercial = "";
         }
         elseif($Costo == 1 || $Tipo === 'Ultra Wifi')
         {
            $Pago = $Costo;
            $PagoComercial = "S/.$Costo";
         }
         else
         {
            $mes =24;
            $Pago = $Costo;
            $PagoComercial = "S/. ${Costo} x ${mes} meses";
         }
      }
      
      $this->AsignarComercial($Tipo,$Pago,$EsAlquiler ? 'Alquiler':'Comodato',$ignorar,$PagoComercial);
      $this->Impacto = $Costo;
   }
   
   
   public static $DVR_Pack1;
   
   public static $SD_Pack1;
   
   public static $HD_Pack1;
   public static $HD_Pack90;
   public static $HD_Pack180;
   public static $HD_Pack150;
   public static $HD_Comodato;
   
   public static $HDDTH_Pack1;
   public static $HDDTH_Pack90;
   public static $HDDTH_Pack180;
   public static $HDDTH_Pack150;
   
   public static $SDDTH_Pack1;
   public static $SDDTH_Pack90;
   
   public static $Decos;
   
   public static $HD_Alquiler;
   public static $HDDTH_Alquiler;
   public static $HDDTH_Comodato;
   
   public static $SMART_Pack1;
   public static $SMARTDTH_Pack1; 
}

Deco::$SD_Pack1 = new Deco('Se entregara un Decodificador SD por el pago unico de S/.1',21251,1,'SD');

Deco::$HD_Pack1 = new Deco('Se entregara un Decodificador HD por el pago unico de S/.1',22537,1,'HD');
Deco::$HD_Pack90 = new Deco('Se entregara un Decodificador HD por un costo de S/.12.90 por 24 meses',22537,12.90,'HD');
Deco::$HD_Pack150 = new Deco('Se entregara un Decodificador HD por un costo de S/.12.90 por 24 meses',22537,12.90,'HD');
Deco::$HD_Pack150 = new Deco('Se entregara un Decodificador HD por un costo de S/.12.90 por 24 meses',22537,12.90,'HD');

Deco::$HD_Comodato = new Deco('',21932,0,'HD');
Deco::$HDDTH_Comodato = new Deco('',22644,0,'HD');

//Deco::$DVR_Pack1 = new Deco('Se entregara un Decodificador DVR por el pago unico de S/.1',22536,1,'DVR');
Deco::$DVR_Pack1 = new Deco('Se entregara un Decodificador DVR por el pago unico de S/.1',22536,10,'Ultra Wifi');

Deco::$SDDTH_Pack1 = new Deco('Se entregara un Decodificador SD por el pago unico de S/.1',20709,1,'SD');


Deco::$HDDTH_Pack1 = new Deco('Se entregara un Decodificador HD por el pago unico de S/.1',22642,1,'HD');
Deco::$HDDTH_Pack90 = new Deco('Se entregara un Decodificador HD por un costo de S/.12.90 por 24 meses',22642,12.90,'HD');
Deco::$HDDTH_Pack150 = new Deco('Se entregara un Decodificador HD por un costo de S/.12.90 por 24 meses',22642,12.90,'HD');


Deco::$HD_Alquiler = new Deco('Se entregara un Decodificador HD por un costo de S/.12.90 por 24 meses',21931,12.90,'HD',true);
Deco::$HDDTH_Alquiler = new Deco('Se entregara un Decodificador HD por un costo de S/.12.90 por 24 meses',22643,12.90,'HD',true);


Deco::$SMART_Pack1 = new Deco('Se entregara un Decodificador SMART por el pago unico de S/.1',21933,0,'Smart');
Deco::$SMARTDTH_Pack1 = new Deco('Se entregara un Decodificador SMART por el pago unico de S/.1',22646,0,'Smart');

Deco::$Decos = array(
    'CATV_HD' => Deco::$HD_Comodato,
    'DTH_HD' => Deco::$HDDTH_Comodato,
    'CATV_HD_Alquiler' => Deco::$HD_Alquiler,
    'DTH_HD_Alquiler' => Deco::$HDDTH_Alquiler,
    'CATV_SMART' => Deco::$SMART_Pack1,
    'DTH_SMART' => Deco::$SMARTDTH_Pack1,
    
    'CATV' => array(
        'SD' => array(1 => Deco::$SD_Pack1),
        'HD' => array(
            1 => Deco::$HD_Pack1, 
            90 => Deco::$HD_Pack90, 
            180 => Deco::$HD_Pack180,
            12 => Deco::$HD_Alquiler,
            150 => Deco::$HD_Pack150,
            ),
        'DVR' => array(1 => Deco::$DVR_Pack1),
        'SMART' => array(1 => Deco::$SMART_Pack1),
    ),
    'DTH' => array(
        'SD' => array(1 => Deco::$SDDTH_Pack1, 90 => Deco::$SDDTH_Pack90),
        'HD' => array(
            1 => Deco::$HDDTH_Pack1, 
            90 => Deco::$HDDTH_Pack90, 
            180 => Deco::$HDDTH_Pack180,
            12 => Deco::$HDDTH_Alquiler,
            150 => Deco::$HDDTH_Pack150),
        'SMART' => array(1 => Deco::$SMARTDTH_Pack1),
    )
);




        