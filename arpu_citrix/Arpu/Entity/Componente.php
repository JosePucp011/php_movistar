<?php


namespace Arpu\Entity;

class Componente
{
   const Linea = 'Linea';
   const Cable = 'Cable';
   const Internet = 'Internet';
   const Plan = 'Plan';
   const Premium = 'Premium';
   const Movil = 'Movil';
   const Movil0 = 'Movil0';
   const Movil1 = 'Movil1';

   public $Nombre;
   public $RentaMonoproducto;
   public $Paquetizado;
   public $Nivel;
   public $Presente;
   public $EsSVA;
   
   
   private function Baja($Tipo,Oferta &$oferta)
   {
      $oferta->movimiento[$Tipo] = Movimiento::Baja;
      $oferta->bajaComponente = true;
      $oferta->arpu = round($oferta->arpu + $this->RentaMonoproducto,2);
      
   }
   
   public function Alta($Tipo,Oferta $oferta,$origen)
   {
      $oferta->movimiento[$Tipo] = Movimiento::Alta;
   }
   
   public function Migracion($Tipo,Oferta $oferta,$destino)
   {
      if($destino->Nivel == $this->Nivel)
      {
         $oferta->movimiento[$Tipo] = Movimiento::Mantiene;
      }
      elseif($destino->Nivel > $this->Nivel)
      {
         $oferta->movimiento[$Tipo] = Movimiento::Up;
      }
      else
      {
         $oferta->movimiento[$Tipo] = Movimiento::Down;
      }
      
      $oferta->arpu = round($oferta->arpu + $this->RentaMonoproducto,2);
   }

   public function Comparar($Tipo,Oferta &$oferta,$destino)
   {

      if ($this->Presente)
      {       
         if ($this->Paquetizado && !$destino->Presente)
         {
            $this->Baja($Tipo,$oferta);
         }
         else if (!$this->Paquetizado && !$destino->Presente)
         {
            $oferta->movimiento[$Tipo] = Movimiento::MantieneMono;
            $oferta->Impacto_ArpuSuelto += $this->RentaMonoproducto();
         }
         else
         {
            $this->Migracion($Tipo,$oferta,$destino);
         }
      }
      else if($destino->Presente)
      {
         $destino->Alta($Tipo,$oferta,$this);
      }
      
      
     
   }
   
   public function RentaMonoproducto()
   {
      return $this->RentaMonoproducto;
   }

}


