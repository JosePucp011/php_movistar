<?php

namespace Arpu\Entity;


class FijoREST {
    /*Servicio REST*/
    public $Presente = 0;
    public $Numero = 0;
    public $Titulo = '';
    public $Subtitulo = '';
    /*Nueva funcionalidad*/
    public $Valido = false;
    public $MovistarTotal = false;
    public $Direccion = '';
    public $EsHFC = false;
    public $Descripcion = '';
}
