<?php 

namespace Arpu\Entity;

class Linea extends Componente
{   
    
   public function __toString()
   {
      if($this->Presente)
      {
         return ' '.$this->Nombre;
      }
      return '';
   }
   
      public $RentaTotal = 0;
      public $RentaPrincipal = 0;
      public $Adicional = 0;
      public $Telefono = 0;
      public $Documento;
      public $Descripcion = '';
}

