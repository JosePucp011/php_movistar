<?php

namespace Arpu\Entity;

class Producto {
    /* Llenados por Codigo */

    public $OperacionComercial;
    public $Registro;
    public $Componentes;
    public $RentaAdicional;
    public $EsPermutacion = 0;
    public $Canales;

    public function EsDuoTvInt() {
        return $this->Internet->Presente &&
                !$this->Linea->Presente &&
                $this->Cable->Presente;
    }

    public function EsDuoBa() {
        return $this->Internet->Presente &&
               $this->Linea->Presente &&
               !$this->Cable->Presente;
    }

    public function EsDuoTv() {
        return !$this->Internet->Presente &&
                $this->Linea->Presente &&
                $this->Cable->Presente;
    }

    public function EsTrio() {
        return $this->Internet->Presente &&
                $this->Linea->Presente &&
                $this->Cable->Presente;
    }

    public function RentaAdicionales() {
        $valor = $this->DecosAdicionales * 15.90 + $this->Ultrawifi * 12.90;

        if (count($this->Cable->Premium->PremiumBloque) > 0 &&
                count($this->Cable->Premium->PremiumBloque[0]->PremiumContenido) > 0) {
            $valor += count($this->Cable->Premium->PremiumBloque[0]->PremiumContenido) * 20 - 20;
        }
        $this->RentaAdicional = $valor;
        return $valor;
    }

    public function EsSva() {
        return $this->Paquete->EsSVA == 1;
    }

    public function __construct() {
        $this->Linea = new Linea();
        $this->Plan = new Plan();
        $this->Internet = new Internet();
        $this->Cable = new Cable();
        $this->Movil0 = new Movil();
        $this->Movil1 = new Movil();
        
        $this->Componentes = array(
            Componente::Linea => $this->Linea,
            Componente::Internet => $this->Internet,
            Componente::Cable => $this->Cable,
            Componente::Plan => $this->Plan,
            'Movil0' => $this->Movil0,
            'Movil1' => $this->Movil1
        );
        $this->Paquete = new Paquete();
    }

    public function crearPs() {
        $operacion = new Operacion(
                $this->Paquete->Ps, 
                $this->Paquete->Nombre, 
                \Arpu\Entity\CA::ComponenteRegistro_Paquete,
                $this->OperacionComercial);
        return $operacion;
    }

    public function AjustarDatos() {
        
    }

    public function __clone() {
        $this->Paquete = clone $this->Paquete;
        $this->Linea = clone $this->Linea;
        $this->Cable = clone $this->Cable;
        $this->Internet = clone $this->Internet;
        $this->Plan = clone $this->Plan;
        $this->Componentes = array(
            Componente::Linea => $this->Linea,
            Componente::Internet => $this->Internet,
            Componente::Cable => $this->Cable,
            Componente::Plan => $this->Plan,
            'Movil0' => $this->Movil0,
            'Movil1' => $this->Movil1
        );
    }

    /* scalar */

    public $Identificador;

    /**
     *
     * @var Paquete
     */
    public $Paquete;


    public $ExcluirConFFTT;


    public $EsHDTotal;

    /**
     *
     * @var Linea
     */
    public $Linea;

    /**
     *
     * @var Internet
     */
    public $Internet;

    /**
     *
     * @var Cable
     */
    public $Cable;

    /**
     *
     * @var Plan 
     */
    public $Plan;
    public $TipoComercializacion;
    
    public $Movil0;
    public $Movil1;
    public $Habilitado;

}
