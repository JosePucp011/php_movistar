<?php

namespace Arpu\Entity;

use Arpu\Entity\Operacion;
use Arpu\Entity\Oferta;

class Internet extends Componente {

    public $Tecnologia;
    public $Velocidad;
    public $TasaGarantizada;
    public $ModemPs;
    public $ModemNombre;
    public $ModemDescripcion;
    public $TienePAM;
    public $Descripcion;
    public $PsAdministrativa;
    public $Naked = 0;
    public $Titulo = '';

    /**
     *
     * @var Cobertura
     */
    public $Cobertura;

    public function __construct() {
        $this->Cobertura = new Cobertura();
    }

    public function Comparar($Tipo, Oferta &$oferta, $destino) {
        parent::Comparar($Tipo, $oferta, $destino);
        $tecnologia = $destino->Tecnologia;
        if (in_array($tecnologia, array('ADSL', 'HFC', 'FTTH'))) {
            $this->$tecnologia($oferta, $destino);
        }
    }
    //Called dynamically
    private function HFC(Oferta $oferta, Internet $destino) {
        
        if ('ADSL' == $this->Tecnologia || !$this->Presente) {
            $modemHFC = $destino->Velocidad < 30000 ? self::$modem_hfc :
                    self::$modem_SmartWifi_HFC;

            $oferta->Registro[] = $this->Cobertura->FTTH ?
                    self::$modem_gpon : $modemHFC;
            $oferta->ModemReemplazo = $this->Cobertura->FTTH ?
                    self::$modem_gpon : $modemHFC;


            $oferta->UbicacionModem = count($oferta->Registro) - 1;
            ++$oferta->CantidadDeModems;
        } elseif (20025 == $this->ModemPs) {
            $oferta->Registro[] = self::$modem_hfc;
            $oferta->UbicacionModem = count($oferta->Registro) - 1;
            $oferta->ModemReemplazo = self::$modem_hfc;
            $oferta->Registro[] = self::$modem_hfcbaja;
            ++$oferta->CantidadDeModems;
        }
    }
    
    //Called dynamically
    private function FTTH(Oferta $oferta, Internet $destino) {
        
        if ('ADSL' == $this->Tecnologia || !$this->Presente) {
            $modemHFC = $destino->Velocidad < 30000 ? self::$modem_hfc :
                    self::$modem_SmartWifi_HFC;

            $oferta->Registro[] = $this->Cobertura->FTTH ?
                    self::$modem_gpon : $modemHFC;
            $oferta->ModemReemplazo = $this->Cobertura->FTTH ?
                    self::$modem_gpon : $modemHFC;


            $oferta->UbicacionModem = count($oferta->Registro) - 1;
            ++$oferta->CantidadDeModems;
        } elseif (20025 == $this->ModemPs) {
            $oferta->Registro[] = self::$modem_hfc;
            $oferta->UbicacionModem = count($oferta->Registro) - 1;
            $oferta->ModemReemplazo = self::$modem_hfc;
            $oferta->Registro[] = self::$modem_hfcbaja;
            ++$oferta->CantidadDeModems;
        }
    }

    //Called dynamically
    private function ADSL(Oferta $oferta, Internet $destino) {
        if (!$this->Presente && $destino->Presente) {
            if ($destino->Velocidad < 30000) {
                $oferta->Registro[] = self::$modem_adsl;
                $oferta->ModemReemplazo = self::$modem_adsl;
            } else {
                $oferta->Registro[] = self::$modem_SmartWifi_ADSL;
                $oferta->ModemReemplazo = self::$modem_SmartWifi_ADSL;
            }
            $oferta->UbicacionModem = count($oferta->Registro) - 1;
            ++$oferta->CantidadDeModems;
        }
    }

    public function __toString() {
        $respuesta = '';
        if ($this->Presente) {
            $respuesta .= ' ' . str_replace("kbps", "kb", str_replace('Mbps', 'Mb', $this->Nombre));
        }
        return $respuesta;
    }

    public static $modem_adsl;
    public static $modem_hfc;
    public static $modem_hfcbaja;
    public static $modem_gpon;
    public static $modem_adsl_1;
    public static $modem_hfc_1;
    public static $modem_gpon_1;
    public static $UltraWifi;
    public static $modem_SmartWifi_ADSL;
    public static $modem_SmartWifi_HFC;

}

Internet::$modem_hfcbaja = new Operacion(20025, 'Modem Docsis 2', 'Modem', 'Baja', '1', 'Modem Docsis 2', '');
Internet::$modem_adsl = new Operacion(9781, 'Modem ADSL', 'Modem', 'Alta PACK CERO', 'PACK 0.5', 'Modem ADSL', '');
Internet::$modem_hfc = new Operacion(22745, 'Modem Docsis 3', 'Modem', 'Alta PACK CERO', 'PACK 0.5', 'Modem Docsis 3', '');
Internet::$modem_gpon = new Operacion(23009, 'Modem GPON', 'Modem', 'Alta PACK CERO', 'PACK 0.5', 'Modem GPON', '');
Internet::$modem_SmartWifi_ADSL = new Operacion(23059, 'Modem Smart Wifi', 'Modem', 'Alta PACK CERO', 'PACK 0.5', 'Modem Smart Wifi', '');
Internet::$modem_SmartWifi_HFC = new Operacion(23060, 'Modem Smart Wifi', 'Modem', 'Alta PACK CERO', 'PACK 0.5', 'Modem Smart Wifi', '');


Internet::$modem_adsl_1 = new Operacion(9781, 'Modem ADSL', 'Modem', 'Alta', 'PACK 1', 'Modem ADSL<br><b>Pago unico S/.1</b>', '');
Internet::$modem_hfc_1 = new Operacion(22745, 'Modem Docsis 3', 'Modem', 'Alta', 'PACK 1', 'Modem Docsis 3<br><b>Pago unico S/.1</b>', '');
Internet::$modem_gpon_1 = new Operacion(23009, 'Modem GPON', 'Modem', 'Alta', 'PACK 1', 'Modem GPON<br><b>Pago unico S/.1</b>', '');

Internet::$UltraWifi = new Operacion(23022, 'Ultra Wifi', 'Ultra Wifi', 'Alta', 'PROM PACK 1', 'Modem GPON<br><b>Pago unico S/.1</b>', 'S/. 1');

