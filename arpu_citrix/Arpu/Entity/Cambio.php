<?php

namespace Arpu\Entity;

class Cambio
{
   public function __construct($Origen,$Destino)
   {
      $this->Origen = $Origen;
      $this->Destino = $Destino;
   }
   
   public $Origen;
   public $Destino;
}