<?php

namespace Arpu\Entity;

class CA 
{
    const Up = 'Up';
    const Mantiene = 'Mantiene';
    const Down = 'Down';
    const Baja = 'Baja';
    const Alta = 'Alta';
    const MantieneMono = 'Mantiene Monoproducto';
    
    const AltaFinanciamiento = 'Alta con Financiamiento';
    const NoEvalua = 'No Evalua Componente';
    
    const DTH = 1;
    const Analogica = 2;
    const Digital = 4;
    const Digitalizada = 8;
    
    public static $Zona = array(
        'DTH' => 1,
        'Analogica' => 2,
        'Digital' => 4,
        'Digitalizada' => 8,
    );
    
    
    
    const Regular = 0;
    const Inestable = 1;
    const DesposicionadoInternet = 2;
    const DesposicionadoTelevision = 4;
    const Menos100Clientes = 8;
    const Competencia = 16;
    const Voy = 32;
    const PriorizarHFC = 33; // (32 + 1);
    
    const ComponenteRegistro_Deco = 'Deco';
    const Modem = 'Modem';
    const UltraWifi = 'Ultra Wifi';
    const ComponenteRegistro_Paquete = 'Paquete';
    const ComponenteRegistro_Canales = 'Canales';
    const ComponenteRegistro_Punto = 'Punto';
    const ComponenteRegistro_Descuento = 'Descuento';
    const ComponenteRegistro_Velocidad = 'Velocidad';
    const ComponenteRegistro_Antena = 'Antena';
        
    public static $PsEmbajador = array(22519,22524);
}



