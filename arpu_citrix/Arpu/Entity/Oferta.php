<?php
namespace Arpu\Entity;
use Arpu\Entity\MensajeDescuento;
use Arpu\Entity\Operacion;
class Oferta
{
   public $EsCompletaTuTrio = false;
   public $CantidadDeModems = 0;
   public $ExclusionCobroInstalacion = false;
   public $EntregaDecosAnalogicos = false;
   public $ModemReemplazo;
   public $UbicacionModem = 0;
   public $AltaBloques = 0;
   public $BajaBloques = 0;
   public $codigoOperacionComercial = 999;
   public $Descuentos = array();

   


   public function PremiumConsolidado()
   {
       $texto = $this->producto->Cable->Premium->Nombre;
       
       if($this->ComponentesMono[Componente::Premium] != null)
       {
           $texto .= ($texto == '' ? '': '|').$this->ComponentesMono[Componente::Premium]->Nombre;
       }
       return $texto;
   }
   
   public function PlanConsolidado()
   {
       $texto = 'MD '.round($this->producto->Plan->RentaMonoproducto);
       
       if($this->ComponentesMono[Componente::Plan] != null)
       {
           $texto = 'MD '.round($this->ComponentesMono[Componente::Plan]->RentaMonoproducto);
       }
       return $texto;
   }
   
   public function CableConsolidado()
   {
       $texto = $this->producto->Cable->Nombre;
       
       if($this->ComponentesMono[Componente::Cable] != null)
       {
           $texto .= $this->ComponentesMono[Componente::Cable]->Nombre;
       }
       return $texto;
   }
   
   public function InternetConsolidado()
   {
       $texto = $this->producto->Internet->Velocidad;
       
       if($this->ComponentesMono[Componente::Internet] != null)
       {
           $texto .= $this->ComponentesMono[Componente::Internet]->Velocidad;
       }
       return $texto;
   }
   
   public function EnRangoArpu($menor,$mayor){
       return $this->salto_arpu >= $menor && $this->salto_arpu <= $mayor;
   }
   
   public $ComercialDeco = array();
   public $RegistroDeco = array();
   
   public function __construct(Producto $producto) {
        $this->producto = $producto;
        $this->Registro = array($producto->Registro);

        foreach ($producto->Paquete->PsAdicionales as $Adicional) {
            $this->Registro[] = Operacion::RegistroPsAdicionales($Adicional);
        }

        if ($this->producto->Internet->PsAdministrativa != null) {
            $this->oferta->Registro[] = new Operacion(
                    $this->producto->Internet->PsAdministrativa, 
                    $this->producto->Internet->Descripcion, '', 'Alta', '');
        }
    }

    
   public $EsClienteInestable;

  
   public function AgregarDeco($deco,$indice = 0)
   {
       if(!isset($this->RegistroDeco[$indice]))
       {
           $this->RegistroDeco[$indice] = array();
       }
       
      $this->RegistroDeco[$indice][] = $deco->Registro;
      
       if(!isset($this->ComercialDeco[$indice]))
       {
           $this->ComercialDeco[$indice] = array();
       }
       
      $this->ComercialDeco[$indice][][] = $deco->Comercial;
      
      if(0 == $indice )
      {
        ++$this->CantidadDeDecos;
      }
   }
   
   public function AgregarDescuento($descuento)
   {
      $this->Registro[] = Operacion::RegistroDescuentoCampana(
              $descuento->Ps, 
              $descuento->Nombre);
      $this->Comercial[] = (new MensajeDescuento($descuento->Nombre))->toArray();
   }
   
   /*scalar*/
   public $Impacto_ArpuSuelto = 0.0;
   /*scalar*/
   public $bajaComponente;
   /*scalar*/
   public $Nombre;
   /*scalar*/
   public $TieneCobroDeco;
   
   public $TieneCobroModem;
   
   public $TieneUltraWifi = false;
   public $Modem = '';
   public $DecosNombre = '';
   
   /*scalar*/
   public $EsEspejoAdslHFC;
   
   /*scalar*/
   public $arpu;
   /*scalar*/
   public $salto_arpu = 0.0;
   /*scalar*/
   public $tipoOperacionComercial;
   /*array*/
   public $movimiento = array(
            Componente::Linea => Movimiento::NoEvalua,
            Componente::Internet => Movimiento::NoEvalua,
            Componente::Cable => Movimiento::NoEvalua,
            Componente::Plan => Movimiento::NoEvalua,
            Componente::Premium => Movimiento::NoEvalua,
            'Movil0' => Movimiento::NoEvalua,
            'Movil1' => Movimiento::NoEvalua
       
       );
   
   /*scalar*/
   public $trafico;
   
   public $ofertaSugeridaSVA;
   
   /*scalar*/
   public $cargo_fijo_origen;
   /*scalar*/
   public $cargo_fijo_destino;
   
   /**
    * @var Producto
    */
   public $producto;
   
   /*array*/
   public $orden;
   /*scalar*/
   public $mejor_up;
   
   /*scalar*/
   public $descripcionMovimiento;
   
   /*scalar*/
   public $requiereActuacionTecnica;
   
   /*array of arrays*/
   public $Comercial = array();
   /**
    *
    * @var Operacion[]
    */
   public $Registro;
   
   /*scalar*/
   public $CantidadDeDecos = 0;
   /*array of constant objects*/
   public $ComponentesMono = array(
            Componente::Linea => null,
            Componente::Internet => null,
            Componente::Cable => null,
            Componente::Plan => null,
            Componente::Premium => null
        );
   
   /*scalar*/
   public $Tipo;
   
   public function setOperacionComercial($valor){
       $this->tipoOperacionComercial = $valor;
       $this->codigoOperacionComercial = self::$OperacionComercial[$valor];
   }
            
   public static $OperacionComercial = array(
      /*'SVA' => 207,
      'Baja Componente' => 999,
      'Completa Linea y BA' => 205,
      'Completa TV y BA' => 202,
      'Completa TV' => 201,
      'Completa BA' => 203,
      'Migracion' => 206,
      'Alta' => 999*/
      
      'SVA' => 207,
      'Baja Componente' => 999,
      'Completa Linea y BA' => 205,
      'Completa TV y BA' => 202,
      'Completa TV' => 201,
      'Completa BA' => 203,
      'Migracion' => 206,
      'Baja Linea y Completa TV' => 210,
      'Baja Linea y Completa BA' => 208,
      'Completa Linea' => 209,
      'Baja Linea y Mantiene Internet' => 211,
      'Completa TV y Linea' => 212,
      'Alta' => 999
  );

   public $Cabecera;
}
