<?php

namespace Arpu\Entity;

class ReglaDeco
{
   public function __construct($regla)
   {
      foreach(explode(';', $regla) as $decos)
      {
            $this->Decos = array();
            foreach(explode('|', $decos) as $deco)
            {
               $datosDeco = explode(',',$deco);

               if(!isset($this->Decos[$datosDeco[0]]))
               {
                  $this->Decos[$datosDeco[0]] = array();
               }

               $this->Decos[$datosDeco[0]][] = new SubRegla($datosDeco[1], isset($datosDeco[2]));
            }
            $this->Decodificadores[] = $this->Decos;
      }
   }
   public $Decodificadores = array();
}

class SubRegla
{
   public function __construct($Precio,$Obligatorio)
   {
      $this->Precio = $Precio;
      $this->Obligatorio = $Obligatorio;
   }
   
   public $Precio;
   public $Obligatorio;
   public $Alquiler;
}
