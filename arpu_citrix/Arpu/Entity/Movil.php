<?php

namespace Arpu\Entity;

/**
 * Description of MOvil
 *
 * @author dvictoriad
 */
class Movil extends Componente {
    
   public $Ciclo;
   public $Posicion = 'movil1';
   public $Equipo;
   public $CtdMBRoaming = 0;
   public $CtdMinutosMovilMovil = 0;
   public $CtdMinutosMovilCompetencia = 0;
   public $Paquetizado = 0;
   public $Roaming = '';
   public $Minutos = '';
   public $Datos = '';
   public $Producto = '';
   public $TipoLinea = '';
   public $GVS = '';
   public $App = '';
   public $MayorIgual8Meses = false;
   public $IdPlan = 0;
   public $EsEmpleado = 0;
   public $DatosInternacionales = "-"; // agre
   public $WhatsappIlimitado = "-"; // agre
   public $Pasagigas = "-"; // agre
   

   public function Baja($Tipo, Oferta &$oferta)
   {
      parent::Baja($Tipo, $oferta);
   }

   public function Alta($Tipo, Oferta $oferta, $origen)
   {
      parent::Alta($Tipo, $oferta, $origen);
   }
   
   public function Migracion($Tipo, Oferta $oferta, $destino)
   {
      parent::Migracion($Tipo, $oferta, $destino);
   }
}
