<?php

namespace Arpu\Entity;

class ProductoServicio
{

   public $Ps;
   public $Nombre;
   public $NombreComercial;
   public $costoDeDeco;
   public $Renta;
   public $Fecha_alta;
   public $NombreAtis;
   
   public function __construct($Ps=null,$Nombre=null,$NombreComercial=null)
   {
      $this->Ps = $Ps;
      $this->Nombre = $Nombre;
      $this->NombreComercial = $NombreComercial;
   }


}
