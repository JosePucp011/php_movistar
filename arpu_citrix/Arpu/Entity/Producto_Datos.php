<?php

namespace Arpu\Entity;

class Producto_Datos {

    public $OperacionComercial;
    public $PsHD;
    public $PsFOX;
    public $PsHBO;
    public $FOXRenta;
    public $HDRenta;
    public $HBORenta;
    public $PermutarUltrawifi;
    public $EsPermutacion;
    public $Identificador;
    public $TipoComercializacion;
    public $Paquete_Ps;
    public $Paquete_PsAdicionales;
    public $Paquete_Nombre;
    public $Paquete_Renta;
    public $Linea_Nombre;
    public $Linea_Nivel;
    public $Internet_Velocidad;
    public $Internet_Tecnologia;
    public $Internet_Cobertura_Iquitos;
    public $Internet_PsAdministrativa;
    public $Internet_Descripcion;
    public $Cable_Categoria;
    public $Cable_Nivel;
    public $Cable_Tipo;
    public $PremiumNombre;
    public $Plan_Nivel;
    public $Tmp_CableCobertura;
    public $Paquete_PsDescuento;
    public $TieneHDIncluido;
    public $Ultrawifi;
    public $DecosAdicionales;
    public $Paquete_Tipo;
    public $Paquete_RentaOrigen;
    public $Linea_Presente;
    public $Linea_Paquetizado;
    public $Cable_Presente;
    public $Cable_Nombre;
    public $Internet_Nivel;
    public $Internet_Paquetizado;
    public $Internet_Presente;
    public $Internet_Nombre;
    public $Plan_Presente;
    public $Plan_Paquetizado;
    public $Plan_Nombre;
    public $Cable_Paquetizado;
    public $Cable_EsSVA;
    public $BloquePrioridad;
    public $Paquete_EsSVA;
    public $Internet_EsSVA;
    public $Plan_EsSVA;
    public $Cable_TipoSVA;
    
    
    /*Datos Convergencia*/
    public $Paquete_RentaFija;
    public $Movil0_Nivel;
    public $Movil1_Nivel;
    public $Movil0_Roaming;
    public $Movil1_Roaming;
    public $Movil0_Nombre;
    public $Movil0_Renta;
    public $Movil1_Renta;
    public $Movil1_Nombre;
    public $Paquete_CodigoMovil;
    public $Paquete_NombreMovil;
    public $Paquete_Score;
    public $PrecioRegular;
    public $Paquete_RentaSinModificar;
    public $AplicaDecoSmart;


}
