<?php 


namespace Arpu\Entity;

class Herramienta 
{  
   /**
    *
    * @var Cobertura
    */
   public $Compensacion;
   public $DescuentoRetenciones;
   public $DecodificadorAdicional;
   public $Gratuidad;
   public $Equipamiento;
   public $Solucion;
   public $BonoMovil;
   
   
   public function __construct()
   {
      $this->Equipamiento = Array();
      $this->Solucion = Array();
      $this->Gratuidad = Array();
      $this->BonoMovil = Array();
      
   }
}

