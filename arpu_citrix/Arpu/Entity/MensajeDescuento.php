<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Arpu\Entity;

/**
 * Description of MensajeDescuento
 *
 * @author dvictoriad
 */
class MensajeDescuento extends MensajeComercial
{
   public function __construct($mensaje)
   {
      parent::__construct($mensaje);
      $this->Tipo = \Arpu\Entity\CA::ComponenteRegistro_Descuento;
   }
}
