<?php

namespace Arpu\Entity;

class DescuentoRetenciones
{
   /**
    *
    * @var string[]
    */
   public $Orden;
   public $Ps;  
   public $Nombre;
   public $Rango;
   public $Tipo;
   public $Descuento;
}
