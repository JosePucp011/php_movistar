<?php 

namespace Arpu\Entity;
class Premium
{
   public $TieneHDI;
   public $TieneHD;
   /**
    *
    * @var PremiumBloque[]
    */
   public $PremiumBloque;
   public $Presente;
   public $Nombre;
   public $RentaMonoproducto;
   public $Descripcion;
   public $Canales;
   public $CanalesPaquete;
   public $CanalesMono;

   public function __construct()
   {
      $this->PremiumBloque = array();
      $this->Canales = '';
      $this->Descripcion = '';
   }
   
   public function TieneSenal($senal)
   {
      if(null == $this->PremiumBloque)
      {
         return false;
      }
      
      foreach($this->PremiumBloque as $premiumBloque)
      {
         if(in_array($senal,$premiumBloque->PremiumContenido))
         {
            return true;
         }
      }
      return false;
   }
   
   public function Nombre()
   {
      $resultado = "";
      foreach($this->PremiumBloque as $componente)
      {
         foreach($componente->PremiumContenido as $contenido){
            if($contenido==""){
               continue;
            }
            
            $resultado .= $contenido . '|';
         }
      }
      return preg_replace('/\\|$/', '', $resultado);
   }
   
   public function NombreUSSD()
   {
      $resultado = "";
      foreach($this->PremiumBloque as $componente)
      {
         foreach($componente->PremiumContenido as $contenido){
            if($contenido=="")
               continue;
            
            if($resultado !== ""){
                $resultado .= '+';
            }
            $resultado .= $contenido;
         }
      }
      return $resultado;
   }
   
}
