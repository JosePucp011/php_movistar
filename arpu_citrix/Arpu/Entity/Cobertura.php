<?php

namespace Arpu\Entity;


class Cobertura {
    public $VelocidadMaxima;
    public $VelocidadActuacion;
    public $HFC;
    public $FTTH;
    public $Iquitos;
    public $RadioEnlace;
    public $MigracionDTH;
    
   public function __construct()
   {
        $this->FTTH = 0;
        $this->MigracionDTH = 0;
   }
}
