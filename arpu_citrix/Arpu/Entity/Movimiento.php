<?php
namespace Arpu\Entity;
class Movimiento
{
   const Up = 'Up';
   const Mantiene = 'Mantiene';
   const Down = 'Down';
   const Baja = 'Baja';
   const Alta = 'Alta';
   const NoEvalua = 'No Evalua Componente';
   const MantieneMono = 'Mantiene Monoproducto';
   
   
   public static $NoEmpeora = 
            array(Movimiento::Up,Movimiento::Mantiene,Movimiento::NoEvalua,Movimiento::MantieneMono);
   public static $NoReduce =
            array(
                Movimiento::Up,
                Movimiento::Mantiene,
                Movimiento::NoEvalua,
                Movimiento::MantieneMono,
                Movimiento::Alta);
   
   
   public static $Mejora = array(Movimiento::Up,Movimiento::Alta);
}
   
