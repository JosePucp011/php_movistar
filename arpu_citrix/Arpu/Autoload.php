<?php

function CalculadoraArpu_Autoload($class)
{
   if(strpos($class,"Arpu\\") === 0)
   {
      require dirname(__FILE__) ."/../".str_replace("\\", "/", $class).".php";
   }
}

spl_autoload_register('CalculadoraArpu_Autoload');
