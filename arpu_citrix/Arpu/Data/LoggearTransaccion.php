<?php



namespace Arpu\Data;

use Arpu\Config\Config;
use Exception;

class LoggearTransaccion {
    private $usuario = '';
    private $queryString = '';
    private $resultado = '';
    private $script = '';
    private $id;
    
    public function __construct() {
        if(isset($_SERVER['QUERY_STRING'])){
            $this->queryString = $_SERVER['QUERY_STRING'];
        }
        
        if(isset($_SESSION["usuario"])){
            $this->usuario = $_SESSION["usuario"];
        }
        
        
        if(isset($_SERVER['SCRIPT_FILENAME'])){
            $this->script = basename($_SERVER['SCRIPT_FILENAME']);
        }
        
        $this->id = uniqid();
    }
    
    public function setearResultado($resultado){
        $this->resultado = $resultado;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function log(){
        try{
            $conexion = Config::ConexionStandalone();
            $query = $conexion->prepare('insert into log(id,query,usuario,script,resultado) values(?,?,?,?,compress(?));');
            $query->bindValue(1,$this->id);
            $query->bindValue(2,$this->queryString);
            $query->bindValue(3,$this->usuario);
            $query->bindValue(4,$this->script);
            $query->bindValue(5,$this->resultado);
            if(!$query->execute()){
                // Fail silently
            }
        } catch (Exception $ex) {
          // Fail silently
        }
    }
    
    
    
}
