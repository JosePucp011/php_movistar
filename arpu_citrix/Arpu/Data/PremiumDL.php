<?php 
namespace Arpu\Data;
use Arpu\Config\Config;
use Arpu\Entity\Premium;

class PremiumDL
{
   /**
    * 
    * @return PremiumBloque
    */
   public static function Obtener()
   {
      $conexion = Config::ConexionBD();
      $obtenerPremium = $conexion->prepare("select * from catalogo_premium_");
      $obtenerPremium->execute();

      $premium = array();
      while (($producto = $obtenerPremium->fetchObject()))
      {
         $producto->PremiumContenido = explode('|', $producto->PremiumContenido);
         $premium[$producto->PremiumPs] = $producto;
      }
      PremiumDL::$cache = $premium;    
   }
   
   public static function ConstruirPremiumBloque($PsMono)
   {
       if(isset($datosProducto->Cable_Tipo))
           return null;
           
       $premiumBloque = clone self::$cache[$PsMono];
       $premiumBloque->Paquetizado = 0;
       $premiumBloque->Presente = true;
       
       return $premiumBloque;
   }
   
   public static function Construir($PsPaquete,$PsMono)
   {
      $premium = new Premium();
      $premium->RentaMonoproducto = 0.0;
      $premium->TieneHDI = false;
      if ($PsPaquete && isset(self::$cache[$PsPaquete]))
      {
         $premiumBloque = clone self::$cache[$PsPaquete];
         $premiumBloque->Paquetizado = 1;
         $premiumBloque->Presente = true;
         $premiumBloque->PremiumRenta = 0.0;
         $premium->Presente = true;
         $premium->PremiumBloque[] = $premiumBloque;
      }

      if ($PsMono)
      {
         $premium->Presente = true;
         foreach (explode(';', $PsMono) as $ps)
         {
            if(!isset(self::$cache[$ps])){
                continue;
            }
             
             
            if ($ps == '21148')
            {
               $premium->TieneHDI = true;
            }
            $premiumBloque = clone self::$cache[$ps];
            $premium->RentaMonoproducto +=  $premiumBloque->PremiumRenta;
            $premiumBloque->Paquetizado = 0;
            $premiumBloque->Presente = true;
            $premium->PremiumBloque[] = $premiumBloque;
         }
      }
      $premium->Nombre = $premium->Nombre();
      return $premium;
   }
 
   
   public static $cache;
}


PremiumDL::Obtener();
