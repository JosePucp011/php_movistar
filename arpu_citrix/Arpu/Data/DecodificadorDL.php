<?php
namespace Arpu\Data;

use Arpu\Config\Config;
use Arpu\Entity\ExceptionBasedatos;
class DecodificadorDL
{
   public static function Obtener()
   {
      $conexion = Config::ConexionBD();
      $obtenerDecodificadores = $conexion->prepare("select * from catalogo_equipamiento_");
      if(!$obtenerDecodificadores || !$obtenerDecodificadores->execute()){
          throw new ExceptionBasedatos();
      }
      
      
      $decodificadores = array();
      while (($producto = $obtenerDecodificadores->fetchObject()))
      {
         $decodificadores[$producto->Ps] = $producto;
      }
      self::$cache = $decodificadores;
   }
      
   public static $cache;
}

DecodificadorDL::Obtener();
