<?php

namespace Arpu\Data;

use Arpu\Util\EjecutarQuery;
use Arpu\Entity\FijoREST;
use Arpu\Entity\MovilREST;
use Arpu\Exception\MalComportamientoException;
use Arpu\Exception\DocumentoEmpleado;


class IdentificadorDL {
    const QUERY_FIJO_DOCUMENTO = 'select * from Planta_Fija where documento = ?';
    const QUERY_PEDIDO_DOCUMENTO = "select * from pedidos_movistartotal where Documento = ?";
    //const QUERY_NOTIFICACION_DOCUMENTO = "select Identificador, Documento, Mensaje, Fecha_Inicio, Fecha_Fin from pedidos_movistartotalNotificacion where CURDATE() >= Fecha_Inicio AND CURDATE() <= Fecha_Fin AND DOCUMENTO = ?";
    const QUERY_NOTIFICACION_MT_DOCUMENTO = "select * from planta_notificaciones where documento = ?";
    const QUERY_LINEA_PROVISIONADA = "select * from lineas_provisionadas where documento = ?";  // ye 6
    const QUERY_REASIGNACION_MT_DNI = "select * from tmp_reasignacion_precio_mt where DNI = ?  limit 1";

    private static $moviles;
    private static $fijos;
    private static $pedidos;
    private static $notificaciones;
    private static $reasignacion;
    private static $provisionadas;
    private static $resultado;
    
    public static function ConsultarDocumentoMultiple($documento){
        self::$resultado = new \stdClass();
        self::LeerDatos($documento);
        self::ValidarDatos($documento);
        self::ConstruirRespuestaMultiple();
 
        return self::$resultado;
    }
    
    
    
    public static function ConstruirRespuestaMultiple(){
        self::ConstruirHogar();
        self::ConstruirFijos();
        self::ConstruirMoviles();
        self::$resultado->Pedidos = self::$pedidos;
        self::$resultado->Notificaciones = self::$notificaciones;
        self::$resultado->Provisionadas = self::$provisionadas;
        self::$resultado->Reasignacion = self::$reasignacion;
    }
    
    private static function ConstruirHogar(){
        
        $HogarFijo = false;
        $HogarMovil = false;
        
        self::$resultado->Hogar = new \stdClass();
        self::$resultado->Hogar->Fijos = array();
        self::$resultado->Hogar->Presente = false;
        self::$resultado->Hogar->Tipo = 0;
        self::$resultado->Empleado = 0;
        foreach(self::$fijos as $fijo){
            if($fijo->MovistarTotal === 1){
                self::$resultado->Hogar->Presente = true;
                self::$resultado->Hogar->Fijos[] = $fijo;
                self::$resultado->Hogar->Tipo = 1;
                $HogarFijo = true;
            }
            if($fijo->EsEmpleado > 0){
                self::$resultado->Empleado = 1;
            }
            
        }
        self::$resultado->Hogar->Moviles = array();
        
        foreach(self::$moviles as $movil){
            if($movil->MovistarTotal === 1){
                self::$resultado->Hogar->Presente = true;
                self::$resultado->Hogar->Moviles[] = $movil;
                self::$resultado->Hogar->Tipo = 1;
                $HogarMovil = true;
            }
            
            if($movil->EsEmpleado > 0){
                self::$resultado->Empleado = 1;
            }
        }
        
        if($HogarFijo && $HogarMovil){
            self::$resultado->Hogar->Tipo = 2;
        }
    }
    
    private static function ConstruirFijos(){
        self::$resultado->Fijos = [];   
        foreach(self::$fijos as $fijo){
            if($fijo->MovistarTotal === 0){
                self::$resultado->Fijos[] = $fijo;
            }
        }
    }
    
    private static function ConstruirMoviles(){
        self::$resultado->Moviles = [];
        foreach(self::$moviles as $movil){
                        
            if($movil->MovistarTotal === 0){
                self::$resultado->Moviles[] = $movil;
            }
        }
    }

    
    private static function LeerDatos($documento){
        self::$moviles = ClienteDLMovil::LeerMovilesPorDocumento($documento);
        self::$fijos = ClienteDL::LeerPorDocumento($documento);
        
        foreach(self::$moviles as $movil){
            self::$resultado->Nombre = $movil->Nombre;
            break;
        }
        
        foreach(self::$fijos as $fijo){
            
            if($fijo->ComportamientoPagoCovergente && !$fijo->EsEmpleado ){ // emple 1
                throw new MalComportamientoException();
            }
            
            self::$resultado->Nombre = $fijo->Nombre;
            break;
        }

        try {
            self::$pedidos = EjecutarQuery::Ejecutar(self::QUERY_PEDIDO_DOCUMENTO, [$documento]);
        } catch(\Exception $exception) {
            self::$pedidos =  [];
        }

        //self::$notificaciones = EjecutarQuery::Ejecutar(self::QUERY_NOTIFICACION_DOCUMENTO, [$documento]);

        self::$notificaciones = EjecutarQuery::Ejecutar(self::QUERY_NOTIFICACION_MT_DOCUMENTO, [$documento]);
        self::$provisionadas = EjecutarQuery::Ejecutar(self::QUERY_LINEA_PROVISIONADA, [$documento]); // ye 7
        self::$reasignacion = EjecutarQuery::Ejecutar(self::QUERY_REASIGNACION_MT_DNI, [$documento]);
        //self::$notificaciones = [];
    }
    
    
    private static function ValidarDatos($documento){
        if(strlen($documento)==11) {
            throw new \Arpu\Exception\DocumentoRUC();
        }

        if(count(self::$moviles) == 0 && count(self::$fijos) == 0 && count(self::$pedidos) == 0 ){
            throw new \Arpu\Exception\DocumentoNoEncontrado();
        }
    }
    //(CLIENTE NO TIENE PEDIDOS)
}