<?php

namespace Arpu\Data;

use Arpu\Entity\MensajesTemporales;
use Arpu\Entity\Cliente;
use Arpu\Config\Config;

class MensajesTemporalesDL {
   

    public static function AgregarACliente(Cliente $cliente) {
        $query = self::EjecutarQuery($cliente->Telefono);

        while (($datos = $query->fetchObject())) {           
                $mensaje = self::Construirmensaje($datos);                
                $cliente->mensaje[] = $mensaje;          
        }
    }
       
    
  
    /**
     * 
     * @param string $telefono
     * @return \PDOStatement
     * @throws ExceptionBasedatos
     */
    private static function EjecutarQuery($telefono){
        $conexion = Config::ConexionBD();
        $query = $conexion->prepare(self::QUERY_PLANTA_MSJ);
        $query->bindValue(1, $telefono);
        if (!$query->execute()) {
            throw new ExceptionBasedatos();
        }
        return $query;
    }

    /**
     * 
     * @param \stdClass $datos
     * @return mensaje
     */
    private static function Construirmensaje($datos) {
        $mensaje = new MensajesTemporales();
        
        if ($datos->Telefono != null) {          
            $mensaje->Telefono = $datos->Telefono;            
            $mensaje->Mensaje  = $datos->Mensaje;           
            $mensaje->Tipo  = $datos->Tipo;
        }
          
        
        return $mensaje;
    }
    
    const QUERY_PLANTA_MSJ = 'select * from MensajesTemporales  
                        where Telefono = ?';
}
