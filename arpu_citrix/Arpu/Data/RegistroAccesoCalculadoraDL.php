<?php

namespace Arpu\Data;
use Arpu\Config\Config;

class RegistroAccesoCalculadoraDL
{
    
    public static function RegistrarConsulta($OfertaSugerida){
        self::insertarRegistroDeAcceso(
           $OfertaSugerida->Cliente->Telefono,
           0, 
           $OfertaSugerida->Retenciones,
           'Consulta telefono'
           );
    }
    
    
   
   public static function insertarRegistroDeAcceso($telefono,$ps,$modo,$accion)
   {
      $conexion = Config::ConexionBD();
      
      $InsertaRegistro = $conexion->prepare("INSERT INTO planta_consultas(telefono,hora,ip,ps,modo,accion)
        VALUES (?,current_timestamp,?,?,?,?)  
        ");
      
      
      $InsertaRegistro->bindValue(1, $telefono);
      $InsertaRegistro->bindValue(2, $_SERVER['REMOTE_ADDR']);
      $InsertaRegistro->bindValue(3, $ps);
      $InsertaRegistro->bindValue(4, $modo);
      $InsertaRegistro->bindValue(5, $accion);
      
      if(!$InsertaRegistro->execute())
      {
         throw new \InvalidArgumentException("Parametros de log incorrectos");
      }
   }
   
public static function insertarRegistroDeLog($telefono,$usuario,$accion)
    {
      $conexion = Config::ConexionBD();
      
	  $InsertaRegistro = $conexion->prepare("INSERT INTO planta_logs(telefono,hora,ip,usuario,accion)
        VALUES (?,current_timestamp,?,?,?)  
        ");
      
      $InsertaRegistro->bindValue(1, $telefono);
      $InsertaRegistro->bindValue(2, $_SERVER['REMOTE_ADDR']);
      $InsertaRegistro->bindValue(3, $usuario);
      $InsertaRegistro->bindValue(4, $accion);
			
      if(!$InsertaRegistro->execute())
      {
         throw new \InvalidArgumentException("Parametros de log incorrectos");
      }
    }
    
    public static function insertarRegistroDeLogeo($usuario,$accion)
    {
      $conexion = Config::ConexionBD();
      $InsertaRegistroLog = $conexion->prepare("INSERT INTO Logeo(FECHA_HORA_EVENTO,ID,TAG,USUARIO,IP_USUARIO_CONEXION,HOST_NAME_USUARIO,TIPO_CONEXION)
        VALUES (current_timestamp,?,?,?,?,?,?)  
        ");
      
      $InsertaRegistroLog->bindValue(1, 0);
      $InsertaRegistroLog->bindValue(2, $accion);
      $InsertaRegistroLog->bindValue(3, $usuario);
      $InsertaRegistroLog->bindValue(4, $_SERVER['REMOTE_ADDR']);
      $InsertaRegistroLog->bindValue(5, $_SERVER['REMOTE_ADDR']);
      $InsertaRegistroLog->bindValue(6, 'WEB');
	
      if(!$InsertaRegistroLog->execute())
      {
         throw new \InvalidArgumentException("Parametros de log incorrectos");
      }
    }

    public static function insertarRegistroDeTransaccion($telefono,$usuario,$accion)
    {
      $conexion = Config::ConexionBD();
      
	$InsertaRegistro = $conexion->prepare("INSERT INTO Transaccion(FECHA_HORA_EVENTO,ID,TAG,USUARIO,LOGIN_ID,TELEFONO,IP_USUARIO_CONEXION)
        VALUES (current_timestamp,?,?,?,?,?,?)  
        ");
      
      $InsertaRegistro->bindValue(1, '0');  
      $InsertaRegistro->bindValue(2, $accion);
      $InsertaRegistro->bindValue(3, $usuario);
      $InsertaRegistro->bindValue(4, '0');
      $InsertaRegistro->bindValue(5, $telefono);
      $InsertaRegistro->bindValue(6, $_SERVER['REMOTE_ADDR']);
			
      if(!$InsertaRegistro->execute())
      {
         throw new \InvalidArgumentException("Parametros Incorrectos");
      }
    }
    
   
}
