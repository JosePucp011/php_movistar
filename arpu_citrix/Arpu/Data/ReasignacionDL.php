<?php

namespace Arpu\Data;

use Arpu\Entity\Reasignacion;
use Arpu\Entity\Cliente;
use Arpu\Config\Config;

class ReasignacionDL {
   

    public static function AgregarACliente(Cliente $cliente) {
        $query = self::EjecutarQuery($cliente->Telefono);

        while (($datos = $query->fetchObject())) {
            if (self::AplicaReasignacionCliente($cliente,$datos)) {
                
                $reasignacion = self::ConstruirReasignacion($datos);                
                $cliente->Reasignacion[] = $reasignacion;
                
            }
        }
    }
    
    private static function AplicaReasignacionCliente(Cliente $cliente,$datos){
        return $cliente->MovistarTotal==1;
    }
    
    
  
    /**
     * 
     * @param string $telefono
     * @return \PDOStatement
     * @throws ExceptionBasedatos
     */
    private static function EjecutarQuery($telefono){
        $conexion = Config::ConexionBD();
        $query = $conexion->prepare(self::QUERY_PLANTA_REASIGNACION);
        $query->bindValue(1, $telefono);
        if (!$query->execute()) {
            throw new ExceptionBasedatos();
        }
        return $query;
    }

    /**
     * 
     * @param \stdClass $datos
     * @return Reasignacion
     */
    private static function ConstruirReasignacion($datos) {
        $reasignacion = new Reasignacion();
        
        if ($datos->fecha != null) {
            $reasignacion->DNI =$datos->DNI;
            $reasignacion->Telefono = $datos->Telefono;
            $reasignacion->Monto_Total_MT_NUEVO  = $datos->Monto_Total_MT_NUEVO;
            $reasignacion->Monto_plan_movil_actual1 = $datos->Monto_plan_movil_actual1;
            $reasignacion->RENTA_MOVIL_NUEVO1 = $datos->RENTA_MOVIL_NUEVO1;
            $reasignacion->TARIFA_INTERNET_FIJO_ACTUAL  = $datos->TARIFA_INTERNET_FIJO_ACTUAL;
            $reasignacion->TARIFA_INTERNET_FIJO_NUEVA  = $datos->TARIFA_INTERNET_FIJO_NUEVA;
            $reasignacion->fecha  = $datos->fecha;
            $reasignacion->Movil_1  = $datos->Movil_1;
            $reasignacion->Movil_2  = $datos->Movil_2;
            $reasignacion->Mensaje  = $datos->Mensaje;
            $reasignacion->Mensaje1  = $datos->Mensaje1;
            $reasignacion->Ajuste  = $datos->Ajuste;
            $reasignacion->Ajuste1  = $datos->Ajuste1;
            $reasignacion->Tipo  = $datos->Tipo;
        }
          
        
        return $reasignacion;
    }
    
    const QUERY_PLANTA_REASIGNACION = 'select * from tmp_reasignacion_precio_mt  
                        where Telefono = ?';
}
