<?php
namespace Arpu\Data;

use Arpu\Entity\Pedido;
use Arpu\Entity\Cliente;
use Arpu\Config\Config;

class PedidoDL {
   

    public static function AgregarACliente(Cliente $cliente) {
        if($query = self::EjecutarQuery($cliente->Documento)) {
            while ($datos = $query->fetchObject()) {
                    $pedido = self::ConstruirPedido($datos);             
                    $cliente->Pedido = $pedido;
            }
        }
    }
    /**
     * 
     * @param string $documento
     * @return \PDOStatement
     * @throws ExceptionBasedatos
     */
    private static function EjecutarQuery($documento){
        try {
            $conexion = Config::ConexionBD();
            $query = $conexion->prepare(self::QUERY_PLANTA_PEDIDOS);
            $query->bindValue(1, $documento);
            if (!$query->execute()) {
                throw new ExceptionBasedatos();
            }
            return $query;
        } catch(\Exception $exception) {
            return [];
        }
    }

    /**
     * 
     * @param \stdClass $datos
     * @return Mensaje
     */

    private static function ConstruirPedido($datos) {
        
        $pedido = new Pedido();
        
        
        return $pedido;
    }   
    const QUERY_PLANTA_PEDIDOS = 'select * from pedidos_movistartotal where Documento = ? limit 1';
}
