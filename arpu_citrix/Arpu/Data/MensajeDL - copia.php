<?php

namespace Arpu\Data;

use Arpu\Entity\Mensaje;
use Arpu\Entity\Cambio;
use Arpu\Entity\Cliente;
use Arpu\Config\Config;

class MensajeDL {
   

    public static function AgregarACliente(Cliente $cliente) {
        $query = self::EjecutarQuery($cliente->Telefono);

        while (($datos = $query->fetchObject())) {
            if (self::AplicaMensajeCliente($cliente,$datos)) {
                
                $mensaje = self::ConstruirMensaje($datos);
                self::MarcarIncrementoPrecio($cliente,$mensaje);
                $cliente->Mensajes[] = $mensaje;
                
            }
        }
    }
    
    private static function AplicaMensajeCliente(Cliente $cliente,$datos){
        return $cliente->Paquete->Ps == $datos->Paquete_Ps || $datos->Paquete_Ps == 0;
    }
    
    
    private static function MarcarIncrementoPrecio(Cliente $cliente,Mensaje $mensaje){
        if ($mensaje->Mensaje === 'Incremento de Precio') {
            $cliente->IncrementoPrecio = 1;
        }
    }
    /**
     * 
     * @param string $telefono
     * @return \PDOStatement
     * @throws ExceptionBasedatos
     */
    private static function EjecutarQuery($telefono){
        $conexion = Config::ConexionBD();
        $query = $conexion->prepare(self::QUERY_PLANTA_MENSAJES);
        $query->bindValue(1, $telefono);
        if (!$query->execute()) {
            throw new ExceptionBasedatos();
        }
        return $query;
    }

    /**
     * 
     * @param \stdClass $datos
     * @return Mensaje
     */
    private static function ConstruirMensaje($datos) {
        $mensaje = new Mensaje();
        
        if ($datos->Fecha != null) {
            $mensaje->Fecha = $datos->Fecha;
            $mensaje->FechaMensaje = $datos->FechaMensaje; // Fecha Para Mensaje
        }
        $mensaje->Mensaje = $datos->Mensaje;
        $mensaje->Devolucion = $datos->Devolucion;

        $campos = array('Renta', 'Velocidad', 'Minutos', 'Canales', 'Mes', 'MPlay','Prix');
        foreach ($campos as $campo) {
            if ($datos->{"Origen_$campo"} != null && $datos->{"Origen_$campo"} != 'NULL') {
                $mensaje->$campo = new Cambio($datos->{"Origen_$campo"}, $datos->{"Destino_$campo"});
            }
        }
        
        return $mensaje;
    }
    
    const QUERY_PLANTA_MENSAJES = 'select * from planta_mensajes_stage 
                        where Telefono = ? 
                        order by Fecha desc limit 1';
}
