<?php

namespace Arpu\Data;
use Arpu\Entity\Componente;
use Arpu\Http\LectorParametros;

class CombinarClienteDL {
    
    private static $clienteFijo;
    private static $clienteMoviles;
    private static $indiceMovil;
    
    public static function AsignarCobertura($cliente,$cobertura){
        if($cobertura === 'HFC'){
            $cliente->Internet->Cobertura->HFC = 1;
            $cliente->Cable->Cobertura = 12;
        } elseif($cobertura === 'FTTH') {
            $cliente->Internet->Cobertura->FTTH = 1;
            $cliente->Internet->Cobertura->HFC = 2;
            $cliente->Cable->Cobertura = 12;
        } else {
            $cliente->Internet->Cobertura->FTTH = 0;
            $cliente->Internet->Cobertura->HFC = 0;
        }
    }
    
    public static function CombinarClientes($clienteFijo, $clienteMoviles, $documento, $tipDocumento) {
        if($clienteFijo === null && count($clienteMoviles) === 0){
            $cliente = new \Arpu\Entity\Cliente;
            $cliente->Alta = true;

            $cliente->Documento = $documento;
            $cliente->Nombre = null;
            $cliente->TipoDocumento = $tipDocumento;
            $cliente->Correo = '-';

            return $cliente;
        }

        self::$clienteFijo = $clienteFijo;
        self::$clienteMoviles = $clienteMoviles;

        $primerCliente = self::IdentificarPrimercliente();
        
        self::CalcularIndiceMovil($primerCliente);
        self::AgregarClientesMoviles($primerCliente, self::$clienteMoviles);
        self::CalcularDocumento($primerCliente,$documento,$tipDocumento);
        return $primerCliente;
    }

    private static function IdentificarPrimercliente() {
        if (self::$clienteFijo !== null) {
            return self::$clienteFijo;
        }

        if (count(self::$clienteMoviles) > 0) {
            return array_pop(self::$clienteMoviles);
        }
    }

    public static function CalcularDocumento($primerCliente,$documento,$tipDocumento){
        $blDocumentoVerif = false;

        if(isset($primerCliente->Componentes[Componente::Linea])) {
            if($primerCliente->Componentes[Componente::Linea]->Documento == $documento) {
                $blDocumentoVerif = true;
            }
        }

        for ($i=0; $i < 2; $i++) { 
            if(isset($primerCliente->Componentes[Componente::Movil.$i]) && 
               !$blDocumentoVerif) {
                if($primerCliente->Componentes[Componente::Movil.$i]->Documento == $documento) {
                    $blDocumentoVerif = true;
    
                    $primerCliente->Documento = $primerCliente->Componentes[Componente::Movil.$i]->Documento;
                    $primerCliente->Nombre = $primerCliente->Componentes[Componente::Movil.$i]->Nombre;
                    $primerCliente->TipoDocumento = $primerCliente->Componentes[Componente::Movil.$i]->TipoDocumento;
                    $primerCliente->Correo = '-';
                }
            }
        }

        if(!$blDocumentoVerif) {
            $primerCliente->Documento = $documento;
            $primerCliente->Nombre = null;
            $primerCliente->TipoDocumento = $tipDocumento;
            $primerCliente->Correo = '-';
        }
    }

    public static function CalcularIndiceMovil($primerCliente){
        self::$indiceMovil = isset($primerCliente->Componentes[Componente::Movil . '0']) ? 1 : 0;
    }
    
    private static function AgregarClientesMoviles($primerCliente,$clientesMoviles){
        
        foreach ($clientesMoviles as $movil) {
            $primerCliente->Componentes[Componente::Movil . self::$indiceMovil] = $movil->Componentes[Componente::Movil . '0'];
            ++self::$indiceMovil;
            $primerCliente->RentaTotal += $movil->RentaTotal;
            $primerCliente->RentaTotal = round($primerCliente->RentaTotal, 2);
            $primerCliente->RentaTotalConAdicionales += $movil->RentaTotal;
            $primerCliente->RentaTotalConAdicionales = round($primerCliente->RentaTotal, 2);
        }
    }
}