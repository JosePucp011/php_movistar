<?php

namespace Arpu\Data;

use Arpu\Config\Config;
use Arpu\Entity\Premium;
use Arpu\Entity\PremiumBloque;
use Arpu\Entity\Producto;
use Arpu\Entity\Producto_Datos;
use \Arpu\Entity\Cliente;
use Arpu\Entity\PremiumCliente;

class ProductoDL {
    
    
    public static function Obtener(Cliente $cliente,$modo='Regular') {
        
        $premiumCliente = self::ObtenerPremiumCliente($cliente);
        if(!isset(self::$cache[$premiumCliente->llave])){
            self::$cache[$premiumCliente->llave] = array();
            self::$productos = &self::$cache[$premiumCliente->llave];
            self::CrearProductosCliente($premiumCliente,$cliente,$modo);
        }
        return self::$cache[$premiumCliente->llave];
    }

    public static function CrearProductosCliente($premiumCliente,Cliente $cliente,$modo='Regular'){
        $conexion = Config::ConexionBD();
        $obtenerProductos = $conexion->prepare(self::QUERY_PRODUCTOS);
        
        if (!$obtenerProductos->execute()) {
            print_r($obtenerProductos->errorInfo());
            throw new \Exception("No se puede obtener la lista de productos");
        }

        while (($datosProducto = $obtenerProductos->fetchObject('Arpu\\Entity\\Producto_Datos'))) {
            
            self::TransformarFTTH($datosProducto, $cliente);
            $permutaciones = self::ObtenerPermutaciones($datosProducto,$premiumCliente,$modo);
            self::AgregarVariosProductos($permutaciones);
        }
    }
    
    private static function TransformarFTTH($datosProducto, Cliente $cliente) {
        
        if($cliente->Internet->Cobertura->FTTH && $datosProducto->Internet_Tecnologia == 'HFC' && $datosProducto->Paquete_Categoria != 'Naked CMS')
        {
            $datosProducto->Internet_Tecnologia = 'FTTH';
        }
        
        return $datosProducto;

        /*if($cliente->Internet->Cobertura->FTTH && $datosProducto->Internet_Tecnologia == 'HFC')
        {
            $datosProducto->Internet_Tecnologia = 'FTTH';
        }
        
        return $datosProducto;*/
    }
    
    public static function ObtenerConvergente(Cliente $cliente,$modo='CuatroPlay'){
        $premiumCliente = self::ObtenerPremiumCliente($cliente);
        $conexion = Config::ConexionBD();
        $obtenerProductos = $conexion->prepare(self::QUERY_CONVERGENTE);
        
        self::$productos = array();

        if (!$obtenerProductos->execute()) {
            print_r($obtenerProductos->errorInfo());
            throw new \Exception("No se puede obtener la lista de productos");
        }

        while (($datosProducto = $obtenerProductos->fetchObject('Arpu\\Entity\\Producto_Datos'))){
            self::ModificarTelevision($datosProducto, $cliente, $modo);
            self::ObtenerPremium($datosProducto, $cliente);
            self::ObtenerModem($datosProducto, $cliente);
            self::ObtenerDeco($datosProducto, $cliente);
            $permutaciones = self::ObtenerPermutaciones($datosProducto,$premiumCliente);
            self::AgregarVariosProductos($permutaciones);
        }
                
        return self::$productos;
    }

    private static function ModificarTelevision($datosProducto, $cliente, $modo) {
        if(($datosProducto->Paquete_Ps == 23247 && $datosProducto->Paquete_CodigoMovil == 4471728) ||
        ($datosProducto->Paquete_Ps == 23246 && $datosProducto->Paquete_CodigoMovil == 4471718) ||
        ($datosProducto->Paquete_Ps == 23244 && $datosProducto->Paquete_CodigoMovil == 4501328) ||
        ($datosProducto->Paquete_Ps == 23244 && $datosProducto->Paquete_CodigoMovil == 4471708)){
            $datosProducto->Cable_Descripcion .='';// ' + <b>Netflix x 6 meses</b>';
        }

        if( (
            ($datosProducto->Paquete_Ps == 23243 && $datosProducto->Paquete_CodigoMovil == 4471698) || 
            ($datosProducto->Paquete_Ps == 23242 && $datosProducto->Paquete_CodigoMovil == 4471688) ||
            ($datosProducto->Paquete_Ps == 23240 && $datosProducto->Paquete_CodigoMovil == 4471668) || 
            ($datosProducto->Paquete_Ps == 23241 && $datosProducto->Paquete_CodigoMovil == 4471678) 

            ) 
          && $modo === 'CuatroPlayOnline'){
            $datosProducto->Cable_Descripcion .='';// ' + <b>Netflix x 3 meses <span style="color: red;">(Exclusivo Online)</span></b>';
        }

       /*  if((($datosProducto->Paquete_Ps == 23243 && $datosProducto->Paquete_CodigoMovil == 4471698) || 
        ($datosProducto->Paquete_Ps == 23242 && $datosProducto->Paquete_CodigoMovil == 4471688)) 
          && $modo === 'CuatroPlayOnline'){
            $datosProducto->Cable_Descripcion .= ' + <b>Netflix x 3 meses <span style="color: red;">(Exclusivo Online)</span></b>';
        } */
    }
    
    private static function ObtenerDeco($datosProducto, $cliente) {
       
        if(!$cliente->Cable->Presente || 
        (!$cliente->Cable->CantidadDecosDigitales 
        && !$cliente->Cable->CantidadDecosHD 
        && !$cliente->Cable->CantidadDecosSmart)){        
                    $datosProducto->DecosNombre  = '2 Decos HD';  
    }
    else if($cliente->Cable->CantidadDecosDigitales>0
            && !$cliente->Cable->CantidadDecosHD
            && !$cliente->Cable->CantidadDecosSmart){
                    $datosProducto->DecosNombre  = '2 Decos HD';        
    }
    else if( ($cliente->Cable->CantidadDecosHD + $cliente->Cable->CantidadDecosSmart) == 1 ){
                    $datosProducto->DecosNombre  = '1 Deco HD'; 
    }
    else if( ($cliente->Cable->CantidadDecosHD + $cliente->Cable->CantidadDecosSmart) >= 2 ){
        $datosProducto->DecosNombre  = '-'; 
    }
    else{
        $datosProducto->DecosNombre  = '-'; 
    }       

    /* if($cliente->Cable->Presente == 1){
        if($datosProducto->Paquete_Renta <= 299){
            if(($cliente->Cable->CantidadDecosHD + $cliente->Cable->CantidadDecosSmart ) > 0){
                $datosProducto->DecosNombre  = '-';
            }else{
                $datosProducto->DecosNombre  = '1 Deco HD';
            }
        }else{
            $datosProducto->DecosNombre  = '1 Deco Smart';
        }
   } */
    }   
    
    private static function ObtenerPremium($datosProducto, $cliente) {
        
        $datosProducto->Canales = '-';
        
        if($cliente->Premium === null ) {
            return; 
        }
        if($cliente->Premium->Canales != null 
                && $cliente->Premium->Canales != '')
        {
            $datosProducto->Canales = $cliente->Premium->Canales;
           
            if($datosProducto->Paquete_Ps != 23240)
            {
                $datosProducto->Canales = str_replace("HD,","",$datosProducto->Canales);
                $datosProducto->Canales = str_replace("HD","",$datosProducto->Canales);
            }
            $datosProducto->Canales = str_replace("MC","FOX",$datosProducto->Canales);
            $datosProducto->Canales = str_replace("GP","Golden Premier",$datosProducto->Canales);
            $datosProducto->Canales = str_replace("HP","Hot Pack",$datosProducto->Canales);
            $datosProducto->Canales = str_replace("EST","Estelar",$datosProducto->Canales);
            
            $Bloque = '';
            if($datosProducto->Canales != ''){
                $Bloque =   'Bloque '.str_replace(","," + Bloque ",$datosProducto->Canales);
            }

            $datosProducto->Canales = $Bloque;   
        }
        
        if($cliente->Cable->Tipo === 'Estelar'){
            if($datosProducto->Canales === ''){
                $datosProducto->Canales = 'Bloque Estelar';
            }else{
                $datosProducto->Canales .= ' + Bloque Estelar';
            }
        }
        
        
    }

    private static function ObtenerModem($datosProducto, $cliente) {
        
        if(!$cliente->Internet->Presente){
        $datosProducto->Internet_Modem = 'Modem Smart Wifi';
        }
        else if($cliente->Internet->Tecnologia=='ADSL' && $cliente->Internet->Cobertura->HFC && 
                ( $cliente->Internet->ModemNombre=='MODEM ADSL' 
                || $cliente->Internet->ModemNombre=='Modem Dual Band' )){
            $datosProducto->Internet_Modem = 'Modem Smart Wifi';
        }
        else if($cliente->Internet->Tecnologia=='HFC' && $cliente->Internet->Cobertura->HFC){

                if($cliente->Internet->ModemNombre=='MODEM DOCSIS 2'){
                    $datosProducto->Internet_Modem = 'Modem Smart Wifi';
                }
                else if($cliente->Internet->ModemNombre=='MODEM DOCSIS 3' || $cliente->Internet->ModemNombre=='Modem Dual Band'){
                    $datosProducto->Internet_Modem = '-';
                }            
        }
        else if($cliente->Internet->Tecnologia=='ADSL' && $cliente->Internet->Cobertura->FTTH && 
                ( $cliente->Internet->ModemNombre=='MODEM ADSL' 
                        || $cliente->Internet->ModemNombre=='Modem Dual Band' )){
                    $datosProducto->Internet_Modem = 'Modem GPON';
        }
        else if($cliente->Internet->Tecnologia=='FTTH' && $cliente->Internet->Cobertura->FTTH && 
                $cliente->Internet->ModemNombre=='Modem Smart Wifi'){
                    $datosProducto->Internet_Modem = '-';
        }

       /*  if($cliente->Internet->Presente){
            $datosProducto->Internet_Modem = 'Modem Smart Wifi';
        }  
        else if($cliente->Internet->Tecnologia=='ADSL' && $datosProducto->Internet_Tecnologia=='HFC' && 
                ( $cliente->Internet->ModemNombre=='MODEM ADSL' 
                  || $cliente->Internet->ModemNombre=='Modem Dual Band' )){
            $datosProducto->Internet_Modem = 'Modem Smart Wifi';
        }
        else if($cliente->Internet->Tecnologia=='HFC' && $datosProducto->Internet_Tecnologia=='HFC'){

                if($cliente->Internet->ModemNombre=='MODEM DOCSIS 2'){
                    $datosProducto->Internet_Modem = 'Modem Smart Wifi';
                }
                else if($cliente->Internet->ModemNombre=='MODEM DOCSIS 3' || $cliente->Internet->ModemNombre=='Modem Dual Band'){
                    $datosProducto->Internet_Modem = '-';
                }
            
        } */
   
    /*     if($cliente->Internet->ModemDescripcion === 'Modem Smart Wifi'
                ||  $cliente->Internet->ModemDescripcion === 'Modem HFC'){
//            
//            if($datosProducto->Internet_Modem === 'Modem Smart Wifi'){
//                $datosProducto->Internet_Modem = 'Mantiene + Repetidor Smart';
//            }else{
                $datosProducto->Internet_Modem = 'Mantiene';
//            }
        } */
    }    
    
    private function AgregarVariosProductos($datosProductos) {
        foreach ($datosProductos as $datos) {
            $producto = self::Construir($datos);
            $producto->Registro = $producto->crearPs();
            self::$productos[$producto->Identificador] = $producto;
        }
    }
    
    private static function ObtenerPermutaciones(Producto_Datos $datosProducto,PremiumCliente $premiumCliente,$modo='Regular') {

        $resultado = array();
        $resultado[] = $datosProducto;
        
        if($datosProducto->PsHD > 0){
            $resultado = array_merge($resultado,self::AgregarHD($resultado)); // +10000
        }
        
        if($datosProducto->PsHBO > 0){
            $resultado = array_merge($resultado,self::AgregarHBO($resultado,$premiumCliente)); // +100000
        }
        
        if($datosProducto->PsFOX > 0){
            $resultado = array_merge($resultado,self::AgregarFOX($resultado,$premiumCliente)); // +1000000
        }
        
//        if($datosProducto->PermutarUltrawifi > 0 && $modo!= 'Online'){
//            $resultado = array_merge($resultado,self::Agregar_UltraWifi($resultado)); // 1000
//        }
        
        foreach($resultado as $datos){
            $datos->Paquete_Renta = round($datos->Paquete_Renta,2);
        }

        return $resultado;
    }
    
    /**
     * 
     * @param \Arpu\Entity\Producto_Datos[] $input
     */
    private static function Agregar_UltraWifi($input){
        $resultado = array();
        foreach($input as $datosProducto){
            
            $nuevoDatos = clone $datosProducto;
            $nuevoDatos->EsPermutacion = 1;
            $nuevoDatos->Paquete_Renta += 12.9;
            $nuevoDatos->Paquete_Nombre .= ' + Smart Wifi';
            $nuevoDatos->Ultrawifi = 1;
            $nuevoDatos->Identificador += 1000;
            $resultado[] = $nuevoDatos;
        }
        return $resultado;
    }
    
    /**
     * 
     * @param \Arpu\Entity\Producto_Datos[] $input
     */
    private static function AgregarHD($input){
        
        $resultado = array();
        foreach($input as $datosProducto){
            $nuevoDatos = clone $datosProducto;
            $nuevoDatos->EsPermutacion = 1;
            $nuevoDatos->Paquete_Renta += $datosProducto->HDRenta;
            $nuevoDatos->Identificador += 10000;
            $nuevoDatos->Paquete_Nombre .= ' HD';
            if($nuevoDatos->Paquete_PsAdicionales !== ''){
                $nuevoDatos->Paquete_PsAdicionales .= '|'.$datosProducto->PsHD;
            } else {
                $nuevoDatos->Paquete_PsAdicionales .= $datosProducto->PsHD;
            }
            
            if($nuevoDatos->PremiumNombre !== ''){
                $nuevoDatos->PremiumNombre .= '|HD';
            } else {
                $nuevoDatos->PremiumNombre .= 'HD';
            }
            
            $resultado[] = $nuevoDatos;
        }
        return $resultado;
    }
    
    /**
     * 
     * @param \Arpu\Entity\Producto_Datos[] $input
     * @param PremiumCliente $premiumCliente
     * @return type
     */
    private static function AgregarHBO($input,  PremiumCliente $premiumCliente){
        $resultado = array();
        foreach ($input as $datosProducto) {

            if ($premiumCliente->HBORenta > 0) {
                $psHBO = $premiumCliente->PsHBO;
                $HBORenta = $premiumCliente->HBORenta;
            } else {
                $psHBO = $datosProducto->PsHBO;
                $HBORenta = $datosProducto->HBORenta;
            }


            $nuevoDatos = clone $datosProducto;
            $nuevoDatos->EsPermutacion = 1;
            $nuevoDatos->Identificador += 100000;
            $nuevoDatos->Paquete_Nombre .= ' HBO';
            $nuevoDatos->Paquete_Renta += $HBORenta;

            if ($nuevoDatos->Paquete_PsAdicionales !== '') {
                $nuevoDatos->Paquete_PsAdicionales .= '|' . $psHBO;
            } else {
                $nuevoDatos->Paquete_PsAdicionales .= $psHBO;
            }

            if ($nuevoDatos->PremiumNombre !== '') {
                $nuevoDatos->PremiumNombre .= '|HBO';
            } else {
                $nuevoDatos->PremiumNombre .= 'HBO';
            }

            $resultado[] = $nuevoDatos;
        }
        return $resultado;
    }
    
    
    /**
     * 
     * @param \Arpu\Entity\Producto_Datos[] $input
     * @param PremiumCliente $premiumCliente
     * @return type
     */
    private static function AgregarFOX($input,  PremiumCliente $premiumCliente){
        $resultado = array();
        foreach ($input as $datosProducto) {
            if ($premiumCliente->FOXRenta > 0) {
                
                $psFOX = $premiumCliente->PsFOX;
                $FOXRenta = $premiumCliente->FOXRenta;
            } else {
                $psFOX = $datosProducto->PsFOX;
                $FOXRenta = $datosProducto->FOXRenta;
            }


            $nuevoDatos = clone $datosProducto;
            $nuevoDatos->EsPermutacion = 1;
            $nuevoDatos->Identificador += 1000000;
            $nuevoDatos->Paquete_Nombre .= ' FOX';
            $nuevoDatos->Paquete_Renta += $FOXRenta;

            if ($nuevoDatos->Paquete_PsAdicionales !== '') {
                $nuevoDatos->Paquete_PsAdicionales .= '|' . $psFOX;
            } else {
                $nuevoDatos->Paquete_PsAdicionales .= $psFOX;
            }

            if ($nuevoDatos->PremiumNombre !== '') {
                $nuevoDatos->PremiumNombre .= '|MC';
            } else {
                $nuevoDatos->PremiumNombre .= 'MC';
            }

            $resultado[] = $nuevoDatos;
        }
        return $resultado;
    }
    
    
    
    private static function ObtenerPremiumCliente(Cliente $cliente){
        $premium = new PremiumCliente();
        
        self::ObtenerPsHBO($premium,$cliente);
        self::ObtenerPsFOX($premium,$cliente);
        
        $premium->llave = $premium->PsHBO.'-'.$premium->PsFOX;
        
        return $premium;
    }
    
    private static function ObtenerPsHBO(PremiumCliente $premium,Cliente $cliente){
        foreach($cliente->Cable->Premium->PremiumBloque as $bloque){
            if(!$bloque->Paquetizado && $bloque->PremiumContenido[0] === 'HBO' && count($bloque->PremiumContenido) === 1){
                $premium->PsHBO =  $bloque->PremiumPs;
                $premium->HBORenta = $bloque->PremiumRenta;
                return;
            }
        }
        $premium->PsHBO =  0;
        $premium->HBORenta = 0;
    }
    
    private static function ObtenerPsFOX(PremiumCliente $premium,Cliente $cliente){
        foreach($cliente->Cable->Premium->PremiumBloque as $bloque){
            if(!$bloque->Paquetizado && $bloque->PremiumContenido[0] === 'MC' && count($bloque->PremiumContenido) === 1){
                $premium->PsFOX =  $bloque->PremiumPs;
                $premium->FOXRenta = $bloque->PremiumRenta;
                return;
             }
        }
        $premium->PsFOX =  0;
        $premium->FOXRenta = 0;
    }
    
    const QUERY_CONVERGENTE = "select * from catalogo_productos where habilitado=1 and "
            . " Paquete_Tipo = '4Play'"; // agre
    
    const QUERY_PRODUCTOS = "
select	
Identificador	,
TipoComercializacion	,
Paquete_Ps	,
Paquete_PsAdicionales	,
Paquete_Nombre	,
Paquete_Renta	,
Linea_Nombre	,
Linea_Nivel	,
Internet_Velocidad	,
Internet_Tecnologia	,
Internet_Cobertura_Iquitos	,
Internet_PsAdministrativa	,
Internet_Descripcion	,
Cable_Categoria	,
Cable_Nivel	,
Cable_Tipo	,
PremiumNombre	,
Plan_Nivel	,
Tmp_CableCobertura	,
Paquete_PsDescuento	,
TieneHDIncluido	,
Ultrawifi	,
DecosAdicionales	,
Paquete_Tipo	,
Paquete_Categoria	,
Paquete_RentaOrigen	,
Linea_Presente	,
Linea_Paquetizado	,
Cable_Presente	,
Cable_Nombre	,
Internet_Nivel	,
Internet_Paquetizado	,
Internet_Presente	,
Internet_Nombre	,
Plan_Presente	,
Plan_Paquetizado	,
Plan_Nombre	,
Cable_Paquetizado	,
Cable_EsSVA	,
BloquePrioridad	,
Paquete_EsSVA	,
Internet_EsSVA	,
Plan_EsSVA	,
Cable_TipoSVA	,
EsPermutacion	,
PsHD	,
PsFOX	,
PsHBO	,
FOXRenta	,
HDRenta	,
HBORenta	,
PermutarUltrawifi	,
OperacionComercial	
from catalogo_productos
where Paquete_Tipo != '4Play'
";
    

    public static function Construir($datosProducto) {

        $producto = new Producto();
        EstructuraDL::Construir($producto, $datosProducto);

        self::CoberturaCable($producto, $datosProducto);
        self::Premium($producto, $datosProducto);
        self::AsignarDependencia($producto);
        $producto->AjustarDatos();

        return $producto;
    }

    /**
     * 
     * @param Producto $producto
     * @param stdclass $datosProducto
     */
    public static function CoberturaCable($producto, $datosProducto) {
        $producto->Cable->Cobertura = 0;

        if (isset($datosProducto->Tmp_CableCobertura)) {
            $zonas = explode('|', $datosProducto->Tmp_CableCobertura);
            foreach ($zonas as $zona) {
                $producto->Cable->Cobertura |= \Arpu\Entity\CA::$Zona[$zona];
            }
        }
    }

    private static function AsignarDependencia($producto) {
        if (isset($producto->Paquete->PsAdicionales) && $producto->Paquete->PsAdicionales != null && is_string($producto->Paquete->PsAdicionales)) {
            $producto->Paquete->PsAdicionales = explode("|", $producto->Paquete->PsAdicionales);
        } else {
            $producto->Paquete->PsAdicionales = array();
        }

        if (isset($producto->Paquete->PsDescuento) && $producto->Paquete->PsDescuento != null) {
            $producto->Paquete->PsAdicionales[] = $producto->Paquete->PsDescuento;
        }
    }

    private static function Premium($producto, $datosProducto) {
        $Premium = new Premium();
        if (isset($datosProducto->PremiumNombre) && $datosProducto->PremiumNombre != null) {
            $PremiumBloque = new PremiumBloque();
            $PremiumBloque->Nombre = $datosProducto->PremiumNombre;
            $PremiumBloque->Paquetizado = $datosProducto->PremiumNombre != null;
            $PremiumBloque->Presente = $datosProducto->PremiumNombre != null;
            $PremiumBloque->PremiumContenido = explode('|', $datosProducto->PremiumNombre);
            $Premium->PremiumBloque[] = $PremiumBloque;

            $Premium->Nombre = $datosProducto->PremiumNombre;
            $Premium->Presente = true;
            $Premium->Paquetizado = true;
        }

        $producto->Cable->Premium = $Premium;
    }

    /**
     *
     * @var \Producto[]
     */
    private static $cache = array(); 
    private static $productos; 

    /**
     *
     * @var \Producto[]
     */
    public static $premiumCache;
}