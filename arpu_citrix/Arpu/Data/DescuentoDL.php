<?php 
namespace Arpu\Data;
use Arpu\Config\Config;

class DescuentoDL
{
   /**
    * 
    * @return Descuento[]
    */
   public static function obtenerDescuentos($tipo)
   {
      if(!isset(self::$tabla[$tipo]))
      {
         throw new \Exception('No se puede obtener ese tipo de descuento');
      }
      
      if(!isset(DescuentoDL::$cache[$tipo]))
      {
         $tabla = self::$tabla[$tipo];
         $conexion = Config::ConexionBD();
         $obtenerDescuentos = $conexion->prepare("select * from $tabla");

         if(!$obtenerDescuentos->execute())
         {
            throw new \Exception('No se puede obtener la lista de Descuentos');
         }

         $descuentos = array();
         
         while (($producto = $obtenerDescuentos->fetchObject()))
         {
            self::Ajustar($producto);
            $descuentos[$producto->Ps] = $producto;
         }
         
         DescuentoDL::$cache[$tipo] = $descuentos;
         
      }
      return DescuentoDL::$cache[$tipo];
   }
   
   public static function Ajustar($datos)
   {
      if($datos->Ps!= null){ $datos->Ps+=0;}
      if($datos->Monto!= null){ $datos->Monto+=0;}
      if($datos->Meses!= null){ $datos->Meses+=0;}
      if($datos->Minimo!= null){ $datos->Minimo+=0;}
      if($datos->Maximo!= null){ $datos->Maximo+=0;}
   }
   
   public static function BuscarDescuento($Ps)
   {
       if(DescuentoDL::$cache['Regular'] == null)
       {
           DescuentoDL::obtenerDescuentos('Regular');
       }
       return DescuentoDL::$cache['Regular'][$Ps];
   }
   
   private static $tabla = array
           (
             'Regular' => 'Catalogo_Descuentos_',
             'Competencia' => 'catalogo_descuentos_competenc_'
           );
   

   private static $cache = array();
}
DescuentoDL::obtenerDescuentos('Regular');