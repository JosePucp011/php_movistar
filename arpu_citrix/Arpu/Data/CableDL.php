<?php
namespace Arpu\Data;

use Arpu\Entity\Cable;
use Arpu\Entity\Decodificador;


class CableDL
{
   public static function Construir($cable, $datos)
   {
      if($datos->Paquete_Categoria=='Duos TV+BA'){
         self::ConstruirDecos($cable, $datos->Tmp_Cable_DecodificadorAtis);
         $cable->Premium = PremiumDL::Construir($datos->Tmp_Premium_PsPaquete,$datos->Tmp_Premium_PsMono);
      }else{
         self::ConstruirDecos($cable, $datos->Linea_Presente ? $datos->Tmp_Cable_DecodificadorAtis : $datos->Tmp_Cable_DecodificadorCms);
         $cable->Premium = PremiumDL::Construir($datos->Tmp_Premium_PsPaquete,$datos->Tmp_Premium_PsMono);
      }
   }

   public static function ConstruirDecos($cable, $decos) {
        $cable->CantidadDecosSD = 0;
        $cable->Decos = array();

        $cable->Decos = $decos ? self::ObtenerDecos($decos) : array();
        foreach ($cable->Decos as $deco) {

            if ($deco->Categoria == 'HD') {
                ++$cable->TenenciaDecos['HD'];
            } elseif ($deco->Categoria == 'Digital') {
                ++$cable->CantidadDecosSD;
                ++$cable->TenenciaDecos['SD'];
            }

            if ($deco->Tipo == 'Sagem2') {
                ++$cable->TenenciaDecos['DVR'];
            }
            ++$cable->TiposDecos[$deco->Tipo];
        }
        $cable->CantidadDeDecos = count($cable->Decos);
        
        $nombre_deco = array();
        
        if($cable->TiposDecos['HD'] > 0){
            $numero = $cable->TiposDecos['HD'];
            array_push($nombre_deco, "$numero Deco HD");
        }
        
        if($cable->TiposDecos['Digital'] > 0){
            $numero = $cable->TiposDecos['Digital'];
            array_push($nombre_deco, "$numero Deco Digital");
        }
        
        if($cable->TiposDecos['DVR'] > 0){
            $numero = $cable->TiposDecos['DVR'];
            array_push($nombre_deco, "$numero Deco Grabador");
        }
        
        if($cable->TiposDecos['Smart'] > 0){
            $numero = $cable->TiposDecos['Smart'];
            array_push($nombre_deco, "$numero Deco Smart");
        }
        $cable->Deco = new \stdClass();
        
        $cable->Deco->Nombre = join('+', $nombre_deco);
        
        
    }

    /**
    * 
    * @param string $decos
    * @return Decodificador[]
    */
   private static function ObtenerDecos($decos)
   {
      $resultado = array();
      foreach (explode(';', $decos) as $ps)
      {
         if(isset(DecodificadorDL::$cache[$ps]))
         {
            $resultado[] = DecodificadorDL::$cache[$ps];  
         }
      }
      return $resultado;
   }

   /**
    * 
    * @param Cable $cable
    * @param Decodificador $decoCms
    * @param Decodificador[] $decosAtis
    * @return null
    */
   private static function AgregarDeco($cable,$decoCms, &$decosAtis)
   {
      foreach ($decosAtis as $indice => $decoAtis)
      {
         if ($decoAtis->Tipo == $decoCms->Tipo 
                 && $decoAtis->TipoAdquisicion == $decoCms->TipoAdquisicion)
         {
            unset($decosAtis[$indice]);
            $cable->Decos[] = $decoAtis;
            return;
         }
      }
      foreach ($decosAtis as $indice => $decoAtis)
      {
         if ($decoAtis->Tipo == $decoCms->Tipo)
         {
            unset($decosAtis[$indice]);
            $cable->Decos[] = $decoAtis;
            return;
         }
      }
      $cable->Decos[] = $decoCms;
      $cable->decosFaltantes[] = $decoCms;
   }

   
}