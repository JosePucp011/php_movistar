<?php

namespace Arpu\Data;

use Arpu\Entity\Reclamo;
use Arpu\Entity\Cliente;
use Arpu\Config\Config;

class ReclamoDL {
   

    public static function AgregarACliente(Cliente $cliente) {
        
        $ContadorReclamos = 0;
        if($query = self::EjecutarQuery($cliente->Telefono)) {
            while ($datos = $query->fetchObject()) {
                $reclamo = self::ConstruirReclamo($datos);
                $cliente->ListaReclamos[] = $reclamo;
                $ContadorReclamos += 1;
            }
        }
        
        self::MarcarReclamos($cliente, $ContadorReclamos);
    }
    /**
     * 
     * @param string $telefono
     * @return \PDOStatement
     * @throws ExceptionBasedatos
     */
    
    private static function MarcarReclamos(Cliente $cliente, $ContadorReclamos){
        
        if($cliente->Reclamos < $ContadorReclamos)
        {
            $cliente->Reclamos = $ContadorReclamos;
        }
    }
    private static function EjecutarQuery($telefono){
        try {
            $conexion = Config::ConexionBD();
            $query = $conexion->prepare(self::QUERY_PLANTA_RECLAMOS);
            $query->bindValue(1, $telefono);
            if (!$query->execute()) {
                throw new ExceptionBasedatos();
            }
            return $query;
        } catch(\Exception $exception) {
            return [];
        }
    }

    /**
     * 
     * @param \stdClass $datos
     * @return Mensaje
     */

    private static function ConstruirReclamo($datos) {
        
        $reclamo = new Reclamo();
        
        $reclamo->Cluster = $datos->Cluster;
        $reclamo->Tipo = $datos->Tipo;
        $reclamo->Fecha = $datos->Fecha;
        $reclamo->Motivo = $datos->Motivo;
        $reclamo->Importe = round($datos->Importe,2);
        $reclamo->LiquidacionFecha = $datos->LiquidacionFecha;
        $reclamo->LiquidacionImporte = round($datos->LiquidacionImporte,2);
        $reclamo->Estado = $datos->Estado;
        $reclamo->Respuesta = $datos->Respuesta;
        
        return $reclamo;
    }   
    const QUERY_PLANTA_RECLAMOS = 'select * from volcado_reclamos where Telefono = ?';
}
