<?php

namespace Arpu\Data;
use Arpu\Config\Config;
use Arpu\Entity\Componente;
use Arpu\Entity\ClienteNoEncontrado;
USE Arpu\Entity\Cliente;
use Arpu\Util\EjecutarQuery;

class ClienteDL
{
   const QUERY_DOCUMENTO_SINGLE = 'select * from Planta_Fija where Documento = ? order by Telefono asc limit 1';
   const QUERY_DOCUMENTO = 'select * from Planta_Fija where Documento = ? order by Telefono asc limit 15';
   const QUERY_TELEFONO = 'select * from Planta_Fija where Telefono = ?';
   
    
    
   public static function Leer($telefono, $documento)
   {
      if($documento != null)
      {
         $query = self::QUERY_DOCUMENTO_SINGLE;
         $valor = $documento;
      }
      else
      {
         $query = self::QUERY_TELEFONO;
         $valor = $telefono;
      }
	  
      $datos = self::CrearNuevosDatos($query,$valor);
      return self::ProcesarData($datos);
   }
   
   
   public static function LeerPorDocumento($documento){
        return EjecutarQuery::Ejecutar(
                self::QUERY_DOCUMENTO, 
                [$documento],
                function($a){  return ClienteDL::ProcesarData($a);});
    }
   
   
   
   private static function ProcesarData($datos){
      $cliente = ClienteDL::Construir($datos);
      $cliente->OfertaConvergente = 0;
      self::AplicarParches($cliente);
      self::AplicarParchesMovistarTotal($cliente,$datos);
      return $cliente;
   }
   
   
   private static function AplicarParchesMovistarTotal($nuevoFijo, $fijo) {
        $nuevoFijo->Presente = 1;
        $nuevoFijo->Numero = $fijo->Telefono;
        $nuevoFijo->Titulo = $fijo->Paquete_Categoria;
        self::ValidarNuevoCliente($nuevoFijo,$fijo);
        $nuevoFijo->Subtitulo = ucwords(strtolower($fijo->Nombre));
        $nuevoFijo->Direccion = "Direccion: " . strtoupper($fijo->Ubicacion_Direccion . "-" . $fijo->Ubicacion_Distrito . "-" . $fijo->Ubicacion_Provincia . "-" . $fijo->Ubicacion_Departamento);
        $nuevoFijo->EsHFC = $fijo->Internet_Tecnologia === 'HFC' || $fijo->Internet_Tecnologia === 'FTTH';
        $nuevoFijo->Descripcion = self::DescripcionFijo($fijo);

    }
    
	private static function ValidarNuevoCliente($nuevoFijo,$fijo){
       if($fijo->Linea_Presente && $fijo->Linea_Nombre === ''){
            $nuevoFijo->Valido = false;
        } elseif(strlen($fijo->Documento) > 9 ){
            $nuevoFijo->Valido = false;
        } else {
            $nuevoFijo->Valido = true;
        }
    }
    
    
    

    private static function DescripcionFijo($fijo){
        $componentes = [];

        $caracteristicas = '';
        if($fijo->Linea_Presente){
            $componentes[] = 'Fijo';
            $caracteristicas .= $fijo->Linea_Nombre.' ';
        }

        if($fijo->Internet_Presente){
            $componentes[] = 'Internet';
            $caracteristicas .= $fijo->Internet_Nombre.' ';
        }

        if($fijo->Cable_Presente){
            $componentes[] = 'Tv';
            $caracteristicas .= $fijo->Cable_Tipo.' ';
            $caracteristicas .= str_replace('EST', 'Estelar',str_replace('MC', 'FOX', $fijo->Premium_Canales));
        }

        if($fijo->Telefono < 0){
            $identificador = 'Codigo CMS';
            $codigo = -$fijo->Telefono;
        } else {
            $identificador = 'Telefono';
            $codigo = $fijo->Telefono;
        }

        $unirComponentes = join('+', $componentes);
        $rentaTotal = $fijo->Linea_RentaMonoproducto + $fijo->Internet_RentaMonoproducto 
                + $fijo->Cable_RentaMonoproducto + $fijo->Premium_RentaMonoproducto+ $fijo->Paquete_Renta;

        

        if($fijo->Linea_Presente && $fijo->Linea_Nombre === ''){
            return "$identificador : $codigo (Producto fijo no paquetizable)";
        } else {
            if($fijo->MovistarTotal) {
                return "$identificador : $codigo ($unirComponentes) $caracteristicas";
            } else {
                return "$identificador : $codigo ($unirComponentes) $caracteristicas S/$rentaTotal";
            }            
        }
    }
   
   /**
    * 
    * @param Cliente $cliente
    */
   public static function AplicarParches($cliente) {
        if ($cliente->Cable->Categoria == 'DTH' && $cliente->Internet->Cobertura->HFC) {
            $cliente->Cable->Cobertura = 8;
        }
        self::ModificarLinea($cliente->Linea);
        self::ModificarInternet($cliente->Internet);
        self::ModificarCable($cliente->Cable, $cliente->Premium);
        self::AgregarAdicionales($cliente);
   }
   public static function AgregarAdicionales($cliente) {
       
       $Adicionales = '';
       
       if($cliente->Linea->MantenimientoCantidad > 0){
           $Adicionales .= 'Mantenimiento ';
       }
       
       if($cliente->Internet->SeguridadTotalCantidad > 0){
           $Adicionales .= 'Seguridad_Total ';
       }
       
       if($cliente->Internet->SmartWifiCantidad > 0){
           $Adicionales .= 'Smart_Wifi ';
       }
       
       if($cliente->Cable->RentaDecodificadorEnAlquiler > 0){
           $Adicionales .= 'Alquiler_Puntos_Adicionales ';
       }
       
       if($cliente->Plan->RentaMonoproducto > 0){
           $Adicionales .= str_replace(" ","_",$cliente->Plan->Descripcion)." ";
       }  
       
       if($cliente->Trafico > 0){
           $Adicionales .= 'Trafico ';
       }
       
       if( ($cliente->EquiposFinanciados + $cliente->FinanciamientoDeuda) > 0){
           $Adicionales .= 'Financiamiento ';
       }
       
       $cliente->AdicionalesFijo = str_replace("_"," ",str_replace(" "," + ",trim($Adicionales)));
       
   }   
   
   public static function ModificarPremium($Premium) {
       
       $Premium = str_replace("MC","FOX",$Premium);
       $Premium = str_replace("GP","Golden Premier",$Premium);
       $Premium = str_replace("HP","Hot Pack",$Premium);
       $Premium = str_replace("EST","Estelar",$Premium);
       
       if($Premium != '' || $Premium != null)
       {
            return 'Bloque '.str_replace(","," + Bloque ",$Premium);
       }
       
       return $Premium;
   }
   
   public static function ModificarCable(\Arpu\Entity\Cable $Cable, \Arpu\Entity\Premium $Premium) {
    
       if($Premium->CanalesPaquete != '' || $Premium->CanalesPaquete != null){
           $Cable->Descripcion = $Cable->Descripcion.' - '. self::ModificarPremium($Premium->CanalesPaquete); 
       }
       
       if($Premium->CanalesMono != '' || $Premium->CanalesMono != null){
           $Premium->Descripcion = self::ModificarPremium($Premium->CanalesMono);
       }
       
   }
   
   public static function ModificarInternet(\Arpu\Entity\Internet $Internet) {
       
       if($Internet->Presente){ 
            if($Internet->Velocidad >= 1000){
                 $Internet->Descripcion = ($Internet->Velocidad/1000).' Megas';
            }else{
                $Internet->Descripcion = $Internet->Nombre;
            }
       }       
   }

   public static function ModificarLinea(\Arpu\Entity\Linea $linea) {
       
       if($linea->Presente){
            if($linea->Nombre === 'CAS'){
               $linea->Descripcion =  '250 min a fijos locales';
            }elseif($linea->Nombre  === 'L30'){
               $linea->Descripcion =  '30 min a fijos locales';
            }elseif($linea->Nombre  === 'LAS'){
               $linea->Descripcion =  'Linea libre al segundo';
            }elseif($linea->Nombre  === 'LS'){
               $linea->Descripcion =  '30 min a fijos locales';
            }elseif($linea->Nombre  === 'PRE'){
               $linea->Descripcion =  'Prepago';
            }elseif($linea->Nombre  === 'TPL'){
               $linea->Descripcion =  'Ilimitado a fijos locales Movistar';
            }elseif($linea->Nombre  === 'TPN'){
               $linea->Descripcion =  'Ilimitado a fijos locales y nacionales Movistar';
            }elseif($linea->Nombre  === 'TSP'){
               $linea->Descripcion =  '500 min a fijos locales';
            }
       }
   }   
   
    public static function Leer2($telefono, $documento, $modo)
   {
      if($documento != null)
      {
         $query = "select * from Planta_Fija a where \"Documento\" = ?";
         $valor = $documento;
      }
      else
      {
         $query = "select * from Planta_Fija a where \"Telefono\" = ?";
         $valor = $telefono;
      }
      
      
      $datos = self::CrearNuevosDatos2($query,$valor);
   }
   
   private static function CrearNuevosDatos($sql,$valor)
   {
      $conexion = Config::ConexionBD();
      $query = $conexion->prepare($sql);
      $query->bindValue(1, $valor);
      
      
      if(!$query->execute())
      {
         echo print_r($query->errorInfo());
         throw new ClienteNoEncontrado();
      }
      
      if ( ($datos = $query->fetchObject()) )
      {
         $query->closeCursor();
         return $datos;
      }
      else
      {
         $query->closeCursor();
         throw new ClienteNoEncontrado();
      }
   }
   
   public static function Llamar()
   {
      return "Here";
   }
   
   private static function CrearNuevosDatos2($sql,$valor)
   {
      $conexion = Config::ConexionBD();
      $query = $conexion->prepare($sql);
      $query->bindValue(1, $valor);
      
      if(!$query->execute())
      {
         throw new ClienteNoEncontrado();
      }
      
      if ( ($datos = $query->fetchObject()) )
      {
         $query->closeCursor();
         return $datos;
      }
      else
      {
         $query->closeCursor();
         throw new ClienteNoEncontrado();
      }
   }
   
   /**
    * 
    * @param stdclass $datos
    * @return \Cliente
    */
   
   public static function Construir($datos)
   {
      $cliente = new Cliente();
      EstructuraDL::Construir($cliente,$datos);

      $cliente->Paquete->DescuentoPermanente = preg_split(
              '/[,]/',$cliente->Paquete->DescuentoPermanentePs,
              -1,
              PREG_SPLIT_NO_EMPTY);
      
      CableDL::Construir($cliente->Cable, $datos);
      InternetDL::Construir($cliente->Internet);
      
      $cliente->Componentes = array(
          Componente::Linea => $cliente->Linea,
          Componente::Plan => $cliente->Plan,
          Componente::Internet => $cliente->Internet,
          Componente::Cable => $cliente->Cable,
      );
      
      $cliente->Paquete->Renta  = round($cliente->Paquete->Renta,2);
      $cliente->Linea->RentaMonoproducto  = round($cliente->Linea->RentaMonoproducto,2);
      $cliente->Plan->RentaMonoproducto  = round($cliente->Plan->RentaMonoproducto,2);
      $cliente->Internet->RentaMonoproducto  = round($cliente->Internet->RentaMonoproducto,2);
      $cliente->Cable->RentaMonoproducto  = round($cliente->Cable->RentaMonoproducto,2);
      $cliente->Cable->Premium->RentaMonoproducto  = round($cliente->Cable->Premium->RentaMonoproducto,2);
     
      if($cliente->Plan!=null && $cliente->Plan->Descripcion=="Multidestino"){
        $cliente->RentasAdicionales  = round($cliente->RentasAdicionales + $cliente->Plan->RentaMonoproducto,2); // bi 1
        $cliente->RentaTotal = round($cliente->Paquete->Renta 
              + $cliente->Linea->RentaMonoproducto              
              + $cliente->Internet->RentaMonoproducto 
              + $cliente->Cable->RentaMonoproducto 
              + $cliente->Cable->Premium->RentaMonoproducto,2);
      }else{
        $cliente->RentasAdicionales  = round($cliente->RentasAdicionales,2); // bi 2      
        $cliente->RentaTotal = round($cliente->Paquete->Renta 
              + $cliente->Linea->RentaMonoproducto 
              + $cliente->Plan->RentaMonoproducto  // bi 1
              + $cliente->Internet->RentaMonoproducto 
              + $cliente->Cable->RentaMonoproducto 
              + $cliente->Cable->Premium->RentaMonoproducto,2);
      }
      
      $cliente->Linea->RentaTotal = $cliente->RentaTotal;
      $cliente->Linea->RentaPrincipal = round($cliente->Paquete->Renta + $cliente->Linea->RentaMonoproducto  + $cliente->Internet->RentaMonoproducto + $cliente->Cable->RentaMonoproducto, 2);
      
      $cliente->Linea->Adicional =  round($cliente->Cable->Premium->RentaMonoproducto
                                            + $cliente->Linea->MantenimientoRenta
                                            + $cliente->Internet->SeguridadTotalRenta
                                            + $cliente->Internet->SmartWifiRenta
                                            + $cliente->Cable->RentaDecodificadorEnAlquiler
                                            + $cliente->Plan->RentaMonoproducto
                                            + $cliente->Trafico
                                            + $cliente->EquiposFinanciados 
                                            + $cliente->FinanciamientoDeuda ,2);           
              
      $cliente->RentaTotalConAdicionales = $cliente->RentaTotal + $cliente->Linea->Adicional;

      if(null != $cliente->AntiguedadEnMeses)
      {
          $tiempo = intval($cliente->AntiguedadEnMeses/12);
          if($tiempo>0)
          {$cliente->Antiguedad = $tiempo." a&ntildeos";}
          else
          {$cliente->Antiguedad = $cliente->AntiguedadEnMeses." Meses";}
      }      
      
        if($cliente->ReciboDigital == 1){
            $cliente->ReciboDigital = 'SI';}
        else {
            $cliente->ReciboDigital = 'NO';}
            
        if($cliente->ProteccionDatos == 1){
            $cliente->ProteccionDatos = 'SI';}
        else {
            $cliente->ProteccionDatos = 'NO';}

      $cliente->OfertaDeco = false;
      
      $cliente->Cable->CantidadDeBloques = 0;
      if(count($cliente->Cable->Premium->PremiumBloque) > 0)
      {
        $cliente->Cable->CantidadDeBloques = count($cliente->Cable->Premium->PremiumBloque[0]->PremiumContenido);
      }
      
      if($cliente->Telefono === 12766370)
            $cliente->PromocionHBO = 1;
      
      if($cliente->Internet->Presente)
      {
        if($cliente->Internet->Cobertura->HFC > 0){
              $cliente->PromocionHFC = 1;}

        if($cliente->Internet->Cobertura->HFC === 0){
              $cliente->PromocioADSL = 1;}      
      }
      
      if($cliente->Cable->Categoria === 'DTH' && $cliente->Internet->Cobertura->HFC > 0)
      {
          $cliente->Internet->Cobertura->MigracionDTH = 1;
      }
      
      if($cliente->Internet->Tecnologia === 'FTTH')
      {
          $cliente->Internet->Cobertura->FTTH = 1;
          $cliente->Internet->Cobertura->HFC = 2;
      }
      
      $cliente->Linea->Telefono = $cliente->Telefono;
      $cliente->Linea->Documento = $cliente->Documento;
      
      
      self::$cliente = $cliente;
      return $cliente;      
   }
   
   /**
    *
    * @var \Cliente
    */
   public static $cliente;
}