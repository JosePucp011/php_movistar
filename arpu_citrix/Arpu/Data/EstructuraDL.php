<?php


namespace Arpu\Data;

class EstructuraDL
{

   public static function Construir($objeto, $datos)
   {
      foreach ($datos as $Campo => $Valor)
      {
         if (strpos($Campo, 'Tmp_') === 0)
         {
            continue;
         }
         $ultimoContenedor = $objeto;
         
         while (strpos($Campo, '_') !== FALSE)
         {
            list($Contenedor, $CampoInterno) = preg_split('/_/', $Campo, 2);
            if (!isset($ultimoContenedor->{$Contenedor}))
            {
              $Clase = strpos($Contenedor, 'Movil') === 0 ? "Arpu\\Entity\\Movil" : $Clase = "Arpu\\Entity\\$Contenedor";
              $ultimoContenedor->{$Contenedor} = new $Clase;
            }
            $ultimoContenedor = $ultimoContenedor->{$Contenedor};
            $Campo = $CampoInterno;
         }
         

         $ultimoContenedor->{$Campo} = $Valor;
      }
   }
   
   
   public static function Digerir($objeto, $datos)
   {
      foreach ($datos as $Campo => $Valor)
      {
         $CampoBackup = $Campo;
         if (strpos($Campo, 'Tmp_') === 0)
         {
            continue;
         }
         $ultimoContenedor = $objeto;
         
         $base = '$cliente';
         while (strpos($Campo, '_') !== FALSE)
         {
            list($Contenedor, $CampoInterno) = preg_split('/_/', $Campo, 2);
            $base .= "->$Contenedor";
            if (!isset($ultimoContenedor->{$Contenedor}))
            {
              
              $Clase = 'Arpu\\Entity\\'.$Contenedor;
              $ultimoContenedor->{$Contenedor} = new $Clase;
              echo $base.' = new \\'.$Clase.";\r\n" ;
              
              
            }
            $ultimoContenedor = $ultimoContenedor->{$Contenedor};
            $Campo = $CampoInterno;
         }
         
         echo $base."->".$Campo.' = $datos->'.$CampoBackup.";\r\n";
         $ultimoContenedor->{$Campo} = $Valor;
      }
   }
   

   private static $camposNumericos = array(
   'RentasAdicionales',
   'Paquete_Ps',
   'Paquete_PsDescuento',
   'Paquete_Renta',
   'Identificador',
   'BloquePrioridad',
   'Ps',
   'Monto',
   'Meses',
   'Minimo',
   'Maximo',
   'Ps',
   'Monto',
   'Meses',
   'Minimo',
   'Maximo',
   'orden',
   'ps',
   'rango',
   'DescuentoPs',
   'Identificador',
   'Paquete_Renta',
   'Linea_Nivel',
   'Internet_Velocidad',
   'Internet_Cobertura_Iquitos',
   'Cable_Nivel',
   'Plan_Nivel',
   'Cable_EsHDTotal',
   'EsPaqueteParrilla',
   'Identificador',
   'Restriccion_Retenciones',
   'Paquete_Renta',
   'Linea_Paquetizado',
   'Linea_Presente',
   'Linea_Nivel',
   'Cable_Nivel',
   'Cable_Paquetizado',
   'Cable_Presente',
   'Cable_EsSVA',
   'PremiumRenta',
   'Identificador',
   'Paquete_Renta',
   'Paquete_EsSVA',
   'Internet_EsSVA',
   'Cable_Paquetizado',
   'Cable_Presente',
   'Cable_EsSVA',
   'Plan_Paquetizado',
   'Plan_Presente',
   'Plan_EsSVA',
   'Plan_Nivel',
   'Telefono',
   'Paquete_ProductoId',
   'Paquete_EsTarifaNueva',
   'Paquete_Ps',
   'Paquete_Renta',
   'Paquete_RentaAnterior',
   'Paquete_RentaConfigurada',
   'Paquete_RentaSinDescuentos',
   'Linea_Nivel',
   'Linea_Paquetizado',
   'Linea_Presente',
   'Linea_RentaMonoproducto',
   'Linea_Ps',
   'Linea_id',
   'Internet_Nivel',
   'Internet_Ps',
   'Internet_Paquetizado',
   'Internet_Presente',
   'Internet_RentaMonoproducto',
   'Internet_TienePAM',
   'Internet_Velocidad',
   'Internet_VelocidadAtis',
   'Internet_Cobertura_HFC',
   'Internet_Cobertura_Iquitos',
   'Internet_Cobertura_RadioEnlace',
   'Internet_Cobertura_VelocidadMaxima',
   'Internet_ModemPs',
   'Cable_Id',
   'Cable_Ps',
   'Cable_RentaMonoproducto',
   'Cable_ClaseServicio',
   'Cable_Cobertura',
   'Cable_Nivel',
   'Cable_Servicio',
   'Cable_Paquetizado',
   'Cable_Presente',
   'Cable_PuntosDigitales',
   'Cable_CantidadDecosHD',
   'Cable_TieneHD',
   'Tmp_Premium_PsPaquete',
   'IncrementoPrecio_EsIncremento',
   'IncrementoPrecio_TieneComunicacion',
   'IncrementoPrecio_Id',
   'Descuento_ComercialesCantidad',
   'Descuento_BloquesTVCantidad',
   'Descuento_VigentesCantidad',
   'Inscripcion',
   'Ciclo',
   'Cliente',
   'Cuenta',
   'ClienteCms',
   'EsEmpleado',
   'Plan_Nivel',
   'Plan_Paquetizado',
   'Plan_Presente',
   'Plan_RentaMonoproducto',
   'Ubicacion_id',
   'EsBuenPagador',
   'AntiguedadEnMeses',
   'Baja_Internet_APC',
   'Baja_Linea_APC',
   'Baja_Television_APC',
   'Baja_Internet_Deuda',
   'Baja_Linea_Deuda',
   'Baja_Television_Deuda',
   'RentaActual',
   'Facturacion',
   'RentaTotal',
   'Blindaje_EstaPosicionadoInternet',
   'Blindaje_EstaPosicionadoTelevision',
   'Blindaje_ZonaCompetenciaId',
   'Blindaje_AccionId',
   'Blindaje_RangoRentaId',
   'Blindaje_ClaroEntrante',
   'Blindaje_ClaroSaliente',
   'telefono',
   'ps',
   'Telefono',
   'Origen_Renta',
   'Destino_Renta',
   'Origen_Minutos',
   'Destino_Minutos',
   'Origen_Canales',
   'Destino_Canales',
   'Inestable',
   'ZonaCompetencia',
   'IncrementoPrecio',  
   'ComportamientoPagoCovergente',   
   'telefono',
   'Averias',
   'Reclamos',
   'Llamadas');

}
