<?php
namespace Arpu\Data;
use Arpu\Entity\Internet;


class InternetDL
{
   public static function Construir(Internet $internet)
   {  
      self::CoberturaFTTH($internet);
   }
   
   private static function CoberturaFTTH(Internet $internet)
   {
       if($internet->Cobertura->HFC === 2)
       {
           $internet->Cobertura->FTTH = 1;
       }
   }
}