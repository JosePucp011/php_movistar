<?php

namespace Arpu\Data;
use Arpu\Config\Config;
class EntregaDecos
{
   public static function Obtener()
   {
      $conexion = Config::ConexionBD();
      $obtenerProductos = $conexion->prepare('select * from catalogo_entregadecos_');

      if (!$obtenerProductos->execute())
      {
         throw new \Exception("No se puede obtener la lista de productos");
      }
      self::$reglas = array();

      while (($datos = $obtenerProductos->fetchObject()))
      {
         self::$reglas[$datos->Grupo] = new \Arpu\Entity\ReglaDeco($datos->Regla);
      }
   }
   
   
   
   
   /**
    *
    * @var \Arpu\Entity\ReglaDeco[]
    */
   public static $reglas;
}

EntregaDecos::Obtener();