<?php

namespace Arpu\Data;
use Arpu\Config\Config;

class FreeviewDL
{

   /**
    * 
    * @param string $telefono
    * @return Mensaje[]
    */
   public static function Obtener($telefono)
   {
      $Freeviews = array();

      try {
         $conexion = Config::ConexionBD();
         $obtenerMasiva = $conexion->prepare(
               'select * from Planta_Beneficios
                           where TELEFONO = ?');

         $obtenerMasiva->bindValue(1, $telefono);
         $obtenerMasiva->execute();
         
         while (($FV = $obtenerMasiva->fetchObject()))
         {
               $Freeview = new \stdClass();
               
               $Freeview->Tipo =$FV->TIPO;
               $Freeview->Procedencia =$FV->PROCEDENCIA_DEL_FREEVIEW;
               $Freeview->Aplica =$FV->APLICA_REGALO;
               $Freeview->Motivo =$FV->MOTIVO_DE_NO_APLICA;
               $Freeview->Nombre =$FV->NOMBRE_DE_FREEVIEW;
               $Freeview->RegistradoLanding =$FV->REGISTRADO_EN_LANDING;
               $Freeview->FechaRegistroLanding =$FV->FECHA_REGISTRO_LANDING;
               $Freeview->Activado =$FV->ACTIVADO;
               $Freeview->FechaInicio =$FV->FECHA_INICIO;
               $Freeview->FechaFin =$FV->FECHA_FIN;
               $Freeview->EstadoFV =$FV->ESTADO_ACT;
               
               $Freeviews[] = $Freeview;
         }
      } catch(\Exception $exception) {

      }
      
      return $Freeviews;
   }

   

}
