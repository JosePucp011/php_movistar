<?php

namespace Arpu\Data;
use Arpu\Config\Config;
use Arpu\Entity\Componente;
use Arpu\Exception\QueryNoEjecutado;
use Arpu\Util\EjecutarQuery;

class ClienteDLMovil {
    const QUERY_MOVIL_TELEFONO = "select * from Planta_Movil where movil = ? limit 15";
    const QUERY_MOVIL_DOCUMENTO = "select * from Planta_Movil where documento = ? order by (case when Movil0_Producto = 'Postpago' then 1 else 0 end) desc limit 15";
    
    public static function IdentificadorMovil($valor){
        $movil = self::LeerMovil($valor);
        
        if($movil->TipoServicio === 'Competencia'){
            return 'Celular no Movistar';
        } elseif($movil->Movil0->FuenteServicio === 'STC'){
            return 'Celular Movistar no apto para paquetizar';
        } else {
            return 'Celular Movistar encontrado';
        }
    }
    
    public static function LeerMovilesPorDocumento($documento){
        return EjecutarQuery::Ejecutar(
                self::QUERY_MOVIL_DOCUMENTO, 
                [$documento],
                function($a){return ConstruirClienteMovil::Construir($a);});
    }
    
    

    public static function LeerMovil($telefono,$posicion ='movil1') {
        return ConstruirClienteMovil::Construir(self::EjecutarQuery(self::QUERY_MOVIL_TELEFONO, $telefono),$posicion);
    }
    

    private static function EjecutarQuery($sql, $valor) {
        $conexion = Config::ConexionBD();
        $query =   $conexion->prepare($sql);
        $query->bindValue(1, $valor);
        if (!$query->execute()) {
            throw new QueryNoEjecutado();
        }

        if (($datos = $query->fetchObject())) {
            return $datos;
        } else {
            return ConstruirClienteCompetencia::Construir($valor);
        }
    }


    private static function CombinarClientes() {
        $primerCliente = self::IdentificarPrimercliente();
        self::CalcularIndiceMovil($primerCliente);
        self::AgregarClientesMoviles($primerCliente, self::$clienteMoviles);
        self::AgregarClientesMoviles($primerCliente, self::$clienteCompetencia);
        return $primerCliente;
    }
    
    private static function CalcularIndiceMovil($primerCliente){
        self::$indiceMovil = isset($primerCliente->Componentes[Componente::Movil . '0']) ? 1 : 0;
    }
    
    private static function AgregarClientesMoviles($primerCliente,$clientesMoviles){
        foreach ($clientesMoviles as $movil) {          
            $primerCliente->Componentes[Componente::Movil . self::$indiceMovil] = $movil->Componentes[Componente::Movil . '0'];
            ++self::$indiceMovil;
            $primerCliente->RentaTotal += $movil->RentaTotal;
            $primerCliente->RentaTotal = round($primerCliente->RentaTotal, 2);
            $primerCliente->RentaTotalConAdicionales += $movil->RentaTotal;
            $primerCliente->RentaTotalConAdicionales = round($primerCliente->RentaTotal, 2);
        }
    }
}
