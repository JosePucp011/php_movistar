<?php

namespace Arpu\Data;
use Arpu\Config\Config;
class DescuentoPermanenteDL
{
   public static function Obtener()
   {
      $conexion = Config::ConexionBD();
      $obtenerDescuentos = $conexion->prepare('select * from catalogo_descuentospermanent_');

      if (!$obtenerDescuentos->execute())
      {
         throw new \Exception("No se puede obtener la lista de productos");
      }
      self::$descuentos = array();

      while (($descuento = $obtenerDescuentos->fetchObject()))
      {
         self::$descuentos[$descuento->DescuentoPs] = $descuento;
      }
   }
  
   
   
   /**
    *
    * @var stdclass[]
    */
   public static $descuentos;
}

DescuentoPermanenteDL::Obtener();