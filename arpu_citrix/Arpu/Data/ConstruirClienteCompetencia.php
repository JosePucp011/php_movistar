<?php


namespace Arpu\Data;


class ConstruirClienteCompetencia {
    public static function Construir($movil){
        $datos = new \stdClass();
        $datos->TipoServicio = 'Competencia';
        $datos->Movil = $movil;
        $datos->Movil0_Movil = $movil;
        $datos->Movil0_Documento = "Competencia";
        $datos->Documento = "Competencia";
        $datos->TipoDocumento = "";
        $datos->Movil0_Nivel = 20000;
        $datos->Movil0_RentaTotal = 40;
        $datos->Movil0_RentaMonoproducto = 40;
        $datos->Movil0_Nombre = 'Plan Competencia S/40';
        $datos->Movil0_NombrePlan = 'Plan Competencia S/40';
        $datos->Movil0_Producto = 'Competencia';
        $datos->Movil0_FuenteServicio = 'AMD';
        $datos->Movil0_TipoLinea = 'No especificado';
        $datos->Movil0_Presente = 1;
        $datos->Movil0_Minutos = '-';
        $datos->Movil0_Datos = '-';
        $datos->Movil0_MovistarTotal = 0;
        $datos->MovistarTotal = 0;
        $datos->Movil0_Apps = 0;
        $datos->RentaTotal = 40;
        $datos->RentaTotalConAdicionales = 40;
        $datos->EsEmpleado = 0;
        return $datos;
    }
}
