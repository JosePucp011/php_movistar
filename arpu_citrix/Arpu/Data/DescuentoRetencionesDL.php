<?php
namespace Arpu\Data;
use Arpu\Config\Config;

class DescuentoRetencionesDL
{
    public static $cache;
    
    public static function Obtener()
    {
        $conexion = Config::ConexionBD();
        $Obtener = $conexion->prepare("select * from catalogo_descuentos_retencion where Tipo!='IncrementoPrecio' order by Orden desc");

        if (!$Obtener->execute())
        {
           throw new \Exception("No se puede obtener la lista de descuentos");
        }

        $Descuentos = array();

        while (($Dato = $Obtener->fetchObject()))
        {
           $Descuentos[] = $Dato;
        }
        DescuentoRetencionesDL::$cache = $Descuentos;
    }
    
    public static function Buscar($Tipo, $Valor, $TipoBlindaje,$IP)
    {

        $Result = Array();
        foreach(DescuentoRetencionesDL::$cache as $Descuento)
        {
            if($Descuento->Tipo === $Tipo && $Descuento->Descuento === $TipoBlindaje
                    && $Descuento->Minimo <= $Valor && $Descuento->Maximo >= $Valor )
            {
                $Result[] = $Descuento;
            }
            
        }          
        $reversed = array_reverse($Result);
        
        return $reversed;
    }
}

DescuentoRetencionesDL::Obtener();
