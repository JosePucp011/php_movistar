<?php

namespace Arpu\Data;

use Arpu\Entity\Componente;
use Arpu\Entity\Cliente;

class ConstruirClienteMovil {
    
    public static function Construir($datos,$posicion ='movil1') {
        $cliente = new Cliente();
        EstructuraDL::Construir($cliente, $datos);
        $cliente->Nombre = $datos->Movil0_Nombre;
        $cliente->Movil0->Posicion = $posicion;
        $cliente->Movil0->Documento = $datos->Documento;
        $cliente->Movil0->TipoDocumento = $datos->TipoDocumento;
        $cliente->Movil0->RentaTotal = $datos->Movil0_RentaTotal;
        $cliente->Movil0->MovistarTotal = $datos->MovistarTotal;
        $cliente->Internet->Cobertura = new \Arpu\Entity\Cobertura();
        $cliente->Cable->Cobertura = 8;
        $cliente->RentaTotalConAdicionales = $datos->Movil0_RentaTotal;
        $cliente->Componentes[Componente::Movil0] = $cliente->Movil0;
        $cliente->Numero = $datos->Movil;
        $cliente->Titulo = $datos->Movil0_Producto;
        $cliente->Subtitulo = ucwords(strtolower($datos->Movil0_Nombre));
        $cliente->Valido = $datos->Movil0_FuenteServicio === 'AMD';

        if ($datos->Movil0_FuenteServicio === 'STC') {
            $cliente->Descripcion = "Celular : $datos->Movil (Servicio en STC)";
        } else {
            $cliente->DescripcionMT = "Celular : $datos->Movil ($datos->Movil0_TipoLinea) $datos->Movil0_NombrePlan";
            $cliente->Descripcion = $cliente->DescripcionMT." S/ $datos->Movil0_RentaTotal";
        }
        
        return $cliente;
    }

}
