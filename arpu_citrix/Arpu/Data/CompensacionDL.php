<?php
namespace Arpu\Data;
use Arpu\Config\Config;

class CompensacionDL
{
    public static $cache;
    
    public static function Obtener()
    {
        $conexion = Config::ConexionBD();
        $Obtener = $conexion->prepare("select * from catalogo_herramientas_retencio order by Ps");

        if (!$Obtener->execute())
        {
           throw new \Exception("No se puede obtener la lista de descuentos");
        }

        $Compensaciones = array();

        while (($Dato = $Obtener->fetchObject()))
        { 
           $Compensaciones[] = $Dato;
        }
        CompensacionDL::$cache = $Compensaciones;
    }
    
    public static function Buscar($cliente)
    {
        $Result = Array();
        $Compensaciones = CompensacionDL::$cache;
        $RentaCliente = 0;

        if($cliente->Es_MonoLinea()){
            $RentaCliente= $cliente->Linea->RentaMonoproducto;
            return $Result;
        }else if($cliente->Paquete->ProductoId == 5){
            $RentaCliente= $cliente->Cable->RentaMonoproducto;
        }elseif($cliente->Paquete->ProductoId == 6 && $cliente->Telefono<0 ){
            $RentaCliente= $cliente->Internet->RentaMonoproducto;
        }else{ 
            $RentaCliente= $cliente->Paquete->Renta; 
            
        }        
        
        foreach($Compensaciones as $Compensacion)
        {
            if( $Compensacion->Minimo <= $RentaCliente 
                && $Compensacion->Maximo >= $RentaCliente                         
                )
            {                
                $Result[] =  $Compensacion;
            }
        }

        return $Result;
    }

}
CompensacionDL::Obtener();
