<?php
namespace Arpu\Data;
use Arpu\Config\Config;

class DescuentoRetencionesIncrementoDL
{
    public static $cache;
    
    public static function Obtener()
    {
        $conexion = Config::ConexionBD();
        $Obtener = $conexion->prepare("select * from catalogo_descuentos_retencion WHERE Tipo='IncrementoPrecio' And (Ps NOT IN (22160,22437))  order by Orden desc");
        /* $Obtener = $conexion->prepare("select * from catalogo_descuentos_retencion WHERE Tipo='IncrementoPrecio' order by Orden desc"); */

        if (!$Obtener->execute())
        {
           throw new \Exception("No se puede obtener la lista de descuentos");
        }

        $Descuentos = array();

        while (($Dato = $Obtener->fetchObject()))
        {
         
           $Descuentos[] = $Dato;
        }

       
        DescuentoRetencionesIncrementoDL::$cache = $Descuentos;
        
        
    }
    
    

    public static function Buscar($Tipo, $Valor, $TipoBlindaje,$IP)
    {
        
        $Result = Array();
        foreach(DescuentoRetencionesIncrementoDL::$cache as $Descuento)
        {
            
            if($IP === 1 && $Descuento->Tipo == $Tipo
                    && $Descuento->Minimo <= $Valor && $Descuento->Maximo >= $Valor)
            {
                $Result[] = $Descuento;
            }             
                       
        }          
        $reversed = array_reverse($Result); 
        
        return $reversed;
    }

    public static function BuscarMonoYNaked($Tipo, $Valor, $CableTipo,$IP,$descripcion)
    {
        
        $Result = Array();
        foreach(DescuentoRetencionesIncrementoDL::$cache as $Descuento)
        {
//             if($IP === 1 && $Descuento->Tipo === 'IncrementoPrecio'
//                    && $Descuento->Minimo <= $Valor && $Descuento->Maximo > $Valor && $Descuento->Descripcion==$descripcion)
            if($IP === 1 && $Descuento->Tipo === 'IncrementoPrecio'
                    && $Descuento->Minimo <= $Valor && $Descuento->Maximo >= $Valor 
                    && $Descuento->Descripcion==$descripcion
                    && $Descuento->Descuento==$CableTipo)
            {
                $Result[] = $Descuento;
            }             
                       
        }          
        $reversed = array_reverse($Result); 
        
        return $reversed;
    }

    public static function BuscarIncremento($Tipo, $Valor,$IP)
    {
        
        $Result = Array();
        foreach(DescuentoRetencionesIncrementoDL::$cache as $Descuento)
        {
            
            if($IP === 1 && $Descuento->Tipo === 'IncrementoPrecio'
                    && $Descuento->Minimo <= $Valor && $Descuento->Maximo >= $Valor && $Descuento->Descuento!='MonoTV')
            {
                $Result[] = $Descuento;
            }             
                       
        }          
        $reversed = array_reverse($Result);        
    
        return $reversed;
    }
}

DescuentoRetencionesIncrementoDL::Obtener();
