<?php


require_once(dirname(__FILE__).'/../Arpu/Autoload.php');


use Arpu\Config\Config;


class Equipo {
    public $Marca;
    public $Modelo;
    public $NombreComercial;
}

class Estrategia {
    public $TipoPago;
    public $TipoCliente;
    public $Modelo;
    public $Rango;
    public $Precio;
}




/**
 * Clase para procesar 
 *
 * @author dvictoriad
 */
class ProcesarCam {
    
    private $excel;
    private $workbook;
    /**
     *
     * @var Equipo[] 
     */
    private $equipos;
    /**
     *
     * @var Estrategia[]
     */
    private $estrategias;
    private $celdaBase;
    
    const UBICACION_MARCAS = 'Seleccion';
    const POSICION_NOMBRE_COMERCIAL = 2;
    const UBICACION_ESTRATEGIA = "POS";
    const NUMERO_RANGOS = 11;
    const COLUMNA_MODELO = 3;
    const LIMITE_BUSQUEDA = 10000;
    
    const TIPO_PAGO_CONTADO = 'Contado';
    const TIPO_PAGO_FINANCIADO = 'Financiado';
    
    const CLAVE_PAGO_CONTADO = 'SIN FINAN';
    const CARACTERES_TIPO_CLIENTE = 5;
    
    private function AbrirArchivo($archivo){
        $this->excel = new COM("Excel.Application");
        $this->workbook = $this->excel->Workbooks->Open($archivo);
    }
    
    
    public function ProcesarArchivo($archivo) {
        
        $this->equipos = [];
        $this->estrategias = [];
        $this->ValidarArchivo($archivo);
        $this->AbrirArchivo($archivo);
        $this->ObtenerEquipos();
        $this->ObtenerEstrategias();
        $this->CerrarArchivo();
        $this->InsertarEnMySQL();
    }
    
    private function InsertarEnMySQL(){
        $conexion = Config::ConexionBD();
        $conexion->exec('truncate table equipos');
        $conexion->exec('truncate table Estrategia');
        
        $queryEquipos = $conexion->prepare('insert into equipos (marca,modelo,nombrecomercial) values(?,?,?)');
        
        foreach($this->equipos as $equipo){
            $queryEquipos->bindValue(1,$equipo->Marca);
            $queryEquipos->bindValue(2,$equipo->Modelo);
            $queryEquipos->bindValue(3,$equipo->NombreComercial);
            $queryEquipos->execute();
        }
        
        $queryEstrategia = $conexion->prepare('insert into Estrategia '
                . '(TipoPago,TipoCliente,Modelo,Rango,Precio) values(?,?,?,?,?)');
        
        foreach($this->estrategias as $estrategia){
            $queryEstrategia->bindValue(1,$estrategia->TipoPago);
            $queryEstrategia->bindValue(2,$estrategia->TipoCliente);
            $queryEstrategia->bindValue(3,$estrategia->Modelo);
            $queryEstrategia->bindValue(4,$estrategia->Rango);
            $queryEstrategia->bindValue(5,$estrategia->Precio);
            $queryEstrategia->execute();
        }
    }
    
    
    
     private function ObtenerEquipos(){
        $this->celdaBase = $this->ObtenerCeldaBase();
        $rangoMarcas = $this->ObtenerRango($this->celdaBase->Validation->Formula1);
        $this->ProcesarMarcas($rangoMarcas);
        $this->LlenarNombreComercial();
    }
    
    
    private function ObtenerEstrategias(){
        foreach($this->workbook->Worksheets as $sheet){
            if(strpos($sheet->Name, self::UBICACION_ESTRATEGIA) === 0){
                echo $sheet->Name."\r\n";
                $this->ProcesarEstrategia($sheet);
            }
        }
    }
    
    private function ProcesarEstrategia($hojaEstrategia){
        $tipoCliente = str_replace(' ','',substr($hojaEstrategia->Name, 0,self::CARACTERES_TIPO_CLIENTE));
        $tipoPago = strpos($hojaEstrategia->Name, self::CLAVE_PAGO_CONTADO) === FALSE ? self::TIPO_PAGO_FINANCIADO : self::TIPO_PAGO_CONTADO;
        
        $i = 1;
        while($hojaEstrategia->Cells($i,self::COLUMNA_MODELO)->Value != 'Modelo' && $i < self::LIMITE_BUSQUEDA){
            ++$i;
        }
        ++$i;
        
        
        while($hojaEstrategia->Cells($i,self::COLUMNA_MODELO)->Value != '' && $i < self::LIMITE_BUSQUEDA){
            for($j = 1; $j <= self::NUMERO_RANGOS; ++$j){
                
                $estrategia = new Estrategia();
                $estrategia->Modelo = $hojaEstrategia->Cells($i,self::COLUMNA_MODELO)->Value;
                $estrategia->Rango = $j;
                $estrategia->TipoCliente = $tipoCliente;
                $estrategia->TipoPago = $tipoPago;
                $estrategia->Precio = $hojaEstrategia->Cells($i,self::COLUMNA_MODELO+$j)->Value;
                $this->estrategias[] = $estrategia;
            }
            ++$i;
        }
    }
    
    
    
    
    private function ObtenerCeldaBase(){
        foreach($this->workbook->Names() as $name ){
            if(strpos($name->Name, self::UBICACION_MARCAS) === 0){
                return $name->RefersToRange;
            }
         }
    }

    private function ObtenerRango($formula){
        $partes = preg_split('/!/', $formula);
        $hoja = str_replace("'",'',str_replace("=",'',$partes[0]));
        $rango = $partes[1];
        return $this->excel->Worksheets($hoja)->Range($rango);
        
    }
    
    private function ProcesarMarcas($rangoMarcas) {
        foreach ($rangoMarcas->Cells as $cell) {
            $marca = $cell->Value;
            $rangoEquipos = $this->workbook->Names($marca)->RefersToRange;
            $this->ObtenerEquiposRango($marca,$rangoEquipos);
        }
    }

    
    private function ObtenerEquiposRango($marca,$rangoEquipos){
        foreach ($rangoEquipos->Cells as $cell) {
           $equipo = new Equipo();
           $equipo->Marca = $marca;
           $equipo->Modelo = $cell->Value;
           $this->equipos[$equipo->Modelo] = $equipo;
        }
    }
    
    
    private function LlenarNombreComercial(){
        
        $buscarvNombreComercial = $this->celdaBase->Offset(self::POSICION_NOMBRE_COMERCIAL,0)->Formula;
        $rangoNombreComercial = $this->ObtenerRango(preg_split('/,/', $buscarvNombreComercial)[1]);
        
        $i = 1;
        while($rangoNombreComercial->Cells($i,1)->Value == "" && $i < self::LIMITE_BUSQUEDA){
            ++$i;
        }
        ++$i;

        while($rangoNombreComercial->Cells($i,1)->Value != "" && $i < self::LIMITE_BUSQUEDA){
            $modelo = $rangoNombreComercial->Cells($i,1)->Value;
            $nombreComercial = $rangoNombreComercial->Cells($i,2)->Value;
            $this->AgregarNombreComercial($modelo,$nombreComercial);
            ++$i;
        }
    }
    
    private function AgregarNombreComercial($modelo, $nombreComercial) {
        if (isset($this->equipos[$modelo])) {
            $equipo = $this->equipos[$modelo];
            $equipo->NombreComercial = $nombreComercial;
        }
    }

    private function CerrarArchivo(){
        $this->workbook->Close();
        $this->excel->Application->Quit();
    }
    
    
    private function ValidarArchivo($archivo){
        if (!file_exists($archivo)) {
            echo "El archivo $archivo no existe";
            exit(0);
        }
    }

}

if(isset($argv[1])){
    $procesarCam = new ProcesarCam();
    $procesarCam->ProcesarArchivo($argv[1]);
} else {
    echo 'No ha especificado archivo';
}


