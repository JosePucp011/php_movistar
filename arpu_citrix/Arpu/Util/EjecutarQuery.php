<?php

namespace Arpu\Util;
use Arpu\Config\Config;

class EjecutarQuery {
    public static function Ejecutar($sql, $variables,$operacion = null) {
        $conexion = Config::ConexionBD();
        $query = $conexion->prepare($sql);
        
        $indice = 1;
        foreach($variables as $valor){
            $query->bindValue($indice, $valor);
            ++$indice;
        }
        
        
        if (!$query->execute()) {
            throw new \Arpu\Exception\QueryNoEjecutado();
        }
        
        $resultado = [];
        while (($datos = $query->fetchObject())) {
            
            if($operacion === null){
                $resultado[] = $datos;
            } else {
                $resultado[] = $operacion($datos);
            }
        }
        
        return $resultado;
    }
}
