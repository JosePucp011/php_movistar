<?php

namespace Arpu\Services;
use Arpu\Logic\Modo\ModoRegular;
use Arpu\Logic\Modo\ModoAverias;
use Arpu\Logic\Modo\ModoOnline;
use Arpu\Logic\Modo\ModoMigracionDown;
use Arpu\Logic\Modo\ModoBajas;
use Arpu\Logic\Modo\ModoCuatroPlay;
use Arpu\Logic\Modo\ModoCuatroPlayOnline;
use Arpu\Logic\Modo\ModoApp;
use Arpu\Data\MensajeDL;
use Arpu\Data\ReasignacionDL;
use Arpu\Data\ReclamoDL;
use Arpu\Data\FreeviewDL;
use Arpu\Data\ClienteDL;
use Arpu\Entity\Cliente;
use Arpu\Data\PedidoDL;
use Arpu\Logic\HerramientaRetencionesBL;

class OfertaSugeridaApi
{
    public static $modo;
    public static $tipoDocumento;
    public static function Obtener($telefono,$tipoDocumento,$numeroDocumento,$modo)
    {
        self::$modo = $modo;
        self::$tipoDocumento = $tipoDocumento; // offer
        $cliente = ClienteDL::Leer($telefono,$numeroDocumento, $modo);
        MensajeDL::AgregarACliente($cliente);
        ReasignacionDL::AgregarACliente($cliente);
        ReclamoDL::AgregarACliente($cliente);
        PedidoDL::AgregarACliente($cliente);
        $cliente->Freeview = FreeviewDL::Obtener($cliente->Telefono);
        $cliente->Herramienta = HerramientaRetencionesBL::Obtener($cliente, $modo);
    
        return self::ObtenerParaCliente($cliente, $modo);
    }
    
    public static function ObtenerParaCliente(Cliente $cliente,$modo)
    {
        $ejecucion = self::crearModo($cliente,$modo);
        $ejecucion->ProcesarProductos();
        return $ejecucion->ObtenerResultadoConsulta();
    }
    
    /**
     * 
     * @param Cliente $cliente
     * @param string $modo
     * @return \Arpu\Logic\Modo\IModo
     */
    
    public static function crearModo($cliente, $modo) {
        if($modo == 'App') {
            return new ModoApp($cliente, $modo);
        } elseif ($modo == 'Baja') {
            return new ModoBajas($cliente, $modo);
        } elseif ($modo == 'MigracionDown') {
            return new ModoMigracionDown($cliente, $modo);
        } elseif ($modo == 'Averias') {
            return new ModoAverias($cliente,'Averia');
        } elseif ($modo == 'Online') {
            return new ModoOnline($cliente,'Online');
        } elseif ($modo == 'CuatroPlay') {
            return new ModoCuatroPlay($cliente,'4Play');
        } elseif ($modo == 'CuatroPlayOnline') {
            return new ModoCuatroPlayOnline($cliente,'4PlayOnline');
        } else {
            return new ModoRegular($cliente,'Regular');
        }
    }

}


