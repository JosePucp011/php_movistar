<?php

namespace Arpu\Http;


use stdClass;
use DateTime;
use DateInterval;
use Arpu\Autenticacion\Autenticacion;

class ModificarObjeto
{
    public static function TransformarResultado($resultado,$documento){
        $r = new \stdClass();

        $r->Resultado = $resultado;

        /* @var $cliente Arpu\Entity\Cliente */
        $cliente = $resultado->Cliente;

        $registro = new stdClass();


        $registro->Nombre = $cliente->Nombre;
        $registro->TipoDocumento = $cliente->TipoDocumento;
        $registro->Documento = $cliente->Documento;
        

        if($cliente->Linea->Presente){
            $registro->Telefono_Fijo = $cliente->Linea->Telefono;
        } else {
            $registro->Telefono_Fijo = 'Alta nueva';
        }

        $r->Cliente = new stdClass();

        $r->Cliente->Nombre = $cliente->Nombre;
        $r->Cliente->Cobertura = '-';   
        $r->Cliente->Documento = $cliente->Documento;
        $r->Cliente->TipoDocumento = $cliente->TipoDocumento;
        $r->Cliente->Correo = '-';

        $r->Cliente->PrincipalTotal = $cliente->Linea->RentaPrincipal;

        /*Tenencia Fija */
        $r->Cliente->Fijo = self::TransformarTenenciaFija($cliente, $cliente->Documento);

        /* Tenencia Moviles */
        $r->Cliente->Moviles = '-';
        $r->Cliente->CicloMovil = '-';

        $registro->Telefono_Movil_1 = "No tiene Movil #1";
        $registro->Telefono_Movil_2 = "No tiene Movil #2";
        $r->Cliente->Movil1 = self::TransformarTenenciaMovil(null,$cliente->Documento);
        $r->Cliente->Movil2 = self::TransformarTenenciaMovil(null,$cliente->Documento);

        if(isset($cliente->Componentes['Movil0'])){
            $registro->Telefono_Movil_1 = $cliente->Componentes['Movil0']->Movil;
            $r->Cliente->Moviles = $cliente->Componentes['Movil0']->Movil;
            $r->Cliente->PrincipalTotal += $cliente->Componentes['Movil0']->RentaTotal;
            $r->Cliente->Movil1 = self::TransformarTenenciaMovil($cliente->Componentes['Movil0'],$cliente->Documento);
            $r->Cliente->CicloMovil = $cliente->Ciclo;
			
			if($cliente->Ciclo == 28){
                $r->Cliente->CicloMovil =  '01';
            }
			
        }

        if(isset($cliente->Componentes['Movil1'])){
            $registro->Telefono_Movil_2 = $cliente->Componentes['Movil1']->Movil;
            $r->Cliente->Moviles = $r->Cliente->Moviles.' - '.$cliente->Componentes['Movil1']->Movil;
            $r->Cliente->PrincipalTotal += $cliente->Componentes['Movil1']->RentaTotal;
            $r->Cliente->Movil2 = self::TransformarTenenciaMovil($cliente->Componentes['Movil1'],$cliente->Documento);
            
			if($cliente->Ciclo == 28){
                $r->Cliente->CicloMovil .= ' - 01';
            }else{
                $r->Cliente->CicloMovil .= ' - '.$cliente->Ciclo;
            }
        }

        /* Arreglos */
        $r->Cliente->Scoring = $cliente->Scoring;
        $r->Cliente->Pedido = $cliente->Pedido;

        $r->Cliente->PrincipalTotal = round($r->Cliente->PrincipalTotal,2);
        $r->Cliente->RentaAdicional = round($cliente->Linea->Adicional,2);
        $r->Cliente->RentaTotal = round($r->Cliente->PrincipalTotal + $cliente->Linea->Adicional,0); //PARCHE APP

        $r->Cliente->FinanciamientoMovil = 0;

        /* Ofertas*/

        if(isset($resultado->Opciones[1])){ 
            $r->Oferta1 = self::TransformarOferta($resultado->Opciones[1], $cliente,1);
        }else{
            $r->Oferta1 = self::TransformarOfertaVacia();
        }
        
        

        if(isset($resultado->Opciones[2])){ 
            $r->Oferta2 = self::TransformarOferta($resultado->Opciones[2], $cliente,2);
        }else{
            $r->Oferta2 = self::TransformarOfertaVacia();
        }
        
        if(isset($resultado->Opciones[3])){ 
            $r->Oferta3 = self::TransformarOferta($resultado->Opciones[3], $cliente,3);
        }else{
            $r->Oferta3 = self::TransformarOfertaVacia();
        }
      
        $r->Oferta4 = self::TransformarOfertaVacia();
      
        $r->Oferta1->Prioridad = 1; 
        $r->Oferta2->Prioridad = 2;
        $r->Oferta3->Prioridad = 3;
        $r->Oferta4->Prioridad = 4;
          
        
        foreach($resultado->Ofertas as $oferta){
            $oferta->DataDisplay = self::TransformarOferta($oferta, $cliente, 4);
        }

        $r->Cliente = self::ModificarMultitularidad($r->Cliente);
        $r->Usuario = Autenticacion::ObtenerUsuario();
        
        return $r;
    }
    
    
    
    private static function TitularFijo($Cliente,$documento){
        if(!$Cliente->Linea->Presente){
            return 'No tiene trio';
        } else {
            
            if($Cliente->MovistarTotal){
               return 'Movistar Total'; 
            }
            return ($Cliente->Linea->Documento == $documento ? 'Mismo titular' : 'Requiere cambio titularidad');
        }
    }
    
    private static function TitularMovil($movil,$documento){ 
        if($movil->Documento == "Competencia"){
            return 'Portabilidad';
        } else if($movil->Documento == $documento) {
            
            if($movil->MovistarTotal){
                return 'Movistar Total';
            }else{
                return 'Mismo titular';   
            }
        } else {
            return 'Requiere Cambio de Titularidad';
        }
    }
    
    private static function TransformarVacio($texto){
        if($texto === '' || $texto === null || $texto === 0 || $texto === ""){ 
            return '-';
        }

        return $texto;
    }
    private static function TransformarAPPS($Apps)
    {
        if($Apps < 6){ 
           return '-';}
           
        if($Apps == 6)
        {
            return '6 - Facebook, Instagram, Messenger, WhatsApp, Spotify y Waze';
        }
        if($Apps == 7)
        {
            return '7 - Facebook, Instagram, Messenger, WhatsApp, Spotify, Waze y M. Play';
        }            
    }

    private static function TransformarInternetTecnologia($Tecnologia,$Cliente)
    {
        if($Cliente->Internet->Tecnologia === 'FTTH' || $Cliente->Internet->Cobertura->FTTH)
        {
            return 'FTTH';
        }
        
        return $Tecnologia;
        
    }

    
    private static function TransformarCategoria($Categoria,$Tecnologia = null)
    {
        $Formato = $Categoria; 
        
        if($Categoria === 'Duos CATV' || $Categoria === 'Duos DTH' )
        {
            $Formato = 'Duo TV';
        }
        if($Categoria === 'Trios CATV' || $Categoria === 'Trios DTH' )
        {
            $Formato = 'Trio '.$Tecnologia;
        }
        if($Categoria === 'Duos BA' )
        {
            $Formato = 'Duo '.$Tecnologia;
        }

        if($Categoria === null )
        {
            $Formato = '-';
        }
        return $Formato;
    }
    private static function TransformarOfertaMovil($Movil,$orden = null,$GVS = null,$ordenMovil = null)
    {
        $OfertaMovil = new stdClass();
        
        if(isset($Movil)){
            
            $OfertaMovil->Minutos = 'Ilimitado a cualquier operador';
            $OfertaMovil->Datos = $Movil->Nombre;
            $OfertaMovil->Roaming = $Movil->Roaming;
            $OfertaMovil->DatosInternacionales = $Movil->DatosInternacionales;
            $OfertaMovil->Apps = $Movil->Apps;
            $OfertaMovil->Renta = $Movil->Renta;
            $OfertaMovil->OperacionComercial = 'CAPL';
            $OfertaMovil->Equipo = '-';
            $OfertaMovil->DatosInternacionales = $Movil->DatosInternacionales;// agre
            $OfertaMovil->WhatsappIlimitado = "Solo texto";// agre
            $OfertaMovil->Pasagigas = "100% de GB";// agre

            if($orden === 4){
                $OfertaMovil->Equipamiento = "<div id=\"Equipo_Texto$orden$ordenMovil\"  onclick=\"Equipo.Mostrar(window.respuesta.Cliente,window.respuesta.Oferta$orden,'$GVS','$orden','$ordenMovil');\" class='EquipamientoAdicional' >Seleccionar</div>";
            }else{
                $OfertaMovil->Equipamiento = "<div id=\"Equipo_Texto$orden$ordenMovil\"  onclick=\"Equipo.Mostrar(window.respuesta.Cliente,window.respuesta.Oferta$orden,'$GVS','$orden','$ordenMovil');\" class='Equipamiento' >Seleccionar</div>";
            }
            
            $OfertaMovil->AppsDescripcion = self::TransformarAPPS($Movil->Apps);
            $OfertaMovil->Financiamiento = 0;
                    
        }else{
            $OfertaMovil->Minutos = "-";
            $OfertaMovil->Datos = "-";
            $OfertaMovil->Roaming = "-";
            $OfertaMovil->Apps = "-";
            $OfertaMovil->OperacionComercial = '-';
            $OfertaMovil->Equipamiento = '-';
            $OfertaMovil->AppsDescripcion = '-';
            $OfertaMovil->Equipo = '-';
            $OfertaMovil->Financiamiento = 0;
            $OfertaMovil->Renta = '-';
            $OfertaMovil->DatosInternacionales = "-";// agre
            $OfertaMovil->WhatsappIlimitado = "-";// agre
            $OfertaMovil->Pasagigas = "-";// agre
        }
        return $OfertaMovil;
    }
    
    private static function TransformarOfertaFijo($producto = null,$cliente = null,$tipoOperacionComercial = null)
    {
        $OfertaFijo = new stdClass();

        if(isset($producto)){
            $OfertaFijo->Linea = $producto->Linea->Descripcion;
            $OfertaFijo->Velocidad = $producto->Internet->Nombre;
            $OfertaFijo->Internet = self::TransformarInternetTecnologia($producto->Internet->Tecnologia,$cliente);
            $OfertaFijo->Modem =  $producto->Internet->Modem;
            $OfertaFijo->Television = $producto->Cable->Descripcion;
            $OfertaFijo->Decodificador = $producto->DecosNombre;
            if($OfertaFijo->Decodificador === ""){$OfertaFijo->Decodificador="-";}
			$OfertaFijo->Bloque ="-"; // $producto->Canales;
            $OfertaFijo->Renta = $producto->Paquete->RentaFija;
            $OfertaFijo->Adicional = self::TransformarVacio($cliente->AdicionalesFijo);
            $OfertaFijo->OperacionComercial = $tipoOperacionComercial;
        }
        else{
            $OfertaFijo->Linea = '-';
            $OfertaFijo->Velocidad = '-';
            $OfertaFijo->Internet = '-';
            $OfertaFijo->Modem =  '-';
            $OfertaFijo->Television = '-';
            $OfertaFijo->Decodificador = '-';
            $OfertaFijo->Bloque = '-';
            $OfertaFijo->Renta = '-';
            $OfertaFijo->Adicional = '-';
            $OfertaFijo->OperacionComercial = '-';  
        }
        return $OfertaFijo;
    }
    
    
    /**
     * 
     * @param type $cliente
     * @param type $componente
     * @return string Alta, Porta Generica, Porta Especial, POS1,POS2,POS3
     */
    private static function ObtenerGVS($cliente,$componente){
        
        
        if(!isset($cliente->Componentes[$componente])){
            return 'ALTA';
        }
        
        $movil = $cliente->Componentes[$componente];
        
        if($movil->Producto === 'Competencia' && $movil->MayorIgual8Meses){
            return 'PORTA_ESPECIAL'  ;
        } else if($movil->Producto === 'Competencia'){
          return 'PORTA_GENERICA';
        } else {
            $gvs = $movil->GVS;
            $validos = ['POS1','POS2','POS3'];
            
            if(in_array($gvs, $validos)){
                return $gvs;
            }
            
            $prepago = ['PRE1','PRE2','PRE3'];
            
            if(in_array($gvs, $prepago)){
                return 'POS2';
            }
            return 'POS1';
        }
    }
    
    private static function TransformarOferta($oferta, $cliente,$orden)
    {
        $OfertaCliente = new stdClass();
        /* @var $producto Arpu\Entity\Producto */
        $producto = $oferta->producto;
        $Usuario = Autenticacion::ObtenerUsuario();

        /* Fija */
        $OfertaCliente->Fijo   = self::TransformarOfertaFijo($producto,$cliente,$oferta->tipoOperacionComercial);
       
        /* Movil */
        $GVS1 = self::ObtenerGVS($cliente, \Arpu\Entity\Componente::Movil0);
        $GVS2 = self::ObtenerGVS($cliente, \Arpu\Entity\Componente::Movil1);
        
        if(isset($oferta->Solo1Movil))
        {
            $OfertaCliente->Movil1 = self::TransformarOfertaMovil($producto->MovilTotal,$orden,$GVS1,1);
            $OfertaCliente->Movil2 = self::TransformarOfertaMovil(null);  
        }
        else{
            
            $OfertaCliente->Movil1 = self::TransformarOfertaMovil($producto->Movil0,$orden,$GVS1,1);
            $OfertaCliente->Movil2 = self::TransformarOfertaMovil($producto->Movil1,$orden,$GVS2,2);
        }
        /* Oferta */
        $OfertaCliente->RentaSinAdicionales = round($producto->Paquete->RentaSinModificar,2);
        $OfertaCliente->Adicionales = self::TransformarAdicional($cliente,$producto);
        $OfertaCliente->RentaTotal = round( $OfertaCliente->RentaSinAdicionales + $OfertaCliente->Adicionales ,2 );

        $OfertaCliente->PrecioRegular = $oferta->cargo_fijo_destino + ($producto->PrecioRegular - $producto->Paquete->Renta);

        $OfertaCliente->Salto = round($oferta->salto_arpu,2);
        $OfertaCliente->SaltoPaquete = round($oferta->salto_arpu,2);

        $OfertaCliente->scoringFijoCodigo = $cliente->Scoring['scoringFijoCodigo'];
        $OfertaCliente->scoringMovilCodigo = $cliente->Scoring['scoringMovilCodigo'];

        $OfertaCliente->Nombre = $producto->Paquete->NombreOrigen;
        $OfertaCliente->Rango = $producto->Paquete->Rango;
        $OfertaCliente->IdPlan = $producto->Paquete->CodigoMovil;
        $OfertaCliente->Plan = $producto->Paquete->CodigoMovil.' - '.$producto->Paquete->NombreMovil;
        if($OfertaCliente->Fijo->Decodificador == '' || $OfertaCliente->Fijo->Decodificador == '-') {
            $OfertaCliente->Equipamiento = $OfertaCliente->Fijo->Modem;
        } else {
            $OfertaCliente->Equipamiento = $OfertaCliente->Fijo->Decodificador.'+'.$OfertaCliente->Fijo->Modem;
        }
        $OfertaCliente->MovilTotal_Renta = $producto->MovilTotal->Renta;
        // Dias movistar
        $ActivarDiasMovistar=true;
        $fecha_actual = date('n');//strtotime(date("d-m-Y",time()));
        $fecha_entrada = strtotime("31-10-2019");
        if($fecha_actual == 12 ){
            $ActivarDiasMovistar=false;               
        }else{
            $ActivarDiasMovistar=true;             
        }
        $ActivarDiasMovistar=true;
        $OfertaCliente->DiasMovistar="";
        if($ActivarDiasMovistar==true /* && $OfertaCliente->RentaSinAdicionales>199 */ && $OfertaCliente->Salto >=0){ 
            $OfertaCliente->DiasMovistar="S/. 20 x 3 Meses" ;// $OfertaCliente->Salto;//"S/.20 x 3 Meses";            
        }
        $OfertaCliente->FinanciamientoMovil = 0;
        $OfertaCliente->Guardar = "<div onclick=\"Pedido.MostrarFormulario(window.respuesta.Oferta$orden);\" class= 'btnGuardar'>Guardar</div>";
        $OfertaCliente->Registro = "<div onclick='Identificadores.MostrarInformacionRegistro(window.respuesta.Cliente,window.respuesta.Oferta$orden);' class='btnGuardar'>Registro</div>";
        
        return $OfertaCliente;

    }
    
    private static function TransformarAdicional(\Arpu\Entity\Cliente $cliente, \Arpu\Entity\Producto $producto){
        
        $Adicional = round($cliente->Linea->Adicional,2);

        if($producto->Paquete->Ps != 23240){
            $Adicional = round($cliente->Linea->Adicional - $cliente->RentaHD ,2);
        }
        if($cliente->Cable->Tipo == 'Estelar'){
            $Adicional += 20;
        }
        
        return round($Adicional,2);
        
    }

    
    private static function TransformarOfertaVacia()
    {
        $OfertaCliente = new stdClass();
        /* Fija */

        $OfertaCliente->Fijo   = self::TransformarOfertaFijo();

        /* Movil */
        $OfertaCliente->Movil1 = self::TransformarOfertaMovil(null);
        $OfertaCliente->Movil2 = self::TransformarOfertaMovil(null);

        /* Oferta */
        $OfertaCliente->RentaSinAdicionales = '-';
        $OfertaCliente->Adicionales = '-';
        $OfertaCliente->RentaTotal = '-';

        $OfertaCliente->PrecioRegular = '-';

        $OfertaCliente->Salto = '-';

        $OfertaCliente->scoringFijoCodigo = '-';
        $OfertaCliente->scoringMovilCodigo = '-';

        $OfertaCliente->Nombre = '-';
        $OfertaCliente->IdPlan = '-';
        $OfertaCliente->Plan = '-';
        $OfertaCliente->Equipamiento = '-';
        $OfertaCliente->FinanciamientoMovil = '-';
        $OfertaCliente->Guardar = '';
        $OfertaCliente->Registro = '';
        
        return $OfertaCliente;

    }
    
    private static function TransformarTenenciaMovil($Movil,$documento){
    
        $TenenciaMovil = new stdClass();

        if(isset($Movil)){
            $TenenciaMovil->Titular = self::TitularMovil($Movil, $documento);
            $TenenciaMovil->TitularLanding = self::TitularMovil($Movil, $documento);
            $TenenciaMovil->Renta = $Movil->RentaTotal;
            $TenenciaMovil->Roaming = $Movil->Roaming;
            $TenenciaMovil->Minutos = $Movil->Minutos;
            $TenenciaMovil->DatosInternacionales = "-";// agre
            $TenenciaMovil->WhatsappIlimitado = "-";// agre
            $TenenciaMovil->Pasagigas = "-";// agre
            $TenenciaMovil->Datos = $Movil->Datos;
            if($Movil->App == 0){
                $TenenciaMovil->Apps = '-';
            }else{
                $TenenciaMovil->Apps = $Movil->App;
            }
            $TenenciaMovil->AppsDescripcion = self::TransformarAPPS($Movil->App);
            $TenenciaMovil->Equipamiento = '-';
            $TenenciaMovil->Categoria = $Movil->Producto;
            $TenenciaMovil->Numero = $Movil->Movil;
            $TenenciaMovil->Nombre = $Movil->Nombre;
            $TenenciaMovil->MovistarTotal = $Movil->MovistarTotal;
            $TenenciaMovil->IdPlan = $Movil->IdPlan;
            
        } else {
            $TenenciaMovil->Titular = 'No tiene Movil';
            $TenenciaMovil->TitularLanding = 'No tiene Movil';
            $TenenciaMovil->RentaTotal = "-";
            $TenenciaMovil->Renta = "-";
            $TenenciaMovil->Roaming = "-";
            $TenenciaMovil->Minutos = "-";
            $TenenciaMovil->DatosInternacionales = "-";// agre
            $TenenciaMovil->WhatsappIlimitado = "-";// agre
            $TenenciaMovil->Pasagigas = "-";// agre
            $TenenciaMovil->Datos = "-";
            $TenenciaMovil->Apps = "-";
            $TenenciaMovil->AppsDescripcion = '-';
            $TenenciaMovil->Categoria = '-';
            $TenenciaMovil->Numero = '-';
            $TenenciaMovil->Nombre = '-';
            $TenenciaMovil->MovistarTotal = 0;
            $TenenciaMovil->Equipamiento = '-';
            $TenenciaMovil->IdPlan = '-';
        }
        return $TenenciaMovil;
    }

    private static function ObtenerCicloFacturacion(){
        
        $fecha = getdate();
        
        $FechaActual = new DateTime($fecha['year'].'-'.$fecha['mon'].'-'.$fecha['mday']);
        $FechaActual->add(new DateInterval('P4D'));        
        
        if($FechaActual->format('d') >= 2 && $FechaActual->format('d') <= 18 )
        {
            $ciclo = '18';
        }
        else
        {
            $ciclo = '01';
        }
              
        return $ciclo;
    }
    
    private static function TransformarTenenciaFija($cliente,$documento){
    
        $Fijo = new stdClass();

        if($cliente->Telefono===0 || $cliente->Telefono === null)
        {
            $cliente->Ciclo = self::ObtenerCicloFacturacion();
            $Fijo->Numero   = '-';
            $Fijo->Ciclo    = $cliente->Ciclo;
        } 
        else 
        {
           $Fijo->Numero = $cliente->Telefono;
           $Fijo->Ciclo =  $cliente->Ciclo;
		   
		   if($cliente->Ciclo == 28){
                $Fijo->Ciclo =  '01';
           }
		   
        }

        $Fijo->MovistarTotal = $cliente->MovistarTotal;
        
        $Titular = self::TitularFijo($cliente, $documento);
        
        $Fijo->Titular = $Titular;
        $Fijo->TitularLanding = self::TitularFijo($cliente, $documento);
        $Fijo->Nombre = $cliente->Nombre;
        $Fijo->Linea = self::TransformarVacio($cliente->Linea->Descripcion);
        $Fijo->Velocidad = self::TransformarVacio($cliente->Internet->Descripcion);
        $Fijo->Internet = self::TransformarVacio($cliente->Internet->Tecnologia);
        $Fijo->Modem = self::TransformarVacio($cliente->Internet->ModemDescripcion);
        $Fijo->Television = self::TransformarVacio($cliente->Cable->Descripcion);
        $Fijo->Decodificador = self::TransformarVacio($cliente->Cable->Deco->Nombre);
        $Fijo->Categoria = self::TransformarCategoria($cliente->Paquete->Categoria,$cliente->Internet->Tecnologia);
        
        if($Fijo->Decodificador === ""){$Fijo->Decodificador="-";}
		
        $Fijo->Bloque = self::TransformarVacio($cliente->Premium->Descripcion);
        $Fijo->RentaTotal = self::TransformarVacio(round($cliente->Linea->RentaTotal,2));
        $Fijo->Renta = self::TransformarVacio($cliente->Linea->RentaPrincipal);
        $Fijo->Adicional = self::TransformarVacio($cliente->AdicionalesFijo);

        $Fijo->Cobertura = "HFC (".$cliente->Ubicacion->Coordenada.")";

        $Fijo->Direccion = $cliente->Ubicacion->Direccion;
        $Fijo->Distrito = $cliente->Ubicacion->Distrito;
        $Fijo->Provincia = $cliente->Ubicacion->Provincia;
        $Fijo->Departamento = $cliente->Ubicacion->Departamento;
        $Fijo->Ubicacion = $cliente->Ubicacion->Distrito.' / '.$cliente->Ubicacion->Provincia.' / '.$cliente->Ubicacion->Departamento;    
        return $Fijo;
    
    }
    
    private static function ModificarMultitularidad($Cliente){
        
        if($Cliente->Fijo->TitularLanding != 'No tiene trio'){
            
            $Cliente->Fijo->TitularLanding = ucwords(strtolower(strtok($Cliente->Fijo->Nombre," ")));
        }
        if($Cliente->Movil1->TitularLanding != 'No tiene Movil'){
                $Cliente->Movil1->TitularLanding = ucwords(strtolower(strtok($Cliente->Movil1->Nombre," ")));
        }
        if($Cliente->Movil2->TitularLanding != 'No tiene Movil'){
                $Cliente->Movil2->TitularLanding = ucwords(strtolower(strtok($Cliente->Movil2->Nombre," ")));
        }
        
        return $Cliente;
    }
}