<?php

namespace Arpu\Http;


use Arpu\Http\LectorParametros;
use Arpu\Data\IdentificadorDL;


class ConsultarDocumento_Borrar {
    public function Procesar(){
        try {
            echo json_encode($this->Consultar());
        } catch (Exception $ex) {
            $ex = null;
            $this->RespuestaVacia();        
        }
    }
    
    public function EjecutarObjeto($input){
        $this->LeerParametrosObjeto($input);
        return $this->Consultar();
    }
    
    private function SetearGet($objeto,$campo){
        if(isset($objeto->BodyIn->$campo)){
            $_GET[$campo] = $objeto->BodyIn->$campo;
        }
    }
    
    private function LeerParametrosObjeto($objeto){
        if(!is_object($objeto) ||  !isset($objeto->BodyIn)){
            return;
        }
        $this->SetearGet($objeto,'documento');
    }
    
    
    private function Consultar(){
        $documento = LectorParametros::LeerDocumento();
        return $this->ProcesarDocumento($documento);
    }
    
    private function ProcesarDocumento($documento){
        if($documento){
            return $this->LeerInformacion($documento);
        } else {
            throw new \InvalidArgumentException();
        }
    }
    
    private function LeerInformacion($telefono){
        $resultado = IdentificadorDL::ConsultarDocumento($telefono);
        \Arpu\Util\Encoder::utf8_encode_deep($resultado);		
        return $resultado;
    }
    
    private function RespuestaVacia(){
        echo json_encode((object)array('error' => 'Documento no encontrado'));
    }
}