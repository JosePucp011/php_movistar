<?php

namespace Arpu\Http;

use InvalidArgumentException;
use Arpu\Logic\Modo\IModo;

class Identificador {
    public $telefono;
    public $documento;
}
class IdentificadorPhone { // offer
    public $phone;
}
class IdentificadorDocumentNumber{ // offer
    public $documentNumber;
}
class LectorParametros {

    public static function LeerTipoDocumento($opcional=false) {
        $tipoDocumento = filter_input(INPUT_GET, 'tipoDocumento',FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^1|4|2|3$/')));
        return self::ValidarRegresar($tipoDocumento, 'Tipo Documento',$opcional);
    }
 // offer
 public static function LeerDocumentType($opcional=false) {
    $documentType = filter_input(INPUT_GET, 'documentType',FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(DNI|RUC)$/')));  
    
    if (!$documentType) {
        //$documentType = 'DNI';
        throw new InvalidArgumentException('No hay un identificador valido');
    }
    return $documentType;  
    
}     
public static function LeerPhone() {
    $phone = filter_input(
            INPUT_GET, 'phone', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^([1-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]|-[0-9]{1,8})$/')));
   
    if (!$phone) {
        throw new InvalidArgumentException('No hay un identificador valido');
    }

    $resultado = new IdentificadorPhone();
    $resultado->phone = $phone;     
    return $resultado;       
}
public static function LeerDocumentNumber() {
    $documentNumber = filter_input(
        INPUT_GET, 'documentNumber', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+$/')));
    if (!$documentNumber) {
        throw new InvalidArgumentException('No hay un identificador valido');
    }

    $resultado = new IdentificadorDocumentNumber();
    $resultado->documentNumber = $documentNumber;     
    return $resultado;       
}
// offer
    public static function LeerDepartamento() {
        $departamento = filter_input(INPUT_GET, 'departamento', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[\p{L}áéíóúñ ]{1,60}$/')));
        return self::ValidarRegresar($departamento, 'Departamento');
    }

    public static function LeerProvincia() {
        $provincia = filter_input(INPUT_GET, 'provincia', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[\p{L}áéíóúñ ]{1,60}$/')));
        return self::ValidarRegresar($provincia, 'Provincia');
    }

    public static function LeerDistrito() { 
       $distrito = filter_input(INPUT_GET, 'distrito', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[\p{L}0123456789áéíóúñ ]{1,60}$/')));
       return self::ValidarRegresar($distrito, 'Distrito');
    }
    
    public static function LeerUbigeo() {
        $ubigeo = filter_input(INPUT_GET, 'ubigeo', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]{1,10}$/')));
        return self::ValidarRegresar($ubigeo, 'Ubigeo');
    }
    
    
    

    public static function LeerDireccion($opcional=false) {
        $direccion = filter_input(INPUT_GET,'direccion');
        return self::ValidarRegresar($direccion, 'direccion',$opcional);
    }


    public static function LeerCoordenada($parametro,$opcional=true) {
        $coordenada = filter_input(INPUT_GET,$parametro, FILTER_VALIDATE_FLOAT);
        return self::ValidarRegresar($coordenada, "Coordenada $parametro",$opcional);
    }


    public static function LeerMovil($indice,$opcional = false) {
        $movil = filter_input(INPUT_GET,"movil$indice", FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^9[0-9]{8}$/')));
        return self::ValidarRegresar($movil, 'Movil',$opcional);
    }

    public static function LeerVersion() {
        $version = filter_var(self::GetGet('version'), FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+$/')));

        if (!$version) {
            return 0;
        }
        return $version;
    }

    public static function LeerTelefono($opcional=false) {

        $telefono = filter_input(INPUT_GET,'telefono', FILTER_VALIDATE_REGEXP, 
                array('options' => array('regexp' => '/^[0-9]{8}$/')));
        
        return self::ValidarRegresar($telefono, 'telefono', $opcional);
    }

    public static function LeerTipo4play($opcional=false) {

        $tipo4play = filter_input(INPUT_GET,'Tipo4play', FILTER_VALIDATE_REGEXP, 
                array('options' => array('regexp' => '/^(CuatroPlay|CuatroPlayOnline)$/')));

        if (!$tipo4play) {
            $tipo4play = 'CuatroPlay';
        }

        return self::ValidarRegresar($tipo4play, 'tipo4play', $opcional);
    }
    
    public static function LeerServicio($opcional=false) {

        $telefono = filter_input(INPUT_GET,'telefono', FILTER_VALIDATE_REGEXP, 
                array('options' => array('regexp' => '/^-[0-9]{0,7}$/')));
        
        return self::ValidarRegresar($telefono, 'telefono', $opcional);
    }
    

    public static function GetGet($indice) {
        if (isset($_GET[$indice])) {
            return $_GET[$indice];
        } else {
            return null;
        }
    }

    public static function LeerScoringFijo($opcional = false) {
        $scoring = filter_var(self::GetGet('scoringFijoResultado'), FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^APROBAR|OBSERVAR|RECHAZAR$/')));
        return self::ValidarRegresar($scoring, 'scoringFijoResultado', $opcional);
    }

    public static function LeerScoringFijoRenta($opcional = false) {
        $scoring = filter_var(self::GetGet('scoringFijo'), FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]{3}$/')));
        
        return self::ValidarRegresar($scoring, '(scoringFijo) Scoring Fijo Renta', $opcional);
    }

    public static function LeerScoringFijoCodigo($opcional = false) {
        $scoring = filter_input(INPUT_GET,'scoringFijoCodigo', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[\p{L}0123456789áéíóúñ ]{1,60}$/')));
        return self::ValidarRegresar($scoring, '(scoringFijoCodigo) Scoring Fijo Codigo',$opcional);
    }

    public static function LeerScoringMovil($opcional = false) {
        $scoring = filter_var(self::GetGet('scoringMovilResultado'), FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^APROBAR|OBSERVAR|RECHAZAR$/')));
        
        return self::ValidarRegresar($scoring, '(scoringMovilResultado)',$opcional);
    }

    public static function LeerScoringMovilRenta($opcional = false) {
        $scoring = filter_input(INPUT_GET,'scoringMovil1', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]{1,4}$/')));

        return self::ValidarRegresar($scoring, '(scoringMovil1) Scoring Movil Renta)',$opcional);
    }

    public static function LeerScoringMovilCodigo($opcional = false) {
        $scoring = filter_input(INPUT_GET,'scoringMovilCodigo', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[\p{L}0123456789áéíóúñ ]{1,60}$/')));
        return self::ValidarRegresar($scoring, '(scoringMovilCodigo) Scoring Movil Codigo',$opcional);
    }

    public static function LeerLimiteCrediticio($opcional = false) {
        $scoring = filter_input(INPUT_GET,'limiteCrediticio', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]{1,10}$/')));
        return self::ValidarRegresar($scoring, '(limiteCrediticio) 3 digitos moviles',$opcional);
    }

    public static function LeerCobertura($opcional = false) {
        $cobertura = filter_input(INPUT_GET,'cobertura',FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^HFC|FTTH|ADSL$/')));
        return self::ValidarRegresar($cobertura, 'cobertura', $opcional);
    }

    public static function LeerIdentificador() {
        $telefono = filter_input(
                INPUT_GET, 'telefono', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[1-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/')));
        $numeroDocumento = filter_input(
                INPUT_GET, 'documento', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+$/')));

        if (!($telefono or $numeroDocumento)) {
            throw new InvalidArgumentException('No hay un identificador valido');
        }

        $resultado = new Identificador();
        $resultado->telefono = $telefono;
        $resultado->documento = $numeroDocumento;
        return $resultado;
    }

    public static function LeerIdentificadorNegativo() {
        $telefono = filter_input(
                INPUT_GET, 'telefono', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^([1-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]|-[0-9]{1,8})$/')));
        $numeroDocumento = filter_input(
                INPUT_GET, 'documento', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+$/')));

        if (!($telefono or $numeroDocumento)) {
            throw new InvalidArgumentException('No hay un identificador valido');
        }

        $resultado = new Identificador();
        $resultado->telefono = $telefono;
        $resultado->documento = $numeroDocumento;
        return $resultado;
    }
    
    public static function LeerFechaAlta($n = "",$opcional = false){
        $fechaAlta = filter_input(INPUT_GET,"fechaAlta$n",FILTER_VALIDATE_REGEXP,
                array('options' => array('regexp' => '/^[0-9]{8}$/')));
        return self::ValidarRegresar($fechaAlta, "Fecha Alta $n", $opcional);
    }
    
    public static function LeerSegmentoMovil($n="",$opcional = false){
        $codigoSegmento = filter_input(INPUT_GET,"codigoSegmento$n",FILTER_VALIDATE_REGEXP,
                array('options' => array('regexp' => '/^(01|02)$/')));
        return self::ValidarRegresar($codigoSegmento, "Segmento $n", $opcional);
    }
    
    public static function LeerOperador($opcional = false){
        $operador = filter_input(INPUT_GET,'operador',FILTER_VALIDATE_REGEXP,
                array('options' => array('regexp' => '/^(20|21|23|24|64)$/')));
        return self::ValidarRegresar($operador, 'Operador', $opcional);
    }
    
    public static function LeerPortabilidad($opcional = false){
        $portabilidad = filter_input(INPUT_GET,'portabilidad',FILTER_VALIDATE_REGEXP,
                array('options' => array('regexp' => '/^(0|1)$/')));
        return self::ValidarRegresar($portabilidad, 'Portabilidad', $opcional);
    }
    

    public static function LeerDocumento($opcional = false) {
        $documento = self::filter_var('documento', FILTER_VALIDATE_REGEXP, 
                ['options' => ['regexp' => '/^[0-9]{8,12}$/']]);
        return self::ValidarRegresar($documento, 'Documento',$opcional);
    }
    
    private static function filter_var($variable,$filtro,$options=0){
        if(!isset($_GET[$variable])){
            return null;
        } else {
            return filter_var($_GET[$variable],$filtro,$options);
        }
    }
    
    
    

    
    private function ValidarRegresarNuevo($parametro,$nombre,$opcional=false){
        if ($parametro === NULL && !$opcional) {
            throw new \Arpu\Exception\ParametroNoEnviado($nombre);
        } elseif($parametro === FALSE && !$opcional ) {
            throw new \Arpu\Exception\ParametroInvalido($nombre);
        } elseif($parametro === NULL || $parametro === FALSE){
            return FALSE;
        }else {
            return $parametro;
        }
    }

    
    
    const REGEX_DOCUMENTO = '/^[0-9]{8,12}$/';
    
    
    
    
    private static function ValidarRegresar($parametro,$nombre,$opcional=false){
        if ($parametro === NULL && !$opcional) {
            throw new \Arpu\Exception\ParametroNoEnviado($nombre);
        } elseif($parametro === FALSE && !$opcional ) {
            throw new \Arpu\Exception\ParametroInvalido($nombre);
        } elseif($parametro === NULL){
            return FALSE;
        } elseif($parametro === FALSE){
            return FALSE;
        } else {
            return $parametro;
        }
    }
    

    public static function LeerRetenciones($exception = true) {
        $modosValidos = '/^' . join('|', IModo::$modos) . '$/';
        $retenciones = filter_input(
                INPUT_GET, 'retenciones', FILTER_VALIDATE_REGEXP, 
                array('options' => array('regexp' => $modosValidos)));

        if (!$retenciones) {
            $retenciones = 'Ninguno';
        }
        return $retenciones;
    }

    public static function LeerPs() {
        $ps = filter_input(
                INPUT_GET, 'ps', FILTER_VALIDATE_REGEXP, array('options' =>
            array('regexp' => '/^[0-9]+$/',
                'flags' => FILTER_NULL_ON_FAILURE
        )));

        if ($ps === null) {
            throw new InvalidArgumentException('El formato de PS no es valido.');
        }

        return $ps;
    }

    public static function LeerAccionAsesor() {
        $accion = filter_input(
                INPUT_GET, 'accion', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(Consulta telefono|Click en producto|Mostrar registro|Mostrar argumentario|Mostrar descuento|Imprimir mi Oferta)$/')));

        if (!$accion) {
            throw new InvalidArgumentException('La accion no se encuentra en el listo de modos permitidos');
        }
        return $accion;
    }

    public static function LeerMonto() {
        $monto = filter_input(
                INPUT_GET, 'monto', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+(\.[0-9]+){0,1}$/')));
        if (!$monto) {
            throw new InvalidArgumentException('El monto debe ser un numero');
        }
        return $monto;
    }
    
    public static function LeerCargoFijo1() {
        $monto = filter_input(
                INPUT_GET, 'movil1_cargofijo', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+(\.[0-9]+){0,1}$/')));
        if (!$monto) {
            return 0;
        }
        return $monto;
    }

    public static function LeerCargoFijo2() {
        $monto = filter_input(
                INPUT_GET, 'movil2_cargofijo', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+(\.[0-9]+){0,1}$/')));
        if (!$monto) {
            return 0;
        }
        return $monto;
    }
    

    public static function LeerSegmentoId() {
        $SegmentoId = filter_input(
                INPUT_GET, 'SegmentoId', FILTER_VALIDATE_REGEXP, array('options' =>
            array('regexp' => '/^[0-9]+$/',
                'flags' => FILTER_NULL_ON_FAILURE
        )));

        if ($SegmentoId === null) {
            throw new InvalidArgumentException('El SegmentoId debe ser un numero');
        }
        return $SegmentoId;
    }

    public static function LeerTipo() {
        $tipo = filter_input(
                INPUT_GET, 'tipo', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(Migracion Down|Baja|Averias)$/')));
        if (!$tipo) {
            throw new InvalidArgumentException('El modo no esta permitido');
        }
        return $tipo;
    }

    public static function LeerTipoPaquete() {
        $tipo = filter_input(
                INPUT_GET, 'TipoPaquete', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(2|3)$/')));

        if (!$tipo) {
            throw new InvalidArgumentException('El Tipo de Paquete no esta permitido');
        }
        return $tipo;
    }

    public static function LeerDescuento() {
        $TipoDescuento = filter_input(
                INPUT_GET, 'descuento', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(Multiproducto|Trios|Duos|Lineas Monoproducto)$/')));
        if (!$TipoDescuento) {
            throw new InvalidArgumentException('El descuento no esta permitido');
        }
        return $TipoDescuento;
    }
    public static function LeerVariableMovil() { // octu 16
        $TipoCliente = filter_input(
                INPUT_GET, 'VariableMovil', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(Competencia|Postpago|Prepago|Control|Abierta|-)$/')));
        if (!$TipoCliente) {
            throw new InvalidArgumentException('El tipo no esta permitido');
        }
        return $TipoCliente;
    }
    public static function LeerTipoPago_Nuevo() {
        $TipoPago = filter_input(
                INPUT_GET, 'tipoPago', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(Contado|1|2|3|4|5)$/')));// octu 11 
        if (!$TipoPago) {
            throw new InvalidArgumentException('El tipo de pago no esta permitido');
        }
        return $TipoPago;
    }
    public static function LeerTipoPago() {
        $TipoPago = filter_input(
                INPUT_GET, 'tipoPago', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(Contado|F0|F50)$/')));
        if (!$TipoPago) {
            throw new InvalidArgumentException('El tipo de pago no esta permitido');
        }
        return $TipoPago;
    }
    
    public static function LeerOnline() {
        $tip = filter_input(
                INPUT_GET, 'modoCuatroyOnline', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(CuatroPlay|CuatroPlayOnline)$/')));
        if (!$tip) {
            throw new InvalidArgumentException('El tipo no esta permitido');
        }
        return $tip;
    }

    public static function LeerTipoCliente() {
        $TipoCliente = filter_input(
                INPUT_GET, 'tipoCliente', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(POS1|POS2|POS3|ALTA|PORTA_ESPECIAL|PORTA_GENERICA)$/')));
        if (!$TipoCliente) {
            throw new InvalidArgumentException('El tipo cliente no esta permitido');
        }
        return $TipoCliente;
    }

    public static function LeerModelo() {
        $rango = filter_input(INPUT_GET, 'modelo');

        if ($rango === null) {
            throw new InvalidArgumentException('El modelo debe ser texto');
        }
        return $rango;
    }

    public static function LeerCodigoMovil() {
        $codigoMovil = filter_input(INPUT_GET, 'Paquete_CodigoMovil');

        if ($codigoMovil === null) {
            throw new InvalidArgumentException('El Código Móvil debe ser texto');
        }
        return $codigoMovil;
    }

    public static function LeerRango() {
        $rango = filter_input(
                INPUT_GET, 'rango', FILTER_VALIDATE_REGEXP, array('options' =>
            array('regexp' => '/^[0-9]+$/',
                'flags' => FILTER_NULL_ON_FAILURE
        )));

        if ($rango === null) {
            throw new InvalidArgumentException('El rango debe ser un numero');
        }
        return $rango;
    }

    public static function LeerTipoFinanciamiento() {
        $rango = filter_input(
                INPUT_GET, 'TipoFinanciamiento', FILTER_VALIDATE_REGEXP, array('options' =>
            array('regexp' => '/^[0-9]+$/',
                'flags' => FILTER_NULL_ON_FAILURE
        )));

        if ($rango === null) {
            throw new InvalidArgumentException('El rango debe ser un numero');
        }
        return $rango;
    }    
    
    public static function LeerCuotaInicial() {
        $monto = filter_input(
                INPUT_GET, 'cuotaInicial', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+(\.[0-9]+){0,1}$/')));
        if (!$monto) {
            return 0;
        }
        return $monto;
    }

    public static function LeerCuotaMes() {
        $monto = filter_input(
                INPUT_GET, 'cuotaMes', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(12|18)$/')));
        if (!$monto) {
            return 0;
        }
        return $monto;
    }
    
    public static function LeerCuotaMensualMaxima() {
        $montoMax = filter_input(
                INPUT_GET, 'cuotaMensualMaxima', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+(\.[0-9]+){0,1}$/')));
        if (!$montoMax) {
            return 0;
        }
        return $montoMax;
    }

    public static function LeerPrecioFinanciado0() {
        $precioFinanciado0 = filter_input(
                INPUT_GET, 'precioFinanciado0', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+(\.[0-9]+){0,1}$/')));
        if (!$precioFinanciado0) {
            return 0;
        }
        return $precioFinanciado0;
    }
    
    public static function LeerScoringMensaje_Nuevo($numero,$opcional=false) {
        $mensaje = filter_input(INPUT_GET, "scoringmovil${numero}_detalle"); //scoringmovil1_ detalle
        
        if($mensaje === NULL || $mensaje === FALSE){
            return self::ValidarRegresar($mensaje, "(scoringmovil${numero}_detalle) ",$opcional);
        }

        $Financiamiento = '3'; // octu 7
        $mensaje=substr(trim($mensaje),-1);
        /* $f50 = strpos($mensaje, 'APLICA financiamiento, excepto con PPF');
		$f = strpos($mensaje, 'APLICA financiamiento');
	
        if ($f50 !== false) {
            $Financiamiento = 'F50';
        }else{
			if ($f !== false) {
				$Financiamiento = 'F0';
			}		
		} */

        //return $Financiamiento;
        return $mensaje;
    }
    public static function LeerScoringMensaje($numero,$opcional=false) {
        $mensaje = filter_input(INPUT_GET, "scoringmovil${numero}_mensaje");
        
        if($mensaje === NULL || $mensaje === FALSE){
            return self::ValidarRegresar($mensaje, "(scoringmovil${numero}_mensaje) ",$opcional);
        }
		
        $Financiamiento = 'CONTADO';
        $f50 = strpos($mensaje, 'APLICA financiamiento, excepto con PPF');
		$f = strpos($mensaje, 'APLICA financiamiento');
	
        if ($f50 !== false) {
            $Financiamiento = 'F50';
        }else{
			if ($f !== false) {
				$Financiamiento = 'F0';
			}		
		}

        return $Financiamiento;
    }
    
    public static function LeerModoMovistarTotal() {
        $tipo = filter_input(
                INPUT_GET, 'ModoMovistarTotal', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(CompletaMT|PlantaMT)$/')));
        if (!$tipo) {
            $tipo = 'CompletaMT';
        }
        return $tipo;
    }
    
    public static function LeerMovilScoring() {
        $movil = filter_input(
                INPUT_GET, 'movil', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^9[0-9]{8}$/')));
        if (!$movil) {
            return '';
        }
        return $movil;
    }
    
    public static function LeerTipoAccionScoring() {
        $tipoAccion = filter_input(
                INPUT_GET, 'tipoAccion', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^(CH|PR)$/')));
        if (!$tipoAccion) {
            return 'PR';
        }
        return $tipoAccion;
    }
    
    public static function LeerRentaScoring() {
        $renta = filter_input(
                INPUT_GET, 'renta', FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^[0-9]+(\.[0-9]+){0,1}$/')));
        if (!$renta) {
            return 0;
        }
        return $renta;
    }
}