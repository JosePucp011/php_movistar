<?php

namespace Arpu\Http;

use Arpu\Data\IdentificadorDL;

class ConsultarDocumento {
    public static function Procesar(){
        try {
            return self::Consultar();
        } catch(PDOException $exception){
            return (object)['error' => 'Problema interno','errorInterno' => get_class($exception)];
        } catch(Arpu\Exception\QueryNoEjecutado $exception){
            return (object)['error' => 'Problema interno','errorInterno' => get_class($exception)];
        } catch(\Exception $exception){
        /*Errores de usuario*/
        //DocumentoNoEnviado
        //DocumentoInvalido
        //DocumentoNoEncontrado
            
            return ((object)['error' => $exception->getMessage()]);
        }
    }
    
    private static function Consultar(){
        $documento = LectorParametros::LeerDocumento();
        return self::ProcesarDocumento($documento);
    }
    
    private static function ProcesarDocumento($documento){
        $resultado = IdentificadorDL::ConsultarDocumentoMultiple($documento);
        \Arpu\Util\Encoder::utf8_encode_deep($resultado);
        return $resultado;
    }
}
