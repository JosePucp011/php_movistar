<?php

namespace Arpu\Http;

use Arpu\Http\LectorParametros;
use Arpu\Data\ClienteDL;
use Arpu\Data\ClienteDLMovil;
use Arpu\Util\Encoder;
use Arpu\Data\CombinarClienteDL;
use \Arpu\Services\OfertaSugerida;
use Arpu\Entity\Componente;
use Exception;
use DateTime;
use DateInterval;

class ConsultaConvergenteClass {
    
    
    private $telefono = FALSE;
    private $movil1 = FALSE;
    private $movil2 = FALSE;
    
    private $documento = '00000000'; //Valor por defecto Documento
    private $cobertura = 'HFC';
    private $scoringFijo = '999'; //Valor por defecto Scoring Fijo
    private $scoringMovil1 = '9990'; //Valor por defecto Scoring Movil
    private $scoringLimiteCrediticio = '999';
    private $scoringFijoResultado = 'APROBAR';
    private $scoringMovilResultado = 'APROBAR';
    private $scoringFijoCodigo = '99999999999999999';
    private $scoringMovilCodigo = '99999999999999999';
    private $cargoFijo = 0;

    private $financiamiento = [
        1 => 'APLICA financiamiento inicial 0%, , , , , , , , , , , , ',
        2 => 'APLICA financiamiento inicial 0%, , , , , , , , , , , , '];
    
    
    private $fechaAlta = [
        1 => '20180113',
        2 => '20180113'
    ];
    
    private $codigoSegmento = [
        1 => '01',
        2 => '01'
        
    ];
    
    private $clienteFijo = null;
    private $clienteMoviles = [];
    private $cliente;
    
    public function LeerParametroAdicionales(){
        $this->cobertura = LectorParametros::LeerCobertura(); //obligatorio
        $this->cliente->cargoFijo1 = LectorParametros::LeerCargoFijo1();
        $this->cliente->cargoFijo2 = LectorParametros::LeerCargoFijo2();
        
        if($this->telefono === FALSE){
            
            $this->scoringFijo = LectorParametros::LeerScoringFijoRenta(); //si app
            $this->scoringFijoResultado = LectorParametros::LeerScoringFijo();
            $this->scoringFijoCodigo = LectorParametros::LeerScoringFijoCodigo();
            $this->AsignarUbicacionCliente();
        }
        
        if($this->telefono === FALSE || $this->cliente->Internet->Tecnologia === 'ADSL' || $this->cliente->Internet->Presente === 0)
        {
            $this->cliente->Ubicacion->Direccion = LectorParametros::LeerDireccion();
            $this->cliente->Ubicacion->Coordenada = LectorParametros::LeerCoordenada('x').",".LectorParametros::LeerCoordenada('y');
        }
       
        $this->scoringMovil1 = LectorParametros::LeerScoringMovilRenta(); //si app
        $this->scoringLimiteCrediticio = LectorParametros::LeerLimiteCrediticio(); //si app
        $this->scoringMovilResultado = LectorParametros::LeerScoringMovil();
        $this->scoringMovilCodigo = LectorParametros::LeerScoringMovilCodigo();
        $this->financiamiento[1] = LectorParametros::LeerScoringMensaje_Nuevo(1);
        $this->financiamiento[2] = LectorParametros::LeerScoringMensaje_Nuevo(2);
        $this->fechaAlta[1] = LectorParametros::LeerFechaAlta(1);
        $this->fechaAlta[2] = LectorParametros::LeerFechaAlta(2);
        $this->codigoSegmento[1] = LectorParametros::LeerSegmentoMovil(1);
        $this->codigoSegmento[2] = LectorParametros::LeerSegmentoMovil(1);
    }
    
    private function AsignarUbicacionCliente(){
        $this->cliente->Ubicacion = new \Arpu\Entity\Ubicacion();
        $this->cliente->Ubicacion->Departamento = LectorParametros::LeerDepartamento();
        $this->cliente->Ubicacion->Provincia = LectorParametros::LeerProvincia();
        $this->cliente->Ubicacion->Distrito = LectorParametros::LeerDistrito();
    }
    
    private function LeerFijo(){
        if ($this->telefono) {
            $this->clienteFijo = ClienteDL::Leer($this->telefono, null, 'CuatroPlay');
        }
    }
    
    private function LeerMovil1(){
        if($this->movil1){
            $this->clienteMoviles[] = ClienteDLMovil::LeerMovil($this->movil1,'movil1');
        }
    }
    
    private function LeerMovil2(){
        if($this->movil2){
            $this->clienteMoviles[] = ClienteDLMovil::LeerMovil($this->movil2,'movil2');
        }
    }
    
    private function AsignarScoring(){
        $this->cliente->Scoring = ['Fijo' => $this->scoringFijo,
                            'Movil1' => $this->scoringMovil1,
                            'LimiteCrediticio' => $this->scoringLimiteCrediticio,
                            'FijoResultado' =>  $this->scoringFijoResultado,
                            'MovilResultado' =>  $this->scoringMovilResultado,
                            'scoringFijoCodigo' =>  $this->scoringFijoCodigo,
                            'scoringMovilCodigo' =>  $this->scoringMovilCodigo,
                            'Financiamiento' =>  $this->financiamiento];
        
        
        
        
        
    }
    
    private function AsignarFechaAlta(){
        
        $this->cliente->FechaAlta = [1 => $this->fechaAlta[1],
            2 => $this->fechaAlta[2]];
        
        $this->cliente->Segmento = [1 => $this->codigoSegmento[1],
            2 => $this->codigoSegmento[2]];
        
        
        $hora = new DateTime();
        $hora->sub(new DateInterval('P8M'));
        $hoy = $hora->format("Ymd");
        
        if( isset($this->cliente->Componentes[Componente::Movil0])){
            $movil = $this->cliente->Componentes[Componente::Movil0];
            
            
            if($movil->Posicion === 'movil1' &&   $this->fechaAlta[1] <= $hoy && $this->codigoSegmento[1] == '02'){
                $movil->MayorIgual8Meses = true;
            }
            
            if($movil->Posicion === 'movil2' &&   $this->fechaAlta[2] <= $hoy && $this->codigoSegmento[2] == '02'){
                $movil->MayorIgual8Meses = true;
            }
        }
        
        if(isset($this->cliente->Componentes[Componente::Movil1])) {
            $movil = $this->cliente->Componentes[Componente::Movil1];
            
            if($movil->Posicion === 'movil2' && $this->fechaAlta[2] <= $hoy && $this->codigoSegmento[2] == '02'){
                $movil->MayorIgual8Meses = true;
            }
            
            if($movil->Posicion === 'movil1' && $this->fechaAlta[1] <= $hoy && $this->codigoSegmento[1] == '02'){
                $movil->MayorIgual8Meses = true;
            }
        }
    }
    
    public function Procesar($parametrosAdicional = true) {
        $this->documento = LectorParametros::LeerDocumento();
        $this->tipDocumento = LectorParametros::LeerTipoDocumento();
        
        if($this->tipDocumento == 1) {
            $this->tipDocumento = 'DNI';
        } else if($this->tipDocumento == 4) {
            $this->tipDocumento = 'RUC';
        } else if($this->tipDocumento == 2) {
            $this->tipDocumento = 'CEX';
        }

        $this->telefono = LectorParametros::LeerTelefono(true); //opcional
        
        if(!$this->telefono){
            $this->telefono = LectorParametros::LeerServicio(true);
        }
        
        $this->movil1 = LectorParametros::LeerMovil(1,true); //opcional
        $this->movil2 = LectorParametros::LeerMovil(2,true); //opcional
        $this->Tipo4play = LectorParametros::LeerTipo4play(true);
        
        $this->LeerFijo();
        $this->LeerMovil1();
        $this->LeerMovil2();

        $this->cliente = CombinarClienteDL::CombinarClientes(
                $this->clienteFijo, 
                $this->clienteMoviles,
                $this->documento,
                $this->tipDocumento);

        if($parametrosAdicional){
            $this->LeerParametroAdicionales();
        }

        $this->AsignarCargoFijoPortabilidad();
        $this->AsignarScoring();
        $this->AsignarFechaAlta();
        
        CombinarClienteDL::AsignarCobertura($this->cliente, $this->cobertura);
        $resultado = OfertaSugerida::ObtenerParaCliente($this->cliente, $this->Tipo4play);
        Encoder::utf8_encode_deep($resultado);

        $objeto = ModificarObjeto::TransformarResultado($resultado, $this->documento);
        return $objeto;
    }
    private function AsignarCargoFijoPortabilidad(){
        
        $cargoFijo = $this->cliente->cargoFijo1 + $this->cliente->cargoFijo2;
        
        if(isset($this->cliente->Componentes[Componente::Movil0]) && isset($this->cliente->Componentes[Componente::Movil1])){
            
            if($this->cliente->Componentes[Componente::Movil0]->Documento == 'Competencia' 
                    && $this->cliente->Componentes[Componente::Movil1]->Documento == 'Competencia' ){
                
                $this->cliente->Componentes[Componente::Movil0]->RentaMonoproducto = $this->cliente->cargoFijo1;
                $this->cliente->Componentes[Componente::Movil0]->RentaTotal = $this->cliente->cargoFijo1;

                $this->cliente->Componentes[Componente::Movil1]->RentaMonoproducto = $this->cliente->cargoFijo2;
                $this->cliente->Componentes[Componente::Movil1]->RentaTotal = $this->cliente->cargoFijo2;
            }else{
                
                if($this->cliente->Componentes[Componente::Movil0]->Documento == 'Competencia' ){
                $this->cliente->Componentes[Componente::Movil0]->RentaMonoproducto = $this->cliente->cargoFijo1;
                $this->cliente->Componentes[Componente::Movil0]->RentaTotal = $this->cliente->cargoFijo1;
                }
                if($this->cliente->Componentes[Componente::Movil1]->Documento == 'Competencia' ){
                    $this->cliente->Componentes[Componente::Movil1]->RentaMonoproducto = $this->cliente->cargoFijo2;
                    $this->cliente->Componentes[Componente::Movil1]->RentaTotal = $this->cliente->cargoFijo2;
                } 
            
            }           
            $this->cliente->RentaTotal += $cargoFijo;
            $this->cliente->RentaTotalConAdicionales += $cargoFijo;
            
        }elseif(isset($this->cliente->Componentes[Componente::Movil0])
                && $this->cliente->Componentes[Componente::Movil0]->Documento == 'Competencia'){
            
                $this->cliente->Componentes[Componente::Movil0]->RentaMonoproducto = $cargoFijo;
                $this->cliente->Componentes[Componente::Movil0]->RentaTotal = $cargoFijo;
                }elseif(isset($this->cliente->Componentes[Componente::Movil1])
                && $this->cliente->Componentes[Componente::Movil1]->Documento == 'Competencia'){
            
                $this->cliente->Componentes[Componente::Movil1]->RentaMonoproducto = $cargoFijo;
                $this->cliente->Componentes[Componente::Movil1]->RentaTotal = $cargoFijo;
                }            
    }
}
