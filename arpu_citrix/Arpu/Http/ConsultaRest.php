<?php


namespace Arpu\Http;
use InvalidArgumentException;
use Arpu\Entity\ContentNotWellFormed;
use Exception;
use stdClass;


class ConsultaRest {
    
    public static function AgregarCampoResultado($input, $output, $nodo, $nodoOrigen, $dato, $opcional) {
        if (!isset($input->$nodoOrigen) || !is_object($input->$nodoOrigen) || !isset($input->$nodoOrigen->$dato)) {
            if (!$opcional) {
                throw new InvalidArgumentException();
            }
        } else {
            if (!isset($output->$nodo)) {
                $output->$nodo = new \stdClass();
            }
            $output->$nodo->$dato = $input->$nodoOrigen->$dato;
        }
    }

    public static function EjecutarConsulta($consulta) {
        $output = new \stdClass();
        $output->HeaderOut = new stdClass();
        $fecha = new \DateTime('now');
        $output->HeaderOut->timestamp = $fecha->format('c');
        $output->HeaderOut->msgType = 'RESPONSE';

        try {
            $input = self::ObtenerInput();

            if(is_object($input)){
                    self::AgregarCampoResultado($input, $output, 'HeaderOut', 'HeaderIn', 'originator', false);
                    self::AgregarCampoResultado($input, $output, 'HeaderOut', 'HeaderIn', 'destination', false);
                    self::AgregarCampoResultado($input, $output, 'HeaderOut', 'HeaderIn', 'pid', true);
                    self::AgregarCampoResultado($input, $output, 'HeaderOut', 'HeaderIn', 'execId', false);
            }
			
            $resultado = $consulta->EjecutarObjeto($input);
            $output->BodyOut = $resultado;
			
        } catch (ContentNotWellFormed $exception) {
            $output->ClientException = new stdClass();
            $output->ClientException->exceptionCategory = 'SVC';
            $output->ClientException->exceptionCode = '1002';
            $output->ClientException->exceptionMsg = 'Content not well-formed';
            $output->ClientException->exceptionDetail = 'No detail';
            $output->ClientException->exceptionSeverity = 'E';
        } catch (InvalidArgumentException $exception) {
            $output->ClientException = new stdClass();
            $output->ClientException->exceptionCategory = 'SVC';
            $output->ClientException->exceptionCode = '1001';
            $output->ClientException->exceptionMsg = 'Missing Mandatory Parameter';
            $output->ClientException->exceptionDetail = 'No detail';
            $output->ClientException->exceptionSeverity = 'E';
        } catch (\Arpu\Entity\ClienteNoEncontrado $exception) {
            $output->ClientException = new stdClass();
            $output->ClientException->exceptionCategory = 'SVC';
            $output->ClientException->exceptionCode = '1234';
            $output->ClientException->exceptionMsg = 'Cliente no encontrado';
            $output->ClientException->exceptionDetail = 'No detail';
            $output->ClientException->exceptionSeverity = 'E';
        } catch (Exception $exception) {
            $output->ClientException = new stdClass();
            $output->ClientException->exceptionCategory = 'SRV';
            $output->ClientException->exceptionCode = '4000';
            $output->ClientException->exceptionMsg = 'Generic Server Fault';
            $output->ClientException->exceptionDetail = 'No detail';
            $output->ClientException->exceptionSeverity = 'E';
        }

		
        echo json_encode(self::ObtenerOutput($output));
    }

    const IMPRIMIR_CABECERA = false;
    
    private static function ObtenerOutput($output){
        if(self::IMPRIMIR_CABECERA){
			
            return $output;
        } elseif(isset($output->BodyOut)){
            return $output->BodyOut;
        } elseif(isset($output->ClientException)){
            return $output->ClientException;
        } else {
            return $output;
        }
    }
    
    
    
    
    private static function ObtenerInput(){
        $input_crudo = file_get_contents('php://input');
//        file_put_contents('E:/input_crudo.txt', $input_crudo);
//		file_put_contents('E:/url_crudo.txt', $_SERVER['REQUEST_URI']);
//		file_put_contents('E:/method_crudo.txt', $_SERVER['REQUEST_METHOD']);
        
        
        $input_json = json_decode($input_crudo);
        
        
        
        if (is_object($input_json)) {
            return $input_json;
        }
        
        $input_xml = simplexml_load_string($input_crudo);
        
        
        if(is_object($input_xml)){
            return $input_xml;
        }
        
        return null;
    }
        
        

}

