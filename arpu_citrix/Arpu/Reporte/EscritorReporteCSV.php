<?php

namespace Arpu\Reporte;

use DateTime;

class EscritorReporteCSV extends EscritorReporte
{
   private $archivo;
   private $separador;
   
   public function __construct($baseArchivo,$separador)
   {
      parent::__construct();
      $fecha = new DateTime('now');
      $nombreDeArchivo = 'd:/Volcado_'.$baseArchivo.'_'.$fecha->format('Y-m-d').'.txt';
      $this->separador = $separador;
      $this->archivo = fopen($nombreDeArchivo,'w');
   }

   protected function imprimirCabecera($objeto)
   {
      foreach(get_object_vars($objeto) as $campo => $valor)
      {
         fputs($this->archivo,$campo.$this->separador);
      }
      fputs($this->archivo,"\n");
   }

   protected function imprimirObjeto($objeto)
   {
      foreach(get_object_vars($objeto) as $campo => $valor)
      {
         fputs($this->archivo,$valor.$this->separador);
      }
      fputs($this->archivo,"\n");
   }
}
