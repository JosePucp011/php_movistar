<?php

namespace Arpu\Reporte;

use Arpu\Data\ClienteDL;
use Arpu\Services\OfertaSugerida;
use Arpu\Data\EstructuraDL;
use DateTime;
use Arpu\Config\Config;
use PDO;

header('Content-type: text/plain');



class ReporteIterator
{
   public static function ImprimirHora()
   {
       $fecha = new DateTime('now');
       echo $fecha->format('Y-m-d H:i:s')."\r\n";
   }

   public function __construct($consulta,$modo='Ninguno',$modoRetencion='BandaAncha')
   {
      set_time_limit(0);
      ini_set('memory_limit','-1');
      $this->conexion = Config::ConexionStandalone();
      $this->consulta = $this->conexion->query($consulta);
      
      $this->modo = $modo;
      $this->modoRetencion = $modoRetencion;
   }
   
   public function Obtener()
   {
      return $this->consulta->fetchObject();
   }
   
   public function Next() {
        if (($datos = $this->Obtener())) {
            try {
                
                $cliente = ClienteDL::Construir($datos, $this->modo);
                
                ClienteDL::AplicarParches($cliente);
                
                return OfertaSugerida::ObtenerParaCliente($cliente, $this->modo, $this->modoRetencion);
            } catch (Exception $excepcion) {
                echo $excepcion->getMessage() . "\r\n";
                echo $excepcion->getFile() . "\r\n";
                echo $excepcion->getLine() . "\r\n";
                return false;
            }
        } else {
            return false;
        }
    }

    /**
   * @var mysqli_stmt
   */
   var $consulta;
   private $modo;
}