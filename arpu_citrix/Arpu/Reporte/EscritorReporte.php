<?php 

namespace Arpu\Reporte;

abstract class EscritorReporte
{
   private $primerRegistro;
   
   public function __construct()
   {
      $this->primerRegistro = true;
   }
   
   protected abstract function imprimirCabecera($objeto);
   protected abstract function imprimirObjeto($objeto);
   
   public function escribir($objeto)
   {
      if($this->primerRegistro)
      {
         $this->imprimirCabecera($objeto);
         $this->primerRegistro = false;
      }
      $this->imprimirObjeto($objeto);
   }
}

