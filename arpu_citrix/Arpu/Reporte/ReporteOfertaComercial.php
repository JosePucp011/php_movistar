<?php

namespace Arpu\Reporte;

require_once(dirname(__FILE__) . '/../Autoload.php');



ReporteIterator::ImprimirHora();
/*Venta */
$reporte = new ReporteIterator('select * from clientes_ limit 10000',$argv[1]);
$escritor = new EscritorReporteCSV($argv[1], ",");

while ($oferta = $reporte->Next())
{
   $cliente = $oferta->Cliente;
   foreach ($oferta->Opciones as $key => $opcion)
   {
        $objeto = new \stdClass();
        $objeto->Telefono = $cliente->Telefono;
        
        $objeto->Tipo_Origen = $cliente->TipoOrigen();
        $objeto->Origen_Velocidad = $cliente->Internet->Velocidad;
        $objeto->Origen_Plan = 'MD '.round($cliente->Plan->RentaMonoproducto);
        $objeto->Origen_Cable = $cliente->Cable->Tipo;
        $objeto->Origen_Premium = $cliente->Cable->Premium->Nombre;
        $objeto->Origen_RentaTotal = round($cliente->RentaTotal,1);
        /* ID Producto*/
        $objeto->Prioridad = $key;
        $objeto->Categoria      =    $opcion->producto->Paquete->Tipo;
        $objeto->Plan_Consolidado      =    $opcion->PlanConsolidado();
        $objeto->Destino_Velocidad      =    $opcion->InternetConsolidado();
        $objeto->Destino_Cable      =    $opcion->CableConsolidado();
        $objeto->Destino_Premium      =    $opcion->PremiumConsolidado();
        $objeto->Destino_RentaTotal      =    round($opcion->salto_arpu + $cliente->RentaTotal,1);
        
        
        
        $escritor->escribir($objeto);  
        continue;
        /*Registros Comerciales*/
        $Promocion_UpVel = '';
        $Promocion_Descuento = '';
        $Promocion_Freeview = '';
        
        foreach($opcion->Comercial as $Opcion)
        {
            if($Opcion[2] === 'Velocidad')
            {
                $Promocion_UpVel = $Opcion[0];
            }
            else if($Opcion[2]=== 'Freeview')
            {
                $Promocion_Freeview = $Opcion[0];
            }
            else if($Opcion[3]=== 'Descuento')
            {
                $Promocion_Descuento = $Opcion[0];
            }
                
        }
        
        $objeto->Promocion_UpVel =      $Promocion_UpVel;
        $objeto->Promocion_Descuento =  $Promocion_Descuento;
        $objeto->Promocion_Freeview =   $Promocion_Freeview;
        
        $Estelar = 0;
        
        if($oferta->Cliente->Cable->Tipo === 'Estelar')
        {
            $Estelar = 20;   
        }
        $objeto->Bloque_Estelar =    $Estelar;    

        $Oferta_Deco1 = '';
        $Oferta_Deco2 = '';
        $Oferta_UltraWifi = '';
        foreach($opcion->ComercialDeco as $Comercial)
        {
          $Count = 0;
          foreach($Comercial as $Opcion)
          {  
            foreach($Opcion as $Opc)
            {
                if($Opc[0]=== 'Ultra Wifi')
                {
                    $Oferta_UltraWifi = 1;
                }
                
                if($Count === 0)
                {
                    $Oferta_Deco1 = $Opc[0]; 
                }
                else 
                {
                    $Oferta_Deco2 = $Opc[0];
                }
            }
            $Count += 1;
          }
        }
        
        $objeto->Oferta_Deco1 = $Oferta_Deco1;
        $objeto->Oferta_Deco2 = $Oferta_Deco2;
        $objeto->Oferta_UltraWifi = $Oferta_UltraWifi;
        
        $Modem_Nombre   =   '';
        $CoutaInstalacion = '';
        foreach($opcion->Registro as $registro )
        {
            if($registro->Componente === 'Modem')
            {
                $Modem_Nombre = $registro->Ps->Nombre;
            }
            if($registro->Ps->Ps === 23166)
            {
                $CoutaInstalacion = 1;
            }
        }
                
        $objeto->Modem_Nombre = $Modem_Nombre;
        $objeto->CuotaInstalacion = $CoutaInstalacion;
        $objeto->Renta_Total =  $opcion->cargo_fijo_destino;
        $objeto->Salto =    $opcion->salto_arpu;
      
      
   }
}

ReporteIterator::ImprimirHora();
//php D:\WebServer\Apache24\htdocs\arpu_citrix_20181010\Arpu\Reporte\ReporteOfertaComercial.php Ninguno
