<?php

namespace Arpu\Config;

echo dirname(__FILE__);

use SoapClient;
use SoapFault;
require_once dirname(__FILE__) . "/../../../../servername.php";

class Ares
{
   public static $WEB_SERVICE_URL;
   public static $APPLICATION_ID;
   
   
   public static function LoginDesarrollo($usuario,$password)
   {
       $resultado = new Ares_Result();
       
       if('error' == $usuario || 'error' == $password)
       {
           $resultado->esValido = false;
           $resultado->response = new stdClass();
           $resultado->response->AutenticarUsuarioResult = new stdClass();
           $resultado->response->AutenticarUsuarioResult->ErrorDescripcion = "Error desarrollo";
       }
       else
       {
           $resultado->esValido = true;
       }
       return $resultado;
   }
   
   public static function LoginProduccion($usuario,$password)
   {
	   $resultado = new Ares_Result();
	   try
	   {
		   
			$client = new SoapClient(Ares::$WEB_SERVICE_URL);
			$request = array(
			  "pUserName" => $usuario,
			  "pPassword" => $password,
			  "pApplicationId" => self::$APPLICATION_ID,
			  "pIpOrigen" => $_SERVER["SERVER_ADDR"],
			  "pIsWinAuthentication" => false);
			$response = $client->AutenticarUsuario($request);
			
			

			
			if ($response->AutenticarUsuarioResult->EsOk)
			{
				$resultado->esValido = true;
			}
			else
			{
				$resultado->esValido = false;
				$resultado->response = $response;
			}
		
		}
		catch(SoapFault $exception)
		{
			$resultado->esValido = true;
		}
        return $resultado;
   }
   
   
   public static function Login($usuario,$password)
   {
       if("desarrollo" == ARPU_SERVER_TYPE)
       {
            return self::LoginDesarrollo($usuario, $password);
       }
       else
       {
           return self::LoginProduccion($usuario, $password);
       }
   }
}



if ("desarrollo" == ARPU_SERVER_TYPE)
{
   Ares::$WEB_SERVICE_URL = "http://10.226.5.69/AresTM_WS_INT/Autentificacion.asmx?wsdl";
   Ares::$APPLICATION_ID = 0;
}
else
{
   Ares::$WEB_SERVICE_URL = "http://172.28.13.111/WSAresTM/Autentificacion.asmx?wsdl";
   Ares::$APPLICATION_ID = 35;
}
