<?php

namespace Arpu\Config;


if(basename($_SERVER['SCRIPT_FILENAME'])==basename(__FILE__)){
    header("HTTP/1.0 404 Not Found");
    exit(0);
}


use PDO;
class Config
{
    /**
     * 
     * @return \PDO
     */
   public static function ConexionBD()
   {
      if(null == Config::$conexion)
      {
         self::$conexion = self::ConexionStandalone();           
         return self::$conexion;
         
      }
      return Config::$conexion;
   }
   
   public static function ConexionStandalone(){
       
       //throws PDOException
       $dbh = new PDO(
              self::$cadena,
              self::$usuario,
              self::$password,
              self::$opciones);
       $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       return $dbh;
   }
   
   
   const SERVICIO_PARQUE_ATIS = "http://10.4.65.142:8111/INT/OSB/SRV/RESTConsultaATIS/ConsultaParqueEnATIS";
   const SERVICIO_DEUDA_MOVIL = "http://10.4.65.142:8211/BillingAccountManagement/BillingAccountAssociationsManagement/BillingCustomer/V1?WSDL";
   const SERVICIO_PARQUE_MOVIL = "http://10.4.65.142:8211/CustomerInformationManagement/CustomerInformation/V1?WSDL";
   const SERVICIO_PARQUE_MOVIL_DETALLE = "http://10.4.65.142:8211/ProductCatalogManagement/ProductCatalog/V1?WSDL";
   
   private static $cadena = "mysql:host=localhost;port=3306;dbname=calculadora_v2";
   private static $usuario = "dvictoriad";
   private static $password = "valorhogares3306";
   /* private static $usuario = "root";
   private static $password = ""; */
   private static $opciones = array(
       PDO::ATTR_EMULATE_PREPARES => false,
       PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true);

   private static $conexion;
   public static $RequiereTraduccion = false;
}
