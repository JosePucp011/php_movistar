<?php

namespace Arpu\Autenticacion;
use Arpu\Config\Config;
use Arpu\Util\Encoder;

class ConsultarCalculadoraActiva {
    
    public static function ObtenerEstadoCalculadora(){        
        $conexion = Config::ConexionBD();        
        $query = "select * from apagado_calculadora where Usuario ='DVICTODION' ;";
        $consulta = $conexion->prepare($query);
       
        if(!$consulta->execute()){
            throw new \Arpu\Exception\QueryNoEjecutado();
        }
            

        $resultado = new \stdClass();

        while( ($usuario = $consulta->fetchObject())){
            if(isset($usuario->Usuario)){
                $resultado->Usuario = $usuario->Usuario; 
                $resultado->Estado = $usuario->Apagado;          

            }
        }
        Encoder::utf8_encode_deep($resultado);
        
        return $resultado;
    }
}