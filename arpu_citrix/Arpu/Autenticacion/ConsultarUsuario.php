<?php

namespace Arpu\Autenticacion;
use Arpu\Config\Config;
use Arpu\Util\Encoder;

class ConsultarUsuario {
    
    public static function ObtenerUsuario($usuario){
        
        $conexion = Config::ConexionBD();
        
        $query = "select * from usuarios where usuario = ?";

        $consulta = $conexion->prepare($query);
        
        $consulta->bindValue(1, $usuario);
        if(!$consulta->execute()){
            throw new \Arpu\Exception\QueryNoEjecutado();
        }
            

        $resultado = new \stdClass();

        while( ($usuario = $consulta->fetchObject())){
            if(isset($usuario->Usuario)){
                $resultado->Usuario = $usuario->Usuario; 
                $resultado->Nombre = $usuario->Nombre; 
                $resultado->Documento = $usuario->Documento; 
                $resultado->Canal = $usuario->Canal; 

            }
        }
        Encoder::utf8_encode_deep($resultado);
        
        return $resultado;
    }
}