<?php

namespace Arpu\Autenticacion;

class Autenticacion {
    public static function AsegurarSesionHtml($header){
        if(self::EsSesionValida()){
            return;
        } else {
            header($header);
            exit(0);
        }
    }
    
    public static function AsegurarSesionJson(){
        if(self::EsSesionValida()){
            return;
        } else {
            $resultado = new \stdClass();
            $resultado->error = 'No ha iniciado sesion';
            echo json_encode($resultado);
            exit(0);
        }
    }

    public static function ObtenerUsuario(){

        if(isset($_SESSION["usuario"])){
            return ConsultarUsuario::ObtenerUsuario($_SESSION["usuario"]);
        }else{
            return null;
        }
    }
    public static function ObtenerEstadoCalculadora(){

        if(isset($_SESSION["usuario"])){
            return ConsultarCalculadoraActiva::ObtenerEstadoCalculadora();
        }else{
            return null;
        }
    }
    
    private static function EsSesionValida(){
        session_start();
        return isset($_SESSION["valido"]);
    }
    

}
