<?php

namespace Arpu\ServiciosIn;

require('../Autoload.php');

use Arpu\Config\Config;

class DeudaMovil {
     public function Consultar($Celular){
        
        try {
            $cliente = new \SoapClient(Config::SERVICIO_DEUDA_MOVIL);
            $request = $this->ConstruirRequest($Celular);
            
            return $cliente->retrieveBills($request)->RetrieveBillsResponseData;
        } catch (Exception $ex) {
            
        }
    }
    
    public function ConstruirRequest($Celular){
        return ['TefHeaderReq' => [
                'userLogin' => 'lramirezvi',
                'serviceChannel' => 'CustomerInformation',
                'application' => 'CALC',
                'ipAddress' => '10.4.43.235',
                'transactionTimestamp' => '2016-05-16T05:24:00',
                'serviceName' => 'retrieveBills',
                'version' => '1.0'
                ],
               'RetrieveBillsRequestData' => [
		"msisdn" => $Celular
                ]
                ];
        
    }
}
echo json_encode((new DeudaMovil())->Consultar(990956917));
