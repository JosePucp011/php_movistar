<?php

namespace Arpu\ServiciosIn;

class ScoringFijo {
    //const WEB_SERVICE_URL = 'http://10.4.65.142:8111/INT/OSB/SRV/RESTConEstrategiaComercial';
    const WEB_SERVICE_URL = 'http://10.4.65.143:8201/INT/OSB/SRV/RESTConEstrategiaComercial';
    
    public static function Ejecutar($tipoDocumento,$documento,$departamento,$provincia,$distrito,$ubigeo){
        
        $objeto = new \stdClass();
        $objeto->ApellidoPaterno = "NO APLICA";
        $objeto->ApellidoMaterno = "NO APLICA";
        $objeto->Nombres = "NO APLICA";
        $objeto->TipoDeDocumento = $tipoDocumento;
        $objeto->NumeroDeDocumento = $documento;
        $objeto->Departamento = $departamento;
        $objeto->Provincia = $provincia;
        $objeto->Distrito = $distrito;
        $objeto->Ubigeo = $ubigeo;
        $objeto->CodVendedor = "";
        $objeto->Zonal = "";
        
        
        $consultaRest = new RestService(self::WEB_SERVICE_URL);
        return $consultaRest->ConsultarObjeto($objeto);
    }
    
    const RESPUESTA_POR_DEFECTO = <<<HERE
{"CodConsulta":"99999999999999999",
"Accion":"CLIENTE NUEVO",
"DeudaAtis":0,
"DeudaCms":0,
"OperacionComercial":[
    {"CodOpe":"ALTA PURA","Renta":"999","Segmento":"A","ContadorDetalle":6,"Detalle":
        [
            {"CodProd":"102","DesProd":"Tr\u00edo","CuotaFin":0,"MesesFin":0,"PagoAdelantado":0,"NroDecosAdic":2},
            {"CodProd":"101","DesProd":"D\u00fao BA","CuotaFin":0,"MesesFin":0,"PagoAdelantado":0,"NroDecosAdic":0},
            {"CodProd":"104","DesProd":"D\u00fao TV","CuotaFin":0,"MesesFin":0,"PagoAdelantado":0,"NroDecosAdic":2},
            {"CodProd":"106","DesProd":"Mono BA","CuotaFin":0,"MesesFin":0,"PagoAdelantado":0,"NroDecosAdic":0},
            {"CodProd":"105","DesProd":"Mono TV","CuotaFin":19.9,"MesesFin":6,"PagoAdelantado":0,"NroDecosAdic":2},
            {"CodProd":"107","DesProd":"Mono L\u00ednea","CuotaFin":0,"MesesFin":0,"PagoAdelantado":0,"NroDecosAdic":0}
        ]
    },
    {"CodOpe":"ALTA COMPONENTE","Renta":"0","Segmento":"C1","ContadorDetalle":0}
],
HERE;
    
}
