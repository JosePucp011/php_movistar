<?php

namespace Arpu\ServiciosIn;
use Arpu\Exception\ErrorRespuestaServicioREST;

class GIS {
    public static function Ejecutar($x,$y){
        $objeto = new \stdClass();
        $objeto->NUM_NUM_TLF = "";
        $objeto->NUM_COD_CDX = "$x";
        $objeto->NUM_COD_CDY = "$y";
        
        $consultaRest = new RestService(self::$WEB_SERVICE_URL);
		
		$resultado = $consultaRest->ConsultarObjeto($objeto);
		if(!isset($resultado->WSSIGFFTT_FFTT_AVEResult)){
			throw new ErrorRespuestaServicioREST();
		}
		
        return $resultado;
    }
    
    public static $WEB_SERVICE_URL;
    
    const RESPUESTA_POR_DEFECTO = <<<ERROR
{ "WSSIGFFTT_FFTT_AVEResult" : {
    "GPON_TEC" : "NO",
    "HFC_TEC"  : "SI",
    "XDSL_TEC" : "NO" },
ERROR;
    
    
}

//GIS::$WEB_SERVICE_URL = "http://10.4.65.142:8111/INT/OSB/SRV/RESTCoberturaFFTTGis/CoberturaFFTTGis";
GIS::$WEB_SERVICE_URL = "http://10.4.65.143:8201/INT/OSB/SRV/RESTCoberturaFFTTGis/CoberturaFFTTGis";