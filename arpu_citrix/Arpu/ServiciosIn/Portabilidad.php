<?php

namespace Arpu\ServiciosIn;
use Arpu\Config\Config;
/**
 * Description of Portabilidad
 *
 * @author dvictoriad
 */
class Portabilidad {
    
    const URL = 'http://10.4.67.69:8011/CustomerManagement/Portability/Proxy/V1?wsdl';
    
    
    public static function Consultar($operadorOrigen,$tipoDocumento,$documento,$celular,$flagPostpagoPrepago,$socket){
        
        try {
            $cliente = new \SoapClient("http://10.4.67.69:8011/CustomerManagement/Portability/Proxy/V1?wsdl");
            $request = self::ConstruirRequest($operadorOrigen, $tipoDocumento, $documento, $celular, $flagPostpagoPrepago);
            $response = $cliente->prevalidatePortIn($request);
            
            $consultaPrevia = $response->PrevalidatePortInResponseData->previousConsultationNumber;
            
			
			print_r($response);
			
			
            
            $conexion = Config::ConexionBD();
            $query = $conexion->prepare("INSERT INTO consultas_portabilidad(previousConsultationNumber,socket)
        VALUES (?,?)");
      
            $query->bindValue(1, $consultaPrevia);
            $query->bindValue(2, $socket);
            $query->execute();
            
            
        } catch (Exception $ex) {

        }
    }
    
    
    public static function ConstruirRequest($operadorOrigen,$tipoDocumento,$documento,$celular,$flagPostpagoPrepago){
        return array(
            
            'TefHeaderReq' => array(
                'userLogin' => 'lramirezvi',
                'serviceChannel' => 'DLC',
                'sessionCode' => 'f5dcb75d-5777-49ce-9b70-499a63bd8c51',
                'application' => 'CALC',
                'idMessage' => '89f33c96-abfa-44c6-9c8e-5e3ce713bc27',
                'ipAddress' => '10.4.43.235',
                'transactionTimestamp' => '2018-08-20T22:29:58.739Z',
                'serviceName' => 'prevalidatePortIn',
                'version' => '1.0'
            ),
            'PrevalidatePortInRequestData' => array(
			  "receipt" => 22,
			  "donor" => $operadorOrigen,
                          'customerIdentification' => 
                array(
                'identificationType' => $tipoDocumento,
                'identificationNumber' => $documento
                ),
                'serviceType' => 1,
                'numbersAndPlansList' => array(
                    'numbersAndPlans' => array(
                        'number' => $celular,
                        'planType' => $flagPostpagoPrepago
                    )
                ) ));
        
    }
    
    
   

}
