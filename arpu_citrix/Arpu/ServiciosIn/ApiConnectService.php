<?php

namespace Arpu\ServiciosIn;
if(basename($_SERVER['SCRIPT_FILENAME'])==basename(__FILE__)){
    header("HTTP/1.0 404 Not Found");
    exit(0);
}

use Arpu\Exception\ErrorConsultaServicioREST;
use Arpu\Exception\ErrorProgramacionInterno;
use Arpu\Exception\ErrorRespuestaServicioREST;

class ApiConnectService {

    private $timeoutConexion = 10;
    private $timeoutOperacion = 15;
    private $url;
    private $request;
    private $data;
    
    public function setTimeoutConexion($segundos) {
        $this->timeoutConexion = $segundos;
    }

    public function setTimeoutOperacion($segundos) {
        $this->timeoutOperacion = $segundos;
    }

    public function __construct($url) {
        $this->url = $url;
    }

    private function Body($objeto) {
        
        if(!is_object($objeto)){
            throw new ErrorProgramacionInterno();
        }
        
        $json = json_encode($objeto);
        
        if($json === FALSE){
            throw new ErrorProgramacionInterno();
        }
        
        return $json;
    }

    private function ConstruirConsulta($objeto) {
        $body = $this->Body($objeto);
        $this->data = $body;
    }

    private function ConfigurarRequest() {
        $ch = curl_init($this->url);

        if ($ch === FALSE) {
            throw new ErrorURLServicioREST();
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:65001');
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0)	;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
			'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VybmFtZSI6IjYwNTg5In0.g0Z6MosucVcxXCMhFw5QoJ6asslIyG9Y4_bXBsV04wK5ueDkU76w-xET_rCMf39UZQU-9zW4Q7geYRLNg82WHg',
            'Content-Length: ' . strlen($this->data))
        );
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeoutConexion);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeoutOperacion);
        $this->request = $ch;
    }
    
    

    public function ConsultarObjeto($objeto) {
        $this->ConstruirConsulta($objeto);
        $this->ConfigurarRequest();
        $resultado = curl_exec($this->request);

        if ($resultado === FALSE) {
			
			
			if (curl_error($this->request)) {
    $error_msg = curl_error($this->request);
	echo $error_msg;
}
			
            throw new ErrorConsultaServicioREST();
        }


        return json_decode($resultado);
    }

    
}
