<?php

namespace Arpu\ServiciosIn;

use Arpu\Exception\ErrorConsultaServicioREST;
use Arpu\Exception\ErrorProgramacionInterno;
use Arpu\Exception\ErrorRespuestaServicioREST;

class RestService {

    private $timeoutConexion = 2;
    private $timeoutOperacion = 10;
    private $url;
    private $request;
    private $data;
    
    public function setTimeoutConexion($segundos) {
        $this->timeoutConexion = $segundos;
    }

    public function setTimeoutOperacion($segundos) {
        $this->timeoutOperacion = $segundos;
    }

    public function __construct($url) {
        $this->url = $url;
    }

    private function Body($objeto) {
        
        if(!is_object($objeto)){
            throw new ErrorProgramacionInterno();
        }
        
        $json = json_encode($objeto);
        
        if($json === FALSE){
            throw new ErrorProgramacionInterno();
        }
        
        
        return " \"BodyIn\" : $json ";
    }

    private function ConstruirConsulta($objeto) {
        $body = $this->Body($objeto);
        $cabecera = self::ESTRUCTURA_CABECERA;
        $this->data = "{ $cabecera ,$body }";
        
    }

    private function ConfigurarRequest() {
        $ch = curl_init($this->url);

        if ($ch === FALSE) {
            throw new ErrorURLServicioREST();
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($this->data))
        );
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeoutConexion);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeoutOperacion);
        $this->request = $ch;
    }
    
    

    public function ConsultarObjeto($objeto) {
        $this->ConstruirConsulta($objeto);
        $this->ConfigurarRequest();
        $resultado = curl_exec($this->request);

        if ($resultado === FALSE) {
            throw new ErrorConsultaServicioREST();
        }


        $dataJson = json_decode($resultado);
        if (!is_object($dataJson) || !isset($dataJson->BodyOut)) {
            throw new ErrorRespuestaServicioREST();
        } else {
            return $dataJson->BodyOut;
        }
    }

    
    
    const ESTRUCTURA_CABECERA = '"HeaderIn" : {
"country" : "PE",
"lang" : "es",
"entity" : "TDP",
"system" : "IVR",
"subsystem" : "CMS",
"originator" : "PE:TDP:IVR:CMS",
"sender" : "OracleServiceBus",
"userId" : "USERIVR",
"wsId" : "SistemIVR",
"wsIp" : "1.1.1.1",
"operation" : "consultarValidacionBiometrica",
"destination" : "PE:TDP:CMS:IVR",
"pid" : "550e8400-e29b-41d4-a716-446655440000",
"execId" : "550e8400-e29b-41d4-a716-446655440000",
"timestamp" : "2015-07-15T14:53:47.233-05:00",
"msgType" : "REQUEST",
"varArg" : {
  "arg" : {
    "key" : "token",
    "values" : {
      "value" : "token"
    }
  }
}
}';
}
