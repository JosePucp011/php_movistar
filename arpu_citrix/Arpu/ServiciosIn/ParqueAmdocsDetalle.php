<?php


namespace Arpu\ServiciosIn;

use Arpu\Config\Config;

require('../Autoload.php');


class ParqueAmdocsDetalle {
    public function Consultar($Celular){
        
        try {
            $cliente = new \SoapClient(Config::SERVICIO_PARQUE_MOVIL_DETALLE);
            $request = $this->ConstruirRequest($Celular);
            return $cliente->retrievePlanInfo($request)->RetrievePlanInfoResponseData;
        } catch (Exception $ex) {
            
        }
    }
    
    public function ConstruirRequest($Celular){
        return ['TefHeaderReq' => [
                'userLogin' => 'lramirezvi',
                'serviceChannel' => 'CustomerInformation',
                'application' => 'CALC',
                'ipAddress' => '10.4.43.235',
                'transactionTimestamp' => '2016-05-16T05:24:00',
                'serviceName' => 'retrievePlanInfo',
                'version' => '1.0'
                ],
               'RetrievePlanInfoRequestData' => [
		"msisdn" => [
                    "number" => $Celular,
                    ]
                ] 
            ];
        
    }
}


echo json_encode((new ParqueAmdocsDetalle())->Consultar(990956917));