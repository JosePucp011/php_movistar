<?php

namespace Arpu\ServiciosIn;
ini_set("default_socket_timeout", 10);

use Arpu\Config\Config;

class ScoringMovilSoap {
    
        const RESPUESTA_POR_DEFECTO  = [
                    "codError" => "0000",
                    "desError" => "0000",
                    "codTipError" => "0000",
                    "desTipError" => "0000",
                    "Score" => "9990",
                    "menScore" => "APLICA financiamiento inicial 0%, , , , , , , , , , , , ",
                    "idConScoring" => "99999999999999999",
                    "limCredito" => "999",
                    "canLineas" => "9", 
                    "restricciones" => "Precio de Equipo: Sin Restricciones. \/,Precio de Chip: S\/. 1 \/ Para CAEQ: UPSELL maxima de S\/. 999.0", 
                    "accion" => "APROBAR"
            ];
    
    public function Consultar($objeto){
        
        try {

            //$cliente = new \SoapClient("http://172.28.104.37:7002/INT/OSB/SRV/WSCustomerInformation?wsdl",['trace' => true]);
            $cliente = new \SoapClient("http://10.4.65.142:8111/INT/OSB/SRV/WSCustomerInformation?wsdl",['connection_timeout' => 10]);
            
//            $wsdl = "http://10.4.65.142:8111/INT/OSB/SRV/WSCustomerInformation?wsdl";
//            $cliente = new SoapClient($wsdl, array("connection_timeout" => 2));
            
            $ns = "http://telefonica.com/globalIntegration/header";
            $name = "HeaderIn";
            /*$data = [
                'country'=>'PE',
                'lang'=>'es',
                'entity'=>'TDP',
                'system'=>'GO',
                'subsystem'=>'PSI',
                'originator'=>'PE:TDP:GO:PSI',
                'sender'=>'OracleServiceBus',
                'userId'=>'GESTEL Legado',
                'operation'=>'RespuestaDeEnvio',
                'destination'=>'PE:CMS:CMS:CMS',
                'execId'=>'a2aa1107-e75c-44ed-90fa-2f96faf01e86',
                'timestamp'=>'2017-01-05T17:52:16.547-05:00',
                'msgType'=>'REQUEST'                
            ];*/
            //'userId'=>'85235081' OR 'GESTEL Legado'
            $data = [
                'country'=>'PE',
                'lang'=>'es',
                'entity'=>'TDP',
                'system'=>'APPCalculadoraMT',
                'subsystem'=>'APPCalculadoraMT',
                'originator'=>'PE:TDP:APPCalculadoraMT:APPCalculadoraMT',
                'sender'=>'BEAPPCalculadoraMT',
                'userId'=>'APPCalculadoraMT',
                'operation'=>'RespuestaDeEnvio',
                'destination'=>'PE:CMS:CMS:CMS',
                'execId'=>'a2aa1107-e75c-44ed-90fa-2f96faf01e86',
                'timestamp'=>'2017-01-05T17:52:16.547-05:00',
                'msgType'=>'REQUEST'                
            ];
            
            $header = new \SoapHeader($ns,$name,$data);
            
            $cliente->__setSoapHeaders($header);
            
            return $cliente->obtenerScoreCrediticio($objeto);
            
            
        } catch (\Exception $ex) {
            
//            print_r($cliente);
//            echo $ex->getMessage();
//            
            $respuestaDefecto = ScoringMovilSoap::RESPUESTA_POR_DEFECTO;
            $respuestaDefecto['errorInterno'] = $ex->getMessage();
            return $respuestaDefecto;
        }
    }

}