<?php


namespace Arpu\ServiciosIn;
if(basename($_SERVER['SCRIPT_FILENAME'])==basename(__FILE__)){
    header("HTTP/1.0 404 Not Found");
    exit(0);
}
class Geocodificacion {
    const WEB_SERVICE_URL = 'https://tdp-rest-backend-fija.mybluemix.net/address/geocodificardireccion';
    
    public static function Ejecutar($objeto){
        $consultaRest = new ApiConnectService(self::WEB_SERVICE_URL);
        return $consultaRest->ConsultarObjeto($objeto);
    }
}
