<?php

namespace Arpu\ServiciosIn;

use Arpu\Config\Config;

require('../Autoload.php');

class ParqueAmdocs {
    
    
    public function Consultar($TipoDocumento,$NumeroDocumento){
        
        try {
            $cliente = new \SoapClient(Config::SERVICIO_PARQUE_MOVIL);
            $request = $this->ConstruirRequest($TipoDocumento,$NumeroDocumento);
            return $cliente->searchCustomer($request);
        } catch (Exception $ex) {
            
        }
    }
    
    public function ConstruirRequest($TipoDocumento,$NumeroDocumento){
        return ['TefHeaderReq' => [
                'userLogin' => 'lramirezvi',
                'serviceChannel' => 'CustomerInformation',
                'application' => 'CALC',
                'ipAddress' => '10.4.43.235',
                'functionalityCode' => 'RetrieveIndividual',
                'transactionTimestamp' => '2016-05-16T05:24:00',
                'serviceName' => 'RetrieveIndividual',
                'version' => '1.0'
                ],
               'SearchCustomerRequestData' => [
		"customerIdentification" => [
                    "userContactPrimaryId" => $NumeroDocumento,
                    "userContactPrimaryType" => $TipoDocumento
                    ],
                "isToGetCycleCode" => false
                ] 
            ];
        
    }
    
    
}


echo json_encode((new ParqueAmdocs())->Consultar('DNI','43150481'));