<?php




namespace Arpu\ServiciosIn;

require('../Autoload.php');

use Arpu\Config\Config;
use Arpu\ServiciosIn\RestService;
use Arpu\Exception\ErrorRespuestaServicioREST;

/*
if(basename($_SERVER['SCRIPT_FILENAME'])==basename(__FILE__)){
    header("HTTP/1.0 404 Not Found");
    exit(0);
}*/

class ParqueAtis {
    
    public function Ejecutar(string $TipoDocumento, string $NumeroDocumento) : array {
        
        $objeto = new \stdClass();
        $objeto->TipoDocumento = $TipoDocumento;
        $objeto->NumeroDocumento = $NumeroDocumento;
        $consulta = new RestService(Config::SERVICIO_PARQUE_ATIS);
        
        $resultadoConsulta = $consulta->ConsultarObjeto($objeto);
        
        if(!isset($resultadoConsulta->DetalleParque) || !is_array($resultadoConsulta->DetalleParque)){
            throw new ErrorRespuestaServicioREST();
        }
        
        $resultado = [];
        foreach($resultadoConsulta->DetalleParque as $servicio){
            if($servicio->Telefono !== null){
                $resultado[] = $servicio;
            }
        }
        
        return $resultado;
    }
}

echo json_encode( (new ParqueAtis())->Ejecutar('DNI', '71268371'));