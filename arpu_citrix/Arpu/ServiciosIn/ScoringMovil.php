<?php

namespace Arpu\ServiciosIn;


class ScoringMovil {
    
    
    public static function Ejecutar($objeto){
        
        $consultaRest = new RestService(self::WEB_SERVICE_URL);
        return $consultaRest->ConsultarObjeto($objeto);
    }
    
    const RESPUESTA_POR_DEFECTO = <<<HERE
{"codError":"0000",
"desError":"0000",
"codTipError":"0000",
"desTipError":"0000",
"Score":"9990",
"menScore":"APLICA financiamiento inicial 0%, , , , , , , , , , , , ",
"idConScoring":"99999999999999999",
"limCredito":"999",
"canLineas":9,
"restricciones":"Precio de Equipo: Sin Restricciones. \/,Precio de Chip: S\/. 1 \/ Para CAEQ: UPSELL maxima de S\/. 999.0",
"accion":"APROBAR",
HERE;
    
    const WEB_SERVICE_URL = 'http://10.4.65.142:8111/INT/OSB/SRV/RESTWSCustomerInformation/obtenerScoreCrediticio';
}
