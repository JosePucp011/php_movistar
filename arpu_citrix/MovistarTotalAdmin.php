<!DOCTYPE html>

<?php
require_once(dirname(__FILE__).'/Arpu/Autoload.php');

use Arpu\Autenticacion\Autenticacion;
use Arpu\Http\LectorParametros;

Autenticacion::AsegurarSesionHtml('Location: LoginTotal.php');
$Usuario = Autenticacion::ObtenerUsuario();
$UsuarioAdmin=Autenticacion::ObtenerEstadoCalculadora(); 
$ModoMovistarTotal = LectorParametros::LeerModoMovistarTotal();
?>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="images/favicon.ico">

<title>Movistar Total</title>
    
<link rel="stylesheet" href='css/main_convergente.css' type='text/css'>   
<link rel="stylesheet" href='css/fonts.css' type='text/css'> 
<link rel="stylesheet" href='css/corporativo_convergente.css' type='text/css'> 
<link rel="stylesheet" href='css/convergencia.css' type='text/css'> 
<!--<link rel='stylesheet' href='css/custom-theme/jquery-ui-1.9.0.custom.min.css' type='text/css'>-->
 
<script src="scripts/jquery-1.8.2.js" type="text/javascript"></script>
<script src="scripts/jquery-ui-1.9.0.custom.min.js" type="text/javascript"></script>
<script src="scripts/jquery-ui-1.9.0.custom.js" type="text/javascript"></script>
<script src="scripts/convergencia/Direccion.js" type="text/javascript"></script>
<script src='scripts/moment.min.js' type='text/javascript'></script>
<script src="scripts/jquery.xml2json.js" type="text/javascript"></script>
<script><?php require 'scripts/convergencia/json2.js'; ?></script>
<script><?php require 'scripts/convergencia/Util.js'; ?></script>
<script><?php require 'scripts/convergencia/Documento.js'; ?></script>
<script><?php require 'scripts/convergencia/Hogar.js'; ?></script>
<script><?php require 'scripts/convergencia/Identificadores.js'; ?></script>
<script><?php require 'scripts/convergencia/Equipo.js'; ?></script>
<script><?php require 'scripts/convergencia/GIS.js'; ?></script>
<script><?php require 'scripts/convergencia/maps.js'; ?></script>
<script><?php require 'scripts/convergencia/Pedido.js'; ?></script>

<script 
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgVUq7KEwik_F-kD97M5_EWua2LA_KNXk&libraries=places&callback=initAutocomplete"
         async defer></script> 
         
<script>
    <?php 
require 'scripts/convergencia/Comparacion.js';
require 'scripts/convergencia/Convergente.js';
require 'scripts/convergencia/ParrillaDibujar.js';
require 'scripts/convergencia/IncrementoPrecio.js';
require 'scripts/convergencia/CiudadSitiada.js';
require 'scripts/convergencia/Retenciones.js';
require 'scripts/convergencia/Freeview.js';
require 'scripts/convergencia/DatosCliente.js';
require 'scripts/convergencia/ParqueAtis.js';
require 'scripts/convergencia/ApagadoCalculdora.js';

?></script>


<script>
$(function() {
    jQuery.fx.off = true;
    Documento.Inicializar();
    Identificadores.Inicializar();
    
//    ParqueAtis.Callback = console.log;
//    ParqueAtis.Llamar('DNI','71268371');

});
</script>

</head>
   
<body style="background-color: white; color: rgba(0, 0, 0, 0.5); margin-top: 0px;" >
    <div>
        <table style="width: 90%; margin: auto; border-bottom: 1px solid rgba(0, 0, 0, 0.2)">
            <tr>
                <td>
                    <a href="#" style="margin-left: 10%"><img style="width: 150px; padding-top: 0.3125rem;padding-bottom: 0.3125rem;" src="images/logo.png"></a>
                </td>
                <td style="text-align: right;">
                 <a class="CerrarSesion" style="margin-right: 10%" href='Menu_Total.php'>Menu</a>   
                 <a class="CerrarSesion" style="margin-right: 10%" href='LogoutTotal.php'>Cerrar Sesi&oacute;n</a>
                </td>
            </tr>
        </table>
    </div>
    <br>
    <h1>Movistar Total</h1>   


  <!-- INICIO DE VERIFICACION DEL ESTADO DE LA CALCULADORA -->
<?php 
    $estadoCalculadora=$UsuarioAdmin->Estado?'DESACTIVAR':'ACTIVAR';
    $colorBotonApagado=$UsuarioAdmin->Estado?'Red':'#00a9e0';
    $colorMensaje=$UsuarioAdmin->Estado?'#00a9e0':'Red';
    $MensajeEstado=$UsuarioAdmin->Estado?"LA CALCULADORA MT ESTA ACTIVADA":"LA CALCULADORA MT ESTA DESACTIVADA";
   
   
    if($Usuario->Canal=='ADMIN'){?>
    <form id='Formulario_Calculadora'  action='' method='post' > 
        <table>        
            <tr>
                <td style="width: 600px;">
                    <input type='submit' id='apagar' name='estado' value='<?php echo $estadoCalculadora ?>' >
                    <label for='apagar' style="  margin-bottom: 0px;margin-left: 10px;color: <?php echo $colorMensaje ?>">
                    <?php echo $MensajeEstado ?>
                    </label>
                </td>
                

            </tr>
        </table>  
       
    </form>  
    <form id="Formulario_Documento">
        <fieldset>
            <legend>Consulta</legend>
            <table>
                <tr>
                    <td  style='width: auto;'>
                        <label for='tipoDocumento'>Tipo Documento </label>
                        <select id='tipoDocumento'>
                            <option value='1'>DNI</option>
                            <option value='4'>RUC</option>
                            <option value='2'>CEX</option>
                        </select>

                        <label for='documento' style="margin-left: 10px;">Documento </label>
                        <input type='text' id='documento' style="margin-right: 10px;">
                        <input type="hidden" id='Tipo4play' value='CuatroPlay' />
                    </td>
                    <td style="width: auto;  text-align: left; margin-right: 10px;">
                        <div id='error_consultaDocumento' class='error'></div>
                        <div id='nombre' style='font-weight:bold'></div>
                        </td>
				    <td  style="width: auto;  text-align: left; margin-right: 10px; color: red;">
                        <div id='empleado' style='font-weight:bold'></div>
                    </td>
                    <td  style="width: auto;  text-align: left; margin-right: 10px; color: rgb(0,176,240);">
                        <div id='movitar_total' style='font-weight:bold'></div>
                    </td>
                    <td  style="width: auto;  text-align: left; margin-right: 10px; color: rgb(0,176,240);">
                        <div id='reasignacion' style='font-weight:bold'></div>
                    </td>
                    <td  style="width: auto;  text-align: left; margin-right: 10px;">
                        <div id='AltaNueva'><h8>Alta Nueva</h8></div>
                    </td>
                    <td>
                        <input id='ModoMovistarTotal' type="text" value="<?php echo $ModoMovistarTotal ?>" style="visibility: hidden;" > 
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>

    
    <form id="Formulario_Hogar" onsubmit="return false">
    </form>
<?php  }else if($UsuarioAdmin->Estado){ // que se muestre todo mas el boton activar ?>

    
    
    <form id="Formulario_Documento">
        <fieldset>
            <legend>Consulta</legend>
            <table>
                <tr>
                    <td  style='width: auto;'>
                        <label for='tipoDocumento'>Tipo Documento </label>
                        <select id='tipoDocumento'>
                            <option value='1'>DNI</option>
                            <option value='4'>RUC</option>
                            <option value='2'>CEX</option>
                        </select>

                        <label for='documento' style="margin-left: 10px;">Documento </label>
                        <input type='text' id='documento' style="margin-right: 10px;">
                        <input type="hidden" id='Tipo4play' value='CuatroPlay' />
                    </td>
                    <td style="width: auto;  text-align: left; margin-right: 10px;">
                        <div id='error_consultaDocumento' class='error'></div>
                        <div id='nombre' style='font-weight:bold'></div>
                        </td>
				    <td  style="width: auto;  text-align: left; margin-right: 10px; color: red;">
                        <div id='empleado' style='font-weight:bold'></div>
                    </td>
                    <td  style="width: auto;  text-align: left; margin-right: 10px; color: rgb(0,176,240);">
                        <div id='movitar_total' style='font-weight:bold'></div>
                    </td>
                    <td  style="width: auto;  text-align: left; margin-right: 10px; color: rgb(0,176,240);">
                        <div id='reasignacion' style='font-weight:bold'></div>
                    </td>
                    <td  style="width: auto;  text-align: left; margin-right: 10px;">
                        <div id='AltaNueva'><h8>Alta Nueva</h8></div>
                    </td>
                    <td>
                        <input id='ModoMovistarTotal' type="text" value="<?php echo $ModoMovistarTotal ?>" style="visibility: hidden;" > 
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>

    
    <form id="Formulario_Hogar" onsubmit="return false">
    </form>
    <?php  } else if(!$UsuarioAdmin->Estado && !$Usuario->Canal=='ADMIN'){  // No mostrar Nada    
    
}?>    
    

<!-- FIN DE LA VERIFICACION DEL ESTADO DE LA CALCULADORA  -->   
    
<!--Consulta de Datos -->

<form id="Formulario_Identificadores" onsubmit="return false">
    <fieldset>
        <legend>Ingrese los datos del cliente</legend>
        <fieldset>
            <legend>Servicio Fijo</legend>
            
            <table style="border-color: #000; border-width: 10px; margin-bottom: 5px">
                <tr style="margin-bottom: 20px;">
                    <td  style="width: auto;  text-align: left; margin-right: 10px;">
                        <label id='telefono_etiqueta' for='telefono'>Telefono Fijo</label>
                        <input type='text' id='telefono' style="margin-right: 10px;">                        
                    </td>
                    <td  style="width: auto;  text-align: left; margin-right: 10px;">
                        <div id='error_telefonoFijo' class='error'>No existe el telefono</div>
                        <div id='mensajeDireccion'></div>    
                    </td>
                    <td  style="width: auto;  text-align: left; margin-right: 10px;">
                        <input type='checkbox' id='telefono_notengo'>
                        <label id ='telefono_notengo_etiqueta' for='telefono_notengo'>No tengo</label> 
  
                    </td>
                    <td  style="width: auto;  text-align: left; margin-right: 10px;">
                        <div id ='cliente_direccion_instalacion'></div> 
                    </td>
                </tr>
            </table>
                        
            <label id='departamento_etiqueta' for='departamento'>Departamento</label>
            <select id="departamento">
            </select>
            <label id='provincia_etiqueta' for='provincia'>Provincia</label>
            <select id="provincia">
            </select>

            <label id='distrito_etiqueta' for='distrito'>Distrito</label>
            <select id="distrito">
            </select> 
                        
                        
            <input id="pac-input" class="controls" type="text" placeholder="Ingrese direccion">
            <div id="map" style="margin-top: 10px;">
                - Si el mapa no carga. Por favor realizar los siguientes pasos<br>
                1. Escribir la direccion en la caja "Direccion para registro"<br>
                2. Buscar el X e Y en GIS y llenarlo en las cajas para "Coordenada X" y "Coordena Y"<br>
                3. Presionar el boton "No encuentro la direccion consultar por X,Y"<br>
                4. Continuar con la consulta de oferta
            </div>    
            
            <table style="margin-top: 10px;">
                <tr>
                    <td style="width: auto;">
                        <label for='cobertura' id='cobertura_etiqueta'>Cobertura</label>
                        <select id="cobertura" style="display: inline-block;" disabled>
                            <option value="-">-</option>
                            <option value="ADSL">ADSL</option>
                            <option value="HFC">HFC</option>
                            <option value="FTTH">FTTH</option>
                        </select>   
                    </td>
                    
                    <td style="width: auto;">
                        <label for='direccion_x' id='direccion_x_etiqueta'>Coordenada X</label>    
                        <input style="width: 140px; margin-right: 5px; background-color: white;" type='text' id='direccion_x' value='' >   
                    </td>
                    <td style="width: auto;">
                        <label for='direccion_y' id='direccion_y_etiqueta'>Coordenada Y</label>
                        <input style="width: 140px; margin-right: 5px; background-color: white;" type='text' id='direccion_y' value='' >   
                    </td>
                    <td style="width: auto;">
                        <label for='direccion_y' id='direccion_texto_etiqueta'>Direccion para registro</label>
                        <input style="width: 300px;  background-color: white;" type='text' id='direccion_texto' value=''>                        
                    </td>                
                </tr>
            </table> 
            <br>
            <div class='ConsultaXY' id='consultarxy'  onclick='GIS.ConsultarSinMapa()' style="width: 350px;" >No encuentro la direccion. Consultar por X,Y</div>
        </fieldset>
        

		<br>
        
        <fieldset>
            <legend>Servicio Movil 1</legend>

            <table>
                <tr style="vertical-align: middle;">
                    <td style="width: auto;">
                        <label for='movil1'>Numero</label>
                        <input type='text' id='movil1'>
                        <label id ='cliente_movil1_tipolinea'></label>
                    </td>
                    <td style="width: auto;">
                        <div id='error_movil1' class='error' style="margin-left: 10px;">El telefono no es Movistar, por favor, llenar los datos de la competencia</div>
                    </td>
                    <td style="width: auto;">
                        <label for='movil1_operador' id='movil1_operador_etiqueta' style="margin-left: 10px;">Operador</label>
                        <select id='movil1_operador'>
                            <option value='20'>Entel</option>
                            <option value='21'>Claro</option>
                            <option value='24'>Bitel</option>
                        </select>
                    </td>
                    <td style="width: auto;">
                        <label for='movil1_tipo' id='movil1_tipo_etiqueta' style="margin-left: 10px;">Servicio</label>
                        <select id='movil1_tipo'>
                            <option value='02'>Postpago</option>
                            <option value='01'>Prepago</option>
                        </select>
                        
                        <label for='movil1_fechaAlta' id='movil1_fechaAlta_etiqueta' style="margin-left: 10px;">Fecha Alta</label>
                        <select id='movil1_fechaAlta'>
                            
                        </select>
                        
                        
                    </td>
                    <td style="width: auto;">
                        <label for='movil1_tipo' id='movil1_cargofijo_etiqueta' style="margin-left: 10px;">Cargo Fijo</label>
                        <input type='text' id='movil1_cargofijo'>
                    </td>
                    <td style="width: auto;">
                        <input type='checkbox' id='movil1_notengo'>
                        <label for='movil1_notengo' id='movil1_notengo_etiqueta'>No tengo</label>
                    </td>
                    <td style=" width: 5px;">
                        <input type='text' id='cliente_movil1_renta' style="visibility: hidden; width: 5px;">
                    </td>
                </tr>
            </table>
        </fieldset>
		<br>
        <fieldset>
            <legend>Servicio Movil 2</legend>
            
            <table>
                <tr style="vertical-align: middle;">
                    <td style="width: auto;">
                        <label for='movil2'>Numero</label>
                        <input type='text' id='movil2'>
                        <label id ='cliente_movil2_tipolinea'></label>
                    </td>
                    <td style="width: auto;">
                        <div id='error_movil2' class='error' style="margin-left: 10px;">El telefono no es Movistar, por favor, llenar los datos de la competencia</div>
                    </td>
                    <td style="width: auto;">
                        <label for='movil2_operador' id='movil2_operador_etiqueta' style="margin-left: 10px;">Operador</label>
                        <select id='movil2_operador'>
                            <option value='20'>Entel</option>
                            <option value='21'>Claro</option>
                            <option value='24'>Bitel</option>
                        </select>
                    </td>
                    <td style="width: auto;">
                        <label for='movil2_tipo' id='movil2_tipo_etiqueta' style="margin-left: 10px;">Servicio</label>
                        <select id='movil2_tipo'>
                            <option value='02'>Postpago</option>
                            <option value='01'>Prepago</option>
                        </select>
                        
                        <label for='movil2_fechaAlta' id='movil2_fechaAlta_etiqueta' style="margin-left: 10px;">Fecha Alta</label>
                        <select id='movil2_fechaAlta'>
                            
                        </select>
                    </td>
                   <td style="width: auto;">
                        <label for='movil2_tipo' id='movil2_cargofijo_etiqueta' style="margin-left: 10px;">Cargo Fijo</label>
                        <input type='text' id='movil2_cargofijo'>
                    </td>
                    <td style="width: auto;">
                        <input type='checkbox' id='movil2_notengo'>
                        <label for='movil2_notengo' id='movil2_notengo_etiqueta' >No tengo</label> 
                    </td>               
                    <td style=" width: 5px;">
                        <input type='text' id='cliente_movil2_renta' style="visibility: hidden ; width: 5px;">
                    </td>
                </tr>
            </table>
        </fieldset>
        <br>
        <table>
            <tr>
                <td><input type='submit' id='consultar' value='Consultar' style="margin-top: 5px;"></td>
                <td style="width: 100%"><div style="margin-left: 10px;"><span id="CodigoUltimaConsulta"></span></div></td>
            </tr>
        </table>        
        <br>
        
        
    </fieldset>
    
    <fieldset id='Formulario_Validacion' style="margin-top: 5px;">
        
        <legend>Evaluacion Crediticia</legend>
        
        
        <label for='scoringconv' >Resultado</label>
        <input type='text' class='datosScoring' id='scoringconv' value='' disabled style="font-weight: bold;">
        = Scoring Fijo (
        <input type='text' class='datosScoring' id='scoringconv_fijo' value='' disabled style="font-weight: bold;">
        )
        = Scoring Movil (
        <input type='text' class='datosScoring' id='scoringconv_movil' value='' disabled style="font-weight: bold;">
        )
        <div id='AvisoLimite' style='display:inline; padding: 2px; margin-left: 5px  margin-right: 5px;border: 1px solid #e01a00; border-radius: 5px;'></div> 
        <br><br>
        
        
        

        <fieldset>
            <legend>Scoring Fijo</legend>

            <label for='scoringfijo' >Resultado</label>
            <input type='text' class='datosScoring' id='scoringfijo' value='' disabled style="font-weight: bold;">
            <input type='text' class='datosScoring' id='scoringfijo_detalle' value='' disabled style="margin-left: 10px;">
            <input type='text' class='datosScoring' id='scoringfijo_renta' value='' disabled style="margin-left: 10px; width: 50px;">
            <input type='text' class='datosScoring' id='scoringfijo_codigo' value='' disabled style="margin-left: 10px; width: 150px;">

        </fieldset>
        
        <fieldset>
            <legend>Scoring Movil</legend>
        
            <table style="margin-top: 5px; width:100%; ">
                <tr>
                    <td>
                        <label for='scoringmovil1'>Resultado</label> 
                        <input type='text' class='datosScoring' id='scoringmovil' value=''  disabled style=" margin-right: 10px; font-weight: bold;">
                        Score <input type='text' class='datosScoring' id='scoringmovil_score' value=''  disabled style="width: 50px; margin-right: 10px;">
                        Limite Credito <input type='text' class='datosScoring' id='scoringmovil_limite' value=''  disabled style="width: 50px; margin-right: 10px;">
                        Lineas Maximas <input type='text' class='datosScoring' id='scoringmovil_maxlinea' value=''  disabled style="width: 50px; margin-right: 10px;">
                        Codigo <input type='text' class='datosScoring' id='scoringmovil_codigo' value=''  disabled style="width: 150px;">
                    </td>
                </tr>
                </table>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 50%;">
                        Mensaje Movil 1 <textarea  class='datosScoring' id='scoringmovil1_mensaje' value=''  disabled style="width: 70%;" ></textarea>
                        <textarea  class='datosScoring' id='scoringmovil1_detalle' value=''  disabled style="width: 1%; visibility: hidden"></textarea>
                    </td>
                    <td>
                        Mensaje Movil 2 <textarea  class='datosScoring' id='scoringmovil2_mensaje' value=''  disabled style="width: 70%;"></textarea>
                        <textarea  class='datosScoring' id='scoringmovil2_detalle' value=''  disabled style="width: 1%; visibility: hidden"></textarea>                        
                    </td>
                </tr>
            </table>
    
        </fieldset>
        
    </fieldset>
    
</form>

<!--Parrilla Oferta-->

<?php
    $numeroOferta = 4;
?>


    
<div id='Display_Ofertas' style="margin-left: 100px; margin-right: 100px;">
    
    <div id='Parrilla_MT'></div>   
        
<br>
<br>


<div  id="modal" class="modal">

<div class="modal-content">
  <div class="modal-header" style="text-align: center; width: 100%;">
    <table style="text-align: center; width: 100%;">
        <tr>
            <td style="text-align: center; width: 90%;"><h1 id="titulo_modal">Oferta </h1></td><td><span style="color: rgb(0,176,240); font-weight: bold;" id="close_modal">X</span></td>
        </tr>
    </table>

</div>
 <div class="modal-body" id="modal-body"></div></div></div>

 <div  id="modal1" class="modal1">
<div class="modal-content1">
  <div class="modal-header1" style="text-align: center; width: 100%;">
    <table style="text-align: center; width: 100%;">
        <tr>
            <td style="text-align: center; width: 90%;"><h1 id="titulo_modal1">Oferta </h1></td><td><span style="color: rgb(0,176,240); font-weight: bold;" id="close_modal1">X</span></td>
        </tr>
    </table>
</div>
 <div class="modal-body1" id="modal-body1"></div></div></div>
        
</body>
</html>