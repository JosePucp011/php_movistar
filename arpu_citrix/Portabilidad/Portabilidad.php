<?php


require_once(dirname(__FILE__).'/../Arpu/Autoload.php');

use Arpu\Config\Config;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;
require __DIR__ . '/vendor/autoload.php';

$conexion = Config::ConexionBD();

$input_crudo = file_get_contents('php://input');

file_put_contents('E:/input_bus.txt',$input_crudo);


$xml = simplexml_load_string($input_crudo);
$namespace1 = $xml->children('ns1',true);

$consultationId = $namespace1->consultationID->consultationNumberID;
$mensaje = $namespace1->subscriptionInformation->activationDate;


$query = $conexion->prepare('select socket from consultas_portabilidad where previousConsultationNumber = ?');
$query->bindValue(1,$consultationId);
$query->execute();
$objeto = $query->fetchObject();
$socket = $objeto->socket;


$client = new Client(new Version2X('http://localhost:3000', [
    'headers' => [
    ]
]));
$client->initialize();
$client->emit('porta_respuesta', ['client' => $socket,'mensaje' => "$mensaje" ]);
$client->close();


echo <<<HERE
<ReceiveMsgPrevalidatePortabilityResponse xmlns="http://telefonica.pe/Portability/ReceiveMsgPrevalidatePortability/V1" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <status>
        <responseCode>0000</responseCode>
        <responseMessage>OK</responseMessage>
    </status>
</ReceiveMsgPrevalidatePortabilityResponse>
HERE;

