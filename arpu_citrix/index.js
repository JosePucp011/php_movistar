$(function() {
    jQuery.fx.off = true;
    InicializarInterfaz();
    InicializarDialogos();
    Formulario.Inicializar();
});

function InicializarInterfaz(){
    $('#modosDeOperacion').buttonset();
    Formulario.ObtenerModoPorDefecto();
    
}

function InicializarDialogos(){
    CrearDialogo("#modal-NoRetener",500,500,150);
    CrearDialogo("#modal-ClienteRiesgoso",500,500,150);
    CrearDialogo("#modal-IPFija",500,500,150);
    CrearDialogo("#modal-registro-oferta",500,500,150);
    CrearDialogo("#modal-argumentario-test",500,500,150);
    CrearDialogo("#modal-descuento-retenciones",700,500,500);
    CrearDialogo("#modal-herramienta-retenciones",700,500,500);
    CrearDialogo("#modal-speech",600,500,500);
    CrearDialogo('#modal-Freeview',750,300,300);
    CrearDialogo('#modal-Reclamo',750,300,300);
    CrearDialogo('#modal-IncrementoPrecio',800,500,500);
    CrearDialogo("#modal-error-incremento",500,500,150);
    CrearDialogo("#modal-error-reconexion",500,500,150);
    CrearDialogo('#modal-ReasignacionPrecio',700,400,400);
    CrearDialogo('#modal-CiudadSitiada',700,400,400);
    CrearDialogo('#modal-analgesico',700,400,400);
    CrearDialogo('#modal-PreguntasFrecuentes',650,300,300);
    CrearDialogo('#modal-IncrementoPrecioArgumentario',650,300,300);
     /* Para el speech 1era linea */
     CrearDialogo('#modal-SpeechLinea',700,400,300);

    CrearDialogo("#modal-CambioTecnologia",500,500,150);
    CrearDialogo("#modal-AjusteIncrementoPrecio",350,100,100);
    CrearDialogo("#modal-ClientePotencial",350,150,100);
    CrearDialogo("#modal-ApagadoCobre",500,500,150);
    CrearDialogo("#modal-RetencionDuoTrio",500,500,150);
    CrearDialogo("#modal-Desposicionado",500,500,150);
    CrearDialogo("#modal-CompetenciaFTTH",500,500,150);
    CrearDialogo("#modal-DescuentoPermanente",500,500,150);
    CrearDialogo("#modal-error-incremento",500,500,150);
    CrearDialogo("#modal-error-reconexion",500,500,150);
}


function CrearDialogo(Dialogo,width,maxHeight,minHeight){
    $(Dialogo).dialog({
       autoOpen: false, resizable: false, draggable: false, modal: true,
       width: width,
       maxWidth: width,
       maxHeight: maxHeight,
       minHeight: minHeight
    });
}




var Reclamo = {};

Reclamo.Consultar = function (Cliente){

    $('#modal-Reclamo').html("");
    Reclamo.Mostrar(Cliente);

};

Reclamo.Mostrar = function(Cliente){

    var html = "";

    if(Cliente.ListaReclamos.length > 0){
        html +=
             "<td></br><table cellpadding='3' cellspacing='3' >" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Cluster</b></td>" +
             "<td class='cabeceraParrilla'><b>Estado</b></td>" +
             "<td class='cabeceraParrilla'><b>Fecha</b></td>" +
             "<td class='cabeceraParrilla'><b>Importe</b></td>" +
             "<td class='cabeceraParrilla'><b>Liquidacion Fecha</b></td>" +
             "<td class='cabeceraParrilla'><b>Liquidacion Importe</b></td>" +
             "<td class='cabeceraParrilla'><b>Motivo</b></td>" +
             "<td class='cabeceraParrilla'><b>Respuesta</b></td>" +
             "<td class='cabeceraParrilla'><b>Tipo</b></td>" +
             "</tr>";
        for (var x = 0; x < Cliente.ListaReclamos.length; x++) {
           html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + Cliente.ListaReclamos[x].Cluster + "</td>" +
             "<td class='cajaTexto'>" + Cliente.ListaReclamos[x].Estado + "</td>" +
             "<td class='cajaTexto'>" + Cliente.ListaReclamos[x].Fecha + "</td>" +
             "<td class='cajaTexto'>" + Cliente.ListaReclamos[x].Importe + "</td>" +
             "<td class='cajaTexto'>" + Cliente.ListaReclamos[x].LiquidacionFecha + "</td>" +
             "<td class='cajaTexto'>" + Cliente.ListaReclamos[x].LiquidacionImporte + "</td>" +
             "<td class='cajaTexto'>" + Cliente.ListaReclamos[x].Motivo + "</td>" +
             "<td class='cajaTexto'>" + Cliente.ListaReclamos[x].Respuesta + "</td>" +
             "<td class='cajaTexto'>" + Cliente.ListaReclamos[x].Tipo + "</td>" +
             "</tr>";
        }
    }
    else {
        html += "Cliente no tiene reclamos presentados";
    }
    $('#modal-Reclamo').html(html);
    $("#modal-Reclamo").dialog("open");
};








var Freeview = {};

Freeview.Consultar = function (Cliente){

    $('#modal-Freeview').html("");
    Freeview.Mostrar(Cliente);

};

Freeview.Mostrar = function(Cliente){

    var html = "";
    
    if(Cliente.Freeview.length === 0)
    {
        html += '<div style="font-size:15px"><b>Consideraciones:</b></div><br>';
        html += '<div style="font-size:15px">• Promoción sujeta a facilidades técnicas.</div><br>';
        html += '<div style="font-size:15px">• Aplica sólo para clientes que cuenten con tecnología digital (no para clientes analógicos).</div><br>';
        html += '<div style="font-size:15px">• Para aplicar al beneficio de Bloque HD es necesario contar con un decodificador HD.</div><br>';
        html += '<div style="font-size:15px">• El cliente que se registre en la promoción, no debe contar con alguna apertura de canales gratuitos al momento del registro.</div><br>';
 
    }
    else
    {
            html += "<center><table cellpadding='3' cellspacing='3'>" +  
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Campaña</b></td>" +
             "<td class='cabeceraParrilla'><b>Aplica a Regalo</b></td>" +
             "<td class='cabeceraParrilla'><b>Motivo</b></td>" +
             "<td class='cabeceraParrilla'><b>Beneficio</b></td>" +
             "<td class='cabeceraParrilla'><b>Registro en Landing</b></td>" +
             "<td class='cabeceraParrilla'><b>Fecha de Registro en Landing</b></td>" +
             "<td class='cabeceraParrilla'><b>Activado</b></td>" +
             "<td class='cabeceraParrilla'><b>Fecha de Inicio</b></td>" +
             "<td class='cabeceraParrilla'><b>Fecha de Fin</b></td>" +
             "<td class='cabeceraParrilla'><b>Estado de Beneficio</b></td></tr>";
            
            for(var i = 0; i < Cliente.Freeview.length; ++i)
            {
                html += "<tr>" +
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].Procedencia + "</td>" +
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].Aplica + "</td>" +
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].Motivo + "</td>" +
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].Nombre + "</td>" +
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].RegistradoLanding + "</td>" +
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].FechaRegistroLanding + "</td>" +
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].Activado + "</td>" +
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].FechaInicio + "</td>" +
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].FechaFin + "</td>"+
                "<td class='etiquetaSva'>" + Cliente.Freeview[i].EstadoFV + "</td></tr>";
            }
        
            html +=  "</table>";
    }
    $('#modal-Freeview').html(html);
    $("#modal-Freeview").dialog("open");
};

// ANALGESICO
var Analgesico={};
Analgesico.Consultar= function(Cliente){
    $("#modal-analgesico").html("");
    Analgesico.Mostrar(Cliente);
};
Analgesico.Mostrar=function(Cliente){
    var html="";
    if(Cliente.Analgesico){
        if(Cliente.AnalgesicoTipo=="4G"){
            html+= "<h3><b>Incluir “Plan Analgésico”</b><br><br>";            
            html+= "<b><i>Sólo si el cliente pregunta por una lentitud en su servicio de internet hogar…</i></b></h3> ";            
           // html+= "<div><b><h3>Speech:</h3></b>";
            html+= '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;">Speech:</h1><br>';
            html+= "Estimado  Sr/Sra <b>"+Cliente.Nombre+"</b> estamos trabajando para que disfrute del internet hogar más rápido. Le otorgamos un bono de Internet ilimitado 4G al ";
            html+= "<b>número "+Cliente.Telefono+"</b> por 2 meses totalmente gratis para que pueda navegar sin interrupciones. Asimismo, le recordamos que puede compartir internet desde su celular a otros celulares o laptops sin cargos adicionales.";  
            
            html+= "<h3><i>Sólo si el cliente decide poner un reclamo o darse de baja</i></h3>" ;
            html+= "<h3><i>(Si el cliente tiene otro bono de retención impactado, no se debe proceder con el speech 2) </i></h3>" ;
            html+= "<h3><i>(Sólo se aplicará como máximo 1 bono de descuento por analgésico) </i></h3>" ;
            html+= "<br>Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> lamentamos los inconvenientes causados y la situación generada por el servicio de internet brindado. Sin embargo, venimos trabajando ya para solucionar cualquier percance con la red de su hogar. Por ello, al evidenciar que usted es un cliente muy valioso para nosotros, le brindamos un ajuste en su recibo de S/.20 sobre el recibo emitido. Adicional, usted seguirá disfrutando del bono ilimitado 4G ";
            html+= "<b> ¿Acepta esta compensación?<b>.<br><br>"        ;
            html+= "<h3>Si no acepta, seguir con el flujo de retención</h3>";   
            html+="</div>";          
        }else if (Cliente.AnalgesicoTipo=="HD"){          
            html+= "<h3><b>Incluir “Plan Analgésico”</b><br><br>";            
            html+= "<b><i>Sólo si el cliente pregunta por una lentitud en su servicio de internet hogar…</i></b></h3> ";            
           // html+= "<div><b><h3>Speech:</h3></b>";
            html+= '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;">Speech:</h1><br>';
            html+= "Estimado  Sr/Sra <b>"+Cliente.Nombre+"</b> estamos trabajando para que disfrute de una mejor navegación de internet. Porque también entendemos que disfrutar de la mejor programación es importante para usted, le otorgamos el ";
            html+= "<b>bloque de canales HD</b> totalmente GRATIS por 3 meses. Te brindamos una mejor calidad y resolución de tus series y programas favoritos.";    
            
            html+= "<h3><i>Sólo si el cliente decide poner un reclamo o darse de baja</i></h3>" ;
            html+= "<h3><i>(Si el cliente tiene otro bono de retención impactado, no se debe proceder con el speech 2) </i></h3>" ;
            html+= "<h3><i>(Sólo se aplicará como máximo 1 bono de descuento por analgésico) </i></h3>" ;
            html+= "<br>Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> lamentamos los inconvenientes causados y la situación generada por el servicio de internet brindado. Sin embargo, venimos trabajando ya para solucionar cualquier percance con la red de su hogar. Por ello, al evidenciar que usted es un cliente muy valioso para nosotros, le brindamos un ajuste en su recibo de S/.20 sobre el recibo emitido. Adicional, usted seguirá disfrutando del bloque de canales HD totalmente gratis ";
            html+= "<b> ¿Acepta esta compensación?<b>.<br><br>";        
            html+= "<h3>Si no acepta, seguir con el flujo de retención</h3>"; 
            
            html+="</div>";    
        }else if (Cliente.AnalgesicoTipo=="PRIX"){            
            html+= "<h3><b>Incluir “Plan Analgésico”</b><br><br>";            
            html+= "<b><i>Sólo si el cliente pregunta por una lentitud en su servicio de internet hogar…</i></b></h3> ";            
           // html+= "<div><b><h3>Speech:</h3></b>";
            html+= '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;">Speech:</h1><br>';
            html+= "Estimado  Sr/Sra <b>"+Cliente.Nombre+"</b> estamos trabajando para que disfrute de una mejor navegación de internet. Porque sabemos que disfrutar de los mejores beneficios es importante para ti, te damos el nivel Premium en ";
            html+= "<b>Movistar Prix </b> por 3 meses totalmente gratis, nuestro programa de agradecimiento con descuentos, concursos y experiencias únicas.";    
           
            html+= "<h3><i>Sólo si el cliente decide poner un reclamo o darse de baja</i></h3>" ;
            html+= "<h3><i>(Si el cliente tiene otro bono de retención impactado, no se debe proceder con el speech 2) </i></h3>" ;
            html+= "<h3><i>(Sólo se aplicará como máximo 1 bono de descuento por analgésico) </i></h3>" ;
            html+= "<br>Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> lamentamos los inconvenientes causados y la situación generada por el servicio de internet brindado. Sin embargo, venimos trabajando ya para solucionar cualquier percance con la red de su hogar. Por ello, al evidenciar que usted es un cliente muy valioso para nosotros, le brindamos un ajuste en su recibo de S/.20 sobre el recibo emitido. Adicional, usted seguirá disfrutando del nivel Premium en nuestro programa Movistar Prix ";
            html+= "<b> ¿Acepta esta compensación?<b>.<br><br>";        
            html+= "<h3>Si no acepta, seguir con el flujo de retención</h3>" ;
            html+="</div>";    
        }

        html+= "<center><br><br><h1 id='AnalgesicoPreguntasFrecuentes' class = 'round verde' style='padding: 6px; width:30%;display: inline-block;vertical-align: middle;cursor: pointer;' > Preguntas Frecuentes </h1></center>";
       // html+=" <h1 id='AnalgesicoSpeechLinea' class = 'round verde' style='padding: 6px; width:30%;display: inline-block;vertical-align: middle;cursor: pointer;' > Speech 1era Linea </h1></center>";
    }


    // Funciones Analgesico
    
    
    $("#modal-analgesico").html(html);

    $('#AnalgesicoPreguntasFrecuentes').click(function()
    { 
        Analgesico.PreguntasFrecuentes(Cliente.AnalgesicoTipo,Cliente);            
    });
    $('#AnalgesicoSpeechLinea').click(function()
    { 
        Analgesico.PreguntasSpeechPrimeraLinea(Cliente.AnalgesicoTipo,Cliente);           
    });

    $("#modal-analgesico").dialog("open");

};
Analgesico.PreguntasFrecuentes = function (tipo,Cliente)
{  
    if(tipo=="4G"){
        $("#modal-PreguntasFrecuentes").
        html(        
        "<h2>En ningún momento pedí el beneficio</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> el beneficio que le otorgamos es totalmente gratis por ser cliente Movistar. Estamos trabajando para poder brindarle la mejor experiencia de internet.<br><br>"
        +"<h2>Yo no recibí ninguna comunicación del beneficio</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> es importante informarle que fue comunicado con anticipación vía correo electrónico. Recuerde que el bono ilimitado 4G no le genera costo adicional.<br><br>"
        +"<h2>Y puedo navegar en youtube / Facebook / netflix?</h2><br>"
        +"Estimado cliente sólo debe tener en cuenta que puede navegar ilimitadamente con este bono en la red 4G.  No hay ningún problema si usted decide usar youtube, Facebook, whatsapp o waze.<br><br>"
        +"<h2>Cómo comparto Internet</h2><br>"        
        +"Para Android:<br>"
        +"<ol>"
        +"<li>Ajustes de teléfono</li>"
        +"<li>Zona Wi-Fi</li>"
        +"<li>Configurar Zona Wi-Fi</li>"
        +"<li>Activar Zona Wi-Fi</li>"
        +"</ol>"
        +"Para IOS:<br>"
        +"<ol>"
        +"<li>Configuración</li>"
        +"<li>Compartir Internet</li>"
        +"<li>Activar Compartir Internet</li>"
        +"<li>Seleccionar sólo Wi-Fi y USB</li>"
        +"<li>Seleccionar contraseña Wi-Fi</li>"
        +"<li>Ingresa la clave deseada </li>"
        +"</ol>"       
        +"<h2>¿Están trabajando en una mejor experiencia de internet? ¿Cuándo estará listo?</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> venimos trabajando constantemente para que pueda disfrutar de la mejor navegación sin interrupciones. Lamentamos los inconvenientes ocasionados mientras trabajamos para usted. Lo mantendremos al tanto.<br><br>"        
        );      
        $("#modal-PreguntasFrecuentes").dialog("open");
    }else if (tipo=="HD"){ // creo que aqui debe ser otras preguntas
        $("#modal-PreguntasFrecuentes").
        html(        
        "<h2>En ningún momento pedí el beneficio</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> el beneficio que le otorgamos es totalmente gratis por ser cliente Movistar. Estamos trabajando para poder brindarle la mejor experiencia de internet.<br><br>"
        +"<h2>Yo no recibí ninguna comunicación del beneficio</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> es importante informarle que fue comunicado con anticipación vía correo electrónico. Recuerde que el bono ilimitado 4G no le genera costo adicional.<br><br>"
        +"<h2>¿Están trabajando en una mejor experiencia de internet? ¿Cuándo estará listo?</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> venimos trabajando constantemente para que pueda disfrutar de la mejor navegación sin interrupciones. Lamentamos los inconvenientes ocasionados mientras trabajamos para usted. Lo mantendremos al tanto.<br><br>"        
        );
        $("#modal-PreguntasFrecuentes").dialog("open");
    }else if(tipo=="PRIX"){
        $("#modal-PreguntasFrecuentes").
        html(        
        "<h2>En ningún momento pedí el beneficio</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> el beneficio que le otorgamos es totalmente gratis por ser cliente Movistar. Estamos trabajando para poder brindarle la mejor experiencia de internet.<br><br>"
        +"<h2>Yo no recibí ninguna comunicación del beneficio</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> es importante informarle que fue comunicado con anticipación vía correo electrónico. Recuerde que el bloque de canales HD es totalmente gratis por los siguientes 3 meses.<br><br>"
        +"<h2>Cuándo podré disfrutar del beneficio</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> Primero tiene que registrarse y en un máximo de 72 horas o 3 días podrá visualizar el nivel Premium.<br><br>"
        +"<h2>¿Están trabajando en una mejor experiencia de internet? ¿Cuándo estará listo?</h2><br>"
        +"Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> venimos trabajando constantemente para que pueda disfrutar de la mejor navegación sin interrupciones. Lamentamos los inconvenientes ocasionados mientras trabajamos para usted. Lo mantendremos al tanto.<br><br>"        
        );
        $("#modal-PreguntasFrecuentes").dialog("open");
    }
   
   
};
Analgesico.PreguntasSpeechPrimeraLinea = function (tipo,Cliente)
{  
    if(tipo=="4G"){
        $("#modal-SpeechPrimera").
        html(  "<h3>Sólo si el cliente decide poner un reclamo o darse de baja</h3>" 
        + "<br>Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> lamentamos los inconvenientes causados y la situación generada por el servicio de internet brindado. Sin embargo, venimos trabajando ya para solucionar cualquier percance con la red de su hogar. Por ello, al evidenciar que usted es un cliente muy valioso para nosotros, le brindamos un ajuste en su recibo de S/.20 sobre el recibo emitido. Adicional, usted seguirá disfrutando del bono ilimitado 4G "
        + "<b> ¿Acepta esta compensación?<b>.<br><br>"        
        + "<h3>Si no acepta, seguir con el flujo de retención</h3><br>"        
        );      
        $("#modal-SpeechPrimera").dialog("open");
    }else if (tipo=="HD"){
        $("#modal-SpeechPrimera").
        html(  
        "<br>Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> lamentamos los inconvenientes causados y la situación generada por el servicio de internet brindado. Sin embargo, venimos trabajando ya para solucionar cualquier percance con la red de su hogar. Por ello, al evidenciar que usted es un cliente muy valioso para nosotros, le brindamos un ajuste en su recibo de S/.20 sobre el recibo emitido. Adicional, usted seguirá disfrutando del bloque de canales HD totalmente gratis "
        + "<b> ¿Acepta esta compensación?<b>.<br><br>"        
        + "<h3>Si no acepta, seguir con el flujo de retención</h3><br>"        
        );      
        $("#modal-SpeechPrimera").dialog("open");
    }else if(tipo=="PRIX"){
        $("#modal-SpeechPrimera").
        html(  
        "<br>Estimado <b>Sr/Sra "+ Cliente.Nombre +"</b> lamentamos los inconvenientes causados y la situación generada por el servicio de internet brindado. Sin embargo, venimos trabajando ya para solucionar cualquier percance con la red de su hogar. Por ello, al evidenciar que usted es un cliente muy valioso para nosotros, le brindamos un ajuste en su recibo de S/.20 sobre el recibo emitido. Adicional, usted seguirá disfrutando del nivel Premium en nuestro programa Movistar Prix "
        + "<b> ¿Acepta esta compensación?<b>.<br><br>"        
        + "<h3>Si no acepta, seguir con el flujo de retención</h3><br>"        
        );      
        $("#modal-SpeechPrimera").dialog("open");
    }
   
   
};
// ANALGESICO
// --- REASIGNACION DE PRECIOS
var ReasignacionPrecio = {};

ReasignacionPrecio.Consultar = function (Cliente){
   $('#modal-ReasignacionPrecio').html("");
   ReasignacionPrecio.Mostrar(Cliente);
};
/****************/
ReasignacionPrecio.formato=function(texto){
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
};
  
ReasignacionPrecio.Mostrar = function(Cliente){
    var html= "";
    var fechaformateada="";    
    fechaformateada=ReasignacionPrecio.formato(Cliente.Reasignacion[0].fecha);      
    var dia = parseInt( fechaformateada.substr(0, 2));

    if( Cliente.Reasignacion[0].Tipo=='A'){
        html += '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;"> Migración de Parrilla - '+ fechaformateada +' </h1><br>';
        /*  html+="<h3>Speech:</h3>"; */
         html+="<b>"+ Cliente.Nombre +"</b>, Por modificaciones tecnológicas en nuestros plantes tenemos que hacer cambios en los precios de los componentes de su paquete Movistar Total. Reafirmamos que con estos cambios no se verá afectado el monto total que venía pagando por su paquete contratado.<br><br>";
         html+="A partir del <b>"+dia+"</b> de Julio se realizará un ajuste del precio en su internet Fijo y en su plan Móvil de su paquete Movistar total, pero su precio total del paquete no se verá afectado y usted seguirá pagando lo mismo.<br>";
             html+="<br>Recuerde que usted seguirá pagando <b>S/."+ Cliente.Reasignacion[0].Monto_Total_MT_NUEVO +"</b> por su paquete Movistar Total contratado.<br>";
             html+="<br>*Recuerda que, si tienes contratado servicios adicionales a tu paquete, el costo de estos será adicional al costo de tu paquete de Movistar Total.";
         html+="<br>*Te recordamos que, si tienes afiliada alguna de tus líneas al débito automático, revises con tu banco el monto límite de cobro (en caso sea necesario modificarlo) para que no se generen problemas de cobro posteriores.";
         html+="<br>*De no estar de acuerdo, tiene derecho al término de su contrato conforme a norma vigente.<br>";
     
         html += '<br><h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;"> Beneficios </h1><br>';
         html += '<table align="center">';
         html += '<tr>';
         html += '<td>';
                     html += '<table align="center">';
                     html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:100%;">Su Plan Móvil Disminuye de </th></tr>';
                     html += '<tr><td>';
                         html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 12px; text-align: center;" >';
                         html += '<tr>';
                         html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Actual</th>';
                         html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> A Partir de Julio</th>';
                         html += '</tr>';
                         html += '<tr style="font-size: 20px;" >';
                         html += '<td><b>S/.'+Cliente.Reasignacion[0].Monto_plan_movil_actual1 +' </td>';
                         html += '<td><b>S/.'+Cliente.Reasignacion[0].RENTA_MOVIL_NUEVO1+' </b></td>';
                         html += '</tr>';
                         html += '</table>';
                     html += '</td></tr>';
                     html += '</table>';
         html += '</td>';
         html += '<td>';
         html += '<table align="center">';
         html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:100%;">Su Internet Fijo Aumenta de </th></tr>';
         html += '<tr><td>';
             html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 12px; text-align: center;" >';
             html += '<tr>';
             html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Actual</th>';
                         html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> A Partir de Julio</th>';
             html += '</tr>';
             html += '<tr style="font-size: 20px;" >';
             html += '<td><b>S/.'+ Cliente.Reasignacion[0].TARIFA_INTERNET_FIJO_ACTUAL +'</b></td>';
             html += '<td><b>S/.'+Cliente.Reasignacion[0].TARIFA_INTERNET_FIJO_NUEVA +'</b></td>';      
             html += '</tr>';
             html += '</table>';
         html += '</td></tr>';
         html += '</table>';
     html += '</td>';
         html += '</tr>';
         html += '</table>';
    }else{
        html += '<h1 style=" border-radius: 4px; background-color:#efe; nborder: 1px solid #1C94C4;color: #1C94C4;padding: 6px;"> Migración de Parrilla - '+fechaformateada+'</h1><br>';
        html+="<b>"+Cliente.Nombre +" </b>, Por modificaciones tecnológicas en nuestros plantes tenemos que hacer cambios en los precios de los componentes de su paquete Movistar Total.<br><br>";
        
        if(Cliente.Reasignacion[0].Tipo=='B'){

            html+="<b>"+Cliente.Reasignacion[0].Mensaje+"</b><br><br><br>";
            html+="<b>Incremento: S/. ";
            html+=Cliente.Reasignacion[0].Ajuste+"</b><br><br>";

        }else if(Cliente.Reasignacion[0].Tipo=='C'){

            if((Cliente.Reasignacion[0].Mensaje!==null && Cliente.Reasignacion[0].Mensaje!=='' ) && Cliente.Reasignacion[0].Movil_1!=='' ){
            html+="<b>"+Cliente.Reasignacion[0].Movil_1+ " - "+Cliente.Reasignacion[0].Mensaje+"</b><br>";
            html+="<b>Ajuste: S/. ";
            html+=Cliente.Reasignacion[0].Ajuste+"</b><br>";
            }

            if((Cliente.Reasignacion[0].Mensaje1!==null && Cliente.Reasignacion[0].Mensaje1!=='') && Cliente.Reasignacion[0].Movil_2!=='' ){
                html+="<b>"+Cliente.Reasignacion[0].Movil_2+ " - "+Cliente.Reasignacion[0].Mensaje1+"</b><br>";
                html+="<b>Ajuste: S/. ";
                html+=Cliente.Reasignacion[0].Ajuste1+"</b><br>";
            }
            
        }

        html+="<br>*Te recordamos que estos nuevos montos se verán reflejados en tu facturación de agosto.";
        html+="<br>*Recuerda que, si tienes contratado servicios adicionales a tu paquete, el costo de estos será adicional al costo de tu paquete de Movistar Total.";
        html+="<br>*Te recordamos que, si tienes afiliada alguna de tus líneas al débito automático, revises con tu banco el monto límite de cobro (en caso sea necesario modificarlo) para que no se generen problemas de cobro posteriores.";
        html+="<br>*De no estar de acuerdo, tiene derecho al término de su contrato conforme a norma vigente.<br><br><br><br><br><br><br>";

    }   
    
    $('#modal-ReasignacionPrecio').html(html);
    $("#modal-ReasignacionPrecio").dialog("open");
}
// --- REASIGNACION DE PRECIOS

var IncrementoPrecio = {};

IncrementoPrecio.Consultar = function (Cliente){
   $('#modal-IncrementoPrecio').html("");
   IncrementoPrecio.Mostrar(Cliente);
};

IncrementoPrecio.Mostrar = function(Cliente){
    var html = "";
    var opciones=Comparacion.ObtenerModoParaMensaje();  
    
    for(var i = 0; i < Cliente.Mensajes.length; ++i)
    {
        var mensaje = Cliente.Mensajes[i];
        
        if( mensaje.Mensaje.search("Incremento") >= 0)
        {  
            //var rentaIncremento=(parseFloat(mensaje.Renta.Destino)-parseFloat(mensaje.Renta.Origen)).toFixed(2);
            var width = '30%';
            if(mensaje.Velocidad !== null)
            {  
            
            html += '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;">'+ mensaje.Mensaje  +' - ' + mensaje.Fecha  +'</h1>';
            html += "<div><center>Hola, <b>"+Cliente.Nombre+"</b> le informo que</center> <center>nuestros planes y sus tarifas se han actualizado, junto con esta actualización tambien aumentamos la</center> <center>velocidad de internet de tu servicio de  <b>"
            +mensaje.Velocidad.Origen+"Mbps</b> a "
            +"<b>" + mensaje.Velocidad.Destino+"Mbps</b>. Toda esta información podrá</center><center> visualizarla en el recibo enviado. A continuación le recordamos que usted cuenta:</center>";
            html +="</div>";
            
            
            html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 15px; text-align: center;" >';
            
            
            html += '<tr>';
            html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Hasta '+ mensaje.Mes.Origen +'</th>';
            html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Desde '+ mensaje.Mes.Destino +'</th>';
            html += '</tr>';
            html += '<tr style="font-size: 20px;" >';
            html += '<td><b>S/.'+ mensaje.Renta.Origen +'</b></td>';
            html += '<td><b>S/.'+ mensaje.Renta.Destino +'</b></td>';
            html += '</tr>';
            html += '</table>';
            
            html += '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;"> Beneficios </h1><br>';
            
            html += '<table align="center" bottom="middle" style="text-align: center; ; width:100%"  ><tr>';
            
            

            html += '<td width='+ width +'; valign="top";>';
                
                    html += '<table align="center">';
                        html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:100%;">Mas Velocidad</th></tr>';
                        html += '<tr><td>';
                            html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 12px; text-align: center;" >';
                            html += '<tr>';
                            html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Antes Tenia</th>';
                            html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Ahora Tiene</th>';
                            html += '</tr>';
                            html += '<tr style="font-size: 20px;" >';
                            html += '<td>'+ mensaje.Velocidad.Origen +' Mbps</td>';
                            html += '<td><b>'+ mensaje.Velocidad.Destino +' Mbps</b></td>';
                            html += '</tr>';
                            html += '</table>';
                        html += '</td></tr>';
                    html += '</table>';
                    
                
                html += '</td>';
            }
            
            
            if(mensaje.Canales !== null){
               
            
                html += '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;">'+ mensaje.Mensaje  +' - ' + mensaje.Fecha  +'</h1>';
                html += "<div><center>Hola, <b>"+Cliente.Nombre+"</b> esta nueva tarifa se da porque hemos actualizado todo nuestros planes.</center>"+ 
                "<center>Queremos que sigas disfrutando de promociones exclusivas y experiencias únicas con el app Movistar Prix y</center>"+
                "<center>además, puedas ver TV en vivo y más de 10 mil contenidos por el app Movistar Play<b>"
               +"</div>";          

                html += '<table align="center" bottom="middle" style="padding:6px; border-collapse: separate; border-spacing: 6px; font-size: 15px; text-align: center;" >';
                html += '<tr>';
                html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Hasta '+ mensaje.Mes.Origen +'</th>';
                html += '<th style=" border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px;"> Desde '+ mensaje.Mes.Destino +'</th>';
                html += '</tr>';
                html += '<tr style="font-size: 20px;" >';
                html += '<td><b>S/.'+ mensaje.Renta.Origen +'</b></td>';
                html += '<td><b>S/.'+ mensaje.Renta.Destino +'</b></td>';
                html += '</tr>';
                html += '</table>';

                html += '<h1 style=" border-radius: 4px; background-color:#efe; border: 1px solid #1C94C4;color: #1C94C4;padding: 6px;"> Reforzar el contenido de Movistar </h1><br>';
                
                html += '<table align="center" bottom="middle" style="text-align: center; ; width:100%"  ><tr>';
                
                /* PARA DUO BA */
                if((Cliente.Internet.Presente && Cliente.Linea.Presente && !Cliente.Cable.Presente) 
                    || (Cliente.Internet.Presente && !Cliente.Linea.Presente && !Cliente.Cable.Presente) ){                    
                 
                }else{
                    html += '<td width='+ width +'; valign="top";>';
                    html += '<table ALIGN="center">';
                        html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:150;">Canales Exclusivos</th></tr>';
                        html += '<tr><td><img src="images/Canales03.png" width=150;>';
                        html += '</td></tr>';
                    html += '</table>';
                html += '</td>';
                }
                /* FIN DUO BA  */

               
            }
                                           
            if(mensaje.MPlay !== null)
            {   
                 /* PARA INTERNET NAKED */
                 if(Cliente.Internet.Presente && !Cliente.Linea.Presente && !Cliente.Cable.Presente ){ 
                    
                 
                }else{
                    html += '<td width='+ width +'; valign="top";>';
                
                    html += '<table ALIGN="center">';
                        html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:150;">Movistar Play</th></tr>';
                        html += '<tr><td>';
                            html += '<table align="center" bottom="middle" style="border-collapse: separate;  font-size: 15px; text-align: center;" >'; 
                            html += '<tr>';
                            html += '<td><img src="images/mplay03.png" width=150px;></td>';
                            html += '</tr>';
                            /* html += '<tr>';
                            html += '<td style="font-size: 12px;">• Canales en vivo 24 horas<br>•Los Partidos de la Copa Movistar<br>•Alquiler de Peliculas</td>';
                            html += '</tr>'; */
                            html += '</table>';
                        html += '</td></tr>';
                    html += '</table>';
                    html += '</td>';
                }
                /* FIN PARA INTERNET NAKED */               
            }
            
            if(mensaje.Prix !== null){
               
                html += '<td width='+ width +'; valign="top";>';
                
                    html += '<table ALIGN="center">';
                        html += '<tr><th style="text-align: center; border-radius: 4px; background-color:#1C94C4; border: 1px solid #1C94C4;color:white ;padding: 6px; font-size: 15px; width:150;">Movistar Prix</th></tr>';
                        html += '<tr><td><img src="images/mprix03.png" width=150px;>';
                        html += '</td></tr>';
                    html += '</table>';
                html += '</td>';
            }
                
            html += '</tr></table>';
            html += "<br><div align='center' bottom='middle' >";
            
            html += "<h1 id='IncrementoPrecio_Argumentario' class = 'round verde' style='padding: 6px; width:30%;display: inline-block;vertical-align: middle;cursor: pointer;' > Preguntas Frecuentes </h1>";
            
            // hasta aqui           
        }  
    }

    
    if(opciones!="Retencion" ){         
        //Si es retencion que NO muestre el speech 1era linea 
     }else{
        for(var i = 0; i < Cliente.Mensajes.length; ++i)
        {
            var mensaje = Cliente.Mensajes[i];

            if( mensaje.Mensaje.search("Incremento") >= 0)
            {   //console.log(mensaje.excluir);

                if (mensaje.ExcluirIncremento==1) {      
                    
                } else{
                    html +=" <h1 id='SpeechLinea' class = 'round verde' style='padding: 6px; width:30%;display: inline-block;vertical-align: middle;cursor: pointer;' > Speech </h1></div><br>";
                }
            }
         }
    }    

    $('#modal-IncrementoPrecio').html(html);
    
    $('#IncrementoPrecio_Argumentario').click(function()
    {   
        IncrementoPrecio.IncrementoPrecio_Argumentario(Cliente);
      /*  if(Cliente.Telefono > 0)
       {
            IncrementoPrecio.IncrementoPrecio_Argumentario(Cliente);
       }
       else
       {
           IncrementoPrecio.IncrementoPrecio_Argumentario_Television();
       } */
    });
    
    $("#modal-IncrementoPrecio").dialog("open");
    
    /* Para el speech Linea  */
    $('#SpeechLinea').click(function(){                 
        IncrementoPrecio.Argumentario_SpeechLinea(Cliente);
    });
    /* Para el speech Linea  */
};

IncrementoPrecio.IncrementoPrecio_Argumentario = function (Cliente)
{   
    
    var fechaMensajeSpechLinea = Cliente.Mensajes.length>0 ?Cliente.Mensajes[0].Fecha:'';
        
         
      $("#modal-IncrementoPrecioArgumentario").
   html(
   "<h1>Preguntas Frecuentes</h1>"
   +"<h2>1. No Acepto el incremento : Cliente Con Beneficios</h2><br>"
   +"Hola, Entendemos tu incomodidad,"+Cliente.Nombre +", pero queremos contarte que este aumento de tarifa llega también con un incremento de [la cantidad de megas][la velocidad de internet] en tu plan actual. Revisemos juntos el detalle estos nuevos beneficios y aclarar todas tus dudas.<br><br>"
   
   +"<h2>2. No Acepto el incremento : Cliente Sin Beneficios</h2><br>"
   +"Hola, "+Cliente.Nombre +" entendemos tu incomodidad en este proceso de restructuración de tarifas. Igual queremos recordarte que tu plan ya te permite disfrutar de promociones exclusivas y experiencias únicas con el app Movistar Prix y además, puedes ver TV en vivo y más de 10 mil contenidos por el app Movistar Play.<br><br>"
   
   +"<h2>3. Cliente no está de acuerdo con explicación y desea solicitar la baja.</h2><br>"
   +"Seguir el Proceso regular de baja (pool de Retención).<br><br>"
   
   +"<h2>4. Yo no recibí ninguna comunicación, me doy con la sorpresa de este cambio recién cuando llega mi recibo</h2><br>"
   +"La comunicación se ha realizado por [carta o recibo] según  el proceso normado por nuestro ente regulador.<br><br>"
   
   +"<h2>5. Si el cliente menciona a Osiptel</h2><br>"
   +"Es importante informarle que este cambio de tarifa ha cumplido con todo el proceso normado por nuestro ente regulador.<br><br>"
         
   +"<h2>6. ¿Por cuánto tiempo es el incremento de precio?</h2><br>"
   +"Esa será su nueva tarifa El nuevo monto que pagará será a partir de la fecha " +fechaMensajeSpechLinea+ " hacia adelante.<br><br>"
   
   + "<h2>7. Yo hice un contrato por un precio menor, ustedes están incumpliendo el contrato, ¿por qué cambian los contratos sin consultar al cliente?</h2><br>"
   + "Hola " + Cliente.Nombre + " las condiciones de su servicio/ plan contratado no han sido modificadas. El cambio es a nivel del cargo mensual, que es permitido según las normas vigentes de nuestro regulador OSIPTEL.<br>"
   
   );
   $("#modal-IncrementoPrecioArgumentario").dialog("open");
   
};

IncrementoPrecio.IncrementoPrecio_Argumentario_Television = function ()
{
      $("#modal-IncrementoPrecioArgumentario").
   html(
   "<h1>Argumentarios</h1>"
   +"<div>El incremento de precio se debe a ajustes en nuestras tarifas"
   +" como parte de la revisión de nuestra oferta comercial y estructura de costos."
   +" Entre estos motivos se encuentran los siguientes:</div>"
   +"<table style='border-spacing: 10px;'>"
   +"<tr><td class='bullet verde'>Inflacion</td><td> La inflación de 2016 a la actualidad fue de 4.6% lo cual incrementa"
   +" las tarifas en general de todos los servicios del país. </td></tr>"
   +"<tr><td class='bullet amarillo'>Contenido</td><td> Mayor contenido de video (Movistar Play y más canales)</td></tr>"
   +"</table>"
   );
   $("#modal-IncrementoPrecioArgumentario").dialog("open");
   
};

/* SPEECH 1ERA LINEA RETENCIONES */
IncrementoPrecio.Argumentario_SpeechLinea = function (Cliente)
{   
   var fechaMensajeSpechLinea = Cliente.Mensajes.length>0 ?Cliente.Mensajes[0].Fecha:'';        
   var MontoAjuste = Cliente.Mensajes.length>0 ?Cliente.Mensajes[0].MontoIncremento:''; 
   var DescuentoNombre = Cliente.Herramienta.DescuentoRetencionesIncremento.length>0?Cliente.Herramienta.DescuentoRetencionesIncremento[0].Nombre:'';      
   var mensaje=Cliente.Mensajes[0];

   if(mensaje.Canales !== null){        
        /* Speech Solo Precios */
        $("#modal-SpeechLinea").
        html(
        "<i>Si cliente no acepto speech disuasivo</i><br>"
        + "<h1><u>Speech de Ajuste</u></h1>"
       /*  +"<h2><u>a. Factura No emitida:</u></h2><br>"
        +"Estimado (a) "+Cliente.Nombre +" podemos realizar un ajuste en tu próximo recibo por el monto de <b>"
        + MontoAjuste 
        +"</b> ¿Acepta la solución ofrecida? <br><br>" */

        +"<h2><u>a. Factura emitida:</u></h2><br>"
        +"Estimado (a) "+Cliente.Nombre +" podemos realizar el ajuste a su recibo por el monto de <b>"
        + MontoAjuste 
        +"</b> ¿Acepta la solución ofrecida? <br><br>"

        +"<ul><li> Si no acepta el speech de ajuste, ofrecer el siguiente speech.</li></ul>"

        + "<h1><u>Speech de Descuentos Retenciones</u></h1>"
        +"<h2><u>a. Factura No emitida:</u></h2><br>"
        +"Estimado (a) "+Cliente.Nombre +" podemos aplicar el descuento de <b>"
        + DescuentoNombre 
        +"</b> en sus 6 siguientes recibos. ¿Estaría de acuerdo? <br><br>"
        
        +"<h2><u>a. Factura emitida con el incremento:</u></h2><br>"
        +"Estimado (a) "+Cliente.Nombre +" podemos aplicar el descuento de <b>"
        + DescuentoNombre 
        +"</b> en sus 6 siguientes recibos.Es decir estaríamos ajustando su recibo actual y adicional los siguientes 6 recibos. En total, tendría 7 meses de descuento. ¿Estaría de acuerdo? <br><br>"

        );
        $("#modal-SpeechLinea").dialog("open");
        /* Fin Speech Solo Precio */

   }else{

    $("#modal-SpeechLinea").
   html(
   "<i>Si cliente no acepto speech disuasivo</i><br>"
   + "<h1><u>Speech de Ajuste</u></h1>"
  /*  +"<h2><u>a. Factura No emitida:</u></h2><br>"
   +"Estimado (a) "+Cliente.Nombre +" podemos realizar un ajuste en tu <b>próximo recibo</b> por el monto de <b>"
   + MontoAjuste +"</b> Si bien en ese "
   +" recibo no pagará el nuevo monto, ya empezará a disfrutar de todos los nuevos beneficios de su plan. ¿Acepta la solución ofrecida? <br><br>"
    */
   +"<h2><u>a. Factura emitida con el incremento:</u></h2><br>"
   +"Estimado (a) "+Cliente.Nombre +" podemos realizar el ajuste a su recibo por el monto de <b>"
   + MontoAjuste 
   +"</b> Si bien en este recibo no pagará el nuevo monto, ya estás disfrutando de los nuevos beneficios de tu plan. ¿Acepta la solución ofrecida? <br><br>"
   
   +"<ul><li> Si no acepta el speech de ajuste, ofrecer el siguiente speech.</li></ul>"
   
   + "<h1><u>Speech de Descuentos Retenciones</u></h1>"
   +"<h2><u>a. Factura No emitida:</u></h2><br>"
   +"Estimado (a) "+Cliente.Nombre +" podemos aplicar el descuento de <b>"
   + DescuentoNombre 
   +"</b> en sus 6 siguientes recibos. ¿Estaría de acuerdo? <br><br>"
      
   +"<h2><u>b. Factura emitida con el incremento:</u></h2><br>"
   +"Estimado (a) "+Cliente.Nombre +" podemos aplicar el descuento de <b>"
   + DescuentoNombre 
   +"</b> en sus 6 siguientes recibos.Es decir estaríamos ajustando su recibo actual y adicional los siguientes 6 recibos. En total, tendría 7 meses de descuento. ¿Estaría de acuerdo? <br><br>"
   
   );
   $("#modal-SpeechLinea").dialog("open");

   }

  
};

/* FIN SPEECH 1ERA LINEA RETENCIONES */

var CiudadSitiada = {};

CiudadSitiada.Consultar = function (Cliente){

$('#modal-CiudadSitiada').html("");
CiudadSitiada.Mostrar(Cliente);
};

CiudadSitiada.Mostrar = function(Cliente,Ofertas){

    var html = "";
    
    html += "<h1 style='border-radius: 4px; background-color:#FFEFEF; border: 1px solid red;color:red;padding: 6px;' >Ciudad Sitiada<br></h1>";
    html += "<br>";
    html += "<h1 style='color:red' >Cliente con nueva tecnología HFC, prioridad migración HFC<br></h1>";
    
    
    $('#modal-CiudadSitiada').html(html);
    $("#modal-CiudadSitiada").dialog("open");
};



var DescuentosRetenciones = {};

DescuentosRetenciones.Consultar = function (Cliente){

$('#modal-descuento-retenciones').html("");
DescuentosRetenciones.Mostrar(Cliente);
};

DescuentosRetenciones.Mostrar = function(Cliente){
    $('#modal-descuento-retenciones').html(
            DescuentosRetenciones.TablaDescuentosMantiene(Cliente.Herramienta.Gratuidad) +
            "<table width='100%' align='center'>"+
            "<tr><font color='red'><b>Bajan el ARPU:</b></font> <br></br> <b>1. Descuentos Comerciales:</b> <br></br></tr>"+
            "<tr width='100%' align='center'>"+
            HerramientasRetenciones.TablaTenenciaDescuentos(Cliente)+
            DescuentosRetenciones.TablaDescuentos(Cliente) +
            DescuentosRetenciones.TablaIncremento(Cliente) +
            //HerramientasRetenciones.TablaDescuentosBloquesTV(Cliente)+
            "</tr></table>"    +
            "<b>2. Baja de suplementario de TV</b>"
            );
    $("#modal-descuento-retenciones").dialog("open");
};

DescuentosRetenciones.TablaDescuentos = function(Cliente){
    
    var html = "<td>" ;
    var Descuentos = Cliente.Herramienta.DescuentoRetenciones;

    if(Descuentos.length === 0 ||  Cliente.Descuento.ComercialesCantidad > 1){
       html += "Este cliente no accede a descuentos Comerciales.";
    }else{
       html += "<center><table cellpadding='3' cellspacing='3'>" +  
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Descuento comerciales</b></td>" +
             "<td class='cabeceraParrilla'><b>P/S</b></td>" +
             "<td class='cabeceraParrilla'><b>Uso</b></td>" +            
             "</tr>";
        for (var x = 0; x < Descuentos.length; x++) {
           html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + Descuentos[x].Nombre + "</td>" +
             "<td class='cajaTexto'>" + Descuentos[x].Ps + "</td>" +
             "<td class='cajaTexto'>" + Descuentos[x].Descripcion + "</td>" +           
             "</tr>";
        }
        html += "</table></center>";
    }
    html += "</td>" ;
    return html;
};

DescuentosRetenciones.TablaIncremento = function(Cliente){
    
    var html = "<td>" ;
    var Descuentos = Cliente.Herramienta.DescuentoRetencionesIncremento;
    
    // Para obtener el porcentaje de descuento   
    var RentaIncrementado = 0;
    var montoAjustar = 0;
    if(DescuentosRetenciones.isset(Cliente.Mensajes[0])) {
        RentaIncrementado = Cliente.Mensajes[0].Renta.Destino;
        montoAjustar = Cliente.Mensajes[0].MontoIncremento;
    }
    
    // Para obtner el porcentaje de descuento

    if(Descuentos==undefined){
        html +="";// "Este cliente no accede a descuentos";
    }else if (Descuentos.length === 0 ||  Cliente.Descuento.ComercialesCantidad > 1 || Cliente.Paquete.Categoria === "Naked CMS") {
        html +=""; // "Este cliente no accede a descuentos";
    }else{
        html += "<center>"+
        "<b><u>Incremento de Precio</u></b>" + 
        "<table cellpadding='3' cellspacing='3'>" +            
         "<tr class='ProductoSVA'>" +
         "<td class='cabeceraParrilla'><b>Monto a ajustar</b></td>" +
         "<td class='cabeceraParrilla'><b>Dscto x Incremento</b></td>" +
         "<td class='cabeceraParrilla'><b>P/S</b></td>" +
        /*  "<td class='cabeceraParrilla'><b>Uso</b></td>" + */
         
         "</tr>";
        for (var x = 0; x < Descuentos.length; x++) {
            
            // procentaje    
            //var porcentaje;
            if((Descuentos[x].Ps=='PYP 69') || (Descuentos[x].Ps=='PYP 70') ){porcentaje=(parseFloat(RentaIncrementado*0.071)).toFixed(2);}
            else if((Descuentos[x].Ps=='PYP 71') || (Descuentos[x].Ps=='PYP 72')){porcentaje=(parseFloat(RentaIncrementado*0.06)).toFixed(2);}
            else{porcentaje='';}
            // porcentaje
            
            if(Descuentos[x].Descripcion!=='Regular'){Descuentos[x].Descripcion='Regular';}
            
           html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + montoAjustar  + "</td>" +
             "<td class='cajaTexto'>" + Descuentos[x].Nombre + "</td>" +
             "<td class='cajaTexto'>" + Descuentos[x].Ps + "</td>" +
             /* "<td class='cajaTexto'>" + Descuentos[x].Descripcion + "</td>" + */

             "</tr>";
        }
        html += "</table></center>";
    }
    html += "</td>" ;
    return html;
};

DescuentosRetenciones.isset = function(variable){
    if(typeof(variable) != "undefined") {  
        return true;
    }else{
        return false;
    }
};

DescuentosRetenciones.TablaDescuentosMantiene = function(Gratuidad){
    
    var i = 1;
    
    var html = "<font color='red'><b>Mantiene el ARPU:</b></font> <br></br> <b>" + i +". Argumentario:</b>" ;
        html += "<br><font style='background-color:yellow;'>   •    Resaltar los atributos del paquete actual que tiene el cliente.</font>";
        i+=1;
        
        
        for (var x = 0; x < Gratuidad.length; x++) {
           html +="<br></br> <b>" + i +". " +  Gratuidad[x] + "</b>";
           html +="<br><font style='background-color:yellow;'>   •    Cuando se haga uso de un Free view, no se podrá ofrecer un descuento.</font>";
           i+=1;
        }
        html +="<br></br> <b>" + i +". Baja de suplementario (Multidestino, Macfee, mantenimiento, etc).</b>";
        html += "<br></br>";
    return html;
};  
  
var HerramientasRetenciones = {};

HerramientasRetenciones.Consultar = function (Cliente){
   $('#modal-herramienta-retenciones').html("");
   HerramientasRetenciones.Mostrar(Cliente);
};

HerramientasRetenciones.Mostrar = function(Cliente){
    $('#modal-herramienta-retenciones').html(
            ""+
            HerramientasRetenciones.TablaCompensaciones(Cliente.Herramienta.Compensacion)+
            "<table width='100%' align='center'><tr width='100%' align='center'>"+
            HerramientasRetenciones.TablaTenenciaDescuentos(Cliente)+
            HerramientasRetenciones.TablaDescuentosComerciales(Cliente)+
            //
             DescuentosRetenciones.TablaIncremento(Cliente) +
            HerramientasRetenciones.TablaDescuentosBloquesTV(Cliente)+
            "</tr></table><tr>"+
            HerramientasRetenciones.TablaEquipamiento(Cliente.Herramienta.DecodificadorAdicional)+
            HerramientasRetenciones.TablaSolucion(Cliente.Herramienta.Solucion)+
            "</table>");
    $("#modal-herramienta-retenciones").dialog("open");
};
HerramientasRetenciones.TablaTenenciaDescuentos = function(Cliente){

       var  html =
             "<td></br><table cellpadding='3' cellspacing='3' >" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Tipo de Descuentos</b></td>" +
             "<td class='cabeceraParrilla'><b>Cantidad</b></td>" +
             "</tr>";
            html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'> Comerciales</td>" +
             "<td class='cajaTexto'> " + Cliente.Descuento.ComercialesCantidad + "</td>" +
             "</tr>";
            html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'> Bloques TV</td>" +
             "<td class='cajaTexto'> " + Cliente.Descuento.BloquesTVCantidad + "</td>" +
             "</tr>";

        html +="</table></br></td>";
        return html;
    
};

HerramientasRetenciones.TablaDescuentosComerciales = function(Cliente){
    if(Cliente.Herramienta.DescuentoRetenciones.length === 0 || Cliente.Descuento.ComercialesCantidad > 1){
       return "<td><b>Este cliente no accede a descuentos comerciales.</b></td>";
    }else{
       var html =
             "<td></br><table cellpadding='3' cellspacing='3' >" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Descuentos Comerciales</b></td>" +
             "<td class='cabeceraParrilla'><b>P/S</b></td>" +
             "<td class='cabeceraParrilla'><b>Uso</b></td>" +
             "</tr>";
        for (var x = 0; x < Cliente.Herramienta.DescuentoRetenciones.length; x++) {
           html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + Cliente.Herramienta.DescuentoRetenciones[x].Nombre + "</td>" +
             "<td class='cajaTexto'>" + Cliente.Herramienta.DescuentoRetenciones[x].Ps + "</td>" +
             "<td class='cajaTexto'>" + Cliente.Herramienta.DescuentoRetenciones[x].Descripcion + "</td>" +
             "</tr>";
        }
        html +="</table></br></td>";
        return html;
    }
};

HerramientasRetenciones.TablaDescuentosBloquesTV = function(Cliente){
    if(Cliente.Cable.Premium.Nombre !== '' && Cliente.Descuento.BloquesTVCantidad < 2 || Cliente.Paquete.ProductoId==7){        
       var  html =
             "<td></br><table cellpadding='3' cellspacing='3' >" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Descuento Bloque TV</b></td>" +
             "<td class='cabeceraParrilla'><b>P/S</b></td>" +
             "</tr>";
            html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'> S/. 10x6 meses</td>" +
             "<td class='cajaTexto'> 22438</td>" +
             "</tr>";
           /*  html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'> S/. 20x2 meses</td>" +
             "<td class='cajaTexto'> 22277</td>" +
             "</tr>";  */
        html +="</table></br></td>";
        return html;
    }
    else
        return "<td><b>Este cliente no accede a descuentos de Bloques de TV.</b></td>";
};

HerramientasRetenciones.TablaGratuidades = function(Gratuidad,BonoMovil){
    if(Gratuidad.length === 0 && BonoMovil.length === 0){
       return "";
    }else{
       var html = "<td><b>Gratuidades:</b></br>";
        for (var x = 0; x < Gratuidad.length; x++) {
           html +="•    " +  Gratuidad[x] + "</br>";
        }
        if( BonoMovil.length > 0){
        html +=
             "<table cellpadding='3' cellspacing='3'>" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>Bono Movil</b></td>" +
             "</tr>";
        for (var x = 0; x < BonoMovil.length; x++) {
           html +=
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + BonoMovil[x] + "</td>" +
             "</tr>";
        }
        html +="</table></br>";}
        
        
        html += "</td>";
        
        return html;
    }
};

HerramientasRetenciones.TablaCompensaciones = function(Compensacion){
    if(Compensacion.length === 0){
        return "";
     }else{
        var    html = "<table><tr><td><b>Solucion Tecnica Priorizada <font color='red'>[Solo Averias]</font>:<br></br>Speech:</b></br></td></tr>";
               html +="<tr align='center'><td>”Sr(a) vemos en el sistema que ha tenido inconvenientes técnicos. Permítame consultarle si este es el motivo de su decisión de baja?” <b>(Cliente confirma)";
               html += "<font color='#043F52'> Lamento el(los) inconveniente(s) que ha tenido, por ese motivo, queremos compensarlo con un ajuste en su recibo actual por un monto de:</b></br></font>   </td></tr>";
              // html += "<font color='red'><b>Queremos compensar</b> </font>los inconvenientes ocasionados con un descuento en su siguiente recibo por un monto de: </br></td></tr>" ;
               html += "<tr><td><table cellpadding='3' cellspacing='3' align = 'center'>" +
                 "<tr class='ProductoSVA'>" +
                 "<td class='cabeceraParrilla'><b>Monto a ajustar</b></td>" +
                 /* "<td class='cabeceraParrilla'><b>P/S</b></td>" + */
                 "</tr>";
         for (var x = 0; x < Compensacion.length; x++) {
            html +=
              "<tr class='ProductoSVA'>" +
              "<td class='cajaTexto'>" + Compensacion[x].NOMBRE + "</td>" +
              /* "<td class='cajaTexto'>" + Compensacion[x].PS + "</td>" + */
              "</tr>";
         }
         html += "</table></td></tr><tr><td><b>Así mismo permítame comunicarlo  con el área técnica a fin que puedan coordinar con Ud. Una visita y así solucionar definitivamente su problema.</br>" ;
         html += "<font style='background-color:yellow;'>Si se ofreció ajuste a la factura, ya no aplica ofrecer descuentos comerciales.</b></font><br></br></td></tr></table>";
         return html;
     }
};

HerramientasRetenciones.TablaEquipamiento = function(DecodificadorAdicional){
    if(DecodificadorAdicional === null){
       return "";
    }else{
       var html =
             "<tr><td><b>Equipamiento:</b><table cellpadding='3' cellspacing='3'  align='center'>" +
             "<tr class='ProductoSVA'>" +
             "<td class='cabeceraParrilla'><b>PS</b></td>" +
             "<td class='cabeceraParrilla'><b>Nombre</b></td>" +
             "<td class='cabeceraParrilla'><b>Operacion Comercial</b></td>" +
             "<td class='cabeceraParrilla'><b>Caracteristica</b></td>" +
             "</tr>" +
             "<tr class='ProductoSVA'>" +
             "<td class='cajaTexto'>" + DecodificadorAdicional.Ps.Ps + "</td>" +
             "<td class='cajaTexto'>" + DecodificadorAdicional.Ps.Nombre + "</td>" +
             "<td class='cajaTexto'>" + DecodificadorAdicional.TipoOperacionComercial + "</td>" +
             "<td class='cajaTexto'>" + DecodificadorAdicional.Caracteristica + "</td>" +
             "</tr>";
        
        html += "</table></br></td></tr>";
        return html;
    }
};

HerramientasRetenciones.TablaSolucion = function(Solucion){
    if(Solucion.length === 0){
       return "";
    }else{
        var html = "<tr><td><b>Promoción de velocidad:</b></br>";
        for (var x = 0; x < Solucion.length; x++) {
            html +=" " +  Solucion[x] + "</br>";
        }
        html += "</td></tr>";
        
        return html;
    }
};
