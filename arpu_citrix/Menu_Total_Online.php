<!DOCTYPE html>

<?php
//header('Location: Menu_Total.php');

require_once(dirname(__FILE__).'/Arpu/Autoload.php');

use Arpu\Autenticacion\Autenticacion;
Autenticacion::AsegurarSesionHtml('Location: LoginTotal.php');
$Usuario = Autenticacion::ObtenerUsuario();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Movistar Total</title>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link href="css/bootstrap_v2.css" rel="stylesheet">

        <link rel="stylesheet" href='css/fonts.css' type='text/css'> 
	<link rel="stylesheet" href="./css/font-awesome.css">
	 <link rel="stylesheet" href="./css/dash.css"> 
	<link rel="stylesheet" href="./css/menu.css">
<style>
  	

</style>    
  </head>

  <body>
   <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-telefonica fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">
		 <img src="./images/logo.png" alt="Acodjar" class="img-responsive img-center " width="150">
		</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
         <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li>
                <a class="nav-link" href='Online_LogoutTotal.php'>
                <span class="glyphicon glyphicon-log-in"></span><b> Cerrar Sesion</b></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>


<div class="contenido ">
        <div class="panel panel-default">

        <section class="section" >
            <br><br><br>
            <h2 class="text-center" style="font-weight: bold">Movistar Total</h2><br>
            <div class="section">
                <div class="full-width container-category">
                    <a href="MovistarTotal_Online.php" id="categori-13">
                        <!--<i class="fa fa-cart-plus" aria-hidden="true" aria-hidden="true"></i>-->
                        <span>VENTA MOVISTAR TOTAL</span>
                    </a>
                    <a href="EstadoPedido_Online.php" id="categori-13">
                        <!--<i class="fa fa-file-o" aria-hidden="true"></i>-->
                        <span>ESTADO DE PEDIDO</span>
                    </a>
                </div>
            </div>
             
        </section>
    	
    	</div>

    </div>

   <footer class="py-0 bg-telefonica fixed-bottom navbar-horizontal">
       <div class="container" style="text-align: center">
           <label style="color: #d3d3d3;" >Copyright &copy; Telefónica del Perú</label>
        </div>
      <!-- /.container -->
    </footer>

    <script src="scripts/jquery-1.8.2.js"></script>
    <script src="scripts/bootstrap.bundle.min.js"></script> 

  </body>

</html>