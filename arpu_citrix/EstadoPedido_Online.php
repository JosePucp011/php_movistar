<!DOCTYPE html>

<?php
//header('Location: EstadoPedido.php');

require_once(dirname(__FILE__).'/Arpu/Autoload.php');

use Arpu\Autenticacion\Autenticacion;
Autenticacion::AsegurarSesionHtml('Location: LoginTotal.php');

?>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="images/favicon.ico">

<title>Movistar Total</title>

<link rel="stylesheet" href='css/main_convergente.css' type='text/css'>
<link rel="stylesheet" href='css/fonts.css' type='text/css'>
<link rel="stylesheet" href='css/corporativo_convergente.css' type='text/css'>
<link rel="stylesheet" href='css/convergencia.css' type='text/css'>


<script src="scripts/jquery-1.8.2.js" type="text/javascript"></script>
<script><?php require 'scripts/convergencia/Util.js'; ?></script>
<script><?php require 'scripts/convergencia/Documento.js'; ?></script>
<script><?php require 'scripts/convergencia/EstadoPedido.js'; ?></script>
<script><?php require 'scripts/convergencia/GIS.js'; ?></script>
<script><?php require 'scripts/convergencia/maps.js'; ?></script>

<script>
    <?php
require 'scripts/convergencia/Comparacion.js';
require 'scripts/convergencia/Convergente.js';
require 'scripts/convergencia/ParrillaDibujar.js';
require 'scripts/convergencia/IncrementoPrecio.js';
require 'scripts/convergencia/CiudadSitiada.js';
require 'scripts/convergencia/Retenciones.js';
require 'scripts/convergencia/Freeview.js';
require 'scripts/convergencia/DatosCliente.js';
?></script>

<script>
$(function() {
    jQuery.fx.off = true;
    Documento.Inicializar();
});
</script>


</head>


<body style="background-color: white; color: rgba(0, 0, 0, 0.5); margin-top: 0px;" >
    <div>
        <table style="width: 90%; margin: auto; border-bottom: 1px solid rgba(0, 0, 0, 0.2)">
            <tr>
                <td>
                    <a href="#" style="margin-left: 10%"><img style="width: 150px; padding-top: 0.3125rem;padding-bottom: 0.3125rem;" src="images/logo.png"></a>
                </td>
                <td style="text-align: right;">
                 <a class="CerrarSesion" style="margin-right: 10%" href='Menu_Total_Online.php'>Menu</a>   
                 <a class="CerrarSesion" style="margin-right: 10%" href='Online_LogoutTotal.php'>Cerrar Sesi&oacute;n</a>
                </td>
            </tr>
        </table>
    </div>

    <br>
    <h1>Estado de Pedido</h1>

    <div id="consulta_usuario">
        <form id="Formulario_Documento">
            <fieldset>
                <legend>Consulta</legend>
                <table>
                    <tr>
                        <td  style='width: auto;'>
                            <label for='tipoDocumento'>Tipo Documento </label>
                            <select id='tipoDocumento'>
                                <option value='1'>DNI</option>
                                <option value='4'>RUC</option>
                                <option value='2'>CEX</option>
                            </select>

                            <label for='documento' style="margin-left: 10px;">Documento </label>
                            <input type='text' id='documento' style="margin-right: 10px;">
                        </td>
                        <td style="width: auto;  text-align: left; margin-right: 10px;">
                            <div id='error_consultaDocumento' class='error'></div>
                            <div id='nombre' style='font-weight:bold'></div>
                        </td>
                        <td  style="width: auto;  text-align: left; margin-right: 10px; color: rgb(0,176,240);">
                            <div id='movitar_total' style='font-weight:bold'></div>
                        </td>
                        <td  style="width: auto;  text-align: left; margin-right: 10px; color: red;">
                            <div id='Estado_Pedido' style='font-weight:bold'></div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
    
    <!--Consulta de Datos -->

    <form id="Formulario_Pedido" onsubmit="return false">
        <fieldset style="border: 1px solid #00b0f0;margin-bottom: 15px;display:none;" id="fieldset_NOTIFICACION">
            <legend>Notificación</legend>
            <p id="pedido_NOTIFICACION"></p>
        </fieldset>

        <fieldset style="margin-bottom: 15px;display:none;" id="fieldset_PEDIDO">
            <legend>Informacion de Pedido</legend>
            <br>

            <div id="InformacionPed" style="width: 100%">
                <div class="cabeceras">
                    <ul>
                        <li>Solicitud Movistar Total</li><!--
                        --><li>Pedido</li>
                    </ul>
                </div>
                <div class="contenido">
                    <ul class="cabeceraCont">
                        <li>Numero</li><!--
                        --><li>Petición u Order ID</li><!--
                        --><li>Solicitud</li><!--
                        --><li>Ciclo Facturación</li><!--
                        --><li>Estado</li><!--
                        --><li>Interacción</li><!--
                        --><li>Motivo</li><!--
                        --><li>Accion</li>
                    </ul>
                    <ul class="cont">
                        <li><p id="pedido_TELEFONO_FIJO"></p></li><!--
                        --><li><p id="pedido_CODIGO_PEDIDO_BACK"></p></li><!--
                        --><li><p id="pedido_PET_ITE_ALT_SIS"></p></li><!--
                        --><li><p id="pedido_CICLO_FIJA"></p></li><!--
                        --><li><p id="pedido_ESTADO_DE_GESTION_1_BACK"></p></li><!--
                        --><li><p id="pedido_FECHA_GESTION_BACK"></p></li><!--
                        --><li><p id="pedido_MOTIVO_FIJA"></p></li><!--
                        --><li><p id="pedido_ACCION_FIJA"></p></li>
                    </ul>
                    <ul class="cont">
                        <li><p id="pedido_TELEFONO_MOVIL"></p></li><!--
                        --><li><p id="pedido_OA_ORDER_ID"></p></li><!--
                        --><li><p id="pedido_O_APPLICATION_DATE"></p></li><!--
                        --><li><p id="pedido_CICLO_MOVIL"></p></li><!--
                        --><li><p id="pedido_ESTADO_ORDEN"></p></li><!--
                        --><li><p id="pedido_FECHA_GESTION_MOVIL"></p></li><!--
                        --><li><p id="pedido_MOTIVO_MOVIL"></p></li><!--
                        --><li><p id="pedido_ACCION_MOVIL"></p></li>
                    </ul>
                    <ul class="cont">
                        <li><p id="pedido_TELEFONO_MOVIL_1"></p></li><!--
                        --><li><p id="pedido_OA_ORDER_ID_1"></p></li><!--
                        --><li><p id="pedido_O_APPLICATION_DATE_1"></p></li><!--
                        --><li><p id="pedido_CICLO_MOVIL_1"></p></li><!--
                        --><li><p id="pedido_ESTADO_ORDEN_1"></p></li><!--
                        --><li><p id="pedido_FECHA_GESTION_MOVIL_1"></p></li><!--
                        --><li><p id="pedido_MOTIVO_MOVIL_1"></p></li><!--
                        --><li><p id="pedido_ACCION_MOVIL1"></p></li>
                    </ul>
                </div>
            </div>

            <br>
        </fieldset>

        <fieldset style="margin-bottom: 15px;display:none;" id="fieldset_CANCELACION">
            <legend>Información de cancelación</legend>

            <table style="width: 100%">
                <tr>
                    <h9 style="margin-left: 10px;">Acción Torre de control </h9>    
                    <textarea style="margin-left: 10px; width: 97%" disabled id='pedido_CANCELACION_ACCION'></textarea>    
                </tr>
                <tr>
                    <td><h9 style="margin-left: 10px;">Monto de Compensacion</h9><input type="text" id="pedido_CANCELACION_MONTO" style="margin-right: 10px;" disabled></td>  
                    <td><h9 style="margin-left: 10px;">Aplicación</h9><input type="text" id="pedido_CANCELACION_APLICACION" style="margin-right: 10px;" disabled></td>
                    <td><h9 style="margin-left: 10px;">Mes de Aplicacion</h9><input type="text" id="pedido_CANCELACION_MES" style="margin-right: 10px;" disabled></td>    
                </tr>    
            </table>        
            
        </fieldset>

        <fieldset style="display:none;" id="fieldset_NETFLIX">
            <legend>Información de Netflix</legend>
            <table style="width: 100%" id="table_NETFLIX">
                <tr>
                    <td><h9 style="margin-left: 10px;">Teléfono</h9><input type="text" id="netflix_TELEFONO" style="margin-right: 10px;" disabled></td>  
                    <td><h9 style="margin-left: 10px;">Fecha de Canje</h9><input type="text" id="netflix_FECHACANJE" style="margin-right: 10px;" disabled></td>
                    <td><h9 style="margin-left: 10px;">Meses gratis</h9><input type="text" id="netflix_MESES" style="margin-right: 10px;" disabled></td>
                    <td><h9 style="margin-left: 10px;">Facturado en</h9><input type="text" id="netflix_FACTURACION" style="margin-right: 10px;" disabled></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div style="margin-top: 12px;margin-left: 10px;"><b>* La cantidad de meses varía del tipo de plan afiliado a Netflix. Cliente debe revisar en netflix.com su "Cuenta".</b>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>

</body>
</html>