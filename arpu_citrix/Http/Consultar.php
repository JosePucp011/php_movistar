<?php
header('Content-type: application/json');

require_once(dirname(__FILE__).'/../Arpu/Autoload.php');

use Arpu\Services\OfertaSugerida;
use Arpu\Data\RegistroAccesoCalculadoraDL;
use Arpu\Util\Encoder;
use Arpu\Http\LectorParametros;


try
{
   $identificador = LectorParametros::LeerIdentificador();
   $resultado = OfertaSugerida::Obtener(
           $identificador->telefono, 
           $identificador->documento,
           LectorParametros::LeerRetenciones());
   
   RegistroAccesoCalculadoraDL::RegistrarConsulta($resultado);
   session_start();
   Encoder::utf8_encode_deep($resultado);
   echo json_encode($resultado);
}
catch(Exception $exception)
{
   die(json_encode((object)array('error' => "Cliente no encontrado"))); 
}

