<?php



require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Http\ConsultaConvergenteClass;

try {   
    $consulta = new ConsultaConvergenteClass();
    $resultado = $consulta->Procesar(false);
    $rest = new stdClass();
    $rest->Cliente = ObtenerCliente($resultado);
    $rest->Oferta1 = ObtenerOferta($resultado);
    
    echo json_encode($rest);
}catch (Exception $ex) {
    echo '{"error":"Cliente no aplica a ofertas"}';
}

function ObtenerOferta($resultado)
{
    $Oferta = new stdClass();
    
    $Oferta->Fijo = ObtenerOfertaFijo($resultado->Oferta1->Fijo);
    $Oferta->Movil1 = ObtenerOfertaMovil($resultado->Oferta1->Movil1);
    $Oferta->Movil2 = ObtenerOfertaMovil($resultado->Oferta1->Movil2);
    
    $Oferta->RentaTotal = $resultado->Oferta1->RentaTotal;
    $Oferta->PrecioRegular = $resultado->Oferta1->PrecioRegular;
    
    return $Oferta;
}
function ObtenerOfertaMovil($movil)
{
    $Movil = new stdClass();
    
    $Movil->Minutos = $movil->Minutos;
    $Movil->Datos = $movil->Datos;
    $Movil->Apps = $movil->Apps;
    
    return $Movil;
}
function ObtenerOfertaFijo($fijo)
{
    $Fijo = new stdClass();
    
    $Fijo->Linea = $fijo->Linea;
    $Fijo->Internet = $fijo->Internet;
    
    if($fijo->Modem === 'Mantiene'){
        $Fijo->Modem = '-';
    }else{
        $Fijo->Modem = $fijo->Modem;
    }
    
    if($fijo->Television != '-'){
        $Fijo->Television = 'Television '.$fijo->Television;
    }else{
        $Fijo->Television = $fijo->Television;
    }
    
    $Fijo->Deco = $fijo->Bloque;
    
    return $Fijo;
}
function ObtenerCliente($resultado)
{
    $Cliente = new stdClass();
    
    $Cliente->Nombre = $resultado->Cliente->Nombre;
    
    $Cliente->Fijo = ObtenerFijo($resultado->Cliente->Fijo);
    $Cliente->Movil1 = ObtenerMovil($resultado->Cliente->Movil1); 
    $Cliente->Movil2 = ObtenerMovil($resultado->Cliente->Movil2); 
    
    $Cliente->RentaTotal = $resultado->Cliente->RentaTotal;
            
    return $Cliente;
}

function ObtenerFijo($fijo)
{
    $Fijo = new stdClass();
    
    $Fijo->Titular = $fijo->TitularLanding;
    $Fijo->Linea = $fijo->Linea;
    $Fijo->Internet = $fijo->Internet;
    $Fijo->Modem = $fijo->Modem;
    
        if($fijo->Television != '-'){
        $Fijo->Television = 'Television '.$fijo->Television;
    }else{
        $Fijo->Television = $fijo->Television;
    }

    $Fijo->Deco = $fijo->Bloque;

    return $Fijo;
}

function ObtenerMovil($movil)
{
    $Movil = new stdClass();
    
    $Movil->Titular = $movil->TitularLanding;
    $Movil->Minutos = $movil->Minutos;
    $Movil->Datos = $movil->Datos;
    $Movil->Apps = $movil->Apps;

    return $Movil;    
}


