<?php

header('Content-type: application/json');
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
use Arpu\Config\Config;

use Arpu\Http\LectorParametros;

$conexion = Config::ConexionBD();

$codigoMovil = LectorParametros::LeerCodigoMovil();

$query = "select Paquete_Rango,MovilTotal_Renta,Paquete_Nombre,Paquete_CodigoMovil,Paquete_NombreMovil from catalogo_productos where Paquete_CodigoMovil = '".$codigoMovil."';";

$consulta = $conexion->query($query);

$resultado = new stdClass();

$Paquete_Rango = '';
$MovilTotal_Renta = 0;
$Paquete_Nombre = '';
$Paquete_CodigoMovil = '';
$Paquete_NombreMovil = '';

while( ($catalogo = $consulta->fetchObject())){
    if(isset($catalogo->Paquete_Rango)){
        $Paquete_Rango = $catalogo->Paquete_Rango;
        $MovilTotal_Renta = $catalogo->MovilTotal_Renta;
        $Paquete_Nombre = $catalogo->Paquete_Nombre;
        $Paquete_CodigoMovil = $catalogo->Paquete_CodigoMovil;
        $Paquete_NombreMovil = $catalogo->Paquete_NombreMovil;
    }
}

$resultado->Rango = $Paquete_Rango;
$resultado->MovilTotal_Renta = $MovilTotal_Renta;
$resultado->Nombre = $Paquete_Nombre;
$resultado->CodigoMovil = $Paquete_CodigoMovil;
$resultado->NombreMovil = $Paquete_NombreMovil;

echo json_encode($resultado);