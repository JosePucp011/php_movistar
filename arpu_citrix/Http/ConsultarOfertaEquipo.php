<?php

header('Content-type: application/json');
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
use Arpu\Config\Config;

use Arpu\Http\LectorParametros;

$conexion = Config::ConexionBD();

$TipoPago = LectorParametros::LeerTipoPago_Nuevo();
$rango =  LectorParametros::LeerRango();
$TipoCliente = LectorParametros::LeerTipoCliente();
$Modelo = LectorParametros::LeerModelo();
$CuotaInicial = LectorParametros::LeerCuotaInicial();
$CuotaMensualMaxima = LectorParametros::LeerCuotaMensualMaxima();
$PrecioFinanciado0 = LectorParametros::LeerPrecioFinanciado0();
$CuotaMes = LectorParametros::LeerCuotaMes();

$TipoFinanciamiento = LectorParametros::LeerTipoFinanciamiento();
$TipoClienteFinal = 'ALTA';

if($TipoFinanciamiento == '1'){
    $TipoClienteFinal = 'PORTA_ESPECIAL';
}
if($TipoFinanciamiento == '2'){
    $TipoClienteFinal = 'PORTA_ESPECIAL_CONTADO';
}
if($TipoFinanciamiento == '3'){
    $TipoClienteFinal = 'POS1';
}
if($TipoFinanciamiento == '4' && $TipoCliente != 'ALTA'){
    $TipoClienteFinal = 'POS2';
}

$Pago = 'Contado';
if($TipoPago == '5'){
    $Pago = 'Financiado_12';
}


$query = "select * from estrategia_tmp where TipoPago = '".$Pago."' and TipoCliente = '".$TipoClienteFinal."' and Modelo = '".$Modelo."' and  rango = '".$rango."';";


$consulta = $conexion->query($query);

$resultado = new stdClass();

$resultado->PrecioEquipo = '';
$resultado->MontoFinanciado = '';
$resultado->CuotaMensual = '';
$resultado->LCInsuficiente = 0;
$resultado->CuotaInicial = 0;

$Precio = 0;

while( ($equipo = $consulta->fetchObject())){
    if(isset($equipo->Precio)){
        $Precio = $equipo->Precio; 
    }
}

if($Precio != 0){
    $MontoFinanciado = 0;
    $CuotaMensual = 0;
    $CuotaInicialTemp = 0;
    
    //Calculando Cuota Inicial
    if($TipoPago == '3') {
        $CuotaInicialTemp = $Precio;
    } elseif($TipoPago == '1' || $TipoPago == '4' || $TipoPago == '5') {
        $CuotaInicialTemp = $PrecioFinanciado0;
    } elseif($TipoPago == '2') {
        $CuotaInicialTemp = round($Precio/2,0);
    }

    if($CuotaInicialTemp > $CuotaInicial) {
        $CuotaInicial = $CuotaInicialTemp;
    }

    if($CuotaInicial > $Precio) {
        $CuotaInicial = $Precio;
    }

    if($TipoPago == '2')
    {
        $MontoFinanciado = $Precio - $CuotaInicial;
    }elseif($TipoPago == '1' || $TipoPago == '4' || $TipoPago == '5'){
        $MontoFinanciado = $Precio - $CuotaInicial;
    }

    $blCorrecto = true;
    if($MontoFinanciado > 0){
        $CuotaMensual = round($MontoFinanciado/$CuotaMes,0);

        if($CuotaMensual > $CuotaMensualMaxima) {
            $blCorrecto = false;
        }
    }

    if($blCorrecto) {
        $resultado->PrecioEquipo = $Precio;
        $resultado->CuotaMensual = $CuotaMensual;
        $resultado->MontoFinanciado = $MontoFinanciado;
        $resultado->CuotaInicial = $CuotaInicial;
    } else {
        $resultado->PrecioEquipo = 'No Aplica';
        $resultado->LCInsuficiente = 1;
    }

    /*$resultado->PrecioEquipo = $Precio;
    if($TipoPago === 'F50')
    {
        $resultado->MontoFinanciado = round($Precio/2,0) - $CuotaInicial;
    }elseif($TipoPago === 'F0'){
        $resultado->MontoFinanciado = $Precio - $CuotaInicial;
    }else{
        $resultado->MontoFinanciado = 0;
    }

    if($resultado->MontoFinanciado > 0){
        $resultado->CuotaMensual = round($resultado->MontoFinanciado /12,0);
    }else{
        $resultado->CuotaMensual = 0;
    } */
}else{
    $resultado->PrecioEquipo = 'No Aplica';
}

echo json_encode($resultado);