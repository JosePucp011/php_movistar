<?php

header('Content-type: application/json');
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
use Arpu\Services\OfertaSugeridaApi;
use Arpu\Entity\ClienteNoEncontrado;
use Arpu\Http\LectorParametros;
use Arpu\Util\Encoder;


function ObtenerTelefono($cliente){
    if ($cliente->Telefono < 0) {
        return '';
    } else {
        return $cliente->Telefono;
    }
}

function ObtenerProductoOrigen($cliente){
    $productoOrigen = new stdClass();
    if($cliente->Paquete->Ps > 0){
        $productoOrigen->PaquetePsOrigen = $cliente->Paquete->Ps;
        $productoOrigen->PaqueteOrigen = $cliente->Paquete->Nombre;
    } elseif($cliente->Telefono < 0) {
        $productoOrigen->PaquetePsOrigen = 'MSS';
        $productoOrigen->PaqueteOrigen = ('Television ' . $cliente->Cable->Tipo);
    } else {
        $productoOrigen->PaquetePsOrigen = 'MLN';
        $productoOrigen->PaqueteOrigen = $cliente->Linea->Nombre;
    }
    return $productoOrigen;
}

function ObtenerProductoDestino($producto,$cliente){
    $ProductoDestinoPs = '';
    $SVAs = '';
    if ($producto->Paquete->EsSVA === 1) {
        $ProductoDestinoPs = '';
        $SVAs = $producto->Paquete->Ps;
    } else {
        $ProductoDestinoPs = $producto->Paquete->Ps;
        $SVAs = '';
        if($cliente->Telefono == '15962120' && 
           $producto->Paquete->Nombre == 'Trio Movistar Voz 20Mbps Estandar Digital + Smart Wifi') {
            $SVAs = '23025';
        }

        foreach ($producto->Paquete->PsAdicionales as $PsAdicionales) {
            $SVAs = $SVAs . $PsAdicionales;
        }
    }
    
    $productoDestino = new stdClass();
    $productoDestino->ProductoDestinoPs = $ProductoDestinoPs;
    $productoDestino->SVAs = $SVAs;
    return $productoDestino;
}

function ObtenerObjetoOferta($cliente,$opcion,$prioridad){
    $objeto = new stdClass();
    $objeto->Tipo = $opcion->producto->Paquete->Tipo;
    $objeto->Items= array();
    return $objeto;
}

function ObtenerOfertaPrincipal(){
    $objetoPrincipal= new stdClass();
    $objetoPrincipal->Offer= array();
    return $objetoPrincipal;
}

function ObtenerOfertaSecundario($cliente,$opcion,$prioridad){
    $objetoSecundario= new stdClass();
    $objetoSecundario->name= $opcion->producto->Paquete->Tipo;
    $objetoSecundario->items= array();
    return $objetoSecundario;
}
/* ORIGEN */
function ObtenerObjetoOfertaTelefono($cliente,$opcion,$prioridad){
    $telefono = new stdClass();
    $telefono->key = "Telefono";
    $telefono->value = ObtenerTelefono($cliente);
    return $telefono;
}

function ObtenerObjetoOfertaServicio($cliente,$opcion,$prioridad){
    $servicio = new stdClass();
    $servicio->key = "Servicio";
    $servicio->value = $cliente->Cable->Servicio;    
    return $servicio;
}

function ObtenerObjetoOfertaPaquetePsOrigen($cliente,$opcion,$prioridad){
    $PaquetePsOrigen= new stdClass();
    $PaquetePsOrigen->key = "PaquetePsOrigen";
    $productoOrigen = ObtenerProductoOrigen($cliente);
    $PaquetePsOrigen->value = $productoOrigen->PaquetePsOrigen;    
    return $PaquetePsOrigen;
}

function ObtenerObjetoOfertaPaqueteOrigen($cliente,$opcion,$prioridad){
    $PaqueteOrigen= new stdClass();
    $PaqueteOrigen->key = "PaqueteOrigen";
    $productoOrigen = ObtenerProductoOrigen($cliente);
    $PaqueteOrigen->value = $productoOrigen->PaqueteOrigen;    
    return $PaqueteOrigen;
}

function ObtenerObjetoOfertaCantidadDecos($cliente,$opcion,$prioridad){
    $CantidadDecos= new stdClass();
    $CantidadDecos->key = "CantidadDecos";   
    $CantidadDecos->value = $cliente->Cable->CantidadDeDecos;    
    return $CantidadDecos;
}

function ObtenerObjetoOfertaRentaTotal($cliente,$opcion,$prioridad){
    $RentaTotal= new stdClass();
    $RentaTotal->key = "RentaTotal";   
    $RentaTotal->value =$cliente->RentaTotal;    
    return $RentaTotal;
}

function ObtenerObjetoOfertaInternet($cliente,$opcion,$prioridad){
    $Internet= new stdClass();
    $Internet->key = "Internet";   
    $Internet->value =$cliente->Internet->Velocidad;    
    return $Internet;
}

function ObtenerObjetoOfertaPrioridad($cliente,$opcion,$prioridad){
    $Prioridad= new stdClass();
    $Prioridad->key = "Prioridad";   
    $Prioridad->value =$prioridad;    
    return $Prioridad;
}
/* FIN ORIGEN */

/* DESTINO */
function ObtenerObjetoOfertaProductoDestinoPs($cliente,$opcion,$prioridad){
    $ProductoDestinoPs= new stdClass();
    $ProductoDestinoPs->key = "ProductoDestinoPs";
    $productoDestino = ObtenerProductoDestino($opcion->producto,$cliente);
    $ProductoDestinoPs->value = (int)$productoDestino->ProductoDestinoPs;    
    return $ProductoDestinoPs;
}

function ObtenerObjetoOfertaProductoDestinoNombre($cliente,$opcion,$prioridad){
    $ProductoDestinoNombre= new stdClass();
    $ProductoDestinoNombre->key = "ProductoDestinoNombre";
    $productoDestino = ObtenerProductoDestino($opcion->producto,$cliente);
    $ProductoDestinoNombre->value = $opcion->producto->Paquete->Nombre;    
    return $ProductoDestinoNombre;
}

function ObtenerObjetoOfertaSVAs($cliente,$opcion,$prioridad){
    $SVAs= new stdClass();
    $SVAs->key = "SVAs";
    $productoDestino = ObtenerProductoDestino($opcion->producto,$cliente);
    $SVAs->value = "null"; //$productoDestino->SVAs    
    return $SVAs;
}

function ObtenerObjetoOfertaProductoDestinoRenta($cliente,$opcion,$prioridad){
    $ProductoDestinoRenta= new stdClass();
    $ProductoDestinoRenta->key = "ProductoDestinoRenta";    
    $ProductoDestinoRenta->value =  $opcion->producto->Paquete->Renta;    
    return $ProductoDestinoRenta;
}

function ObtenerObjetoOfertaMontoFinanciando($cliente,$opcion,$prioridad){
    $MontoFinanciando= new stdClass();
    $MontoFinanciando->key = "MontoFinanciando";    
    $MontoFinanciando->value =  $opcion->producto->Internet->Tecnologia;    
    return $MontoFinanciando;
}

function ObtenerObjetoOfertaCuotasFinanciado($cliente,$opcion,$prioridad){
    $CuotasFinanciado= new stdClass();
    $CuotasFinanciado->key = "CuotasFinanciado";    
    $CuotasFinanciado->value =  "null";    
    return $CuotasFinanciado;
}

function ObtenerObjetoOfertaCodigoOferta($cliente,$opcion,$prioridad){
    $CodigoOferta= new stdClass();
    $CodigoOferta->key = "CodigoOferta";    
    $CodigoOferta->value = $opcion->codigoOperacionComercial;   
    return $CodigoOferta;
}
/* FIN DESTINO */
function ProcesarOferta($oferta) {
    $ID = 1;       
    $arreglos=array();
    $objetoPrincipal=ObtenerOfertaPrincipal(); 

    foreach ($oferta->Opciones as $key => $opcion) {
        
        $objeto = ObtenerObjetoOferta($oferta->Cliente,$opcion,$key);
        $objetoSecundario = ObtenerOfertaSecundario($oferta->Cliente,$opcion,$key);
        // Origen
        $telefono = ObtenerObjetoOfertaTelefono($oferta->Cliente,$opcion,$key);
        $servicio = ObtenerObjetoOfertaServicio($oferta->Cliente,$opcion,$key);
        $PaquetePsOrigen = ObtenerObjetoOfertaPaquetePsOrigen($oferta->Cliente,$opcion,$key);
        $PaqueteOrigen=ObtenerObjetoOfertaPaqueteOrigen($oferta->Cliente,$opcion,$key);
        $CantidadDecos=ObtenerObjetoOfertaCantidadDecos($oferta->Cliente,$opcion,$key);
        $RentaTotal=ObtenerObjetoOfertaRentaTotal($oferta->Cliente,$opcion,$key);
        $Internet=ObtenerObjetoOfertaInternet($oferta->Cliente,$opcion,$key);
        $Prioridad=ObtenerObjetoOfertaPrioridad($oferta->Cliente,$opcion,$key);
        // Destino
        $ProductoDestinoPs=ObtenerObjetoOfertaProductoDestinoPs($oferta->Cliente,$opcion,$key);      
        $ProductoDestinoNombre=ObtenerObjetoOfertaProductoDestinoNombre($oferta->Cliente,$opcion,$key);
        $SVAs=ObtenerObjetoOfertaSVAs($oferta->Cliente,$opcion,$key);
        $ProductoDestinoRenta=ObtenerObjetoOfertaProductoDestinoRenta($oferta->Cliente,$opcion,$key);
        $MontoFinanciando=ObtenerObjetoOfertaMontoFinanciando($oferta->Cliente,$opcion,$key);
        $CuotasFinanciado=ObtenerObjetoOfertaCuotasFinanciado($oferta->Cliente,$opcion,$key);
        $CodigoOferta=ObtenerObjetoOfertaCodigoOferta($oferta->Cliente,$opcion,$key);
        // Origen
        $objetoSecundario->items[]=$telefono;        
        $objetoSecundario->items[]=$servicio;        
        $objetoSecundario->items[]=$PaquetePsOrigen;        
        $objetoSecundario->items[]=$PaqueteOrigen;        
        $objetoSecundario->items[]=$CantidadDecos; 
        $objetoSecundario->items[]=$RentaTotal; 
        $objetoSecundario->items[]=$Internet; 
        $objetoSecundario->items[]=$Prioridad; 
        // Destino
        $objetoSecundario->items[]=$ProductoDestinoPs;        
        $objetoSecundario->items[]=$ProductoDestinoNombre;        
        $objetoSecundario->items[]=$SVAs;        
        $objetoSecundario->items[]=$ProductoDestinoRenta; 
        $objetoSecundario->items[]=$MontoFinanciando; 
        $objetoSecundario->items[]=$CuotasFinanciado; 
        $objetoSecundario->items[]=$CodigoOferta; 
        // Principal
        $objetoPrincipal->Offer[]= $objetoSecundario;
       
        ++$ID;
    }

    foreach ($oferta->Ofertas as $key => $opcion) {
        if ($opcion->mejor_up === null) {
            
        $objeto = ObtenerObjetoOferta($oferta->Cliente,$opcion,$ID);
        $objetoSecundario = ObtenerOfertaSecundario($oferta->Cliente,$opcion,$ID);
        // Origen
        $telefono = ObtenerObjetoOfertaTelefono($oferta->Cliente,$opcion,$ID);
        $servicio = ObtenerObjetoOfertaServicio($oferta->Cliente,$opcion,$ID);
        $PaquetePsOrigen = ObtenerObjetoOfertaPaquetePsOrigen($oferta->Cliente,$opcion,$ID);
        $PaqueteOrigen=ObtenerObjetoOfertaPaqueteOrigen($oferta->Cliente,$opcion,$ID);
        $CantidadDecos=ObtenerObjetoOfertaCantidadDecos($oferta->Cliente,$opcion,$ID);
        $RentaTotal=ObtenerObjetoOfertaRentaTotal($oferta->Cliente,$opcion,$ID);
        $Internet=ObtenerObjetoOfertaInternet($oferta->Cliente,$opcion,$ID);
        $Prioridad=ObtenerObjetoOfertaPrioridad($oferta->Cliente,$opcion,$ID);
        // Destino
        $ProductoDestinoPs=ObtenerObjetoOfertaProductoDestinoPs($oferta->Cliente,$opcion,$ID);      
        $ProductoDestinoNombre=ObtenerObjetoOfertaProductoDestinoNombre($oferta->Cliente,$opcion,$ID);
        $SVAs=ObtenerObjetoOfertaSVAs($oferta->Cliente,$opcion,$ID);
        $ProductoDestinoRenta=ObtenerObjetoOfertaProductoDestinoRenta($oferta->Cliente,$opcion,$ID);
        $MontoFinanciando=ObtenerObjetoOfertaMontoFinanciando($oferta->Cliente,$opcion,$ID);
        $CuotasFinanciado=ObtenerObjetoOfertaCuotasFinanciado($oferta->Cliente,$opcion,$ID);
        $CodigoOferta=ObtenerObjetoOfertaCodigoOferta($oferta->Cliente,$opcion,$ID);
        // Origen
        $objetoSecundario->items[]=$telefono;        
        $objetoSecundario->items[]=$servicio;        
        $objetoSecundario->items[]=$PaquetePsOrigen;        
        $objetoSecundario->items[]=$PaqueteOrigen;        
        $objetoSecundario->items[]=$CantidadDecos; 
        $objetoSecundario->items[]=$RentaTotal; 
        $objetoSecundario->items[]=$Internet; 
        $objetoSecundario->items[]=$Prioridad; 
        // Destino
        $objetoSecundario->items[]=$ProductoDestinoPs;        
        $objetoSecundario->items[]=$ProductoDestinoNombre;        
        $objetoSecundario->items[]=$SVAs;        
        $objetoSecundario->items[]=$ProductoDestinoRenta; 
        $objetoSecundario->items[]=$MontoFinanciando; 
        $objetoSecundario->items[]=$CuotasFinanciado; 
        $objetoSecundario->items[]=$CodigoOferta; 
        // Principal
        $objetoPrincipal->Offer[]= $objetoSecundario;

            ++$ID;
        }
    }
    
   // $arreglos[]=$objetoPrincipal;
    return $objetoPrincipal;
}


try
{	
    if(isset($_GET["phone"])){     
        $campo='phone';
        $Phone=LectorParametros::LeerPhone();
    }else if(isset($_GET["documentNumber"]) or isset($_GET["documentType"]) ){       
        $campo='documentType';
        $DocumentType=LectorParametros::LeerDocumentType(); 
        $campo='documentNumber'; 
        $DocumentNumber=LectorParametros::LeerDocumentNumber();           
    }else{
        $campo='Field Do not exist';
    }

    $var_phone='';
    $var_documentNumber='';
    $var_documentType='';

    if(isset($Phone)){
        $var_phone=$Phone->phone; 
    }
    if(isset($DocumentNumber)){
        $var_documentNumber=$DocumentNumber->documentNumber;        
    }
    if(isset($DocumentType)){
        $var_documentType=$DocumentType; 
    }
  
   $oferta = OfertaSugeridaApi::Obtener(
            $var_phone, 
            $var_documentType,
            $var_documentNumber,
           'App',
           'BandaAncha');   
   $resultado = ProcesarOferta($oferta);
   Encoder::utf8_encode_deep($resultado);
   echo json_encode($resultado);
}
catch(InvalidArgumentException $excepcion)
{   $d='';
    if(isset($campo) and !empty($campo)){
        $d=$campo;
    }

    $z='Missing mandatory parameter '.$d;
    header("HTTP/1.1 400 Bad Request");
    die(json_encode((object)array('Exception'=>array('exceptionId' => 'SVC1001',
    'exceptionText'=>'Missing Mandatory Parameter',
    'moreInfo'=>'API Request without mandatory field',
    'userMessage'=>$z)                                         
                                     )));
   
}
catch (ClienteNoEncontrado $excepcion)
{   
    $x=$var_phone;
    $y=$var_documentNumber;
    $z='';
    if(!empty($x)){
        $z=$x;
    }elseif(!empty($y)){
        $z=$y;
    } else{
        $z='';
    }

    $a='Resource';
    $b=$z;
    $c='does not exist';
    $d=$a." ".$b." ".$c;

    header("HTTP/1.1 404 Not Found");
    die(json_encode((object)array('Exception'=>array('exceptionId' => 'SVC1006',
    'exceptionText'=>'Missing Mandatory Parameter',
    'moreInfo'=>'Not existing Resource Id',
    'userMessage'=>$d)                                         
                                     )));
    

    
}
catch(Exception $excecion)
{
   die(json_encode((object) array('error' => 'Ocurrio error Interno')));
}

