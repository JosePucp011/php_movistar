<?php



require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Autenticacion\Autenticacion;
Autenticacion::AsegurarSesionJson();
use Arpu\Http\ConsultaConvergenteClass;


$log = new Arpu\Data\LoggearTransaccion();


try {   
    $consulta = new ConsultaConvergenteClass();
    $resultado = $consulta->Procesar();
    Arpu\Util\Encoder::utf8_encode_deep($resultado);
    
    $resultado->CodigoConsulta = $log->getId();
    $json = json_encode($resultado);
    $log->setearResultado($json);
    echo $json;
}catch(Exception $excepcion){
    $resultado = (object)['error' => 'Cliente no aplica a oferta','errorInterno' => $excepcion->getMessage()];
    $resultado->CodigoConsulta = $log->getId();
    $json = json_encode($resultado);
    $log->setearResultado($json);
    echo $json;
}
$log->log();