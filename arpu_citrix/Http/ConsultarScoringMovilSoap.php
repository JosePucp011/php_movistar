<?php


require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Autenticacion\Autenticacion;
use Arpu\Http\LectorParametros;
use Arpu\ServiciosIn\ScoringMovilSoap;

Autenticacion::AsegurarSesionJson();

$operadores = [
    '20' => 'Entel',
    '21' => 'America Movil Peru SAC (Claro)',
    '23' => 'Winner Systems',
    '24' => 'Viettel',
    '64' => 'Operador virtual'
];

try {
    
    //No Cambiar este orden de construccion
    $consulta = new stdClass();
    
    $portabilidad = LectorParametros::LeerPortabilidad(true);
            
    
    $consulta->tipDocumento = LectorParametros::LeerTipoDocumento();
    $consulta->numDocumento = LectorParametros::LeerDocumento();
    $consulta->sisOrigen = "APPCalculadoraMT";
    $consulta->modDecisional= "MV";
    $consulta->tipMoneda= "1";
    $consulta->tlfPortado= "";
    $consulta->idTipSustento= "";
    $consulta->portabilidad= "";
    $consulta->operador= "";
    $consulta->nombreOperador= "";
    $consulta->fechaAlta= "";
    $consulta->codigoSegmento= "";
    $consulta->tipoAccion= LectorParametros::LeerTipoAccionScoring();
    $consulta->msisdn= LectorParametros::LeerMovilScoring();
    $consulta->sumaCargoRecurrente= round(LectorParametros::LeerRentaScoring()/1.18,2);
    
    
    if($portabilidad === '1'){
        
        $consulta->tlfPortado= LectorParametros::LeerMovilScoring();
        $consulta->portabilidad = $portabilidad;
        $consulta->operador = LectorParametros::LeerOperador();
        
        
        if(!isset($operadores[$consulta->operador])){
          throw new Arpu\Exception\ErrorProgramacionInterno();  
        } 
        
        $consulta->nombreOperador = $operadores[$consulta->operador];
        $consulta->fechaAlta = LectorParametros::LeerFechaAlta();
        $consulta->codigoSegmento = LectorParametros::LeerSegmentoMovil();
        $consulta->msisdn= "";
        
    }
        
    $scoringMovil = ScoringMovilSoap::Consultar($consulta);
    echo json_encode(ScoringMovilSoap::Consultar($consulta));       

    //ErrorConsultaServicioREST
    //DocumentoNoEnviado
    //DocumentoInvalido
} catch (Exception $ex) {
    echo ScoringMovilSoap::RESPUESTA_POR_DEFECTO.'"errorInterno": "'.$ex->getMessage().'"}';
}

