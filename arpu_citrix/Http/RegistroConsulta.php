<?php

header('Content-type: application/json');
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');

use Arpu\Data\RegistroAccesoCalculadoraDL;
use Arpu\Http\LectorParametros;

try
{
   $identificador = LectorParametros::LeerIdentificador();
   $modo = LectorParametros::LeerRetenciones();
   $ps = LectorParametros::LeerPs();
   $accion = LectorParametros::LeerAccionAsesor();

   $response = RegistroAccesoCalculadoraDL::insertarRegistroDeAcceso(
                   $identificador->telefono, $ps, $modo, $accion
   );
} catch (Exception $e)
{
   echo $e->getMessage();
}
