<?php
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Http\ConsultarDocumento;

function DocumentoNoEncontrado(){
    echo '{"error":"Documento no encontrado"}';
    exit(0);
}


function IntentarConseguirMenosTres($moviles){
    $movilesPrepago = [];
    $movilesPostpago = [];
    
    foreach($moviles as $movil){
        if($movil->Movil0->TipoLinea === 'Prepago'){
            $movilesPrepago[] = $movil;
        } else {
            $movilesPostpago[] = $movil;
        }
    }
    
    
    
    
    if(count($movilesPostpago) >= 3){
        
        DocumentoNoEncontrado();
    } elseif(count($movilesPostpago) > 0) {
        
        return $movilesPostpago;
    } else {
        DocumentoNoEncontrado();
    }
    
     
    
    
    
}


$resultado = ConsultarDocumento::Procesar();

if (isset($resultado->error)) {
    DocumentoNoEncontrado();
}

if($resultado->Hogar->Presente){
    DocumentoNoEncontrado();
}


$conteoFijos = 0;
$nuevoArregloFijos = [];
foreach($resultado->Fijos as $fijo){
    if($fijo->Valido){
        ++$conteoFijos;
        $nuevoArregloFijos[] = $fijo;
    }
}
$resultado->Fijos = $nuevoArregloFijos;

/*Si el cliente tiene mas de dos fijos validos*/
if($conteoFijos >= 2){
    DocumentoNoEncontrado();
}

foreach($resultado->Moviles as $movil){
    if(!$movil->Valido){
        DocumentoNoEncontrado();
    }
}
    



if(count($resultado->Moviles) >= 3){
    
    $resultado->Moviles = IntentarConseguirMenosTres($resultado->Moviles);
}



$response = new stdClass();
$response->Fijo = ObtenerFijo($resultado);
$response->Movil1 = ObtenerMovil($resultado,0);
$response->Movil2 = ObtenerMovil($resultado,1);
echo json_encode($response);


function ObtenerFijo($resultado)
{
    $Fijo = new stdClass();
            
    if(isset($resultado->Fijos[0])){
        $fijoInterno = $resultado->Fijos[0];
        $Fijo->Presente = $fijoInterno->Presente;
        $Fijo->Numero = $fijoInterno->Numero;
        $Fijo->Titulo = $fijoInterno->Titulo;
        $Fijo->Subtitulo = $fijoInterno->Subtitulo;         
    }
    else{
        $Fijo->Presente = 0; 
    }
    
    return $Fijo;
}

function ObtenerMovil($resultado,$indice)
{
    $Movil_ = new stdClass();
    
    if(isset($resultado->Moviles[$indice])){
        $movilInterno = $resultado->Moviles[$indice];
        $Movil_->Presente = $movilInterno->Presente;
        $Movil_->Numero = $movilInterno->Numero;
        $Movil_->Titulo = $movilInterno->Titulo;
        $Movil_->Subtitulo = $movilInterno->Subtitulo;             
    } else {
        $Movil_->Presente = 0;
    }
    
    return $Movil_;
}





if(0){
header('Content-type: application/json');
echo <<<HERE
{
    "Fijo": {
        "Presente": 0
    },
    "Movil1": {
        
        "Presente": 1,
		"Numero": 976580820,
        "Titulo": "Linea Movil Abierta",
        "Subtitulo": "Plan Internet Total S/. 95"
    },
    "Movil2": {
        "Presente": 0
    }
}

HERE;


exit(0);

}