<?php
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Autenticacion\Autenticacion;
Autenticacion::AsegurarSesionJson();

$log = new Arpu\Data\LoggearTransaccion();


use Arpu\Http\LectorParametros;
use Arpu\ServiciosIn\GIS;

try {
    $x = LectorParametros::LeerCoordenada('x',false);
    $y = LectorParametros::LeerCoordenada('y',false);
    $resultado = GIS::Ejecutar($x, $y);
    $json = json_encode($resultado);
    $log->setearResultado($json);
    echo $json;
} catch (\Exception $excepcion) {
    
    $json = GIS::RESPUESTA_POR_DEFECTO.'"errorInterno": "'.$excepcion->getMessage().'"}';
    $log->setearResultado($json);
    echo $json;
}

$log->log();
