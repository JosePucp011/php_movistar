<?php
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Config\Config;

class RegistrarOferta{

    public static function insertarRegistroOferta($Asesor_Usuario = '',	$Asesor_Nombre = '',	$Asesor_DNI = '',	$Cliente_Nombre = '',	$Cliente_TipoDocumento = '',	$Cliente_Documento = '',	$Fijo_Numero = '',	$Fijo_Tenencia = '',	$Fijo_OperacionComercial = '',	$Movil1_Numero = '',	$Movil1_Tenencia = '',	$Movil1_OperacionComercial = '',	$Movil2_Numero = '',	$Movil2_Tenencia = '',	$Movil2_OperacionComercial = '',	$Oferta_MT = '',	$Salto = '',	$Linea_de_Contacto = '',	$Correo = '',	$Ubicacion = '',	$Direccion = '',	$CoordenadaX = '',	$CoordenadaY = '',    $OperacionComercial='',    $CodigoScoringFijo = '',    $CodigoScoringMovil1 = '',    $CodigoScoringMovil2 = '')
    {
        $conexion = Config::ConexionBD();
        
        $InsertaRegistro = $conexion->prepare("INSERT INTO registropedido(Asesor_Usuario,	Asesor_Nombre,	Asesor_DNI,	Fecha_Solicitud,	Cliente_Nombre,	Cliente_TipoDocumento,	Cliente_Documento,	Fijo_Numero,	Fijo_Tenencia,	Fijo_OperacionComercial,	Movil1_Numero,	Movil1_Tenencia,	Movil1_OperacionComercial,	Movil2_Numero,	Movil2_Tenencia,	Movil2_OperacionComercial,	Oferta_MT,	Salto,	Linea_de_Contacto,	Correo,	Ubicacion,	Direccion, Coordenada_X, Coordenada_Y,  MT_OperacionComercial,  Codigo_ScoringFijo,  Codigo_ScoringMovil1, Codigo_ScoringMovil2)
                                                    VALUES (?,	?,	?,	current_timestamp,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,  ?,  ?,  ?,  ?);");
      
        $InsertaRegistro->bindValue(1,RegistrarOferta::validarCaracteres($Asesor_Usuario));
        $InsertaRegistro->bindValue(2,RegistrarOferta::validarCaracteres($Asesor_Nombre));
        $InsertaRegistro->bindValue(3,RegistrarOferta::validarCaracteres($Asesor_DNI));
        $InsertaRegistro->bindValue(4,RegistrarOferta::validarCaracteres($Cliente_Nombre));
        $InsertaRegistro->bindValue(5,$Cliente_TipoDocumento);
        $InsertaRegistro->bindValue(6,RegistrarOferta::validarCaracteres($Cliente_Documento));
        $InsertaRegistro->bindValue(7,RegistrarOferta::validarCaracteres($Fijo_Numero));
        $InsertaRegistro->bindValue(8,$Fijo_Tenencia);
        $InsertaRegistro->bindValue(9,$Fijo_OperacionComercial);
        $InsertaRegistro->bindValue(10,$Movil1_Numero);
        $InsertaRegistro->bindValue(11,$Movil1_Tenencia);
        $InsertaRegistro->bindValue(12,$Movil1_OperacionComercial);
        $InsertaRegistro->bindValue(13,$Movil2_Numero);
        $InsertaRegistro->bindValue(14,$Movil2_Tenencia);
        $InsertaRegistro->bindValue(15,$Movil2_OperacionComercial);
        $InsertaRegistro->bindValue(16,$Oferta_MT);
        $InsertaRegistro->bindValue(17,$Salto);
        $InsertaRegistro->bindValue(18,RegistrarOferta::validarCaracteres($Linea_de_Contacto));
        $InsertaRegistro->bindValue(19,RegistrarOferta::validarCaracteresCorreo($Correo));
        $InsertaRegistro->bindValue(20,RegistrarOferta::validarCaracteres($Ubicacion));
        $InsertaRegistro->bindValue(21,RegistrarOferta::validarCaracteres($Direccion));
        $InsertaRegistro->bindValue(22,RegistrarOferta::validarCaracteres($CoordenadaX));
        $InsertaRegistro->bindValue(23,RegistrarOferta::validarCaracteres($CoordenadaY));
        $InsertaRegistro->bindValue(24,$OperacionComercial);
        $InsertaRegistro->bindValue(25,$CodigoScoringFijo);
        $InsertaRegistro->bindValue(26,$CodigoScoringMovil1);
        $InsertaRegistro->bindValue(27,$CodigoScoringMovil2);
		
      if(!$InsertaRegistro->execute())
      {
          throw new \Arpu\Exception\QueryNoEjecutado();
          
      }else{
          echo json_encode('Pedido Registrado');
      }
    }
    
    public static function validarCaracteres($texto) {
        return trim(str_replace('\n','',str_replace('\t','',str_replace('\r','',$texto))));
    }
    public static function validarCaracteresCorreo($texto) {
        $texto = RegistrarOferta::validarCaracteres($texto);
        if($texto != '' && $texto != '-') {
            $CaracterFirst = substr($texto,0,1);
            if($CaracterFirst == '-') {
                $texto = substr($texto,1);
            }

            $CaracterLast = substr($texto,strlen($texto)-1,1);
            if($CaracterLast == '-') {
                $texto = substr($texto,0,strlen($texto)-1);
            }
        }
        return $texto;
    }

    public static function ObtenerDocumento($objeto){
        
        $documento = $objeto->cliente->Documento;
        
        if($objeto->cliente->Documento === ''
                || $objeto->cliente->Documento === 'Competencia'){
            $documento = $objeto->Documento;
        }
        
        return $documento;
    }
    
    public static function MarcarOfertaAdicional($objeto){
        
        if(isset($objeto->oferta->Prioridad)){
            $prioridad = 'Oferta '.$objeto->oferta->Prioridad.' - ';
        }
        else {
            $prioridad = 'Oferta Adicional - ';
        }    
        
        return $prioridad;
    }
    
    public static function MarcarCoordenadas($objeto){
        
        if($objeto->x != '' && $objeto->y != '' 
            && $objeto->x != 'x: ' && $objeto->y != 'y: ' ){
            $Coordenada = " | ".$objeto->x.";".$objeto->y;
        }else{
            $Coordenada = "";
        }
        
        return $objeto->cliente->Fijo->Direccion.$Coordenada;
    }
    public static function MarcarCoordenada($coordenada){
        $coordenadaUpper=strtoupper(trim($coordenada));
        if($coordenadaUpper == '' || $coordenadaUpper == 'X:' || $coordenadaUpper == 'Y:') {
            $coordenada = "";
        }
        
        return $coordenada;
    }
    public static function ObtenerOperacionComercial($objeto){
        
        $OperacionComercial = 'Completa MT';
        
        if( $objeto->cliente->Fijo->MovistarTotal 
                && $objeto->cliente->Movil1->MovistarTotal ){
        
            if(!$objeto->cliente->Movil2->MovistarTotal){
                
              if($objeto->cliente->Movil1->IdPlan === $objeto->oferta->IdPlan){
                $OperacionComercial = 'Segunda Linea';  
              }
              else{
                $OperacionComercial = 'Migracion MT + Segunda Linea';  
              }
            
                
            }else{
                $OperacionComercial = 'Migracion MT';
            }
        }     
        
        return $OperacionComercial; 
    }
}

try{
    $datos = file_get_contents('php://input');
    
    $objeto = json_decode($datos);
    
    if(is_object($objeto)){
        
        $documento = RegistrarOferta::ObtenerDocumento($objeto);
        $Prioridad = RegistrarOferta::MarcarOfertaAdicional($objeto);
        //$Direccion = RegistrarOferta::MarcarCoordenadas($objeto);
        $x = RegistrarOferta::MarcarCoordenada($objeto->x);
        $y = RegistrarOferta::MarcarCoordenada($objeto->y);
	    $OperacionComercial = RegistrarOferta::ObtenerOperacionComercial($objeto);

        RegistrarOferta::insertarRegistroOferta(
                $objeto->asesorUsuario,
                $objeto->asesorNombre,
                $objeto->asesorDocumento,
                $objeto->cliente->Nombre,
                $objeto->cliente->TipoDocumento,
                $documento,
                $objeto->ClienteFijoRegistro,
                $objeto->cliente->Fijo->Categoria,
                $objeto->operacionComercialFijo,
                $objeto->ClienteMovil1Registro,
                $objeto->cliente->Movil1->Categoria,
                $objeto->operacionComercialMovil1,
                $objeto->ClienteMovil2Registro,
                $objeto->cliente->Movil2->Categoria,
                $objeto->operacionComercialMovil2,
                $Prioridad.$objeto->oferta->Nombre,
                $objeto->oferta->Salto,
                $objeto->numeroContacto,
                $objeto->correoCliente,
                $objeto->cliente->Fijo->Ubicacion,
                $objeto->cliente->Fijo->Direccion,
                $x,
                $y,
                $OperacionComercial,
                $objeto->CodigoScoringFijo,
                $objeto->CodigoScoringMovil1,
                $objeto->CodigoScoringMovil2
            );
        
    } else {
        echo json_encode('Pedido No Registrado');
    }
} catch(\Arpu\Exception\QueryNoEjecutado $e){
    echo json_encode('Pedido ya ha sido registrado');
}
catch (Exception $e)
{
   $error = $e->getMessage();
   if(strpos($error,'SQLSTATE[23000]') !== false){
       echo json_encode('Cliente ya cuenta con un pedido registrado. No se grabará este pedido');
   }else{
       echo json_encode('Datos Incorrectos');
   } 
}