<?php

header('Content-type: application/json');

require_once(dirname(__FILE__).'/../Arpu/Autoload.php');

use Arpu\Config\Config;
use Arpu\Util\Encoder;

$conexion = Config::ConexionBD();

$consulta = $conexion->query("select * from registropedido "
        . "where Asesor_Usuario != 'DVICTODION' AND DATE(Fecha_Solicitud)=CURDATE() order by Fecha_Solicitud DESC");

$resultado = [];


while( ($pedido = $consulta->fetchObject())){
    
    if(!isset($resultado[$pedido->ID])){
        $resultado[$pedido->ID] = [];
    }

    $resultado[$pedido->ID] = $pedido;
    
}

Encoder::utf8_encode_deep($resultado);

echo json_encode($resultado);
        