<?php

require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Autenticacion\Autenticacion;
use Arpu\Http\LectorParametros;
use Arpu\ServiciosIn\Portabilidad;
Autenticacion::AsegurarSesionJson();

//Receipt 22 Movistar
//Donor 24 Viettel
//Leer Tipo de Documento
//Leer Numero de Documento
//Leer Tipo de Plan
//Leer Celular

$operadorOrigen = $_GET['operadorOrigen'];
$tipoDocumento = $_GET['tipoDocumento'];
$documento = $_GET['documento'];
$celular = $_GET['celular'];
$flagPostpagoPrepago = $_GET['flag_postpago_prepago'];
$socket = $_GET['socket'];



Portabilidad::Consultar($operadorOrigen, $tipoDocumento, $documento, $celular, $flagPostpagoPrepago,$socket);