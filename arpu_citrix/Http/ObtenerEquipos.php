<?php

header('Content-type: application/json');

require_once(dirname(__FILE__).'/../Arpu/Autoload.php');

use Arpu\Config\Config;
use Arpu\Http\LectorParametros;

$conexion = Config::ConexionBD();

$codigoFinanciamiento = LectorParametros::LeerTipoPago_Nuevo();
$variableMovil = LectorParametros::LeerVariableMovil();
$consulta = $conexion->query('select * from equipos_nuevo');

$resultado = [];

while(($equipo = $consulta->fetchObject())){
    if(!isset($resultado[$equipo->Marca])){
        $resultado[$equipo->Marca] = [];
    }

    $equipo->PrecioFinanciado0 = 0;
    
    if($variableMovil=='Competencia'){
        if($equipo->TipoGama == 'Entrada'){ 
            if($codigoFinanciamiento==1){ 
                $equipo->PrecioFinanciado0 = 99;
            }elseif($codigoFinanciamiento==4){
                $equipo->PrecioFinanciado0 = 0;
            }elseif($codigoFinanciamiento==5){
                $equipo->PrecioFinanciado0 = 0;
            }
        }elseif($equipo->TipoGama == 'Media'){
            if($codigoFinanciamiento==1){ 
                $equipo->PrecioFinanciado0 = 199; 
            }elseif($codigoFinanciamiento==4){
                $equipo->PrecioFinanciado0 = 0;
            }elseif($codigoFinanciamiento==5){
                $equipo->PrecioFinanciado0 = 0;
            }
        }elseif($equipo->TipoGama == 'Alta'){
            if($codigoFinanciamiento==1){ 
                $equipo->PrecioFinanciado0 = 399; 
            }elseif($codigoFinanciamiento==4){
                $equipo->PrecioFinanciado0 = 0;
            }elseif($codigoFinanciamiento==5){
                $equipo->PrecioFinanciado0 = 399;
            }
        }elseif($equipo->TipoGama == 'Premium'){
            if($codigoFinanciamiento==1){ 
                $equipo->PrecioFinanciado0 = 1599;
            }elseif($codigoFinanciamiento==4){
                $equipo->PrecioFinanciado0 = 1599;
            }elseif($codigoFinanciamiento==5){
                $equipo->PrecioFinanciado0 = 1599;
            }
        }        
    }else{
        if($equipo->TipoGama == 'Entrada'){ 
            if($codigoFinanciamiento==1){ 
                $equipo->PrecioFinanciado0 = 99;
            }elseif($codigoFinanciamiento==4){
                $equipo->PrecioFinanciado0 = 0;
            }elseif($codigoFinanciamiento==5){
                $equipo->PrecioFinanciado0 = 0;
            }
        }elseif($equipo->TipoGama == 'Media'){
            if($codigoFinanciamiento==1){ 
                $equipo->PrecioFinanciado0 = 199;
            }elseif($codigoFinanciamiento==4){
                $equipo->PrecioFinanciado0 = 0;
            }elseif($codigoFinanciamiento==5){
                $equipo->PrecioFinanciado0 = 0;
            }
        }elseif($equipo->TipoGama == 'Alta'){
            if($codigoFinanciamiento==1){ 
                $equipo->PrecioFinanciado0 = 399;
            }elseif($codigoFinanciamiento==4){
                $equipo->PrecioFinanciado0 = 0;
            }elseif($codigoFinanciamiento==5){
                $equipo->PrecioFinanciado0 = 0;
            }
        }elseif($equipo->TipoGama == 'Premium'){
            if($codigoFinanciamiento==1){ 
                $equipo->PrecioFinanciado0 = 1599;
            }elseif($codigoFinanciamiento==4){
                $equipo->PrecioFinanciado0 = 1599;
            }elseif($codigoFinanciamiento==5){
                $equipo->PrecioFinanciado0 = 1599;
            }
        }     
    }

    
    $resultado[$equipo->Marca][] = $equipo;
}

Arpu\Util\Encoder::utf8_encode_deep($resultado);

echo json_encode($resultado);