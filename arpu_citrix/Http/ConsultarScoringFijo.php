<?php
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Autenticacion\Autenticacion;
use Arpu\Http\LectorParametros;
use Arpu\ServiciosIn\ScoringFijo;


Autenticacion::AsegurarSesionJson();


try {
        $documento = LectorParametros::LeerDocumento();
        $tipoDocumento = LectorParametros::LeerTipoDocumento();
        $departamento = LectorParametros::LeerDepartamento();
        $provincia = LectorParametros::LeerProvincia();
        $distrito = LectorParametros::LeerDistrito();
        $ubigeo = LectorParametros::LeerUbigeo();
        echo json_encode(ScoringFijo::Ejecutar($tipoDocumento,$documento, $departamento, $provincia, $distrito,$ubigeo));
}catch(\Exception $ex) {
    echo ScoringFijo::RESPUESTA_POR_DEFECTO.'"errorInterno": "'.$ex->getMessage().'"}';
}
    
    