<?php

require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Autenticacion\Autenticacion;
Autenticacion::AsegurarSesionJson();

use Arpu\Http\LectorParametros;
use Arpu\Data\ClienteDLMovil;

try {
    $telefono = LectorParametros::LeerMovil("");
    $movil = ClienteDLMovil::LeerMovil($telefono);
    
    if($movil->TipoServicio === 'Movil' && $movil->Valido && !$movil->MovistarTotal){
        echo json_encode((object)['ok' => 'Celular Movistar encontrado', 'Renta' =>$movil-> RentaTotalConAdicionales]); 
    } elseif($movil->TipoServicio === 'Movil' && $movil->Valido && $movil->MovistarTotal) {
        echo json_encode((object)['ok' => 'Celular Movistar MT']);
    } elseif($movil->TipoServicio === 'Movil' && !$movil->Valido){
        echo json_encode((object)['error' => 'Celular Movistar no paquetizable']); 
    } elseif($movil->TipoServicio === 'Competencia'){
        echo json_encode((object)['error' => 'Celular no Movistar']);
    } else {
        die('Here');
    }
} catch (PDOException $exception) {
    echo json_encode((object) ['error' => 'Problema interno', 'errorInterno' => get_class($exception)]);
} catch (Arpu\Exception\QueryNoEjecutado $exception) {
    echo json_encode((object) ['error' => 'Problema interno', 'errorInterno' => get_class($exception)]);
} catch (\Exception $exception) {
    echo json_encode((object) ['error' => $exception->getMessage()]);
}
