<?php


require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Autenticacion\Autenticacion;
use Arpu\Http\LectorParametros;
use Arpu\ServiciosIn\ScoringMovil;



Autenticacion::AsegurarSesionJson();

$operadores = [
    '20' => 'Entel',
    '21' => 'America Movil Peru SAC (Claro)',
    '23' => 'Winner Systems',
    '24' => 'Viettel',
    '64' => 'Operador virtual'
];

try {
    
    //No Cambiar este orden de construccion
    $consulta = new stdClass();
    
    $consulta->tipDocumento = LectorParametros::LeerTipoDocumento();
    $consulta->numDocumento = LectorParametros::LeerDocumento();
    $consulta->sisOrigen = "ACTI";
    $portabilidad = LectorParametros::LeerPortabilidad(true);
    
    if($portabilidad === '1'){
        $consulta->portabilidad = $portabilidad;
        $consulta->operador = LectorParametros::LeerOperador();
        
        if(!isset($operadores[$consulta->operador])){
          throw new Arpu\Exception\ErrorProgramacionInterno();  
        } 
        $consulta->nombreOperador = $operadores[$consulta->operador];
        $consulta->fechaAlta = LectorParametros::LeerFechaAlta();
        $consulta->codigoSegmento = LectorParametros::LeerSegmentoMovil();
    }
    
    
    echo json_encode(ScoringMovil::Ejecutar($consulta));
    //ErrorConsultaServicioREST
    //DocumentoNoEnviado
    //DocumentoInvalido
} catch (Exception $ex) {
    echo ScoringMovil::RESPUESTA_POR_DEFECTO.'"errorInterno": "'.$ex->getMessage().'"}';
}

