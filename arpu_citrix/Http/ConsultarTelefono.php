<?php

require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Autenticacion\Autenticacion;
Autenticacion::AsegurarSesionJson();

use Arpu\Http\LectorParametros;
USE Arpu\Data\ClienteDL;

class ConsultarTelefono {
    public static function Procesar(){
        try {
            self::Consultar();
        } catch (Exception $ex) {
            $ex = null;
            self::RespuestaVacia();        
        }
    }
    
    private static function Consultar(){
        $telefono = LectorParametros::LeerTelefono();
        self::ProcesarTelefono($telefono);
    }
    
    private static function ProcesarTelefono($telefono){
        if($telefono){
            $fijo = ClienteDL::Leer($telefono,null,'CuatroPlay');
            $resultado = new stdClass();
            $resultado->ok = 'Telefono encontrado';
            $resultado->Fijo = $fijo;
            
            if($resultado->Fijo->TipoDocumento == 'RUC' || strlen($resultado->Fijo->Documento) == 11) {
                self::RespuestaRUC();
                return;
            }

            echo json_encode($resultado);
        } else {
            self::RespuestaVacia();
        }
    }
    
    private static function RespuestaVacia(){
        echo json_encode((object)array('error' => 'Telefono no encontrado'));
    }

    private static function RespuestaRUC(){
        echo json_encode((object)array('error' => 'Telefono pertenece a Cliente con Documento RUC'));
    }
}

ConsultarTelefono::Procesar();