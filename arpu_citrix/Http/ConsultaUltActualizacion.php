<?php

ini_set('display_errors',1);
header('Content-type: application/json');
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');

use Arpu\Config\Config;
use Arpu\Http\LectorParametros;

$data = array();
try
{
   $conexion = Config::ConexionBD();
   $consulta = $conexion->query("SELECT date_format(current_timestamp,'%d/%m/%Y') FECHA");
   if(($datos = $consulta->fetchObject()))
   {
     $data['PLANTA_CALCULADORA'] = "Actualizado al ".$datos->FECHA;
   }

   unset($consulta);
   $consulta = $conexion->query("SELECT date_format(current_timestamp,'%d/%m/%Y') FECHA");
   if($datos = $consulta->fetchObject())
   {
     $data['INFO_AVERIAS'] = "Actualizado al ".$datos->FECHA;
   }

   echo json_encode($data);

} catch (Exception $e)
{
   echo $e->getMessage();
}
