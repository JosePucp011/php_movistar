<?php

header('Content-type: application/json');
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
use Arpu\Config\Config;

use Arpu\Http\LectorParametros;

$conexion = Config::ConexionBD();

$TipoPago = LectorParametros::LeerTipoPago();
$rango =  LectorParametros::LeerRango();
$TipoCliente = LectorParametros::LeerTipoCliente();
$Modelo = LectorParametros::LeerModelo();
$CuotaInicial = LectorParametros::LeerCuotaInicial();
$CuotaMensualMaxima = LectorParametros::LeerCuotaMensualMaxima();
$PrecioFinanciado0 = LectorParametros::LeerPrecioFinanciado0();
$CuotaMes = LectorParametros::LeerCuotaMes();

$Pago = 'Contado';

if($TipoPago !== 'Contado')
{
    $Pago = 'Financiado';
}

$query = "select * from estrategia where TipoPago = '".$Pago."' and TipoCliente = '".$TipoCliente."' and Modelo = '".$Modelo."' and  rango = '".$rango."';";


$consulta = $conexion->query($query);

$resultado = new stdClass();

$resultado->PrecioEquipo = '';
$resultado->MontoFinanciado = '';
$resultado->CuotaMensual = '';
$resultado->LCInsuficiente = 0;
$resultado->CuotaInicial = 0;

$Precio = 0;

while( ($equipo = $consulta->fetchObject())){
    if(isset($equipo->Precio)){
        $Precio = $equipo->Precio; 
    }
}

if($Precio != 0){
    $MontoFinanciado = 0;
    $CuotaMensual = 0;
    $CuotaInicialTemp = 0;
    
    //Calculando Cuota Inicial
    if($TipoPago == 'Contado') {
        $CuotaInicialTemp = $Precio;
    } elseif($TipoPago == 'F0') {
        $CuotaInicialTemp = $PrecioFinanciado0;
    } elseif($TipoPago == 'F50') {
        $CuotaInicialTemp = round($Precio/2,0);
    }

    if($CuotaInicialTemp > $CuotaInicial) {
        $CuotaInicial = $CuotaInicialTemp;
    }

    if($CuotaInicial > $Precio) {
        $CuotaInicial = $Precio;
    }

    if($TipoPago === 'F50')
    {
        $MontoFinanciado = $Precio - $CuotaInicial;
    }elseif($TipoPago === 'F0'){
        $MontoFinanciado = $Precio - $CuotaInicial;
    }

    $blCorrecto = true;
    if($MontoFinanciado > 0){
        $CuotaMensual = round($MontoFinanciado/$CuotaMes,0);

        if($CuotaMensual > $CuotaMensualMaxima) {
            $blCorrecto = false;
        }
    }

    if($blCorrecto) {
        $resultado->PrecioEquipo = $Precio;
        $resultado->CuotaMensual = $CuotaMensual;
        $resultado->MontoFinanciado = $MontoFinanciado;
        $resultado->CuotaInicial = $CuotaInicial;
    } else {
        $resultado->PrecioEquipo = 'No Aplica';
        $resultado->LCInsuficiente = 1;
    }

    /*$resultado->PrecioEquipo = $Precio;
    if($TipoPago === 'F50')
    {
        $resultado->MontoFinanciado = round($Precio/2,0) - $CuotaInicial;
    }elseif($TipoPago === 'F0'){
        $resultado->MontoFinanciado = $Precio - $CuotaInicial;
    }else{
        $resultado->MontoFinanciado = 0;
    }

    if($resultado->MontoFinanciado > 0){
        $resultado->CuotaMensual = round($resultado->MontoFinanciado /12,0);
    }else{
        $resultado->CuotaMensual = 0;
    } */
}else{
    $resultado->PrecioEquipo = 'No Aplica';
}

echo json_encode($resultado);