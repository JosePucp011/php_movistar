<?php

header('Content-type: application/json');
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
use Arpu\Services\OfertaSugerida;
use Arpu\Entity\ClienteNoEncontrado;
use Arpu\Http\LectorParametros;
use Arpu\Util\Encoder;


function ObtenerTelefono($cliente){
    if ($cliente->Telefono < 0) {
        return '';
    } else {
        return $cliente->Telefono;
    }
}

function ObtenerProductoOrigen($cliente){
    $productoOrigen = new stdClass();
    if($cliente->Paquete->Ps > 0){
        $productoOrigen->PaquetePsOrigen = $cliente->Paquete->Ps;
        $productoOrigen->PaqueteOrigen = $cliente->Paquete->Nombre;
    } elseif($cliente->Telefono < 0) {
        $productoOrigen->PaquetePsOrigen = 'MSS';
        $productoOrigen->PaqueteOrigen = ('Television ' . $cliente->Cable->Tipo);
    } else {
        $productoOrigen->PaquetePsOrigen = 'MLN';
        $productoOrigen->PaqueteOrigen = $cliente->Linea->Nombre;
    }
    return $productoOrigen;
}

function ObtenerProductoDestino($producto,$cliente){
    $ProductoDestinoPs = '';
    $SVAs = '';
    if ($producto->Paquete->EsSVA === 1) {
        $ProductoDestinoPs = '';
        $SVAs = $producto->Paquete->Ps;
    } else {
        $ProductoDestinoPs = $producto->Paquete->Ps;
        $SVAs = '';
        if($cliente->Telefono == '15962120' && 
           $producto->Paquete->Nombre == 'Trio Movistar Voz 20Mbps Estandar Digital + Smart Wifi') {
            $SVAs = '23025';
        }

        foreach ($producto->Paquete->PsAdicionales as $PsAdicionales) {
            $SVAs = $SVAs . $PsAdicionales;
        }
    }
    
    $productoDestino = new stdClass();
    $productoDestino->ProductoDestinoPs = $ProductoDestinoPs;
    $productoDestino->SVAs = $SVAs;
    return $productoDestino;
}

function ObtenerObjetoOferta($cliente,$opcion,$prioridad){
    $objeto = new stdClass();
    $objeto->Tipo = $opcion->producto->Paquete->Tipo;
    $objeto->Telefono = ObtenerTelefono($cliente);
    $objeto->Servicio = $cliente->Cable->Servicio;

    $productoOrigen = ObtenerProductoOrigen($cliente);
    $objeto->PaquetePsOrigen = $productoOrigen->PaquetePsOrigen;
    $objeto->PaqueteOrigen = $productoOrigen->PaqueteOrigen;
    $objeto->CantidadDecos = $cliente->Cable->CantidadDeDecos;
    $objeto->RentaTotal = $cliente->RentaTotal;
    $objeto->Internet = $cliente->Internet->Velocidad;
    $objeto->Prioridad = $prioridad;

    $productoDestino = ObtenerProductoDestino($opcion->producto,$cliente);
    $objeto->ProductoDestinoPs = $productoDestino->ProductoDestinoPs;
    $objeto->SVAs = $productoDestino->SVAs;
    $objeto->ProductoDestinoNombre = $opcion->producto->Paquete->Nombre;
    $objeto->ProductoDestinoRenta = $opcion->producto->Paquete->Renta;
    //$objeto->MontoFinanciando = $cliente->Internet->Tecnologia;
    $objeto->MontoFinanciando = $opcion->producto->Internet->Tecnologia;
    $objeto->CuotasFinanciado = '';

    $objeto->CodigoOferta = $opcion->codigoOperacionComercial;
    return $objeto;
}


function ProcesarOferta($oferta) {
    $ID = 1;
    $resultado = array();
    foreach ($oferta->Opciones as $key => $opcion) {
        $objeto = ObtenerObjetoOferta($oferta->Cliente,$opcion,$key);
        $objeto->Prioridad = $key;
        $resultado[] = $objeto;
        ++$ID;
    }

    foreach ($oferta->Ofertas as $key => $opcion) {
        if ($opcion->mejor_up === null) {
            $objeto = ObtenerObjetoOferta($oferta->Cliente,$opcion,$ID);
            $resultado[] = $objeto;
            ++$ID;
        }
    }

    return $resultado;
}


try
{
	
   $identificador = LectorParametros::LeerIdentificadorNegativo();
   
   $oferta = OfertaSugerida::Obtener(
           $identificador->telefono, 
           $identificador->documento,
           'App',
           'BandaAncha');
	
   
   $resultado = ProcesarOferta($oferta);
   Encoder::utf8_encode_deep($resultado);
   echo json_encode($resultado);
}
catch(InvalidArgumentException $excepcion)
{
   die(json_encode((object)array('error' => 'Parametro Incorrecto')));
}
catch (ClienteNoEncontrado $excepcion)
{
	die(json_encode((object) array('error' => 'No se encuentra al cliente' )));
}
catch(Exception $excecion)
{
   die(json_encode((object) array('error' => 'Ocurrio error Interno')));
}

