<?php
$consultationNumber = uniqid();

header('Content-type: application/xml');


echo <<<HERE
<ns2:PrevalidatePortInResponse xmlns:ns2="http://telefonica.pe/CustomerManagement/Portability/V1/types">
    <ns3:TefHeaderRes xmlns:ns3="http://telefonica.pe/TefResponseHeader/V1">
        <ns3:idMessage>89f33c96-abfa-44c6-9c8e-5e3ce713bc27</ns3:idMessage>
        <ns3:serviceName>prevalidatePortIn</ns3:serviceName>
        <ns3:responseDateTime>2018-08-20T17:29:58.993-05:00</ns3:responseDateTime>
        <ns3:transactionID>68c445cf-143f-4e67-9c97-6a4e3ace88d0</ns3:transactionID>
    </ns3:TefHeaderRes>

    <ns2:PrevalidatePortInResponseData>
        <ns2:previousConsultationNumber>$consultationNumber</ns2:previousConsultationNumber>
        <ns2:status>
            <ns2:code>000</ns2:code>
            <ns2:description>OK</ns2:description>
        </ns2:status>
    </ns2:PrevalidatePortInResponseData>
</ns2:PrevalidatePortInResponse>

HERE;
