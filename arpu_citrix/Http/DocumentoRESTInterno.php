<?php
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');
header('Content-type: application/json');

use Arpu\Autenticacion\Autenticacion;
use Arpu\Http\ConsultarDocumento;


Autenticacion::AsegurarSesionJson();

$log = new Arpu\Data\LoggearTransaccion();

$json = json_encode(ConsultarDocumento::Procesar());
echo $json;

$log->setearResultado($json);
$log->log();