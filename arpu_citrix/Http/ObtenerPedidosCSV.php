<?php
header('Content-type: text/csv');
header('Content-Disposition: attachment; filename="Lista_Pedidos.csv"');
ini_set('memory_limit','-1');
require_once(dirname(__FILE__).'/../Arpu/Autoload.php');

use Arpu\Config\Config;

$conexion = Config::ConexionBD();

$consulta = $conexion->query("select * from registropedido  where Asesor_Usuario != 'DVICTODION' order by ID");

$resultado = [];


echo "ID,Asesor Usuario,Asesor Nombre,Asesor DNI,Fecha Solicitud,Cliente Nombre,Cliente TipoDocumento,Cliente Documento,Fijo Numero,Fijo Tenencia,Fijo OperacionComercial,Movil1 Numero,Movil1 Tenencia,Movil1 OperacionComercial,Movil2 Numero,Movil2 Tenencia,Movil2 OperacionComercial,Oferta MT,Salto,Linea Contacto,Correo, Operacion Comercial , Codigo Scoring Fijo,Codigo Scoring Movil1,Codigo Scoring Movil2,Coordenada X,Coordenada Y,Ubicacion,Direccion\r\n";

while( ($pedido = $consulta->fetchObject())){
    $direccion = str_replace("\"", "", $pedido->Direccion);
    $x = str_replace("x:", "", $pedido->Coordenada_X);
    $y = str_replace("y:", "", $pedido->Coordenada_Y);

    if(!isset($resultado[$pedido->ID])){
        echo $pedido->ID.','.validarCaracteres($pedido->Asesor_Usuario).','.validarCaracteres($pedido->Asesor_Nombre).','.validarCaracteres($pedido->Asesor_DNI).','.$pedido->Fecha_Solicitud.','.quitarDelimitador(validarCaracteres($pedido->Cliente_Nombre)).','.$pedido->Cliente_TipoDocumento.','.validarCaracteres($pedido->Cliente_Documento).','.$pedido->Fijo_Numero.','.$pedido->Fijo_Tenencia.','.$pedido->Fijo_OperacionComercial.','.$pedido->Movil1_Numero.','.$pedido->Movil1_Tenencia.','.$pedido->Movil1_OperacionComercial.','.$pedido->Movil2_Numero.','.$pedido->Movil2_Tenencia.','.$pedido->Movil2_OperacionComercial.','.$pedido->Oferta_MT.','.$pedido->Salto.','.$pedido->Linea_de_Contacto.','.validarCaracteres($pedido->Correo).','.$pedido->MT_OperacionComercial.','.$pedido->Codigo_ScoringFijo.','.$pedido->Codigo_ScoringMovil1.','.$pedido->Codigo_ScoringMovil2.','.quitarDelimitador(validarCaracteres($x)).','.quitarDelimitador(validarCaracteres($y)).','.validarCaracteres($pedido->Ubicacion).','.quitarDelimitador(validarCaracteres($direccion)).','."\r\n";
    }
}

function validarCaracteres($texto) {
    return trim(str_replace('\n','',str_replace('\t','',str_replace('\r','',$texto))));
}

function quitarDelimitador($texto) {
    return trim(str_replace(',',' ',$texto));
}
