<?php
session_start();
if (!isset($_SESSION["logged_user"]) || isset($_SESSION["logged_user"]) && !$_SESSION["logged_user"])
{
    header('Location: login.php');
}
require_once(dirname(__FILE__).'/Arpu/Autoload.php');
use Arpu\Http\LectorParametros;

$retenciones = LectorParametros::LeerRetenciones();
$redirect = 'index.php';

?>
<!DOCTYPE html>
<html>
    <head>
<title>Calculadora de ARPU</title>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link rel="stylesheet" href='css/main.css' type='text/css'>
<link rel='stylesheet' href='css/custom-theme/jquery-ui-1.9.0.custom.min.css' type='text/css'>

<link rel="shortcut icon" href="images/favicon.ico">
   
<script src='scripts/jquery-1.8.2.js' type='text/javascript'></script>
<script src='scripts/main.js' type='text/javascript'></script>
<script src='scripts/util.js' type='text/javascript'></script>
<script src='scripts/Comparacion.js' type='text/javascript'></script>
<script src='scripts/Formulario.js' type='text/javascript'></script>
<script src='scripts/DatosCliente.js' type='text/javascript'></script>
<script src='scripts/OfertaDetalle.js' type='text/javascript'></script>
<script src='scripts/ParrillaDibujar.js' type='text/javascript'></script>



<script src='index.js' type='text/javascript'></script>
<script src='scripts/jquery-ui-1.9.0.custom.min.js' type='text/javascript'></script>
<script src='scripts/jquery-ui-1.9.0.custom.js' type='text/javascript'></script>

</head>
<body>

<div id="modal-descuento-retenciones" title="DESCUENTOS A OFRECER"> </div>
<div id="modal-herramienta-retenciones" title="HERRAMIENTAS A OFRECER"> </div>
<div id="modal-NoRetener" title="NO MIGRAR"> </div>
<div id="modal-ClienteRiesgoso" title="CLIENTE RIESGOSO"> </div>
<div id="modal-IPFija" title="MIGRACION LINEA"> </div>
<div id="modal-registro-oferta" title="Datos de Registro"></div>
<div id="modal-Freeview" title="BENEFICIOS MOVISTAR"></div>
<div id="modal-Reclamo" title="RECLAMOS"></div>
<div id="modal-IncrementoPrecio" title="INCREMENTO DE PRECIO"></div>
<div id="modal-ReasignacionPrecio" title="MIGRACION DE PARRILLA"></div>
<div id="modal-IncrementoPrecioArgumentario" title="Argumentario incremento de tarifa"></div>
<div id="modal-PreguntasFrecuentes" title="Preguntas Frecuentes"></div>
<div id="modal-CiudadSitiada" title="CIUDAD SITIADA"></div>
<div id="modal-analgesico" title="Plan Analgésico"></div>

<!-- Para el Speech 1era Linea -->
<!-- <div id="modal-SpeechLinea" title="SPEECH DE 1ERA LINEA"></div> -->
<div id="modal-SpeechLinea" title="SPEECH"></div>

<div id="modal-CambioTecnologia" title="Prioridad Cambio de Tecnología"> </div>
<div id="modal-AjusteIncrementoPrecio" title="Mensaje"> </div>
<div id="modal-ClientePotencial" title="Aviso"> </div>
<div id="modal-ApagadoCobre" title="Mensaje Apagado Ura Venezuela"> </div>
<div id="modal-RetencionDuoTrio" title="Prioridad completa tu trio"> </div>
<div id="modal-Desposicionado" title="Prioridad actualización de paquete"> </div>
<div id="modal-CompetenciaFTTH" title="Beneficios de FTTH"> </div>
<div id="modal-DescuentoPermanente" title="Informar al cliente"> </div>
<div id="modal-error-incremento" title="Monto de incremento tarifario errado"> </div>
<div id="modal-error-reconexion" title="Monto de reconexión errado"> </div>

<script>
<?php    
if($retenciones == 'Migracion Down'){
?>
window.modoPorDefecto = '#modoRetenciones';
window.opcionPorDefecto = "#retenciones_migracion_down";
<?php
} elseif($retenciones == 'Averias'){
?>
window.modoPorDefecto = '#modoAverias';
<?php
} elseif($retenciones == 'Online'){
?>
window.modoPorDefecto = '#modoOnline';
<?php
}
?>
    
    
    
</script>

<table>
<tr>

<!-- Panel de busqueda -->
<td valign='top' id="PanelIzquierda">
    <h2 id="dv_dtfechaultactualizacion"><img src="images/loading.gif"></h2>

<div id='PanelDatosBusqueda'>
    
<form action='' method='post' onsubmit="return false;">
<div id='modosDeOperacion'>
<input type='radio' name='modo' id='modoRegular'>
<label for='modoRegular'>Venta</label>
<input type='radio' name='modo' id='modoRetenciones'>
<label for='modoRetenciones'>Retencion</label>
<input type='radio' name='modo' id='modoAverias'>
<label for='modoAverias'>Tecnico</label>
<input type='radio' name='modo' id='modoOnline'>
<label for='modoOnline'>Online</label>
</div>
<div id='retenciones_opciones'>
<input type='radio' id='retenciones_migracion_down'name='tipo_retenciones' value='Migracion Down'><label for="retenciones_migracion_down">Migracion Down</label>
   <input type='radio' id='retenciones_baja' name='tipo_retenciones' value='Baja'><label for="retenciones_baja">Baja</label>
</div>
<div id="Identificadores">
    <table>    
        <tr><td>Tel&eacute;fono:</td><td><input  maxlength='8' autocomplete='off' type='text' id='telefono' name='telefono' value=''></td></tr>
        <tr><td>Documento:</td><td><input  maxlength='12'  autocomplete='off' type='text' id='numeroDocumento' name='numeroDocumento' value=''></td></tr>
    </table>
    <div class='error cliente_dato' id='error_busqueda'></div>
</div>
</form>
</div>

    


<div id='DatosCliente'>
<h2 id='cliente_nombre' class='medio cliente_dato'></h2>
<div class='error cliente_dato' id="cliente_DescuentoEmpleado" ></div>
<div class='error cliente_dato' id="cliente_Tecnica" ></div>
             
<table id='Tabla_Datos_Principales_Cliente'>
<?php
    $campos = array(
        'Cliente_Telefono' => 'Telefono',
        'Cliente_Ciclo' => 'Ciclo',
        'Cliente_Segmento' => 'Segmento',
        'Cliente_VelocidadMaxima' => 'Vel. Maxima',
        'Cliente_TiempoMigracionTecn' => 'SLA Migracion Tecnica',
        'Cliente_TiempoAveria' => 'SLA Averia',
        
        'Cliente_Digitalizado' => 'Fecha Digitalizacion',
        'Cliente_TipoBlindaje' => 'Tipo',
        'Cliente_ClienteCms' => 'Cliente CMS',
        'Cliente_ZonaCompetencia' => 'Zona Competencia',
        'Cliente_OperadorZonaCompetencia' => 'Operador Competencia',
        'Cliente_ProteccionDatos' => 'Proteccion de Datos',
        'Cliente_ReciboDigital' => 'Recibo Digital',
        );
    
    foreach($campos as $key => $value){
?>
    <tr>
    <td><?= $value.' :'?></td>
    <td><div class='cliente_dato' id=<?= "'".$key."'"  ?></div></td>
    </tr>
<?php
    }
?>
</table>             
               
<table id="TablaAverias">
<tr><th>Tipo</th><th>Cantidad</th></tr>

<?php
    $camposAverias = array(
        'Cliente_Averias' => 'Averias',
        'Cliente_Reclamos' => 'Reclamos',
        'Cliente_Llamadas' => 'Consultas Tecnicas'
        );
    
    foreach($camposAverias as $key => $value){
?>
    <tr>
        <td><a href='#' id='descuentoRetenciones_0' class='linkVolcado<?= $value.''?>' style='color: #008000; text-decoration: none;' > <?= $value.''?> </a></td>
        <td class='cliente_dato' id=<?= "'".$key."'"  ?> ></td>
    </tr>
<?php
    }
?>
</table>        

<a href='MovistarTotal.php'  target='new'  style='text-decoration:none;'><h2>Calculadora Movistar Total</h2></a>
<a href='http://10.4.40.108:8080/premium'  target='new'  style='text-decoration:none;'><h2>Calculadora Movil</h2></a>
<a href='http://innova/prevencion/'  target='new'  style='text-decoration:none; '><h2>Calculadora Prevencion</h2></a>


    
    
</td>

<!-- Panel central de contenido -->
<td align="center" valign='top'>
<table>
    <tr>
        <td id='MigracionMasiva'></td>
        <td id='sesion_consulta'>
        Para Soporte: <a href="mailto:Calculadora.Fija@telefonica.com">calculadora.fija@telefonica.com</a>
        <a href=<?= "\"login.php?action=logout&redirect=$redirect\"" ?> >Cerrar Sesi&oacute;n</a>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div id="NuevoContenido" class='medio'> </div>
			<div id="MensajeTrioSinDuoInternetTv" class='MensajeTrioSinDuoInternetTv' style="display:none"> </div> 
            <table id='ParrillaCompleto'>
               <tr>
                  <td><div id='parrilla'></div></td>
                  <td><div id='oferta'></div></td>
               </tr>
            </table>
        </td>
    </tr>
</table>




</tr>
</table>

</body>
</html>

