<!DOCTYPE html>

<?php
require_once(dirname(__FILE__).'/Arpu/Autoload.php');

use Arpu\Autenticacion\Autenticacion;
Autenticacion::AsegurarSesionHtml('Location: LoginTotal.php');

?>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="images/favicon.ico">

<title>Movistar Total</title>

<link rel="stylesheet" href='css/main_convergente.css' type='text/css'>
<link rel="stylesheet" href='css/fonts.css' type='text/css'>
<link rel="stylesheet" href='css/corporativo_convergente.css' type='text/css'>
<link rel="stylesheet" href='css/convergencia.css' type='text/css'>

<script><?php require 'scripts/jquery-1.8.2.js';?></script>
<script><?php require 'scripts/convergencia/Util.js'; ?></script>


<script>
    <?php
require 'scripts/convergencia/Comparacion.js';
require 'scripts/convergencia/Convergente.js';
require 'scripts/convergencia/ParrillaDibujar.js';
require 'scripts/convergencia/IncrementoPrecio.js';
require 'scripts/convergencia/CiudadSitiada.js';
require 'scripts/convergencia/Retenciones.js';
require 'scripts/convergencia/Freeview.js';
require 'scripts/convergencia/DatosCliente.js';
require 'scripts/convergencia/Pedido.js';
?></script>

<script>
$(function() {
    jQuery.fx.off = true;
    Pedido.Mostrar();
});
</script>


</head>


<body style="background-color: white; color: rgba(0, 0, 0, 0.5); margin-top: 0px;" >
    <div>
        <table style="width: 90%; margin: auto; border-bottom: 1px solid rgba(0, 0, 0, 0.2)">
            <tr>
                <td>
                    <a href="#" style="margin-left: 10%"><img style="width: 150px; padding-top: 0.3125rem;padding-bottom: 0.3125rem;" src="images/logo.png"></a>
                </td>
                <td style="text-align: right;">
                 <a class="CerrarSesion" style="margin-right: 10%" href='Menu_Total.php'>Menu</a>   
                 <a class="CerrarSesion" style="margin-right: 10%" href='LogoutTotal.php'>Cerrar Sesi&oacute;n</a>
                </td>
            </tr>
        </table>
    </div>

    <br>
    <h1>Lista de Pedido</h1>
    <table style="width: 100%;">
        <tr>
            <td><input type='submit' onclick="Pedido.Mostrar()" value='Actualizar' class="ActualizarPedidos" ></td>
            <td style="text-align: right;"><a href="Http\ObtenerPedidosCSV.php" class="linkPedidos" style=" margin-right: 10%;">Exportar</a>
        </tr>
    </table>
    
    <br>        
    <!--Consulta de Datos -->
    
    <div style='overflow: auto; margin-left: 5%; margin-right: 5%; margin-top: 1%;'>
        <div id='RegistroPedidos' style="width: 100%; height: 450px;"></div>
    </div>
    
</body>
</html>